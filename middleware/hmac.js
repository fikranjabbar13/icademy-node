const crypto = require("crypto");
const tempClient = [{
    '7e50c639d6edcefb7c33d70d8dad26de7d561b699b0227a4c37b0b39f3b9f4d7': {
        key: '1NV35PR0',
        secret: '1NV35PR0@2022',
        project_id: 86,
        meeting_id: 225,
        user_id: 52,
        company_id: 1,
    }
}];
exports.verify = (req, res, next) => {
    try {

        if (req.headers.apikey) {

            let check = tempClient.findIndex((str) => { if (str[req.headers.apikey]) { return true; } else { return false; } });

            if (check > -1) {
                let obj = tempClient[check][req.headers.apikey];
                const hash = crypto.createHmac('sha256', obj.secret).update(obj.key).digest("hex");
                if (req.headers.apikey === hash) {
                    console.log(req.body)
                    req.dataClient = {
                        user_id: obj.user_id,
                        project_id: obj.project_id,
                        meeting_id: obj.meeting_id,
                        company_id: obj.company_id
                    };
                    req.body.meeting_id = obj.meeting_id;
                    req.body.user_id = obj.user_id;
                    req.body.peserta = [];
                    req.body.is_required_confirmation = 0;
                    req.body.moderator = [];
                    req.body.is_akses = 1;

                    next(null);
                } else {
                    res.status = 401;
                    return res.json({ error: true, result: "Unauthorized Access" });
                }
            } else {
                res.status = 401;
                return res.json({ error: true, result: "Unauthorized Access" });
            }

        } else {
            res.status = 401;
            return res.json({ error: true, result: "Unauthorized Access" });
        }
    } catch (e) {
        res.status = 500;
        return res.json({ error: true, result: "Data Invalid" });
    }
}