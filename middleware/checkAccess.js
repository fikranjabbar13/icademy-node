const fs = require("fs");
const path = require("path");
const db = require("../configs/database");
const { checkAccessBySub, checkAccess_function } = require("../controllers/grup");


exports.routeList = async (data) => {
    try {
        let obj = { meeting: { sub: ['general', 'meeting'], list: [] }, webinar: { sub: ['general', 'webinar'], list: [] }, training: { sub: ['general', 'training'], list: [] }, general: { sub: ['general'], list: [] } };

        let routeData = '';

        if (fs.existsSync('../routeList.json')) {
            let datas = fs.readFileSync('../routeList.json', 'utf-8');
            if (datas.length > 0) {
                routeData = datas.toString();
            }
        }

        // sort
        if (data.length > 0) {
            data.forEach(arg => {
                let stack = arg.stack;
                stack.forEach(str => {
                    if (str.route.path.search("/meeting") > -1) {
                        obj.meeting.list.push(str.route.path)
                    }
                    else if (str.route.path.search("/webinar") > -1) {
                        obj.webinar.list.push(str.route.path)
                    }
                    else if (str.route.path.search("/training") > -1) {
                        obj.training.list.push(str.route.path)
                    } else {
                        obj.general.list.push(str.route.path)

                    }
                });
            });

            let dataJson = JSON.stringify(obj);
            if (routeData.length != dataJson.length) {
                //write JSON string to a file
                fs.writeFile('../routeList.json', dataJson, (err) => {
                    if (err) {
                        console.log(err)
                    }
                });
            }
        }
    } catch (e) { console.log(e, "routeList") }

}

exports.checkAccess = async (data, req) => {
    let access = await checkAccess_function(data);
    return access;
}

exports.checkAccessBySub = async (req, res, next) => {
    let routePath = req.route.path;
    let company_id = null;

    let routeData = '';

    if (fs.existsSync('../routeList.json')) {
        let datas = fs.readFileSync('../routeList.json', 'utf-8');
        if (datas.length > 0) {
            routeData = datas.toString();
        }
    }

    if (routeData.length > 0) {
        routeData = JSON.parse(routeData);
        if (routePath.search("/meeting") > -1 || routePath.search("/meet") > -1 || routePath.search("/meetpub") > -1) {

            if (routePath.search("/meet") > -1 || routePath.search("/meetpub") > -1) {
                let result = await db.query(`select l.company_id from liveclass_booking lb left join liveclass l on l.class_id = lb.meeting_id where lb.id='${req.params.booking_id}' limit 1;`);

                if (result.length > 0) {
                    company_id = result[0].company_id;
                }
            }
            req.app.sub = routeData.meeting.sub;
        }
        else if (routePath.search("/webinar") > -1) {
            if (routePath.search("/webinar/one") > -1 || routePath.search("/webinar/tamu/one") > -1) {
                if (!req.app.token) {
                    let result = await db.query(`select w.company_id from webinars w where w.id='${req.params.id}' limit 1;`);

                    if (result.length > 0) {
                        company_id = result[0].company_id;
                    }
                }
            }
            req.app.sub = routeData.webinar.sub;
        }
        else if (routePath.search("/training") > -1) {
            req.app.sub = routeData.training.sub;
        } else {
            req.app.sub = routeData.general.sub;
        }
        const dataAccess = {
            company_id: req.app.token ? req.app.token.company_id.toString() : company_id,
            sub: req.app.sub
        };
        let access = await checkAccessBySub(dataAccess);
        req.app.accessInfo = access;
    } else {
        req.app.accessInfo = [];
    }
    next(null);
}