/*required 
    apt install ffmpeg
*/
const { exec } = require("child_process");
const momentjs = require("moment-timezone");
const fs = require("fs");
const path = require("path");
const { BBB_URL } = require("../env.json");
const paths = "my-presentation";

exports.combineFile = async (recordId) => {
    return new Promise(async (resolve) => {
        //[AUDIO]https://con.icademy.id/presentation/f5354c576bb89b67972d7fe269df256a54fef036-1642474896475/video/webcams.mp4
        //[DESKSHARE]https://con.icademy.id/presentation/f5354c576bb89b67972d7fe269df256a54fef036-1642474896475/deskshare/deskshare.mp4
        //let recordId = 'f5354c576bb89b67972d7fe269df256a54fef036-1642474896475';

        let filename = `media-${recordId}.mp4`;
        let split_url = BBB_URL.split("/bigbluebutton")[0];
        let dirMinGW = 'C:/Program Files (x86)/GnuWin32/bin';
        let tmp = `${paths}/${recordId}`;
        let output = `${paths}/output`

        let audio = `${split_url}/presentation/${recordId}/video/webcams.mp4`;
        let deskshare = `${split_url}/presentation/${recordId}/deskshare/deskshare.mp4`;
        let webcams = `${split_url}/presentation/${recordId}/video/webcams.webm`;

        let wgetUnix = `cd ${tmp} && wget ${audio} ${deskshare} ${webcams}`;
        let wgetWin = `cd ${tmp} && "${dirMinGW}/wget.exe" ${audio} ${deskshare} ${webcams}`;

        try {

            if (!fs.existsSync(paths)) {
                fs.mkdirSync(paths);
            }
            if (!fs.existsSync(output)) {
                fs.mkdirSync(output);
            }
            if (!fs.existsSync(tmp)) {
                fs.mkdirSync(tmp);
            }

            // check temp files
            let checkFilePath = path.join(__basedir, `../${output}`);
            let Dir = `${checkFilePath}${filename}`;

            if (!fs.existsSync(Dir)) {
                exec(wgetUnix, async (error, stdout, stderr) => {
                    // if (error) {
                    //     console.log(error.message, "ERROR")
                    // }

                    // build ffmpeg
                    let filePath = path.join(__basedir, `../${tmp}/`);
                    let checkAudio = `${filePath}webcams.mp4`;
                    let checkDeskhare = `${filePath}deskshare.mp4`;
                    let checkWebcams = `${filePath}webcams.webm`;

                    let list = '';
                    let listRm = '';

                    if (fs.existsSync(checkAudio)) {
                        list += ` -i webcams.mp4 `;
                        listRm += ` webcams.mp4 `;
                    } else {
                        console.log("error build file");
                        //return resolve({ error: true, result: "Error build file." });
                    }
                    if (fs.existsSync(checkDeskhare)) {
                        list += ` -i deskshare.mp4 `;
                        listRm += ` deskshare.mp4 `;
                    }
                    if (fs.existsSync(checkWebcams)) {
                        list += ` -i webcams.webm `;
                        listRm += ` webcams.webm `;
                    }
                    console.log(listRm, "listRm")


                    // for win
                    let ffmpegUnix = `cd ${tmp} && ffmpeg ${list} -c copy ${filename} && cd .. && cp ${recordId}/${filename} output && rm -rf ${recordId}`;
                    // for unix
                    let ffmpegWin = `cd ${tmp} && "${dirMinGW}/ffmpeg.exe" ${list} -c copy ${filename} && cd .. && cp ${recordId}/${filename} output && rm -rf ${recordId}`;
                    console.log(ffmpegWin, 8888)

                    exec(ffmpegUnix, async (error, stdout, stderr) => {
                        if (error) {
                            console.log(error.message, "ERROR")
                        }
                        let dir = `${output}/${filename}`;
                        if (fs.existsSync(dir)) {
                            return resolve({ error: false, result: filename });
                        } else {
                            console.log("error build file");
                            return resolve({ error: true, result: "Error build file." });
                        }

                    });
                });
            } else {
                return resolve({ error: false, result: filename });
            }

        } catch (e) {
            console.log(e, 666)
            return resolve({ error: true, result: "Error packages3." });
        }
    });
}
