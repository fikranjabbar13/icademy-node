const db = require('../configs/database');

exports.updateStatusBulkQuestions = async (dataDetail) => {
    try {
        const results = await new Promise((resolve, reject) => {
            db.query(`UPDATE training_questions
            SET status= ?
            WHERE id IN (?);`,
                [dataDetail.status, dataDetail.dataIdQuestions],
                (err, res) => {
                    if (err) reject(err);
                    resolve(res);
                }
            );
        })
            .then((response) => response)

        return results;
    } catch (error) {
        throw error;
    }
};