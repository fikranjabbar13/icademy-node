const moment = require("moment");
let today = moment(new Date()).format("YYYY-MM-DD");
const db = require('../configs/database');
// task field
const sortField = ['title','description','project_id','milestone_id','assigned_to','deadline','labels','points','status','status_id','priority_id',
"well_type_condition","achieved_target","hole_depth",
'operator_id','operation_status', 'last_hour_operator', 'activity_status','drilling_gross','drilling_day','cost','npt','npt_hrs',
'target_start','target_end','next_target','predrill_gross','predrill_net','predrill_porosity','predrill_saturation','predrill_hc',
'postdrill_gross','postdrill_net','postdrill_saturation','postdrill_porosity','postdrill_hc','start_date','collaborators','parent_task_id','blocking','blocked_by',"sort"];
const arrAction = ['created','updated','deleted','bitbucket_notification_received','github_notification_received'];
const sortFieldActivity = ['created_by','action','log_type','log_type_id','log_for','log_for_id','changes','action_title'];
const fieldReminder = ['title','task_id','project_id','start_date','start_time','type','repeat_every','repeat_type','no_of_cycles','created_by'];
const fieldComment = ['description','task_id','project_id', 'comment_id', 'files', 'created_by'];
const FieldChecklistItem = ['title','task_id','is_checked',"sort","deleted"];
const FieldCommentAttribute = ['project_comment_id','created_by','pinned_by',"deleted"];
const FieldOperators = ['operator_id','operator_name'];

const addActivityTask = async (field, params) => {
    return new Promise(async(hasil)=>{
        try {
    
            let arrField = sortField;
            let arrFieldActivity = sortFieldActivity;
            let tmpData = [];
            let changeData = {};
            let result = null;
            let query = '';
    
            if(field.id){
                let manyValue = ["milestone_id","labels","collaborators"];
                query = `
                    SELECT ${sortField.toString()} from crm_tasks where id=? ;
                `;
                result = await db.query(query,[field.id]);
                if(result.length) result = result[0];

                for(let i=0; i < arrField.length; i++){


                    if(['start_date','deadline'].indexOf(arrField[i]) > -1){
                        let CheckDate = (result[arrField[i]] && result[arrField[i]] != '') ? moment(result[arrField[i]]).format('YYYY-MM-DD') : null;
                        result[arrField[i]] = CheckDate;
                        field[arrField[i]] = CheckDate;
                    }else{
                        if(field[arrField[i]] !== undefined){
                            field[arrField[i]] = field[arrField[i]]?.toString() || null;
                        }
                        if(result[arrField[i]]){
                            result[arrField[i]] = result[arrField[i]].toString();
                        }
                    }

                    if( result[arrField[i]] !== field[arrField[i]] ){
                        // if(manyValue.indexOf(arrField[i]) > -1){
                        // }
                        changeData[arrField[i]] = {
                            from: result[arrField[i]],
                            to: field[arrField[i]]
                        }
                    }
                }
            }
    
            if(Object.keys(changeData).length > 0){
                field.changes = JSON.stringify(changeData);
            }
    
            for(let i=0; i < arrFieldActivity.length; i++){
                if(field[arrFieldActivity[i]] !== undefined){
                    tmpData.push(field[arrFieldActivity[i]]);
                } 
            }
            
            
            query = `INSERT INTO crm_activity_logs(${sortFieldActivity.toString()}) value(?)`;
            result = await db.query(query,[tmpData]);
            return hasil({ error: false, result:'success' });
    
        } catch (error) {
            console.log(error);
            return hasil({ error: error, result:null });
        }
    });
};

const parseString = async (list,type, distinct)=>{
    return new Promise((hasil)=>{
        try {            
            //list = list.substr(0,list.length-2);
            let arrLabel = list.split(',');
            let arrField = {
                operator_list : FieldOperators,
                subtask_list : ['subtask_id','subtask_title','subtask_status_id'],
                checklist_item:["id","title","is_checked","task_id","sort"],
                logged:["hours","start_time","end_time","task_id","project_id","note","status","user_id"],
                priority_list:["priority_id","priority_title","priority_icon","priority_color"], 
                status_list: ["status_id","status_title","status_key_name","status_color"],
                task_member_list :['user_id','user_name','user_avatar','project_id'],
                project_members_list :['user_id','user_name','user_avatar'],
                project_list:['project_id','project_title'],
                milestone_byProject_list: ["milestone_id", "milestone_title", "milestone_project_id"],
                byProject_members_list :['user_id','user_name','user_project_id'],
                tasks_list: ['task_id','task_title','project_id'],
                labels_list: ['label_id','label_title','label_color'],
                collaborator_list: ['user_id','user_name','user_avatar','project_id'],
                milestone_list: ["milestone_id", "milestone_title", "milestone_deadline", "milestone_status",'project_id'],
                milestone_list2: ["milestone_id", "milestone_title", 'project_id'],
                blocked_list:["id","title","created_date"],
                blocking_list:["id","title","created_date"],
                comment_like_list: ['like_id',"comment_id","task_id","created_by_user","created_by_username","created_by_useravatar",'created_at'],
                comment_pin_list: ["pin_id","comment_id","task_id","created_by_user","created_by_username","created_by_useravatar",'created_at'],
                events_list: ['id','task_id','project_id','ticket_id','lead_id','type','title','description','start_date','end_date','start_tme','end_time','reminder_status','recurring','repeat_type','repeat_every'],
            };
            let splitLabel = null;
            let label = [];
            
            if(arrLabel.length){
    
                splitLabel = arrField[type];
                
                for(let i=0; i < arrLabel.length; i++){
                    
                    arrLabel[i] = arrLabel[i].split('|');
                    let labelObject = {};
                    
                    for(let j=0; j < arrLabel[i].length; j++){
                        if(!isNaN(Number(arrLabel[i][j])) && arrLabel[i][j] != ''){
                            labelObject[splitLabel[j]] = Number(arrLabel[i][j]);
                        }else{
                            labelObject[splitLabel[j]] = arrLabel[i][j] === '' ? null : arrLabel[i][j];
                        }
                    }
                    
                    if(distinct){
                        let idx = []; 
                        if(type == 'byProject_members_list'){
                            idx = label.filter((list)=> (list.user_id == labelObject.user_id && list.user_project_id == labelObject.project_id));
                            if(idx.length == 0) label.push(labelObject);
                        }else if(type == 'milestone_byProject_list'){
                            idx = label.filter((list)=> (list.milestone_id == labelObject.milestone_id && list.milestone_project_id == labelObject.milestone_project_id));
                            if(idx.length == 0) label.push(labelObject);
                        }else if(type == 'project_list'){
                            idx = label.filter((list)=> (list.project_id == labelObject.project_id));
                            if(idx.length == 0) label.push(labelObject);
                        }
                    }else{
                        label.push(labelObject);
                    }
                }
            }
            return hasil(label);
        } catch (error) {
            //console.log(error,list)
            return hasil([]);
        }
    });
}

const makeQuickFilter = async(filter, user)=>{
    return new Promise((hasil)=>{
        let response = '';
        try {  
            let recently_updated_last_time = new Date(); // data last login user
            let query = '';
            let mention_string = ":"+user.name+"]";

            if(filter == "recently_meaning") {
                response = query;
            }else{

                let project_comments_table_query = `
                    SELECT crm_project_comments.task_id 
                    FROM crm_project_comments 
                    WHERE crm_project_comments.deleted=0 AND crm_project_comments.task_id!=0
                `;
                let project_comments_table_group_by = "GROUP BY crm_project_comments.task_id";
    
                if (filter === "recently_updated") {
                    query = ` AND (crm_tasks.status_changed_at IS NOT NULL AND crm_tasks.status_changed_at>='${recently_updated_last_time}')`;
                }else if (filter === "recently_commented") {
                    query = ` AND crm_tasks.id IN(${project_comments_table_query} AND crm_project_comments.created_at>='${recently_updated_last_time}' ${project_comments_table_group_by})`;
                }else if (filter === "mentioned_me" && recently_updated_last_time) {
                    query = ` AND crm_tasks.id IN(${project_comments_table_query} AND crm_project_comments.description LIKE '%${mention_string}%' ${project_comments_table_group_by})`;
                }else if (filter === "recently_mentioned_me" && recently_updated_last_time) {
                    query = ` AND crm_tasks.id IN(${project_comments_table_query} AND crm_project_comments.description LIKE '%${mention_string}%' AND crm_project_comments.created_at>='${recently_updated_last_time}' ${project_comments_table_group_by})`;
                } else if (filter === "recurring_tasks") {
                    query = ` AND (crm_tasks.recurring=1)`;
                } else {
                    query = ` AND (crm_tasks.status_changed_at IS NOT NULL AND crm_tasks.status_changed_at>='${recently_updated_last_time}' AND crm_tasks.status_id=${filter})`;
                }
                response = query;
            }

            
        } catch (error) {
            console.log({ error, file:'crmTask.js', scope:'makeQuickFilter'});
        }
        return hasil(response);
    })
}

const filterTask = async(filter,user)=>{
    return new Promise(async(hasil)=>{
        let where ='';
        let leftJoin = '';
        try {
            if (filter.project_id) {
                where += ` AND crm_tasks.project_id='${filter.project_id}' `;
            }
            if (filter.client_id) {
                where += ` AND crm_tasks.client_id='${filter.client_id}' `;
            }
            if (filter.parent_task_id) {
                where += ` AND crm_tasks.parent_task_id='${filter.parent_task_id}' `;
            }
            if (filter.priority_id) {
                where += ` AND crm_tasks.priority_id='${filter.priority_id}' `;
            }
            if (filter.status_id) {
                where += ` AND FIND_IN_SET(crm_tasks.status_id,'${filter.status_id.toString()}') `;
            }
            if (filter.task_id) {
                where += ` AND crm_tasks.id='${filter.task_id}' `;
            }
            if (filter.operator_id) {
                where += ` AND crm_operators.id='${filter.operator_id}' `;
            }
            if (filter.assigned_to) {
                where += ` AND crm_tasks.assigned_to='${filter.assigned_to}' `;
            }
            if (filter.specific_user_id) {
                where += ` AND (crm_tasks.assigned_to='${filter.specific_user_id}' OR FIND_IN_SET('${filter.specific_user_id}', crm_tasks.collaborators)) `;
            }
            if (filter.project_status) {
                where += ` /*AND FIND_IN_SET(crm_projects.status,'${filter.project_status}')*/ `;
            }
            if (filter.milestone_id) {
                where += ` AND crm_tasks.milestone_id='${filter.milestone_id}' `;
            }
            if (filter.task_status_id) {
                where += ` AND crm_tasks.status_id='${filter.task_status_id}' `;
            }
            if (filter.start_date && filter.deadline) {
                if(filter.for_events){
                    if(filter.deadline_for_event && filter.start_date_for_event){
                        where += ` AND ( 
                            ( crm_tasks.deadline IS NOT NULL AND crm_tasks.deadline BETWEEN '${filter.start_date}' AND '${filter.deadline}' ) 
                            OR ( crm_milestones.due_date BETWEEN '${filter.start_date}' AND '${filter.deadline}' ) 
                            OR ( crm_tasks.start_date IS NOT NULL AND  crm_tasks.start_date BETWEEN '${filter.start_date}' AND '${filter.deadline}' )
                        )`;
                    }else if(filter.start_date_for_event){
                        where += ` AND (crm_tasks.start_date IS NOT NULL AND crm_tasks.start_date BETWEEN '${filter.start_date}' AND '${filter.deadline}' ) `;
                    }else if(filter.deadline_for_event){
                        where += ` AND (
                                (crm_tasks.deadline IS NOT NULL AND crm_tasks.deadline BETWEEN '${filter.start_date}' AND '${filter.deadline}' ) 
                                OR ( crm_milestones.due_date BETWEEN '${filter.start_date}' AND '${filter.deadline}' ) 
                                )`;
                    }
                }else{
                    where += ` AND (crm_tasks.deadline IS NOT NULL AND crm_tasks.deadline BETWEEN '${filter.start_date}' AND '${filter.deadline}' ) `;
                }
            } else if(typeof filter.deadline === 'string'){
                
                if(filter.deadline.toLowerCase() === 'expired'){
                    where += ` AND IF(crm_tasks.deadline IS NULL, crm_milestones.due_date<'${today}', crm_tasks.deadline<'${today}') `;
                }else if (filter.deadline == today) { //today
                    where +=  ` AND IF(crm_tasks.deadline IS NULL, crm_milestones.due_date='${filter.deadline}', crm_tasks.deadline='${filter.deadline}') `;
                }else { //future deadlines, extract today's
                    let addDays = moment(today).add(1,'days').format("YYYY-MM-DD");
                    where +=  ` AND IF(crm_tasks.deadline IS NULL, crm_milestones.due_date BETWEEN '${addDays}' AND '${filter.deadline}', crm_tasks.deadline BETWEEN '${addDays}' AND '${filter.deadline}') `;
                }
            }
            if (filter.tickets_id) {
                where += ` AND crm_tasks.tickets_id='${filter.tickets_id}' `;
            }
            if (filter.project_member_id) {
                // where += ` AND crm_project_members.user_id='${filter.project_member_id}' `;
                // leftJoin +=  ` LEFT JOIN crm_project_members ON crm_tasks.project_id= crm_project_members.project_id AND crm_project_members.deleted=0 AND crm_project_members.user_id='${filter.project_member_id}' `;
            }
            if (filter.quick_filter) {
                where += await makeQuickFilter(filter.quick_filter,user);
            }
        } catch (error) {
            console.log({error, file:'crmTask', scope:'filterTask'});
        }

        return hasil({ where, leftJoin });
    })

}

exports.getTaskComments = async (field, params) => {
    let response = { error:false, result:[] }
    try {
        // GET comments, pinComment,likes
        const query = `
            SELECT
            cpc.id, cpc.description, cpc.files, cpc.comment_id, cpc.created_at, cpc.task_id, cpc.project_id,cpc.deleted,
            cpc.created_by , u1.name as 'created_user_name', u1.avatar as 'created_user_avatar',
            (
                SELECT GROUP_CONCAT(cl.id,'|',cpc.id,'|',cpc.task_id,'|',cl.created_by,'|',u2.name,'|',IFNULL(u2.avatar,''),'|',cl.created_at) 
                FROM crm_likes cl
                LEFT JOIN user u2 on u2.user_id = cl.created_by
                WHERE cl.project_comment_id = cpc.id and cl.deleted=0
            ) as likes_list,
            (
                SELECT GROUP_CONCAT(pin.id,'|',cpc.id,'|',cpc.task_id,'|',pin.pinned_by,'|',u3.name,'|',IFNULL(u3.avatar,''),'|',pin.created_at) 
                FROM crm_pin_comments pin
                LEFT JOIN user u3 on u3.user_id = pin.pinned_by
                WHERE pin.project_comment_id = cpc.id and pin.deleted=0
            ) as pins_list

            FROM crm_project_comments cpc
            LEFT JOIN user u1 on u1.user_id = cpc.created_by 
            WHERE 
            cpc.deleted=0 and 
            cpc.task_id='${params.idTask}'
            ORDER BY cpc.created_at DESC;
        `;
        let result = await db.query(query);
        let pin = [];
        if(result.length){

            let parent = result.filter(v => v.comment_id === 0);
            let child = result.filter(v => v.comment_id !== 0);

            for(let i=0; i < parent.length; i++){
                let list = parent[i];
                let objects = {
                    id: list.id,
                    description: list.description,
                    comment_id: list.comment_id,
                    project_id: list.project_id,
                    sort: i,
                    files: list.files,
                    created_by:{
                        user_id: list.created_by,
                        user_name: list.created_user_name,
                        user_avatar: list.created_user_avatar
                    },
                    replies: child.filter(chld => chld.comment_id === list.id),
                    created_date: moment(list.created_at).format("YYYY-MM-DD hh:mm:ss A"),
                    comment_pin_list:{},
                    comment_like_list:{},
                    comment_like_tooltip:null,
                    comment_unpin:false,
                    comment_unlike:false,
                };
    
                objects.comment_pin_list = await parseString(list.pins_list,'comment_pin_list');
                objects.comment_like_list = await parseString(list.likes_list,'comment_like_list');

                if(objects.comment_pin_list.length){
                    for (let index = 0; index < objects.comment_pin_list.length; index++) {
                        if(objects.comment_pin_list[index].created_by_user === params.user.user_id){
                            objects.comment_unpin = objects.comment_pin_list[index].pin_id;
                        }
                        pin.push(objects.comment_pin_list[index]);
                    }
                }

                let like=[]
                if(objects.comment_like_list.length){
                    for (let index = 0; index < objects.comment_like_list.length; index++) {
                        if(objects.comment_like_list[index].created_by_user === params.user.user_id){
                            objects.comment_unlike = objects.comment_like_list[index].like_id;
                        }
                        like.push(objects.comment_like_list[index].created_by_username);
                    }
                    objects.comment_like_tooltip = like.join(" , ");
                }
                
                parent[i] = objects;
            }

            response.result = { comments:parent, pin_comment_list:pin }
        }

        return response;
    } catch (error) {
        console.log({ error, file:"crmTask", scope:"getTaskComments" });
        response.error = true;
        return response;
    }
};

exports.getTaskActivity = async (field, params) => {
    let response = { error:false, result:[] }
    try {
        // GET activity task / project
        
        const query = `
            SELECT
            cal.id, cal.log_type_id, cal.log_for_id, cal.log_type, cal.log_for, cal.action_title, cal.changes ,
            cal.created_by, u1.name as 'created_name', u1.avatar as 'created_avatar', cal.created_at

            FROM crm_activity_logs cal
            LEFT JOIN user u1 on u1.user_id = cal.created_by 
            WHERE cal.log_type = ? and cal.log_type_id = ? and cal.log_for_id = ? 
            ORDER BY cal.created_at DESC;
        `;

        let result = await db.query(query,[params.log_type, params.log_type_id, params.log_for_id]);
        
        if(result.length){
            for(let i=0; i < result.length; i++){
                let list = result[i];
                if(list.changes && list.changes.length > 1){
                    list.changes = JSON.parse(list.changes);
                }
                list.created_at = moment(list.created_at).format("YYYY-MM-DD hh:mm:ss")
                result[i] = list;
            }
        }
        response.result = result;
        return response;
    } catch (error) {
        console.log({ error, file:"crmTask", scope:"getTaskActivity" });
        response.error = true;
        return response;
    }
};

exports.getAttributeProject = async (field, params) => {
    let response = { error:false, result:[] }
    try {
        // GET activity task / project
        const tables = {
            labels: "crm_labels",
            milestones: "crm_milestones",
            subtasks: 'crm_tasks',
            members_project: "crm_project_members",
        }
        
        const query = `
            SELECT
            ${params.idProject} as id,
            (
                SELECT GROUP_CONCAT(cl.id,'|',cl.title,'|',cl.color)
                FROM crm_labels cl
                WHERE cl.user_id = '${params.user_id}' and cl.deleted=0 and cl.context = 'task'
            ) as labels_list,
            (
                SELECT GROUP_CONCAT(cm.id,'|',cm.title,'|',cm.due_date,'|','active','|',cm.project_id)
                FROM crm_milestones cm
                WHERE cm.project_id = '${params.idProject}' and cm.deleted=0 
            ) as milestone_list,
            (
                SELECT GROUP_CONCAT(ct.id,'|',ct.title,'|',ct.project_id) 
                FROM crm_tasks ct 
                WHERE ct.project_id = '${params.idProject}' and ct.deleted=0 
            ) as tasks_list,
            /*(
                SELECT GROUP_CONCAT(cpm.user_id,'|',u.name,'|',IFNULL(u.avatar,''),'|',cpm.project_id) 
                FROM crm_project_members cpm 
                LEFT JOIN user u on u.user_id = cpm.user_id and lower(u.status) = 'active' 
                WHERE cpm.project_id = '${params.idProject}' and cpm.deleted=0 
            ) as project_members_list,*/
            (
                SELECT GROUP_CONCAT(cts.id,'|',cts.title,'|',cts.key_name,'|',cts.color) 
                FROM crm_task_status cts  
                WHERE cts.deleted=0 
            ) as status_list,
            (
                SELECT GROUP_CONCAT(cpt.id,'|',cpt.title,'|',cpt.icon,'|',cpt.color) 
                FROM crm_task_priority cpt 
                WHERE cpt.deleted=0 
            ) as priority_list,
            (
                SELECT GROUP_CONCAT(cp.id,'|',cp.title)
                FROM crm_projects cp
                -- left join crm_project_members cpm on cpm.project_id = cp.id and cp.deleted=0
                -- left join user u on u.user_id = cpm.user_id and cpm.deleted=0
                -- left join company c on c.company_id = u.company_id
                -- WHERE c.company_id='${params.company_id}'
                WHERE cp.deleted = 0
            ) as project_list,
            (
                SELECT GROUP_CONCAT(cpm.user_id,'|',u.name,'|',IFNULL(u.avatar,''),'|',cpm.project_id) 
                FROM crm_project_members cpm 
                LEFT JOIN user u on u.user_id = cpm.user_id and lower(u.status) = 'active' and cpm.deleted=0
                left join company c on c.company_id = u.company_id
                WHERE c.company_id='${params.company_id}' 
            ) as task_member_list,
            (
                SELECT GROUP_CONCAT(u.user_id,'|',u.name,'|',IFNULL(u.avatar,'')) 
                FROM user u 
                left join company c on c.company_id = u.company_id
                WHERE c.company_id='${params.company_id}' 
            ) as project_members_list
        `;
        console.log(query,'-----------')
        let result = await db.query(query);
        
        if(result.length){

            for(let i=0; i < result.length; i++){
                let list = result[i];
                list.labels_list = await parseString(list.labels_list,'labels_list');
                list.milestone_list = await parseString(list.milestone_list,'milestone_list');
                list.tasks_list = await parseString(list.tasks_list,'tasks_list');
                list.project_members_list = await parseString(list.project_members_list,'project_members_list');
                list.task_member_list = await parseString(list.task_member_list,'task_member_list');
                list.status_list = await parseString(list.status_list,'status_list');
                list.priority_list = await parseString(list.priority_list,'priority_list');
                list.project_list = await parseString(list.project_list,'project_list',true);
                
                // add default
                if(list.milestone_list.length){
                    list.milestone_list.forEach((str)=>{ str.value = str.milestone_id; str.label = str.milestone_title })
                    list.milestone_list.splice(0,0,{ value:0, label:'- Milestone -' })
                }
                if(list.project_list.length){
                    list.project_list.forEach((str)=>{ str.value = str.project_id; str.label = str.project_title })
                    list.project_list.splice(0,0,{ value:0, label:'- Project -' })
                }
                if(list.task_member_list.length){
                    let tmp = [];
                    list.task_member_list.forEach((str)=>{ 
                        let idx = tmp.findIndex((str2)=> str2.user_id === str.user_id)
                        if(idx < 0){
                            str.value = str.user_id; 
                            str.label = str.user_name 
                            tmp.push(str);
                        }
                    });
                    list.task_member_list = tmp;
                    list.task_member_list.splice(0,0,{ value:0, label:'- Team member -' });
                }
                if(list.project_members_list.length){
                    // list.project_members_list = list.project_members_list.filter((v,i,a)=>a.findIndex(v2=>(v2.user_id===v.user_id))===i)
                    let tmp = [];
                    
                    list.project_members_list.forEach((str)=>{ 
                        let idx = tmp.findIndex((str2)=> str2.user_id === str.user_id)
                        if(idx < 0){
                            str.value = str.user_id; 
                            str.label = str.user_name 
                            tmp.push(str);
                        }
                    });
                    list.project_members_list = tmp;
                    list.project_members_list.splice(0,0,{ value:0, label:'- Team member -' });
                }
                if(list.status_list.length){
                    list.status_list.forEach((str)=>{ str.value = str.status_id; str.label = str.status_title })
                    list.status_list.splice(0,0,{ value:0, label:'- Status -' })
                }
                if(list.priority_list.length){
                    list.priority_list.forEach((str)=>{ str.value = str.priority_id; str.label = str.priority_title })
                    list.priority_list.splice(0,0,{ value:0, label:'- Priority -' })
                }
                if(list.labels_list.length){
                    list.labels_list.forEach((str)=>{ str.value = str.label_id; str.label = str.label_title })
                    list.labels_list.splice(0,0,{ value:0, label:'- Labels -' })
                }

                list.points = [
                    { value: 0, label: '- Points -' },
                    { value: 1, label: '1 Point' },
                    { value: 2, label: '2 Point' },
                    { value: 3, label: '3 Point' },
                    { value: 4, label: '4 Point' },
                    { value: 5, label: '5 Point' },
                  ];
                list.checklist_item=[];
                
                result[i] = list;
            }
        }
        response.result = result;
        return response;
    } catch (error) {
        console.log({ error, file:"crmTask", scope:"getAttributeProject" });
        response.error = true;
        return response;
    }
};

exports.getTask = async (field, params) => {
    let response = { error: false, result:[] }
    try {

        let filter = {}
        if(params.query.filter){
            filter = await filterTask(params.query.filter,params.user);
        }
        const query = `
            SELECT 
            crm_operators.id as operator_id,
            crm_operators.title as operator_name,
            crm_tasks.*, 
            crm_task_status.key_name AS status_key_name, 
            crm_task_status.title AS status_title, 
            crm_task_status.color AS status_color, 
            user.name AS assigned_to_user, 
            user.avatar as assigned_to_avatar,  
            crm_projects.title AS project_title, 
            crm_milestones.title AS milestone_title, 
            IF(crm_tasks.deadline IS NULL, crm_milestones.due_date,crm_tasks.deadline) AS deadline,
            crm_tickets.title AS ticket_title, 
            (
                SELECT GROUP_CONCAT(user.user_id, '|', user.name, '|' , IFNULL(user.avatar,'')) 
                FROM user 
                WHERE  
                FIND_IN_SET(user.user_id, crm_tasks.collaborators)
            ) AS collaborator_list, 
            (
                SELECT GROUP_CONCAT(IFNULL(cpt.hours,0),'|',IFNULL(cpt.start_time,''),'|',IFNULL(cpt.end_time,''),'|',IFNULL(cpt.task_id,''),'|',IFNULL(cpt.project_id,''),'|',IFNULL(cpt.note,''),'|',IFNULL(cpt.status,''),'|',IFNULL(cpt.user_id,''))  
                FROM crm_project_time cpt
                WHERE cpt.task_id = crm_tasks.id order by id DESC limit 1
            ) AS logged, 
            (
                SELECT GROUP_CONCAT(IFNULL(cci.id,''),'|',IFNULL(cci.title,''),'|',IFNULL(cci.is_checked,''),'|',IFNULL(cci.task_id,''),'|',IFNULL(cci.sort,''))  
                FROM crm_checklist_items cci
                WHERE cci.task_id = crm_tasks.id and cci.deleted = 0 order by sort ASC
            ) AS checklist_item,
            (
                SELECT GROUP_CONCAT(ct.id, '|', ct.title, '|' , ct.created_date) 
                FROM crm_tasks ct 
                WHERE  
                FIND_IN_SET(ct.id, crm_tasks.blocking)
            ) AS blocking_list, 
            (
                SELECT GROUP_CONCAT(ct2.id, '|', ct2.title, '|' , ct2.status_id) 
                FROM crm_tasks ct2 
                WHERE ct2.parent_task_id = crm_tasks.id order by ct2.sort ASC
            ) AS subtask_list, 
            (
                SELECT GROUP_CONCAT(ct.id, '|', ct.title, '|' , ct.created_date) 
                FROM crm_tasks ct 
                WHERE  
                FIND_IN_SET(ct.id, crm_tasks.blocked_by)
            ) AS blocked_by_list, 
            (
                SELECT GROUP_CONCAT(ce.id,'|',ce.task_id,'|',ce.project_id,'|',ce.ticket_id,'|',
                ce.lead_id,'|',ce.type,'|',ce.title,'|',ce.description,'|',
                IFNULL(ce.start_date,''),'|',IFNULL(ce.end_date,''),'|',IFNULL(ce.start_time,''),'|',IFNULL(ce.end_time,''),'|',
                ce.reminder_status,'|',ce.recurring,'|',ce.repeat_type,'|',ce.repeat_every) 
                FROM crm_events ce 
                WHERE ce.task_id = crm_tasks.id
            ) AS events_list, 
            crm_task_priority.title AS priority_title, 
            crm_task_priority.icon AS priority_icon, 
            crm_task_priority.color AS priority_color, 
            IF(crm_tasks.deadline IS NULL, crm_milestones.title, '') AS deadline_milestone_title,
            notification_table.task_id AS unread, 
            (
                SELECT GROUP_CONCAT(crm_labels.id, '|', crm_labels.title, '|', crm_labels.color) 
                FROM crm_labels 
                WHERE FIND_IN_SET(crm_labels.id, crm_tasks.labels)
            ) AS labels_list 
                
            FROM crm_tasks 
            LEFT JOIN crm_operators ON crm_operators.id = crm_tasks.operator_id 
            LEFT JOIN user ON user.user_id= crm_tasks.assigned_to 
            LEFT JOIN crm_projects ON crm_tasks.project_id=crm_projects.id 
            LEFT JOIN crm_milestones ON crm_tasks.milestone_id=crm_milestones.id 
            LEFT JOIN crm_task_status ON crm_tasks.status_id = crm_task_status.id 
            LEFT JOIN crm_task_priority ON crm_tasks.priority_id = crm_task_priority.id 
            LEFT JOIN crm_tickets ON crm_tasks.ticket_id = crm_tickets.id 
            LEFT JOIN (
                SELECT crm_notifications.task_id 
                FROM crm_notifications 
                WHERE crm_notifications.deleted=0 
                AND crm_notifications.event='project_task_commented' 
                AND !FIND_IN_SET('${params.user.user_id}', crm_notifications.read_by) 
                AND crm_notifications.user_id!=1 
                GROUP BY crm_notifications.task_id
                ) AS notification_table ON notification_table.task_id = crm_tasks.id 
            ${ filter.leftJoin }
                    
            WHERE crm_tasks.deleted=0 

            ${params.id ? ` AND crm_tasks.id = '${params.id}' `:''} 
            ${ params.query.dashboard ? ` AND (crm_tasks.assigned_to='${params.user.user_id}' OR FIND_IN_SET('${params.user.user_id}',crm_tasks.collaborators)) `: '' }
            ${params.user.level == 'client' ? ` AND ( crm_tasks.assigned_to=1 OR FIND_IN_SET('${params.user.user_id}', crm_tasks.collaborators)) `:''} 
            ${ params.query.filter.task_status ? '' : ` AND FIND_IN_SET(crm_tasks.status_id,'${params.query.filter.status_id ? params.query.filter.status_id.toString() : '1,2,3'}') ` }
            ${ params.query.filter.project_status ? '' : `-- AND FIND_IN_SET(crm_projects.status,'open') `} 
            ${ filter.where }

            ORDER BY crm_tasks.id DESC;
        `;
        // ${field.level == 'client' ? ` AND ( crm_tasks.assigned_to=1 OR FIND_IN_SET('${field.user_id}', crm_tasks.collaborators)) `:' '} 
        
        let result = await db.query(query);
        let resultStatus = [];
        let arrStatus = [];
        let countStatus = 0;
        
        if(!params.id){
            resultStatus = await db.query('select id,title,key_name,color,sort from crm_task_status where deleted=0 and hide_from_kanban=0 order by sort ASC;');
            arrStatus = []; // for key {}
            resultStatus.forEach(el => {
                // sort by array
                let exist = arrStatus.findIndex((str)=> str.id == el.id);
                if(exist < 0){
                    arrStatus.push({ ...el, list:[]})
                }
    
                // sort by key
                //if(!arrStatus[el.status_key_name]){ arrStatus[el.status_key_name] = { ...el,list:[] } }
            });
        }

        if(result.length){
            countStatus = resultStatus.length;

            for(let i=0; i < result.length; i++){
                let list = result[i];
                countStatus++;
                let objects = {
                    idForReactDnD: countStatus.toString(),
                    id: list.id.toString(),
                    title: list.title,
                    description: list.description,
                    project_id: list.project_id,
                    project_title: list.project_title,
                    milestone_id: list.milestone_id,
                    milestone_title: list.milestone_title,
                    tickets_id : list.tickets_id,
                    tickets_title : list.tickets_title,
                    points: list.points,
                    start_date: (list.start_date && list.start_date != '')  ? moment(list.start_date).format("YYYY-MM-DD") : null,
                    deadline: (list.deadline && list.deadline != '') ? moment(list.deadline).format("YYYY-MM-DD") : null,
                    logged:[],
                    sort: list.sort,
                    reminder_list:[],
                    labels_list:[],
                    priority:{},
                    collaborator_list:[],
                    dependency:{
                        blocking:[],
                        blocked_by:[]
                    },
                    checklist_item:[],
                    subtask_list:[],
                    events_list:[],
                    status:{},
                    assigned_to:{},
                    // petronas
                    operator_id: list.operator_id,
                    operator_name : list.operator_name,
                   
                    operation_status: list.operation_status,
                    last_hour_operator: list.last_hour_operator,
                    activity_status: list.activity_status,
                    
                    drilling_gross : list.drilling_gross,
                    drilling_day : list.drilling_day,
                    cost : list.cost,
                    npt : list.npt,
                    npt_hrs : list.npt_hrs,
                    target_start : list.target_start,
                    target_end : list.target_end,
                    next_target : list.next_target,
                    predrill_gross : list.predrill_gross,
                    predrill_net : list.predrill_net,
                    predrill_porosity : list.predrill_porosity,
                    predrill_saturation : list.predrill_saturation,
                    predrill_hc : list.predrill_hc,
                    postdrill_gross : list.postdrill_gross,
                    postdrill_net : list.postdrill_net,
                    postdrill_saturation : list.postdrill_saturation,
                    postdrill_porosity : list.postdrill_porosity,
                    postdrill_hc:list.postdrill_hc,
                    
                    well_type_condition : list.well_type_condition,
                    achieved_target : list.achieved_target,
                    hole_depth : list.hole_depth,

                    created_date: moment(list.created_date).format("YYYY-MM-DD hh:mm:ss"),
                };
    
                objects.labels_list = await parseString(list.labels_list,'labels_list');
                objects.collaborator_list = await parseString(list.collaborator_list,'collaborator_list');
                objects.dependency.blocked_by = await parseString(list.blocked_by_list,'blocked_list');
                objects.dependency.blocking = await parseString(list.blocking_list,'blocking_list');
                objects.events_list = await parseString(list.events_list,'events_list');
                objects.logged = await parseString(list.logged,'logged');
                objects.logged = objects.logged.length ? objects.logged[0] : [];
                objects.subtask_list = await parseString(list.subtask_list,'subtask_list');
                objects.checklist_item = await parseString(list.checklist_item,'checklist_item');
                
                objects.status = {
                    status_id : list.status_id,
                    status_title: list.status_title,
                    status_key_name: list.status_key_name,
                    status_color: list.status_color
                }
                objects.priority = {
                    priority_id: list.priority_id,
                    priority_title: list.priority_title,
                    priority_icon: list.priority_icon,
                    priority_color: list.priority_color
                }

                objects.assigned_to = {
                    assigned_to_user_id : list.assigned_to,
                    assigned_to_user_name : list.assigned_to_user,
                    assigned_to_user_avatar : list.assigned_to_avatar
                }

                if(!params.id){
                    // sort by index
                    let exist = arrStatus.findIndex((str)=> str.id == objects.status.status_id);
                    if(exist > -1){
                        arrStatus[exist].list.push(objects)
                    }
                    // sort by key
                    // if(arrStatus[objects.status.status_key_name]){
                    //     arrStatus[objects.status.status_key_name].list.push(objects)
                    // }
                }

                result[i] = objects;
            }
        }

        if(params.query.type === 'kanban' && !params.id){
            result = arrStatus;
            result.forEach((list, i)=>{
                //let name = Object.keys(list);
                list = list.list.sort((a,b)=> a.sort-b.sort); 
            });
        }

        response.result = result;
    } catch (error) {
        console.log({ error, file:"crmTask", scope:"getTask" });
        response.error = error;
    }

    return response;
};

exports.getTaskList = async (field, params) => {
    let response = { error: false, result:[] }
    try {

        let filter = {}
        if(params.query.filter){
            filter = await filterTask(params.query.filter,params.user);
           // console.log("masuk",filter);
        }
        const query = `
            SELECT 
            crm_tasks.id,
            crm_tasks.title
                
            FROM crm_tasks 
            LEFT JOIN user ON user.user_id= crm_tasks.assigned_to 
            LEFT JOIN crm_projects ON crm_tasks.project_id=crm_projects.id 
            LEFT JOIN crm_milestones ON crm_tasks.milestone_id=crm_milestones.id 
            LEFT JOIN crm_task_status ON crm_tasks.status_id = crm_task_status.id 
            LEFT JOIN crm_task_priority ON crm_tasks.priority_id = crm_task_priority.id 
            LEFT JOIN crm_tickets ON crm_tasks.ticket_id = crm_tickets.id 
            LEFT JOIN (
                SELECT crm_notifications.task_id 
                FROM crm_notifications 
                WHERE crm_notifications.deleted=0 
                AND crm_notifications.event='project_task_commented' 
                AND !FIND_IN_SET('${params.user.user_id}', crm_notifications.read_by) 
                AND crm_notifications.user_id!=1 
                GROUP BY crm_notifications.task_id
                ) AS notification_table ON notification_table.task_id = crm_tasks.id 
            ${ filter.leftJoin }
                    
            WHERE crm_tasks.deleted=0 

            ${params.id ? ` AND crm_tasks.id = '${params.id}' `:''} 
            ${ params.query.dashboard ? ` AND (crm_tasks.assigned_to='${params.user.user_id}' OR FIND_IN_SET('${params.user.user_id}',crm_tasks.collaborators)) `: '' }
            ${params.user.level == 'client' ? ` AND ( crm_tasks.assigned_to=1 OR FIND_IN_SET('${params.user.user_id}', crm_tasks.collaborators)) `:''} 
            ${ params.query.filter.task_status ? '' : ` AND FIND_IN_SET(crm_tasks.status_id,'${params.query.filter.status_id ? params.query.filter.status_id.toString() : '1,2,3'}') ` }
            ${ params.query.filter.project_status ? '' : `AND FIND_IN_SET(crm_projects.status,'open') `} 
            ${ filter.where }

            ORDER BY crm_tasks.id DESC;
        `;
        // ${field.level == 'client' ? ` AND ( crm_tasks.assigned_to=1 OR FIND_IN_SET('${field.user_id}', crm_tasks.collaborators)) `:' '} 
        
        let result = await db.query(query);
        let resultStatus = [];
        let arrStatus = [];
        let countStatus = 0;
        
        if(!params.id){
            resultStatus = await db.query('select id,title,key_name,color,sort from crm_task_status where deleted=0 and hide_from_kanban=0 order by sort ASC;');
            arrStatus = []; // for key {}
            resultStatus.forEach(el => {
                // sort by array
                let exist = arrStatus.findIndex((str)=> str.id == el.id);
                if(exist < 0){
                    arrStatus.push({ ...el, list:[]})
                }
    
                // sort by key
                //if(!arrStatus[el.status_key_name]){ arrStatus[el.status_key_name] = { ...el,list:[] } }
            });
        }

        if(result.length){
            countStatus = resultStatus.length;

            for(let i=0; i < result.length; i++){
                let list = result[i];
                countStatus++;
                let objects = {
                    id: list.id.toString(),
                    title: list.title,
                    value:list.id,
                    label:list.title
                };

                result[i] = objects;
            }
        }

        response.result = result;
    } catch (error) {
        console.log({ error, file:"crmTask", scope:"getTask" });
        response.error = error;
    }

    return response;
};

exports.getTaskFilterData = async (field, params) => {
    let response = { error: false, result:[] }
    try {
        let milestones = `
            SELECT GROUP_CONCAT(cm.id,'|',cm.title,'|',cm.project_id)
            FROM crm_milestones cm
            left join crm_project_members cpm on cpm.project_id = cm.project_id and cm.deleted=0
            left join user u on u.user_id = cpm.user_id and cpm.deleted=0
            left join company c on c.company_id = u.company_id
            WHERE c.company_id='${field.company_id}' 
        `;
        let members = `
            SELECT GROUP_CONCAT(cpm.user_id,'|',u.name,'|',cpm.project_id) 
            FROM crm_project_members cpm 
            LEFT JOIN user u on u.user_id = cpm.user_id and lower(u.status) = 'active' and cpm.deleted=0
            left join company c on c.company_id = u.company_id
            WHERE c.company_id='${field.company_id}' 
        `;
        if(params){
            if(params.idProject && params.idProject !== 'null'){
                milestones = `
                    SELECT GROUP_CONCAT(cm.id,'|',cm.title,'|',cm.project_id)
                    FROM crm_milestones cm
                    WHERE cm.project_id='${params.idProject}' 
                `;
                members += ` AND cpm.project_id='${params.idProject}' `;
            }
        }
        const query = `
            SELECT 
            (
                SELECT GROUP_CONCAT(cl.id,'|',cl.title,'|',cl.color)
                FROM crm_labels cl
                WHERE cl.user_id = '${field.user_id}' and cl.deleted=0 and cl.context = 'task'
            ) as labels_list,
            (
                ${milestones}
            ) as milestone_byProject_list,
            (
                SELECT GROUP_CONCAT(cp.id,'|',cp.title)
                FROM crm_projects cp
                -- left join crm_project_members cpm on cpm.project_id = cp.id and cp.deleted=0
                -- left join user u on u.user_id = cpm.user_id and cpm.deleted=0
                -- left join company c on c.company_id = u.company_id
                -- WHERE c.company_id='${field.company_id}'
                WHERE cp.deleted = 0
            ) as project_list,
            (
                ${ members }
            ) as byProject_members_list,
            (
                SELECT GROUP_CONCAT(cts.id,'|',cts.title,'|',cts.key_name,'|',cts.color) 
                FROM crm_task_status cts  
                WHERE cts.deleted=0 
            ) as status_list,
            (
                SELECT GROUP_CONCAT(cpt.id,'|',cpt.title,'|',cpt.icon,'|',cpt.color) 
                FROM crm_task_priority cpt 
                WHERE cpt.deleted=0 
            ) as priority_list 
            FROM DUAL;
        `;

        let result = await db.query(query);
        
        if(result.length){
            for(let i=0; i < result.length; i++){
                let list = result[i];
                let objects = {
                    milestone_byProject_list:[],
                    project_list:[],
                    byProject_members_list:[],
                    status_list:[],
                    priority_list:[],
                    labels_list:[]
                }
    
                objects.milestone_byProject_list = await parseString(list.milestone_byProject_list,'milestone_byProject_list',1);
                objects.project_list = await parseString(list.project_list,'project_list',1);
                objects.byProject_members_list = await parseString(list.byProject_members_list,'byProject_members_list',1);
                objects.status_list = await parseString(list.status_list,'status_list');
                objects.priority_list = await parseString(list.priority_list,'priority_list');
                objects.labels_list = await parseString(list.labels_list,'labels_list');

                // add value and label
                if(objects.milestone_byProject_list.length){
                    objects.milestone_byProject_list.forEach((str)=>{ str.value = str.milestone_id; str.label = str.milestone_title })
                    objects.milestone_byProject_list.splice(0,0,{ value:0, label:'- Milestone -' })
                }
                if(objects.project_list.length){
                    objects.project_list.forEach((str)=>{ str.value = str.project_id; str.label = str.project_title })
                    objects.project_list.splice(0,0,{ value:0, label:'- Project -' })
                }
                if(objects.byProject_members_list.length){
                    let tmp = [{ value:0, label:'- Team member -' }];
                    objects.byProject_members_list.forEach((str)=>{ 
                        let idx = tmp.findIndex((check)=> check.value === str.user_id);
                        str.value = str.user_id; 
                        str.label = str.user_name 
                        if(idx == -1){
                            tmp.push(str);
                        }
                    })
                    objects.byProject_members_list = tmp;
                }
                if(objects.status_list.length){
                    objects.status_list.forEach((str)=>{ str.value = str.status_id; str.label = str.status_title })
                    objects.status_list.splice(0,0,{ value:0, label:'- Status -' })
                }
                if(objects.priority_list.length){
                    objects.priority_list.forEach((str)=>{ str.value = str.priority_id; str.label = str.priority_title })
                    objects.priority_list.splice(0,0,{ value:0, label:'- Priority -' })
                }
                if(objects.labels_list.length){
                    objects.labels_list.forEach((str)=>{ str.value = str.label_id; str.label = str.label_title })
                    objects.labels_list.splice(0,0,{ value:0, label:'- Labels -' })
                }
                result[i] = objects;
            }
        }

        response.result = result;
    } catch (error) {
        console.log({ error, file:"crmTask", scope:"getTask" });
        response.error = error;
    }

    return response;
};

exports.storeTask = async (field, params) => {
    let response = { error: false, result: [] }
    try {
        let result = null;
        let arrField = sortField;
        let tmpData = [];
        let tmpField = [];
        for(let i=0; i < arrField.length; i++){
            try {
                if(field[arrField[i]] !== undefined){

                    if(["milestone_id","labels","collaborators"].indexOf(arrField[i]) > -1 && field.project_id){
                            let dataRec = field[arrField[i]].length ? field[arrField[i]].toString():null;
                            tmpField.push(`\`${arrField[i]}\``);
                            tmpData.push(dataRec);
                    }else{
                        tmpField.push(`\`${arrField[i]}\``);
                        tmpData.push(field[arrField[i]]);
                    }
                    
                }
            } catch (error) {}
        }

        const query = `
            INSERT INTO crm_tasks(${tmpField.toString()})
            value(?);
        `;
        result = await db.query(query,[tmpData]);
        if(result.insertId){

            response.result = result.insertId;
            if(!params.subtasks){
                field.id = null;
                field.changes = null;
                field.action_title = 'Added';
                field.action = 'created';
                field.log_type = 'task';
                field.log_type_id = result.insertId;
                field.log_for = 'project';
                field.log_for_id = field.project_id || 0;
        
                let log = await addActivityTask(field,null);
                if(log.error){
                    response = log
                    db.query('DELETE FROM crm_tasks where id=?',[result.insertId]);
                    //return { error: true, result: log.error };
                }
            }
        }else{
            response.error = true;
        }
    } catch (error) {
        console.log({ error, file:'crmTask',scope:'storeTask' });
        response.error = error;
    }
    return response;
};

exports.editTask = async (field, params) => {
    let response = { error: false, result:[] };
    try {
        let arrField = sortField;
        let tmpData = [];
        for(let i=0; i < arrField.length; i++){
            try {                
                if(field[arrField[i]] !== undefined){
                    if(["milestone_id","labels","colaborators"].indexOf(arrField[i]) > -1 && field.project_id){
                        if(field[arrField[i]]){
                            tmpData.push(`${arrField[i]}="${field[arrField[i]].toString()}"`);
                        }else{
                            tmpData.push(`${arrField[i]}=null`);
                        }
                    }
                    else{
                        if(field[arrField[i]]){
                            tmpData.push(`${arrField[i]}="${field[arrField[i]]}"`);
                        }else{
                            tmpData.push(`${arrField[i]}=null`);
                        }
                    }
                } 
            } catch (error) {
                
            }
        }

        field.id = field.id;
        field.changes = null;
        field.action_title = 'Updated';
        field.action = 'updated';
        field.log_type = 'task';
        field.log_type_id = field.id;
        field.log_for = 'project';
        field.log_for_id = field.project_id || 0;

        //response.result = field;
        response = await addActivityTask(field,null);
        const query = `
            UPDATE crm_tasks SET ${tmpData.toString()} where id=? ;
        `;
        const result = db.query(query,[field.id]);
        response.result = field.id;

    } catch (error) {
        console.log(error);
        response.error = true;
        response.result = error;
    }
    return response;
};

// edit for label,milestone,project,colloaborators
exports.editTaskInfo = async (field, params) => {
    try {
        let arrField = sortField;
        let tmpData = [];
        for(let i=0; i < arrField.length; i++){
            if(field[arrField[i]] !== undefined){
                tmpData.push(`${arrField[i]}="${field[arrField[i]]}"`);
            } 
        }

        const query = `
            UPDATE crm_tasks ${arrField.toString()} where id=? ;
        `;
        const result = db.query(query,[params.id]);

        field.id = params.id;
        field.changes = null;
        field.action_title = 'Updated';
        field.action = 'updated';
        field.log_type = 'task';
        field.log_type_id = params.id;
        field.log_for = 'project';

        await addActivityTask(field,null);

        return result;
    } catch (error) {
        throw error;
    }
};

exports.storeTaskReminder = async (field, params) => {
    let response = { error: false, result:[] };
    try {
        let result = null;
        let arrField = fieldReminder;
        let tmpData = [];
        let tmpField = [];
        for(let i=0; i < arrField.length; i++){
            if(field[arrField[i]] !== undefined){
                tmpField.push(`\`${arrField[i]}\``);
                tmpData.push(field[arrField[i]]);
            } 
        }

        const query = `
            INSERT INTO crm_events(${tmpField.toString()})
            value(?);
        `;
        result = await db.query(query,[tmpData]);
        response.result = 'success';
    } catch (error) {
        response.result = [];
        response.error = error;
        console.log(error)
    }
    return response;
};

exports.storeTaskComment = async (field, params) => {
    let response = { error: false, result:[] };
    try {
        let result = null;
        let arrField = fieldComment;
        let tmpData = [];
        let tmpField = [];
        for(let i=0; i < arrField.length; i++){
            if(field[arrField[i]] !== undefined){
                tmpField.push(`\`${arrField[i]}\``);
                tmpData.push(field[arrField[i]]);
            } 
        }

        const query = `
            INSERT INTO crm_project_comments(${tmpField.toString()})
            value(?);
        `;
        result = await db.query(query,[tmpData]);
        response.result = 'success';
    } catch (error) {
        response.result = [];
        response.error = error;
        console.log(error)
    }
    return response;
};

exports.storeTaskChecklistItem = async (field, params) => {
    let response = { error: false, result:[] };
    try {
        let result = null;
        let arrField = FieldChecklistItem;
        let tmpData = [];
        let tmpField = [];

        const deleted = await db.query(`DELETE from crm_checklist_items where task_id='${params.id}'`);
        for(let i=0; i < field.length; i++){
            let data = [];
            let value = Object.values(field[i]);
            for(let j=0; j < value.length; j++){
                data.push(`'${value[j]}'`)   
            }

            await db.query(`INSERT INTO crm_checklist_items(${Object.keys(field[i])}) value(${data.toString()})`);
        }

        response.result = 'success';
    } catch (error) {
        response.result = [];
        response.error = error;
        console.log(error)
    }
    return response;
};

exports.storeTaskCommentAttribute = async (field, params) => {
    let response = { error: false, result:[] };
    try {
        let result = null;
        let arrField = FieldCommentAttribute;
        let tmpData = [];
        let tmpField = [];
        let table = 'crm_likes';

        if(field.attType === 'pin'){
            table = 'crm_pin_comments'
            field.pinned_by = field.created_by;
            delete field.created_by
            
        }

        for(let i=0; i < arrField.length; i++){
            if(field[arrField[i]] !== undefined){
                if(params.status === 'store'){
                    tmpField.push(`\`${arrField[i]}\``);
                    tmpData.push(field[arrField[i]]);
                }else{
                    tmpData.push(`${arrField[i]}="${field[arrField[i]]}"`);
                }
            } 
        }

        let query = '';
        if(params.status === 'store'){
            query = `
                INSERT INTO ${table}(${tmpField.toString()})
                value(?);
            `;
            result = await db.query(query,[tmpData]);
        }else{
            query = `
            UPDATE ${table} SET ${tmpData.toString()} WHERE id=?;
            `;
            result = await db.query(query,[field.id]);
        }

        response.result = 'success';
    } catch (error) {
        response.result = [];
        response.error = error;
        console.log(error)
    }
    return response;
};

exports.deleteTaskReminder = async (field, params) => {
    let response = { error: false, result:[] };
    try {
        let result = null;
        fieldReminder.push("deleted");
        let arrField = fieldReminder;
        let tmpData = [];
        let table = 'crm_events';

        for(let i=0; i < arrField.length; i++){
            if(field[arrField[i]] !== undefined){
                tmpData.push(`${arrField[i]}="${field[arrField[i]]}"`);
            } 
        }
        let query = `UPDATE ${table} SET ${tmpData.toString()} WHERE id=?;`;
        result = await db.query(query,[field.id]);

        response.result = 'success';
    } catch (error) {
        response.result = [];
        response.error = error;
        console.log(error)
    }
    return response;
};

exports.deleteTaskComment = async (field, params) => {
    let response = { error: false, result:[] };
    try {
        let result = null;
        fieldReminder.push("deleted");
        let arrField = fieldReminder;
        let tmpData = [];
        let table = 'crm_project_comments';

        for(let i=0; i < arrField.length; i++){
            if(field[arrField[i]] !== undefined){
                tmpData.push(`${arrField[i]}="${field[arrField[i]]}"`);
            } 
        }
        let query = `UPDATE ${table} SET ${tmpData.toString()} WHERE id=?;`;
        result = await db.query(query,[field.id]);

        response.result = 'success';
    } catch (error) {
        response.result = [];
        response.error = error;
        console.log(error)
    }
    return response;
};

exports.storeTaskLogged = async(field,params)=>{
    return new Promise(async(hasil)=>{
        let response = { error:false, result:[] };
        try {
            let store = null;
            let now = new Date();
            let hours = 0;
            let id = 0;
            let start_time = null;
            let end_time = null;
            let query = `
                SELECT id, start_time from crm_project_time where task_id=? and project_id=? and status='open'
            `;
            let getId = await db.query(query,[field.task_id, field.project_id]);
            if(getId.length){
                id = getId[0].id;
                start_time = getId[0].start_time;

                hours = moment.duration(moment(now).diff(moment(start_time))).asSeconds();
                end_time = now;
                
                store = await db.query(`UPDATE crm_project_time SET note=?, end_time=?, status=?, hours=? where task_id=? and project_id=?`,[
                    field.note, now, field.status, hours, field.task_id, field.project_id
                ]);    
                
                response.result = [{ hours, start_time, end_time, ...field }];
            }else{
                store = await db.query(`INSERT INTO crm_project_time(user_id,task_id,project_id,start_time,status,hours) value(?)`,[[
                    field.user_id, field.task_id, field.project_id, now, field.status, 0
                ]]);    
                response.result = [];
            }
            
        } catch (error) {
            response.error = error;
        }
        return hasil(response);
    });
}

const get_gantt_data = async(property)=>{

    return new Promise(async(hasil)=>{
        let tasks_table = 'crm_tasks';
        let milestones_table = 'crm_milestones';
        let users_table = 'user';
        let task_status_table = 'crm_task_status';
        let project_members_table = 'crm_project_members';
        let projects_table = 'crm_projects';
    
        let where = '';
        if(property.milestone_id){
            where += ` AND ${tasks_table}.milestone_id='${property.milestone_id}' `;
        }
        
        if(property.project_id){
            where += ` AND ${tasks_table}.project_id='${property.project_id}' `;
        }else {
            //show only opened project's tasks on global view
            where += ` AND ${tasks_table}.project_id IN(SELECT ${projects_table}.id FROM ${projects_table} WHERE ${projects_table}.deleted=0 AND ${projects_table}.status='open') `;
        }
    
        if(property.assigned_to){
            where += ` AND ${tasks_table}.assigned_to='${property.assigned_to}' `;
        }
    
        if(property.status_id){
            where += ` AND ${tasks_table}.status_id IN(${property.status_id.toString()}) `;
        }
    
        if (property.exclude_status) {
            where += ` AND ${tasks_table}.status_id!='${property.exclude_status}' `;
        }
    
        let extra_join = "";
        let extra_where = "";
        if (property.user) {
            extra_join = ` LEFT JOIN (SELECT ${project_members_table}.user_id, ${project_members_table}.project_id FROM ${project_members_table} WHERE ${project_members_table}.user_id='${property.user}' AND ${project_members_table}.deleted=0 GROUP BY ${project_members_table}.project_id) AS project_members_table ON project_members_table.project_id= ${tasks_table}.project_id `;
            extra_where = ` AND project_members_table.user_id='${property.user}' `;
        }
    
        if (property.show_assigned_tasks_only_user_id) {
            where += ` AND (${tasks_table}.assigned_to='${property.show_assigned_tasks_only_user_id}' OR FIND_IN_SET('${property.show_assigned_tasks_only_user_id}', ${tasks_table}.collaborators)) `;
        }
    
        let sql = '';
        sql = `
            SELECT 
            ${tasks_table}.project_id,
            ${tasks_table}.id AS task_id, 
            ${tasks_table}.title AS task_title, 
            ${tasks_table}.status_id, 
            IF(${tasks_table}.start_date, ${tasks_table}.start_date,null) as start_date, 
            IF(${tasks_table}.deadline,DATE_FORMAT(${tasks_table}.deadline,"%Y-%m-%d"),null) AS end_date, 
            ${tasks_table}.parent_task_id,
            ${milestones_table}.id AS milestone_id, 
            ${milestones_table}.title AS milestone_title, 
            IF(${milestones_table}.due_date,DATE_FORMAT(${milestones_table}.due_date,"%Y-%m-%d"),null) AS milestone_due_date, 
            ${tasks_table}.assigned_to, 
            ${users_table}.name AS assigned_to_name, 
            ${tasks_table}.project_id, 
            CONCAT(${projects_table}.title) AS project_name,
            ${task_status_table}.title AS status_title, 
            ${task_status_table}.color AS status_color 
            FROM ${tasks_table} 
            LEFT JOIN ${milestones_table} ON ${milestones_table}.id= ${tasks_table}.milestone_id AND crm_milestones.project_id = crm_tasks.project_id
            LEFT JOIN ${users_table} ON ${users_table}.user_id= ${tasks_table}.assigned_to
            LEFT JOIN ${task_status_table} ON ${task_status_table}.id =  ${tasks_table}.status_id
            LEFT JOIN ${projects_table} ON ${projects_table}.id= ${tasks_table}.project_id
            ${extra_join} 
            WHERE ${tasks_table}.deleted=0 ${where} ${extra_where} 
            ORDER BY ${tasks_table}.start_date, ${milestones_table}.due_date DESC
        `;
        let result = await db.query(sql);
        return hasil(result);
    });

}

exports.getGanttData = async(field,params)=>{
    let response = { error:false, result:[] }
    return new Promise(async(hasil)=>{
        try {
            
            let { project_id,group_by,milestone_id,user_id, status } = params;
        
            // project_id = 0, group_by = "milestones", milestone_id = 0, user_id = 0, status = ""
            let options={
                "status_id" : status,
                "show_assigned_tasks_only_user_id" : user_id,
                "milestone_id" : milestone_id,
                "assigned_to" : user_id,
                "user": user_id,
                "project_id" : project_id
            };
        
            if (!status) {
                options["exclude_status"] = 3; // hanya tampilin yg belum selesai
            }

            let get_data = await get_gantt_data(options);
            let now = new Date(moment(new Date()).format("Y-MM-DD"));
            let tasks_array = [];
            let group_array = [];

            // filter array for startdate and end date where they empty
            get_data = get_data.filter(v => v.start_date !== null && v.end_date !== null);

            console.log(get_data.length)
            for(let i=0; i < get_data.length; i++){
                let items = get_data[i];

                if(items.start_date){
                    items.start_date = new Date(items.start_date);
                }

                if(items.end_date){
                    items.end_date = new Date(items.end_date);
                }


                let start_date = items.start_date ? items.start_date : now;
                let end_date = items.end_date ? items.end_date : items.milestone_due_date;
                let group_id = 0;
                let group_name = "";
        
                if (!end_date) {
                    end_date = start_date;
                }
        
                if (group_by === "milestones") {
                    group_id = items.milestone_id || (+new Date * Math.random()).toString(36).substring(0,6)  ;
                    group_name = items.milestone_title;
                } else if (group_by === "members") {
                    group_id = items.assigned_to;
                    group_name = items.assigned_to_name;
                } else if (group_by === "projects") {
                    group_id = items.project_id;
                    group_name = items.project_name;
                }
        
                group_id = `${group_by}-${group_id}`;
                if (!group_name) {
                    group_name = "not_specified";
                }
        
                let color = items.status_color;
        
                //has deadline? change the color 
                if (items.status_id == 1 && items.end_date && now > items.end_date) {
                    color = "#d9534f";
                }
        
                if (end_date < start_date) {
                    end_date = start_date;
                }
        

                let gantt_array_data = {};
                let idx = group_array.findIndex((arg)=> arg.id === group_id);
                if (idx < 0) {
                    //add it first
                    let maju3hari = moment(start_date).add(3,"days");
                    gantt_array_data = {
                        "id" : group_id,
                        "milestoneGroup" : group_id,
                        project_id:items.project_id || 0,
                        "name" : group_name,
                        "start" : start_date,
                        "end" : new Date(maju3hari),
                        "draggable" : false, //disable group dragging
                        "custom_class" : "no-drag",
                        "type":"milestone",
                        "progress" : 0,
                        ...items
                    };
        
                    //add group seperately 
                    group_array.push(gantt_array_data);
                }

                let group_key = group_array.findIndex((arg)=> arg.milestoneGroup === group_id);
                
                if (group_array[group_key].start > start_date) {
                    group_array[group_key]["start"] = start_date;
        
                    let maju3hari = moment(start_date).add(3,"days");
                    group_array[group_key]["end"] = new Date(maju3hari);
                }
        
                let dependencies = group_id;
        
                //link parent task
                if (items.parent_task_id) {
                    dependencies += ", "+items.parent_task_id;
                }
        
                //add task data under a group
                gantt_array_data = {
                    "id" : items.task_id.toString(),
                    "project_id":items.project_id || 0,
                    "name" : items.task_title,
                    "start" : start_date,
                    "end" : end_date,
                    "bg_color" : color,
                    "progress" : 0,
                    "type":"task",
                    "dependencies" : dependencies,
                    "draggable" : false,
                    "milestoneGroup" : group_id,
                        "custom_class" : "no-drag",
                        ...items
                    //"draggable" : true : false, //disable dragging
                };
        
                tasks_array.push(gantt_array_data);
        
            }

            if(params.group_by === '0ss') {

                get_data.forEach((str)=>{
                    if(!str.dependencies) str.dependencies = '';
                    str.id = str.task_id.toString();
                    str.title = str.task_title;
                    str.color = str.status_color;
                    str.progress = 0;
                    ///str.type = ;
                    str.draggable = false;
                    str.name = str.task_title;

                    str.start_date = moment(str.start_date || new Date()).format("YYYY-MM-DD");
                    str.end_date = moment(str.end_date || new Date()).format("YYYY-MM-DD");
                    str.start = moment(str.start_date || new Date()).format("YYYY-MM-DD");
                    str.end = moment(str.end_date || new Date()).format("YYYY-MM-DD");
                    
                });
                response.result = get_data;
            }else {
                let gantt = [];
                let keys = Object.keys(tasks_array);
                if(tasks_array.length){
                    for(let i=0; i < tasks_array.length; i++){
                        let idx = group_array.findIndex((check)=> check.id.toString() === tasks_array[i].milestoneGroup);
                        
                        if(idx > -1){
                            let idx2 = gantt.findIndex((str)=> str.id === group_array[idx].id);
                            if(idx2 == -1){
                                gantt.push(group_array[idx])
                            }
                        }
                      
                            gantt.push(tasks_array[i])
                        
                    }
                }
                
                if(gantt.length){
                    gantt.forEach((str)=>{
                        if(!str.dependencies) str.dependencies = '';
                        str.start_date = moment(str.start_date).format("YYYY-MM-DD");
                        str.end_date = moment(str.end_date).format("YYYY-MM-DD");
                        str.start = moment(str.start).format("YYYY-MM-DD");
                        str.end = moment(str.end).format("YYYY-MM-DD");
                    });
                }
                response.result = gantt;
            }
            
        } catch (error) {
            console.log(error)
            response.error = error;
        }
        return hasil(response);
    });
}

exports.importTask = async(data)=>{
    return new Promise(async(hasil)=>{
        let response = { error:false, result:null };
        try {
            
            const Excel = require('exceljs');
            const wb = new Excel.Workbook();
            const filePath = data.filePath;
    
            wb.xlsx.readFile(filePath).then( async function(){
                let sh = wb.getWorksheet("Sheet1");
    
                //Get all the rows data [1st and 2nd column]
                let tempArray = [];
                let result=[];
                for (i = 2; i <= sh.rowCount; i++) {
                    // cek project ada atau tidak
                    // cek milestone ada atau tidak berdasarkan project
                    // cek assigned to dan collaborators sebagai member project atau tidak
                    // labels jika tidak ada akan created ke temp dan di move ke crm_labels pada trigger selanjutnya. akan dihapus dengan crontab param 1H
    
                    // 0=title
                    // 1=description
                    // 2=project
                    // 3=point
                    // 4=milestone
                    // 5=assignee
                    // 6=collaborator
                    // 7=status
                    // 8=label
                    // 9=start_date
                    // 10=deadline
                    if(!sh.getRow(i).getCell(1).value && !sh.getRow(i).getCell(2).value && !sh.getRow(i).getCell(3).value && 
                    !sh.getRow(i).getCell(4).value && !sh.getRow(i).getCell(5).value && !sh.getRow(i).getCell(6).value && 
                    !sh.getRow(i).getCell(7).value && !sh.getRow(i).getCell(8).value && !sh.getRow(i).getCell(9).value && 
                    !sh.getRow(i).getCell(10).value && !sh.getRow(i).getCell(11).value){
                        // is null all skip
                    }else{
                        tempArray.push({
                            title:sh.getRow(i).getCell(1).value,
                            description:sh.getRow(i).getCell(2).value,
                            project_title:sh.getRow(i).getCell(3).value,
                            points:sh.getRow(i).getCell(4).value,
                            milestone_title:sh.getRow(i).getCell(5).value,
                            assigned_to:sh.getRow(i).getCell(6).value,
                            collaborators:sh.getRow(i).getCell(7).value,
                            status_title:sh.getRow(i).getCell(8).value,
                            labels_title:sh.getRow(i).getCell(9).value,
                            start_date:sh.getRow(i).getCell(10).value ? moment(new Date(sh.getRow(i).getCell(10).value)).format("YYYY-MM-DD") : null,
                            deadline:sh.getRow(i).getCell(11).value ? moment(new Date(sh.getRow(i).getCell(11).value)).format("YYYY-MM-DD") : null,
                            priority_id:1,
                        })
                    }
                }
    
                //SQL
                let query = `
                    SELECT 
                    (
                    SELECT group_concat(id,'|',title) from crm_projects cp
                    LEFT JOIN user u ON u.user_id = cp.created_by
                    LEFT JOIN company c ON c.company_id = u.company_id
                    WHERE c.company_id = '${data.companyID}' AND cp.deleted =0
                    ) AS project_list,
                    
                    (
                        SELECT group_concat(cm.id,'|',cm.title,'|',cm.project_id) from crm_milestones cm
                        LEFT JOIN crm_projects cp ON cp.id = cm.project_id
                        LEFT JOIN user u ON u.user_id = cp.created_by
                        LEFT JOIN company c ON c.company_id = u.company_id
                        WHERE c.company_id = '${data.companyID}' AND cm.deleted =0
                    ) AS milestone_list, 
                    
                    (
                    SELECT group_concat(cpm.user_id,'|',u.name) FROM crm_project_members cpm
                    left join crm_projects cp ON cp.id = cpm.project_id
                    LEFT JOIN user u ON u.user_id = cpm.user_id
                    LEFT JOIN company c ON c.company_id = u.company_id
                    WHERE c.company_id = '${data.companyID}' AND cp.deleted =0 AND cpm.deleted = 0
                    ) AS member_list,
                    
                    (
                    SELECT group_concat(id,'|',title,'|',key_name) FROM crm_task_status cts
                    LEFT JOIN company c ON c.company_id = cts.company_id
                    WHERE c.company_id = '${data.companyID}' AND cts.deleted =0
                    ) AS status_list,
                    
                    (
                    SELECT group_concat(id,'|',title) FROM crm_labels cl
                    LEFT JOIN user u ON u.user_id = cl.user_id
                    LEFT JOIN company c ON c.company_id = u.company_id
                    WHERE c.company_id = '${data.companyID}' AND cl.deleted =0 AND lower(cl.context) = 'task'
                    ) AS 'labels_list'
                    
                    FROM DUAL;				
                `;
                let checkData = await db.query(query);
                let arr = {};
                if(checkData.length){
                    for(let i=0; i< checkData.length; i++){
                        let list = checkData[i];
                        arr.project_list = await parseString(list.project_list,'project_list');
                        arr.milestone_list = await parseString(list.milestone_list,'milestone_list2');
                        arr.status_list = await parseString(list.status_list,'status_list');
                        arr.labels_list = await parseString(list.labels_list,'labels_list');
                        arr.collaborator_list = await parseString(list.member_list,'collaborator_list');
                    }

                    let idx = -1;
                    let storeEelem = [];

                    for(let k=0; k < tempArray.length; k++){
                        let selected_project_id = null;
                        tempArray[k].error = {};

                        if(tempArray[k].project_title){
                            idx = arr.project_list ? arr.project_list.findIndex((str)=> str.project_title.toLowerCase() === tempArray[k].project_title.toLowerCase()) : -1;
                            if(idx == -1){
                                tempArray[k].error.project = `Project ${tempArray[k].project_title} is not found.`
                            }else{
                                selected_project_id = arr.project_list[idx].project_id;
                                tempArray[k].project_id = selected_project_id;
                            }
                        }else{
                            tempArray[k].error.project = `Project ${tempArray[k].project_title} is not found.`
                        }

                        // milestone
                        if(tempArray[k].milestone_title){
                            idx = arr.milestone_list ? arr.milestone_list.findIndex((str)=> str.milestone_title.toLowerCase() === tempArray[k].milestone_title.toLowerCase()) : -1;
                            if(idx == -1){
                                tempArray[k].error.milestone = `Milestone ${tempArray[k].milestone_title} is not found.`
                            }else{
                                if(selected_project_id === arr.milestone_list[idx].project_id){
                                    tempArray[k].milestone_id = [arr.milestone_list[idx].milestone_id];
                                }else{
                                    tempArray[k].error.milestone = `Milestone ${tempArray[k].milestone_title} is not found.`
                                }
                            }
                        }else{
                            tempArray[k].milestone_id = null;
                        }

                        // assigned_to
                        if(tempArray[k].assigned_to){
                            idx = arr.collaborator_list ? arr.collaborator_list.findIndex((str)=> str.user_name.toLowerCase() === tempArray[k].assigned_to.toLowerCase()) : -1;
                            if(idx == -1){
                                tempArray[k].error.assigned_to = `User ${tempArray[k].assigned_to} is not found.`
                            }else{
                                tempArray[k].assigned_to = arr.collaborator_list[idx].user_id;
                            }
                        }else{
                            tempArray[k].error.assigned_to = `User ${tempArray[k].assigned_to} is not found.`
                        }

                        // collaborators
                        if(tempArray[k].collaborators){

                            let arr_collabs = tempArray[k].collaborators.split(',');
                            let storeCollabs = [];
                            let state = [];
                            arr_collabs.forEach((collabs)=>{
                                idx = arr.collaborator_list ? arr.collaborator_list.findIndex((str)=> str.user_name.toLowerCase() === collabs.toLowerCase()) : -1;
                                if(idx == -1){
                                    state.push(collabs);
                                }else{
                                    storeCollabs.push(arr.collaborator_list[idx].user_id); 
                                }
                            })
                            if(!state.length){
                                tempArray[k].collaborators = storeCollabs;
                            }else{
                                tempArray[k].error.collaborators = `Collaborators ${state.toString()} is not found.`;
                            }
                        }else{
                            tempArray[k].collaborators = [];
                        }

                        // created status jika tidak ada
                        if(tempArray[k].status_title){
                            idx = arr.status_list ? arr.status_list.findIndex((str)=> str.status_title.toLowerCase() === tempArray[k].status_title.toLowerCase()) : -1;
                            if(idx == -1){
                                tempArray[k].error.status = `Status ${tempArray[k].status_title} is not found.`
                            }else{
                                tempArray[k].status_key_name = arr.status_list[idx].status_key_name;
                                tempArray[k].status_id = arr.status_list[idx].status_id;
                            }
                        }else{
                            tempArray[k].status_id = 1;
                            tempArray[k].status = 'to_do';
                        }

                        // labels
                        if(tempArray[k].labels_title){
                            let storeLabels2 = [];
                            let errlabels = [];
                            let arrLabels = tempArray[k].labels_title.split(',');
                            for(let j=0; j < arrLabels.length; j++){
                                let str2 = arrLabels[j];
                                idx = arr.labels_list ? arr.labels_list.findIndex((str)=> str.label_title.toLowerCase() === str2.toLowerCase()) : -1;
                                if(idx == -1){
                                    // let xUnixMili = Number(moment(new Date()).format("x"))+Math.floor(Math.random()*50);
                                    // let id_temp = `crm_labels_temp-${new Date()}`;
                                    // let value = [[str2,"#83c340","task",data.userId,id_temp]];
                                    // await db.query(`insert into crm_labels_temporary(title,color,context,user_id,id_temporary) value(?)`,value);
                                    errlabels.push(arrLabels[j]);
                                }else{
                                    storeLabels2.push(arr.labels_list[idx].label_id);
                                }
                            }
                            if(errlabels.length){
                                tempArray[k].error.labels = `Labels ${errlabels.toString()} is not found.`;
                            }
                            tempArray[k].labels = storeLabels2;
                        }else{
                            tempArray[k].labels = [];
                        }
                        
                        let checkErr = Object.keys(tempArray[k].error);
                        
                        if(checkErr.length){
                            let listErrValue = Object.values(tempArray[k].error);
                            let listErrObject = {};
                            checkErr.forEach((list)=> listErrObject[list] = "#ffbfbf");
                            checkErr = listErrObject;
                            tempArray[k].error = { listErrValue, listErrKey:checkErr, bg_color: '#ffbfbf', font_color : '#ffbfbf'}
                        }else{
                            tempArray[k].error = { listErrValue:[],listErrKey:{},bg_color: '#fff', font_color : '#000'}
                        }
                        if(!tempArray[k].error.listErrValue.length){
                            let form = {
                                "title":tempArray[k].title || null,
                                "description":tempArray[k].description || null,
                                "project_id":tempArray[k].project_id || 0,
                                "milestone_id":tempArray[k].milestone_id || [],
                                "assigned_to":tempArray[k].assigned_to || null,
                                "deadline":tempArray[k].deadline || null,
                                "start_date":tempArray[k].start_date || null,
                                "labels":tempArray[k].labels || [],
                                "points":tempArray[k].points || 1,
                                "status":tempArray[k].status_key_name || 'to_do',
                                "status_id":tempArray[k].status_id || 1,
                                "priority_id":tempArray[k].priority_id,
                                "collaborators":tempArray[k].collaborators || []
                            };
                            storeEelem.push(form);
                        }
                    }

                    response.result = { dataStore: storeEelem, dataResponse: tempArray };
                    return hasil(response);
                }else{
                    return hasil(response);
                }
            });
        } catch (error) {
            response.error=true;
            response.result=error;
            return hasil(response);  
        }
    })
}
exports.addActivityTask;


