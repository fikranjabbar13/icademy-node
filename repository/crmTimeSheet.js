const moment = require("moment");
let today = moment(new Date()).format("YYYY-MM-DD");
const db = require('../configs/database');
const sortField = ['title','description','project_id','milestone_id','assigned_to','deadline','labels','points','status','status_id','priority_id','start_date','collaborators','parent_task_id','blocking','blocked_by',"sort"];

const addActivityTask = async (field, params) => {
    return new Promise(async(hasil)=>{
        try {
    
            let arrField = FieldProject;
            let arrFieldActivity = sortFieldActivity;
            let tmpData = [];
            let changeData = {};
            let result = null;
            let query = '';
    
            if(field.id){
                query = `
                    SELECT ${arrField.toString()} from crm_projects where id=? ;
                `;
                result = await db.query(query,[field.id]);
                if(result.length) result = result[0];

                for(let i=0; i < arrField.length; i++){


                    if(['start_date','deadline'].indexOf(arrField[i]) > -1){
                        let CheckDate = (result[arrField[i]] && result[arrField[i]] != '') ? moment(result[arrField[i]]).format('YYYY-MM-DD') : null;
                        result[arrField[i]] = CheckDate;
                        field[arrField[i]] = CheckDate;
                    }else{
                        if(field[arrField[i]] !== undefined){
                            field[arrField[i]] = field[arrField[i]].toString();
                        }
                        if(result[arrField[i]]){
                            result[arrField[i]] = result[arrField[i]].toString();
                        }
                    }

                    if( result[arrField[i]] !== field[arrField[i]] ){
                        // if(manyValue.indexOf(arrField[i]) > -1){
                        // }
                        changeData[arrField[i]] = {
                            from: result[arrField[i]],
                            to: field[arrField[i]]
                        }
                    }
                }
            }
    
            if(Object.keys(changeData).length > 0){
                field.changes = JSON.stringify(changeData);
            }
    
            for(let i=0; i < arrFieldActivity.length; i++){
                if(field[arrFieldActivity[i]] !== undefined){
                    tmpData.push(field[arrFieldActivity[i]]);
                } 
            }
            
            
            query = `INSERT INTO crm_activity_logs(${sortFieldActivity.toString()}) value(?)`;
            result = await db.query(query,[tmpData]);
            return hasil({ error: false, result:'success' });
    
        } catch (error) {
            console.log(error);
            return hasil({ error: error, result:null });
        }
    });
};

const parseString = async (list,type, distinct)=>{
    return new Promise((hasil)=>{
        try {            
            //list = list.substr(0,list.length-2);
            let arrLabel = list.split(',');
            let arrField = {
                subtask_list : ['subtask_id','subtask_title','subtask_status_id'],
                checklist_item:["id","title","is_checked","task_id","sort"],
                logged:["hours","start_time","end_time","task_id","project_id","note","status","user_id"],
                priority_list:["priority_id","priority_title","priority_icon","priority_color"], 
                status_list: ["status_id","status_title","status_key_name","status_color"],
                project_status_list: ["status_id","status_title","status_color","status_key_name","status_project_id"],
                project_members_list :['user_id','user_name','user_avatar','project_id'],
                project_client_list :['client_id','client_company_name','client_type'],
                project_list:['project_id','project_title'],
                milestone_byProject_list: ["milestone_id", "milestone_title", "milestone_project_id",'milestone_due_date'],
                byProject_members_list :['user_id','user_name','user_project_id'],
                tasks_list: ['task_id','task_title','project_id'],
                labels_list: ['label_id','label_title','label_color'],
                collaborator_list: ['user_id','user_name','user_avatar','project_id'],
                milestone_list: ["milestone_id", "milestone_title", "milestone_deadline", "milestone_status",'project_id'],
                milestone_list2: ["milestone_id", "milestone_title", 'project_id'],
                blocked_list:["id","title","created_date"],
                blocking_list:["id","title","created_date"],
                comment_like_list: ['like_id',"comment_id","task_id","created_by_user","created_by_username","created_by_useravatar",'created_at'],
                comment_pin_list: ["pin_id","comment_id","task_id","created_by_user","created_by_username","created_by_useravatar",'created_at'],
                events_list: ['id','task_id','project_id','ticket_id','lead_id','type','title','description','start_date','end_date','start_tme','end_time','reminder_status','recurring','repeat_type','repeat_every'],
            };
            let splitLabel = null;
            let label = [];
            
            if(arrLabel.length){
    
                splitLabel = arrField[type];
                
                for(let i=0; i < arrLabel.length; i++){
                    
                    arrLabel[i] = arrLabel[i].split('|');
                    let labelObject = {};
                    
                    for(let j=0; j < arrLabel[i].length; j++){
                        if(!isNaN(Number(arrLabel[i][j])) && arrLabel[i][j] != ''){
                            labelObject[splitLabel[j]] = Number(arrLabel[i][j]);
                        }else{
                            labelObject[splitLabel[j]] = arrLabel[i][j] === '' ? null : arrLabel[i][j];
                        }
                    }
                    
                    if(distinct){
                        let idx = []; 
                        if(type == 'byProject_members_list'){
                            idx = label.filter((list)=> (list.user_id == labelObject.user_id && list.user_project_id == labelObject.project_id));
                            if(idx.length == 0) label.push(labelObject);
                        }else if(type == 'milestone_byProject_list'){
                            idx = label.filter((list)=> (list.milestone_id == labelObject.milestone_id && list.milestone_project_id == labelObject.milestone_project_id));
                            if(idx.length == 0) label.push(labelObject);
                        }else if(type == 'project_list'){
                            idx = label.filter((list)=> (list.project_id == labelObject.project_id));
                            if(idx.length == 0) label.push(labelObject);
                        }
                    }else{
                        label.push(labelObject);
                    }
                }
            }
            return hasil(label);
        } catch (error) {
            //console.log(error,list)
            return hasil([]);
        }
    });
}

/**
 * 
 * @param {*} field 
 * @param {*} params 
 * @returns {error=boolean,result=number}
 */
exports.getTotalTimeProject = async (field, params)=>{
    // ("project_id" => $project_id, "user_id" => $user_id, "status" => "open", "deleted" => 0)
    return new Promise(async(hasil)=>{
        let response = { error:false, result:0 }
        try {
            let sql = `
                select IFNULL(sum(hours),0) as total_time 
                from crm_project_time 
                where project_id=? and deleted=0;
            `;
            let result = await db.query(sql,[params.id]);
            if(result.length){
                let sec = Number(result[0].total_time.toFixed(2));
                // sec = new Date(sec * 1000).toISOString().slice(11, 19);
                // let split = sec.split(':');
                // if(split.length == 2) sec = `0:${sec}`;
                response.result = sec
            }
        } catch (error) {
            response.error =error;
        }
        return hasil(response)
    })
}
