const db = require('../configs/database');

exports.getFormatExamResultByAssigneeId = async (dataDetail) => {
    try {
        const results = await new Promise((resolve, reject) => {
            db.query(
                /* sql */ `SELECT fo.format
                FROM training_exam_result r
                LEFT JOIN training_user u ON u.id = r.training_user_id
                LEFT JOIN training_company tc ON tc.id = u.training_company_id
                LEFT JOIN training_company_licenses_format fo ON fo.company_id = tc.company_id
                WHERE r.id = ?`,
                [dataDetail.idAssignee],
                (err, res) => {
                    if (err) reject(err);
                    resolve(res);
                }
            );
        })
            .then((response) => response)

        return results;
    } catch (error) {
        throw error;
    }
};

exports.getTrainingExamResultByAssigneeId = async (dataDetail) => {
    try {
        const results = await new Promise((resolve, reject) => {
            db.query(
                /* sql */ `SELECT r.*, u.gender, e.company_id FROM training_exam_result r JOIN training_user u ON u.id = r.training_user_id JOIN training_exam e ON e.id = r.exam_id WHERE r.id = ? `,
                [dataDetail.idAssignee],
                (err, res) => {
                    if (err) reject(err);
                    resolve(res);
                }
            );
        })
            .then((response) => response)

        return results;
    } catch (error) {
        throw error;
    }
};

exports.getTotalPassByGender = async (dataDetail) => {
    try {
        const results = await new Promise((resolve, reject) => {
            db.query(
                /* sql */ `SELECT (COUNT(NULLIF(m.license_number,''))+1) AS number
                            FROM training_exam_result r
                            JOIN training_user u ON u.id = r.training_user_id
                            LEFT JOIN training_membership m ON m.training_user_id = r.training_user_id
                            JOIN training_exam e ON e.id = r.exam_id
                            WHERE r.pass=1 AND DATE(r.created_at)=CURDATE() AND u.gender= ?  AND e.company_id= ?`,
                [dataDetail.gender, dataDetail.idCompany],
                (err, res) => {
                    if (err) reject(err);
                    resolve(res);
                }
            );
        })
            .then((response) => response)

        return results;
    } catch (error) {
        throw error;
    }
};

exports.getTrainingMembership = async (dataDetail) => {
    try {
        const results = await new Promise((resolve, reject) => {
            db.query(
                /* sql */ `SELECT * FROM training_membership WHERE training_user_id= ?  ORDER BY created_at DESC LIMIT 1`,
                [dataDetail.idTrainingUser],
                (err, res) => {
                    if (err) reject(err);
                    resolve(res);
                }
            );
        })
            .then((response) => response)

        return results;
    } catch (error) {
        throw error;
    }
};

exports.extendMembership = async (dataDetail) => {
    try {
        const results = await new Promise((resolve, reject) => {
            db.query(
                /* sql */ `UPDATE training_membership SET expired=?, status=? WHERE id=?`,
                [dataDetail.expired, dataDetail.status, dataDetail.idMembership],
                (err, res) => {
                    if (err) reject(err);
                    resolve(res);
                }
            );
        })
            .then((response) => response)

        return results;
    } catch (error) {
        throw error;
    }
};

exports.replaceMembership = async (dataDetail) => {
    try {
        const results = await new Promise((resolve, reject) => {
            db.query(
                /* sql */ `UPDATE training_membership SET license_number=?, expired=?, status=? WHERE id=?`,
                [dataDetail.licenseNumber, dataDetail.expired, dataDetail.status, dataDetail.idMembership],
                (err, res) => {
                    if (err) reject(err);
                    resolve(res);
                }
            );
        })
            .then((response) => response)

        return results;
    } catch (error) {
        throw error;
    }
};

exports.updateTrainingUser = async (dataDetail) => {
    try {
        const results = await new Promise((resolve, reject) => {
            db.query(
                /* sql */ `UPDATE training_user SET license_number=? WHERE id=?`,
                [dataDetail.licenseNumber, dataDetail.idTrainingUser],
                (err, res) => {
                    if (err) reject(err);
                    resolve(res);
                }
            );
        })
            .then((response) => response)

        return results;
    } catch (error) {
        throw error;
    }
};

exports.insertTrainingMembership = async (dataDetail) => {
    try {
        const results = await new Promise((resolve, reject) => {
            db.query(
                /* sql */ `INSERT INTO training_membership (training_user_id, expired, photo, license_card, license_number, status, created_at) VALUES(?)`,
                [dataDetail.data],
                (err, res) => {
                    if (err) reject(err);
                    resolve(res);
                }
            );
        })
            .then((response) => response)

        return results;
    } catch (error) {
        throw error;
    }
};

exports.getIdMembershipByTrainingUser = async (dataDetail) => {
    try {
        const results = await new Promise((resolve, reject) => {
            db.query(
                /* sql */ `SELECT id FROM training_membership WHERE training_user_id = ? ORDER BY id DESC LIMIT 1;`,
                [dataDetail.idTrainingUser],
                (err, res) => {
                    if (err) reject(err);
                    resolve(res);
                }
            );
        })
            .then((response) => response)

        return results;
    } catch (error) {
        throw error;
    }
};

exports.retrieveIdTrainingUserByIdMembership = async (dataDetail) => {
    try {
        const results = await new Promise((resolve, reject) => {
            db.query(
                /* sql */ `SELECT training_user_id FROM training_membership WHERE id = ? ORDER BY id DESC LIMIT 1;`,
                [dataDetail.idMembership],
                (err, res) => {
                    if (err) reject(err);
                    resolve(res);
                }
            );
        })
            .then((response) => response)

        return results;
    } catch (error) {
        throw error;
    }
};

exports.updatePhotoTakenByIdTrainingUser = async (dataDetail) => {
    try {
        const results = await new Promise((resolve, reject) => {
            db.query(
                /* sql */ `UPDATE training_exam_result SET photo_taken = 1 WHERE training_user_id = ?;`,
                [dataDetail.idTrainingUser],
                (err, res) => {
                    if (err) reject(err);
                    resolve(res);
                }
            );
        })
            .then((response) => response)

        return results;
    } catch (error) {
        throw error;
    }
};