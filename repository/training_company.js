const db = require('../configs/database');

exports.getListCompanyByIdCourse = async (detailListCompanyByIdCourse) => {
    /**
     * Get List Company By Id Course
     * idCompany : number
     */
    try {
        const results = await new Promise((resolve, reject) => {
            db.query(
                `
                SELECT 
                    distinct  
                    tc.* 
                FROM 
                    training_company tc 
                LEFT JOIN training_course_company tcc ON tcc.training_company_id = tc.id 
                WHERE 
                    (tc.company_id = '${detailListCompanyByIdCourse.idCompany}' AND tcc.training_course_id = '${detailListCompanyByIdCourse.idCourse}') OR tc.company_id = '${detailListCompanyByIdCourse.idCompany}' 
                    AND STATUS = 'active' 
                ORDER BY 
                    NAME ASC;
                
                SELECT 
                distinct 
                tc.* 
                from training_course_company tcc
                left join training_company tc on tc.id = tcc.training_company_id
                where 
                tcc.training_course_id = '${detailListCompanyByIdCourse.idCourse}' AND tcc.active = 1;
                `,
                (err, res) => {
                    console.info(err, 9090)
                    if (err) reject(err);

                    if (res[1].length > 0) {
                        resolve(res[1]);
                    } else {
                        resolve(res[0]);
                    }
                }
            );
        })
            .then((response) => response)

        return results;
    } catch (error) {
        throw error;
    }
};

exports.getIDCompanyByTrainingCompany = async (dataDetail) => {
    try {
        const results = await new Promise((resolve, reject) => {
            db.query(
                /* sql */ `SELECT company_id AS idCompany FROM training_company WHERE id = ? LIMIT 1;`,
                [dataDetail.idTrainingCompany],
                (err, res) => {
                    if (err) reject(err);
                    resolve(res);
                }
            );
        })
            .then((response) => response)

        return results;
    } catch (error) {
        throw error;
    }
};

exports.getAdminCompany = async (dataDetail) => {
    try {
        const results = await new Promise((resolve, reject) => {
            db.query(
                /* sql */ `SELECT user_id FROM user WHERE company_id = ? AND level = 'admin' AND status = 'active';`,
                [dataDetail.idCompany],
                (err, res) => {
                    if (err) reject(err);
                    resolve(res);
                }
            );
        })
            .then((response) => response)

        return results;
    } catch (error) {
        throw error;
    }
};

exports.getAdminTrainingByIDCompany = async (dataDetail) => {
    try {
        const results = await new Promise((resolve, reject) => {
            db.query(
                /* sql */ `SELECT
	u.user_id
FROM
	user u
	LEFT JOIN grup g ON g.grup_id = u.grup_id
	JOIN company c ON c.company_id = u.company_id 
WHERE
	c.company_id = ?
	AND u.level = 'client'
	AND grup_name = 'Admin Training'
	AND u.STATUS = 'active';`,
                [dataDetail.idCompany],
                (err, res) => {
                    if (err) reject(err);
                    resolve(res);
                }
            );
        })
            .then((response) => response)

        return results;
    } catch (error) {
        throw error;
    }
};

exports.getTrainingRegistrationSchemaByIdTrainingCompany = async (dataDetail) => {
    try {
        const results = await new Promise((resolve, reject) => {
            db.query(
                /* sql */ `select *  
            from training_registration_schema  
            WHERE training_company_id = ?;`,
                [dataDetail.idTrainingCompany],
                (err, res) => {
                    if (err) reject(err);
                    resolve(res);
                }
            );
        })
            .then((response) => response)
        return results;
    } catch (error) {
        throw error;
    }
};
