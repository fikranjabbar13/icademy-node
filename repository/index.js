module.exports = {
    //Training Exam
    readTrainingExam: require('./training_exam').readTrainingExam,
    readComposition: require('./training_exam').readComposition,
    readQuestion: require('./training_exam').readQuestion,
    readOptions: require('./training_exam').readOptions,
    retrieveRepoExamByCourse: require('./training_exam').retrieveRepoExamByCourse,
    retrieveUserMembership: require('./training_exam').retrieveUserMembership,
    retrieveTrainingExamResult: require('./training_exam').retrieveTrainingExamResult,
    createTrainingExam: require('./training_exam').createTrainingExam,
    createTrainingExamComposition: require('./training_exam').createTrainingExamComposition,
    retrieveQuestionByExamIdAndQuestionAndAnswer: require('./training_exam').retrieveQuestionByExamIdAndQuestionAndAnswer,
    readTemporaryExamResultByIdAssignee: require('./training_exam').readTemporaryExamResultByIdAssignee,
    

    //Training Course
    getListTagByIdCompany: require('./training_course').getListTagByIdCompany,
    getScoreWebinar: require('./training_course').getScoreWebinar,
    getLiveClass: require('./training_course').getLiveClass,
    getScoreLiveClass: require('./training_course').getScoreLiveClass,
    getUserTraining: require('./training_course').getUserTraining,
    getDataUser: require('./training_course').getDataUser,
    getTrainingCourseCompany: require('./training_course').getTrainingCourseCompany,
    getTrainingCourseByTrainingUserId: require('./training_course').getTrainingCourseByTrainingUserId,
    getTrainingExamCheck: require('./training_course').getTrainingExamCheck,
    getRequiredCourse: require('./training_course').getRequiredCourse,
    getTrainingCourseSession: require('./training_course').getTrainingCourseSession,
    getQuiz: require('./training_course').getQuiz,
    getExam: require('./training_course').getExam,
    getUnassignedExam: require('./training_course').getUnassignedExam,
    getAgendaByCourseId: require('./training_course').getAgendaByCourseId,
    getListCompanyByCourseId: require('./training_course').getListCompanyByCourseId,
    getTrainingCourseByTrainingUserIdAndIdCourse: require('./training_course').getTrainingCourseByTrainingUserIdAndIdCourse,
    getTrainingCourseSubmission: require('./training_course').getTrainingCourseSubmission,
    getDetailSubmission: require('./training_course').getDetailSubmission,
    postTrainingUserAnswer: require('./training_course').postTrainingUserAnswer,
    getDetailSubmissionById: require('./training_course').getDetailSubmissionById,
    updateTrainingUserAnswer: require('./training_course').updateTrainingUserAnswer,
    getDetailSubmissionUser: require('./training_course').getDetailSubmissionUser,
    getDetailSubmissionByIdCourse: require('./training_course').getDetailSubmissionByIdCourse,
    getTrainingSubmissionAnswerByIdSubmission: require('./training_course').getTrainingSubmissionAnswerByIdSubmission,

    //Training Company
    getListCompanyByIdCourse: require('./training_company').getListCompanyByIdCourse,
    getIDCompanyByTrainingCompany: require('./training_company').getIDCompanyByTrainingCompany,
    getAdminCompany: require('./training_company').getAdminCompany,
    getAdminTrainingByIDCompany: require('./training_company').getAdminTrainingByIDCompany,
    getTrainingRegistrationSchemaByIdTrainingCompany: require('./training_company').getTrainingRegistrationSchemaByIdTrainingCompany,

    //Grup
    getListAllCompany: require('./grup').getListAllCompany,
    getListAllGlobalSettingsTemplate: require('./grup').getListAllGlobalSettingsTemplate,
    insertGlobalSettings: require('./grup').insertGlobalSettings,
    insertGlobalSettingsTemplate: require('./grup').insertGlobalSettingsTemplate,
    retrieveSettingsTemplate: require('./grup').retrieveSettingsTemplate,
    retrieveCheckAccessByRole: require('./grup').retrieveCheckAccessByRole,
    retrieveCheckAccessBySub: require('./grup').retrieveCheckAccessBySub,
    removeListAllGlobalSettings: require('./grup').removeListAllGlobalSettings,

    //Membership
    getFormatExamResultByAssigneeId: require('./training_membership').getFormatExamResultByAssigneeId,
    getTrainingExamResultByAssigneeId: require('./training_membership').getTrainingExamResultByAssigneeId,
    getTotalPassByGender: require('./training_membership').getTotalPassByGender,
    getTrainingMembership: require('./training_membership').getTrainingMembership,
    extendMembership: require('./training_membership').extendMembership,
    replaceMembership: require('./training_membership').replaceMembership,
    updateTrainingUser: require('./training_membership').updateTrainingUser,
    insertTrainingMembership: require('./training_membership').insertTrainingMembership,
    getIdMembershipByTrainingUser: require('./training_membership').getIdMembershipByTrainingUser,
    retrieveIdTrainingUserByIdMembership: require('./training_membership').retrieveIdTrainingUserByIdMembership,
    updatePhotoTakenByIdTrainingUser: require('./training_membership').updatePhotoTakenByIdTrainingUser,

    //Liveclass
    postParticipantsByIdUser: require('./liveclass').postParticipantsByIdUser,
    retrieveUserParticipantsByIdBooking: require('./liveclass').retrieveUserParticipantsByIdBooking,
    postUserGuestByEmail: require('./liveclass').postUserGuestByEmail,
    retrieveUserGuestByEmail: require('./liveclass').retrieveUserGuestByEmail,
    retrieveRepoLiveClassByCourse: require('./liveclass').retrieveRepoLiveClassByCourse,

    //Training Report
    retrieveIdTrainingCompany: require('./training_report').retrieveIdTrainingCompany,
    retrieveTotalTrainingUser: require('./training_report').retrieveTotalTrainingUser,
    retrieveTotalExamCompany: require('./training_report').retrieveTotalExamCompany,
    retrieveUserPassedByIdTrainingCompany: require('./training_report').retrieveUserPassedByIdTrainingCompany,
    retrieveUserPassedByIdCompany: require('./training_report').retrieveUserPassedByIdCompany,
    retrieveUserScoreExam: require('./training_report').retrieveUserScoreExam,
    retrieveHistoryUserTrainingExam: require('./training_report').retrieveHistoryUserTrainingExam,
    retrieveDataTrainingUserByEmail: require('./training_report').retrieveDataTrainingUserByEmail,
    retrieveTotalExamTrainingCompany: require('./training_report').retrieveTotalExamTrainingCompany,
    retrieveAverageTimeExamCompany: require('./training_report').retrieveAverageTimeExamCompany,
    retrieveAverageTimeExamTraining: require('./training_report').retrieveAverageTimeExamTraining,


    //Training User
    updateStatusBulkTrainingUser: require('./training_user').updateStatusBulkTrainingUser,
    updateStatusBulkUser: require('./training_user').updateStatusBulkUser,
    updateStatusBulkMembershipUser: require('./training_user').updateStatusBulkMembershipUser,
    getSourceDataTrainingUser: require('./training_user').getSourceDataTrainingUser,
    createFormRegistrationUser: require('./training_user').createFormRegistrationUser,
    readRegistrationUserByTrainingCompany: require('./training_user').readRegistrationUserByTrainingCompany,
    retrieveUserRegistrationDetailById: require('./training_user').retrieveUserRegistrationDetailById,
    updateStatusUserRegistrationById: require('./training_user').updateStatusUserRegistrationById,
    readAllRegistrationUserBySuperAdmin: require('./training_user').readAllRegistrationUserBySuperAdmin,
    countUserRegistrationByIdFormAndStatus: require('./training_user').countUserRegistrationByIdFormAndStatus,
    updateStatusToRemovedUserRegistration: require('./training_user').updateStatusToRemovedUserRegistration,
    readDetailRegistrationUserByIdTraining: require('./training_user').readDetailRegistrationUserByIdTraining,
    
    //Training Question
    updateStatusBulkQuestions: require('./training_question').updateStatusBulkQuestions,
    crmTask : require('./crmTask'),
    crmProject : require("./crmProject"),
    crmTimeSheet : require("./crmTimeSheet"),
    crmOperators : require("./crmOperators")
}