const db = require('../configs/database');

exports.postParticipantsByIdUser = async (detailObject) => {
    try {
        const results = await new Promise((resolve, reject) => {
            db.query(
                /* sql */ `INSERT INTO liveclass_booking_participant (booking_id, user_id, confirmation, actual) VALUES (?);`,
                [[detailObject.idBooking, detailObject.idParticipant, '', '']],
                (err, res) => {
                    if (err) reject(err);
                    resolve(res);
                }
            );
        })
            .then((response) => response)
        return results;
    } catch (error) {
        throw error;
    }
};

exports.retrieveRepoLiveClassByCourse = async (dataRequest) => {
    try {
        const results = await new Promise((resolve, reject) => {
            db.query(
                /* sql */ `SELECT id, judul, company_id, training_course_id FROM webinars WHERE company_id = ? AND (training_course_id=0 OR training_course_id=?)and is_training_liveclass=1 AND publish=1;`,
                [dataRequest.idCompany, dataRequest.idCourse], (err, res) => {
                    if (err) reject(err);
                    resolve(res);
                })
        }).then(response => response)
            .catch(err => err);
        return results;
    } catch (error) {
        throw error;
    }
}

exports.retrieveUserParticipantsByIdBooking = async (detailObject) => {
    try {
        const results = await new Promise((resolve, reject) => {
            db.query(
                /* sql */ `SELECT * FROM liveclass_booking_participant WHERE booking_id = ? AND user_id = ?;`,
                [detailObject.idBooking, detailObject.idParticipant],
                (err, res) => {
                    if (err) reject(err);
                    resolve(res);
                }
            );
        })
            .then((response) => response)
        return results;
    } catch (error) {
        throw error;
    }
};

exports.retrieveUserGuestByEmail = async (detailObject) => {
    try {
        const results = await new Promise((resolve, reject) => {
            db.query(
                /* sql */ `SELECT * FROM liveclass_booking_tamu WHERE meeting_id = ? AND email = ? AND status = 1;`,
                [detailObject.idBooking, detailObject.emailUser],
                (err, res) => {
                    if (err) reject(err);
                    resolve(res);
                }
            );
        })
            .then((response) => response)
        return results;
    } catch (error) {
        throw error;
    }
};

exports.postUserGuestByEmail = async (detailObject) => {
    try {
        const results = await new Promise((resolve, reject) => {
            db.query(
                /* sql */ `INSERT INTO liveclass_booking_tamu(meeting_id,email,status) VALUES(?);`,
                [[detailObject.idBooking, detailObject.emailUser, 1]],
                (err, res) => {
                    if (err) reject(err);
                    resolve(res);
                }
            );
        })
            .then((response) => response)
        return results;
    } catch (error) {
        throw error;
    }
};

