const db = require('../configs/database');

exports.readTrainingExam = async (req) => {
    try {
        const results = await new Promise((resolve, reject) => {
            db.query(/* sql */`SELECT *,
            (SELECT SUM(total) FROM training_exam_composition WHERE exam_id=?) AS number_of_question
            FROM training_exam
            WHERE id = ?
            LIMIT 1;`, [req.params.id, req.params.id], (err, res) => {
                if (err) reject(err);
                resolve(res);
            })
        }).then(response => response)
            .catch(err => err);
        return results;
    } catch (error) {
        throw error;
    }
}

exports.readComposition = async (req) => {
    try {
        const results = await new Promise((resolve, reject) => {
            db.query(/* sql */`SELECT total, course_id, tag FROM training_exam_composition WHERE exam_id='${req.params.id}'`, (err, res) => {
                if (err) reject(err);
                resolve(res);
            })
        }).then(response => response)
            .catch(err => err);
        return results;
    } catch (error) {
        throw error;
    }
}

exports.retrieveRepoExamByCourse = async (dataRequest) => {
    try {
        const results = await new Promise((resolve, reject) => {
            db.query(
                /* sql */ `SELECT id, title, company_id, course_id FROM training_exam WHERE company_id = ? AND status='Active';`,
                [dataRequest.idCompany], (err, res) => {
                    if (err) reject(err);
                    resolve(res);
                })
        }).then(response => response)
            .catch(err => err);
        return results;
    } catch (error) {
        throw error;
    }
}

exports.readQuestion = async (req) => {
    try {
        const results = await new Promise((resolve, reject) => {
            db.query(
                /* sql */ `SELECT q1.id, c2.id AS category_id, q1.category_id AS subcategory_id, q1.question, q1.answer, q1.sort,q1.type
                FROM training_exam_question q1
                LEFT JOIN training_exam_category c ON c.id = q1.category_id
                LEFT JOIN training_exam_category c2 ON c2.id = c.parent
                WHERE q1.exam_id = ?
                ORDER BY q1.sort ASC;`,
                [req.params.id], (err, res) => {
                    if (err) {
                        reject(err);
                    } else {
                        let tmp = [];
                        if (res.length > 0) {
                            res.forEach((str) => {
                                str.short_answer = false;
                                if (str.type == 2) str.short_answer = true;
                                tmp.push(str);
                            });
                        }
                        resolve(tmp);
                    }

                })
        }).then(response => response)
            .catch(err => err);
        return results;
    } catch (error) {
        throw error;
    }
}

exports.readOptions = async (dataQuestion) => {
    try {
        const results = await new Promise((resolve, reject) => {
            db.query(
                /* sql */ `SELECT * FROM training_exam_option WHERE question_id='${dataQuestion.id}'`, (err, res) => {
                if (err) reject(err);
                resolve(res);
            })
        }).then(response => response)
            .catch(err => err);
        return results;
    } catch (error) {
        throw error;
    }
}

exports.readTemporaryExamResultByIdAssignee = async (dataExam) => {
    try {
        const results = await new Promise((resolve, reject) => {
            db.query(
                /* sql */ `SELECT * FROM temporary_exam_result WHERE training_assignee_id = ?`, [dataExam.idAssignee], (err, res) => {
                if (err) reject(err);
                resolve(res);
            })
        }).then(response => response)
            .catch(err => err);
        return results;
    } catch (error) {
        throw error;
    }
}

exports.retrieveUserMembership = async (dataMembership) => {
    try {
        const results = await new Promise((resolve, reject) => {
            db.query(
                /* sql */ `SELECT * FROM training_membership TM WHERE TM.training_user_id = ? AND TM.status = ?;`, [dataMembership.idTrainingUser, dataMembership.status], (err, res) => {
                if (err) reject(err);
                resolve(res);
            })
        }).then(response => response)
            .catch(err => err);
        return results;
    } catch (error) {
        throw error;
    }
};

exports.retrieveTrainingExamResult = async (dataTrainingExamResult) => {
    try {
        const results = await new Promise((resolve, reject) => {
            db.query(
                /* sql */ `SELECT TER.* FROM training_exam TE LEFT JOIN training_exam_result TER ON TE.id = TER.exam_id WHERE TER.training_user_id = ? AND TE.exam = 1 AND TER.pass = 1 AND TE.generate_membership = 1 AND TER.photo_taken = 0;`, [dataTrainingExamResult.idTrainingUser], (err, res) => {
                if (err) reject(err);
                resolve(res);
            })
        }).then(response => response)
            .catch(err => err);
        return results;
    } catch (error) {
        throw error;
    }
}

exports.createTrainingExam = async (detailObject) => {
    try {
        const results = await new Promise((resolve, reject) => {
            db.query(
                /* sql */ `INSERT INTO training_exam
                (company_id, title, licenses_type_id, time_limit, minimum_score, generate_question, course_id, scheduled, generate_membership, see_correct_answer, multiple_assign, start_time, end_time, exam, repeatable, consume_quota, status, created_by, created_at)
                VALUES (?);`,
                [[detailObject.company_id, detailObject.title, detailObject.licenses_type_id, detailObject.time_limit, detailObject.minimum_score, detailObject.generate_question, detailObject.course_id ? detailObject.course_id : 0, detailObject.scheduled, detailObject.generate_membership, detailObject.see_correct_answer, detailObject.multiple_assign, detailObject.start_time, detailObject.end_time, detailObject.exam, detailObject.repeatable, detailObject.consume_quota, 'Active', detailObject.created_by, new Date()]], (err, res) => {
                    if (err) reject(err);
                    resolve(res);
                })
        }).then(response => response)
            .catch(err => err);
        return results;
    } catch (error) {
        throw error;
    }
}

exports.createTrainingExamComposition = async (detailObject) => {
    try {
        const results = await new Promise((resolve, reject) => {
            db.query(
                /* sql */ `INSERT INTO training_exam_composition (exam_id, course_id, total, tag) VALUES(?);`,
                [[detailObject.idExam, detailObject.course_id, detailObject.total, detailObject.tag]], (err, res) => {
                    if (err) reject(err);
                    resolve(res);
                })

        }).then(response => response)
            .catch(err => err);
        return results;
    } catch (error) {
        throw error;
    }
}

exports.retrieveQuestionByExamIdAndQuestionAndAnswer = async (detailObject) => {
    try {
        const results = await new Promise((resolve, reject) => {
            db.query(
                /* sql */ `SELECT * FROM training_exam_question TEQ WHERE TEQ.exam_id = ? AND TEQ.question = ? AND TEQ.answer = ? AND TEQ.sort = ?;`,
                [detailObject.idExam, detailObject.question, detailObject.answer, detailObject.sort], (err, res) => {
                    if (err) reject(err);
                    resolve(res);
                })
        }).then(response => response)
            .catch(err => err);
        return results;
    } catch (error) {
        throw error;
    }
}

