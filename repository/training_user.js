const db = require("../configs/database");

exports.updateStatusBulkTrainingUser = async (dataDetail) => {
  try {
    const results = await new Promise((resolve, reject) => {
      db.query(
        `UPDATE training_user
            SET status= ?
            WHERE id IN (?);`,
        [dataDetail.status, dataDetail.dataIdUser],
        (err, res) => {
          if (err) reject(err);
          resolve(res);
        }
      );
    }).then((response) => response);

    return results;
  } catch (error) {
    throw error;
  }
};

exports.updateStatusBulkUser = async (dataDetail) => {
  try {
    const results = await new Promise((resolve, reject) => {
      db.query(
        `UPDATE user
            INNER JOIN training_user ON user.email = training_user.email
            SET user.status = ?
            WHERE training_user.id IN (?);`,
        [dataDetail.status, dataDetail.dataIdUser],
        (err, res) => {
          if (err) reject(err);
          resolve(res);
        }
      );
    }).then((response) => response);

    return results;
  } catch (error) {
    throw error;
  }
};

exports.updateStatusBulkMembershipUser = async (dataDetail) => {
  try {
    const results = await new Promise((resolve, reject) => {
      db.query(
        `UPDATE training_membership
            SET status=? 
            WHERE training_user_id IN (?);`,
        [dataDetail.status, dataDetail.dataIdUser],
        (err, res) => {
          if (err) reject(err);
          resolve(res);
        }
      );
    }).then((response) => response);

    return results;
  } catch (error) {
    throw error;
  }
};

exports.getSourceDataTrainingUser = async (dataDetail) => {
  try {
    const results = await new Promise((resolve, reject) => {
      db.query(
        `SELECT source, name, initial_password, email, phone FROM training_user WHERE id IN (?)`,
        [dataDetail.dataIdUser],
        (err, res) => {
          if (err) reject(err);
          resolve(res);
        }
      );
    }).then((response) => response);

    return results;
  } catch (error) {
    throw error;
  }
};

exports.createFormRegistrationUser = async (detailObject) => {
  try {
    const results = await new Promise((resolve, reject) => {
      db.query(
        /* sql */ `INSERT INTO training_registration_list
                (training_company_id, form_id, data, status, period)
                VALUES (?);`,
        [
          [
            detailObject.idTrainingCompany,
            detailObject.idForm,
            detailObject.data,
            1,
            detailObject.period
          ],
        ],
        (err, res) => {
          if (err) reject(err);
          resolve(res);
        }
      );
    }).then((response) => response);
    return results;
  } catch (error) {
    throw error;
  }
};

exports.readRegistrationUserByTrainingCompany = async (detailObject) => {
  try {
    const results = await new Promise((resolve, reject) => {
      db.query(
        /* sql */ `SELECT
	trl.id,
	trl.training_company_id,
	trl.form_id,
	trl.data,
	trl.status,
	tc.name AS training_company_name,
  trl.payment_status,
  trl.payment_id
FROM
	training_registration_list trl
LEFT JOIN training_company tc ON trl.training_company_id = tc.id
WHERE
	trl.training_company_id = ?
	AND trl.status = 1`,
        [detailObject.idTrainingCompany],
        (err, res) => {
          if (err) reject(err);
          resolve(res);
        }
      );
    }).then((response) => response);
    return results;
  } catch (error) {
    throw error;
  }
};

exports.readDetailRegistrationUserByIdTraining = async (detailObject) => {
  try {
    const results = await new Promise((resolve, reject) => {
      db.query(
        /* sql */ `
        SELECT
        trl.id,
        trl.training_company_id,
        trl.form_id,
        trl.data,
        trl.status,
        tc.name AS training_company_name
        FROM
        training_user tu
        LEFT JOIN training_company tc ON tc.id = tu.training_company_id
        LEFT JOIN training_registration_list trl ON trl.training_company_id = tc.id
        WHERE
        tu.id = ? AND 
        trl.data LIKE CONCAT('%', tu.email ,'%') AND 
        trl.status = 0
        ORDER BY trl.id DESC 
        LIMIT 1;
        `,
        [detailObject.idTrainingUser],
        (err, res) => {
          if (err) reject(err);
          resolve(res);
        }
      );
    }).then((response) => response);
    return results;
  } catch (error) {
    throw error;
  }
};

exports.readAllRegistrationUserBySuperAdmin = async (detailObject) => {
  try {
    const results = await new Promise((resolve, reject) => {
      db.query(
        /* sql */ `	SELECT
	trl.id,
	trl.training_company_id,
	trl.form_id,
	trl.data,
	trl.status,
	tc.name AS training_company_name,
  IF(trs.is_paid='0',0,1) as is_paid,
  trl.created_at,
  trl.payment_status,
  trl.payment_id
FROM
	training_registration_list trl
LEFT JOIN training_company tc ON trl.training_company_id = tc.id
left join training_registration_schema trs on trs.id = trl.form_id
WHERE
trl.training_company_id IN (
	SELECT
		id 
	FROM
		training_company tc
	WHERE
	company_id = ?) 
	AND trl.status = 1
  ORDER BY trl.created_at DESC;`,
        [detailObject.idCompany],
        (err, res) => {
          if (err) reject(err);
          resolve(res);
        }
      );
    }).then((response) => response);
    return results;
  } catch (error) {
    throw error;
  }
};

exports.retrieveUserRegistrationDetailById = async (detailObject) => {
  try {
    const results = await new Promise((resolve, reject) => {
      db.query(
        /* sql */ `SELECT id, training_company_id, form_id, data, status FROM training_registration_list WHERE id IN (?) AND status = 1`,
        [detailObject.idUserRegistration],
        (err, res) => {
          if (err) reject(err);
          resolve(res);
        }
      );
    }).then((response) => response);
    return results;
  } catch (error) {
    throw error;
  }
};

exports.updateStatusUserRegistrationById = async (detailObject) => {
  try {
    const results = await new Promise((resolve, reject) => {
      db.query(
        /* sql */ `UPDATE training_registration_list SET status = 0 WHERE id = ?; `,
        [detailObject.idUserRegistration],
        (err, res) => {
          if (err) reject(err);
          resolve(res);
        }
      );
    }).then((response) => response);
    return results;
  } catch (error) {
    throw error;
  }
};


exports.countUserRegistrationByIdFormAndStatus = async (detailObject) => {
  try {
    const results = await new Promise((resolve, reject) => {
      db.query(
        /* sql */ `SELECT COUNT(*) AS count_user FROM training_registration_list trl WHERE trl.form_id = ? AND status = ? AND period = ?;`,
        [detailObject.idForm, detailObject.status, detailObject.period],
        (err, res) => {
          if (err) reject(err);
          resolve(res);
        }
      );
    }).then((response) => response);
    return results;
  } catch (error) {
    throw error;
  }
};

exports.updateStatusToRemovedUserRegistration = async (detailObject) => {
  try {
    const results = await new Promise((resolve, reject) => {
      db.query(
        /* sql */ `UPDATE training_registration_list SET status = 2 WHERE id IN (?); `,
        [detailObject.idUserRegistration],
        (err, res) => {
          if (err) reject(err);
          resolve(res);
        }
      );
    }).then((response) => response);
    return results;
  } catch (error) {
    throw error;
  }
};