const db = require('../configs/database');
const moment = require("moment");

exports.retrieveIdTrainingCompany = async (dataDetail) => {
    try {
        const results = await new Promise((resolve, reject) => {
            let queryString = `SELECT * FROM training_company WHERE company_id = ?`;
            if (dataDetail.grupName === "Admin Training") {
                queryString = `SELECT * FROM training_company WHERE id = ?`;
            }
            db.query(queryString,
                [dataDetail.idCompany],
                (err, res) => {
                    if (err) reject(err);
                    resolve(res);
                }
            );
        })
            .then((response) => response)

        return results;
    } catch (error) {
        throw error;
    }
};

exports.retrieveDataTrainingUserByEmail = async (dataDetail) => {
    try {
        const results = await new Promise((resolve, reject) => {
            db.query(
                /* sql */ `SELECT * FROM training_user tu WHERE tu.email = ? AND tu.status = 'active' AND tu.level = 'admin';`,
                [dataDetail.email],
                (err, res) => {
                    if (err) reject(err);
                    resolve(res);
                }
            );
        })
            .then((response) => response)

        return results;
    } catch (error) {
        throw error;
    }
};

exports.retrieveTotalTrainingUser = async (dataDetail) => {
    try {
        const results = await new Promise((resolve, reject) => {
            db.query(
                /* sql */ `SELECT COUNT(*) AS totalTrainingUser FROM training_user tu WHERE tu.training_company_id = ? AND tu.status = 'active' AND tu.level = 'user';`,
                [dataDetail.idTrainingCompany],
                (err, res) => {
                    if (err) reject(err);
                    resolve(res);
                }
            );
        })
            .then((response) => response)

        return results;
    } catch (error) {
        throw error;
    }
};

exports.retrieveTotalExamCompany = async (dataDetail) => {
    try {
        const results = await new Promise((resolve, reject) => {
            let form = dataDetail;
            let queryFilterLicenses = `AND e.licenses_type_id IN (${String(
              form.licenses_type
            )})`;
            let queryFilterTrainingCompany = `AND u.training_company_id IN (${String(
              form.training_company
            )})`;
            let queryFilterPass = `AND r.pass IN (${String(form.pass)})`;
            let queryFilterCert = `AND (r.certificate_status IN ('${form.cert.join(
              `','`
            )}') ${form.cert.filter(item => item === "No").length
              ? `OR r.certificate_status IS NULL)`
              : ")"
              } `;
            db.query(
                /* sql */ `SELECT SUM(r.score) AS totalScore, COUNT(r.training_user_id) AS totalUserDoExam, MAX(r.created_at) AS last_updated
                FROM training_exam_result r
                LEFT JOIN training_exam e ON e.id = r.exam_id
                LEFT JOIN training_user u ON u.id = r.training_user_id
                LEFT JOIN training_company tc ON tc.id = u.training_company_id
                WHERE tc.company_id=?
                AND r.created_at BETWEEN '${moment(new Date(form.start)).format(
                    "YYYY-MM-DD"
                  )}' AND '${moment(new Date(form.end)).format("YYYY-MM-DD")} 23:59:59'
                      AND e.company_id='${form.company}'
                      ${form.licenses_type.length ? queryFilterLicenses : ""}
                      ${form.training_company.length ? queryFilterTrainingCompany : ""}
                      ${form.pass.length ? queryFilterPass : ""}
                      ${form.cert.length ? queryFilterCert : ""}
                ;
                SELECT COUNT(r.id) as totalExamCompany
                FROM training_exam_result r
                LEFT JOIN training_exam e ON e.id = r.exam_id
                LEFT JOIN training_user u ON u.id = r.training_user_id
                LEFT JOIN training_company tc ON tc.id = u.training_company_id
                WHERE tc.company_id=?
                AND r.created_at BETWEEN '${moment(new Date(form.start)).format(
                    "YYYY-MM-DD"
                  )}' AND '${moment(new Date(form.end)).format("YYYY-MM-DD")} 23:59:59'
                      AND e.company_id='${form.company}'
                      ${form.licenses_type.length ? queryFilterLicenses : ""}
                      ${form.training_company.length ? queryFilterTrainingCompany : ""}
                      ${form.pass.length ? queryFilterPass : ""}
                      ${form.cert.length ? queryFilterCert : ""}
                ;
                SELECT COUNT(*) AS totalUser FROM
                (
                    SELECT COUNT(r.training_user_id) AS totalUser
                    FROM training_exam_result r
                    LEFT JOIN training_exam e ON e.id = r.exam_id
                    LEFT JOIN training_user u ON u.id = r.training_user_id
                    LEFT JOIN training_company tc ON tc.id = u.training_company_id
                    WHERE tc.company_id=?
                    AND r.created_at BETWEEN '${moment(new Date(form.start)).format(
                        "YYYY-MM-DD"
                    )}' AND '${moment(new Date(form.end)).format("YYYY-MM-DD")} 23:59:59'
                        AND e.company_id='${form.company}'
                        ${form.licenses_type.length ? queryFilterLicenses : ""}
                        ${form.training_company.length ? queryFilterTrainingCompany : ""}
                        ${form.pass.length ? queryFilterPass : ""}
                        ${form.cert.length ? queryFilterCert : ""}
                    GROUP BY r.training_user_id
                ) AS result
                ;`,
                [dataDetail.idCompany, dataDetail.idCompany, dataDetail.idCompany],
                (err, res) => {
                    if (err) reject(err);
                    resolve(res);
                }
            );
        })
            .then((response) => response);
        const data = {
            lastUpdated: results[0][0].last_updated || '--',
            totalScore: results[0][0].totalScore || 0,
            totalUserDoExam: results[0][0].totalUserDoExam || 0,
            totalExamCompany: results[1][0].totalExamCompany || 0,
            totalUser: results[2][0].totalUser || 0
        }
        return data;
    } catch (error) {
        throw error;
    }
};

exports.retrieveTotalExamTrainingCompany = async (dataDetail) => {
    try {
        const results = await new Promise((resolve, reject) => {
            let form = dataDetail;
            let queryFilterLicenses = `AND e.licenses_type_id IN (${String(
              form.licenses_type
            )})`;
            let queryFilterTrainingCompany = `AND u.training_company_id IN (${String(
              form.training_company
            )})`;
            let queryFilterPass = `AND r.pass IN (${String(form.pass)})`;
            let queryFilterCert = `AND (r.certificate_status IN ('${form.cert.join(
              `','`
            )}') ${form.cert.filter(item => item === "No").length
              ? `OR r.certificate_status IS NULL)`
              : ")"
              } `;
            db.query(
                /* sql */ `
                SELECT SUM(r.score) AS totalScore, COUNT(r.training_user_id) AS totalUserDoExam, MAX(r.created_at) AS last_updated
                FROM training_exam_result r
                LEFT JOIN training_exam e ON e.id = r.exam_id
                LEFT JOIN training_user u ON u.id = r.training_user_id
                WHERE u.training_company_id=?
                AND r.created_at BETWEEN '${moment(new Date(form.start)).format(
                    "YYYY-MM-DD"
                  )}' AND '${moment(new Date(form.end)).format("YYYY-MM-DD")} 23:59:59'
                      AND e.company_id='${form.company}'
                      ${form.licenses_type.length ? queryFilterLicenses : ""}
                      ${form.training_company.length ? queryFilterTrainingCompany : ""}
                      ${form.pass.length ? queryFilterPass : ""}
                      ${form.cert.length ? queryFilterCert : ""}
                ;
                SELECT COUNT(r.id) as totalExamCompany
                FROM training_exam_result r
                LEFT JOIN training_exam e ON e.id = r.exam_id
                LEFT JOIN training_user u ON u.id = r.training_user_id
                WHERE u.training_company_id=?
                AND r.created_at BETWEEN '${moment(new Date(form.start)).format(
                    "YYYY-MM-DD"
                  )}' AND '${moment(new Date(form.end)).format("YYYY-MM-DD")} 23:59:59'
                      AND e.company_id='${form.company}'
                      ${form.licenses_type.length ? queryFilterLicenses : ""}
                      ${form.training_company.length ? queryFilterTrainingCompany : ""}
                      ${form.pass.length ? queryFilterPass : ""}
                      ${form.cert.length ? queryFilterCert : ""}
                ;
                SELECT COUNT(*) AS totalUser FROM
                (
                    SELECT COUNT(r.training_user_id) AS totalUser
                    FROM training_exam_result r
                    LEFT JOIN training_exam e ON e.id = r.exam_id
                    LEFT JOIN training_user u ON u.id = r.training_user_id
                    LEFT JOIN training_company tc ON tc.id = u.training_company_id
                    WHERE u.training_company_id=?
                    AND r.created_at BETWEEN '${moment(new Date(form.start)).format(
                        "YYYY-MM-DD"
                    )}' AND '${moment(new Date(form.end)).format("YYYY-MM-DD")} 23:59:59'
                        AND e.company_id='${form.company}'
                        ${form.licenses_type.length ? queryFilterLicenses : ""}
                        ${form.training_company.length ? queryFilterTrainingCompany : ""}
                        ${form.pass.length ? queryFilterPass : ""}
                        ${form.cert.length ? queryFilterCert : ""}
                    GROUP BY r.training_user_id
                ) AS result
                ;`,
                [dataDetail.idTrainingCompany, dataDetail.idTrainingCompany, dataDetail.idTrainingCompany],
                (err, res) => {
                    if (err) reject(err);
                    resolve(res);
                }
            );
        })
            .then((response) => response);
        const data = {
            lastUpdated: results[0][0].last_updated || '--',
            totalScore: results[0][0].totalScore || 0,
            totalUserDoExam: results[0][0].totalUserDoExam || 0,
            totalExamCompany: results[1][0].totalExamCompany || 0,
            totalUser: results[2][0].totalUser || 0
        }
        return data;
    } catch (error) {
        throw error;
    }
};

exports.retrieveUserPassedByIdTrainingCompany = async (dataDetail) => {
    try {
        const results = await new Promise((resolve, reject) => {
            let form = dataDetail;
            let queryFilterLicenses = `AND e.licenses_type_id IN (${String(
              form.licenses_type
            )})`;
            let queryFilterTrainingCompany = `AND u.training_company_id IN (${String(
              form.training_company
            )})`;
            let queryFilterPass = `AND r.pass IN (${String(form.pass)})`;
            let queryFilterCert = `AND (r.certificate_status IN ('${form.cert.join(
              `','`
            )}') ${form.cert.filter(item => item === "No").length
              ? `OR r.certificate_status IS NULL)`
              : ")"
              } `;
            db.query(
                /* sql */ `SELECT COUNT(r.id) as TotalPassedExam
                FROM training_exam_result r
                LEFT JOIN training_exam e ON e.id = r.exam_id
                LEFT JOIN training_user u ON u.id = r.training_user_id
                WHERE r.pass=1 AND u.training_company_id=?
                AND r.created_at BETWEEN '${moment(new Date(form.start)).format(
                    "YYYY-MM-DD"
                  )}' AND '${moment(new Date(form.end)).format("YYYY-MM-DD")} 23:59:59'
                      AND e.company_id='${form.company}'
                      ${form.licenses_type.length ? queryFilterLicenses : ""}
                      ${form.training_company.length ? queryFilterTrainingCompany : ""}
                      ${form.pass.length ? queryFilterPass : ""}
                      ${form.cert.length ? queryFilterCert : ""}
                ;`,
                [dataDetail.idTrainingCompany],
                (err, res) => {
                    if (err) reject(err);
                    resolve(res);
                }
            );
        })
            .then((response) => response);
        return results;
    } catch (error) {
        throw error;
    }
};

exports.retrieveUserPassedByIdCompany = async (dataDetail) => {
    try {
        const results = await new Promise((resolve, reject) => {
            let form = dataDetail;
            let queryFilterLicenses = `AND e.licenses_type_id IN (${String(
              form.licenses_type
            )})`;
            let queryFilterTrainingCompany = `AND u.training_company_id IN (${String(
              form.training_company
            )})`;
            let queryFilterPass = `AND r.pass IN (${String(form.pass)})`;
            let queryFilterCert = `AND (r.certificate_status IN ('${form.cert.join(
              `','`
            )}') ${form.cert.filter(item => item === "No").length
              ? `OR r.certificate_status IS NULL)`
              : ")"
              } `;
            db.query(
                /* sql */ `SELECT COUNT(r.id) as TotalPassedExam
                FROM training_exam_result r
                LEFT JOIN training_exam e ON e.id = r.exam_id
                LEFT JOIN training_user u ON u.id = r.training_user_id
                LEFT JOIN training_company tc ON tc.id = u.training_company_id
                WHERE r.pass=1 AND tc.company_id=?
                AND r.created_at BETWEEN '${moment(new Date(form.start)).format(
                    "YYYY-MM-DD"
                  )}' AND '${moment(new Date(form.end)).format("YYYY-MM-DD")} 23:59:59'
                      AND e.company_id='${form.company}'
                      ${form.licenses_type.length ? queryFilterLicenses : ""}
                      ${form.training_company.length ? queryFilterTrainingCompany : ""}
                      ${form.pass.length ? queryFilterPass : ""}
                      ${form.cert.length ? queryFilterCert : ""}
                ;`,
                [dataDetail.idCompany],
                (err, res) => {
                    if (err) reject(err);
                    resolve(res);
                }
            );
        })
            .then((response) => response);
        return results;
    } catch (error) {
        throw error;
    }
};

exports.retrieveUserScoreExam = async (dataDetail) => {
    try {
        const results = await new Promise((resolve, reject) => {
            db.query(
                /* sql */ `SELECT score, created_at FROM training_exam_result ter WHERE ter.exam_id = ?;`,
                [dataDetail.idExam],
                (err, res) => {
                    if (err) reject(err);
                    resolve(res);
                }
            );
        })
            .then((response) => response);
        return results;
    } catch (error) {
        throw error;
    }
};

exports.retrieveAverageTimeExamTraining = async (dataDetail) => {
    try {
        const results = await new Promise((resolve, reject) => {
            let form = dataDetail;
            let queryFilterLicenses = `AND e.licenses_type_id IN (${String(
              form.licenses_type
            )})`;
            let queryFilterTrainingCompany = `AND u.training_company_id IN (${String(
              form.training_company
            )})`;
            let queryFilterPass = `AND r.pass IN (${String(form.pass)})`;
            let queryFilterCert = `AND (r.certificate_status IN ('${form.cert.join(
              `','`
            )}') ${form.cert.filter(item => item === "No").length
              ? `OR r.certificate_status IS NULL)`
              : ")"
              } `;
            db.query(
                /* sql */ `SELECT SEC_TO_TIME(ROUND(AVG(TIME_TO_SEC(timediff(rc.submission_time,rc.start_time))), 0)) AS AverageTime
                FROM training_exam_record rc
                LEFT JOIN training_exam_result r ON r.id = rc.exam_result_id
                LEFT JOIN training_exam e ON e.id = r.exam_id
                LEFT JOIN training_user u ON u.id = r.training_user_id
                WHERE u.training_company_id=?
                AND r.created_at BETWEEN '${moment(new Date(form.start)).format(
                    "YYYY-MM-DD"
                  )}' AND '${moment(new Date(form.end)).format("YYYY-MM-DD")} 23:59:59'
                      AND e.company_id='${form.company}'
                      ${form.licenses_type.length ? queryFilterLicenses : ""}
                      ${form.training_company.length ? queryFilterTrainingCompany : ""}
                      ${form.pass.length ? queryFilterPass : ""}
                      ${form.cert.length ? queryFilterCert : ""}
                ;`,
                [dataDetail.idTrainingCompany],
                (err, res) => {
                    if (err) reject(err);
                    resolve(res);
                }
            );
        })
            .then((response) => response);
        return results;
    } catch (error) {
        throw error;
    }
};

exports.retrieveAverageTimeExamCompany = async (dataDetail) => {
    try {
        const results = await new Promise((resolve, reject) => {
            let form = dataDetail;
            let queryFilterLicenses = `AND e.licenses_type_id IN (${String(
              form.licenses_type
            )})`;
            let queryFilterTrainingCompany = `AND u.training_company_id IN (${String(
              form.training_company
            )})`;
            let queryFilterPass = `AND r.pass IN (${String(form.pass)})`;
            let queryFilterCert = `AND (r.certificate_status IN ('${form.cert.join(
              `','`
            )}') ${form.cert.filter(item => item === "No").length
              ? `OR r.certificate_status IS NULL)`
              : ")"
              } `;
            db.query(
                /* sql */ `SELECT SEC_TO_TIME(ROUND(AVG(TIME_TO_SEC(timediff(rc.submission_time,rc.start_time))), 0)) AS AverageTime
                FROM training_exam_record rc
                LEFT JOIN training_exam_result r ON r.id = rc.exam_result_id
                LEFT JOIN training_exam e ON e.id = r.exam_id
                LEFT JOIN training_user u ON u.id = r.training_user_id
                LEFT JOIN training_company tc ON tc.id = u.training_company_id
                WHERE tc.company_id=?
                AND r.created_at BETWEEN '${moment(new Date(form.start)).format(
                    "YYYY-MM-DD"
                  )}' AND '${moment(new Date(form.end)).format("YYYY-MM-DD")} 23:59:59'
                      AND e.company_id='${form.company}'
                      ${form.licenses_type.length ? queryFilterLicenses : ""}
                      ${form.training_company.length ? queryFilterTrainingCompany : ""}
                      ${form.pass.length ? queryFilterPass : ""}
                      ${form.cert.length ? queryFilterCert : ""}
                ;`,
                [dataDetail.idCompany],
                (err, res) => {
                    if (err) reject(err);
                    resolve(res);
                }
            );
        })
            .then((response) => response);
        return results;
    } catch (error) {
        throw error;
    }
};


exports.retrieveHistoryUserTrainingExam = async (dataDetail) => {
    try {
        const results = await new Promise((resolve, reject) => {
            db.query(
                /* sql */ `SELECT r.*,
                IF(rec.start_time IS NULL, a.start_time, rec.start_time) AS start_time,
                IF(rec.submission_time IS NULL, a.submission_time, rec.submission_time) AS submission_time,
                SEC_TO_TIME(TIMESTAMPDIFF(SECOND, IF(rec.start_time IS NULL, a.start_time, rec.start_time), IF(rec.submission_time IS NULL, a.submission_time, rec.submission_time))) AS work_time,
                u.name,
                u.email,
                c.title AS course_name,
                e.title AS exam_name,
                (
                        CASE
                    WHEN e.exam = 1 && e.status = 'Inactive' THEN 'Inactive Exam'
                    WHEN e.exam = 0 && e.status = 'Inactive' THEN 'Inactive Quiz'
                    WHEN e.exam = 1 && e.status = 'Active' THEN 'Exam'
                    WHEN e.exam = 0 && e.status = 'Active' THEN 'Quiz'
                                ELSE 'Exam'
                    END
                ) AS exam_type,
                e.minimum_score,
                lt.name AS licenses_type,
                tc.name AS training_company,
                u.license_number
                FROM training_exam_result r
                LEFT JOIN training_exam_assignee a ON a.id = r.assignee_id
                LEFT JOIN training_user u ON u.id = r.training_user_id
                LEFT JOIN training_company tc ON tc.id = u.training_company_id
                LEFT JOIN training_exam e ON e.id = r.exam_id
                LEFT JOIN training_licenses_type lt ON lt.id = e.licenses_type_id
                LEFT JOIN training_course c ON c.id = e.course_id
                LEFT JOIN training_exam_record rec ON rec.exam_result_id = r.id
                WHERE r.training_user_id = ?;`,
                [dataDetail.idTrainingUser],
                (err, res) => {
                    if (err) reject(err);
                    resolve(res);
                }
            );
        })
            .then((response) => response);
        return results;
    } catch (error) {
        throw error;
    }
};

