const db = require('../configs/database');

exports.getListAllCompany = async () => {
    try {
        const results = await new Promise((resolve, reject) => {
            db.query(
                /* sql */ `SELECT
                    c.* 
                FROM
                    company c
                WHERE STATUS = 'active';`,
                [],
                (err, res) => {
                    if (err) reject(err);
                    resolve(res);
                }
            );
        })
            .then((response) => response)
        return results;
    } catch (error) {
        throw error;
    }
};

exports.getListAllGlobalSettingsTemplate = async () => {
    try {
        const results = await new Promise((resolve, reject) => {
            db.query(
                /* sql */ `SELECT
                    gst.* 
                FROM
                    global_settings_template gst;`,
                [],
                (err, res) => {
                    if (err) reject(err);
                    resolve(res);
                }
            );
        })
            .then((response) => response)
        return results;
    } catch (error) {
        throw error;
    }
};

exports.insertGlobalSettings = async (detailInsertGlobalSettings) => {
    try {
        const results = await new Promise((resolve, reject) => {
            db.query(
                /* sql */ `INSERT INTO global_setting (company_id, status, id_global_settings_template)
                    VALUES (?,?,?);`,
                [
                    detailInsertGlobalSettings.idCompany,
                    detailInsertGlobalSettings.status,
                    detailInsertGlobalSettings.idGlobalSettingsTemplate,
                ],
                (err, res) => {
                    if (err) reject(err);
                    resolve(res);
                }
            );
        })
            .then((response) => response)
        return results;
    } catch (error) {
        throw error;
    }
};

exports.insertGlobalSettingsTemplate = async (detailInsertGlobalSettingsTemplate) => {
    try {
        const results = await new Promise((resolve, reject) => {
            db.query(
                /* sql */ `INSERT INTO global_settings_template (sub, role, code, name, status)
                    VALUES (?,?,?,?,?);`,
                [
                    detailInsertGlobalSettingsTemplate.sub,
                    detailInsertGlobalSettingsTemplate.role,
                    detailInsertGlobalSettingsTemplate.code,
                    detailInsertGlobalSettingsTemplate.name,
                    detailInsertGlobalSettingsTemplate.status,
                ],
                (err, res) => {
                    if (err) reject(err);
                    resolve(res);
                }
            );
        })
            .then((response) => response)
        return results;
    } catch (error) {
        throw error;
    }
};

exports.retrieveSettingsTemplate = async (dataSettingsTemplate) => {
    try {
        const results = await new Promise((resolve, reject) => {
            db.query(
                /* sql */ `SELECT
                    gst.* 
                FROM
                    global_settings_template gst
                WHERE gst.code = ? AND gst.role = ?;`,
                [dataSettingsTemplate.code, dataSettingsTemplate.role],
                (err, res) => {
                    if (err) reject(err);
                    resolve(res);
                }
            );
        })
            .then((response) => response)
        return results;
    } catch (error) {
        throw error;
    }
};

exports.removeListAllGlobalSettings = async () => {
    try {
        const results = await new Promise((resolve, reject) => {
            db.query(
                /* sql */ `TRUNCATE global_setting;`,
                [],
                (err, res) => {
                    if (err) reject(err);
                    resolve(res);
                }
            );
        })
            .then((response) => response)
        return results;
    } catch (error) {
        throw error;
    }
};

exports.retrieveCheckAccessByRole = async (detailAccess) => {
    try {
        const results = await new Promise((resolve, reject) => {
            db.query(
                /* sql */ `SELECT gst.code, (IF (g.status IS NULL, gst.status, g.status)) AS status FROM global_setting g LEFT JOIN global_settings_template gst ON gst.id_access = g.id_global_settings_template JOIN company c ON c.company_id = g.company_id WHERE g.company_id = ? AND gst.role = ? AND code IN (?);`,
                [detailAccess.company_id, detailAccess.role, detailAccess.code],
                (err, res) => {
                    if (err) reject(err);
                    resolve(res);
                }
            );
        })
            .then((response) => response)
        return results;
    } catch (error) {
        throw error;
    }
};

exports.retrieveCheckAccessBySub = async (detailAccess) => {
    try {
        const results = await new Promise((resolve, reject) => {

            const queryString = `
            SELECT gst.role ,gst.sub, gst.code, (IF (g.status IS NULL, gst.status, g.status)) AS status 
            FROM global_setting g 
            LEFT JOIN global_settings_template gst ON gst.id_access = g.id_global_settings_template 
            JOIN company c ON c.company_id = g.company_id 
            WHERE g.company_id = ? AND gst.sub in(?);`;

            const params = [detailAccess.company_id, detailAccess.sub];

            db.query(queryString, params,
                (err, res) => {
                    if (err) reject(err);
                    resolve(res);
                }
            );
        })
            .then((response) => response)
        return results;
    } catch (error) {
        throw error;
    }
};

