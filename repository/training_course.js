const { response } = require('express');
const db = require('../configs/database');
const axios = require('axios');
var env = require('../env.json');

exports.getScoreWebinar = async (detailWebinar) => {
    /**
     * Detail Webinar
     * idWebinar : number
     * Type Test [1 | 0]: Post Test / Pre Test
     * idUser : number
     */
    try {
        const results = await new Promise((resolve, reject) => {
            db.query(
                /* sql */ `SELECT 
                b.id, b.pertanyaan, c.options, c.answer, d.options AS jawaban, b.jawab AS jawaban_benar
                FROM webinar_test_answer a
                INNER JOIN webinar_test_options d
                ON a.webinar_options = d.id
                INNER JOIN webinar_test_questions b
                ON a.webinar_questions = b.id
                INNER JOIN webinar_test_options c
                ON b.id = c.webinar_test_id
                WHERE b.webinar_id = ?
                AND b.jenis = ?
                AND a.user_id = ?;`,
                [detailWebinar.idWebinar, detailWebinar.typeTest, detailWebinar.idUser],
                (err, res) => {
                    if (err) reject(err);

                    if (res.length) {
                        let hasil = res.reduce((accumulator, current) => {
                            if (!accumulator[current.id]) {
                                accumulator[current.id] = {};
                                accumulator[current.id]['pertanyaan'] = current.pertanyaan;
                                accumulator[current.id]['jawaban'] = current.jawaban;
                                accumulator[current.id]['jawaban_benar'] = current.jawaban_benar;
                                accumulator[current.id]['options'] = [];
                            }

                            accumulator[current.id]['options'].push({
                                options: current.options,
                                answer: current.answer,
                            });

                            return accumulator;
                        }, {});

                        let benar = 0;
                        let salah = 0;
                        let arr = [];
                        Object.keys(hasil).map((e) => {
                            if (hasil[e].jawaban === hasil[e].jawaban_benar) {
                                benar++;
                            } else {
                                salah++;
                            }

                            arr.push(hasil[e]);
                        });
                        resolve((benar / (benar + salah)) * 100);
                    } else {
                        // Not Having Pretest
                        resolve(0);
                    }
                }
            );
        })
            .then((response) => response)

        return results;
    } catch (error) {
        throw error;
    }
};

exports.getLiveClass = async (detailEssay) => {
    /**
     * Detail Live Class
     * email : email
     * userId : id User
     * idTrainingCourse : number
     */

    /**
     * OLD Query
     * SELECT w.*, IF((SELECT p.id FROM webinar_peserta p LEFT JOIN user u ON u.user_id = p.user_id WHERE u.email=? AND p.webinar_id=w.id) IS NOT NULL, 1, 0) AS is_invited, 'Live Class' AS type,
                (IF((SELECT COUNT(p.id) AS hadir FROM webinar_peserta p WHERE p.webinar_id=w.id AND p.user_id=? AND p.status='2')>0,1,0)) AS is_read,
                -- IF (wea.score IS NULL, 0, wea.score) AS score
                FROM webinars w
                -- LEFT JOIN webinar_essay_answer wea ON wea.webinar_id = w.id
                WHERE w.training_course_id=? 
                -- AND wea.user_id=? 
                AND w.publish='1'
                ORDER BY w.id DESC;
     */
    try {
        const results = await new Promise((resolve, reject) => {
            db.query(
                /* sql */ `SELECT w.*, IF((SELECT p.id FROM webinar_peserta p LEFT JOIN user u ON u.user_id = p.user_id WHERE u.email=? AND p.webinar_id=w.id ORDER BY id DESC LIMIT 1) IS NOT NULL, 1, 0) AS is_invited, 'Live Class' AS type,
                (IF((SELECT COUNT(p.id) AS hadir FROM webinar_peserta p WHERE p.webinar_id=w.id AND p.user_id=? AND p.status='2')>0,1,0)) AS is_read
                FROM webinars w
                WHERE w.training_course_id=? 
                AND w.publish='1'
                ORDER BY w.id DESC;`,
                [detailEssay.email, detailEssay.userId, detailEssay.idTrainingCourse, detailEssay.userId],
                (err, res) => {
                    if (err) reject(err);
                    resolve(res);
                }
            );
        })
            .then((response) => response)
        return results;
    } catch (error) {
        throw error;
    }
};

exports.getScoreLiveClass = async (detailEssay) => {
    /**
     * Detail Live Class
     * email : email
     * userId : id User
     * idTrainingCourse : number
     */
    try {
        const results = await new Promise((resolve, reject) => {
            db.query(
                /* sql */ `SELECT CONVERT(score, CHAR) AS score FROM webinar_essay_answer wea WHERE wea.webinar_id = ? AND wea.user_id = ?;`,
                [detailEssay.idWebinar, detailEssay.idUser],
                (err, res) => {
                    if (err) reject(err);
                    resolve(res);
                }
            );
        })
            .then((response) => response)
        return results;
    } catch (error) {
        throw error;
    }
};



exports.getUserTraining = async (detailUserTraining) => {
    /**
     * Detail User Training
     * id Training User: number
     */
    try {
        const results = await new Promise((resolve, reject) => {
            db.query(
                /* sql */ `SELECT * FROM training_user WHERE id=? AND status='active' LIMIT 1;`,
                [detailUserTraining.idTrainingUser],
                (err, res) => {
                    if (err) reject(err);
                    resolve(res);
                }
            );
        })
            .then((response) => response)

        return results;
    } catch (error) {
        throw error;
    }
};

exports.getDataUser = async (detailDataUser) => {
    /**
     * Detail User Training
     * email: email
     */
    try {
        const results = await new Promise((resolve, reject) => {
            db.query(
                /* sql */ `SELECT * FROM user WHERE email=? AND status='active' LIMIT 1`,
                [detailDataUser.email],
                (err, res) => {
                    if (err) reject(err);
                    resolve(res);
                }
            );
        })
            .then((response) => response)

        return results;
    } catch (error) {
        throw error;
    }
};

exports.getTrainingCourseCompany = async (detailTrainingCourseCompany) => {
    /**
     * Detail User Training
     * companyId: number
     */

    /* backup
        FROM training_course_company tcc
        left join training_company tc on tc.id = tcc.company_id
        left join training_course c on c.id = tcc.course_id
        LEFT JOIN training_course_session s ON s.course_id=c.id
        LEFT JOIN training_course_media m ON m.session_id=s.id
        LEFT JOIN training_course c2 ON c2.id = c.require_course_id
        WHERE tcc.company_id = tc.id and tc.company_id = ?
    */
    try {
        const results = await new Promise((resolve, reject) => {
            db.query(
                /* sql */ `SELECT c.*, GROUP_CONCAT(DISTINCT m.type) AS content, (SELECT COUNT(id) FROM training_course_session WHERE course_id=c.id) AS session_count,
        ((SELECT IF(SUM(time) IS NULL, 0, SUM(time)) FROM training_course_session WHERE course_id=c.id)+(SELECT IF(SUM(time_limit) IS NULL, 0, SUM(time_limit)) FROM training_exam WHERE course_id=c.id AND status='Active')+(SELECT IF(SUM(TIMESTAMPDIFF(MINUTE, start_time, end_time)) IS NULL, 0, SUM(TIMESTAMPDIFF(MINUTE, start_time, end_time))) FROM webinars WHERE training_course_id=c.id AND publish='1')) AS total_session_time,
        (IF(NOW() BETWEEN c.start_time AND c.end_time != '1' && c.scheduled = 1, '0', '1')) AS on_schedule, c2.title AS require_course_title, 'Course' AS type

        FROM training_course_company tcc 
        left join training_company tc on tc.id = tcc.training_company_id 
        left join training_course c on c.id = tcc.training_course_id 
        LEFT JOIN training_course_session s ON s.course_id=c.id 
        LEFT JOIN training_course_media m ON m.session_id=s.id 
        LEFT JOIN training_course c2 ON c2.id = c.require_course_id 
        WHERE tcc.training_company_id = tc.id and tc.company_id = ? AND c.status='Active'
        GROUP BY c.id 
        ORDER BY c.scheduled ASC, c.start_time ASC, require_course_id ASC`,
                [detailTrainingCourseCompany.companyId],
                (err, res) => {
                    if (err) reject(err);
                    resolve(res);
                }
            );
        })
            .then((response) => response)

        return results;
    } catch (error) {
        throw error;
    }
};
exports.getTrainingCourseByTrainingUserId = async (data,headers) => {
    /**
     * Detail User Training
     * companyId: number
     */

    /* backup
        FROM training_course_company tcc
        left join training_company tc on tc.id = tcc.company_id
        left join training_course c on c.id = tcc.course_id
        LEFT JOIN training_course_session s ON s.course_id=c.id
        LEFT JOIN training_course_media m ON m.session_id=s.id
        LEFT JOIN training_course c2 ON c2.id = c.require_course_id
        WHERE tcc.company_id = tc.id and tc.company_id = ?
    */
    try {
        let checkAssignCourse = require("../controllers/training_course").checkAssignCourse;
        let results = await new Promise((resolve, reject) => {
            db.query(
                /* sql */ `SELECT c.*, GROUP_CONCAT(DISTINCT m.type) AS content, (SELECT COUNT(id) FROM training_course_session WHERE course_id=c.id) AS session_count,
        ((SELECT IF(SUM(time) IS NULL, 0, SUM(time)) FROM training_course_session WHERE course_id=c.id)+(SELECT IF(SUM(time_limit) IS NULL, 0, SUM(time_limit)) FROM training_exam WHERE course_id=c.id AND status='Active')+(SELECT IF(SUM(TIMESTAMPDIFF(MINUTE, start_time, end_time)) IS NULL, 0, SUM(TIMESTAMPDIFF(MINUTE, start_time, end_time))) FROM webinars WHERE training_course_id=c.id AND publish='1')) AS total_session_time,
        (IF(NOW() BETWEEN c.start_time AND c.end_time != '1' && c.scheduled = 1, '0', '1')) AS on_schedule, c2.title AS require_course_title, 'Course' AS type

        FROM training_course_company tcc 
        LEFT JOIN training_user tu ON tu.training_company_id = tcc.training_company_id
        left join training_company tc on tc.id = tcc.training_company_id 
        left join training_course c on c.id = tcc.training_course_id 
        LEFT JOIN training_course_session s ON s.course_id=c.id 
        LEFT JOIN training_course_media m ON m.session_id=s.id 
        LEFT JOIN training_course c2 ON c2.id = c.require_course_id 
        WHERE tcc.training_company_id = tc.id AND c.status='Active' AND tu.id=? AND tcc.active=1
        GROUP BY c.id 
        ORDER BY c.scheduled ASC, c.start_time ASC, require_course_id ASC`,
                [data.trainingUserId],
                (err, res) => {
                    if (err) reject(err);
                    resolve(res);
                }
            );
        })
            .then((response) => response)

        /*
            let filter = await new Promise(async (resolve)=>{
            let tmp = [];
            try {
                
                let confAxios = { headers: { "Authorization": headers.authorization } };
            
                for (let index = 0; index < results.length; index++) {
                    
                    let respData = await axios.get(`http://localhost:3200/v2/training/course/assignment/${results[index].id}`, confAxios);
                    if(respData.data.result.length > 0){
                        
                        let listAssignCourse = respData.data.result;
                        listAssignCourse.forEach(async (company)=>{
                            let idx = company.training_user.findIndex( (assign)=>{ return (assign.assigned > 0 && assign.id.toString() === data.trainingUserId) });
                            //console.log(idx,company.training_course_id,"HELLO============")
                            if(idx > -1){
                                tmp.push(results[index]);
                            }
                        })
                    }
                }
                
            } catch (error) {
                console.log(error,"err ===============")
            }
            resolve(tmp);
        });
        */

        let filter = await checkAssignCourse(results,{ training_user_id : data.trainingUserId });
        console.log(filter.length,'===========')
        return filter;
    } catch (error) {
        throw error;
    }
};

exports.getTrainingCourseByTrainingUserIdAndIdCourse = async (data) => {
    /**
     * Detail User Training
     * companyId: number
     */

    try {
        const results = await new Promise((resolve, reject) => {
            db.query(
                /* sql */ `SELECT c.*, GROUP_CONCAT(DISTINCT m.type) AS content, (SELECT COUNT(id) FROM training_course_session WHERE course_id=c.id) AS session_count,
        ((SELECT IF(SUM(time) IS NULL, 0, SUM(time)) FROM training_course_session WHERE course_id=c.id)+(SELECT IF(SUM(time_limit) IS NULL, 0, SUM(time_limit)) FROM training_exam WHERE course_id=c.id AND status='Active')+(SELECT IF(SUM(TIMESTAMPDIFF(MINUTE, start_time, end_time)) IS NULL, 0, SUM(TIMESTAMPDIFF(MINUTE, start_time, end_time))) FROM webinars WHERE training_course_id=c.id AND publish='1')) AS total_session_time,
        (IF(NOW() BETWEEN c.start_time AND c.end_time != '1' && c.scheduled = 1, '0', '1')) AS on_schedule, c2.title AS require_course_title, 'Course' AS type

        FROM training_course_company tcc 
        LEFT JOIN training_user tu ON tu.training_company_id = tcc.training_company_id
        left join training_company tc on tc.id = tcc.training_company_id 
        left join training_course c on c.id = tcc.training_course_id 
        LEFT JOIN training_course_session s ON s.course_id=c.id 
        LEFT JOIN training_course_media m ON m.session_id=s.id 
        LEFT JOIN training_course c2 ON c2.id = c.require_course_id 
        WHERE tcc.training_company_id = tc.id AND c.status='Active' AND tu.id= ? AND c.id = ? AND tcc.active=1
        GROUP BY c.id 
        ORDER BY c.scheduled ASC, c.start_time ASC, require_course_id ASC`,
                [data.idTrainingUser, data.idCourse],
                (err, res) => {
                    if (err) reject(err);
                    resolve(res);
                }
            );
        })
            .then((response) => response)

        return results;
    } catch (error) {
        throw error;
    }
};

exports.getTrainingExamCheck = async (detailTrainingExam) => {
    /**
     * Detail Training Exam 
     * requireCourseId: number
     */
    try {
        const results = await new Promise((resolve, reject) => {
            db.query(
                /* sql */ `SELECT MIN(r.pass_exam) AS require_check
                        FROM (
                        SELECT e.title, (SELECT COUNT(r.id) FROM training_exam_result r JOIN training_user tu ON tu.id = r.training_user_id WHERE r.exam_id=e.id AND r.pass=1 AND tu.email= ? ) AS pass_exam
                        FROM training_exam e
                        WHERE e.course_id = ? AND e.exam=1
                        ) AS r`,
                [detailTrainingExam.email, detailTrainingExam.requireCourseId],
                (err, res) => {
                    if (err) reject(err);
                    resolve(res);
                }
            );
        })
            .then((response) => response)

        return results;
    } catch (error) {
        throw error;
    }
};

exports.getRequiredCourse = async (detailRequiredCourse) => {
    /**
     * Detail Training Exam 
     * requireCourseId: number
     */
    try {
        const results = await new Promise((resolve, reject) => {
            db.query(
                /* sql */ `SELECT title, start_time, end_time, scheduled FROM training_course WHERE id=?`,
                [detailRequiredCourse.requireCourseId],
                (err, res) => {
                    if (err) reject(err);
                    resolve(res);
                }
            );
        })
            .then((response) => response)

        return results;
    } catch (error) {
        throw error;
    }
};

exports.getTrainingCourseSession = async (detailTrainingCourseSession) => {
    /**
     * Detail Training Course Sessions 
     * email: email
     * courseId: number
     */
    try {
        const results = await new Promise((resolve, reject) => {
            db.query(
                /* sql */ `SELECT s.*, IF(r.is_read IS NOT NULL, r.is_read, 0) AS is_read, 'Session' AS type
                        FROM training_course_session s
                        LEFT JOIN (SELECT r.* FROM training_course_session_read r JOIN training_user u ON u.id = r.training_user_id WHERE u.email=? GROUP BY r.session_id) r ON r.session_id = s.id
                        WHERE s.course_id=?
                        ORDER BY s.sort ASC`,
                [detailTrainingCourseSession.email, detailTrainingCourseSession.courseId],
                (err, res) => {
                    if (err) reject(err);
                    resolve(res);
                }
            );
        })
            .then((response) => response)

        return results;
    } catch (error) {
        throw error;
    }
};

exports.getTrainingCourseSubmission = async (detailTrainingCourseSubmission) => {
    /**
     * Detail Training Course Sessions 
     * email: email
     * courseId: number
     */
    try {
        const results = await new Promise((resolve, reject) => {
            db.query(
                /* sql */ `SELECT s.*, IF(r.is_read IS NOT NULL, r.is_read, 0) AS is_read, 'Submission' AS type, IF(r.created_at, DATEDIFF(s.start_time,r.created_at),DATEDIFF(s.start_time,s.end_time) ) as 'duration' 
                        FROM training_course_submission s
                        LEFT JOIN (SELECT r.* FROM training_submission_answer r JOIN training_user u ON u.id = r.training_user_id WHERE u.email=? GROUP BY r.submission_id) r ON r.submission_id = s.id
                        WHERE s.course_id=? and r.status='active'
                        ORDER BY s.sort ASC`,
                [detailTrainingCourseSubmission.email, detailTrainingCourseSubmission.courseId],
                (err, res) => {
                    console.log([detailTrainingCourseSubmission.email, detailTrainingCourseSubmission.courseId])
                    if (err) reject(err);
                    resolve(res);
                }
            );
        })
            .then((response) => response)

        return results;
    } catch (error) {
        throw error;
    }
};

exports.getQuiz = async (detailQuiz) => {

    /**
     * Detail Training Course Sessions 
     * idTrainingUser: number
     * idCourse: number
     */

    /**
      * Notes
      * Untuk Skrng Query untuk score ambil yang terakhir, sebelumnya tidak
      * Query sblm = (SELECT GROUP_CONCAT(r.score) FROM training_exam_result r WHERE r.exam_id=e.id AND r.training_user_id=? AND r.assignee_id=a.id) AS score
      * Query sesudah = (SELECT CONVERT(r.score, CHAR) FROM training_exam_result r WHERE r.exam_id=e.id AND r.training_user_id=? AND r.assignee_id=a.id ORDER BY r.created_at DESC LIMIT 1) AS score
      */

    try {

        const queryQuizUser = /* sql */ `SELECT e.*, (SELECT SUM(total) FROM training_exam_composition WHERE exam_id=e.id) AS number_of_question, l.name AS licenses_type,  IF(e.scheduled=1,'Yes','No') AS is_scheduled, IF(e.exam = 1, 'Exam', 'Quiz') AS type,
        (IF((SELECT COUNT(r.id) AS hadir FROM training_exam_result r WHERE r.exam_id=e.id AND r.training_user_id=? AND r.assignee_id=a.id)>0,1,0)) AS is_read, a.id AS assignee_id,
        (SELECT CONVERT(r.score, CHAR) FROM training_exam_result r WHERE r.exam_id=e.id AND r.training_user_id=? AND r.assignee_id=a.id ORDER BY r.created_at DESC LIMIT 1) AS score
        FROM training_exam e
        LEFT JOIN training_licenses_type l ON l.id = e.licenses_type_id
        LEFT JOIN training_exam_assignee a ON a.exam_id = e.id
        WHERE e.status='Active' AND e.exam=0 AND e.course_id=? AND a.training_user_id=?
        ORDER BY e.created_at DESC;`;

        const queryQuizAdmin = /* sql */ `SELECT e.*, (SELECT SUM(total) FROM training_exam_composition WHERE exam_id=e.id) AS number_of_question, l.name AS licenses_type,  IF(e.scheduled=1,'Yes','No') AS is_scheduled, IF(e.exam = 1, 'Exam', 'Quiz') AS type,
        (IF((SELECT COUNT(r.id) AS hadir FROM training_exam_result r WHERE r.exam_id=e.id AND r.training_user_id=?)>0,1,0)) AS is_read
        FROM training_exam e
        LEFT JOIN training_licenses_type l ON l.id = e.licenses_type_id
        WHERE e.status='Active' AND e.exam=0 AND e.course_id=?
        ORDER BY e.created_at DESC;`;

        const choosenQuery = detailQuiz.idCourse === 0 ? queryQuizAdmin : queryQuizUser;

        const results = await new Promise((resolve, reject) => {
            db.query(choosenQuery,
                [detailQuiz.idTrainingUser, detailQuiz.idTrainingUser, detailQuiz.idCourse, detailQuiz.idTrainingUser],
                (err, res) => {
                    if (err) reject(err);
                    resolve(res);
                }
            );
        })
            .then((response) => response)

        return results;
    } catch (error) {
        throw error;
    }

};

exports.getExam = async (detailExam) => {
    /**
    * Detail Training Course Sessions 
    * idTrainingUser: number
    * idCourse: number
    */

    /**
     * Notes
     * Untuk Skrng Query untuk score ambil yang terakhir, sebelumnya tidak
     * Query sblm = (SELECT GROUP_CONCAT(r.score) FROM training_exam_result r WHERE r.exam_id=e.id AND r.training_user_id=? AND r.assignee_id=a.id) AS score
     * Query sesudah = (SELECT CONVERT(r.score, CHAR) FROM training_exam_result r WHERE r.exam_id=e.id AND r.training_user_id=? AND r.assignee_id=a.id ORDER BY r.created_at DESC LIMIT 1) AS score
     */

    try {

        const queryQuizUser = /* sql */ `SELECT e.*, (SELECT SUM(total) FROM training_exam_composition WHERE exam_id=e.id) AS number_of_question, l.name AS licenses_type,  IF(e.scheduled=1,'Yes','No') AS is_scheduled, IF(e.exam = 1, 'Exam', 'Quiz') AS type,
        (IF((SELECT COUNT(r.id) AS hadir FROM training_exam_result r WHERE r.exam_id=e.id AND r.training_user_id='${detailExam.idTrainingUser}' AND r.assignee_id=a.id)>0,1,0)) AS is_read, a.id AS assignee_id,
        (SELECT CONVERT(r.score, CHAR) FROM training_exam_result r WHERE r.exam_id=e.id AND r.training_user_id='${detailExam.idTrainingUser}' AND r.assignee_id=a.id ORDER BY r.created_at DESC LIMIT 1) AS score,
        (SELECT r.id FROM training_exam_result r WHERE r.exam_id=e.id AND r.training_user_id='${detailExam.idTrainingUser}' AND r.assignee_id=a.id ORDER BY r.created_at DESC LIMIT 1) AS resultId,
        a.start_time, a.submission_time, a.status as 'statusExam' 
        FROM training_exam e
        LEFT JOIN training_licenses_type l ON l.id = e.licenses_type_id
        LEFT JOIN training_exam_assignee a ON a.exam_id = e.id
        WHERE e.status='Active' AND e.exam=1 AND e.course_id='${detailExam.idCourse}' AND a.training_user_id='${detailExam.idTrainingUser}' ORDER BY a.created_at DESC;`;

        const queryQuizAdmin = /* sql */ `
        SELECT e.*, 
        (SELECT SUM(total) FROM training_exam_composition WHERE exam_id=e.id) AS number_of_question, 
        l.name AS licenses_type,  
        IF(e.scheduled=1,'Yes','No') AS is_scheduled, 
        IF(e.exam = 1, 'Exam', 'Quiz') AS type,
        (IF((SELECT COUNT(r.id) AS hadir FROM training_exam_result r WHERE r.exam_id=e.id AND r.training_user_id='${detailExam.idTrainingUser}')>0,1,0)) AS is_read 
        FROM training_exam e
        LEFT JOIN training_licenses_type l ON l.id = e.licenses_type_id
        WHERE e.status='Active' AND e.exam=1 AND e.course_id='${detailExam.idCourse}'
        ORDER BY a.created_at DESC;`;

        const choosenQuery = detailExam.idCourse === 0 ? queryQuizAdmin : queryQuizUser;

        const results = await new Promise((resolve, reject) => {
            db.query(choosenQuery,
                (err, res) => {
                    if (err) reject(err);
                    resolve(res);
                }
            );
        })
            .then((response) => response)

        return results;
    } catch (error) {
        throw error;
    }
};

exports.getUnassignedExam = async (detailUnassignedExam) => {
    /**
     * Detail Training Course Sessions 
     * idTrainingUser: number
     * companyId: number
     */
    try {
        const results = await new Promise((resolve, reject) => {
            db.query(
                /* sql */ `SELECT e.*, (SELECT SUM(total) FROM training_exam_composition WHERE exam_id=e.id) AS number_of_question, l.name AS licenses_type,  IF(e.scheduled=1,'Yes','No') AS is_scheduled, IF(e.exam = 1, 'Exam', 'Quiz') AS type,
                    (IF((SELECT COUNT(r.id) AS hadir FROM training_exam_result r WHERE r.exam_id=e.id AND r.training_user_id=? AND r.assignee_id=a.id)>0,1,0)) AS is_read,
                    (SELECT CONVERT(r.score, CHAR) FROM training_exam_result r WHERE r.exam_id=e.id AND r.training_user_id=? AND r.assignee_id=a.id ORDER BY r.created_at DESC LIMIT 1) AS score,
                    (SELECT r.id FROM training_exam_result r WHERE r.exam_id=e.id AND r.training_user_id='${detailUnassignedExam.idTrainingUser}' AND r.assignee_id=a.id ORDER BY r.created_at DESC LIMIT 1) AS resultId,
                    (SELECT r.assignee_id FROM training_exam_result r WHERE r.exam_id=e.id AND r.training_user_id='${detailUnassignedExam.idTrainingUser}' AND r.assignee_id=a.id ORDER BY r.created_at DESC LIMIT 1) AS assignee_id,
                    a.submission_time,
                    a.start_time,
                    a.status as 'statusAssignee' 
                    FROM training_exam e
                    LEFT JOIN training_licenses_type l ON l.id = e.licenses_type_id
                    LEFT JOIN training_exam_assignee a ON a.exam_id = e.id
                    WHERE e.company_id=? AND e.status='Active' AND (e.course_id = 0 OR e.course_id IS NULL) AND a.training_user_id=?
                    ORDER BY e.created_at DESC;`,
                [detailUnassignedExam.idTrainingUser, detailUnassignedExam.idTrainingUser, detailUnassignedExam.companyId, detailUnassignedExam.idTrainingUser],
                (err, res) => {
                    if (err) reject(err);
                    resolve(res);
                }
            );
        })
            .then((response) => response)

        return results;
    } catch (error) {
        throw error;
    }
};

exports.getAgendaByCourseId = async (detail) => {
    try {
        const results = await new Promise((resolve, reject) => {
            db.query(
                /* sql */ `SELECT * FROM agenda_course WHERE course_id = ?;`,
                [detail.idCourse],
                (err, res) => {
                    if (err) reject(err);
                    resolve(res);
                }
            );
        })
            .then((response) => response)

        return results;
    } catch (error) {
        throw error;
    }
};

exports.getListTagByIdCompany = async (detail) => {
    try {
        const results = await new Promise((resolve, reject) => {
            let append = '';
            if (detail.lisensi_id) {
                append += ` and c.licenses_type_id='${detail.lisensi_id}' `;
            }
            let sqlString = /* sql */ `SELECT c.id, c.tag, COUNT(c.id) AS total_questions 
            FROM training_questions c
            LEFT JOIN training_licenses_type t ON t.id = c.licenses_type_id
            WHERE t.company_id = '${detail.idCompany}' AND c.tag IS NOT null and ${detail.lisensi_id ? 'c.licenses_type_id =' + detail.lisensi_id : 'c.licenses_type_id IS NOT NULL'}
            GROUP BY tag ORDER BY id;`;

            db.query(sqlString,
                (err, res) => {
                    if (err) reject(err);
                    resolve(res);
                }
            );
        })
            .then((response) => response)

        return results;
    } catch (error) {
        throw error;
    }
};

exports.getListCompanyByCourseId = async (detail) => {
    try {
        const results = await new Promise((resolve, reject) => {
            db.query(
                /* sql */ `SELECT
            tcc.id, 
            tcc.training_course_id,
            tcc.training_company_id,
            tc.name AS 'company',
            tc.image AS 'image_company',
            tcc.created_at AS 'assignment_date', 
            if(tcc.active > 0,'Active','Inactive') AS 'assignment_status'
            from training_course_company tcc
            left join training_company tc on tc.id = tcc.training_company_id
            where 
            tcc.training_course_id = ? AND tcc.active = 1;`,
                [detail.idCourse],
                (err, res) => {
                    if (err) reject(err);
                    resolve(res);
                }
            );
        })
            .then((response) => response)

        return results;
    } catch (error) {
        throw error;
    }
};

exports.getDetailSubmissionByIdCourse = async (detail) => {
    try {
        const queryStrings = detail.type === 'upcomming' ? `SELECT tcs.id, tcs.course_id, tcs.title, tcs.content, tcs.type, tcs.sort, tcs.start_time AS startTime, tcs.end_time AS endTime FROM training_course_submission tcs WHERE tcs.course_id =? AND tcs.start_time >= ?` : `SELECT tcs.id, tcs.course_id, tcs.title, tcs.content, tcs.type, tcs.sort, tcs.start_time AS startTime, tcs.end_time AS endTime FROM training_course_submission tcs WHERE tcs.course_id =? AND (DATE(?) BETWEEN DATE(tcs.start_time) AND DATE(tcs.end_time))`;
        const results = await new Promise((resolve, reject) => {
            db.query(
                /* sql */ queryStrings,
                [detail.idCourse, detail.currentDate],
                (err, res) => {
                    if (err) reject(err);
                    resolve(res);
                }
            );
        })
            .then((response) => response)

        return results;
    } catch (error) {
        throw error;
    }
};

exports.getDetailSubmission = async (detail) => {
    try {
        const results = await new Promise((resolve, reject) => {
            db.query(
                /* sql */ `SELECT * FROM training_course_submission WHERE id =?`,
                [detail.idSubmission],
                (err, res) => {
                    if (err) reject(err);
                    resolve(res);
                }
            );
        })
            .then((response) => response)

        return results;
    } catch (error) {
        throw error;
    }
};

exports.getDetailSubmissionUser = async (detail) => {
    try {
        const results = await new Promise((resolve, reject) => {
            db.query(
                /* sql */ `SELECT tcs.*, tsa.id AS answer_id, tsa.submission_file, tsa.submission_text, tsa.score, tsa.count_uploaded, tsa.created_at AS answer_created_at, tsa.updated_at AS answer_updated_at, IF(tsa.is_read IS NULL, 0, tsa.is_read) AS is_read
FROM training_course_submission tcs
LEFT JOIN (SELECT * FROM training_submission_answer WHERE training_user_id=? AND submission_id=?) tsa ON tcs.id = tsa.submission_id
WHERE tcs.id =?`,
                [detail.idTrainingUser, detail.idSubmission, detail.idSubmission],
                async (err, res) => {
                    if (err) reject(err);

                    let media = await db.query(`SELECT * FROM training_course_media WHERE submission_id=?`, [detail.idSubmission]);
                    res[0].media = media;
                    resolve(res);
                }
            );
        })
            .then((response) => response)

        return results;
    } catch (error) {
        throw error;
    }
};

exports.getTrainingSubmissionAnswerByIdSubmission = async (detail) => {
    try {
        const results = await new Promise((resolve, reject) => {
            db.query(
                /* sql */ `SELECT * FROM training_submission_answer WHERE submission_id = ? AND training_user_id = ? AND is_read = 1`,
                [detail.idSubmission, detail.idTrainingUser],
                async (err, res) => {
                    if (err) reject(err);
                    resolve(res);
                }
            );
        })
            .then((response) => response)

        return results;
    } catch (error) {
        throw error;
    }
};

exports.getDetailSubmissionById = async (detail) => {
    try {
        const results = await new Promise((resolve, reject) => {
            db.query(
                /* sql */ `SELECT * FROM training_course_submission WHERE id = ?`,
                [detail.idSubmission],
                async (err, res) => {
                    if (err) reject(err);
                    resolve(res);
                }
            );
        })
            .then((response) => response)

        return results;
    } catch (error) {
        throw error;
    }
};

exports.postTrainingUserAnswer = async (detailObject) => {
    try {
        const results = await new Promise((resolve, reject) => {
            db.query(
                /* sql */ `INSERT INTO training_submission_answer
                (submission_id, training_user_id, submission_file, submission_text, count_uploaded, is_read)
                VALUES (?);`,
                [[detailObject.idSubmission, detailObject.idTrainingUser, detailObject.arrSubmissionFile, detailObject.textSubmission, detailObject.countUploaded, 0]], (err, res) => {
                    if (err) reject(err);
                    resolve(res);
                })
        }).then(response => response)
            .catch(err => err);
        return results;
    } catch (error) {
        throw error;
    }
}

exports.updateTrainingUserAnswer = async (dataDetail) => {
    try {
        const results = await new Promise((resolve, reject) => {
            db.query(
                /* sql */ `UPDATE training_submission_answer SET submission_file=?, submission_text=?, count_uploaded=?, is_read=1 WHERE submission_id=? AND training_user_id=?`,
                [dataDetail.arrSubmissionFile, dataDetail.textSubmission, dataDetail.countUploaded, dataDetail.idSubmission, dataDetail.idTrainingUser],
                (err, res) => {
                    if (err) reject(err);
                    resolve(res);
                }
            );
        })
            .then((response) => response)

        return results;
    } catch (error) {
        throw error;
    }
};
