
const db = require('../configs/database');
exports.storeOperators = async (field, params) => {
    let response = { error: false, result:[] };
    try {
        let result = null;
        let arrField = ["id","title","deleted"];
        let tmpData = [];
        let tmpField = [];
        let table = 'crm_operators';

        for(let i=0; i < arrField.length; i++){
            if(field[arrField[i]] !== undefined){
                if(params.status === 'store'){
                    tmpField.push(`\`${arrField[i]}\``);
                    tmpData.push(field[arrField[i]]);
                }else{
                    tmpData.push(`${arrField[i]}="${field[arrField[i]]}"`);
                }
            } 
        }

        let query = '';
        if(params.status === 'store'){
            query = `
                INSERT INTO ${table}(${tmpField.toString()})
                value(?);
            `;
            result = await db.query(query,[tmpData]);
        }else{
            query = `
            UPDATE ${table} SET ${tmpData.toString()} WHERE id=?;
            `;
            result = await db.query(query,[field.id]);
        }

        response.result = 'success';
    } catch (error) {
        response.result = [];
        response.error = error;
        console.log(error)
    }
    return response;
};
exports.getOperators = async (field, params) => {
    let response = { error: false, result:[] };
    try {
        let get = await db.query(` select id, title, deleted from crm_operators `);
        response.result = get;
    } catch (error) {
        response.result = [];
        response.error = error;
        console.log(error)
    }
    return response;
};

