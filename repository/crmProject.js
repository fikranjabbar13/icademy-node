const moment = require("moment");
const db = require('../configs/database');
const modelTimesheet = require("./crmTimeSheet");
let today = moment(new Date()).format("YYYY-MM-DD");
const sortField = ['title','description','project_id','milestone_id','assigned_to','deadline','labels','points','status','status_id','priority_id','start_date','collaborators','parent_task_id','blocking','blocked_by',"sort"];
const arrAction = ['created','updated','deleted','bitbucket_notification_received','github_notification_received'];
const sortFieldActivity = ['created_by','action','log_type','log_type_id','log_for','log_for_id','changes','action_title'];
const fieldReminder = ['title','task_id','project_id','start_date','start_time','type','repeat_every','repeat_type','no_of_cycles','created_by'];
const fieldComment = ['description','task_id','project_id','files','created_by'];
const FieldLabel = ['title','color','context','user_id',"deleted"];
const FieldProject = ['title','project_type','client_id','description','start_date','deadline','price','labels','created_by','status','starred_by'];
const FieldMilestone = ['title','description','due_date','project_id',"deleted"];

const addActivityTask = async (field, params) => {
    return new Promise(async(hasil)=>{
        try {
    
            let arrField = FieldProject;
            let arrFieldActivity = sortFieldActivity;
            let tmpData = [];
            let changeData = {};
            let result = null;
            let query = '';
    
            if(field.id){
                query = `
                    SELECT ${arrField.toString()} from crm_projects where id=? ;
                `;
                result = await db.query(query,[field.id]);
                if(result.length) result = result[0];

                for(let i=0; i < arrField.length; i++){


                    if(['start_date','deadline'].indexOf(arrField[i]) > -1){
                        let CheckDate = (result[arrField[i]] && result[arrField[i]] != '') ? moment(result[arrField[i]]).format('YYYY-MM-DD') : null;
                        result[arrField[i]] = CheckDate;
                        field[arrField[i]] = CheckDate;
                    }else{
                        if(field[arrField[i]] !== undefined){
                            field[arrField[i]] = field[arrField[i]].toString();
                        }
                        if(result[arrField[i]]){
                            result[arrField[i]] = result[arrField[i]].toString();
                        }
                    }

                    if( result[arrField[i]] !== field[arrField[i]] ){
                        // if(manyValue.indexOf(arrField[i]) > -1){
                        // }
                        changeData[arrField[i]] = {
                            from: result[arrField[i]],
                            to: field[arrField[i]]
                        }
                    }
                }
            }
    
            if(Object.keys(changeData).length > 0){
                field.changes = JSON.stringify(changeData);
            }
    
            for(let i=0; i < arrFieldActivity.length; i++){
                if(field[arrFieldActivity[i]] !== undefined){
                    tmpData.push(field[arrFieldActivity[i]]);
                } 
            }
            
            
            query = `INSERT INTO crm_activity_logs(${sortFieldActivity.toString()}) value(?)`;
            result = await db.query(query,[tmpData]);
            return hasil({ error: false, result:'success' });
    
        } catch (error) {
            console.log(error);
            return hasil({ error: error, result:null });
        }
    });
};

const parseString = async (list,type, distinct)=>{
    return new Promise((hasil)=>{
        try {            
            //list = list.substr(0,list.length-2);
            let arrLabel = list.split(',');
            let arrField = {
                subtask_list : ['subtask_id','subtask_title','subtask_status_id'],
                checklist_item:["id","title","is_checked","task_id","sort"],
                logged:["hours","start_time","end_time","task_id","project_id","note","status","user_id"],
                priority_list:["priority_id","priority_title","priority_icon","priority_color"], 
                status_list: ["status_id","status_title","status_key_name","status_color"],
                project_status_list: ["status_id","status_title","status_color","status_key_name","status_project_id"],
                project_members_list :['user_id','user_name','user_avatar','project_id'],
                project_client_list :['client_id','client_company_name','client_type'],
                project_list:['project_id','project_title'],
                milestone_byProject_list: ["milestone_id", "milestone_title", "milestone_project_id",'milestone_due_date'],
                byProject_members_list :['user_id','user_name','user_project_id'],
                tasks_list: ['task_id','task_title','project_id'],
                labels_list: ['label_id','label_title','label_color'],
                collaborator_list: ['user_id','user_name','user_avatar','project_id'],
                milestone_list: ["milestone_id", "milestone_title", "milestone_deadline", "milestone_status",'project_id'],
                milestone_list2: ["milestone_id", "milestone_title", 'project_id'],
                blocked_list:["id","title","created_date"],
                blocking_list:["id","title","created_date"],
                comment_like_list: ['like_id',"comment_id","task_id","created_by_user","created_by_username","created_by_useravatar",'created_at'],
                comment_pin_list: ["pin_id","comment_id","task_id","created_by_user","created_by_username","created_by_useravatar",'created_at'],
                events_list: ['id','task_id','project_id','ticket_id','lead_id','type','title','description','start_date','end_date','start_tme','end_time','reminder_status','recurring','repeat_type','repeat_every'],
            };
            let splitLabel = null;
            let label = [];
            
            if(arrLabel.length){
    
                splitLabel = arrField[type];
                
                for(let i=0; i < arrLabel.length; i++){
                    
                    arrLabel[i] = arrLabel[i].split('|');
                    let labelObject = {};
                    
                    for(let j=0; j < arrLabel[i].length; j++){
                        if(!isNaN(Number(arrLabel[i][j])) && arrLabel[i][j] != ''){
                            labelObject[splitLabel[j]] = Number(arrLabel[i][j]);
                        }else{
                            labelObject[splitLabel[j]] = arrLabel[i][j] === '' ? null : arrLabel[i][j];
                        }
                    }
                    
                    if(distinct){
                        let idx = []; 
                        if(type == 'byProject_members_list'){
                            idx = label.filter((list)=> (list.user_id == labelObject.user_id && list.user_project_id == labelObject.project_id));
                            if(idx.length == 0) label.push(labelObject);
                        }else if(type == 'milestone_byProject_list'){
                            idx = label.filter((list)=> (list.milestone_id == labelObject.milestone_id && list.milestone_project_id == labelObject.milestone_project_id));
                            if(idx.length == 0) label.push(labelObject);
                        }else if(type == 'project_list'){
                            idx = label.filter((list)=> (list.project_id == labelObject.project_id));
                            if(idx.length == 0) label.push(labelObject);
                        }
                    }else{
                        label.push(labelObject);
                    }
                }
            }
            return hasil(label);
        } catch (error) {
            //console.log(error,list)
            return hasil([]);
        }
    });
}

const makeQuickFilter = async(filter, user)=>{
    return new Promise((hasil)=>{
        let response = '';
        try {  
            let recently_updated_last_time = new Date(); // data last login user
            let query = '';
            let mention_string = ":"+user.name+"]";

            if(filter == "recently_meaning") {
                response = query;
            }else{

                let project_comments_table_query = `
                    SELECT crm_project_comments.task_id 
                    FROM crm_project_comments 
                    WHERE crm_project_comments.deleted=0 AND crm_project_comments.task_id!=0
                `;
                let project_comments_table_group_by = "GROUP BY crm_project_comments.task_id";
    
                if (filter === "recently_updated") {
                    query = ` AND (crm_tasks.status_changed_at IS NOT NULL AND crm_tasks.status_changed_at>='${recently_updated_last_time}')`;
                }else if (filter === "recently_commented") {
                    query = ` AND crm_tasks.id IN(${project_comments_table_query} AND crm_project_comments.created_at>='${recently_updated_last_time}' ${project_comments_table_group_by})`;
                }else if (filter === "mentioned_me" && recently_updated_last_time) {
                    query = ` AND crm_tasks.id IN(${project_comments_table_query} AND crm_project_comments.description LIKE '%${mention_string}%' ${project_comments_table_group_by})`;
                }else if (filter === "recently_mentioned_me" && recently_updated_last_time) {
                    query = ` AND crm_tasks.id IN(${project_comments_table_query} AND crm_project_comments.description LIKE '%${mention_string}%' AND crm_project_comments.created_at>='${recently_updated_last_time}' ${project_comments_table_group_by})`;
                } else if (filter === "recurring_tasks") {
                    query = ` AND (crm_tasks.recurring=1)`;
                } else {
                    query = ` AND (crm_tasks.status_changed_at IS NOT NULL AND crm_tasks.status_changed_at>='${recently_updated_last_time}' AND crm_tasks.status_id=${filter})`;
                }
                response = query;
            }

            
        } catch (error) {
            console.log({ error, file:'crmTask.js', scope:'makeQuickFilter'});
        }
        return hasil(response);
    })
}

const filterTask = async(filter,user)=>{
    return new Promise(async(hasil)=>{
        let where ='';
        let leftJoin = '';
        try {
            if (filter.project_id) {
                where += ` AND crm_tasks.project_id='${filter.project_id}' `;
            }
            if (filter.client_id) {
                where += ` AND crm_tasks.client_id='${filter.client_id}' `;
            }
            if (filter.parent_task_id) {
                where += ` AND crm_tasks.parent_task_id='${filter.parent_task_id}' `;
            }
            if (filter.priority_id) {
                where += ` AND crm_tasks.priority_id='${filter.priority_id}' `;
            }
            if (filter.status_id) {
                where += ` AND FIND_IN_SET(crm_tasks.status_id,'${filter.status_id.toString()}') `;
            }
            if (filter.task_id) {
                where += ` AND crm_tasks.id='${filter.task_id}' `;
            }
            if (filter.assigned_to) {
                where += ` AND crm_tasks.assigned_to='${filter.assigned_to}' `;
            }
            if (filter.specific_user_id) {
                where += ` AND (crm_tasks.assigned_to='${filter.specific_user_id}' OR FIND_IN_SET('${filter.specific_user_id}', crm_tasks.collaborators)) `;
            }
            if (filter.project_status) {
                where += ` AND FIND_IN_SET(crm_projects.status,'${filter.project_status}') `;
            }
            if (filter.milestone_id) {
                where += ` AND crm_tasks.milestone_id='${filter.milestone_id}' `;
            }
            if (filter.task_status_id) {
                where += ` AND crm_tasks.status_id='${filter.task_status_id}' `;
            }
            if (filter.start_date && filter.deadline) {
                if(filter.for_events){
                    if(filter.deadline_for_event && filter.start_date_for_event){
                        where += ` AND ( 
                            ( crm_tasks.deadline IS NOT NULL AND crm_tasks.deadline BETWEEN '${filter.start_date}' AND '${filter.deadline}' ) 
                            OR ( crm_milestones.due_date BETWEEN '${filter.start_date}' AND '${filter.deadline}' ) 
                            OR ( crm_tasks.start_date IS NOT NULL AND  crm_tasks.start_date BETWEEN '${filter.start_date}' AND '${filter.deadline}' )
                        )`;
                    }else if(filter.start_date_for_event){
                        where += ` AND (crm_tasks.start_date IS NOT NULL AND crm_tasks.start_date BETWEEN '${filter.start_date}' AND '${filter.deadline}' ) `;
                    }else if(filter.deadline_for_event){
                        where += ` AND (
                                (crm_tasks.deadline IS NOT NULL AND crm_tasks.deadline BETWEEN '${filter.start_date}' AND '${filter.deadline}' ) 
                                OR ( crm_milestones.due_date BETWEEN '${filter.start_date}' AND '${filter.deadline}' ) 
                                )`;
                    }
                }else{
                    where += ` AND (crm_tasks.deadline IS NOT NULL AND crm_tasks.deadline BETWEEN '${filter.start_date}' AND '${filter.deadline}' ) `;
                }
            } else if(typeof filter.deadline === 'string'){
                
                if(filter.deadline.toLowerCase() === 'expired'){
                    where += ` AND IF(crm_tasks.deadline IS NULL, crm_milestones.due_date<'${today}', crm_tasks.deadline<'${today}') `;
                }else if (filter.deadline == today) { //today
                    where +=  ` AND IF(crm_tasks.deadline IS NULL, crm_milestones.due_date='${filter.deadline}', crm_tasks.deadline='${filter.deadline}') `;
                }else { //future deadlines, extract today's
                    let addDays = moment(today).add(1,'days').format("YYYY-MM-DD");
                    where +=  ` AND IF(crm_tasks.deadline IS NULL, crm_milestones.due_date BETWEEN '${addDays}' AND '${filter.deadline}', crm_tasks.deadline BETWEEN '${addDays}' AND '${filter.deadline}') `;
                }
            }
            if (filter.tickets_id) {
                where += ` AND crm_tasks.tickets_id='${filter.tickets_id}' `;
            }
            if (filter.project_member_id) {
                where += ` AND crm_project_members.user_id='${filter.project_member_id}' `;
                leftJoin +=  ` LEFT JOIN crm_project_members ON crm_tasks.project_id= crm_project_members.project_id AND crm_project_members.deleted=0 AND crm_project_members.user_id='${filter.project_member_id}' `;
            }
            if (filter.quick_filter) {
                where += await makeQuickFilter(filter.quick_filter,user);
            }
        } catch (error) {
            console.log({error, file:'crmTask', scope:'filterTask'});
        }

        return hasil({ where, leftJoin });
    })

}

const get_labels_data_query = async(company)=>{
    let labels_table = "crm_labels";
    return `(
        SELECT 
        GROUP_CONCAT(${labels_table}.id, '|', ${labels_table}.title, '|', ${labels_table}.color) 
        FROM ${labels_table} 
        LEFT JOIN user u on u.user_id = ${labels_table}.user_id
        LEFT JOIN company c on c.company_id = u.company_id 
        WHERE 
        FIND_IN_SET(${labels_table}.id, crm_projects.labels) AND 
        ${labels_table}.context = 'project' AND 
        c.company_id = '${company}' AND
        ${labels_table}.deleted = 0
        ) AS labels_list
    `;
}

const get_status_data_query = async(company)=>{
    let statusTable = "crm_project_status";
    return `(
        SELECT GROUP_CONCAT(${statusTable}.id, '|', ${statusTable}.title, '|', ${statusTable}.color,'|',${statusTable}.key_name,'|',${statusTable}.project_id) 
        FROM ${statusTable} 
        LEFT JOIN crm_projects cp on cp.id = ${statusTable}.project_id 
        LEFT JOIN user u on u.user_id = cp.created_by
        LEFT JOIN company c on c.company_id = u.company_id 
        WHERE
        c.company_id = '${company}' and ${statusTable}.deleted=0 
        ) AS status_list
    `;
}

exports.getProjectActivity = async (field, params) => {
    let response = { error:false, result:[] }
    try {
        // GET activity task / project
        
        const query = `
            SELECT
            cal.id, cal.log_type_id, cal.log_for_id, cal.log_type, cal.log_for, cal.action_title, cal.changes ,
            cal.created_by, u1.name as 'created_name', u1.avatar as 'created_avatar', cal.created_at

            FROM crm_activity_logs cal
            LEFT JOIN user u1 on u1.user_id = cal.created_by 
            WHERE cal.log_for_id = ? 
            ORDER BY cal.created_at DESC;
        `;

        let result = await db.query(query,[params.idProject]);
        
        if(result.length){
            for(let i=0; i < result.length; i++){
                let list = result[i];
                if(list.changes && list.changes.length > 1){
                    list.changes = JSON.parse(list.changes);
                }
                list.created_at = moment(list.created_at).format("YYYY-MM-DD hh:mm:ss")
                result[i] = list;
            }
        }
        response.result = result;
        return response;
    } catch (error) {
        console.log({ error, file:"crmTask", scope:"getProjectActivity" });
        response.error = true;
        return response;
    }
};

exports.getAttributeProject = async (field, params) => {
    let response = { error:false, result:[] }
    try {
        // GET activity task / project
        const tables = {
            labels: "crm_labels",
            milestones: "crm_milestones",
            subtasks: 'crm_tasks',
            members_project: "crm_project_members",
        }
        
        const query = `
            SELECT
            ${params.idProject} as id,
            (
                SELECT GROUP_CONCAT(cl.id,'|',cl.title,'|',cl.color)
                FROM crm_labels cl
                WHERE cl.user_id = '${params.user_id}' and cl.deleted=0 and cl.context = 'task'
            ) as labels_list,
            (
                SELECT GROUP_CONCAT(cm.id,'|',cm.title,'|',cm.due_date,'|','active','|',cm.project_id)
                FROM crm_milestones cm
                WHERE cm.project_id = '${params.idProject}' and cm.deleted=0 
            ) as milestone_list,
            (
                SELECT GROUP_CONCAT(ct.id,'|',ct.title,'|',ct.project_id) 
                FROM crm_tasks ct 
                WHERE ct.project_id = '${params.idProject}' and ct.deleted=0 
            ) as tasks_list,
            /*(
                SELECT GROUP_CONCAT(cpm.user_id,'|',u.name,'|',IFNULL(u.avatar,''),'|',cpm.project_id) 
                FROM crm_project_members cpm 
                LEFT JOIN user u on u.user_id = cpm.user_id and lower(u.status) = 'active' 
                WHERE cpm.project_id = '${params.idProject}' and cpm.deleted=0 
            ) as project_members_list,*/
            (
                SELECT GROUP_CONCAT(cts.id,'|',cts.title,'|',cts.key_name,'|',cts.color) 
                FROM crm_task_status cts  
                WHERE cts.deleted=0 
            ) as status_list,
            (
                SELECT GROUP_CONCAT(cpt.id,'|',cpt.title,'|',cpt.icon,'|',cpt.color) 
                FROM crm_task_priority cpt 
                WHERE cpt.deleted=0 
            ) as priority_list,
            (
                SELECT GROUP_CONCAT(cp.id,'|',cp.title)
                FROM crm_projects cp
                left join crm_project_members cpm on cpm.project_id = cp.id and cp.deleted=0
                left join user u on u.user_id = cpm.user_id and cpm.deleted=0
                left join company c on c.company_id = u.company_id
                WHERE c.company_id='${params.company_id}'
            ) as project_list,
            (
                SELECT GROUP_CONCAT(cpm.user_id,'|',u.name,'|',IFNULL(u.avatar,''),'|',cpm.project_id) 
                FROM crm_project_members cpm 
                LEFT JOIN user u on u.user_id = cpm.user_id and lower(u.status) = 'active' and cpm.deleted=0
                left join company c on c.company_id = u.company_id
                WHERE c.company_id='${params.company_id}' 
            ) as project_members_list
        `;

        let result = await db.query(query);
        
        if(result.length){

            for(let i=0; i < result.length; i++){
                let list = result[i];
                list.labels_list = await parseString(list.labels_list,'labels_list');
                list.milestone_list = await parseString(list.milestone_list,'milestone_list');
                list.tasks_list = await parseString(list.tasks_list,'tasks_list');
                list.project_members_list = await parseString(list.project_members_list,'project_members_list');
                list.status_list = await parseString(list.status_list,'status_list');
                list.priority_list = await parseString(list.priority_list,'priority_list');
                list.project_list = await parseString(list.project_list,'project_list',true);
                
                // add default
                if(list.milestone_list.length){
                    list.milestone_list.forEach((str)=>{ str.value = str.milestone_id; str.label = str.milestone_title })
                    list.milestone_list.splice(0,0,{ value:0, label:'- Milestone -' })
                }
                if(list.project_list.length){
                    list.project_list.forEach((str)=>{ str.value = str.project_id; str.label = str.project_title })
                    list.project_list.splice(0,0,{ value:0, label:'- Project -' })
                }
                if(list.project_members_list.length){
                    list.project_members_list.forEach((str)=>{ str.value = str.user_id; str.label = str.user_name })
                    list.project_members_list.splice(0,0,{ value:0, label:'- Team member -' })
                }
                if(list.status_list.length){
                    list.status_list.forEach((str)=>{ str.value = str.status_id; str.label = str.status_title })
                    list.status_list.splice(0,0,{ value:0, label:'- Status -' })
                }
                if(list.priority_list.length){
                    list.priority_list.forEach((str)=>{ str.value = str.priority_id; str.label = str.priority_title })
                    list.priority_list.splice(0,0,{ value:0, label:'- Priority -' })
                }
                if(list.labels_list.length){
                    list.labels_list.forEach((str)=>{ str.value = str.label_id; str.label = str.label_title })
                    list.labels_list.splice(0,0,{ value:0, label:'- Labels -' })
                    console.log(list.labels_list)
                }

                list.points = [
                    { value: 0, label: '- Points -' },
                    { value: 1, label: '1 Point' },
                    { value: 2, label: '2 Point' },
                    { value: 3, label: '3 Point' },
                    { value: 4, label: '4 Point' },
                    { value: 5, label: '5 Point' },
                  ];
                list.checklist_item=[];
                
                result[i] = list;
            }
        }
        response.result = result;
        return response;
    } catch (error) {
        console.log({ error, file:"crmTask", scope:"getAttributeProject" });
        response.error = true;
        return response;
    }
};

exports.storeTaskReminder = async (field, params) => {
    let response = { error: false, result:[] };
    try {
        let result = null;
        let arrField = fieldReminder;
        let tmpData = [];
        let tmpField = [];
        for(let i=0; i < arrField.length; i++){
            if(field[arrField[i]] !== undefined){
                tmpField.push(`\`${arrField[i]}\``);
                tmpData.push(field[arrField[i]]);
            } 
        }

        const query = `
            INSERT INTO crm_events(${tmpField.toString()})
            value(?);
        `;
        result = await db.query(query,[tmpData]);
        response.result = 'success';
    } catch (error) {
        response.result = [];
        response.error = error;
        console.log(error)
    }
    return response;
};

exports.storeTaskComment = async (field, params) => {
    let response = { error: false, result:[] };
    try {
        let result = null;
        let arrField = fieldComment;
        let tmpData = [];
        let tmpField = [];
        for(let i=0; i < arrField.length; i++){
            if(field[arrField[i]] !== undefined){
                tmpField.push(`\`${arrField[i]}\``);
                tmpData.push(field[arrField[i]]);
            } 
        }

        const query = `
            INSERT INTO crm_project_comments(${tmpField.toString()})
            value(?);
        `;
        result = await db.query(query,[tmpData]);
        response.result = 'success';
    } catch (error) {
        response.result = [];
        response.error = error;
        console.log(error)
    }
    return response;
};

exports.deleteTaskReminder = async (field, params) => {
    let response = { error: false, result:[] };
    try {
        let result = null;
        fieldReminder.push("deleted");
        let arrField = fieldReminder;
        let tmpData = [];
        let table = 'crm_events';

        for(let i=0; i < arrField.length; i++){
            if(field[arrField[i]] !== undefined){
                tmpData.push(`${arrField[i]}="${field[arrField[i]]}"`);
            } 
        }
        let query = `UPDATE ${table} SET ${tmpData.toString()} WHERE id=?;`;
        result = await db.query(query,[field.id]);

        response.result = 'success';
    } catch (error) {
        response.result = [];
        response.error = error;
        console.log(error)
    }
    return response;
};

exports.storeTaskLogged = async(field,params)=>{
    return new Promise(async(hasil)=>{
        let response = { error:false, result:[] };
        try {
            let store = null;
            let now = new Date();
            let hours = 0;
            let id = 0;
            let start_time = null;
            let end_time = null;
            let query = `
                SELECT id, start_time from crm_project_time where task_id=? and project_id=? and status='open'
            `;
            let getId = await db.query(query,[field.task_id, field.project_id]);
            if(getId.length){
                id = getId[0].id;
                start_time = getId[0].start_time;

                hours = moment.duration(moment(now).diff(moment(start_time))).asSeconds();
                end_time = now;
                
                store = await db.query(`UPDATE crm_project_time SET note=?, end_time=?, status=?, hours=? where task_id=? and project_id=?`,[
                    field.note, now, field.status, hours, field.task_id, field.project_id
                ]);    
                
                response.result = [{ hours, start_time, end_time, ...field }];
            }else{
                store = await db.query(`INSERT INTO crm_project_time(user_id,task_id,project_id,start_time,status,hours) value(?)`,[[
                    field.user_id, field.task_id, field.project_id, now, field.status, 0
                ]]);    
                response.result = [];
            }
            
        } catch (error) {
            response.error = error;
        }
        return hasil(response);
    });
}

exports.addActivityTask;

exports.getDetail = async(field, params)=>{
    let response = { error:false, result:[] }
    return new Promise(async(hasil)=>{
        try {
            const projects_table = "crm_projects";
            const project_members_table = 'crm_project_members';
            const clients_table = 'crm_clients';
            const tasks_table = 'crm_tasks';
            let where = "";
            let extra_join = "";
            let extra_where = "";
            let total_times = 0;
            if(params.id){
                let getTimes = await modelTimesheet.getTotalTimeProject(field,params);
                if(!getTimes.error) total_times = getTimes.result;

                where += ` AND ${projects_table}.id='${params.id}' `;
            }

            if (params.filter.client_id) {
                where += ` AND ${projects_table}.client_id='${params.filter.client_id}' AND ${projects_table}.project_type='client_project' `;
            }

            if (params.filter.status) {
                where += ` AND (FIND_IN_SET(${projects_table}.status, '${params.filter.status.toString()}')) `;
            }

            if (params.filter.labels) {
                if(params.filter.labels.length){
                    where += ` AND (FIND_IN_SET('${params.filter.labels.toString()}', ${projects_table}.labels)) `;
                }
            }

            if (params.filter.deadline && !params.filter.for_events) {
                if (params.filter.deadline === "expired") {
                    where += ` AND (${projects_table}.deadline IS NOT NULL AND ${projects_table}.deadline<'${today}') `;
                } else {
                    where += ` AND (${projects_table}.deadline IS NOT NULL AND ${projects_table}.deadline<='${params.filter.deadline}') `;
                }
            }

            if (params.filter.start_date && params.filter.deadline) {
                if (params.filter.start_date_for_events) {
                    where += ` AND (${projects_table}.start_date BETWEEN '${params.filter.start_date}' AND '${params.filter.deadline}') `;
                } else {
                    where += ` AND (${projects_table}.deadline BETWEEN '${params.filter.start_date}' AND '${params.filter.deadline}') `;
                }
            }

            if (params.filter.starred_projects) {
                where += ` AND FIND_IN_SET(':${params.filter.user_id}:',${projects_table}.starred_by) `;
            }

            if (!params.filter.client_id && params.filter.user_id && !params.filter.starred_projects) {
                extra_join = ` LEFT JOIN (SELECT ${project_members_table}.user_id, ${project_members_table}.project_id FROM ${project_members_table} WHERE ${project_members_table}.user_id='${params.filter.user_id}' AND ${project_members_table}.deleted=0 GROUP BY ${project_members_table}.project_id) AS project_members_table ON project_members_table.project_id= ${projects_table}.id `;
                extra_where = ` AND (project_members_table.user_id=${params.filter.user_id} OR crm_projects.created_by=${params.filter.user_id}) `;
            }

            let select_labels_data_query = await get_labels_data_query(field.companyId);
           // let select_status_data_query = await get_status_data_query(field.companyId);
            
            db.query('SET SQL_BIG_SELECTS=1');
            
            let sql = `
            SELECT 
            ${
                params.id ? `${total_times} as total_times, ` 
                :''
            }
            ${projects_table}.*, 
            ${clients_table}.company_name as client_company, 
            ${clients_table}.currency_symbol,  
            total_points_table.total_points, 
            completed_points_table.completed_points, 
            ${select_labels_data_query} 
            FROM ${projects_table}
            LEFT JOIN ${clients_table} ON ${clients_table}.id= ${projects_table}.client_id
            LEFT JOIN (SELECT project_id, SUM(points) AS total_points FROM ${tasks_table} WHERE deleted=0 GROUP BY project_id) AS  total_points_table ON total_points_table.project_id= ${projects_table}.id
            LEFT JOIN (SELECT project_id, SUM(points) AS completed_points FROM ${tasks_table} WHERE deleted=0 AND status_id=3 GROUP BY project_id) AS  completed_points_table ON completed_points_table.project_id= ${projects_table}.id
            ${extra_join}     
            WHERE ${projects_table}.deleted=0 ${where} ${extra_where} 
            ORDER BY ${projects_table}.start_date DESC`;
            
            let result = await db.query(sql);
            if(result.length){
                let object = {
                    list:[],
                    filterData:{}
                }

                for(let i=0; i<result.length; i++){
                    let list = result[i];
                    if(list.labels_list){
                        object.filterData.labels_list = await parseString(list.labels_list,'labels_list');

                        let splitLabel = list.labels.split(",");
                        let tmpLabel = [];
                        for(let j=0; j < splitLabel.length; j++){
                            let idx = object.filterData.labels_list.findIndex((item)=> item.label_id === Number(splitLabel[j]) )
                            if(idx > -1) tmpLabel.push(object.filterData.labels_list[idx]);
                        }

                        if(tmpLabel.length){
                            list.labels = tmpLabel;
                        }else{
                            list.labels = []
                        }
                    }
                    list.completed_points = list.completed_points ? Number(list.completed_points) : 0;
                    list.total_points = list.total_points ? Number(list.total_points) : 0;
                    list.progress = Math.round((list.completed_points/list.total_points) * 100) || 0;

                    delete list.status_list;
                    delete list.labels_list;
                    result[i] = list;
                }
            }
            response.result = result;
        } catch (error) {
            console.log(error)
        }
        return hasil(response);
    });
    
}

exports.getProjectsFilterData = async (field, params) => {
    let response = { error: false, result:[] }
    try {
        let result = [];
        if(field.select){
            const query = `
                SELECT 
                ${
                    field.select.labels ? `                
                    (
                        SELECT GROUP_CONCAT(cl.id,'|',cl.title,'|',cl.color)
                        FROM crm_labels cl
                        left join user u on u.user_id = cl.user_id 
                        left join company c on c.company_id = u.company_id 
                        WHERE c.company_id='${field.company_id}' and cl.deleted=0 and cl.context = '${field.context}'
                    ) as labels_list,
                    `: ' null as labels_list, '
                }
                ${
                    field.select.client ? `                
                    (
                        SELECT GROUP_CONCAT(cl.id,'|',cl.company_name,'|',cl.type)
                        FROM crm_clients cl
                        left join user u on u.user_id = cl.created_by
                        left join company c on c.company_id = u.company_id 
                        WHERE c.company_id='${field.company_id}' and cl.deleted=0 
                    ) as client_list,
                    `: ' null as client_list, '
                }
                ${
                    field.select.milestone ? `
                    (
                        SELECT GROUP_CONCAT(cm.id,'|',cm.title,'|',cm.project_id,'|',cm.due_date)
                        FROM crm_milestones cm
                        left join crm_project_members cpm on cpm.project_id = cm.project_id and cm.deleted=0
                        left join user u on u.user_id = cpm.user_id and cpm.deleted=0
                        left join company c on c.company_id = u.company_id
                        WHERE c.company_id='${field.company_id}' 
                        ${
                            field.project_id ? ` and cm.project_id='${field.project_id}' ` : ''
                        } 
                    ) as milestone_byProject_list, 
                    `:' null as milestone_byProject_list, '
                }
                ${
                    field.select.status ? `
                    (
                        SELECT GROUP_CONCAT(cts.id,'|',cts.title,'|',cts.color,'|',cts.key_name,'|',cts.project_id) 
                        FROM crm_project_status cts  
                        left join crm_projects cp on cp.id = cts.project_id  
                        left join user u on u.user_id = cp.created_by  
                        left join company c on c.company_id = u.company_id 
                        WHERE cts.deleted=0 and c.company_id = '${field.company_id}' 
                    ) as project_status_list 
                    `: ' null as project_status_list '
                }
                
                FROM DUAL;
            `;
    
            result = await db.query(query);
        }
        
        if(result.length){
            for(let i=0; i < result.length; i++){
                let list = result[i];
                let objects = {
                    milestone_byProject_list:[],
                    status_list:[],
                    labels_list:[],
                    client_list:[]
                }
    
                objects.milestone_byProject_list = await parseString(list.milestone_byProject_list,'milestone_byProject_list',1);
                objects.status_list = await parseString(list.project_status_list,'project_status_list');
                objects.labels_list = await parseString(list.labels_list,'labels_list');
                objects.client_list = await parseString(list.client_list,'project_client_list');

                // add value and label
                if(objects.milestone_byProject_list.length){
                    objects.milestone_byProject_list.forEach((str)=>{ str.value = str.milestone_id; str.label = str.milestone_title })
                    objects.milestone_byProject_list.splice(0,0,{ value:0, label:'- Milestone -' })
                }
                if(objects.status_list.length){
                    objects.status_list.forEach((str)=>{ str.value = str.status_id; str.label = str.status_title })
                    objects.status_list.splice(0,0,{ value:0, label:'- Status -' })
                }
                if(objects.labels_list.length){
                    objects.labels_list.forEach((str)=>{ str.value = str.label_id; str.label = str.label_title })
                    objects.labels_list.splice(0,0,{ value:0, label:'- Labels -' })
                }
                if(objects.client_list.length){
                    objects.client_list.forEach((str)=>{ str.value = str.client_id; str.label = str.client_company_name })
                    objects.client_list.splice(0,0,{ value:0, label:'- Client -' })
                }
                result[i] = objects;
            }
        }

        response.result = result;
    } catch (error) {
        console.log({ error, file:"crmProject", scope:"getfilterdata" });
        response.error = error;
    }

    return response;
};

exports.storeLabelProject = async (field, params) => {
    let response = { error: false, result:[] };
    try {
        let result = null;
        let arrField = FieldLabel;
        let tmpData = [];
        let tmpField = [];
        let table = 'crm_labels';

        for(let i=0; i < arrField.length; i++){
            if(field[arrField[i]] !== undefined){
                if(params.status === 'store'){
                    tmpField.push(`\`${arrField[i]}\``);
                    tmpData.push(field[arrField[i]]);
                }else{
                    tmpData.push(`${arrField[i]}="${field[arrField[i]]}"`);
                }
            } 
        }

        let query = '';
        if(params.status === 'store'){
            query = `
                INSERT INTO ${table}(${tmpField.toString()})
                value(?);
            `;
            result = await db.query(query,[tmpData]);
        }else{
            query = `
            UPDATE ${table} SET ${tmpData.toString()} WHERE id=?;
            `;
            result = await db.query(query,[field.id]);
        }

        response.result = 'success';
    } catch (error) {
        response.result = [];
        response.error = error;
        console.log(error)
    }
    return response;
};
exports.storeMilestone = async (field, params) => {
    let response = { error: false, result:[] };
    try {
        let result = null;
        let arrField = FieldMilestone;
        let tmpData = [];
        let tmpField = [];
        let table = 'crm_milestones';

        for(let i=0; i < arrField.length; i++){
            if(field[arrField[i]] !== undefined){
                if(params.status === 'store'){
                    tmpField.push(`\`${arrField[i]}\``);
                    tmpData.push(field[arrField[i]]);
                }else{
                    tmpData.push(`${arrField[i]}="${field[arrField[i]]}"`);
                }
            } 
        }

        let query = '';
        if(params.status === 'store'){
            query = `
                INSERT INTO ${table}(${tmpField.toString()})
                value(?);
            `;
            result = await db.query(query,[tmpData]);
        }else{
            query = `
            UPDATE ${table} SET ${tmpData.toString()} WHERE id=?;
            `;
            result = await db.query(query,[field.id]);
        }

        response.result = 'success';
    } catch (error) {
        response.result = [];
        response.error = error;
        console.log(error)
    }
    return response;
};
exports.getProjectsMilestone = async (field, params) => {
    let response = { error: false, result:[] }
    try {
        let result = [];
        if(field.select){
            const query = `
                SELECT 
                
                (
                    SELECT GROUP_CONCAT(cm.id,'|',cm.title,'|',cm.project_id,'|',cm.due_date)
                    FROM crm_milestones cm
                    left join crm_projects cpm on cpm.id = cm.project_id and cm.deleted=0
                    left join user u on u.user_id = cpm.created_by and cpm.deleted=0
                    left join company c on c.company_id = u.company_id
                    WHERE c.company_id='${field.company_id}' 
                    ${
                        field.project_id ? ` and cm.project_id='${field.project_id}' ` : ''
                    } 
                ) as milestone_byProject_list
                
                FROM DUAL;
            `;
    
            result = await db.query(query);
        }
        
        if(result.length){
            for(let i=0; i < result.length; i++){
                let list = result[i];
                let objects = {
                    milestone_byProject_list:[],
                }
    
                objects.milestone_byProject_list = await parseString(list.milestone_byProject_list,'milestone_byProject_list',1);

                // add value and label
                if(objects.milestone_byProject_list.length){
                    objects.milestone_byProject_list.forEach((str)=>{ str.value = str.milestone_id; str.label = str.milestone_title })
                    objects.milestone_byProject_list.splice(0,0,{ value:0, label:'- Milestone -' })
                }
                result[i] = objects;
            }
        }

        response.result = result;
    } catch (error) {
        console.log({ error, file:"crmProject", scope:"get milestone" });
        response.error = error;
    }

    return response;
};
exports.storeProject = async (field, params) => {
    let response = { error: false, result: {} }
    try {
        let result = null;
        let arrField = FieldProject;
        let tmpData = [];
        let tmpField = [];
        for(let i=0; i < arrField.length; i++){
            try {
                if(field[arrField[i]] !== undefined){

                    if(["labels"].indexOf(arrField[i]) > -1 && field.project_id){
                            let dataRec = field[arrField[i]].length ? field[arrField[i]].toString():null;
                            tmpField.push(`\`${arrField[i]}\``);
                            tmpData.push(dataRec);
                    }else{
                        tmpField.push(`\`${arrField[i]}\``);
                        tmpData.push(field[arrField[i]]);
                    }
                    
                }
            } catch (error) {}
        }

        const query = `
            INSERT INTO crm_projects(${tmpField.toString()})
            value(?);
        `;
        result = await db.query(query,[tmpData]);

        if(result.insertId){

            response.result.newIdProject = result.insertId;
            if(!params.subtasks){
                field.id = null;
                field.changes = null;
                field.action_title = 'Added';
                field.action = 'created';
                field.log_type = 'task';
                field.log_type_id = 0;
                field.log_for = 'project';
                field.log_for_id = result.insertId;
        
                // let log = await addActivityTask(field,null);
                // if(log.error){
                //     response = log
                //     db.query('DELETE FROM crm_projects where id=?',[result.insertId]);
                //     //return { error: true, result: log.error };
                // }
            }
        }else{
            response.error = true;
        }
    } catch (error) {
        console.log({ error, file:'crmProject',scope:'storeProject' });
        response.error = error;
    }
    return response;
};

exports.editProject = async (field, params) => {
    let response = { error: false, result:[] };
    try {
        let arrField = FieldProject;
        let tmpData = [];
        for(let i=0; i < arrField.length; i++){
            try {                
                if(field[arrField[i]] !== undefined){
                    if(["labels"].indexOf(arrField[i]) > -1 && field.project_id){
                        if(field[arrField[i]]){
                            tmpData.push(`${arrField[i]}="${field[arrField[i]].toString()}"`);
                        }else{
                            tmpData.push(`${arrField[i]}=null`);
                        }
                    }
                    else{
                        tmpData.push(`${arrField[i]}="${field[arrField[i]]}"`);
                    }
                } 
            } catch (error) {
                
            }
        }

        field.id = field.id;
        field.project_id = field.id;
        field.changes = null;
        field.action_title = 'Updated';
        field.action = 'updated';
        field.log_type = 'task';
        field.log_type_id = 0;
        field.log_for = 'project';
        field.log_for_id = field.id;

        //response.result = field;
        response = await addActivityTask(field,null);
        const query = `
            UPDATE crm_projects SET ${tmpData.toString()} where id=? ;
        `;
        //console.log(query,field)
        const result = db.query(query,[field.id]);
        response.result = field.id;

    } catch (error) {
        console.log(error);
        response.error = true;
        response.result = error;
    }
    return response;
};


exports.cloneTaskProject = async (field, attribute) => {
    const idProjectNew = field.newIdProject;
    const idProjectToCopy = field.oldIdProject;

    const start_date_project = attribute.change_task_date ? `(SELECT '${field.start_date_project}')` : `ct2.start_date`;
    const deadline_project = attribute.change_task_date ? `(SELECT '${field.deadline_project}')` : `ct2.deadline`;
    const assignee = attribute.copy_assignee ? `ct2.collaborators` : `(SELECT NULL)`;
    const task_status = attribute.move_task_status ? `(SELECT '${'to_do'}')` : `ct2.status`;
    const task_status_id = attribute.move_task_status ? `(SELECT ${'1'})` : `ct2.status_id`;

    let response = { error: false, result: [] }

    try {
        const query = `
        insert into crm_tasks ( title, description, project_id, milestone_id, assigned_to, deadline, labels, points, status, status_id, priority_id, start_date, collaborators, sort, recurring, repeat_every, repeat_type, no_of_cycles, recurring_task_id, no_of_cycles_completed, blocking, blocked_by, parent_task_id, next_recurring_date, reminder_date, ticket_id,status_changed_at, deleted
            ) select ct2.title, ct2.description, (SELECT ${idProjectNew}), ct2.milestone_id, ct2.assigned_to, ${deadline_project}, ct2.labels, ct2.points, ${task_status}, ${task_status_id}, ct2.priority_id, ${start_date_project}, ${assignee}, ct2.sort, ct2.recurring, ct2.repeat_every, ct2.repeat_type, ct2.no_of_cycles, ct2.recurring_task_id, ct2.no_of_cycles_completed, ct2.blocking, ct2.blocked_by, ct2.parent_task_id, ct2.next_recurring_date, ct2.reminder_date, ct2.ticket_id, ct2.status_changed_at, ct2.deleted
             from crm_tasks ct2 where ct2.project_id=?;
            `;
            result = await db.query(query, [idProjectToCopy]);
            console.log(query,'?')
            if(result.insertId){

            }else{
                //could be no task with this project id
                response.error = false;
            }
    } catch (error) {
        console.log(error,'?')
        response.error = error;
    }
    return response;
};

exports.cloneMilestoneProject = async (field, params) => {
    const idProjectNew = field.newIdProject;
    const idProjectToCopy = field.oldIdProject;
    let response = { error: false, result: [] }

    try {
        const query = `
        INSERT INTO crm_milestones (title, project_id, due_date, description, deleted) SELECT cm.title, (SELECT ${idProjectNew}), cm.due_date, cm.description, cm.deleted FROM crm_milestones cm WHERE project_id = ?;
            `;
    
            result = await db.query(query, [idProjectToCopy]);
            console.log(result,'milestone')

            if(result.insertId){

            }else{
                response.error = false;
            }
    } catch (error) {
        response.error = error;
    }
    return response;
};

exports.deleteProjects = async (field, params) => {
    let response = { error: false, result:[] };
    try {
        // response = await addActivityTask(field,null);
        const query = `
            UPDATE crm_projects SET deleted = 1 where id=? ;
        `;

        const result = db.query(query,[field.id]);
        response.result = 'Success Remove Project';

    } catch (error) {
        response.error = true;
        response.result = error;
    }
    return response;
};

exports.storeProjectMember = async (field, params) => {
    let response = { error: false, result:[] };
    try {

        // query optional error for unknown temporary table
//         const queryOptional = `
        
// BEGIN;
// DROP TEMPORARY TABLE crm_member_tmp;
// CREATE TEMPORARY TABLE crm_member_tmp(user_id INT(11), project_id INT(11), is_leader INT(1));
// INSERT INTO crm_member_tmp ( user_id, project_id, is_leader ) VALUES ?;
 
// INSERT INTO crm_project_members ( user_id, project_id, is_leader ) SELECT
// user_id,
// project_id,
// is_leader 
// FROM
// 	crm_member_tmp cmt
// WHERE
// 	NOT EXISTS (SELECT * FROM crm_project_members cpm WHERE cmt.user_id = cpm.user_id AND cmt.project_id = cpm.project_id AND cmt.is_leader = cpm.is_leader);
	
// COMMIT;`
        const query = `
            INSERT INTO crm_project_members (user_id, project_id, is_leader) VALUES ? ;
        `;
        const result = db.query(query,[field.idUser.map(user => [user, field.idProject, 0])]);
        response.result = "Success Stored Project";

    } catch (error) {
        console.log(error);
        response.error = true;
        response.result = error;
    }
    return response;
};

exports.removeProjectMember = async (field, params) => {
    let response = { error: false, result:[] };
    try {
        const query = `
            UPDATE crm_project_members SET deleted = 1 WHERE user_id = ? AND project_id = ? ;
        `;
        //console.log(query,field)
        const result = db.query(query,[field.idUser, field.idProject]);
        response.result = 'Success Remove Member Project';

    } catch (error) {
        console.log(error);
        response.error = true;
        response.result = error;
    }
    return response;
};

exports.getProjectMember = async (field, params) => {
    let response = { error: false, result:[] };
    try {
        const query = `
        SELECT
	cpm.*,
	usr.name,usr.avatar, usr.level
FROM
	crm_project_members cpm 
LEFT JOIN user usr ON usr.user_id = cpm.user_id
WHERE
	cpm.project_id = ? AND cpm.deleted = 0;
        ;
        `;
        //console.log(query,field)
        const result = await db.query(query,[field.idProject]);
        response.result = result;

    } catch (error) {
        console.log(error);
        response.error = true;
        response.result = error;
    }
    return response;
};

exports.cloneMembers = async(field, params)=>{
    let response = { error:false, result:[] }
    try {
        let cloneMembers = await db.query(`INSERT INTO crm_project_members(user_id, project_id) 
        SELECT distinct c.user_id, '${field.newIdProject}' AS 'project_id' FROM crm_project_members c 
        where c.project_id = '${field.oldIdProject}';`);
        response.result = cloneMembers;
    } catch (error) {
        console.log(error)
        response.error = error;
    }
    return response
}   

exports.getProjectNotes = async (field) => {
    let response = { error: false, result:[] };
    try {
        const query = `SELECT
        cm.*,
        cnf.file_location,
        cnf.title as title_file,
        cnf.id as id_file,
        cl.title as title_label,
        cl.color,
        cl.id as id_label
    FROM
        crm_notes cm
        LEFT JOIN crm_notes_files cnf ON cnf.id_crm_notes = cm.id 
        LEFT JOIN crm_labels cl ON cl.id = cm.labels
    WHERE
        cm.deleted = 0 AND cm.project_id=?;`;
        let result = await db.query(query,[field.idProject]);
        let final = [];


        if(result.length) {
            result.forEach((note) => {
                const indexID = final.findIndex(v => v.id === note.id);
                  if(indexID >= 0) {
                    final[indexID].file_location.push({id: note.id_file, file_location: note.file_location, title_file: note.title_file});
                  }else{
                    final.push({...note, file_location: note.file_location ? [{id: note.id_file, file_location: note.file_location, title_file: note.title_file}] : []})
                  }
              })
        }
        response.result = final;

    } catch (error) {
        response.error = true;
        response.result = error;
    }
    return response;
};

exports.postProjectNotes = async (dto) => {
    let response = { error: false, result:[] };

    const objQuery = [[dto.created_by, moment(new Date()).format('YYYY-MM-DD HH:mm:ss'), dto.title, dto.description, dto.project_id, dto.client_id || null, dto.user_id, dto.labels || null, dto.is_public]];
    try {
        const query = `
            INSERT INTO crm_notes (created_by, created_at, title, description, project_id, client_id, user_id, labels, is_public) VALUE(?) ;
        `;
        const result = await db.query(query, objQuery);
        response.result = result;

    } catch (error) {
        response.error = true;
        response.result = error;
    }
    return response;
};

exports.postFileProjectNotes = async (dto) => {
    let response = { error: false, result:[] };

    const objQuery = [[dto.idNotes, dto.file_location, dto.title]];
    try {
        const query = `
            INSERT INTO crm_notes_files (id_crm_notes, file_location, title) VALUE(?) ;
        `;
        const result = await db.query(query, objQuery);
        response.result = result;

    } catch (error) {
        response.error = true;
        response.result = error;
    }
    return response;
};

exports.updateProjectNotes = async (dto) => {
    let response = { error: false, result:[] };
    const objQuery = [dto.title, dto.description, dto.project_id, dto.client_id || null, dto.user_id, dto.labels || null, dto.is_public, dto.id];
    const rawQuery = `UPDATE crm_notes SET title=?, description=?, project_id=?, client_id=?, user_id=?, labels=?, is_public=? WHERE id=?;`

    try {
        const result = await db.query(rawQuery, objQuery);
        response.result = result;

    } catch (error) {
        response.error = true;
        response.result = error;
    }
    return response;
};

exports.removeProjectNotes = async (dto) => {
    let response = { error: false, result:[] };

    const objQuery = [dto.id];
    try {
        const query = `
            UPDATE crm_notes SET deleted=1 WHERE id=?;
        `;
        const result = await db.query(query, objQuery);
        response.result = result;

    } catch (error) {
        response.error = true;
        response.result = error;
    }
    return response;
};

exports.removeProjectNotesFileByIdNotes = async (dto) => {
    let response = { error: false, result:[] };

    const objQuery = [dto.id];
    try {
        const query = `
            DELETE FROM crm_notes_files WHERE id_crm_notes=?;
        `;
        const result = await db.query(query, objQuery);
        response.result = result;

    } catch (error) {
        response.error = true;
        response.result = error;
    }
    return response;
};

exports.removeProjectNotesFile = async (dto) => {
    let response = { error: false, result:[] };

    const objQuery = [dto.id];
    try {
        const query = `
            DELETE FROM crm_notes_files WHERE id=?;
        `;
        const result = await db.query(query, objQuery);
        response.result = result;

    } catch (error) {
        response.error = true;
        response.result = error;
    }
    return response;
};
