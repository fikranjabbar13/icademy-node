const mongoose = require('mongoose'),
  Schema = mongoose.Schema;

// mongoose.connect('mongodb://icademy.stg.kelola.co.id:27017/jigasi', {
//   useNewUrlParser: true,
//   useCreateIndex: true,
//   useUnifiedTopology: true,
// });

const transcriptsSchema = new Schema(
    {
      path: { type: String, index: true, unique: true },
      room_name: { type: String },
      start_time: { type: Date },
      end_time: { type: Date },
      events: [
        {
          event: { type: String },
          participant: {
            avatar_url: { type: String },
            name: { type: String },
            id: { type: String },
          },
          message_id: { type: String },
          transcript: [
            {
              confidence: { type: Number },
              text: { type: String },
            },
          ],
          language: { type: String },
          stability: { type: Number },
          timestamps: { type: Date },
        },
      ],
    },
    {
      timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at',
      },
    }
  ),
  transcripts = mongoose.model('transcriptsJson', transcriptsSchema);

module.exports = transcripts;
