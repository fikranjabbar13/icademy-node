const mongoose = require('mongoose'),
  transcriptsSchema = new mongoose.Schema(
    {
      path: { type: String, index: true, unique: true },
      room_name: { type: String },
      room_date: { type: String },
      room_time: { type: String },
      transcript_end: { type: String },
      detail: [{ type: { type: String } }],
    },
    {
      timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at',
      },
    }
  ),
  transcripts = mongoose.model('transcriptsTxt', transcriptsSchema);

module.exports = transcripts;
