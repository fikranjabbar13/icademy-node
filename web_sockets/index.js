exports.socket = (io) => {
	io.set('origins', '*');
	io.on('connection', (socket) => {
		console.log('New user connected.');

		socket.on('send', (data) => {
			io.emit('broadcast', data);
		});

		socket.on('disconnect', () => {
			console.log('User disconnect.');
		})
	});
}