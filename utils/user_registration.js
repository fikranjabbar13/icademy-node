const moduleDB = require('../repository');
const moment = require('moment');

async function FuncCheckRegistrationSchema (retrieveDetailRegistrationSchema) {
    if(retrieveDetailRegistrationSchema.length) {
        const dtoCountListUser = {
            idForm: retrieveDetailRegistrationSchema[0].id,
            period: retrieveDetailRegistrationSchema[0].close_registration,
            quota: retrieveDetailRegistrationSchema[0].slot
        }
        // Period is Not NULL Must Check Periode
        if(dtoCountListUser.period){
            // Untuk Cek Periode
            const thisDay = moment(new Date()).format('YYYY-MM-DD');
            const periodToday = moment(thisDay);
            const period = moment(dtoCountListUser.period);
            const isUserCanRegistered = period.diff(periodToday, 'days') >= 0;
            
            // Cek Periode Registrasi Open
            if(isUserCanRegistered) {
                // Get Promise All Untuk Get Count User Pending & Active
                const resultAllCount = await Promise.all([moduleDB.countUserRegistrationByIdFormAndStatus({...dtoCountListUser, status: 1}), moduleDB.countUserRegistrationByIdFormAndStatus({...dtoCountListUser, status: 0})])
                // User Pending
                const countUserPending = resultAllCount[0][0] ? resultAllCount[0][0].count_user : 0;
                // User Active
                const countUserActive = resultAllCount[1][0] ? resultAllCount[1][0].count_user : 0;
                const allUserPendingAndActive = countUserActive + countUserPending;
                //Cek Quota nya ada atau tidak
                if(dtoCountListUser.quota){
                    // Cek Quota Registrasi 
                    if(allUserPendingAndActive >= dtoCountListUser.quota) {
                        // Closed
                        retrieveDetailRegistrationSchema[0].isRegistrationClosed = true;
                    }else {
                        // Open
                        retrieveDetailRegistrationSchema[0].isRegistrationClosed = false;
                    }
                }
            }else {
                // Closed
                retrieveDetailRegistrationSchema[0].isRegistrationClosed = true;
            }
        }else {
            // Dont Have Periode Doesnt Need To Check
            retrieveDetailRegistrationSchema[0].isRegistrationClosed = false;
        }
    }

    return retrieveDetailRegistrationSchema;
}


module.exports = {
    FuncCheckRegistrationSchema: FuncCheckRegistrationSchema
}