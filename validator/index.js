module.exports = {
    //Grup
    postCreateGlobalSetting: require('./group').postCreateGlobalSetting,
    retrieveAccess: require('./group').retrieveAccess,

    //LiveClass
    postParticipants: require('./liveclass').postParticipants,

    //Training Exam
    importFileQuestions: require('./training_exam').importFileQuestions,
    deleteBulkTrainingExam: require('./training_exam').deleteBulkTrainingExam,
    retrieveRequestExamByCourse: require('./training_exam').retrieveRequestExamByCourse,

    //webinar
    sendEssay: require('./webinar').sendEssay,
    prepostTest: require('./webinar').prepostTest,
    guestRegistration: require("./webinar").guestRegistration,
    guestAction: require("./webinar").guestAction,
    setSpeaker: require("./webinar").setSpeaker,
    setSpeakerExternal: require("./webinar").setSpeakerExternal,
    delSpeakerExternal : require("./webinar").delSpeakerExternal,
    sendMailSpeakerExternal : require("./webinar").sendMailSpeakerExternal,
    retrieveOfferLetterRequest: require("./webinar").retrieveOfferLetterRequest,

    //Training Report
    validateDetailReportRequest: require('./training_report').validateDetailReportRequest,
    validateHistoryUserTrainingExam: require('./training_report').validateHistoryUserTrainingExam,

    //Training Course
    validateReportCourseByIdCourse: require('./training_course').validateReportCourseByIdCourse,
    validPostSubmission: require('./training_course').postSubmission,
    validGetSubmission: require('./training_course').getSubmission,
    validDeleteSubmission: require('./training_course').deleteSubmission,
    validAssigneeSubmission: require('./training_course').assigneeSubmission,
    validDeleteAssigneeSubmission: require('./training_course').deleteAssigneeSubmission,
    validateUserAnswerSubmission: require('./training_course').validateUserAnswerSubmission,
    validateGetUserAnswerSubmission: require('./training_course').validateGetUserAnswerSubmission,

    //Training User
    validateUpdateStatusUserBulkRequest: require('./training_user').validateUpdateStatusUserBulkRequest,
    validatePostRegistrationForm: require('./training_user').validatePostRegistrationForm,
    validateReadRegistrationUserByTrainingCompany: require('./training_user').validateReadRegistrationUserByTrainingCompany,
    validateActivateUserByIdUserRegistration: require('./training_user').validateActivateUserByIdUserRegistration,

    //Training Question
    validateUpdateStatusQuestionsBulkRequest: require('./training_question').validateUpdateStatusQuestionsBulkRequest,

    crmTask : require('./crmTask'),
    crmOperators : require('./crmOperators'),
}