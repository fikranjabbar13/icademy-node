const Joi = require('joi');

exports.validateReportCourseByIdCourse = async (input) => {

    const schema = Joi.object({
        idCourse: Joi.number().required(),
        idCompany: Joi.number().required()
    }).required();

    try {
        const response = await schema.validateAsync(input);
        return response;
    } catch (error) {
        throw error;
    }
};

exports.postSubmission = async (input) => {

    const schema = Joi.object({
        id: Joi.number().allow(null, false, 0),
        course_id: Joi.number().positive().required(),
        type: Joi.number().required(),
        title: Joi.string().allow('', null),
        content: Joi.string().allow(null, '', false),
        start_time_submission: Joi.date().allow(null, '', false),
        end_time_submission: Joi.date().allow(null, '', false),
        sort: Joi.number(),
        media: Joi.array()
    });

    try {
        const response = await schema.validateAsync(input);
        return response;
    } catch (error) {
        throw error;
    }
};

exports.assigneeSubmission = async (input) => {

    const schema = Joi.object({
        submission_id: Joi.number().allow(null, false, 0),
        course_id: Joi.number().positive().required(),
        training_user_id: Joi.number().positive().required(),
        action: Joi.string().valid("add", "delete"),
        created_by: Joi.number().positive().required()
    });

    try {
        const response = await schema.validateAsync(input);
        return response;
    } catch (error) {
        throw error;
    }
};

exports.deleteAssigneeSubmission = async (input) => {

    const schema = Joi.object({
        id: Joi.number().positive().required()
    });

    try {
        const response = await schema.validateAsync(input);
        return response;
    } catch (error) {
        throw error;
    }
};

exports.deleteSubmission = async (input) => {

    const schema = Joi.object({
        id: Joi.number().allow(null, false)
    });

    try {
        const response = await schema.validateAsync(input);
        return response;
    } catch (error) {
        throw error;
    }
};

exports.getSubmission = async (input) => {

    const schema = Joi.object({
        id: Joi.number().allow(null, false)
    });

    try {
        const response = await schema.validateAsync(input);
        return response;
    } catch (error) {
        throw error;
    }
};

exports.validateUserAnswerSubmission = async (input) => {
    const schema = Joi.object({
        idSubmission: Joi.number().required(),
        idTrainingUser: Joi.number().required(),
        // fileSubmission: Joi.string(),
        // fileSubmissionLocation: Joi.string(),
        textSubmission: Joi.string().allow(null, '')
    });

    try {
        const response = await schema.validateAsync(input);
        return response;
    } catch (error) {
        throw error;
    }
}

exports.validateGetUserAnswerSubmission = async (input) => {
    const schema = Joi.object({
        idSubmission: Joi.number().required(),
        idTrainingUser: Joi.number().required(),
    });

    try {
        const response = await schema.validateAsync(input);
        return response;
    } catch (error) {
        throw error;
    }
}

