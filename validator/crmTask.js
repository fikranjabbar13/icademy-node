const Joi = require('joi');
const moment = require('moment');
const now = moment(new Date()).format("YYYY-MM-DD");

exports.store = async (input) => {
    return new Promise(async (hasil)=>{
        let Field = {
            title : Joi.string().required(),
            description : Joi.string().allow('',null),
            project_id : Joi.number().allow(0,null),
            milestone_id: Joi.array().items(Joi.number()).allow(0,null),
            assigned_to : Joi.number().allow(0,null),
            start_date : Joi.date().allow(null),
            deadline : Joi.date().allow(null),
            labels : Joi.array().items(Joi.number()).allow(0,null),
            points : Joi.number().allow(0,null),
            status : Joi.string().allow(null),
            status_id : Joi.number().allow(0,null),
            priority_id : Joi.number().allow(0,null),
            collaborators : Joi.array().items(Joi.number()).allow(0,null),
            blocking:Joi.array().items(Joi.number()).allow(0,null),
            blocked_by:Joi.array().items(Joi.number()).allow(0,null),
            parent_task_id:Joi.number().allow(0,null),
            sort:Joi.number().allow(0,null),

            operator_id:Joi.number().allow(0,null),
            operation_status: Joi.string().allow('',null),
            last_hour_operator: Joi.string().allow('',null),
            activity_status: Joi.string().allow('',null),
            
            well_type_condition: Joi.string().valid('Oil','Gas','Dry').allow(null),
            achieved_target:Joi.number().allow(0,null),
            hole_depth:Joi.number().allow(0,null),

            wells_category : Joi.number().allow(0,null),
            drilling_gross : Joi.number().allow(0,null),
            drilling_day : Joi.number().allow(0,null),
            cost : Joi.number().allow(0,null),
            npt : Joi.number().allow(0,null),
            npt_hrs : Joi.number().allow(0,null),
            target_start : Joi.number().allow(0,null),
            target_end : Joi.number().allow(0,null),
            next_target: Joi.string().allow('',null),
            predrill_gross: Joi.number().allow(0,null),
            predrill_net : Joi.number().allow(0,null),
            predrill_porosity : Joi.number().allow(0,null),
            predrill_saturation : Joi.number().allow(0,null),
            predrill_hc : Joi.number().allow(0,null),
            postdrill_gross: Joi.number().allow(0,null),
            postdrill_net : Joi.number().allow(0,null),
            postdrill_porosity : Joi.number().allow(0,null),
            postdrill_saturation : Joi.number().allow(0,null),
            postdrill_hc : Joi.number().allow(0,null),
        }
        const schema = Joi.object(Field);
    
        try {
            const response = await schema.validateAsync(input);
            return hasil({ error:false,result: response });
        } catch (error) {
            return hasil({ error:error.details[0].message,result: false });
        }
    });
};

exports.storeImport = async (input) => {
    return new Promise(async (hasil)=>{
        let Field = {
            data: Joi.array().items({
                title : Joi.string().required(),
                description : Joi.string().allow('',null),
                project_id : Joi.number().allow(0,null),
                milestone_id: Joi.array().items(Joi.number()).allow(0,null),
                assigned_to : Joi.number().allow(0,null),
                start_date : Joi.date().allow(null),
                deadline : Joi.date().allow(null),
                labels : Joi.array().items(Joi.number()).allow(0,null),
                points : Joi.number().allow(0,null),
                status : Joi.string().allow(null),
                status_id : Joi.number().allow(0,null),
                priority_id : Joi.number().allow(0,null),
                collaborators : Joi.array().items(Joi.number()).allow(0,null),
                blocking:Joi.array().items(Joi.number()).allow(0,null),
                blocked_by:Joi.array().items(Joi.number()).allow(0,null),
                parent_task_id:Joi.number().allow(0,null),
                sort:Joi.number().allow(0,null),
                
                operator_id:Joi.number().allow(0,null),
                operation_status: Joi.string().allow('',null),
                last_hour_operator: Joi.string().allow('',null),
                activity_status: Joi.string().allow('',null),
                
                well_type_condition: Joi.string().valid('Oil','Gas','Dry').allow(null),
                achieved_target:Joi.number().allow(0,null),
                hole_depth:Joi.number().allow(0,null),
                
                wells_category : Joi.number().allow(0,null),
                drilling_gross : Joi.number().allow(0,null),
                drilling_day : Joi.number().allow(0,null),
                cost : Joi.number().allow(0,null),
                npt : Joi.number().allow(0,null),
                npt_hrs : Joi.number().allow(0,null),
                target_start : Joi.number().allow(0,null),
                target_end : Joi.number().allow(0,null),
                next_target: Joi.string().allow('',null),
                predrill_gross: Joi.number().allow(0,null),
                predrill_net : Joi.number().allow(0,null),
                predrill_porosity : Joi.number().allow(0,null),
                predrill_saturation : Joi.number().allow(0,null),
                predrill_hc : Joi.number().allow(0,null),
                postdrill_gross: Joi.number().allow(0,null),
                postdrill_net : Joi.number().allow(0,null),
                postdrill_porosity : Joi.number().allow(0,null),
                postdrill_saturation : Joi.number().allow(0,null),
                postdrill_hc : Joi.number().allow(0,null),
            }).required()
        }
        const schema = Joi.object(Field);
    
        try {
            const response = await schema.validateAsync(input);
            return hasil({ error:false,result: response });
        } catch (error) {
            return hasil({ error:error.details[0].message,result: false });
        }
    });
};

exports.edit = async (input,singleInput) => {
    return new Promise(async (hasil)=>{
        let Field = {
            title : Joi.string().required(),
            description : Joi.string().allow('',null),
            project_id : Joi.number().allow(0,null),
            milestone_id: Joi.array().items(Joi.number()).allow(0,null),
            assigned_to : Joi.number().allow(0,null),
            start_date : Joi.date().allow(null),
            deadline : Joi.date().allow(null),
            labels : Joi.array().items(Joi.number()).allow(0,null),
            points : Joi.number().allow(0,null),
            status : Joi.string().allow(null),
            status_id : Joi.number().allow(0,null),
            priority_id : Joi.number().allow(0,null),
            collaborators : Joi.array().items(Joi.number()).allow(0,null),
            blocking:Joi.array().items(Joi.number()).allow(0,null),
            blocked_by:Joi.array().items(Joi.number()).allow(0,null),
            parent_task_id:Joi.number().allow(0,null),
            sort:Joi.number().allow(0,null),
            
            operator_id:Joi.number().allow(0,null),
            operation_status: Joi.string().allow('',null),
            last_hour_operator: Joi.string().allow('',null),
            activity_status: Joi.string().allow('',null),
            
            well_type_condition: Joi.string().valid('Oil','Gas','Dry').allow(null),
            achieved_target:Joi.number().allow(0,null),
            hole_depth:Joi.number().allow(0,null),
            
            wells_category : Joi.number().allow(0,null),
            drilling_gross : Joi.number().allow(0,null),
            drilling_day : Joi.number().allow(0,null),
            cost : Joi.number().allow(0,null),
            npt : Joi.number().allow(0,null),
            npt_hrs : Joi.number().allow(0,null),
            target_start : Joi.number().allow(0,null),
            target_end : Joi.number().allow(0,null),
            next_target: Joi.string().allow('',null),
            predrill_gross: Joi.number().allow(0,null),
            predrill_net : Joi.number().allow(0,null),
            predrill_porosity : Joi.number().allow(0,null),
            predrill_saturation : Joi.number().allow(0,null),
            predrill_hc : Joi.number().allow(0,null),
            postdrill_gross: Joi.number().allow(0,null),
            postdrill_net : Joi.number().allow(0,null),
            postdrill_porosity : Joi.number().allow(0,null),
            postdrill_saturation : Joi.number().allow(0,null),
            postdrill_hc : Joi.number().allow(0,null),
        }
        Field.id = Joi.number().allow(0,null);
        if(singleInput != 'null'){
            if(singleInput === 'status'){
                Field = { id:Field.id, [singleInput] : Field[singleInput], status_id: Field.status_id }
            }else{
                Field = { id:Field.id, [singleInput] : Field[singleInput] }
            }
        }
    
        let schema = Joi.object(Field);
    
        try {
            const response = await schema.validateAsync(input);
            return hasil({ error:false,result: response });
        } catch (error) {
            return hasil({ error:error.details[0].message,result: false });
        }
    })
};

exports.editBatch = async (input,singleInput) => {
    return new Promise(async (hasil)=>{
        let Field = {
            project_id: Joi.number().required(),
            milestone_id: Joi.array().items(Joi.number()).allow(0,null),
            assigned_to : Joi.number().allow(0,null),
            start_date : Joi.date().allow(null),
            deadline : Joi.date().allow(null),
            labels : Joi.array().items(Joi.number()).allow(0,null),
            status : Joi.string().allow(null),
            status_id : Joi.number().allow(0,null),
            collaborators : Joi.array().items(Joi.number()).allow(0,null),
            selectedTask: Joi.array().items(Joi.number()).required(),
            'operation_status': Joi.string().allow('',null),
            'last_hour_operator': Joi.string().allow('',null),
            'activity_status': Joi.string().allow('',null),
            wells_category : Joi.number().allow(0,null),
            drilling_gross : Joi.number().allow(0,null),
            drilling_day : Joi.number().allow(0,null),
            cost : Joi.number().allow(0,null),
            npt : Joi.number().allow(0,null),
            npt_hrs : Joi.number().allow(0,null),
            target_start : Joi.number().allow(0,null),
            target_end : Joi.number().allow(0,null),
            next_target: Joi.string().allow('',null),
            predrill_gross: Joi.number().allow(0,null),
            predrill_net : Joi.number().allow(0,null),
            predrill_porosity : Joi.number().allow(0,null),
            predrill_saturation : Joi.number().allow(0,null),
            predrill_hc : Joi.number().allow(0,null),
            postdrill_gross: Joi.number().allow(0,null),
            postdrill_net : Joi.number().allow(0,null),
            postdrill_porosity : Joi.number().allow(0,null),
            postdrill_saturation : Joi.number().allow(0,null),
            postdrill_hc : Joi.number().allow(0,null),
        }
    
        let schema = Joi.object(Field);
    
        try {
            if(!input.milestone_id && !input.assigned_to && !input.start_date && !input.deadline && !input.labels && !input.collaborators && !input.status){
                return hasil({ error:'No selected field',result: false });
            }else{
                const response = await schema.validateAsync(input);
                return hasil({ error:false,result: response });
            }
        } catch (error) {
            return hasil({ error:error.details[0].message,result: false });
        }
    })
};

exports.editKanban = async (input,singleInput) => {
    return new Promise(async (hasil)=>{
        let Field = {
            id: Joi.number().required(),
            status : Joi.string().allow(null).required(),
            status_id : Joi.number().allow(0,null).required(),
            sort:Joi.number().allow(0,null).required(),
        }
    
        let schema = Joi.array().items(Joi.object(Field));
    
        try {
            const response = await schema.validateAsync(input);
            return hasil({ error:false,result: response });
        } catch (error) {
            return hasil({ error:error.details[0].message,result: false });
        }
    })
};

exports.storeTaskComment = async (input) => {
    return new Promise(async (hasil)=>{
        let FieldComment = {
            description : Joi.string().required(),
            task_id: Joi.number().allow(0,null),
            project_id : Joi.number().allow(0,null),
            comment_id : Joi.number().allow(0,null),
            files:Joi.string().allow(null),
            created_by: Joi.number().allow(0,null)
        }
        const schema = Joi.object(FieldComment);
    
        try {
            const response = await schema.validateAsync(input);
            return hasil({ error:false,result: response });
        } catch (error) {
            return hasil({ error:error.details[0].message,result: false });
        }
    });
};

exports.storeTaskReminder = async (input) => {
    return new Promise(async (hasil)=>{
        let FieldReminder = {
            title: Joi.string().required(),
            task_id: Joi.number().allow(0,null),
            project_id : Joi.number().allow(0,null),
            start_date: Joi.string().allow(null),
            start_time: Joi.string().allow(null),
            type:Joi.string().valid('reminder','event').required(),
            repeat_every:Joi.number().valid(0,1),
            repeat_type:Joi.string().valid('days','weeks','months','years').allow(null),
            no_of_cycles: Joi.number().min(1).allow(null),
            created_by: Joi.number().allow(0,null)
        }
        const schema = Joi.object(FieldReminder);
    
        try {
            const response = await schema.validateAsync(input);
            return hasil({ error:false,result: response });
        } catch (error) {
            return hasil({ error:error.details[0].message,result: false });
        }
    });
};

exports.deleteTaskReminder = async (input) => {
    return new Promise(async (hasil)=>{

        const schema = Joi.object({ 
            id: Joi.number().min(1).required(), 
            deleted: Joi.number().valid(0,1).required(),
            created_by: Joi.number().min(1).required(),
            type: Joi.string().valid('reminder').required()
        });
    
        try {
            const response = await schema.validateAsync(input);
            return hasil({ error:false,result: response });
        } catch (error) {
            return hasil({ error:error.details[0].message,result: false });
        }
    });
};

exports.deleteTaskComment = async (input) => {
    return new Promise(async (hasil)=>{

        const schema = Joi.object({ 
            id: Joi.number().min(1).required(), 
            deleted: Joi.number().valid(0,1).required(),
            created_by: Joi.number().min(1).required()
        });
    
        try {
            const response = await schema.validateAsync(input);
            return hasil({ error:false,result: response });
        } catch (error) {
            return hasil({ error:error.details[0].message,result: false });
        }
    });
};

exports.storeTaskChecklistItem = async (input) => {
    return new Promise(async (hasil)=>{
        let FieldChecklistItem = {
            id: Joi.number().allow(0,null),
            title: Joi.string().required(),
            is_checked: Joi.number().valid(0,1).allow(null),
            task_id: Joi.number().required(),
            sort: Joi.number(),
            deleted:Joi.number()
        }
        const schema = Joi.array().items(Joi.object(FieldChecklistItem));
    
        try {
            const response = await schema.validateAsync(input);
            return hasil({ error:false,result: response });
        } catch (error) {
            return hasil({ error:error.details[0].message,result: false });
        }
    });
};

exports.storeTaskCommentAttribute = async (input) => {   
    return new Promise(async (hasil)=>{
        let FieldCommentAttribute = {
            attType : Joi.string().valid('pin','like').required(),
            project_comment_id: Joi.number().min(1).required(),
            created_by: Joi.number().required()
        }
        try {
            if(input.id){
                input.deleted = 1;
                FieldCommentAttribute.id = Joi.number().min(1).required();
                FieldCommentAttribute.deleted = Joi.number().min(1).required();
            }
            const schema = Joi.object(FieldCommentAttribute);
            const response = await schema.validateAsync(input);
            
            return hasil({ error:false,result: response });
        } catch (error) {
            return hasil({ error:error.details[0].message,result: false });
        }
    }); 
};

exports.storeTaskLogged = async (input) => {   
    return new Promise(async (hasil)=>{
        let FieldLogged = {
            project_id : Joi.number().min(0).required(),
            task_id: Joi.number().min(1).required(),
            status: Joi.string().valid('open','logged').required(),
            note: Joi.string().allow(null),
            user_id:Joi.number().min(1).required()
        }
        try {

            const schema = Joi.object(FieldLogged);
            const response = await schema.validateAsync(input);
            
            return hasil({ error:false,result: response });
        } catch (error) {
            return hasil({ error:error.details[0].message,result: false });
        }
    }); 
};

exports.storeProjectNotes = async (input,singleInput) => {
    return new Promise(async (hasil)=>{
        let Field = {
            created_by: Joi.number().required(),
            title : Joi.string().required(),
            description : Joi.string().required(),
            project_id:Joi.number().required(),
            user_id:Joi.number().required(),
            labels:Joi.number().allow('').required(),
            files:Joi.array().allow('', null).required(),
            is_public:Joi.number().valid(1, 0).required(),
        }
    
        let schema = Joi.object(Field);
    
        try {
            const response = await schema.validateAsync(input);
            return hasil({ error:false,result: response });
        } catch (error) {
            return hasil({ error:true ,result: error.details[0].message });
        }
    })
};

exports.updateProjectNotes = async (input,singleInput) => {
    return new Promise(async (hasil)=>{
        let Field = {
            id: Joi.number().required(),
            created_by: Joi.number().required(),
            title : Joi.string().required(),
            description : Joi.string().required(),
            project_id:Joi.number().required(),
            user_id:Joi.number().required(),
            labels:Joi.number().allow('', null).required(),
            files:Joi.array().allow('', null).required(),
            is_public:Joi.number().valid(1, 0).required(),
        }
    
        let schema = Joi.object(Field);
    
        try {
            const response = await schema.validateAsync(input);
            return hasil({ error:false,result: response });
        } catch (error) {
            return hasil({ error:true ,result: error.details[0].message });
        }
    })
};