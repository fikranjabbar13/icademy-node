const Joi = require('joi');

exports.validateUpdateStatusQuestionsBulkRequest = async (detailObject) => {

    const schema = Joi.object({
        dataIdQuestions: Joi.array().items(Joi.number()).required(),
        status: Joi.string().valid('Active', 'Inactive')
    }).required();

    try {
        const response = await schema.validateAsync(detailObject);
        return response;
    } catch (error) {
        throw error;
    }
};
