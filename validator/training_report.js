const Joi = require('joi');

exports.validateDetailReportRequest = async (detailObject) => {

    const schema = Joi.object({
        idCompany: Joi.number().required(),
        grupName: Joi.string().required(),
        start: Joi.date(),
        end: Joi.date(),
        licenses_type: Joi.array().items(Joi.number()),
        company: Joi.number(),
        training_company: Joi.array().items(Joi.number()),
        pass: Joi.array().items(Joi.string()),
        cert: Joi.array().items(Joi.string()),
    }).required();

    try {
        const response = await schema.validateAsync(detailObject);
        return response;
    } catch (error) {
        throw error;
    }
};

exports.validateHistoryUserTrainingExam = async (detailObject) => {

    const schema = Joi.object({
        idTrainingUser: Joi.number().required(),
    }).required();

    try {
        const response = await schema.validateAsync(detailObject);
        return response;
    } catch (error) {
        throw error;
    }
};