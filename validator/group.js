const Joi = require('joi');

exports.postCreateGlobalSetting = async (detailCreateGlobalSetting) => {

    const schema = Joi.object({
        sub: Joi.string().required(),
        role: Joi.string().valid('admin', 'sekretaris', 'moderator', 'speaker', 'participant', 'training'),
        code: Joi.string().required(),
        name: Joi.string().required(),
        status: Joi.number().required()
    }).required();

    try {
        const response = await schema.validateAsync(detailCreateGlobalSetting);
        return response;
    } catch (error) {
        throw error;
    }
};

exports.retrieveAccess = async (detailAccess) => {

    // Role Client Default Client is Empty
    const schema = Joi.object({
        company_id: Joi.string().required(),
        role: Joi.string().valid('admin', 'sekretaris', 'moderator', 'speaker', 'participant', 'training', 'client', 'user'),
        code: Joi.array().items(Joi.string()),
    }).required();

    try {
        const response = await schema.validateAsync(detailAccess);
        return response;
    } catch (error) {
        throw error;
    }
};