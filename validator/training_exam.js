const Joi = require('joi');

exports.importFileQuestions = async (detailObject) => {

    const schema = Joi.object({
        exam_id: Joi.number().required(),
    }).required();

    try {
        const response = await schema.validateAsync(detailObject);
        return response;
    } catch (error) {
        throw error;
    }
};

exports.deleteBulkTrainingExam = async (input) => {

    const schema = Joi.object({
        id: Joi.array().items(Joi.number().min(1)).required(),
        created_by: Joi.number().min(1).required()
    }).required();

    try {
        const response = await schema.validateAsync(input);
        return response;
    } catch (error) {
        throw error;
    }
};

exports.retrieveRequestExamByCourse = async (input) => {

    const schema = Joi.object({
        idCourse: Joi.number().required(),
        idCompany: Joi.number().required()
    }).required();

    try {
        const response = await schema.validateAsync(input);
        return response;
    } catch (error) {
        throw error;
    }
};
