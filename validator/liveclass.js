const Joi = require('joi');

exports.postParticipants = async (detailObject) => {

    const schema = Joi.object({
        idBooking: Joi.number().required(),
        participants: Joi.array().items(Joi.number()),
        email: Joi.array().items(Joi.string()),
    }).required();

    try {
        const response = await schema.validateAsync(detailObject);
        return response;
    } catch (error) {
        throw error;
    }
};