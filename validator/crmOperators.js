const Joi = require('joi');
exports.store = async (input,singleInput) => {
    return new Promise(async (hasil)=>{
        let Field = {
            title : Joi.string().required(),
            deleted : Joi.number().allow(0),
        }
        if(input.id){
            Field.id = Joi.number().allow(0);
        }
    
        let schema = Joi.object(Field);
        try {
            const response = await schema.validateAsync(input);
            return hasil({ error:false,result: response });
        } catch (error) {
            return hasil({ error:error.details[0].message,result: false });
        }
    })
};
