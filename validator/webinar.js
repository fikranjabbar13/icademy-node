const Joi = require('joi');

exports.sendEssay = async (input) => {

    const schema = Joi.object({
        sent: Joi.number().required(),
        essay: Joi.string().required(),
        time: Joi.number().min(1).required(),
    }).required();

    try {
        const response = await schema.validateAsync(input);
        return response;
    } catch (error) {
        throw error;
    }
};

exports.prepostTest = async (input) => {

    const schema = Joi.object({
        id: Joi.number().required(),
        jenis: Joi.number().allow(0, 1).required(),
        waktu: Joi.number().min(1).required(),
        webinar_test: Joi.array().items(Joi.object())
    }).required();

    try {
        const response = await schema.validateAsync(input);
        return response;
    } catch (error) {
        throw error;
    }
};

exports.guestRegistration = async (input) => {
    try {
        let tmp = {
            webinarId: Joi.number().min(5).required(),
            name: Joi.string().min(3).required(),
            email: Joi.string().email().required(),
            phone: Joi.number().min(8).required(),
            address: Joi.string().allow(null, ''),
            slip: Joi.object().allow('', null, 'null'),
            isPaid: Joi.number().min(0).max(1).required(),
        };
        if (Number(input.isPaid) == 1) {
            tmp.slip = Joi.object().required();
        }
        const schema = Joi.object(tmp);
        const response = await schema.validateAsync(input);
        return {isError: false, result: response};
    } catch (error) {
        return {isError: true, result: error.details[0].message};
    }
};

exports.guestAction = async (input) => {
    try {
        let tmp = {
            webinar_id: Joi.number().min(5).required(),
            id: Joi.number().min(1).required(),
            approved_by: Joi.number().required()
        };

        const schema = Joi.object(tmp).required();
        const response = await schema.validateAsync(input);
        return response;
    } catch (error) {
        return false;
    }
};

exports.setSpeaker = async (input) => {
    try {
        let tmp = {
            webinarId: Joi.number().min(5).required(),
            userId: Joi.number().min(1).required(),
            option: Joi.string().valid('peserta', 'guest').required()
        };

        if (input.option === 'guest') {
            tmp.userId = Joi.string().min(5).required()
        }

        const schema = Joi.object(tmp).required();
        const response = await schema.validateAsync(input);
        return response;
    } catch (error) {
        return false;
    }
};

exports.setSpeakerExternal = async (input) => {
    try {
        let tmp = {
            webinarId: Joi.number().min(5).required(),
            name: Joi.string().required(),
            email: Joi.string().email().required(),
            phone: Joi.string().required(),
            role: Joi.string().valid('speaker').required(),
        };

        const schema = Joi.object(tmp).required();
        const response = await schema.validateAsync(input);
        return response;
    } catch (error) {
        return false;
    }
};

exports.delSpeakerExternal = async (input) => {
    try {
        let tmp = {
            webinarId: Joi.number().min(5).required(),
            bulk: Joi.array().required(),
        };

        const schema = Joi.object(tmp).required();
        const response = await schema.validateAsync(input);
        return response;
    } catch (error) {
        return false;
    }
};

exports.sendMailSpeakerExternal = async (input) => {
    try {
        let tmp = {
            webinarId: Joi.number().min(5).required(),
            socket_id: Joi.string().allow(null,''),
            role: Joi.string(),
            dataMail: Joi.array().required(),
            dataTime: Joi.string()
        };

        const schema = Joi.object(tmp).required();
        const response = await schema.validateAsync(input);
        return response;
    } catch (error) {
        return false;
    }
};

exports.retrieveOfferLetterRequest = async (detailObject) => {

    const schema = Joi.object({
        textOfferLetter: Joi.string().required(),
        peserta: Joi.array().min(1).items(Joi.object({id: Joi.number().required(), name: Joi.string().required(), email: Joi.string().required()})).messages({
            'array.min': 'Choose Guest/Participant to Send Offer Letter'
        }),
    }).required();

    try {
        const response = await schema.validateAsync(detailObject);
        return response;
    } catch (error) {
        throw error;
    }
};
