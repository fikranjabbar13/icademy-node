const Joi = require('joi');

exports.validateUpdateStatusUserBulkRequest = async (detailObject) => {

    const schema = Joi.object({
        dataIdUser: Joi.array().items(Joi.number()).required(),
        status: Joi.string().valid('active', 'inactive')
    }).required();

    try {
        const response = await schema.validateAsync(detailObject);
        return response;
    } catch (error) {
        throw error;
    }
};

exports.validatePostRegistrationForm = async (detailObject) => {

    const schema = Joi.object({
        idTrainingCompany: Joi.number().required(),
        idForm: Joi.number().required(),
        data: Joi.string().required()
    }).required();

    try {
        const response = await schema.validateAsync(detailObject);
        return response;
    } catch (error) {
        throw error;
    }
};

exports.validateReadRegistrationUserByTrainingCompany = async (detailObject) => {

    const schema = Joi.object({
        idCompany: Joi.number().required(),
        isTrainingCompany: Joi.boolean().valid(true, false)
    }).required();

    try {
        const response = await schema.validateAsync(detailObject);
        return response;
    } catch (error) {
        throw error;
    }
};

exports.validateActivateUserByIdUserRegistration = async (detailObject) => {

    const schema = Joi.object({
        idUserRegistration: Joi.array().items(Joi.number()).required(),
    }).required();

    try {
        const response = await schema.validateAsync(detailObject);
        return response;
    } catch (error) {
        throw error;
    }
};

