var fs = require('fs');
var path = require('path');
var env = require('./env.json');
var createError = require('http-errors');
var express = require('express');
var compres = require('compression');
var cors = require('cors');
var helmet = require('helmet');
var responseTime = require('response-time');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var logger = require('morgan');
const { BullMonitorExpress } = require('@bull-monitor/express');
const Queue = require('bull');
const { routeList } = require("./middleware");

// console.log = () => { };

var fileManMiddleware = require('@opuscapita/filemanager-server').middleware;
var configDir = {
  fsRoot: path.resolve(__dirname, './public'),
  rootName: 'File Manager'
};

var indexRouter = require('./routes/index');
var v1Router = require('./routes/v1');
var v2Router = require('./routes/v2');
var v3Router = require('./routes/v3');
routeList([v1Router, v2Router, v3Router])

// var apm = require('elastic-apm-node').start({
//   // Override service name from package.json
//   // Allowed characters: a-z, A-Z, 0-9, -, _, and space
//   serviceName: 'restapiahmad',
//   // Use if APM Server requires a token
//   secretToken: '',
//   // Set custom APM Server URL (default: http://localhost:8200)
//   serverUrl: 'http://elk.carsworld.co.id:8200'
// });

var app = express();

var server = require('http').createServer(app);
server.setTimeout(3600000);
var io = require('socket.io')(server);
require('./web_sockets/index').socket(io);

// view engine setup
(async () => {
  app.set('views', path.join(__dirname, 'views'));
  app.set('view engine', 'pug');

  // disable powered
  app.disable('x-powered-by');
  app.use(compres());
  app.use(function (req, res, next) {
    req.setTimeout(3600000);
    res.header('Access-Control-Allow-Origin', '*');
    next();
  });

  // support parsing of application/json type post data
  app.use(bodyParser.json({ limit: '50mb' }));
  //support parsing of application/x-www-form-urlencoded post data
  app.use(bodyParser.urlencoded({ extended: true, limit: '50mb' }));

  app.use(cors());
  app.use(helmet());
  app.use(responseTime());

  app.use(logger('dev'));
  app.use(express.json({ limit: '50mb' }));
  app.use(express.urlencoded({ extended: false, limit: '50mb' }));
  app.use(cookieParser());
  app.use(express.static(path.join(__dirname, 'public')));

  const monitor = new BullMonitorExpress({

    queues: [
      new Queue('sendMail', { redis: { port: env.REDIS_PORT, host: env.REDIS_HOST, password: env.REDIS_PASSWORD } }),

      // new Queue('sendMail', { redis: { port: env.REDIS_PORT, host: env.REDIS_HOST, password: env.password } }),
      // readonly queue
    ],
    // enables graphql playground at /my/url/graphql. true by default
    // enable metrics collector. false by default
    // metrics are persisted into redis as a list
    // with keys in format "bull_monitor::metrics::{{queue}}"
    metrics: {
      // collect metrics every X
      // where X is any value supported by https://github.com/kibertoad/toad-scheduler
      collectInterval: { hours: 1 },
      maxMetrics: 100,
      // disable metrics for specific queues
      blacklist: ['1'],
    },
  });

  await monitor.init();
  app.use('/monitor', monitor.router);
  app.use('/', indexRouter);
  app.use('/v1', v1Router);
  app.use('/v2', v2Router);
  app.use('/v3', v3Router);
  app.use('/', fileManMiddleware(configDir));

  // catch 404 and forward to error handler
  app.use(function (req, res, next) {
    next(createError(404));
  });

  // error handler
  app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
  });
})();

module.exports = { app, server };
