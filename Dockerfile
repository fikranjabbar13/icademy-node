# base image
FROM node:12

# contact me
MAINTAINER Ahmad Ardiansyah '<ahmad.ardiansyah@carsworld.id>'

# copy all project to docker
WORKDIR /app
COPY . .

# install dependencies
RUN npm install

# open port
EXPOSE 3200

# start application
CMD ["npm", "start"]
