const io = require("socket.io-client2");
const env = require('../env.json');
const ws = io.connect(env.SOCKET_URL, { transports: ["websocket", "polling"], query: { actor: 'backend' } });
ws.on('connect', () => {
    console.info("ON SOCKET")
});
module.exports = ws;