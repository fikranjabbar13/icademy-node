const path = require("path");
const Queue = require('bull');
const env = require('../env.json');
const absolutePath = path.resolve("./configs/sendMail.js");
const options = {
    delay: 1000,
    attempts: 2,
    backoff: 20000
};
const sendMailQueue = new Queue('sendMail', { redis: { port: env.REDIS_PORT, host: env.REDIS_HOST, password: env.REDIS_PASSWORD } });

sendMailQueue.isReady().then(() => {
    console.info("OK")
})

sendMailQueue.process(absolutePath)

sendMailQueue.on('failed', async (job, err) => {
    if (typeof job === 'number') {
        console.info("pause")
        await sendMailQueue.pause();
        setTimeout(async () => {
            console.info("resume")
            await sendMailQueue.resume();
            console.info('add again');
            if (job.data) {
                await sendMailQueue.add(job.data, { delay: 3000, priority: 1, removeOnFail: true });
            }
        }, 50000);
    }
});

module.exports = sendMailQueue;