/** @namespace */
const CREDENTIAL = require("./august-snowfall-330503-674243c70e19.json");
const { google } = require('googleapis');
const { MAIL_USER } = require("../../env.json");


const calendar = google.calendar({
    version: CREDENTIAL.versionCalendar,
    auth: CREDENTIAL.apikey
});

const auth = new google.auth.JWT(
    CREDENTIAL.client_email,
    null,
    CREDENTIAL.private_key,
    CREDENTIAL.scopes,
    MAIL_USER
);

let basePayload = {
    'kind': 'Calendar#Event',
    htmlLink: 'https://app.icademy.stg2.kelola.co.id/meet/12345',
    creator: {
        id: '232',
        'email': 'hello@icademy.co.id',
        displayName: 'Icademy',
        self: true
    },
    "organizer": {
        "id": "232",
        'email': 'hello@icademy.co.id',
        displayName: 'Icademy',
        self: true
    },
    visibility: 'private',
    transparency: "opaque",
    'summary': 'Test Awesome Event!',
    'location': 'ICADEMY - Jl. Let. Jen. S. Parman Kav. 106 A. Jakarta 11440 - Indonesia',
    'description': `<b>Join Meet</b> <br/><a href="https://app.icademy.stg2.kelola.co.id/">icademy</a>`,
    'start': {
        'dateTime': '2022-02-17T22:10:00+07:00',
        'timeZone': 'Asia/Jakarta'
    },
    'end': {
        'dateTime': '2022-02-17T23:00:00+07:00',
        'timeZone': 'Asia/Jakarta'
    },
    'recurrence': [
        'RRULE:FREQ=DAILY;COUNT=1'
    ],
    'attendees': [
        { 'email': 'agusmemons90@gmail.com', self: true, responseStatus: "needsAction", resource: true },
        { 'email': 'agus.triadji@gmail.com', self: true, responseStatus: "needsAction", resource: true }
    ],
    "source": {
        "url": 'https://app.icademy.stg2.kelola.co.id/meet/12345',
        "title": "Join Meet URL",
        self: true
    },
    'reminders': {
        'useDefault': false,
        'overrides': [
            { 'method': 'email', 'minutes': 15/*24 * 60*/ },
            { 'method': 'popup', 'minutes': 15 }
        ]
    },
    locked: true,
    privateCopy: false,
    guestsCanInviteOthers: false,
    guestsCanModify: false,
    guestsCanSeeOtherGuests: false,
    extendedProperties: {
        private: {
            typeRelated: 1, // 1 for meeting, 2 for webinar
            relatedId: 12 // id meeting liveclass_booking / id webinars
        }
    }
};

exports.basePayload = () => {
    return basePayload;
}

exports.eventGet = async (eventId) => {
    return new Promise(async (resolve) => {

        let result = { error: false, data: false };

        try {
            const config = {
                calendarId: "primary",
                auth: auth,
                eventId: eventId
            }
            let response = await calendar.events.get(config);

            if (response.status == 200) {
                result.data = response.data;
            } else {
                result.error = true;
                result.data = response;
            }
        } catch (error) {
            result.error = true;
            result.data = error;
        }

        return resolve(result);
    });

}

/**
 * Create event
 * 
 * @async
 * @function eventInsert
 * 
 * @param {number} params.typeRelated - 1:meeting|2:webinar
 * @param {number} params.relatedId - id liveclass_booking/webinars
 * @param {string} params.htmlLink - link meet ex: https://app.icademy.id/meet/:idMeet
 * @param {string} params.summary - title event
 * @param {string} params.description - description event
 * @param {object} params.start - {dateTime: '2022-02-17T23:00:00+07:00','timeZone': 'Asia/Jakarta'}
 * @param {object} params.end - {dateTime: '2022-02-18T23:00:00+07:00','timeZone': 'Asia/Jakarta'}
 * @param {object} params.attendees - [{ 'email': 'agusmemons90@gmail.com', self: true, responseStatus: "needsAction", resource: true }]
 * @param {string} params.eventId - calendar_event field liveclass_booking or webinar
 * @returns {error: Boolean, data: Object}
 */
exports.eventInsert = async (params = Object) => {
    return new Promise(async (resolve) => {

        let result = { error: false, data: null };

        try {

            basePayload.summary = params.summary;
            basePayload.description = params.description;
            basePayload.start = params.start;
            basePayload.end = params.end;
            basePayload.extendedProperties.private.relatedId = params.relatedId;
            basePayload.extendedProperties.private.relatedId = params.typeRelated;
            basePayload.htmlLink = params.htmlLink;
            basePayload.attendees = params.attendees;

            if (params.eventId) {
                basePayload.id = params.eventId;
                let update = await this.eventUpdate(basePayload);
                return resolve(update)
            } else {

                let config = {
                    auth: auth,
                    'calendarId': 'primary',
                    'resource': basePayload,
                    sendUpdates: "all",
                    sendNotifications: true,
                    prettyPrint: true
                };

                let response = await calendar.events.insert(config);

                if (response.status == 200) {
                    result.data = response.data;
                } else {
                    result.error = true;
                    result.data = response;
                }
            }


        } catch (error) {
            console.log(error, 500)
            result.error = true;
            result.data = error;
        }

        return resolve(result);
    });
}

exports.eventUpdate = async (params) => {
    return new Promise(async (resolve) => {

        let result = { error: false, data: null };

        try {

            let config = {
                calendarId: "primary",
                auth: auth,
                eventId: params.id,
                sendUpdates: "all",
                prettyPrint: true,
                requestBody: params
            };

            let response = await calendar.events.update(config)

            if (response.status == 200) {
                result.data = response.data;
            } else {
                result.error = true;
                result.data = response;
            }
        } catch (error) {
            result.error = true;
            result.data = error;
        }

        return resolve(result);
    });
}

exports.eventDelete = async (eventId) => {
    return new Promise(async (resolve) => {

        let result = { error: false, data: false };

        try {
            const config = {
                sendNotifications: true,
                calendarId: "primary",
                auth: auth,
                eventId: eventId,
                sendUpdates: "all"
            }
            let response = await calendar.events.delete(config);

            console.log(response, "DELETE")
            if (response.status == 200) {
                result.data = response.data;
            } else {
                result.error = true;
                result.data = response;
            }
        } catch (error) {
            result.error = true;
            result.data = error;
        }

        return resolve(result);
    });

}
