exports.gCalendarBasePayload = require('./gCalendar').basePayload;
exports.gCalendarEventGet = require('./gCalendar').eventGet;
exports.gCalendarEventInsert = require('./gCalendar').eventInsert;
exports.gCalendarEventUpdate = require('./gCalendar').eventUpdate;
exports.gCalendarEventDelete = require('./gCalendar').eventDelete;