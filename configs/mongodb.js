/**
 * config of mongoose
 */
module.exports = {
  host: 'icademy.stg.kelola.co.id:27017/jigasi',
  options: {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
  },
};
