const nodemailer = require('nodemailer');
const fs = require('fs');
const env = require('../env.json');
const privateKey = fs.readFileSync("./google._domainkey.icademy.id.pem", "utf8");
const dkim = {
    keys: [{
        domainName: "icademy.id",
        keySelector: "google._domainkey.icademy.id",
        skipFields: "message-id:date",
        privateKey: privateKey,
    }],
    cacheDir: false
}
let obj = {
    host: env.MAIL_HOST,
    //host: "smtp-relay.gmail.com",
    port: env.MAIL_PORT,
    secure: env.MAIL_SECURE,
    pool: true,
    tls: true,
    auth: {
        user: env.MAIL_USER,
        pass: env.MAIL_PASS
    },
    from: env.MAIL_USER
}
const transporter = nodemailer.createTransport(obj);

transporter.verify(function (error, success) {
    if (error) {
        console.info(error, "VERIFY CON MAIL");
    } else {
        console.info("Server is ready to take our messages");
    }
});
exports.dkim = dkim;
module.exports = transporter;