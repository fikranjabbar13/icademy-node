const db = require('../configs/database');
const ws = require('./socketClient');
const transporter = require("./transporterMail");
const dkim = require("./transporterMail").dkim;

// console.log('pool jalan')
let max = 30;
let count = 1;
module.exports = function (job, done) {
    job.data.dkim = dkim;
    
    if (count >= max) {
        count = 1;
        // return done(true);
    }
    count++;
    // console.log('try sendmail : ', count);
    
    if (job.data.type === 'sendmail_role_external_webinar') {
        let query_error = '';
        // let dataExist = await db.query(`SELECT * FROM webinar_peserta WHERE webinar_id=? AND user_id=?`, [job.data.payload_id, job.data.user_id]);
        let values = `'${job.data.payload_id}', '${job.data.to}','${job.data.subject}', '2'`;
        let success = ` UPDATE webinar_peserta 
                        SET status = 1 
                    WHERE webinar_id = '${job.data.payload_id}' 
                    AND user_id = '${job.data.user_id}' `;

        let hold = ` UPDATE webinar_peserta 
                        SET status = 4 
                    WHERE webinar_id = '${job.data.payload_id}' 
                    AND user_id = '${job.data.user_id}' `;

        if (job.data.voucher) {
            // dataExist = await db.query(`SELECT * FROM webinar_tamu WHERE webinar_id=? AND voucher=?`, [job.data.payload_id, job.data.voucher]);
            values += `,'${job.data.voucher}'`;
            success = `
                UPDATE ${job.data.guest ? 'webinar_tamu': 'webinar_role_external'} SET status = 1  
                WHERE webinar_id = '${job.data.payload_id}' 
                AND voucher = '${job.data.user_id}'
            `;
            hold = `
                UPDATE ${job.data.guest ? 'webinar_tamu': 'webinar_role_external'} SET status = 4 
                WHERE webinar_id = '${job.data.payload_id}' 
                AND voucher = '${job.data.user_id}'
            `;
            // if (dataExist.length && dataExist[0].status !== 2){
            //     hold='';
            // }
        } else {
            values += `,'${job.data.user_id}'`;
        }
        transporter.sendMail(job.data, (err, response) => {
            if (err) {
                console.error(err, "ERROR SEND MAIL");
                values += `,'${JSON.stringify(err)}'`;
                if (job.data.voucher) {
                    query_error = `INSERT INTO mail_log_error(activity_id, email, subject, type,voucher, error) value(${values}); ${hold}`;
                } else {
                    query_error = `INSERT INTO mail_log_error(activity_id, email, subject, type,user_id, error) value(${values}); ${hold}`;
                }
                db.query(query_error);
                return done(new Error(err));
                //process.exit(1);
            } else {
                console.log(response.messageId, "ON")
                if (response.messageId) {

                    // if (dataExist.length && dataExist[0].status !== 2){
                        db.query(success);
                    // }

                    ws.emit("APP::sendmail_external_webinar", { data: { email: job.data.to, status: 1, typeRole:job.data.typeRole, user_id: job.data.user_id, webinar_id: job.data.payload_id } });
                    //ws.emit("WEB::sendmail_participant_webinar", { data: { email: job.data.to, status: 1, guest: job.data.guest } }, job.data.socket_id);

                    return done();
                    //process.exit(0);

                } else {
                    values += `,'No Error Info'`;
                    if (job.data.voucher) {
                        query_error = `INSERT INTO mail_log_error(activity_id, email, subject, type,voucher, error) value(${values}); ${hold}`;
                    } else {
                        query_error = `INSERT INTO mail_log_error(activity_id, email, subject, type,user_id, error) value(${values}); ${hold}`;
                    }
                    db.query(query_error);
                    return done(new Error('ERROR 2'));
                    // process.exit(1);
                }
            }
        });

    }
    else if (job.data.type === 'sendmail_participant_webinar') {
        let query_error = '';
        // let dataExist = await db.query(`SELECT * FROM webinar_peserta WHERE webinar_id=? AND user_id=?`, [job.data.payload_id, job.data.user_id]);
        let values = `'${job.data.payload_id}', '${job.data.to}','${job.data.subject}', '2'`;
        let success = ` UPDATE webinar_peserta 
                        SET status = 1 
                    WHERE webinar_id = '${job.data.payload_id}' 
                    AND user_id = '${job.data.user_id}' `;

        let hold = ` UPDATE webinar_peserta 
                        SET status = 4 
                    WHERE webinar_id = '${job.data.payload_id}' 
                    AND user_id = '${job.data.user_id}' `;

        if (job.data.guest) {
            // dataExist = await db.query(`SELECT * FROM webinar_tamu WHERE webinar_id=? AND voucher=?`, [job.data.payload_id, job.data.voucher]);
            values += `,'${job.data.voucher}'`;
            success = `
                UPDATE webinar_tamu 
                    SET status = 1 
                WHERE webinar_id = '${job.data.payload_id}' 
                AND voucher = '${job.data.voucher}'
            `;
            hold = `
                UPDATE webinar_tamu  
                    SET status = 4 
                WHERE webinar_id = '${job.data.payload_id}' 
                AND voucher = '${job.data.voucher}'
            `;
            // if (dataExist.length && dataExist[0].status !== 2){
            //     hold='';
            // }
        } else {
            values += `,'${job.data.user_id}'`;
        }
        transporter.sendMail(job.data, (err, response) => {
            if (err) {
                console.error(err, "ERROR SEND MAIL");
                values += `,'${JSON.stringify(err)}'`;
                if (job.data.guest) {
                    query_error = `INSERT INTO mail_log_error(activity_id, email, subject, type,voucher, error) value(${values}); ${hold}`;
                } else {
                    query_error = `INSERT INTO mail_log_error(activity_id, email, subject, type,user_id, error) value(${values}); ${hold}`;
                }
                db.query(query_error);
                return done(new Error(err));
                //process.exit(1);
            } else {
                console.log(response.messageId, "ON")
                if (response.messageId) {

                    // if (dataExist.length && dataExist[0].status !== 2){
                        db.query(success);
                    // }

                    ws.emit("APP::sendmail_participant_webinar", { data: { email: job.data.to, status: 1, guest: job.data.guest, user_id: job.data.user_id, webinar_id: job.data.payload_id } });
                    //ws.emit("WEB::sendmail_participant_webinar", { data: { email: job.data.to, status: 1, guest: job.data.guest } }, job.data.socket_id);

                    return done();
                    //process.exit(0);

                } else {
                    values += `,'No Error Info'`;
                    if (job.data.guest) {
                        query_error = `INSERT INTO mail_log_error(activity_id, email, subject, type,voucher, error) value(${values}); ${hold}`;
                    } else {
                        query_error = `INSERT INTO mail_log_error(activity_id, email, subject, type,user_id, error) value(${values}); ${hold}`;
                    }
                    db.query(query_error);
                    return done(new Error('ERROR 2'));
                    // process.exit(1);
                }
            }
        });

    }
    else if (job.data.type === 'sendmail_certificate_webinar') {

        transporter.sendMail(job.data, (err, response) => {

            let values = `'${job.data.webinar_id}', '${job.data.user_id}', '${job.data.to}','${job.data.subject}', '2'`;
            let Query_update = null;

            if (err) {

                values += `,'${JSON.stringify(err)}'`;
                let Query_error = `INSERT INTO mail_log_error(activity_id, user_id, email, subject, type, error) value(${values});`;
                Query_update = `${Query_error} UPDATE webinar_sertifikat SET status = 4 WHERE webinar_id = ${job.data.webinar_id} AND user_id = '${job.data.user_id}' AND peserta = '${job.data.peserta}'`;

                db.query(Query_update);
                return done(new Error(err));
                //process.exit(1);
            } else {

                if (response.messageId) {

                    Query_update = `UPDATE webinar_sertifikat SET status = 1 WHERE webinar_id = ${job.data.webinar_id} AND user_id = '${job.data.user_id}' AND peserta = '${job.data.peserta}'`;

                    db.query(Query_update);
                    ws.emit("APP::sendmail_certificate_webinar", { data: { email: job.data.to, status: 1, webinar_id: job.data.webinar_id, user_id: job.data.user_id } });

                    return done();
                    //process.exit(0);

                } else {

                    values += `,'No Error Info'`;
                    let Query_error = `INSERT INTO mail_log_error(activity_id, user_id, email, subject, type, error) value(${values});`;
                    Query_update = `${Query_error} UPDATE webinar_sertifikat SET status = 4 WHERE webinar_id = ${job.data.webinar_id} AND user_id = '${job.data.user_id}' AND peserta = '${job.data.peserta}'`;

                    db.query(Query_update);
                    return done(new Error(err));
                    // process.exit(1);
                }
            }
        });
    }
    else if (job.data.type === 'sendmail_certificate_training') {

        transporter.sendMail(job.data, (err, response) => {

            let values = `'${job.data.training_exam_result}', '${job.data.user_id}', '${job.data.to}','${job.data.subject}', '3'`;
            let Query_update = null;

            if (err) {
                values += `,'${JSON.stringify(err)}'`;
                let Query_error = `INSERT INTO mail_log_error(activity_id, user_id, email, subject, type, error) value(${values});`;
                Query_update = `${Query_error} UPDATE training_exam_result SET certificate_status = 'Failed', certificate = '${job.data.filepath_cert_user}' WHERE id='${job.data.training_exam_result}';`;

                db.query(Query_update);
                return done(new Error(err));
                //process.exit(1);
            } else {

                if (response.messageId) {

                    Query_update = `UPDATE training_exam_result SET certificate_status = 'Sent', certificate = '${job.data.filepath_cert_user}' WHERE id='${job.data.training_exam_result}';`;
                    console.info(Query_update,'OK');
			db.query(Query_update);
                    ws.emit("APP::sendmail_certificate_training", { data: { email: job.data.to, status: 'Sent', result_id: job.data.training_exam_result, filepath_cert_user: job.data.filepath_cert_user, company_id: job.data.company_id } });

                    return done();
                    //process.exit(0);

                } else {
                    values += `,'No Error Info'`;
                    let Query_error = `INSERT INTO mail_log_error(activity_id, user_id, email, subject, type, error) value(${values});`;
                    Query_update = `${Query_error} UPDATE training_exam_result SET certificate_status = 'Failed', certificate = '${job.data.filepath_cert_user}' WHERE id='${job.data.training_exam_result}';`;

                    db.query(Query_update);
                    return done(new Error(err));
                    // process.exit(1);
                }
            }
        });
    }
    else if (job.data.type === 'sendmail_participant_meeting') {

        transporter.sendMail(job.data, (err, response) => {

            let values = `'${job.data.to}','${job.data.subject}', '1'`;
            let Query_update = null;

            if (err) {

                values += `,'${JSON.stringify(err)}'`;
                let Query_error = `INSERT INTO mail_log_error(email, subject, type, error) value(${values});`;
                db.query(Query_error);
                return done(new Error(err));
                //process.exit(1);
            } else {

                if (response.messageId) {
                    return done();
                } else {

                    values += `,'No Error Info'`;
                    let Query_error = `INSERT INTO mail_log_error(email, subject, type, error) value(${values});`;
                    db.query(Query_error);
                    return done(new Error(err));
                    // process.exit(1);
                }
            }
        });
    }
    else if (job.data.type === 'sendmail_information_email_user') {

        transporter.sendMail(job.data, (err, response) => {

            let values = `'${job.data.to}','${job.data.subject}', '5'`;
            let Query_update = null;

            if (err) {

                values += `,'${JSON.stringify(err)}'`;
                let Query_error = `INSERT INTO mail_log_error( email, subject, type, error) value(${values});`;
                db.query(Query_error);
                return done(new Error(err));
                //process.exit(1);
            } else {

                if (response.messageId) {
                    return done();
                } else {

                    values += `,'No Error Info'`;
                    let Query_error = `INSERT INTO mail_log_error(email, subject, type, error) value(${values});`;
                    db.query(Query_error);
                    return done(new Error(err));
                    // process.exit(1);
                }
            }
        });
    }
    else if (job.data.type === 'sendmail_offer_letter'){

        transporter.sendMail(job.data, (err, response) => {

            let values = `'${job.data.to}','${job.data.subject}', '5'`;
            let Query_update = null;

            if (err) {

                values += `,'${JSON.stringify(err)}'`;
                let Query_error = `INSERT INTO mail_log_error( email, subject, type, error) value(${values});`;
                db.query(Query_error);
                return done(new Error(err));
                //process.exit(1);
            } else {

                if (response.messageId) {
                    return done();
                } else {

                    values += `,'No Error Info'`;
                    let Query_error = `INSERT INTO mail_log_error(email, subject, type, error) value(${values});`;
                    db.query(Query_error);
                    return done(new Error(err));
                    // process.exit(1);
                }
            }
        });

    }
    else if (job.data.type === 'sendmail_certificate_course'){

        transporter.sendMail(job.data, (err, response) => {

            let values = `'${job.data.to}','${job.data.subject}', '5'`;
            let Query_update = null;

            if (err) {

                values += `,'${JSON.stringify(err)}'`;
                let Query_error = `INSERT INTO mail_log_error( email, subject, type, error) value(${values});`;
                db.query(Query_error);
                return done(new Error(err));
                //process.exit(1);
            } else {

                if (response.messageId) {
                    return done();
                } else {

                    values += `,'No Error Info'`;
                    let Query_error = `INSERT INTO mail_log_error(email, subject, type, error) value(${values});`;
                    db.query(Query_error);
                    return done(new Error(err));
                    // process.exit(1);
                }
            }
        });

    }
    

}
