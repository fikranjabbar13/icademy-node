const fs = require("fs");
const path = require("path");
const url = require('url')
const env = require("../../env.json");

// 210 x 297
const PageSizeA4={
    portrait:{
        single: {width:"205mm", height:"292mm",heightSingle:"292mm"},
        twoSide: {width:"205mm", height:"584mm", heightSingle:"292mm" },
        backgroundSize:"background-size:205mm 292mm;"
    },
    landscape:{
        single: {width:"297mm", height:"207mm",heightSingle:"207mm"},
        twoSide: {width:"297mm", height:"414mm", heightSingle:"207mm"},
        backgroundSize:"background-size:295mm 208mm;"
    }
}

function ParseAttribute(cert_background,signature,sign,logo,cert_orientation,bodyNewPage,imageTopLeft,imageTopRight,certNumber){
    return new Promise(async(resolve)=>{

        const defaultBG = url.pathToFileURL(path.resolve("./public/defaultFrame.png")).href
        let PageSize = '';
        let backgroundSize = PageSizeA4[cert_orientation].backgroundSize
        let PageSecond = '';
        let TTD2 = [];
        let signData = "";

        if(logo.path){
            logo = url.pathToFileURL(path.resolve(logo.path)).href;
            // logo =`file://${path.resolve(__dirname,`../../${logo.path}`)}`;
            //logo = `${env.APP_URL}/cert_logo/${logo.filename}`;
        }

        signature.map(x => {
            //x.path = `${env.APP_URL}/signature/${x.filename}`;
            //x.path =`file://${path.resolve(__dirname,`../../${x.path}`)}`;
            x.path= url.pathToFileURL(path.resolve(x.path)).href;
            TTD2.push(x.path);
        });

        sign.map((item, index) => {
            signData =
                signData +
                `<span style="display: table-cell; font-family:'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif">
                    <img style="width:120px; height: 90px" src="${TTD2[index]}" />
                    <br>
                    <span style="font-size:15px">${item.cert_sign_name}</span><br>
                    <span style="font-size:15px;">${item.cert_sign_title}</span>
                </span>`;
        });

        if(cert_background){
            cert_background= url.pathToFileURL(path.resolve(cert_background.path)).href;
            // cert_background = `${env.APP_URL}/others/${cert_background.filename}`;
        }


        if(imageTopLeft){
            imageTopLeft= url.pathToFileURL(path.resolve(imageTopLeft.path)).href;
            //imageTopLeft = `${env.APP_URL}/others/${imageTopLeft.filename}`;
        }

        if(imageTopRight){
            imageTopRight= url.pathToFileURL(path.resolve(imageTopRight.path)).href;
            //imageTopRight = `${env.APP_URL}/others/${imageTopRight.filename}`;
        }

        cert_background = cert_background ? cert_background : defaultBG;
        imageTopLeft = imageTopLeft ? `<img style="width:160px; height: 100px; padding:20px;float: left;" src="${imageTopLeft ? imageTopLeft : ''}" />` : '';    
        imageTopRight = imageTopRight ? `<img style="width:160px; height: 100px; padding:20px;float: right;" src="${imageTopRight ? imageTopRight : ''}" />`: '';    
        certNumber = certNumber ? `<h4>${certNumber}</h4>`: '';

        if(bodyNewPage){
            PageSize = PageSizeA4[cert_orientation].twoSide;
        }else{
            PageSize = PageSizeA4[cert_orientation].single;
        }

        if(bodyNewPage){
            PageSecond = `
            <div style="clear: both;padding-bottom:17px;"></div>
            <div 
                style="
                width:${PageSize.width};
                height:${PageSize.heightSingle};
                background-image:url('${cert_background}');
                ${backgroundSize}
                background-repeat: no-repeat;
                ">
                <center>
                <div style="clear: both;"><br></div>
                <div style="clear: both;"><br></div>
                <div style="width:85%; height:80%; padding-top:5px; display: block; text-align:center; border: none; position: relative"><br><br>
                    ${bodyNewPage}
                </div>
                </center>
            </div>
            `;
        }

        return resolve({ signature,sign,logo,cert_orientation,bodyNewPage,imageTopLeft,imageTopRight,certNumber,PageSecond,PageSize,backgroundSize,signData,cert_background})
    })
}

exports.htmltCourse = async (
    name,cert_title,tanggal,signature,sign,logo,cert_subtitle,cert_description,
    cert_topic,cert_background,cert_orientation,bodyNewPage,imageTopLeft,imageTopRight,certNumber
    )=>{

    let parse = await ParseAttribute(cert_background,signature,sign,logo,cert_orientation,bodyNewPage,imageTopLeft,imageTopRight,certNumber);
    
    let html=`
    <!DOCTYPE html>
    <html>
    <head></head>
    <style>
        body{
            width:${parse.PageSize.width};
            height:${parse.PageSize.height};
            font-family:'Times New Roman', Times, serif;
        }
    </style>
    <body>
        <div>
            <!-- FRONT -->
            <div 
                style="
                width:100%;
                height:${parse.PageSize.heightSingle};
                background-image:url('${parse.cert_background}');
                ${parse.backgroundSize}
                background-repeat: no-repeat;
                ">
                <div style="clear: both;"><br></div>
                <center>
                    <div style="padding-top:${cert_orientation == 'landscape' ?"18":"50"}px;">
                        <div style="width: 83%; height:90px; display: block; ${cert_orientation == 'potrait' ? 'padding-top:80;' : ''}">
                            ${parse.imageTopLeft}
                            ${parse.imageTopRight}
                        </div>
                    </div
                </center>
                <center>
                    <div style="padding-top:${cert_orientation == 'landscape' ?"10":"20"}px;">
                        <center>
                        <div style="width:500px; display: block; padding-top:${cert_orientation == 'landscape' ? '10' : 40}px; font-family:'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;">
                            ${parse.certNumber}
                        </div>
                        </center>
                    </div>
                </center>
                <center>
                <div style="padding-top:${cert_orientation == 'landscape'?"0":"20"}px;width:${parse.PageSize.width};">
                    <div style="width:85%;display: block; text-align:center; border: none; position: relative; ${cert_orientation == 'landscape' ? '' : 'padding-top:60;'}">
                        
                        <div style="padding-top:${cert_orientation == 'landscape' ?"-10":"10"}px;">
                            <img style="width:200px;height:100px;" src="${parse.logo}" />
                        </div>

                        <div style="padding-top:10px;">
                            <span style="font-size:35px; font-weight:bold">${cert_title}</span>
                        </div>
                        
                        <div style="padding-top:10px;">
                            <span style="font-size:20px">${cert_subtitle}</span>
                        </div>
                        
                        <div style="${cert_orientation == 'landscape' ? 'padding-top:20px;padding-bottom:20px' :'padding-top:40px;padding-bottom:40px' }">
                            <span style="font-size:35px"><b>${name}</b></span>
                        </div>

                        <div style="padding-top:${cert_orientation == 'landscape' ?"10":"20"}px;">
                            <span style="font-size:20px">${cert_description}</span>
                        </div>
                        <div style="clear: both;"><br></div>

                        <div style="padding-top:8px;">
                            <span style="font-size:20px"><b>${cert_topic}</b></span>
                        </div>
                        <div style="clear: both;"><br></div>

                        <div style="padding-top:0px;">
                            <span style="font-size:15px;"><i>${tanggal}</i></span>
                        </div>
                        <div style="clear: both;"><br></div>

                        <center>
                            <div style="padding-top:${cert_orientation == 'landscape' ? "0" : "20"}px;">
                                <div style="display:table;width:60%; margin-left:-10px">
                                    ${parse.signData}
                                </div>
                            </div>
                        <center>
                    </div>
                </div>
                </center>
            </div>
            <!-- BACK -->
            ${parse.PageSecond}
        </div>
    </body>
    </html>
    `;
    return html;
}