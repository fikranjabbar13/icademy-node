===================
# Version 4.7.0
===================

# Feature's
===========
# 20/09/2022
* New Feature Project Add Departement / Branch.

# Bug's Fixing
==============
# 20/09/2022
* Fix Change Group Id => Branch Id also in Database.

===================
## Version 4.6.0 
=================== 
===================
# Feature's
===================
# 16/09/2022
* Add Name Project in Meeting List.
* Add Last Update on Exam History Statistic.
# 13/09/2022
* [] Delete File MoM on Menu Files. (No Commit Just React Integration)

# 12/09/2022
* [69cb6ef4c2d9e34bfad17931933bc3da83893a08] Add Feedback Details.
# 08/09/2022
* [22d1d16810ec239c5e4c3d3636eda03c2fe69f86] Xendit Send Email Integration.
# 07/09/2022
* [d1e5b3691db78689fd58b5bbf3e8f70779d5e5ec] Xendit Payment Integration.


==================
# All Bug's Fixing
==================
# 15/09/2022
* [23995283f93e4eed8e677df192eb0d260a6f3dc7] Fix Check Is Read Exam
* [f9d0fcd333068c3d9621241f4f55a7844d3cc38f] Add Req Query Web For Read Exam
# 13/09/2022

* [a61ad003589750578e77ee90e98f517ab2eba0ef] Fix Check audio and cam for change status present user.
* [69cb6ef4c2d9e34bfad17931933bc3da83893a08] Fix Message Exam Assignment, FEATURE: Add Feedback Details.
* [2a8792edfb4c1ee955c92b062648bcb755e6f39b] Fix Bug Project Admin Set Access Default to Editor.

# 13/09/2022
* [69cb6ef4c2d9e34bfad17931933bc3da83893a08] Fix Message Exam Assigned.
# 08/09/2022
* [22d1d16810ec239c5e4c3d3636eda03c2fe69f86] Fix Webinar Bug Display Anonymous.
* [22d1d16810ec239c5e4c3d3636eda03c2fe69f86] Fix Webinar Bug Display Anonymous, FEATURE: Add Feature Send Email Xendit.
* [596cf90542cf2b190bab161f1c37bb38f4878fa9] Fix set width image cert course.
# 07/09/2022
* [2fb1b76ead7d0910359184fa7ee4098dcb5121c5] Fix Message Exam Assigned.
* [b6a60668123548e35f248535afd13b2ee1181af9] Fix undefined browser.
* [2fb1b76ead7d0910359184fa7ee4098dcb5121c5] Fix Bug User Company When Email Inactive on Training User.
