/*
 Navicat Premium Data Transfer

 Source Server         : mariadb
 Source Server Type    : MariaDB
 Source Server Version : 100318
 Source Host           : localhost:3306
 Source Schema         : kelola

 Target Server Type    : MariaDB
 Target Server Version : 100318
 File Encoding         : 65001

 Date: 29/04/2020 08:25:50
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for certificate_user
-- ----------------------------
DROP TABLE IF EXISTS `certificate_user`;
CREATE TABLE `certificate_user` (
  `certificate_id` int(50) NOT NULL,
  `user_id` int(50) NOT NULL,
  UNIQUE KEY `certificate_id` (`certificate_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

SET FOREIGN_KEY_CHECKS = 1;
