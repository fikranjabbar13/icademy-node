/*
 Navicat Premium Data Transfer

 Source Server         : mariadb
 Source Server Type    : MariaDB
 Source Server Version : 100318
 Source Host           : localhost:3306
 Source Schema         : kelola

 Target Server Type    : MariaDB
 Target Server Version : 100318
 File Encoding         : 65001

 Date: 29/04/2020 08:25:34
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for certificate
-- ----------------------------
DROP TABLE IF EXISTS `certificate`;
CREATE TABLE `certificate` (
  `id` int(50) NOT NULL AUTO_INCREMENT COMMENT 'id sertifikat auto increment',
  `title` varchar(100) NOT NULL COMMENT 'nama sertifikat',
  `template` int(1) NOT NULL COMMENT 'id template sertifikat',
  `type_activity` int(1) NOT NULL COMMENT '1 = course, 2 = forum, 3 = meeting',
  `activity_id` int(11) NOT NULL COMMENT 'id aktivitas',
  `signature_1` varchar(50) NOT NULL COMMENT 'tanda tangan 1',
  `signature_name_1` varchar(50) NOT NULL COMMENT 'nama tanda tangan 1',
  `signature_2` varchar(50) DEFAULT NULL COMMENT 'tanda tangan 2',
  `signature_name_2` varchar(50) DEFAULT NULL COMMENT 'nama tanda tangan 2',
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`) USING BTREE,
  UNIQUE KEY `type_activity` (`type_activity`,`activity_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

SET FOREIGN_KEY_CHECKS = 1;
