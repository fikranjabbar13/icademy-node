var env = require('../env.json');
var conf = require('../configs/config');
var db = require('../configs/database');

exports.getHistoryAll = (req, res, next) => {
    db.query(`SELECT * FROM user_activity WHERE user_id = '${req.params.user_id}' ORDER BY created_at DESC`, (error, result, fields) => {
        if(error) {
          res.json({error: true, result: error});
        } else {
          res.json({error: false, result: result});
        }
    });
};

exports.newHistoryCourse = (req, res, next) => {
    let formData = {
        course_id: req.body.course_id,
        user_id: req.body.user_id,
        description : req.body.description,
        title : req.body.title
    };
    
    let sql = `
    INSERT INTO 
        user_activity (
            id_user_activity, user_id, tipe, activity_title, description
        )
    VALUES (
        '${formData.course_id}', '${formData.user_id}', 'course', '${formData.title}', '${formData.description}'
    )`;
  
        db.query(sql, (error, result, fields) => {
          if(error) {
            res.json({error: true, result: error});
          } else {
            res.json({error: false, result: result});
          }
        });
}

exports.newHistoryForum = (req, res, next) => {
    let formData = {
        forum_id: req.body.forum_id,
        user_id: req.body.user_id,
        description : req.body.description,
        title : req.body.title
    };
    
    let sql = `
    INSERT INTO 
        user_activity (
            id_user_activity, user_id, tipe, activity_title, description
        ) 
    VALUES (
        '${formData.forum_id}', '${formData.user_id}', 'forum', '${formData.title}', '${formData.description}'
    )`;
  
        db.query(sql, (error, result, fields) => {
          if(error) {
            res.json({error: true, result: error});
          } else {
            res.json({error: false, result: result});
          }
        });
}

exports.newHistoryClass = (req, res, next) => {
    let formData = {
        class_id: req.body.class_id,
        user_id: req.body.user_id,
        description : req.body.description,
        title : req.body.title
    };
    
    let sql = `
    INSERT INTO 
        user_activity (
            id_user_activity, user_id, tipe, activity_title, description
        ) 
    VALUES (
        '${formData.class_id}', '${formData.user_id}', 'liveclass', '${formData.title}', '${formData.description}'
    )`;
  
    db.query(sql, (error, result, fields) => {
        if(error) {
            res.json({error: true, result: error});
        } else {
            res.json({error: false, result: result});
        }
    });
}

exports.newHistoryLogin = (req, res, next) => {
    
    let formData = {
        user_id: req.body.user_id,
        description : req.body.description,
        title : req.body.title
    };
    
    let sql = `
    INSERT INTO 
        user_activity (
            user_id, tipe, activity_title, description
        ) 
    VALUES (
        '${formData.user_id}', 'login', '${formData.title}', '${formData.description}'
    )`;
  
    db.query(sql, (error, result, fields) => {
        if(error) {
            res.json({error: true, result: error});
        } else {
            res.json({error: false, result: result});
        }
    });
}

function secondsToHms(d) {
    d = Number(d);
    var h = Math.floor(d / 3600);
    var m = Math.floor((d % 3600) / 60);
    var s = Math.floor((d % 3600) % 60);
  
    var hDisplay = h > 0 ? h + ':' : '00:';
    var mDisplay = m > 0 ? m + ':' : '00:';
    var sDisplay = s > 0 ? s : '00';
    return hDisplay + mDisplay + sDisplay;
  }

exports.chart = (req, res) => {
    let sql = `CALL chart_activity(?, ?, ?)`;
    let data = [req.params.company_id, req.params.start, req.params.end];
    db.query(sql, data, (error, result) => {
        if(error) {
            res.json({error: true, result: error});
        } else {
            let alias = ''
            let chart1 = {
                grup: [],
                count: []
            }
            result[0].map(elem => {
                chart1.grup.push(elem.grup_name); 
                chart1.count.push(elem.count);
            });

            let chart2 = {
                name: [],
                count: []
            }
            result[1].map(elem => {
                chart2.name.push(elem.name); 
                chart2.count.push(elem.count);
            });

            let chart3 = {
                tipe: [],
                count: []
            }
            result[2].map(elem => {
                if (elem.tipe === 'liveclass'){
                    alias = 'Meeting'
                }
                else if(elem.tipe === 'course'){
                    alias = 'Course'
                }
                else if(elem.tipe === 'forum'){
                    alias = 'Forum'
                }
                chart3.tipe.push(alias); 
                chart3.count.push(elem.count);
            });

            let chart4 = {
                tipe: [],
                count: []
            }
            result[3].map(elem => {
                if (elem.tipe === 'liveclass'){
                    alias = 'Meeting'
                }
                else if(elem.tipe === 'course'){
                    alias = 'Course'
                }
                else if(elem.tipe === 'forum'){
                    alias = 'Forum'
                }
                chart4.tipe.push(alias); 
                chart4.count.push(elem.count);
            });

            let active = result[4][0].count;
            let audience = result[5][0].count;
            let duration = secondsToHms(result[6][0].count);

            let data = {
                chart1,
                chart2,
                chart3,
                chart4,
                active,
                audience,
                duration
            }
            res.json({error: false, result: data});
        }
    });
};
