var env = require('../env.json');
var conf = require('../configs/config');
var db = require('../configs/database');

var multer = require('multer');
const storage = multer.diskStorage({
  filename: function (req, file, cb) {
    cb(null, file.originalname);
  }
})
let upload = multer({ storage: storage }).array('files');

const AWS = require('aws-sdk');
const fs = require('fs');
const s3 = new AWS.S3({
  accessKeyId: env.AWS_ACCESS,
  secretAccessKey: env.AWS_SECRET
});

exports.uploadAttachments = (req, res) => {
	upload(req, res, async (err) => {
		if(!req.files) {
      res.json({ error: true, result: err });
    } else {
    	// loop all files
      for(let i=0; i<req.files.length; i++) {
        let path = req.files[i].path;
        let newDate = new Date();
        let keyName = `${env.AWS_BUCKET_ENV}/attachments`;
        
        var params = {
          ACL: 'public-read',
          Bucket: env.AWS_BUCKET,
          Body: fs.createReadStream(path),
          Key: `${keyName}/${req.files[i].originalname}`
        };
        
        let dd = await s3.upload(params).promise();
        fs.unlinkSync(path);
        await db.query(`INSERT INTO tasks_attachments (task_id, file) VALUES (?, ?)`, [req.body.taskId, dd.Location]);
      }

      db.query(`SELECT * FROM tasks_attachments WHERE task_id = ?`, [req.body.taskId], (error, result) => {
      	res.json({ error: false, result: result });
      });
    }
	});
}

exports.deleteAttachmentById = (req, res) => {
	db.query(`DELETE FROM tasks_attachments WHERE id = ?`, [req.params.id], (error, result) => {
		if(error) res.json({ error: true, result: error });

		res.json({ error: false, result: result });
	})
}

exports.createSubtask = (req, res) => {
	let form = {
		taskId: req.body.taskId,
		title: req.body.title
	};

	let sql = `INSERT INTO tasks_subtasks (task_id, title) VALUES (?, ?)`;
	db.query(sql, [form.taskId, form.title], (error, rows) => {
		if(error) res.json({ error: true, result: error });

		db.query(`SELECT * FROM tasks_subtasks WHERE id = ?`, [rows.insertId], (error, rows) => {
			res.json({ error: false, result: rows.length ? rows[0] : [] });
		});
	});
}

exports.updateSubtaskById = (req, res) => {
	let form = {
		title: req.body.title,
		status: req.body.status
	};

	let sql = `UPDATE tasks_subtasks SET title = ?, status = ? WHERE id = ?`;
	db.query(sql, [form.title, form.status, req.params.id], (error, rows) => {
		if(error) res.json({ error: true, result: error });

		db.query(`SELECT * FROM tasks_subtasks WHERE id = ?`, [req.params.id], (error, rows) => {
			res.json({ error: false, result: rows.length ? rows[0] : [] });
		});
	})
}

exports.deleteSubtaskById = (req, res) => {
	let sql = `DELETE FROM tasks_subtasks WHERE id = ?`;
	db.query(sql, [req.params.id], (error, rows) => {
		if(error) res.json({ error: true, result: error });

		res.json({ error: false, result: rows });
	})
}

exports.assigneUser = (req, res) => {
	let form = {
		taskId: req.body.taskId,
		userId: req.body.userId
	};

	let sql = `INSERT INTO tasks_assigne (task_id, user_id) VALUES (?, ?)`;
	db.query(sql, [form.taskId, form.userId], (error, rows) => {
		if(error) res.json({ error: true, result: error });

		let sql = `SELECT ta.*, u.name, u.email FROM tasks_assigne ta JOIN user u ON u.user_id = ta.user_id
			WHERE ta.task_id = ?`;
		db.query(sql, [form.taskId], (error, rows) => {
			res.json({ error: false, result: rows})
		})
	})
}

exports.deleteAssigne = (req, res) => {
	let sql = `DELETE FROM tasks_assigne WHERE id = ?`;
	db.query(sql, [req.params.id], (error, rows) => {
		if(error) res.json({ error: true, result: error });

		res.json({ error: false, result: rows });
	});
}

exports.deleteAssigneByTask = (req, res) => {
	let sql = `DELETE FROM tasks_assigne WHERE task_id = ?`;
	db.query(sql, [req.params.taskId], (error, rows) => {
		if(error) res.json({ error: true, result: error });

		res.json({ error: false, result: rows });
	});	
}

exports.deleteBulkAssigne = (req, res) => {
	let sql = `DELETE FROM tasks_assigne WHERE task_id = ? AND user_id = ?`;
	db.query(sql, [req.params.taskId, req.params.userId], (error, rows) => {
		if(error) res.json({ error: true, result: error });

		res.json({ error: false, result: rows });
	});	
}

exports.createTask = (req, res) => {
	let form = {
		name: req.body.name,
		start: req.body.start,
		end: req.body.end,

		projectId: req.body.projectId,
		creator: req.body.userId
	};

	let sql = `INSERT INTO tasks (name, start, end, project_id, creator) 
		VALUES ('${form.name}', '${form.start}', '${form.end}', '${form.projectId}', '${form.creator}')`;

	db.query(sql, (error, result) => {
		if(error) res.json({ error: true, result: error });

		db.query(`SELECT * FROM tasks WHERE id = '${result.insertId}'`, async (error, result) => {
			let sql = '';

			for(var i=0; i<result.length; i++) {
				sql = `SELECT * FROM tasks_subtasks WHERE task_id = '${result[i].id}'`;
				let subtasks = await db.query(sql);
				result[i].subtasks = subtasks;

				sql = `SELECT * FROM tasks_attachments WHERE task_id = '${result[i].id}'`;
				let attachments = await db.query(sql);
				result[i].attachments = attachments;
			}
			res.json({ error: false, result: result.length ? result[0] : [] });
		});
	});
}

exports.getTaskByProject = (req, res) => {
	let projectId = req.params.projectId;

	let sql = `SELECT t.*, t2.color AS tags_color, t2.label AS tags_label FROM tasks t LEFT JOIN project_tags t2 ON t.tags_id=t2.id WHERE t.project_id = '${projectId}' ORDER BY start ASC`;

	db.query(sql, async (error, result) => {
		if(error) res.json({ error: true, result: error });

		let sql = '';

		for(var i=0; i<result.length; i++) {
			sql = `SELECT ta.*, u.name, u.email FROM tasks_assigne ta JOIN user u ON u.user_id = ta.user_id
			WHERE ta.task_id = ?`;
			let assigne = await db.query(sql, [result[i].id]);
			result[i].assigne = assigne;

			sql = `SELECT * FROM tasks_subtasks WHERE task_id = '${result[i].id}'`;
			let subtasks = await db.query(sql);
			result[i].subtasks = subtasks;

			sql = `SELECT * FROM tasks_attachments WHERE task_id = '${result[i].id}'`;
			let attachments = await db.query(sql);
			result[i].attachments = attachments;
		}

		res.json({ error: false, result: result });
	})
}

exports.getTaskById = (req, res) => {
	let id = req.params.id;

	let sql = `SELECT t.*, t2.color AS tags_color, t2.label AS tags_label FROM tasks t LEFT JOIN project_tags t2 ON t.tags_id=t2.id WHERE t.id = '${id}'`;

	db.query(sql, async (error, result) => {
		if(error) res.json({ error: true, result: error });

		let sql = '';

		for(var i=0; i<result.length; i++) {
			sql = `SELECT ta.*, u.name, u.email FROM tasks_assigne ta JOIN user u ON u.user_id = ta.user_id
			WHERE ta.task_id = ?`;
			let assigne = await db.query(sql, [result[i].id]);
			result[i].assigne = assigne;

			sql = `SELECT * FROM tasks_subtasks WHERE task_id = '${result[i].id}'`;
			let subtasks = await db.query(sql);
			result[i].subtasks = subtasks;

			sql = `SELECT * FROM tasks_attachments WHERE task_id = '${result[i].id}'`;
			let attachments = await db.query(sql);
			result[i].attachments = attachments;
		}

		res.json({ error: false, result: result.length ? result[0] : [] });
	})
}

exports.updateTaskById = (req, res) => {
	let form = {};
	let sql = ``;
	let id = req.params.id;

	db.query(`SELECT is_lock FROM tasks WHERE id = '${id}'`, (error, result, fields) => {
		if(error) {
		  res.json({error: true, result: error})
		} else {
		  if (result[0].is_lock === 1 && req.body.hasOwnProperty('start') && req.body.hasOwnProperty('end')){
			  res.json({error: false, result: 'locked'})
		  }
		  else{
			sql = `UPDATE tasks SET `;
		
			if(req.body.hasOwnProperty('status') 
					&& req.body.hasOwnProperty('description') 
					&& req.body.hasOwnProperty('name')) {
		
				form.name = req.body.name;
				form.start = req.body.start;
				form.end = req.body.end;
				form.description = req.body.description;
				form.status = req.body.status;
		
				sql += `status = '${form.status}', description = '${form.description === 'null' ? '' : form.description}', name = '${form.name}' `;
			} else {
				if(req.body.hasOwnProperty('name')) {
					form.name = req.body.name;
		
					sql += `name = '${form.name}' `;
				}
		
				if(req.body.hasOwnProperty('start') && req.body.hasOwnProperty('end')) {
					form.start = req.body.start;
					form.end = req.body.end;
		
					sql += `start = '${form.start}', end = '${form.end}' `;
				}
			}
		
			sql += `WHERE id = '${id}'`;
		
			// res.json({ error: false, result: sql });
			
			db.query(sql, (error, result) => {
				if(error) res.json({ error: true, result: error });
		
				db.query(`SELECT * FROM tasks WHERE id = '${id}'`, async (error, result) => {
					let sql = '';
		
					for(var i=0; i<result.length; i++) {
						sql = `SELECT ta.*, u.name, u.email FROM tasks_assigne ta JOIN user u ON u.user_id = ta.user_id
						WHERE ta.task_id = ?`;
						let assigne = await db.query(sql, [result[i].id]);
						result[i].assigne = assigne;
						
						sql = `SELECT * FROM tasks_subtasks WHERE task_id = '${result[i].id}'`;
						let subtasks = await db.query(sql);
						result[i].subtasks = subtasks;
		
						sql = `SELECT * FROM tasks_attachments WHERE task_id = '${result[i].id}'`;
						let attachments = await db.query(sql);
						result[i].attachments = attachments;
					}
		
					res.json({ error: false, result: result.length ? result[0] : [] });
				});
			});
		  }
		}
	})

}

exports.lock = (req, res, next) => {
	let form = {
	  lock: req.body.lock
	}
  
	db.query(`UPDATE tasks SET is_lock = '${form.lock}' WHERE id = '${req.params.id}'`, (error, result, fields) => {
	  if(error) {
		res.json({error: true, result: error})
	  } else {
		res.json({error: false, result: result})
	  }
	})
}

exports.delete = (req, res, next) => {
	db.query(`DELETE FROM tasks WHERE id = '${req.params.id}'`, (error, result, fields) => {
	  if(error) {
		res.json({error: true, result: error})
	  } else {
		res.json({error: false, result: result})
	  }
	})
}