var env = require("../env.json");
var conf = require("../configs/config");
var db = require("../configs/database");
var fs = require('fs');
const asyncs = require('async');

var multer = require('multer');
const { strictEqual } = require("assert");
var storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, './public/company');
    },
    filename: (req, file, cb) => {
        cb(null, 'excel-' + Date.now() + '-' + file.originalname);
    }
});
var uploadExcel = multer({ storage: storage }).single('excel');
const validatorRequest = require('../validator');

/**
 * create webinar test
 * @param {Number} req.body.id  id webinar
 * @param {Array.<{tanya: String, a: String, b: String, c: String, ...}>} req.body.webinar_test
 * @param {Boolean} res.body.error
 * @param {String} res.body.result
 */
let create = (data, callback) => {
    //console.log('agus data : ', data)
    let arr_questions = ['tanya', 'jawab'];
    let arr_option = ['a', 'b', 'c', 'd', 'e'];
    let query_question = [];
    let query_option = [];

    // build data or validate
    //let questions = [data.id, elem.tanya, data.jenis, elem.jawab, 'Active'];
    data.webinar_test.forEach(str => {
        let tmp = [data.id, data.jenis, 'Active'];

        arr_questions.forEach(key => {
            tmp.push(str[key])
        });

        query_question.push(tmp);
    });

    data.webinar_test.forEach(str => {
        tmp = [];

        arr_option.forEach(key => {
            if (str[key] !== '') {
                tmp.push([0, key, str[key]])
            }
        });
        query_option.push(tmp);
    });

    // return console.log(query_option, 9090)
    // insert


    let x = 0;
    asyncs.eachSeries(query_question, (str, next) => {
        db.query(
            `INSERT INTO webinar_test_questions (webinar_id, jenis, status, pertanyaan, jawab) VALUES (?,?,?,?,?);`,
            str,
            (err2, res) => {
                if (err2) {
                    console.log(err2, x, 'rollback err 2')
                    return next({ error: true, result: "failed create webinar test" });
                } else {

                    let values = '';
                    query_option[x].forEach(keys => {
                        keys[0] = res.insertId;

                        if (keys[2]) {
                            db.query(
                                `INSERT INTO webinar_test_options (webinar_test_id, options, answer) VALUES(?,?,?) `,
                                keys
                            );
                        }
                    })

                    //con.rollback();
                    //console.log(res, query_option[x], x, values, 'AFTER INSERT quest')

                    x++;
                    next(null);

                }
            });
    }, (e) => {
        if (e) {
            return callback({ error: true, result: e });
        } else {
            db.query(
                `UPDATE webinars SET ${data.jenis === 0 ? 'waktu_pretest' : 'waktu_posttest'}=${data.waktu} 
                WHERE id='${data.id} '`
            );
            return callback({ error: false, result: "success create webinar test" });
        }
    })


    // data.webinar_test.map(async (elem) => {
    //     let questions = [data.id, elem.tanya, data.jenis, elem.jawab, 'Active'];
    //     console.log('agus : ', questions)
    //     let options = [];

    //     let insert_questions = async () => {
    //         try {
    //             return await db.query(
    //                 `INSERT INTO webinar_test_questions (webinar_id, pertanyaan, jenis, jawab, status)
    //                             VALUES (?);`,
    //                 [questions]
    //             );
    //         } catch (error) {
    //             return callback({ error: true, result: await error.message });
    //         }
    //     };

    //     let last_insert_id = await insert_questions();

    //     await Object.keys(elem).map((value, id) => {
    //         if (value !== 'tanya' && value !== 'jawab' && value !== 'id' && elem[value] !== "") {
    //             options.push([last_insert_id.insertId, value, elem[value]]);
    //         }
    //     });

    //     db.query(
    //         `UPDATE webinars SET ${data.jenis === 0 ? 'waktu_pretest' : 'waktu_posttest'}=${data.waktu} WHERE id='${data.id}'`
    //     );

    //     let insert_options = async () => {
    //         try {
    //             await db.query(
    //                 `INSERT INTO webinar_test_options (webinar_test_id, options, answer)
    //                             VALUES ?;`,
    //                 [options]
    //             );
    //         } catch (error) {
    //             return callback({ error: true, result: await error.message });
    //         }
    //     };

    //     await insert_options();
    // });
    // return callback({ error: false, result: "success create webinar test" });
};

let create_polling = (data, callback) => {
    //console.log('agus data : ', data)
    let arr_option = ['a', 'b', 'c', 'd', 'e'];
    let query_question = [];

    // build data or validate
    data.webinar_test.forEach(str => {
        let tmp = [data.id, str.jenis, 'Active', str.tanya];
        let option_type = [];

        if (str.jenis != 3) {

            arr_option.forEach(key => {
                if (str[key] && str[key] !== "") {
                    option_type[key] = str[key];
                }
            });

        } else {
            // FREETEXT
            option_type[arr_option[0]] = str[arr_option[0]];
        }

        let jsons = JSON.stringify(Object.assign({}, option_type));
        tmp.push(jsons)
        query_question.push(tmp);
    });


    // insertIds
    var insertIds = [];
    let x = 0;
    asyncs.eachSeries(query_question, (str, next) => {

        db.query(
            `INSERT INTO webinar_test_polling (webinar_id, jenis, status, pertanyaan, jawab) VALUES (?,?,?,?,?);`,
            str,
            (err2, res) => {
                if (err2) {
                    console.log(err2, x, 'rollback err 2')
                    return next({ error: true, result: "failed create webinar test" });
                } else {
                    insertIds.push(res.insertId)
                    next(null);
                }
            });

    }, (e) => {
        if (e) {
            return callback({ error: true, result: e });
        } else {
            return callback({ error: false, insertIds, result: "success create webinar test" });
        }
    });
};

exports.create = async (req, res) => {
    create(req.body, (result) => res.json(result));
};

exports.create_polling = async (req, res) => {
    create_polling(req.body, (result) => res.json(result));
};

/**
 * delete webinar test
 * @param {String} req.params.idÍ
 * @param {Boolean} res.body.error
 * @param {String} res.body.result
 */
let remove = (data, callback) => {
    db.query(
        `UPDATE webinar_test_questions SET status='Inactive'
        WHERE webinar_id=${data.id}
        AND jenis=${data.jenis};`,
        (error, result) => {
            if (error) {
                return callback({ error: true, result: error.message });
            } else {
                if (result.affectedRows !== 0) {
                    return callback({ error: false, result: "success delete webinar test" });
                } else {
                    return callback({ error: true, result: "failed delete webinar test" });
                }
            }
        }
    );
};

let remove_polling = (data, callback) => {
    db.query(
        `UPDATE webinar_test_polling SET status='Inactive'
        WHERE webinar_id=${data.id};`,
        (error, result) => {
            if (error) {
                return callback({ error: true, result: error.message });
            } else {
                if (result.affectedRows !== 0) {
                    return callback({ error: false, result: "success delete webinar test" });
                } else {
                    return callback({ error: true, result: "failed delete webinar test" });
                }
            }
        }
    );
};

exports.delete = (req, res) => {
    remove(req.params, (result) => res.json(result));
};

exports.update = async (req, res) => {
    try {
        let form = req.body;
        let input = await validatorRequest.prepostTest(form);
        remove(req.body, (result) => {
            if (result.error) {
                res.json({ error: true, result: "failed update webinar test" });
            } else {
                create(req.body, (callback) => {
                    if (callback.error) {
                        res.json({ error: true, result: "failed update webinar test" });
                    } else {
                        res.json({ error: false, result: "success update webinar test" });
                    }
                });
            }
        });

    } catch (error) {
        res.json({ error: true, result: error });
    }
};


exports.readPeserta = (req, res) => {
    let { jenis, user_id, id } = req.params;
    if (jenis == 'essay') {
        db.query(`SELECT
                webinar_essay_answer.*, If(webinar_tamu.name is not null, webinar_tamu.name, user.name) as name
            FROM 
                webinar_essay_answer
                JOIN webinars ON webinars.id = webinar_essay_answer.webinar_id
                LEFT JOIN user ON user.user_id = webinar_essay_answer.user_id
                LEFT JOIN webinar_tamu ON webinar_tamu.voucher = webinar_essay_answer.user_id
            WHERE 
                webinar_essay_answer.user_id = ?
                AND webinar_essay_answer.webinar_id = ?`, [user_id, id], (error, result) => {
            if (error) {
                res.json({ error: true, result: error.message });
            } else {
                res.json({ error: false, result: result, terjawab: result.length > 0 ? true : false });
            }
        })
    } else {
        db.query(
            `SELECT t1.*, t1.id AS question_id, t2.*, t2.id AS option_id FROM webinar_test_questions t1
            INNER JOIN webinar_test_options t2
            ON t1.id = t2.webinar_test_id
            WHERE t1.webinar_id = ${req.params.id}
            AND t1.status ='Active'
            AND t1.jenis = ${req.params.jenis}
            ORDER BY t1.id ASC;`,
            (error, result) => {
                if (error) {
                    res.json({ error: true, result: error.message });
                } else {
                    let group = result.reduce((accumulator, current) => {
                        if (!accumulator[current.webinar_test_id]) {
                            accumulator[current.webinar_test_id] = {};
                            accumulator[current.webinar_test_id]["tanya"] = current.pertanyaan;
                        }
                        accumulator[current.webinar_test_id][current.options] = [current.option_id, current.answer];
                        accumulator[current.webinar_test_id]["question_id"] = current.question_id;

                        return accumulator;
                    }, {});

                    let kuesioner = [];
                    Object.keys(group).map((elem) => {
                        kuesioner.push(group[elem]);
                    });
                    db.query(
                        `SELECT COUNT(a.id) AS jawaban FROM webinar_test_answer a JOIN webinar_test_questions q ON a.webinar_questions=q.id WHERE a.user_id='${req.params.user_id}' AND a.webinar_id='${req.params.id}' AND q.jenis='${req.params.jenis}'`,
                        (error, result) => {
                            let jawaban = result[0].jawaban;
                            db.query(
                                `SELECT COUNT(q.id) AS pertanyaan FROM webinar_test_questions q WHERE q.webinar_id='${req.params.id}' AND q.jenis='${req.params.jenis}'`,
                                (error, result) => {
                                    if (error) { res.json({ error: true, result: error }) }
                                    let pertanyaan = result[0].pertanyaan;
                                    db.query(
                                        `SELECT ${req.params.jenis === '0' ? 'waktu_pretest' : 'waktu_posttest'} AS waktu FROM webinars WHERE id='${req.params.id}'`,
                                        (error, result) => {
                                            if (error) { res.json({ error: true, result: error }) }
                                            res.json({ error: false, result: kuesioner, waktu: result[0].waktu, enable: pertanyaan > 0 ? true : false, terjawab: jawaban > 0 ? true : false });
                                        })
                                })
                        })
                }
            }
        );
    }
};
exports.read = (req, res) => {
    db.query(
        `SELECT t1.id as idpertanyaan, t1.id, t1.webinar_id, t1.pertanyaan, t1.jenis, LOWER(t1.jawab) as jawab , t1.status , t2.* FROM webinar_test_questions t1
        INNER JOIN webinar_test_options t2
        ON t1.id = t2.webinar_test_id
        WHERE t1.webinar_id = ${req.params.id}
        AND t1.status = 'Active'
        AND t1.jenis = ${req.params.jenis}
        ORDER BY t1.id ASC;`,
        (error, result) => {
            if (error) {
                res.json({ error: true, result: error.message });
            } else {
                let group = result.reduce((accumulator, current) => {
                    if (!accumulator[current.webinar_test_id]) {
                        accumulator[current.webinar_test_id] = {};
                        accumulator[current.webinar_test_id]["tanya"] = current.pertanyaan;
                        accumulator[current.webinar_test_id]["jawab"] = current.jawab;
                    }
                    accumulator[current.webinar_test_id]["id"] = current.idpertanyaan;
                    accumulator[current.webinar_test_id][current.options] = current.answer;

                    return accumulator;
                }, {});

                let kuesioner = [];
                Object.keys(group).map((elem) => {
                    kuesioner.push(group[elem]);
                });

                db.query(
                    `SELECT ${req.params.jenis === '0' ? 'waktu_pretest' : 'waktu_posttest'} AS waktu FROM webinars WHERE id='${req.params.id}'`,
                    (error, result) => {
                        if (error) { res.json({ error: true, result: error }) }
                        res.json({ error: false, result: kuesioner, waktu: result[0].waktu });
                    })
            }
        }
    );
};

exports.webinarEssay = (req, res) => {
    const { id, user_id, answer } = req.body
    let sql = `SELECT * FROM webinar_essay_answer WHERE webinar_id = ? AND user_id = ?`
    db.query(sql, [id, user_id], (error, result) => {
        if (error) {
            res.json({ error: true, result: error.message });
        } else {
            if (result.length > 0) {
                let sql = `UPDATE webinar_essay_answer SET answer = ? WHERE webinar_id = ? AND user_id = ?`
                db.query(sql, [answer, id, user_id], (error, result) => {
                    if (error) {
                        res.json({ error: true, result: error.message });
                    } else {
                        res.json({ error: false, result: result });
                    }
                })
            } else {
                let sql = `INSERT INTO webinar_essay_answer (answer, webinar_id, user_id) VALUES (?)`
                db.query(sql, [[answer, id, user_id]], (error, result) => {
                    if (error) {
                        res.json({ error: true, result: error.message });
                    } else {
                        res.json({ error: false, result: result });
                    }
                })
            }
        }
    })
}

/**
 * create webinar test
 * @param {Number} req.body.id  id webinar
 * @param {Array.<{questions_id: String, options_id: String}>} req.body.webinar_test
 * @param {Boolean} res.body.error
 * @param {String} res.body.result
 */
exports.input = async (req, res) => {
    let data = [];
    if (req.body.webinar_test.length >= 1) {
        await req.body.webinar_test.map((elem) => {
            data.push([req.body.id, req.body.user_id, req.body.pengguna, elem.questions_id, elem.options_id]);
        });
        await db.query(
            `INSERT INTO webinar_test_answer
                (webinar_id, user_id, pengguna, webinar_questions, webinar_options)
            VALUES ?;`,
            [data],
            (error, result) => {
                if (error) {
                    res.json({ error: true, result: error.message });
                } else {
                    res.json({ error: false, result: "success input webinar test" });
                }
            }
        );
    }
    else {
        res.json({ error: false, result: "success input webinar test tanpa menjawab" });
    }
};

// exports.result = (req, res) => {
//     db.query(
//         `SELECT t2.id, t2.pertanyaan, t3.options, t3.answer, count(t3.answer) AS count
//         FROM kuesioner_answer t1
//         INNER JOIN kuesioner_questions t2
//         ON t1.webinar_questions = t2.id
//         INNER JOIN kuesioner_options t3
//         ON t1.webinar_options = t3.id
//         WHERE t1.webinar_id = '${req.params.id}'
//         GROUP BY t2.id, t3.id;`,
//         (error, result) => {
//             if (error) {
//                 res.json({ error: true, result: error.message });
//             } else {
//                 let group = result.reduce((accumulator, current) => {
//                     if (!accumulator[current.id]) {
//                         accumulator[current.id] = {};
//                         accumulator[current.id]["tanya"] = current.pertanyaan;
//                     }
//                     accumulator[current.id][current.options] = current.answer;
//                     accumulator[current.id]["count"] = current.count;

//                     return accumulator;
//                 }, {});

//                 let kuesioner = [];
//                 Object.keys(group).map((elem) => {
//                     kuesioner.push(group[elem]);
//                 });

//                 res.json({ error: false, result: kuesioner });
//             }
//         }
//     );
// };

exports.result = (req, res) => {
    db.query(
        `SELECT a.id, a.jenis, b.id
        FROM webinar_test_questions a
        INNER JOIN webinar_test_options b
        ON a.id = b.webinar_test_id
        AND a.jawab = b.options
        AND a.status = 'Active'
        WHERE a.webinar_id = '${req.params.id}';
        
        SELECT a.user_id, IF(a.pengguna, d.name, e.name) AS name, a.pengguna, b.jenis, a.webinar_options, c.id
        FROM webinar_test_answer a 
        INNER JOIN webinar_test_questions b
        ON a.webinar_questions = b.id
        INNER JOIN webinar_test_options c
        ON b.id = c.webinar_test_id AND b.jawab = c.options
        LEFT JOIN user d
        ON a.user_id = d.user_id
        LEFT JOIN webinar_tamu e
        ON a.user_id = e.voucher
        WHERE a.webinar_id = '${req.params.id}'`,

        /*`SELECT IF(a.jenis = 0, @jenis0 := COUNT(a.id), @jenis1 := COUNT(a.id)), a.jenis 
        FROM webinar_test_questions a 
        WHERE a.webinar_id = '${req.params.id}'
        GROUP BY a.jenis;
        
        SELECT a.webinar_id, b.jenis, a.user_id, u.name, IF(b.jenis = 0, (COUNT(a.id)/@jenis0)*100, (COUNT(a.id)/@jenis1)*100) AS 'betul'
        FROM webinar_test_answer a
        INNER JOIN (
            SELECT user_id, name FROM user
            UNION
            SELECT voucher AS user_id, name FROM webinar_tamu
        ) u
        ON u.user_id = a.user_id
        INNER JOIN webinar_test_questions b
        ON a.webinar_questions = b.id
        INNER JOIN webinar_test_options c
        ON a.webinar_options = c.id
        AND c.options = b.jawab
        WHERE a.webinar_id = '${req.params.id}'
        GROUP BY b.jenis, a.user_id
        ORDER BY b.jenis;`,*/
        async (error, result) => {
            if (error) {
                res.json({ error: true, result: error.message });
            } else {
                console.log(result)
                let c = result[0].reduce((accumulator, current) => {
                    if (current.jenis === 0) {
                        accumulator.pretest = accumulator.pretest + 1;
                    } else if (current.jenis === 1) {
                        accumulator.posttest = accumulator.posttest + 1;
                    };

                    return accumulator;
                }, { pretest: 0, posttest: 0 });

                let b = result[1].reduce((accumulator, current) => {
                    if (!accumulator[current.user_id]) {
                        accumulator[current.user_id] = {};
                        accumulator[current.user_id]['user_id'] = current.user_id;
                        accumulator[current.user_id]['name'] = current.name;
                        if (current.jenis === 0) {
                            accumulator[current.user_id]['pretest'] = { benar: 0, salah: 0, total: c.pretest, nilai: 0 };
                            accumulator[current.user_id]['posttest'] = { benar: 0, salah: 0, total: c.posttest, nilai: 0 }
                            if (current.webinar_options === current.id) {
                                accumulator[current.user_id]['pretest']['benar'] = accumulator[current.user_id]['pretest']['benar'] + 1;
                            } else {
                                accumulator[current.user_id]['pretest']['salah'] = accumulator[current.user_id]['pretest']['salah'] + 1;
                            };
                        } else if (current.jenis === 1) {
                            accumulator[current.user_id]['pretest'] = { benar: 0, salah: 0, total: c.pretest, nilai: 0 };
                            accumulator[current.user_id]['posttest'] = { benar: 0, salah: 0, total: c.posttest, nilai: 0 };
                            if (current.webinar_options === current.id) {
                                accumulator[current.user_id]['posttest']['benar'] = accumulator[current.user_id]['posttest']['benar'] + 1;
                            } else {
                                accumulator[current.user_id]['posttest']['salah'] = accumulator[current.user_id]['posttest']['salah'] + 1;
                            };
                        }
                    }
                    else {
                        if (current.jenis === 0) {
                            if (current.webinar_options === current.id) {
                                accumulator[current.user_id]['pretest']['benar'] = accumulator[current.user_id]['pretest']['benar'] + 1;
                            } else {
                                accumulator[current.user_id]['pretest']['salah'] = accumulator[current.user_id]['pretest']['salah'] + 1;
                            };
                        } else if (current.jenis === 1) {
                            if (current.webinar_options === current.id) {
                                accumulator[current.user_id]['posttest']['benar'] = accumulator[current.user_id]['posttest']['benar'] + 1;
                            } else {
                                accumulator[current.user_id]['posttest']['salah'] = accumulator[current.user_id]['posttest']['salah'] + 1;
                            };
                        }
                    }

                    accumulator[current.user_id]['pretest'].nilai = (accumulator[current.user_id]['pretest']['benar'] / accumulator[current.user_id]['pretest'].total) * 100;
                    accumulator[current.user_id]['posttest'].nilai = (accumulator[current.user_id]['posttest']['benar'] / accumulator[current.user_id]['posttest'].total) * 100;
                    accumulator[current.user_id]['selisih'] = accumulator[current.user_id]['posttest'].nilai - accumulator[current.user_id]['pretest'].nilai


                    accumulator[current.user_id]['pretest'].nilai = accumulator[current.user_id]['pretest'].nilai ? accumulator[current.user_id]['pretest'].nilai : 0;
                    accumulator[current.user_id]['posttest'].nilai = accumulator[current.user_id]['posttest'].nilai ? accumulator[current.user_id]['posttest'].nilai : 0;
                    accumulator[current.user_id]['selisih'] = accumulator[current.user_id]['selisih'] ? accumulator[current.user_id]['selisih'] : 0;
                    return accumulator;
                }, {});

                return res.json({ error: false, result: await b });

                // let hasil = result[1].reduce((accumulator, current) => {
                //     if(!accumulator[current.user_id]) {
                //         accumulator[current.user_id] = {};
                //         accumulator[current.user_id]['user_id'] = current.user_id;
                //         accumulator[current.user_id]['name'] = current.name;
                //         if(current.jenis === 0) {
                //             accumulator[current.user_id]['pretest'] = current.betul;
                //             accumulator[current.user_id]['posttest'] = 0;
                //         } else {
                //             accumulator[current.user_id]['pretest'] = 0;
                //             accumulator[current.user_id]['posttest'] = current.betul;
                //         };
                //     };

                //     if(current.jenis === 0) {
                //         accumulator[current.user_id]['pretest'] = current.betul;
                //         accumulator[current.user_id]['selisih'] = accumulator[current.user_id]['posttest'] - accumulator[current.user_id]['pretest'];
                //     } else {
                //         accumulator[current.user_id]['posttest'] = current.betul;
                //         accumulator[current.user_id]['selisih'] = accumulator[current.user_id]['posttest'] - accumulator[current.user_id]['pretest'];
                //     };

                //     return accumulator;
                // }, {})

                // res.json({ error: false, result: await hasil });
            }
        }
    );
};

// exports.result = (req, res) => {
//     db.query(
//         `SELECT b.id AS question_id, b.pertanyaan, c.id AS answer_id, c.answer, a.user_id, IF(a.pengguna,d.name,e.name) AS name
//         FROM webinar_test_answer a
//         INNER JOIN webinar_test_questions b
//         ON a.webinar_questions = b.id
//         INNER JOIN webinar_test_options c
//         ON a.webinar_options = c.id
//         LEFT JOIN user d
//         ON a.user_id = d.user_id
//         LEFT JOIN webinar_tamu e
//         ON a.user_id = e.id
//         WHERE a.webinar_id = '${req.params.id}'
//         AND b.jenis = '${req.params.jenis}'
//         ORDER BY user_id, question_id`,
//         async (error, result) => {
//             console.log(result)
//             if (error) {
//                 res.json({ error: true, result: error.message });
//             } else {
//                 let group = await result.reduce(
//                     (accumulator, current) => {
//                         if (!accumulator.jawaban[current.user_id]) {
//                             accumulator.jawaban[current.user_id] = {};
//                             accumulator.jawaban[current.user_id]["nama"] = current.name;
//                             accumulator.jawaban[current.user_id]["jawaban"] = [];
//                         }

//                         if (!accumulator.pertanyaan[current.question_id]) {
//                             accumulator.pertanyaan[current.question_id] = {};
//                             accumulator.pertanyaan[current.question_id]["pertanyaan"] = current.pertanyaan;
//                         }

//                         accumulator.jawaban[current.user_id]["jawaban"].push(current.answer);

//                         return accumulator;
//                     },
//                     { pertanyaan: {}, jawaban: {} }
//                 );

//                 let pertanyaan = [],
//                     jawaban = [];
//                 Object.keys(await group.pertanyaan).map((elem) => {
//                     pertanyaan.push(group.pertanyaan[elem].pertanyaan);
//                 });
//                 Object.keys(await group.jawaban).map((elem) => {
//                     jawaban.push(group.jawaban[elem]);
//                 });

//                 res.json({ error: false, result: { pertanyaan: await pertanyaan, jawaban: await jawaban } });
//             }
//         }
//     );
// };

exports.import = (req, res, next) => {
    uploadExcel(req, res, (err) => {
        if (!req.file) {
            res.json({ error: true, result: err });
        } else {

            // create class Excel
            var Excel = require('exceljs');
            var wb = new Excel.Workbook();
            var path = require('path');
            var filePath = path.resolve(__dirname, '../public/company/' + req.file.filename);

            wb.xlsx.readFile(filePath).then(async function () {

                var sh = wb.getWorksheet("Sheet1");
                let tmp = [];

                sh.eachRow((row, rowNumber) => {

                    if (rowNumber > 1) {
                        tmp.push(row.values);
                    }
                });

                let opsi = ["a", "b", "c", "d", "e"];
                let tempArray = [];
                let q_tanya = [];
                let q_opsi = [];
                let duplicate = []
                // checking
                tmp.forEach((str)=>{
                    let idx = q_tanya.findIndex((item)=>{ return item === str[2]});
                    if(idx == -1){
                        q_tanya.push(str[2]); 
                    }else{
                        duplicate.push(str[2]); 
                    }
                });
                if(duplicate.length){
                    return res.json({ error:true, result:`Duplicate question : '${duplicate[0]}'` });
                }

                asyncs.eachSeries(tmp, (str, cb) => {

                    let x = 0;
                    let builds = {
                        webinarId: req.body.webinarId,
                        kuesionerId: null,
                        pertanyaan: str[2],
                        jawaban: str[3],
                        a: null,
                        aId: null,
                        b: null,
                        bId: null,
                        c: null,
                        cId: null,
                        d: null,
                        dId: null,
                        e: null,
                        eId: null,
                    };

                    // str[2] pertanyaan
                    // str[3] jawaban
                    // str[4-n] pilihan
                    db.query(
                        `INSERT INTO webinar_test_questions (webinar_id, jenis, status, pertanyaan, jawab) VALUES (?,?,?,?,?);`,
                        [req.body.webinarId, req.body.jenis, 'Active', str[2], str[3]],
                        (err_quest, res_quest) => {

                            try {

                                builds.kuesionerId = res_quest.insertId;

                                for (var i = 4; i < str.length; i++) {

                                    let res_opsi = db.query(
                                        `INSERT INTO webinar_test_options (webinar_test_id, options, answer) VALUE(?,?,?);`,
                                        [res_quest.insertId, opsi[x], str[i]],
                                    );
                                    builds[opsi[x]] = str[i];
                                    builds[`${opsi[x]}Id`] = res_opsi.insertId;
                                    x++;
                                }
                                tempArray.push(builds);
                                return cb(null);

                            } catch (e) {
                                console.log(e, 'err 3')
                                return cb({ error: true, result: "failed import webinar test EDB-INSERT" });
                            }
                        }
                    );

                }, (e) => {
                    fs.unlinkSync(filePath);
                    if (e) { return res.json(e); }
                    else {
                        return res.json({ error: false, result: tempArray });
                    }
                });

            });

        }
    })
}

exports.import_polling = (req, res, next) => {
    uploadExcel(req, res, (err) => {
        if (!req.file) {
            res.json({ error: true, result: err });
        } else {

            // create class Excel
            var Excel = require('exceljs');
            var wb = new Excel.Workbook();
            var path = require('path');
            var filePath = path.resolve(__dirname, '../public/company/' + req.file.filename);

            wb.xlsx.readFile(filePath).then(async function () {

                var sh = wb.getWorksheet("Sheet1");
                let tmp = [];

                sh.eachRow((row, rowNumber) => {

                    if (rowNumber > 5) {

                        tmp.push(row.values);
                    }
                });

                let opsi = ["a", "b", "c", "d", "e"];
                let tempArray = [];
                let q_tanya = [];
                let q_opsi = [];
                let duplicate = []
                // checking
                tmp.forEach((str)=>{
                    let idx = q_tanya.findIndex((item)=>{ return item === str[3]});
                    if(idx == -1){
                        q_tanya.push(str[3]); 
                    }else{
                        duplicate.push(str[3]); 
                    }
                });
                if(duplicate.length){
                    return res.json({ error:true, result:`Duplicate question : '${duplicate[0]}'` });
                }
                asyncs.eachSeries(tmp, (str, cb) => {

                    try {

                        let x = 0;
                        let option_type = [];
                        let jenis = str[2].toLowerCase();
                        if (jenis === 'multiple choice') {
                            str[5] = str[5].toLowerCase(); // converting to string 'true' | 'false'
                            str[4] = str[4].toLowerCase(); // converting to string 'true' | 'false'
                            if (str[8] || str[7]) {
                                str[2] = 1; // a/b/c/d/e
                            } else if (str.length == 7 || str[6]) {
                                str[2] = 2; // yes/no/abs
                            } else if (
                                ((str[5] == false || str[4] == true) || (str[5] == true || str[4] == false) || (str[5] == 'false' || str[4] == 'true') || (str[5] == 'true' || str[4] == 'false')) &&
                                !str[6]
                            ) {
                                str[2] = 0; // true/false
                            }

                            for (let t = 4; t < str.length; t++) {
                                if (opsi[x]) {
                                    option_type[opsi[x]] = str[t];
                                }
                                x++;
                            }
                        } else {
                            str[2] = 3; // freetext
                            option_type[opsi[x]] = str[4] || "";
                        }

                        let builds = {
                            webinarId: req.body.webinarId,
                            pollingId: null,
                            pertanyaan: str[3],
                            jenis: str[2],
                            result: []
                        };


                        // str[2] jenis
                        // str[3] questions
                        // str[4-n] pilihan or freetext

                        // req.body.jenis = "multiple choices" or "freetext"
                        // if multiple choice and option exists A/B/C/D or greater than 3 options = "A/B/C/D/E"
                        // if multiple choice and option exists A/B/C or greater than 2 options = "AGRESS/DISAGREE/ABS"
                        // if multiple choice and option exists A/B or only 2 options = "TRUE/FALSE"

                        // field jawab TYPE TEXT, JSON.stringify for convert

                        option_type = Object.assign({}, option_type);
                        let jsons = JSON.stringify(option_type);

                        console.log(str, jsons, req.body.webinarId, '???')
                        db.query(
                            `INSERT INTO webinar_test_polling (webinar_id, jenis, status, pertanyaan, jawab) VALUES (?,?,?,?,?);`,
                            [req.body.webinarId, str[2], 'Active', str[3], jsons],
                            (err_quest, res_quest) => {

                                if (err_quest) {

                                    console.log(err_quest, 'err 3')
                                    return cb({ error: true, result: "failed import webinar polling EDB-INSERT 2" });
                                }

                                try {
                                    builds.pollingId = res_quest.insertId;
                                    builds = Object.assign({}, builds, option_type);
                                    tempArray.push(builds);
                                    return cb(null);

                                } catch (e) {
                                    console.log(e, 'err 4')
                                    return cb({ error: true, result: "failed import webinar polling EDB-INSERT" });
                                }
                            }
                        );
                    } catch (error) {
                        cb({ error: true, result: "Format file incorrect, please check your file upload." })
                        return;
                    }

                }, (e) => {
                    fs.unlinkSync(filePath);
                    console.log(filePath, "PATH")
                    if (e) {
                        return res.json(e);
                    } else {
                        return res.json({ error: false, result: tempArray });
                    }
                });

            });

        }
    })
}

exports.view_polling_live = (req, res) => {

    //if (typeof req.params.id === 'number') {

    db.query(
        `select id, webinar_id , pertanyaan as tanya, jenis, result as answer, jawab, polling_status AS status from webinar_test_polling where webinar_id = ? and status='Active' order by id desc;`,
        [req.params.id],
        async (err, result) => {
            if (err) {
                return res.json({ error: true, result: err });
            }
            else {
                let tmp = []
                await Promise.all(result.map(async (item) => {
                    let totalResponden = await db.query(`SELECT COUNT(*) AS total_responden FROM webinar_test_polling_answer WHERE poll_id='${item.id}'`);
                    item.total_responden = totalResponden[0].total_responden;
                    let jawaban = await db.query(`SELECT answer AS value, COUNT(*) AS total FROM webinar_test_polling_answer WHERE poll_id='${item.id}' GROUP BY answer`);
                    Promise.all(jawaban.map((x) => {
                        x.percent = x.total / totalResponden[0].total_responden * 100;
                        x.percent = x.percent.toFixed(0);
                    }))
                    item.answer = totalResponden.length ? jawaban : [];

                    let jsons = JSON.parse(item.jawab);
                    jsons = Object.assign({}, jsons)
                    //return console.log(jsons)
                    delete item.jawab;
                    str = Object.assign({}, item, jsons);
                    tmp.push(str);
                }));

                tmp.sort((a, b) => (a.id < b.id ? -1 : 1));

                let j = 1;
                tmp.forEach((str) => { str.no = j; j++; })
                return res.json({ error: false, result: tmp });
            }
        }
    )

};

exports.view_polling = (req, res) => {

    //if (typeof req.params.id === 'number') {

    db.query(
        `select id, webinar_id , pertanyaan as tanya, jenis, jawab from webinar_test_polling where webinar_id = ? and status='Active';`,
        [req.params.id],
        (err, result) => {
            let tmp = []
            asyncs.eachSeries(result, (str, next) => {
                // if (!str.result) {
                //     str.status = 'Draft';
                // }

                let jsons = JSON.parse(str.jawab);
                jsons = Object.assign({}, jsons)
                //return console.log(jsons)
                delete str.jawab;
                // if (!str.answer) {
                //     str.answer = [];
                // }
                let builds = {
                    webinarId: str.webinar_id,
                    pollingId: str.id,
                    pertanyaan: str.tanya,
                    jenis: str.jenis,
                };
                builds = Object.assign({}, str, jsons);
                tmp.push(builds);
                next(null);
            }, (e) => {
                return res.json({ error: false, result: tmp });
            })
        }
    )

};

exports.deleteByIdPolling = (req, res) => {
    // delete on table webinar options
    // console.log(12)
    let sql = `UPDATE webinar_test_polling SET status='Inactive' WHERE id = ?`;
    db.query(sql, [req.params.id], (error, rows) => {
        if (error) res.json({ error: true, result: error })

        res.json({ error: false, result: rows })
    })
}

exports.update_polling = (req, res) => {
    remove_polling(req.body, (result) => {
        if (result.error) {
            res.json({ error: true, result: "failed update webinar test" });
        } else {
            create_polling(req.body, (callback) => {
                if (callback.error) {
                    res.json({ error: true, result: "failed update webinar test" });
                } else {
                    res.json({ error: false, result: "success update webinar test" });
                }
            });
        }
    });
};
exports.update_polling_status = (req, res) => {
    let sql = `UPDATE webinar_test_polling SET polling_status='${req.body.status}' WHERE id = '${req.params.id}'`;
    db.query(sql, (error, rows) => {
        if (error) {
            return res.json({ error: true, result: error });
        }
        else {
            return res.json({ error: false, result: rows });
        }
    })
}
exports.submit_polling = (req, res) => {
    // delete on table webinar options
    let sql = `INSERT INTO webinar_test_polling_answer (user_id, pengguna, poll_id, answer) VALUES (?)`;
    db.query(sql, [[req.body.user_id, req.body.pengguna, req.body.poll_id, req.body.answer]], (error, rows) => {
        if (error) res.json({ error: true, result: error })
        else {
            res.json({ error: false, result: rows })
        }
    })
}

exports.update_polling_id = (req, res) => {
    let data = req.body;
    if (req.params.id && data.webinar_id) {
        let arr_option = ['a', 'b', 'c', 'd', 'e'];
        let query_question = [];

        // build data or validate
        data.webinar_test.forEach(str => {
            let option_type = [];

            if (str.jenis != 3) {

                arr_option.forEach(key => {
                    if (str[key] && str[key].length !== "") {
                        option_type[key] = str[key];
                    }
                });

            } else {
                // FREETEXT
                option_type[arr_option[0]] = str[arr_option[0]];
            }

            let jsons = JSON.stringify(Object.assign({}, option_type));
            db.query(
                `update webinar_test_polling set pertanyaan = ?, jawab =?, jenis=? where webinar_id=? and id=?`,
                [str.tanya, jsons, str.jenis, data.webinar_id, data.id],
                (err, result) => {
                    if (err) {
                        return res.json({ error: true, result: err });
                    } else {
                        return res.json({ error: false, result: "Update success" });

                    }
                }
            )
        });
    } else {
        return res.json({ error: true, result: "Validation error" });
    }
};

exports.create_polling_id = (req, res) => {
    let data = req.body;
    if (data.id) {
        let arr_option = ['a', 'b', 'c', 'd', 'e'];
        let query_question = [];

        // build data or validate
        data.webinar_test.forEach(str => {
            let option_type = [];

            if (str.jenis != 3) {

                arr_option.forEach(key => {
                    if (str[key] && str[key].length !== "") {
                        option_type[key] = str[key];
                    }
                });

            } else {
                // FREETEXT
                option_type[arr_option[0]] = str[arr_option[0]];
            }

            let jsons = JSON.stringify(Object.assign({}, option_type));
            db.query(
                `INSERT INTO webinar_test_polling (webinar_id, jenis, status, polling_status, pertanyaan, jawab) VALUES (?,?,?,?,?,?)`,
                [data.id, str.jenis, 'Active', 'Draft', str.tanya, jsons],
                (err, result) => {
                    if (err) {
                        return res.json({ error: true, result: err });
                    } else {
                        return res.json({ error: false, result: result });

                    }
                }
            )
        });
    } else {
        return res.json({ error: true, result: "Validation error" });
    }
};

exports.import_old = (req, res, next) => {
    uploadExcel(req, res, (err) => {
        if (!req.file) {
            res.json({ error: true, result: err });
        } else {

            // create class Excel
            var Excel = require('exceljs');
            var wb = new Excel.Workbook();
            var path = require('path');
            var filePath = path.resolve(__dirname, '../public/company/' + req.file.filename);

            wb.xlsx.readFile(filePath).then(async function () {

                var sh = wb.getWorksheet("Sheet1");

                //Get all the rows data [1st and 2nd column]    
                var webinarId = req.body.webinarId;
                var jenis = req.body.jenis;
                // var tempArray = [];
                for (i = 2; i <= sh.rowCount; i++) {

                    console.log('==========================================================')

                    var querySoal = `INSERT INTO webinar_test_questions (webinar_id, jenis, pertanyaan, jawab, status) VALUES ('${webinarId}', '${jenis}', '${sh.getRow(i).getCell(2).value}', '${sh.getRow(i).getCell(3).value}', 'Active')`;
                    console.log(querySoal)

                    // proses insert soal
                    const result = await db.query(querySoal);

                    // create variable
                    let aId = { insertId: null }, bId = { insertId: null }, cId = { insertId: null }, dId = { insertId: null }, eId = { insertId: null };

                    // pilihan jawaban A
                    if (sh.getRow(i).getCell(4).value) {
                        var queryJawab = `INSERT INTO webinar_test_options (webinar_test_id, options, answer) VALUES ('${result.insertId}', 'a', '${sh.getRow(i).getCell(4).value}')`;
                        console.log(queryJawab)
                        aId = await db.query(queryJawab)
                    }

                    // B
                    if (sh.getRow(i).getCell(5).value) {
                        var queryJawab = `INSERT INTO webinar_test_options (webinar_test_id, options, answer) VALUES ('${result.insertId}', 'b', '${sh.getRow(i).getCell(5).value}')`;
                        console.log(queryJawab)
                        bId = await db.query(queryJawab)
                    }

                    // C
                    if (sh.getRow(i).getCell(6).value) {
                        var queryJawab = `INSERT INTO webinar_test_options (webinar_test_id, options, answer) VALUES ('${result.insertId}', 'c', '${sh.getRow(i).getCell(6).value}')`;
                        console.log(queryJawab)
                        cId = await db.query(queryJawab)
                    }

                    // D
                    if (sh.getRow(i).getCell(7).value) {
                        var queryJawab = `INSERT INTO webinar_test_options (webinar_test_id, options, answer) VALUES ('${result.insertId}', 'd', '${sh.getRow(i).getCell(7).value}')`;
                        console.log(queryJawab)
                        dId = await db.query(queryJawab)
                    }

                    // E
                    if (sh.getRow(i).getCell(8).value) {
                        var queryJawab = `INSERT INTO webinar_test_options (webinar_test_id, options, answer) VALUES ('${result.insertId}', 'e', '${sh.getRow(i).getCell(8).value}')`;
                        console.log(queryJawab)
                        eId = await db.query(queryJawab)
                    }
                    console.log('==========================================================')

                    tempArray.push({
                        webinarId: webinarId,
                        kuesionerId: result.insertId,
                        pertanyaan: sh.getRow(i).getCell(2).value,
                        jawaban: sh.getRow(i).getCell(3).value,
                        a: sh.getRow(i).getCell(4).value,
                        aId: sh.getRow(i).getCell(4).value ? aId.insertId : null,

                        b: sh.getRow(i).getCell(5).value,
                        bId: sh.getRow(i).getCell(5).value ? bId.insertId : null,

                        c: sh.getRow(i).getCell(6).value,
                        cId: sh.getRow(i).getCell(6).value ? cId.insertId : null,

                        d: sh.getRow(i).getCell(7).value,
                        dId: sh.getRow(i).getCell(7).value ? dId.insertId : null,

                        e: sh.getRow(i).getCell(8).value,
                        eId: sh.getRow(i).getCell(8).value ? eId.insertId : null,
                    });
                }

                fs.unlinkSync(filePath);

                res.json({ error: false, result: tempArray });
            });

        }
    })
}

exports.deleteByIdPertanyaan = (req, res) => {
    // delete on table webinar options
    let sql = `UPDATE webinar_test_questions SET status='Inactive' WHERE id = ?`;
    db.query(sql, [req.params.id], (error, rows) => {
        if (error) res.json({ error: true, result: error })

        res.json({ error: false, result: rows })
    })
}

exports.setScoreEssay = async (req, res) => {
    if (req.body.score < 0 || req.body.score > 100) {
        res.json({ error: true, result: 'Value must be between 0 - 100' });
    } else {
        let editScore = await db.query(`UPDATE webinar_essay_answer SET score=? where user_id=? and webinar_id=? and id=?;`, [req.body.score, req.body.user_id, req.body.webinar_id, req.body.id]);
        if (editScore.affectedRows == 1) {
            res.json({ error: false, result: req.body });
        } else {
            res.json({ error: true, result: 'Value must be between 0 - 100' });
        }
    }
}

exports.resultByUser = (req, res) => {
    let { jenis, id } = req.params
    if (jenis == 'essay') {
        let sql = `SELECT
                webinar_essay_answer.*, If(webinar_tamu.name is not null, webinar_tamu.name, user.name) as name
            FROM 
                webinar_essay_answer
                JOIN webinars ON webinars.id = webinar_essay_answer.webinar_id
                LEFT JOIN user ON user.user_id = webinar_essay_answer.user_id
                LEFT JOIN webinar_tamu ON webinar_tamu.voucher = webinar_essay_answer.user_id
            WHERE 
                webinar_essay_answer.webinar_id = ?`
        db.query(sql, [id], (error, result) => {
            if (error) {
                res.json({ error: true, result: error.message });
            } else {
                res.json({ error: false, result: result });
            }
        })
    } else {
        db.query(`SELECT 
        b.id, b.pertanyaan, c.options, c.answer, d.options AS jawaban, b.jawab AS jawaban_benar
        FROM webinar_test_answer a
        INNER JOIN webinar_test_options d
        ON a.webinar_options = d.id
        INNER JOIN webinar_test_questions b
        ON a.webinar_questions = b.id
        INNER JOIN webinar_test_options c
        ON b.id = c.webinar_test_id
        WHERE b.webinar_id = ?
        AND b.jenis = ?
        AND a.user_id = ?;`, [req.params.id, req.params.jenis, req.params.user], (error, result) => {
            if (error) {
                res.json({ error: true, result: error.message });
            } else {
                let hasil = result.reduce((accumulator, current) => {
                    if (!accumulator[current.id]) {
                        accumulator[current.id] = {};
                        accumulator[current.id]['pertanyaan'] = current.pertanyaan;
                        accumulator[current.id]['jawaban'] = current.jawaban;
                        accumulator[current.id]['jawaban_benar'] = current.jawaban_benar;
                        accumulator[current.id]['options'] = [];
                    }

                    accumulator[current.id]['options'].push({ options: current.options, answer: current.answer });

                    return accumulator;
                }, {});

                let benar = 0;
                let salah = 0;
                let arr = [];
                Object.keys(hasil).map(e => {
                    if (hasil[e].jawaban === hasil[e].jawaban_benar) {
                        benar++;
                    } else {
                        salah++;
                    }

                    arr.push(hasil[e]);
                });

                res.json({ error: false, result: { list: arr, benar: benar, salah: salah, nilai: ((benar / (benar + salah)) * 100).toFixed(2) } });
            };
        })
    }
}
