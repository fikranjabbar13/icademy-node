const db = require('../configs/database');

exports.create = (req, res) => {
    db.query(
        `INSERT INTO ruangan_mengajar
            (company_id, nama_ruangan, folder, pengajar, created_by, created_at)
        VALUES (?);`,
        [[req.body.company_id, req.body.nama_ruangan, req.body.folder, req.body.pengajar, req.body.created_by, new Date()]],
        (error, result) => {
            console.log(error);
            if(error) {
                return res.json({error: true, result: error.message});
            };

            return res.json({error: false, result});
        }
    );
};

exports.read = (req, res) => {
    db.query(
        `SELECT id, company_id, nama_ruangan, folder, pengajar
        FROM ruangan_mengajar
        WHERE id = ?;`,
        [req.params.id],
        (error, result) => {
            console.log(result)
            if(error) {
                return res.json({error: true, result: error.message});
            };

            return res.json({error: false, result});
        }
    );
};

exports.readByCompany = (req, res) => {
    db.query(
        `SELECT r.id, r.company_id, r.nama_ruangan, r.folder as folder_id, f.name as folder, r.pengajar as pengajar_id, l.nama as pengajar
        FROM ruangan_mengajar r JOIN files_folder f ON f.id = r.folder JOIN learning_guru l ON l.id = r.pengajar
        WHERE r.company_id = ?;`,
        [req.params.id],
        (error, result) => {
            console.log(result)
            if(error) {
                return res.json({error: true, result: error.message});
            };
            
            return res.json({error: false, result});
        }
    );
};

exports.update = (req, res) => {
    db.query(
        `UPDATE ruangan_mengajar
        SET nama_ruangan = ?,
            folder = ?,
            pengajar = ?
        WHERE id = ?;`,
        [req.body.nama_ruangan, req.body.folder, req.body.pengajar, req.params.id],
        (error, result) => {
            if(error) {
                return res.json({error: true, result: error.message});
            };

            return res.json({error: false, result});
        }
    );
};


exports.delete = (req, res) => {
    db.query(
        `DELETE FROM ruangan_mengajar
        WHERE id = ?;`,
        [req.params.id],
        (error, result) => {
            if(error) {
                return res.json({error: true, result: error.message});
            };

            return res.json({error: false, result});
        }
    );
};

exports.deleteByCompany = (req, res) => {
    db.query(
        `DELETE FROM ruangan_mengajar
        WHERE company_id = ?;`,
        [req.params.id],
        (error, result) => {
            if(error) {
                return res.json({error: true, result: error.message});
            };

            return res.json({error: false, result});
        }
    );
};
