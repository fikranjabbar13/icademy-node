var env = require('../env.json');
var conf = require('../configs/config');
var db = require('../configs/database');

exports.createUserCourse = async (req, res, next) => {
	let form = {
		user_id: req.body.user_id,
		course_id: req.body.course_id
	}

	let numRows = await db.query(`SELECT * FROM user_course WHERE user_id = '${form.user_id}' AND course_id = '${form.course_id}'`)
	if(numRows.length === 1) {
		res.json({ error: true, result: 'Kamu sudah mengikuti kursus ini.' })
	} else {
		db.query(`INSERT INTO user_course (id_user_course, user_id, course_id, stat_chapter) VALUES (null, '${form.user_id}','${form.course_id}', '1')`, (error, result, fields) => {
			if(error) {
				res.json({ error: true, result: error })
			} else {
				db.query(`SELECT * FROM user_course WHERE id_user_course = '${result.insertId}'`, (error, result, fields) => {
					
					db.query(`SELECT title FROM course WHERE course_id = '${form.course_id}'`, (error, result, fields) => {
						db.query(`INSERT INTO user_activity (user_id, tipe, description) VALUES ('${form.user_id}', 'kursus', 'Anda mulai mengikuti kursus ${result[0].title}')`)
					})

					res.json({ error: false, result: result[0] })
				})
			}
		})
	}
}

exports.getMyCourse = (req, res, next) => {
	db.query(`SELECT * FROM user_course WHERE user_id = '${req.params.user_id}'`, async (error, result, fields) => {
		if(error) {
			res.json({ error: true, result: error })
		} else {
			if(result.length !== 0) {
				for(let i=0; i<result.length; i++) {
					let course = await db.query(`SELECT c.course_id, c.category_id, ct.category_name, c.type, c.title, c.caption, c.image 
						FROM course c JOIN learning_category ct ON ct.category_id = c.category_id 
						WHERE c.course_id = '${result[i].course_id}'`);
					
					result[i].course = course[0];

					let chapters = await db.query(`SELECT * FROM course_chapter WHERE course_id = '${result[i].course_id}'`);
					
					let total_chapter = await db.query(`
						SELECT COUNT(chapter_id) AS total_chapter
						FROM course_chapter
						WHERE course_id = '${result[i].course_id}'
					`);
					let total_chapter_done = await db.query(`
						SELECT COUNT(user_course_chapter_id) AS total_chapter_done
						FROM user_course_chapter 
						WHERE course_id = '${result[i].course_id}' 
						AND user_id = '${req.params.user_id}'
					`);
					let total_exam = await db.query(`
						SELECT COUNT(exam_id) AS total_exam
						FROM exam
						WHERE course_id = '${result[i].course_id}'
					`);
					let total_exam_done = await db.query(`
						SELECT COUNT(exam_result_id) AS total_exam_done
						FROM exam_result
						WHERE course_id = '${result[i].course_id}'
						AND user_id='${req.params.user_id}'
					`);

					let chapter_done = total_chapter_done[0].total_chapter_done;
					let exam_done = total_exam_done[0].total_exam_done ;
					let chapter = total_chapter[0].total_chapter;
					let exam = total_exam[0].total_exam;
					let proses = Math.round((chapter_done+exam_done)/(chapter+exam)*100);

					
					//result[i].chapters = chapters
					result[i] = Object.assign(result[i],{chapters, proses});
				}

				res.json({ error: false, result: result });
			} else {
				res.json({ error: false, result: result })
			}
		}
	})
}

exports.updateStatChapter = (req, res, next) => {
	db.query(`UPDATE user_course SET stat_chapter = '${req.body.stat_chapter}' WHERE user_id = '${req.params.user_id}' AND course_id = '${req.params.course_id}'`, (error, result, fields) => {
		if(error) {
			res.json({error: true, result: error})
		} else {
			db.query(`SELECT * FROM user_course WHERE user_id = '${req.params.user_id}' AND course_id = '${req.params.course_id}'`, (error, result, fields) => {
				res.json({error: false, result: result[0]})
			})
		}
	})
}

exports.getResultExam = (req, res, next) => {
	db.query(`SELECT * FROM exam_result WHERE user_id = '${req.params.user_id}' AND course_id = '${req.params.course_id}'`, (error, result, fields) => {
		if(error) {
			res.json({error: true, result: error})
		} else {
			res.json({error: false, result: result})
		}
	})
}

exports.cekCourse = async (req, res, next) => {
	let numRows = await db.query(`SELECT * FROM user_course WHERE user_id = '${req.params.user_id}' AND course_id = '${req.params.course_id}'`)	
	if(numRows.length === 0) {
		res.json({ error: false, result: false, response: numRows })
	} else {
		res.json({ error: false, result: true, response: numRows })
	}
}
