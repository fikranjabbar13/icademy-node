var env = require('../env.json');
var conf = require('../configs/config');
var db = require('../configs/database');

var multer = require('multer');
const storage = multer.diskStorage({
  filename: function (req, file, cb) {
    cb(null, file.originalname);
  }
})
const AWS = require('aws-sdk');
const fs = require('fs');
const s3 = new AWS.S3({
  accessKeyId: env.AWS_ACCESS,
  secretAccessKey: env.AWS_SECRET
});

let uploadLogo = multer({ storage: storage }).single('image');
let uploadThumbnail = multer({storage: storage}).single('thumbnail');

exports.createCourse = (req, res, next) => {
  uploadLogo(req, res, (err) => {
    if(!req.file) {
      res.json({ error: true, result: err });
    } else {
      let path = req.file.path;
      let newDate = new Date();
      let keyName = `${env.AWS_BUCKET_ENV}/course`;

      var params = {
        ACL: 'public-read',
        Bucket: env.AWS_BUCKET,
        Body: fs.createReadStream(path),
        Key: `${keyName}/${req.file.originalname}`,
        Metadata: {
          'Content-Type': req.file.mimetype,
          'Content-Disposition': 'inline'
        }
      };

      s3.upload(params, (err, data) => {
        if (err) {
          res.json({ error: true, msg: "Error Upload Image", result: err });
        }
        if (data) {
          fs.unlinkSync(path);

          let formData = {
            category_id: req.body.category_id,
            user_id: req.body.user_id,
            type: req.body.type,
            title: req.body.title,
            caption: req.body.caption,
            body: req.body.body,
            image: data.Location,
            created_at: conf.dateTimeNow(),
            publish: '1'
          };

          db.query(`INSERT INTO course (course_id, category_id, user_id, type, title, caption, body, image, created_at, publish) VALUES (
            null, '${formData.category_id}', '${formData.user_id}', '${formData.type}', '${formData.title}', '${formData.caption}',
            '${formData.body}', '${formData.image}', '${formData.created_at}', '${formData.publish}')`, (error, result, fields) => {
            if(error) {
              res.json({error: true, result: error});
            } else {
              db.query(`SELECT * FROM course WHERE course_id = '${result.insertId}'`, (error, result, fields) => {
                res.json({error: false, result: result[0]});
              })
            }
          });

        }
      });

    }
  });
};

exports.getAllCourse = (req, res, next) => {
  db.query(`SELECT c.*, ct.category_name FROM course c JOIN learning_category ct ON ct.category_id = c.category_id`, (error, result, fields) => {
    if(error) {
      res.json({error: true, result: error});
    } else {
      res.json({error: false, result: result});
    }
  });
};

exports.getAllCourseByCategory = (req, res, next) => {
  db.query(`SELECT c.*, ct.category_name
    FROM course c JOIN learning_category ct ON ct.category_id = c.category_id
    WHERE ct.company_id = '${req.params.company_id}' AND c.category_id = '${req.params.category_id}' AND c.publish = '1' ORDER BY c.course_id DESC`, async (error, result, fields) => {
    if(error) {
      res.json({error: true, result: error});
    } else {
      for(let i=0; i<result.length; i++) {
        let countChapter = await db.query(`SELECT * FROM course_chapter WHERE course_id = '${result[i].course_id}'`);
        result[i].count_chapter = countChapter.length;
      }
      res.json({error: false, result: result});
    }
  });
};

exports.getOneCourse = (req, res, next) => {
  db.query(`SELECT c.*, ct.category_name FROM course c JOIN learning_category ct ON ct.category_id = c.category_id WHERE c.course_id = '${req.params.course_id}'`, (error, result, fields) => {
    if(error) {
      res.json({error: true, result: error});
    } else {
      res.json({error: false, result: (result.length !== 0 ) ? result[0] : [] });
    }
  });
};

exports.updateCourse = (req, res, next) => {
  let formData = {
    category_id: req.body.category_id,
    type: req.body.type,
    title: req.body.title,
    caption: req.body.caption,
    body: req.body.body,
    publish: req.body.publish
  };

  db.query(`UPDATE course SET
    category_id = '${formData.category_id}',
    type = '${formData.type}',
    title = '${formData.title}',
    caption = '${formData.caption}',
    body = '${formData.body}',
    publish = '${formData.publish}'
    WHERE course_id = '${req.params.course_id}'`, (error, result, fields) => {
    if(error) {
      res.json({error: true, result: error});
    } else {
      db.query(`SELECT * FROM course WHERE course_id = '${req.params.course_id}'`, (error, result, fields) => {
        res.json({error: false, result: result[0]});
      })
    }
  })
};

exports.updatePublishCourse = (req, res, next) => {
  let formData = {
    publish: req.body.publish
  };

  db.query(`UPDATE course SET
    publish = '${formData.publish}'
    WHERE course_id = '${req.params.course_id}'`, (error, result, fields) => {
    if(error) {
      res.json({error: true, result: error});
    } else {
      db.query(`SELECT * FROM course WHERE course_id = '${req.params.course_id}'`, (error, result, fields) => {
        res.json({error: false, result: result[0]});
      })
    }
  })
};

exports.updateImageCourse = (req, res, next) => {
  uploadLogo(req, res, (err) => {
    if(!req.file) {
      res.json({error: true, result: err});
    } else {

      let path = req.file.path;
      let newDate = new Date();
      let keyName = `${env.AWS_BUCKET_ENV}/course`;

      var params = {
        ACL: 'public-read',
        Bucket: env.AWS_BUCKET,
        Body: fs.createReadStream(path),
        Key: `${keyName}/${req.file.originalname}`
      };

      s3.upload(params, (err, data) => {
        if (err) {
          res.json({ error: true, msg: "Error Upload Image", result: err });
        }
        if (data) {
          fs.unlinkSync(path);

          let formData = {
            image: data.Location
          };

          db.query(`UPDATE course SET image = '${formData.image}' WHERE course_id = '${req.params.course_id}'`, (error, result, fields) => {
            if(error) {
              res.json({error: true, result: error});
            } else {
              res.json({error: false, result: formData.image});
            }
          })

        }
      });

    }
  })
};

exports.updateThumbnailCourse = (req, res, next) => {
  uploadThumbnail(req, res, (err) => {
    if(!req.file) {
      res.json({error: true, result: err});
    } else {

      let path = req.file.path;
      let newDate = new Date();
      let keyName = `${env.AWS_BUCKET_ENV}/course`;

      var params = {
        ACL: 'public-read',
        Bucket: env.AWS_BUCKET,
        Body: fs.createReadStream(path),
        Key: `${keyName}/${req.file.originalname}`
      };

      s3.upload(params, (err, data) => {
        if (err) {
          res.json({ error: true, msg: "Error Upload Image", result: err });
        }
        if (data) {
          fs.unlinkSync(path);

          let formData = {
            image: data.Location
          };

          db.query(`UPDATE course SET thumbnail = '${formData.image}' WHERE course_id = '${req.params.course_id}'`, (error, result, fields) => {
            if(error) {
              res.json({error: true, result: error});
            } else {
              res.json({error: false, result: formData.image});
            }
          })

        }
      });

    }
  })
};

exports.deleteCourse = (req, res, next) => {
  // db.query(`DELETE FROM course WHERE course_id = '${req.params.course_id}'`, (error, result, fields) => {
  db.query(`UPDATE course SET publish='0' WHERE course_id = '${req.params.course_id}'`, (error, result, fields) => {
    if(error) {
      res.json({error: true, result: error});
    } else {
      res.json({error: false, result: result});
    }
  })
};

exports.getAllCourseByCompany = (req, res, next) => {
  db.query(`SELECT c.*, ct.category_name
    FROM course c JOIN learning_category ct ON ct.category_id = c.category_id
    WHERE ct.company_id = '${req.params.company_id}' AND c.publish = '1' ORDER BY c.course_id DESC`, async (error, result, fields) => {
    if(error) {
      res.json({error: true, result: error});
    } else {
      for(let i=0; i<result.length; i++) {
        let countChapter = await db.query(`SELECT * FROM course_chapter WHERE course_id = '${result[i].course_id}'`);
        result[i].count_chapter = countChapter.length;
      }
      res.json({error: false, result: result});
    }
  });
};

// exports.getCourseByCompany = (req, res, next) => {
//   let sql = `SELECT lc.*, c.*
//     FROM learning_category lc JOIN course c ON c.category_id = lc.category_id
//     WHERE lc.company_id = '${req.params.company_id}'`;
//   db.query(sql, (error, result, fields) => {
//     if(error) {
//       res.json({ error: true, result: error });
//     } else {
//       res.json({error: false, result: result });
//     }
//   })
// };
