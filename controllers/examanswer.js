var conf = require('../configs/config');
var db = require('../configs/database');
var dateFormat = require('dateformat');
var now = new Date();

exports.answerQuestion = async (req, res, next) => {
	let numRow = await db.query(`SELECT * FROM exam_answer WHERE user_id = '${req.body.user_id}' AND question_id = '${req.body.question_id}'`);
	if(numRow.length !== 0) {
		res.json({ error: true, result: 'Anda sudah menjawab pertanyaan ini.'})
	} else {
		db.query(`INSERT INTO exam_answer (question_id, user_id, answer_number, answer_option)
			VALUES ('${req.body.question_id}', '${req.body.user_id}', '${req.body.answer_number}', '${req.body.answer_option}')`,
			(error, result, fields) => {
			if(error) {
				res.json({ error: true, result: error });
			} else {
				db.query(`SELECT * FROM exam_answer WHERE answer_id = '${result.insertId}'`, (error, result, fields) => {
					res.json({ error: false, result: result[0] })
				})
			}
		})
	}
}

exports.updateAnswerQuestion= (req, res, next) => {
	db.query(`UPDATE exam_answer SET answer_number = '${req.body.answer_number}', answer_option = '${req.body.answer_option}' WHERE user_id = '${req.params.user_id}' AND question_id = '${req.params.question_id}'`,
		(error, result, fields) => {
		if(error) {
			res.json({ error: true, result: error })
		} else {
			db.query(`SELECT * FROM exam_answer WHERE answer_id = '${req.params.answer_id}'`, (error, result, fields) => {
				res.json({ error: false, result: result[0] })
			})
		}
	})
}

exports.getOneAnswerQuestion = (req, res, next) => {
	db.query(`SELECT * FROM exam_answer WHERE user_id = '${req.params.user_id}' AND question_id = '${req.params.question_id}'`, (error, result, fields) => {
		if(error) {
			res.json({error: true, result: error})
		} else {
			res.json({error: false, result: result.length !== 0 ? result[0] : [] })
		}
	})
}

exports.getAllAnswerQuestion = (req, res, next) => {
	db.query(`SELECT q.exam_id, q.question_id, q.question, q.number, q.correct_option, a.*
			FROM exam_answer a JOIN exam_question q ON q.question_id = a.question_id
			WHERE q.exam_id = '${req.params.exam_id}' AND a.user_id = '${req.params.user_id}' ORDER BY q.number ASC`, async (error, result, fields) => {
		if(error) {
			res.json({error: true, result: error})
		} else {
			for(let i=0; i<result.length; i++) {
				let options = await db.query(`SELECT * FROM exam_option WHERE question_id = '${result[i].question_id}' ORDER BY exam_option ASC`)
				result[i].options = options
			}
			res.json({error: false, result: result })
		}
	})
}

exports.submitAnswerQuestion = (req, res, next) => {
	db.query(`SELECT q.exam_id, q.question_id, q.question, q.correct_option, a.*
			FROM exam_answer a JOIN exam_question q ON q.question_id = a.question_id
			WHERE q.exam_id = '${req.body.examId}' AND a.user_id = '${req.body.userId}' ORDER BY answer_number DESC`,
			async (error, result, fields) => {
		if(error) {
			res.json({error: true, result: error})
		} else {
			// check result is 0
			if(result.length != 0) {
				let correctOption = 0;
				for(let i=0; i<result.length; i++) {
					if(result[i].correct_option === result[i].answer_option) { correctOption++; }
				}
				form.benar = correctOption;
				form.salah = result.length - correctOption;
				form.score = (correctOption / result.length) * 100

				res.json({ error: false, result: form })
			} else {
				form.benar = 0;
				form.salah = 0;
				form.score = 0;
				
				res.json({error: false, result: form })
			}

		}
	})
}

exports.getStartExamQuestion = async (req, res, next) => {
	let numRows = await db.query(`SELECT * FROM exam_result WHERE user_id = '${req.params.user_id}' AND exam_id = '${req.params.exam_id}'`)
	if(numRows.length !== 0) {
		// update
		res.json({error: true, result: 'Anda sudah mendapatkan nilai dari ujian ini.'})
	} else {
		// insert
		let getCourseId = await db.query(`SELECT course_id FROM exam WHERE exam_id = '${req.params.exam_id}' LIMIT 1`)
		let dateNow = dateFormat(now, "yyyy-mm-dd HH:MM:ss");
		db.query(`INSERT INTO exam_result (user_id, exam_id, course_id, time_start) VALUES
			('${req.params.user_id}','${req.params.exam_id}','${getCourseId[0].course_id}','${dateNow}')`, (error, result, fields) => {
				if(error) {
					res.json({ error: true, result: error })
				} else {
					db.query(`SELECT * FROM exam_result WHERE exam_result_id = '${result.insertId}'`, (error, result, fields) => {
						res.json({ error: false, result: result[0] })
					})
				}
			})
	}
}
