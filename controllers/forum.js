var env = require('../env.json');
var conf = require('../configs/config');
var db = require('../configs/database');

var multer = require('multer');
var storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, './public/category');
  },
  filename: (req, file, cb) => {
    var filetype = '';
    if(file.mimetype === 'image/gif') {
      filetype = 'gif';
    }
    if(file.mimetype === 'image/png') {
      filetype = 'png';
    }
    if(file.mimetype === 'image/jpeg') {
      filetype = 'jpg';
    }
    cb(null, 'forum-' + Date.now() + '.' + filetype);
  }
});
var uploadCover = multer({storage: storage}).single('cover');

exports.createForum = (req, res, next) => {
  let form = {
    companyId: req.body.company_id,
    userId: req.body.user_id,
    title: req.body.title,
    body: req.body.body,
    tags: req.body.tags
  }

  db.query(`INSERT INTO forum (company_id, user_id, title, body, tags, publish) VALUES ('${form.companyId}', '${form.userId}', '${form.title}', '${form.body}', '${form.tags}', '1')`, (error, result, fields) => {
    if(error) {
      res.json({ error: true, result: error })
    } else {
      db.query(`SELECT * FROM forum WHERE forum_id = '${result.insertId}'`, (error, result, fields) => {
        res.json({error: false, result: result[0]})
      })
    }
  })
}

exports.addUserForum = (req, res, next) => {
  let form = {
    forumId: req.body.forum_id,
    userId: req.body.user_id
  }
  db.query(`DELETE FROM user_forum WHERE forum_id = '${form.forumId}' AND user_id='${form.userId}'`)
  db.query(`INSERT INTO user_forum (forum_id, user_id ) VALUES ('${form.forumId}', '${form.userId}' )`, (error, result, fields) => {
    if(error) {
      res.json({ error: true, result: error })
    } else {
      db.query(`SELECT f.*, u.forum_status AS bookmark
                FROM forum f
                LEFT JOIN user_forum u ON f.forum_id = u.forum_id
                WHERE f.forum_id = '${form.forumId}'
                AND (u.user_id IS NULL OR u.user_id='${form.userId}')
                ORDER BY f.forum_id DESC`, (error, result, fields) => {
        res.json({error: false, result: result})
      })
    }
  })
}

exports.getAllForum = (req, res, next) => {
  db.query(`SELECT * FROM forum`, (error, result, fields) => {
    if(error) {
      res.json({error: true, result: error})
    } else {
      res.json({error: false, result: result})
    }
  })
}

exports.getUserForum = (req, res, next) => {
  db.query(`
  SELECT f.*, u.id
  FROM 
    forum f, user_forum u 
  WHERE f.forum_id = u.forum_id 
  AND u.forum_status = 1
  AND u.user_id = ${req.params.user_id}`, (error, result, fields) => {
    if(error) {
      res.json({error: true, result: error})
    } else {
      res.json({error: false, result: result})
    }
  })
}

exports.getForumById = (req, res, next) => {
  db.query(`SELECT f.*, u.forum_status AS bookmark
            FROM forum f
            LEFT JOIN user_forum u ON f.forum_id = u.forum_id
            WHERE f.forum_id = '${req.params.forum_id}'
            AND (u.user_id IS NULL OR u.user_id='${req.params.userId}')
            ORDER BY f.forum_id DESC`, async (error, result, fields) => {
    if(error) {
      res.json({error: true, result: error})
    } else {
      if(result.length !== 0) {
        for(let i=0; i<result.length; i++) {
          let komentar = await db.query(`SELECT u.name, u.avatar, p.* FROM forum_post p JOIN user u ON u.user_id = p.user_id WHERE p.forum_id = '${result[i].forum_id}' ORDER BY post_id ASC`)
          result[i].komentar = komentar
        }
      }
      res.json({error: false, result: result})
    }
  })
}

exports.getForumByUser = (req, res, next) => {
  db.query(`SELECT * FROM forum WHERE user_id = '${req.params.user_id}' ORDER BY forum_id DESC`, (error, result, fields) => {
    if(error) {
      res.json({error: true, result: error})
    } else {
      res.json({error: false, result: result})
    }
  })
}

exports.getForumByCompany = (req, res, next) => {
  db.query(`SELECT f.*, u.forum_status AS bookmark
            FROM forum f
            LEFT JOIN user_forum u ON f.forum_id = u.forum_id
            WHERE f.company_id = '${req.params.company_id}'
            AND (u.user_id IS NULL OR u.user_id = '${req.params.userId}')
            AND publish = '1' ORDER BY forum_id DESC`, async (error, result, fields) => {
    if(error) {
      res.json({error: true, result: error})
    } else {
      if(result.length != 0) {
        for(let i=0; i<result.length; i++) {
          let countKomen = await db.query(`SELECT forum_id FROM forum_post WHERE forum_id = '${result[i].forum_id}'`)
          result[i].komentar = countKomen.length
        }
      }
      res.json({error: false, result: result})
    }
  })
}

exports.updateForum = (req, res, next) => {
  let form = {
    title: req.body.title,
    body: req.body.body,
    tags: req.body.tags
  }

  db.query(`UPDATE forum SET title = '${form.title}', body = '${form.body}', tags = '${form.tags}' WHERE forum_id = '${req.params.forum_id}'`, (error, result, fields) => {
    if(error) {
      res.json({error: true, result: error})
    } else {
      db.query(`SELECT * FROM forum WHERE forum_id = '${req.params.forum_id}'`, (error, result, fields) => {
        res.json({error: false, result: result[0]})
      })
    }
  })
}

exports.updateForumCover = (req, res, next) => {
  uploadCover(req, res, (err) => {
    if(!req.file) {
      res.json({ error: true, result: err });
    } else {
      let form = {
        cover: `${env.APP_URL}/category/${req.file.filename}`
      }

      db.query(`UPDATE forum SET cover = '${form.cover}' WHERE forum_id = '${req.params.forum_id}'`, (error, result, fields) => {
        if(error) {
          res.json({error: true, result: error})
        } else {
          res.json({error: false, result: form.cover})
        }
      })
    }
  })
}

exports.updateKunciForum = (req, res, next) => {
  let form = {
    kunci: req.body.kunci
  }

  db.query(`UPDATE forum SET kunci = '${form.kunci}' WHERE forum_id = '${req.params.forum_id}'`, (error, result, fields) => {
    if(error) {
      res.json({error: true, result: error})
    } else {
      db.query(`SELECT * FROM forum WHERE forum_id = '${req.params.forum_id}'`, (error, result, fields) => {
        res.json({error: false, result: result[0]})
      })
    }
  })
}

exports.deleteUserForum = (req, res, next) => {
  db.query(`DELETE FROM user_forum WHERE forum_id = '${req.params.forum_id}' AND user_id = '${req.params.user_id}'`, (error, result, fields) => {
    if(error) {
      res.json({error: true, result: error})
    } else {
      db.query(`SELECT f.*, u.forum_status AS bookmark
                FROM forum f
                LEFT JOIN user_forum u ON f.forum_id = u.forum_id
                WHERE f.forum_id = '${req.params.forum_id}'
                AND (u.user_id IS NULL OR u.user_id='${req.params.user_id}')
                ORDER BY f.forum_id DESC`, (error, result, fields) => {
        res.json({error: false, result: result})
      })
    }
  })
}

exports.deleteForum = (req, res, next) => {
  db.query(`DELETE FROM forum WHERE forum_id = '${req.params.forum_id}'`, (error, result, fields) => {
    if(error) {
      res.json({error: true, result: error})
    } else {
      res.json({error: false, result: result})
    }
  })
}
