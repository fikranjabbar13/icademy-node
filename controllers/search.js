const db = require("../configs/database");

exports.search = async (req, res) => {
    let level = req.body.level;
    let userId = req.body.userId;
    let companyId = req.body.companyId;
    let text = req.params.text;
  
    // search meeting
    let queryUser = `SELECT
        l.*,
        u.name,
        GROUP_CONCAT(p.user_id) AS participant,
        COUNT(lp.user_id) AS total_participant,
        LEFT(l.schedule_start, 10) AS tanggal,
        MID(l.schedule_start, 12, 8) AS waktu_start,
      MID(l.schedule_end,12, 8) AS waktu_end
      FROM
        liveclass l
      LEFT JOIN user u ON (l.moderator IS NOT NULL AND l.moderator = u.user_id)
      LEFT JOIN liveclass_participant p ON l.class_id = p.class_id
      LEFT JOIN liveclass_participant lp ON lp.class_id = l.class_id
      WHERE l.company_id='${companyId}' AND ((l.is_private='1' AND p.user_id='${userId}') OR l.is_private='0') AND l.room_name LIKE '%${text}%'
      GROUP BY l.class_id
      ORDER BY l.schedule_start DESC`;
  
    let queryAdmin = `SELECT
        l.*,
        u.name,
        GROUP_CONCAT(p.user_id) AS participant,
        COUNT(p.user_id) AS total_participant,
        LEFT(l.schedule_start, 10) AS tanggal,
        MID(l.schedule_start, 12, 8) AS waktu_start,
      MID(l.schedule_end,12, 8) AS waktu_end
      FROM
        liveclass l
      LEFT JOIN user u ON (l.moderator IS NOT NULL AND l.moderator = u.user_id)
      LEFT JOIN liveclass_participant p ON l.class_id = p.class_id
      WHERE l.company_id = '${companyId}' AND l.room_name LIKE '%${text}%'
      GROUP BY l.class_id
      ORDER BY l.schedule_start DESC`
    let meeting = await db.query(level == 'client' ? queryUser : queryAdmin);

    // search webinar
    let webinar = await db.query(`SELECT * 
			FROM webinars 
			WHERE company_id = '${companyId}' AND publish='1' AND judul LIKE '%${text}%'
			ORDER BY id DESC`);
		if(webinar.length == 0) {
		} else {
			for(var i=0; i<webinar.length; i++) {
				let moderator = await db.query(`SELECT user_id, name FROM user WHERE user_id IN (${webinar[i].moderator})`);
				webinar[i].moderator = moderator.length != 0 ? moderator : {user_id: webinar[i].moderator, name: "Anonymous"};
				
				let sekretaris = await db.query(`SELECT user_id, name FROM user WHERE user_id IN (${webinar[i].sekretaris})`);
				webinar[i].sekretaris = sekretaris.length != 0 ? sekretaris : {user_id: webinar[i].sekretaris, name: "Anonymous"};

				let pembicara = await db.query(`SELECT user_id, name FROM user WHERE user_id IN (${webinar[i].pembicara})`);
				webinar[i].pembicara = pembicara.length != 0 ? pembicara : {user_id: webinar[i].pembicara, name: "Anonymous"};
				
				let owner = await db.query(`SELECT user_id, name FROM user WHERE user_id IN (${webinar[i].owner})`);
				webinar[i].owner = owner.length != 0 ? owner : {user_id: webinar[0].owner, name: "Anonymous"};
				
				let peserta = await db.query(`SELECT w.*, u.name FROM webinar_peserta w JOIN user u ON u.user_id = w.user_id WHERE webinar_id = '${webinar[i].id}'`)
				webinar[i].peserta = peserta;
				
				let tamu = await db.query(`SELECT * FROM webinar_tamu WHERE webinar_id='${webinar[i].id}'`);
				webinar[i].tamu = tamu;
			}
		}

    // search project
    
	let queryUserProject = `SELECT p.id, p.name AS title,
	(SELECT COUNT(l.class_id) FROM liveclass l LEFT JOIN liveclass_participant lp ON lp.class_id=l.class_id WHERE l.folder_id=p.id AND ((l.is_private='1' AND lp.user_id='${userId}') OR l.is_private='0')) AS meeting,
	(SELECT COUNT(w.id) FROM webinars w WHERE w.project_id=p.id AND w.publish=1) AS webinar,
	IF(p.company_id!='${companyId}', c.company_name, null) AS share_from
	FROM files_folder p
	LEFT JOIN files_folder_user pu ON pu.folder_id=p.id
	LEFT JOIN company c ON c.company_id=p.company_id
	LEFT JOIN project_share s ON s.project_id=p.id
	WHERE (p.company_id='${companyId}' OR s.user_id='${companyId}') AND p.publish='1' AND p.mother='0' AND ((p.is_limit=1 AND (pu.user_id='${userId}' AND pu.role='Project Admin') OR (pu.user_id='${userId}' AND pu.role='User')) OR p.is_limit=0) AND p.name LIKE '%${text}%'
	GROUP BY p.id
	ORDER BY p.name ASC`;

	let queryAdminProject = `SELECT p.id, p.name AS title, count(DISTINCT l.class_id) AS meeting, count(DISTINCT w.id) AS webinar, IF(p.company_id!='${companyId}', c.company_name, null) AS share_from
	FROM files_folder p
	LEFT JOIN liveclass l ON p.id=l.folder_id
	LEFT JOIN (SELECT * FROM webinars w WHERE w.publish=1) AS w ON w.project_id=p.id
	LEFT JOIN project_share s ON s.project_id=p.id
	LEFT JOIN company c ON c.company_id=p.company_id
	WHERE (p.company_id='${companyId}' OR s.user_id='${userId}') AND p.mother='0' AND p.publish='1' AND p.name LIKE '%${text}%'
	GROUP BY p.id
	ORDER BY p.name ASC`;

	let project = await db.query(level == 'client' ? queryUserProject : queryAdminProject);
      for (let i=0; i<project.length; i++){
        let related_user = await db.query(`SELECT DISTINCT u.name, u.avatar
        FROM gantt_tasks t
        JOIN gantt_user tu ON t.id = tu.task_id
        JOIN user u ON tu.user_id = u.user_id
        WHERE t.project_id='${project[i].id}'`)
        project[i].related_user = related_user.length != 0 ? related_user : null;

        let progress = await db.query(`SELECT
        ROUND((SELECT SUM(progress) FROM gantt_tasks WHERE project_id='${project[i].id}' AND visibility!='Private' AND type!='project') /
        (SELECT COUNT(id) FROM gantt_tasks WHERE project_id='${project[i].id}' AND visibility!='Private' AND type!='project') * 100, 2)
        AS progress`)
        project[i].progress = progress[0].progress;
      }
    // search task
    let task = await db.query(`SELECT t.*, GROUP_CONCAT(u.user_id) AS owner_id, (SELECT time FROM gantt_logs l WHERE l.task_id=t.id AND l.description LIKE '%to Done%' ORDER BY l.time DESC LIMIT 1) AS done_time FROM gantt_tasks t LEFT JOIN gantt_user u ON u.task_id=t.id WHERE t.text LIKE '%${text}%' AND u.user_id IN (${userId}) AND ((t.visibility IN ('Private','') OR t.visibility IS NULL) OR (t.visibility='Private' AND u.user_id='${userId}')) GROUP BY t.id ORDER BY t.start_date ASC`);

    let result = {
      meeting: meeting,
      webinar: webinar,
      project: project,
      task: task
    }
    res.json({error: false, result: result})
  };