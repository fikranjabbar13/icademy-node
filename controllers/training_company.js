var env = require('../env.json');
const db = require('../configs/database');
var multer = require('multer');
const AWS = require('aws-sdk');
const fs = require('fs');
const { format } = require('../configs/database');
const moduleDB = require('../repository');
var moment = require('moment');
const util = require('../utils/user_registration');
const s3 = new AWS.S3({
  accessKeyId: env.AWS_ACCESS,
  secretAccessKey: env.AWS_SECRET
});
const storage = multer.diskStorage({
	destination: (req, file, cb) => {
    cb(null, './public/training/company');
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname);
  }
})
let uploadImage = multer({ storage: storage }).single('image');

var storageEx = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, './public/training/company/excel');
  },
  filename: (req, file, cb) => {
    cb(null, 'excel-' + Date.now() + '-' + file.originalname);
  }
});
var uploadExcel = multer({storage: storageEx}).single('file');

exports.import = (req, res) => {
	uploadExcel(req, res, (err) => {
		if(!req.file) {
      res.json({ error: true, result: err });
    } else {
    	// create class Excel
    	var Excel = require('exceljs');
			var wb = new Excel.Workbook();
			var path = require('path');
			var filePath = path.resolve(__dirname, '../public/training/company/excel/'+req.file.filename);

			var form = {
				company_id: req.body.company_id,
				status: 'active',
				created_by: req.body.created_by,
                created_at: moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
			};

			wb.xlsx.readFile(filePath).then( async function(){
		    var sh = wb.getWorksheet("Sheet1");
		    console.log('countRow: ', sh.rowCount);

		    var tempArray = [];
		    for (let i = 4; i <= sh.rowCount; i++) {
                if (sh.getRow(i).getCell(1).value !== null){
                    // check company exists
                    let checkCompany = await db.query(`SELECT name FROM training_company WHERE name = '${sh.getRow(i).getCell(1).value}' AND company_id='${form.company_id}' AND status='active'`);
    
                    if(checkCompany.length != 1) {
                            let sqlString = `INSERT INTO training_company (company_id, name, address, telephone, fax, website, email, status, created_by, created_at)
                                VALUES ('${form.company_id}', '${sh.getRow(i).getCell(1).value}', '${sh.getRow(i).getCell(2).value}', '${sh.getRow(i).getCell(3).value}',
                                '${sh.getRow(i).getCell(4).value}', '${sh.getRow(i).getCell(5).value}', '${sh.getRow(i).getCell(6).value}',
                                '${form.status}', '${form.created_by}', '${form.created_at}')`;
    
                            let insertTrainingCompany = await db.query(sqlString);
                    }
                }
                else{
                    break;
                }
		    }

		    res.json({ error: false, result: 'success' });
		  });

    }
  })
}
exports.uploadImage = (req, res) => {
        uploadImage(req, res, (err) => {
          if(!req.file) {
            res.json({ error: true, result: err });
          } else {
                  if(env.AWS_BUCKET) {
                    let path = req.file.path;
                    let newDate = new Date();
                    let keyName = `${env.AWS_BUCKET_ENV}/training/company`;
        
                    var params = {
                    ACL: 'public-read',
                    Bucket: env.AWS_BUCKET,
                    Body: fs.createReadStream(path),
                    Key: `${keyName}/${Date.now()}-${req.file.originalname}`,
                    ContentDisposition: 'inline',
                    ContentType: req.file.mimetype
                    };
        
                    s3.upload(params, (err, data) => {
                    if (err) {
                        res.json({ error: true, msg: "Error Upload Image", result: err });
                    }
                    if (data) {
                        fs.unlinkSync(path);
        
                        let form = {
                            image: data.Location
                        }
        
                        db.query(`UPDATE training_company SET image = '${form.image}' WHERE id = '${req.params.id}'`, (error, result, fields) => {
                        if(error) {
                            res.json({error: true, result: error})
                        } else {
                            res.json({error: false, result: result})
                        }
                        })
        
                    }
                    })
                  }
                  else {
                      let form = {
                          image: `${env.APP_URL}/training/company/${req.file.filename}`
                      }
      
                      db.query(`UPDATE training_company SET image = '${form.image}' WHERE id = '${req.params.id}'`, (error, result, fields) => {
                          if(error) {
                              res.json({error: true, result: error})
                          } else {
                              res.json({error: false, result: result})
                          }
                      })
                  }
      
          }
        })
}
exports.create = (req, res) => {
    db.query(
        `INSERT INTO training_company
        (company_id, name, address, telephone, fax, website, email, status, created_by, created_at)
        VALUES (?);`,
        [[req.body.company_id, req.body.name, req.body.address, req.body.telephone, req.body.fax, req.body.website, req.body.email, 'active', req.body.created_by, new Date()]],
        (error, result) => {
            if(error) {
                return res.json({error: true, result: error.message});
            }
            else{
                return res.json({error: false, result: result})
            }
        }
    );
};

exports.read = (req, res) => {
    db.query(
        `SELECT *
        FROM training_company
        WHERE id = ?
        LIMIT 1;`,
        [req.params.id],
        (error, result) => {
            if(error) {
                return res.json({error: true, result: error.message});
            };
            return res.json({error: false, result: result[0]});
        }
    );
};

exports.browseByCompany = (req, res) => {
    db.query(
        `SELECT *
        FROM training_company
        WHERE company_id=? AND status='active'
        ORDER BY name ASC;`,
        [req.params.id],
        (error, result) => {
            if(error) {
                return res.json({error: true, result: error.message});
            };
            return res.json({error: false, result: result});
        }
    );
};

exports.getListCompanyByIdCourse = async (req, res) => {
    try {
        const retrieveListCompanyByIdCourse = await moduleDB.getListCompanyByIdCourse({idCompany: req.params.idCompany, idCourse: req.params.idCourse});
        res.json({error: false, result: retrieveListCompanyByIdCourse});
    } catch (error) {
        res.json({error: true, result: error.message});
    }
}

exports.browseByCompanyArchived = (req, res) => {
    db.query(
        `SELECT *
        FROM training_company
        WHERE company_id=? AND status='inactive'
        ORDER BY name ASC;`,
        [req.params.id],
        (error, result) => {
            if(error) {
                return res.json({error: true, result: error.message});
            };
            return res.json({error: false, result: result});
        }
    );
};

exports.update = (req, res) => {
    db.query(
        `UPDATE training_company
        SET name = ?,
            address = ?,
            telephone = ?,
            fax = ?,
            website = ?,
            email = ?
        WHERE id = ?;`,
        [req.body.name, req.body.address, req.body.telephone, req.body.fax, req.body.website, req.body.email, req.params.id],
        (error, result) => {
            if(error) {
                return res.json({error: true, result: error.message});
            }
            else{
                return res.json({error: false, result: result})
            }
        }
    );
};

exports.updateEnableRegist = (req, res) => {
    db.query(
        `UPDATE training_company
        SET enable_registration = ?
        WHERE id = ?;`,
        [req.body.enable_registration, req.params.id],
        (error, result) => {
            if(error) {
                return res.json({error: true, result: error.message});
            }
            else{
                return res.json({error: false, result: result})
            }
        }
    );
};

exports.updateEnablePaidRegist = (req, res) => {
    db.query(
        `UPDATE training_registration_schema
        SET is_paid = ?
        WHERE id = ?;`,
        [req.body.is_paid.toString(), req.params.id],
        (error, result) => {
            if(error) {
                return res.json({error: true, result: error.message});
            }
            else{
                return res.json({error: false, result: result})
            }
        }
    );
};


exports.delete = (req, res) => {
    db.query(
        `UPDATE training_company
        SET status='inactive'
        WHERE id = ?;`,
        [req.params.id],
        (error, result) => {
            if(error) {
                return res.json({error: true, result: error.message});
            };

            db.query(
                `UPDATE user
                INNER JOIN training_user ON user.email = training_user.email
                SET user.status = 'pasive'
                WHERE training_user.training_company_id = ? AND training_user.status='Active';`,
                [req.params.id],
                (error, result) => {
                    if(error) {
                        return res.json({error: true, result: error.message});
                    };
        
                    db.query(
                        `UPDATE training_user
                        SET status='Inactive_Company'
                        WHERE training_company_id = ? AND status='Active';`,
                        [req.params.id],
                        (error, result) => {
                            if(error) {
                                return res.json({error: true, result: error.message});
                            };
                            return res.json({error: false, result});
                        }
                    );
                }
            );
        }
    );
};

exports.activate = (req, res) => {
    db.query(
        `UPDATE training_company
        SET status='active'
        WHERE id = ?;`,
        [req.params.id],
        (error, result) => {
            if(error) {
                return res.json({error: true, result: error.message});
            };

            db.query(
                `UPDATE user
                INNER JOIN training_user ON user.email = training_user.email
                SET user.status = 'active'
                WHERE training_user.training_company_id = ? AND training_user.status='Inactive_Company';`,
                [req.params.id],
                (error, result) => {
                    if(error) {
                        return res.json({error: true, result: error.message});
                    };
        
                    db.query(
                        `UPDATE training_user
                        SET status='Active'
                        WHERE training_company_id = ? AND status='Inactive_Company';`,
                        [req.params.id],
                        (error, result) => {
                            if(error) {
                                return res.json({error: true, result: error.message});
                            };
                            return res.json({error: false, result});
                        }
                    );
                }
            );
        }
    );
};

exports.updateEnableRegist = (req, res) => {
    db.query(
        `UPDATE training_company
        SET enable_registration = ?
        WHERE id = ?;`,
        [req.body.enable_registration, req.params.id],
        (error, result) => {
            if(error) {
                return res.json({error: true, result: error.message});
            }
            else{
                return res.json({error: false, result: result})
            }
        }
    );
};

exports.getFormRegisterCompany = async (req, res)=>{
    try {
        let retrieveDetailRegistrationSchema = await moduleDB.getTrainingRegistrationSchemaByIdTrainingCompany({idTrainingCompany: req.params.id});
        if(req.query.mode === 'public'){
            if(retrieveDetailRegistrationSchema.length) {
                retrieveDetailRegistrationSchema = await util.FuncCheckRegistrationSchema(retrieveDetailRegistrationSchema);
            }
        };

        res.json({error: false, result: retrieveDetailRegistrationSchema});
    } catch (error) {
        res.json({error: true, result: error.message});
    }
}

exports.postFormRegisterCompany = async (req, res)=>{
    try {        
        if(req.body.isPaid){
            req.body.isPaid = 1;
        }else{
            req.body.isPaid = 0;
        }
        let get = await db.query(
            `select *  
            from training_registration_schema  
            WHERE training_company_id = ?;`,
            [req.params.id]);
        
        let updateSql =`
        UPDATE training_registration_schema SET title=?, subtitle=?, is_paid=?, price=?, close_registration=?, slot=? ,json_data=?, created_by=?, media=? WHERE training_company_id=?; 
        `;
        
        let insertSql =`
        INSERT INTO training_registration_schema (title, subtitle, is_paid, price, close_registration, slot, training_company_id, json_data, created_by,media) VALUE(?); 
        `;
        let store = null; 
        let json_data = req.body.json_data.length ? JSON.stringify(req.body.json_data) : null;
        let media_data = req.body.media.length ? JSON.stringify(req.body.media) : null;

        if(get.length){
            store = await db.query(updateSql,[req.body.title, req.body.subtitle, req.body.isPaid.toString(), req.body.price, req.body.closeRegistration, req.body.slot ,json_data, req.app.token.user_id, media_data, req.params.id]);
        }else{
            store = await db.query(insertSql,[[req.body.title, req.body.subtitle, req.body.isPaid.toString(), req.body.price, req.body.closeRegistration, req.body.slot ,req.body.training_company_id, json_data, req.app.token.user_id, media_data]]);
        }

        res.json({ error:false, result:'Success' });
    } catch (error) {
        console.info(error);
        res.json({ error:true, result:'Failed' });
    }
}