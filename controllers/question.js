var env = require('../env.json');
var conf = require('../configs/config');
var db = require('../configs/database');

var multer = require('multer');
var storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, './public/company');
  },
  filename: (req, file, cb) => {
    cb(null, 'excel-' + Date.now() + '-' + file.originalname);
  }
});
var uploadExcel = multer({storage: storage}).single('excel');

exports.createQuestion = (req, res, next) => {
	let form = {
		exam_id: req.body.exam_id,
		tag: req.body.tag,
		number: req.body.number,
		question: req.body.question,
		correct_option: req.body.correct_option
	}

	db.query(`INSERT INTO exam_question
		(question_id, exam_id, tag, number, question, correct_option) VALUES
		(null, '${form.exam_id}', '${form.tag}', '${form.number}', '${form.question}', '${form.correct_option}')`, (error, result, fields) => {
		if(error) {
			res.json({error: true, result: error})
		} else {
			db.query(`SELECT * FROM exam_question WHERE question_id = '${result.insertId}'`, (error, result, fields) => {
				res.json({error: false, result: result[0]})
			})
		}
	})
}

exports.getAllQuestion = (req, res, next) => {
	db.query(`SELECT * FROM exam_question`, (error, result, fields) => {
		if(error) {
			res.json({error: true, result: error})
		} else {
			res.json({error: false, result: result})
		}
	})
}

exports.getAllQuestionByExam = (req, res, next) => {
	db.query(`SELECT * FROM exam_question WHERE exam_id = '${req.params.exam_id}'`, async (error, result, fields) => {
		if(error) {
			res.json({error: true, result: error})
		} else {
			let tempArray = result;
			for(let i=0; i<tempArray.length; i++) {
				let options = await db.query(`SELECT * FROM exam_option WHERE question_id = '${tempArray[i].question_id}' ORDER BY exam_option ASC`);
				tempArray[i].options = options;
			}

			res.json({error: false, result: tempArray})
		}
	})
}

exports.getOneQuestion = (req, res, next) => {
	db.query(`SELECT * FROM exam_question WHERE question_id = '${req.params.question_id}'`, async (error, result, fields) => {
		if(error) {
			res.json({error: true, result: error})
		} else {
			if(result.length !== 0) {
				let numRows = await db.query(`SELECT * FROM exam_option WHERE question_id = '${result.insertId}'`)
				result[0].options = numRows
			}
			res.json({error: false, result: result.length !== 0 ? result[0] : []})
		}
	})
}

exports.updateQuestion = (req, res, next) => {
	let form = {
		tag: req.body.tag,
		number: req.body.number,
		question: req.body.question,
		correct_option: req.body.correct_option
	}

	db.query(`UPDATE exam_question SET
		tag = '${form.tag}',
		number = '${form.number}',
		question = '${form.question}',
		correct_option = '${form.correct_option}'
		WHERE question_id = '${req.params.question_id}'`, (error, result, fields) => {
		if(error) {
			res.json({error: true, result: error})
		} else {
			db.query(`SELECT * FROM exam_question WHERE question_id = '${req.params.question_id}'`, (error, result, fields) => {
				res.json({error: false, result: result[0]})
			})
		}
	})
}

exports.deleteQuestion = (req, res, next) => {
	db.query(`DELETE FROM exam_question WHERE question_id = '${req.params.question_id}'`, (error, result, fields) => {
		if(error) {
			res.json({error: true, result: error})
		} else {
			res.json({error: false, result: result})
		}
	})
}


exports.deleteBulkQuestion = (req, res, next) => {
	db.query(`DELETE FROM exam_question WHERE question_id IN (${req.params.question_id})`, (error, result, fields) => {
		if(error) {
			res.json({error: true, result: error})
		} else {
			res.json({error: false, result: result})
		}
	})
}

exports.importQuestion = function(req, res, next) {
	uploadExcel(req, res, (err) => {
		if(!req.file) {
      res.json({ error: true, result: err });
    } else {

    	// create class Excel
    	var Excel = require('exceljs');
			var wb = new Excel.Workbook();
			var path = require('path');
			var filePath = path.resolve(__dirname, '../public/company/'+req.file.filename);

			wb.xlsx.readFile(filePath).then( async function(){
		    var sh = wb.getWorksheet("Sheet1");

		    //Get all the rows data [1st and 2nd column]
		    var tempArray = [];
		    for (i = 2; i <= sh.rowCount; i++) {

		    	console.log('==========================================================')

		    	var querySoal = `INSERT INTO exam_question (exam_id, tag, number, question, correct_option) VALUES (
					'${req.body.exam_id}', '${sh.getRow(i).getCell(2).value}', '${sh.getRow(i).getCell(1).value}',
					 '${sh.getRow(i).getCell(3).value}', '${sh.getRow(i).getCell(9).value}')`;
		    	console.log(querySoal)

		    	// proses insert soal
		    	const result = await db.query(querySoal);
	    		// pilihan jawaban A
		    	if(sh.getRow(i).getCell(4).value) {
			    	var queryJawab = `INSERT INTO exam_option (question_id, exam_option, description) VALUES ('${result.insertId}', 'A', '${sh.getRow(i).getCell(4).value}')`;
		    		console.log(queryJawab)
		    		db.query(queryJawab)
		    	}

		    	// pilihan jawaban B
		    	if(sh.getRow(i).getCell(5).value) {
			    	var queryJawab = `INSERT INTO exam_option (question_id, exam_option, description) VALUES ('${result.insertId}', 'B', '${sh.getRow(i).getCell(5).value}')`;
		    		console.log(queryJawab)
		    		db.query(queryJawab)
		    	}

		    	// pilihan jawaban C
		    	if(sh.getRow(i).getCell(6).value) {
			    	var queryJawab = `INSERT INTO exam_option (question_id, exam_option, description) VALUES ('${result.insertId}', 'C', '${sh.getRow(i).getCell(6).value}')`;
		    		console.log(queryJawab)
		    		db.query(queryJawab)
		    	}

		    	// pilihan jawaban D
		    	if(sh.getRow(i).getCell(7).value) {
			    	var queryJawab = `INSERT INTO exam_option (question_id, exam_option, description) VALUES ('${result.insertId}', 'D', '${sh.getRow(i).getCell(7).value}')`;
		    		console.log(queryJawab)
		    		db.query(queryJawab)
		    	}

		    	// pilihan jawaban E
		    	if(sh.getRow(i).getCell(8).value) {
			    	var queryJawab = `INSERT INTO exam_option (question_id, exam_option, description) VALUES ('${result.insertId}', 'E', '${sh.getRow(i).getCell(8).value}')`;
		    		console.log(queryJawab)
		    		db.query(queryJawab)
		    	}
		    	console.log('==========================================================')

		    	tempArray.push({
		    		examId: req.body.exam_id,
		    		no: sh.getRow(i).getCell(1).value,
		    		tag: sh.getRow(i).getCell(2).value,
		    		soal: sh.getRow(i).getCell(3).value,
		    		a: sh.getRow(i).getCell(4).value,
		    		b: sh.getRow(i).getCell(5).value,
		    		c: sh.getRow(i).getCell(6).value,
		    		d: sh.getRow(i).getCell(7).value,
		    		e: sh.getRow(i).getCell(8).value,
		    		jawaban: sh.getRow(i).getCell(9).value,
		    	});
		    }

		    res.json({ error: false, result: tempArray })
			});

    }
	})
}

exports.updatePenjelasanQuestion = (req, res) => {
  let sql = `UPDATE exam_question SET penjelasan = ? WHERE question_id = ?`;
  let data = [req.body.penjelasan, req.params.question_id];
  db.query(sql, data, (err, rows) => {
    if(err) res.json({ error: true, result: err })

    let sql = `SELECT * FROM exam_question WHERE question_id = ?`;
    db.query(sql, [req.params.question_id], (err, rows) => {
      console.log(rows)
      res.json({ error: false, result: rows.length ? rows[0] : [] })
    })
  })
}
