var conf = require('../configs/config');
var db = require('../configs/database');

exports.createWording = (req, res, next) => {
	let form = {
		parameter: req.body.parameter,
		type: req.body.type,
		word: req.body.word
	};

	// create Wording
	db.query(`INSERT INTO wording 
		(id_wording, parameter, type, word) 
		VALUES 
		(null, '${form.parameter}', '${form.type}', '${form.word}')`, (error, result, fields) => {
		if(error) {
			res.json({ error: true, result: error });
		} else {
			db.query(`SELECT * FROM wording WHERE id_wording = '${result.insertId}'`, (error, result, fields) => {
				res.json({
					error: false,
					result: result[0]
				});
			})
		}
	});
};

exports.getWording = (req, res, next) => {
	let field = req.params.by;
	let value = req.params.value;

	db.query(`SELECT * FROM wording WHERE ${field} = '${value}'`, (error, result, fields) => {
		if(error) {
			res.json({ error: true, result: error });
		} else {
			res.json({
				error: false,
				result: result
			});
		}
	});
};

exports.updateWording = (req, res, next) => {
	let field = req.params.by;
	let value = req.params.value;

	let form = {
		parameter: req.body.parameter,
		type: req.body.type,
		word: req.body.word
	};

	db.query(`UPDATE wording SET 
			parameter = '${form.parameter}', 
			type = '${form.type}', 
			word = '${form.word}' 
			WHERE ${field} = '${value}'`, (error, result, fields) => {
		if(error) {
			res.json({ error: true, result: error });
		} else {
			db.query(`SELECT * FROM wording WHERE ${field} = '${value}'`, (error, result, fields) => {
				res.json({
					error: false,
					result: result
				});
			})
		}
	});
};

exports.deleteWording = (req, res, next) => {
	let field = req.params.by;
	let value = req.params.value;

	db.query(`DELETE FROM wording WHERE ${field} = '${value}'`, (error, result, fields) => {
		if(error) {
			res.json({ error: true, result: error });
		} else {
			res.json({
				error: false,
				result: result
			});
		}
	});
};
