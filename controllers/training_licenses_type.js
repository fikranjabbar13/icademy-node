const db = require('../configs/database');
var multer = require('multer');
const AWS = require('aws-sdk');
var env = require('../env.json');
const fs = require('fs');

const s3 = new AWS.S3({
    accessKeyId: env.AWS_ACCESS,
    secretAccessKey: env.AWS_SECRET
});
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, './public/training/membership/image_card');
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname);
    }
})
let uploadImage = multer({ storage: storage }).single('image');
exports.uploadImage = (req, res) => {
    uploadImage(req, res, (err) => {
        if (!req.file) {
            res.json({ error: true, result: err });
        } else {
            if (env.AWS_BUCKET) {
                let path = req.file.path;
                let newDate = new Date();
                let keyName = `${env.AWS_BUCKET_ENV}/training/membership/image_card`;

                var params = {
                    ACL: 'public-read',
                    Bucket: env.AWS_BUCKET,
                    Body: fs.createReadStream(path),
                    Key: `${keyName}/${Date.now()}-${req.file.originalname}`
                };

                s3.upload(params, (err, data) => {
                    if (err) {
                        res.json({ error: true, msg: "Error Upload Image", result: err });
                    }
                    if (data) {
                        fs.unlinkSync(path);

                        let form = {
                            image: data.Location
                        }

                        db.query(`UPDATE training_licenses_type SET image_card = '${form.image}' WHERE id = '${req.params.id}'`, (error, result, fields) => {
                            if (error) {
                                res.json({ error: true, result: error })
                            } else {
                                res.json({ error: false, result: result })
                            }
                        })

                    }
                })
            }
            else {
                let form = {
                    image: `${env.APP_URL}/training/membership/image_card/${req.file.filename}`
                }

                db.query(`UPDATE training_licenses_type SET image_card = '${form.image}' WHERE id = '${req.params.id}'`, (error, result, fields) => {
                    if (error) {
                        res.json({ error: true, result: error })
                    } else {
                        res.json({ error: false, result: result })
                    }
                })
            }

        }
    })
}
exports.deleteImage = (req, res) => {
    if (req.params.id) {

        db.query(`UPDATE training_licenses_type SET image_card = '' WHERE id = '${req.params.id}'`, (error, result, fields) => {
            if (error) {
                res.json({ error: true, result: error })
            } else {
                res.json({ error: false, result: result })
            }
        })
    } else {
        res.json({ error: true, result: 'Delete image is failed' })
    }
}
exports.browse = (req, res) => {
    db.query(`SELECT * FROM training_licenses_type WHERE company_id = '${req.params.company_id}' AND status='active'`, (error, result) => {
        if (error) {
            res.json({ error: true, result: error.message })
        }
        else {
            res.json({ error: false, result: result })
        }
    })
}

exports.create = (req, res) => {
    db.query(`INSERT INTO training_licenses_type (company_id, name, image_card, status) VALUES (?);`, [[req.body.company_id, req.body.name, '', 'active']], (error, result) => {
        if (error) {
            res.json({ error: true, result: error.message })
        }
        else {
            res.json({ error: false, result: result })
        }
    })
}

exports.edit = (req, res) => {
    db.query(`UPDATE training_licenses_type SET name = ? WHERE id = ?`, [req.body.name, req.body.id], (error, result) => {
        if (error) {
            res.json({ error: true, result: error.message })
        }
        else {
            res.json({ error: false, result: result })
        }
    })
}

exports.delete = (req, res) => {
    db.query(`UPDATE training_licenses_type SET status = 'inactive' WHERE id = '${req.params.id}'`, (error, result) => {
        if (error) {
            res.json({ error: true, result: error.message })
        }
        else {
            res.json({ error: false, result: result })
        }
    })
}

exports.readLicenseFormat = (req, res) => {
    db.query(`SELECT * FROM training_company_licenses_format WHERE company_id = '${req.params.id}'`, (error, result) => {
        if (error) {
            res.json({ error: true, result: error.message })
        }
        else {
            res.json({ error: false, result: result[0] })
        }
    })
}

exports.updateLicenseFormat = async (req, res) => {
    let check = await db.query(`SELECT * FROM training_company_licenses_format WHERE company_id='${req.params.id}'`)
    if (check.length) {
        db.query(
            `UPDATE training_company_licenses_format
            SET format = ?
            WHERE company_id = ?;`,
            [req.body.format, req.params.id],
            (error, result) => {
                if (error) {
                    return res.json({ error: true, result: error.message });
                }
                else {
                    return res.json({ error: false, result: result })
                }
            }
        );
    }
    else {
        db.query(
            `INSERT INTO training_company_licenses_format (company_id, format) VALUES ('${req.params.id}','${req.body.format}')`,
            [req.body.format, req.params.id],
            (error, result) => {
                if (error) {
                    return res.json({ error: true, result: error.message });
                }
                else {
                    return res.json({ error: false, result: result })
                }
            }
        );
    }
};