var conf = require('../configs/config');
var db = require('../configs/database');

exports.getMyCreateCourse = (req, res, next) => {
	let userId = req.params.user_id;
	let companyId = req.params.company_id;

	db.query(`SELECT c.*, ct.category_name
		FROM course c LEFT JOIN learning_category ct ON ct.category_id = c.category_id 
		WHERE c.user_id = '${userId}' AND c.publish = '1' AND ct.company_id = '${companyId}'`, async (error, result, fields) => {
		if(error) {
			res.json({error: true, result: error})
		} else {
			if(result.length != 0) {
				for(let i=0; i<result.length; i++) {
					let userCourse = await db.query(`SELECT course_id FROM user_course WHERE course_id = '${result[i].course_id}'`);
					result[i].peserta = userCourse.length;

					let quizCourse = await db.query(`SELECT course_id FROM exam WHERE course_id = '${result[i].course_id}' AND quiz = '1'`);
					result[i].quiz = quizCourse.length;

					let examCourse = await db.query(`SELECT course_id FROM exam WHERE course_id = '${result[i].course_id}' AND quiz = '0'`);
					result[i].ujian = examCourse.length;
				}
			} 

			res.json({error: false, result: result})
		}
	})
};

exports.getHaislKursus = (req, res, next) => {
	let courseId = req.params.course_id;
	// let userId = req.params.user_id;

	db.query(`SELECT c.course_id, c.image, c.title, ct.category_name, c.created_at
		FROM course c LEFT JOIN learning_category ct ON ct.category_id = c.category_id 
		WHERE c.course_id = '${courseId}'`, async (error, result, fields) => {
		if(error) {
			res.json({error: true, result: error})
		} else {
			if(result.length != 0) {
				let userCourse = await db.query(`SELECT course_id FROM user_course WHERE course_id = '${courseId}'`);
				result[0].peserta = userCourse.length;

				let quizCourse = await db.query(`SELECT course_id, exam_id FROM exam WHERE course_id = '${courseId}' AND quiz = '1'`);
				result[0].quiz = quizCourse.length;

				let examCourse = await db.query(`SELECT course_id, exam_id FROM exam WHERE course_id = '${courseId}' AND quiz = '0' AND exam_publish = '1'`);
				result[0].ujian = examCourse.length;
				
				let usersCourse = await db.query(`SELECT u.user_id, u.name, u.identity 
					FROM user u JOIN user_course uc ON uc.user_id = u.user_id 
					WHERE uc.course_id = '${courseId}'`);
				result[0].users = usersCourse;

				if(usersCourse.length != 0) {
					for(let j=0; j<usersCourse.length; j++) {
						let tempQuiz = [], tempExam = [];
						//get nilai quiz
						for(let i=0; i<quizCourse.length; i++) {
							let scoreResult = await db.query(`SELECT * 
								FROM exam_result 
								WHERE user_id = '${usersCourse[j].user_id}' AND course_id = '${quizCourse[i].course_id}' AND exam_id = '${quizCourse[i].exam_id}'`);
							tempQuiz.push(scoreResult);
						}
						// result[0].users[j].quiz = quizCourse.length == 0 ? [] : quizCourse;
						result[0].users[j].quiz = tempQuiz;

						//get nilai ujian
						for(let i=0; i<examCourse.length; i++) {
							let scoreResult = await db.query(`SELECT * 
								FROM exam_result 
								WHERE user_id = '${usersCourse[j].user_id}' AND course_id = '${examCourse[i].course_id}' AND exam_id = '${examCourse[i].exam_id}'`);
							tempExam.push(scoreResult);
						}
						// result[0].users[j].ujian = examCourse.length == 0 ? [] : examCourse;
						result[0].users[j].ujian = tempExam;
					}
				}
				res.json({error: false, result: result[0]})
			} else {
				res.json({error: false, result: []})
			}
		}
	})	
};