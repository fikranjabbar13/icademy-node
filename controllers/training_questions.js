var env = require('../env.json');
const db = require('../configs/database');
var multer = require('multer');
const AWS = require('aws-sdk');
const fs = require('fs');
const { format } = require('../configs/database');
var moment = require('moment');
const { array } = require('joi');
const exceljsRichText = require("../middleware/exceljsRichText");
const e = require('connect-timeout');
const moduleDB = require('../repository');
const validatorRequest = require('../validator');
const s3 = new AWS.S3({
    accessKeyId: env.AWS_ACCESS,
    secretAccessKey: env.AWS_SECRET
});
var storageEx = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, './public/training/questions/excel');
    },
    filename: (req, file, cb) => {
        cb(null, 'excel-' + Date.now() + '-' + file.originalname);
    }
});
var uploadExcel = multer({ storage: storageEx }).single('file');

exports.import = (req, res) => {
    uploadExcel(req, res, (err) => {
        if (!req.file) {
            res.json({ error: true, result: err });
        } else {
            // create class Excel
            var Excel = require('exceljs');
            var wb = new Excel.Workbook();
            var path = require('path');
            var filePath = path.resolve(__dirname, '../public/training/questions/excel/' + req.file.filename);

            var form = {
                company_id: req.body.company_id,
                status: 'Active',
                created_at: moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
            };


            wb.xlsx.readFile(filePath).then(async function () {
                var sh = wb.getWorksheet("Sheet1");
                let tmp = [];
                let getValue = sh.getSheetValues();
                let field = [
                    'Status',
                    'Row',
                    sh.getRow(1).getCell(1).value,
                    sh.getRow(1).getCell(2).value,
                    sh.getRow(1).getCell(3).value,
                    sh.getRow(1).getCell(4).value,
                    sh.getRow(1).getCell(5).value,
                    sh.getRow(1).getCell(6).value,
                    sh.getRow(1).getCell(7).value,
                    sh.getRow(1).getCell(8).value,
                    sh.getRow(1).getCell(9).value,
                    sh.getRow(1).getCell(10).value,
                    sh.getRow(1).getCell(11).value,
                    sh.getRow(1).getCell(12).value
                ];

                for (var i = 4; i < sh.rowCount; i++) {
                    if (sh.getRow(i).getCell(1).value !== null) {

                        // remove field yg tidak dipakai
                        getValue[i].shift();
                        if (getValue[i][getValue[i].length - 1] === '<== Starts at this row') {
                            getValue[i].pop();
                        }
                        // check rich text
                        if (typeof getValue[i][2] === 'object' || exceljsRichText.isRichValue(getValue[i][2])) {
                            getValue[i][2] = exceljsRichText.richToHtml(getValue[i][2]);
                        }
                        if (typeof getValue[i][3] === 'object' || exceljsRichText.isRichValue(getValue[i][3])) {
                            getValue[i][3] = exceljsRichText.richToHtml(getValue[i][3]);
                        }
                        if (typeof getValue[i][4] === 'object' || exceljsRichText.isRichValue(getValue[i][4])) {
                            getValue[i][4] = exceljsRichText.richToHtml(getValue[i][4]);
                        }
                        if (typeof getValue[i][5] === 'object' || exceljsRichText.isRichValue(getValue[i][5])) {
                            getValue[i][5] = exceljsRichText.richToHtml(getValue[i][5]);
                        }
                        if (typeof getValue[i][6] === 'object' || exceljsRichText.isRichValue(getValue[i][6])) {
                            getValue[i][6] = exceljsRichText.richToHtml(getValue[i][6]);
                        }
                        if (typeof getValue[i][7] === 'object' || exceljsRichText.isRichValue(getValue[i][7])) {
                            getValue[i][7] = exceljsRichText.richToHtml(getValue[i][7]);
                        }

                        if (typeof getValue[i][9] === 'object' || exceljsRichText.isRichValue(getValue[i][9])) {
                            getValue[i][9] = exceljsRichText.richToHtml(getValue[i][9]);
                        }

                        tmp.push({ // static setup object
                            licenses_type: getValue[i][0],
                            course: getValue[i][1],
                            question: getValue[i][2],
                            option: {
                                a: getValue[i][3], //a
                                b: getValue[i][4], //b
                                c: getValue[i][5], //c
                                d: getValue[i][6], //d
                                e: getValue[i][7] //e
                            },
                            answer: getValue[i][8].toLocaleUpperCase(),
                            explanation: getValue[i][9] || '',
                            tag: getValue[i][10] || '',
                            type: getValue[i][11] || '1',
                            origin: getValue[i]
                        })
                    }
                }


                let data = [];
                if (tmp.length > 0) {
                    for (let index = 0; index < tmp.length; index++) {
                        let str = tmp[index];
                        console.log(str.origin, '???')
                        // check licenses type exists
                        let checkLicensesType = await db.query(`SELECT id FROM training_licenses_type WHERE name = '${str.licenses_type}' AND company_id='${form.company_id}' AND status='Active' ORDER BY id DESC LIMIT 1`);
                        // check course exists
                        let checkCourse = await db.query(`SELECT id FROM training_course WHERE title = '${str.course}' AND company_id='${form.company_id}' AND status='Active' ORDER BY id DESC LIMIT 1`);
                        let arr = ['Success', index];

                        if (checkLicensesType.length > 0) {

                            let appendQuery = ` VALUES (
                                NULL, 
                                '${str.type}',
                                '${str.tag}', 
                                '${str.question}', 
                                '${str.answer}', 
                                '${str.explanation}',
                                '${checkLicensesType[0].id}', 
                                '${form.status}', 
                                '${form.created_at}')`;

                            if (checkCourse.length > 0) {
                                appendQuery = ` VALUES (
                                    '${checkCourse[0].id}',
                                    '${str.type}', 
                                    '${str.tag}', 
                                    '${str.question}', 
                                    '${str.answer}', 
                                    '${str.explanation}',
                                    '${checkLicensesType[0].id}', 
                                    '${form.status}', 
                                    '${form.created_at}')`;
                            }
                            let sqlString = `INSERT INTO training_questions (training_course_id,type, tag, question, answer, explanation, licenses_type_id, status, created_at) ${appendQuery};`;
                            let sqlStringOption = `INSERT INTO training_questions_options (question_id, option_label, option_text) VALUES(?, ?, ?)`;
                            try {
                                let insert = await db.query(sqlString);
                                let keys = Object.keys(str.option);

                                if (!insert.insertId) {
                                    arr[0] = 'Failed';
                                }

                                for (let i = 0; i < keys.length; i++) {
                                    if (str.option[keys[i]] == 0 || str.option[keys[i]]) {
                                        let insertOp = await db.query(sqlStringOption, [insert.insertId, keys[i].toLocaleUpperCase(), str.option[keys[i]]]);
                                        if (insertOp.insertId) {

                                            arr[0] = 'Success';
                                        } else {
                                            arr[0] = 'Warning';
                                            break;
                                        }
                                    }
                                }

                                arr = arr.concat(str.origin);
                                data.push(arr);

                            } catch (e) {
                                arr[0] = 'Failed';
                                arr = arr.concat(str.origin);
                                data.push(arr);
                            }
                        } else {
                            arr[0] = 'Failed';
                            arr = arr.concat(str.origin);
                            data.push(arr);
                        }

                        //console.log(data, 'TEST')
                    }
                }
                //console.log(data, 'TEST2')
                res.json({ error: false, result: { field: field, data: data } });
                return;
            });

        }
    })
}

exports.browseByCompany = async (req, res) => {
    db.query(
        `SELECT q.*, c.title AS course, t.name AS licenses_type
        FROM training_questions q
        LEFT JOIN training_course c ON c.id = q.training_course_id
        LEFT JOIN training_licenses_type t ON t.id = q.licenses_type_id
        WHERE t.company_id = '${req.params.id}' AND q.status='Active'
        ORDER BY created_at DESC`,
        [req.params.company_id],
        async (error, result) => {
            if (error) {
                return res.json({ error: true, result: error.message });
            };
            return res.json({ error: false, result: result });
        }
    );
};
exports.browseByCompanyArchived = async (req, res) => {
    db.query(
        `SELECT q.*, c.title AS course, t.name AS licenses_type
        FROM training_questions q
        LEFT JOIN training_course c ON c.id = q.training_course_id
        LEFT JOIN training_licenses_type t ON t.id = q.licenses_type_id
        WHERE t.company_id = '${req.params.id}' AND q.status='Inactive'
        ORDER BY created_at DESC`,
        [req.params.company_id],
        async (error, result) => {
            if (error) {
                return res.json({ error: true, result: error.message });
            };
            return res.json({ error: false, result: result });
        }
    );
};

exports.read = (req, res) => {
    db.query(
        `SELECT *
        FROM training_questions
        WHERE id = ?
        LIMIT 1;`,
        [req.params.id],
        async (error, result) => {
            if (error) {
                return res.json({ error: true, result: error.message });
            };

            let options = await db.query(`SELECT * FROM training_questions_options WHERE question_id=?`, [req.params.id]);
            result[0].options = options;
            return res.json({ error: false, result: result[0] });
        }
    );
};

exports.create = async (req, res) => {

    try {

        let form = req.body;

        let sqlString = `INSERT INTO training_questions 
                        (training_course_id,tag, question, answer, explanation, licenses_type_id, status, created_at,type) 
                        VALUES (?);`
        let sqlParams = [[req.body.training_course_id, req.body.tag, req.body.question, req.body.answer, req.body.explanation, req.body.licenses_type_id, 'Active', new Date()]];

        if (form.type == 2) {
            if (form.optionShortAnswer.length == 0) {
                return res.json({ error: true, result: "Question must be one asnwer correction." });
            }

            sqlParams[0].push(2)
            let result = await db.query(sqlString, sqlParams);
            if (result.insertId) {
                await Promise.all(form.optionShortAnswer.map(async (item) => {
                    if (item.option_text !== '') {

                        return new Promise((resolve, reject) => {
                            db.query(`INSERT INTO training_questions_options (question_id, option_label, option_text) VALUES(?, ?, ?)`, [result.insertId, item.option_label, item.option_text], (err, res) => {
                                if (err) reject(res);
                                resolve(res);
                            });
                        })
                    }
                }));

                return res.json({ error: false, result: result })

            } else {
                return res.json({ error: true, result: error.message });
            }
        } else {
            sqlParams[0].push(1)
            let result = await db.query(sqlString, sqlParams);
            if (result.insertId) {
                await Promise.all(form.options.map(async (item) => {
                    if (item.option_text !== '') {

                        return new Promise((resolve, reject) => {
                            db.query(`INSERT INTO training_questions_options (question_id, option_label, option_text) VALUES(?, ?, ?)`, [result.insertId, item.option_label, item.option_text], (err, res) => {
                                if (err) reject(res);
                                resolve(res);
                            });
                        })
                    }
                }));

                return res.json({ error: false, result: result })

            } else {
                return res.json({ error: true, result: error.message });
            }
        }
    } catch (err) {
        res.json({ error: true, result: err })
    }

};

exports.update = (req, res) => {
    db.query(
        `UPDATE training_questions
        SET
        tag=?,
        training_course_id=?,
        question=?,
        answer=?,
        explanation=?,
        licenses_type_id=?,
        type=? 
        WHERE id=?;`,
        [req.body.tag, req.body.training_course_id, req.body.question, req.body.answer, req.body.explanation, req.body.licenses_type_id, req.body.type, req.params.id],
        async (error, result) => {
            if (error) {
                return res.json({ error: true, result: error.message });
            }
            else {
                await db.query(`DELETE FROM training_questions_options WHERE question_id=?`, [req.params.id]);
                if (req.body.type === 2) {
                    await Promise.all(req.body.optionShortAnswer.map(async (item) => {
                        if (item.option_text !== '') {
                            await db.query(`INSERT INTO training_questions_options (question_id, option_label, option_text) VALUES('${req.params.id}','${item.option_label}','${item.option_text}')`);
                        }
                    }))
                } else {
                    await Promise.all(req.body.options.map(async (item) => {
                        if (item.option_text !== '') {
                            await db.query(`INSERT INTO training_questions_options (question_id, option_label, option_text) VALUES('${req.params.id}','${item.option_label}','${item.option_text}')`);
                        }
                    }))
                }
                return res.json({ error: false, result: result })
            }
        }
    );
};

exports.updateStatusQuestionsBulk = async (req, res) => {
    try {

        //Validate Request 
		const retrieveValidRequest = await validatorRequest.validateUpdateStatusQuestionsBulkRequest(req.body);
        await moduleDB.updateStatusBulkQuestions(retrieveValidRequest);

		res.json({ error: false, result: 'Success Updated Status Questions' })
    } catch (error) {
        res.json({ error: true, result: error.message })
    }
};

exports.delete = (req, res) => {
    db.query(
        `UPDATE training_questions
        SET status='Inactive'
        WHERE id = ?;`,
        [req.params.id],
        (error, result) => {
            if (error) {
                return res.json({ error: true, result: error.message });
            };

            return res.json({ error: false, result });
        }
    );
};

exports.activate = (req, res) => {
    db.query(
        `UPDATE training_questions
        SET status='Active'
        WHERE id = ?;`,
        [req.params.id],
        (error, result) => {
            if (error) {
                return res.json({ error: true, result: error.message });
            };

            return res.json({ error: false, result });
        }
    );
};