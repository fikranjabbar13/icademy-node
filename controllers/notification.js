const env = require('../env.json');
const db = require('../configs/database');
let conf = require('../configs/config');
let moment = require("moment-timezone");

exports.getNotificationAll = (req, res, next) => {
  var { companyId } = req.query;
  var sql = ``;
  if (companyId) {
    sql = `SELECT n.*, c.company_name FROM notification n LEFT JOIN company c ON c.company_id = n.company_id WHERE (n.user_id = '${req.params.user_id}' OR n.company_id = '${companyId}') ORDER BY n.created_at DESC;
    SELECT * FROM reminder WHERE user_id = '${req.params.user_id}' ORDER BY date DESC;`
  } else {
    sql = `SELECT * FROM notification WHERE user_id = '${req.params.user_id}' ORDER BY created_at DESC;
    SELECT * FROM reminder WHERE user_id = '${req.params.user_id}' ORDER BY date DESC;`;
  }
  db.query(sql, async (error, result, fields) => {
    if (error) {
      res.json({ error: true, result: error });
    } else {
      if (result[0].length > 0) {
        let tmp = [];
        for (let i = 0; i < result[0].length; i++) {
          if (result[0][i].type == 3) {
            if (result[0][i].activity_id) {
              tmp.push(result[0][i].activity_id);
            }
          }
        }

        if (tmp.length > 0) {

          // You are invited meeting "Test12333333" that will start on "January 21, 2022 11:00-12:00 ((UTC+08:00) Taipei Time Zone)"
          let resultBooking = await db.query(`
            select id, DATE_FORMAT(lb.date_start,'%Y-%m-%d %H:%i:%s') as date_start, DATE_FORMAT(lb.date_end,'%Y-%m-%d %H:%i:%s') as date_end, lb.timezone , lbp.start_time 
            from liveclass_booking lb 
            LEFT JOIN liveclass_booking_participant lbp ON lbp.booking_id = lb.id AND lbp.user_id = '${req.params.user_id}'  
            where lb.id in(${tmp.toString()})`);

          for (let i = 0; i < resultBooking.length; i++) {

            let tz = resultBooking[i].timezone || 'Asia/Jakarta';
            resultBooking[i].date_start = moment.tz(resultBooking[i].start_time ? resultBooking[i].start_time : resultBooking[i].date_start, tz)
            resultBooking[i].date_end = moment.tz(resultBooking[i].date_end, tz)

          }

          for (let i = 0; i < result[0].length; i++) {
            if (result[0][i].type == 3) {
              let idx = resultBooking.findIndex((str) => { return str.id == result[0][i].activity_id });
              if (idx > -1) {
                result[0][i].convert = resultBooking[idx];
              }
            }
          }
        }
      }
      res.json({ error: false, result: result });
    }
  });
};

exports.getNotificationUnread = (req, res, next) => {
  db.query(
    `SELECT * FROM notification WHERE user_id = '${req.params.user_id}' AND isread = '0' AND tag=1 ORDER BY created_at DESC;
      SELECT * FROM reminder WHERE user_id = '${req.params.user_id}' AND isread = '0' ORDER BY date DESC;`,
    (error, result, fields) => {
      if (error) {
        res.json({ error: true, result: error });
      } else {
        res.json({ error: false, result: result });
      }
    });
};

exports.getNotificationRecent = (req, res, next) => {
  db.query(
    `SELECT * FROM notification WHERE user_id = '${req.params.user_id}' AND isread <> '1' ORDER BY created_at DESC;
      SELECT * FROM reminder WHERE user_id = '${req.params.user_id}' AND isread <> '1' ORDER BY date DESC`,
    (error, result, fields) => {
      if (error) {
        res.json({ error: true, result: error });
      } else {
        res.json({ error: false, result: result });
      }
    });
};

exports.updateNotificationToRead = (req, res, next) => {
  db.query(`UPDATE notification SET isread = '1' WHERE id = '${req.body.id}'`, (error, result, fields) => {
    if (error) {
      res.json({ error: true, result: error });
    } else {
      res.json({ error: false, result: result });
    }
  });
};

exports.updateNotificationToShow = (req, res, next) => {
  db.query(`UPDATE notification SET isread = '2' WHERE id = '${req.body.id}'`, (error, result, fields) => {
    if (error) {
      res.json({ error: true, result: error });
    } else {
      res.json({ error: false, result: result });
    }
  });
};

exports.addNotification = async (req, res, next) => {
  var data = req.body;
  var companyId = data.hasOwnProperty('company_id') ? data.company_id : null;
  if (data.type === 12) {
    let user = await db.query(`SELECT u.user_id FROM user u LEFT JOIN training_user tu ON tu.email = u.email WHERE tu.id = '${data.user_id}'`);
    if (user.length) {
      data.user_id = user[0].user_id
    }
  }
  db.query(`
        INSERT INTO
            notification (user_id, company_id, type, activity_id, description, destination, isread )
            VALUES('${data.user_id}', '${companyId}', '${data.type}', '${data.activity_id}', '${data.desc}', '${data.dest}', '0')`, (error, result, fields) => {
    if (error) {
      res.json({ error: true, result: error });
    } else {
      res.json({ error: false, result: result });
    }
  });
};

exports.addNotificationBulk = async (req, res, next) => {
  var data = req.body;
  var companyId = data.hasOwnProperty('company_id') ? data.company_id : null;

  await Promise.all(data.user_id.map(async (item) => {
    if (data.type === 12) {
      let user = await db.query(`SELECT u.user_id FROM user u LEFT JOIN training_user tu ON tu.email = u.email WHERE tu.id = '${item}'`);
      if (user.length) {
        item = user[0].user_id
      }
    }

    let desc = null;
    if (data.type == 3) {
      let resData = await db.query(`
        SELECT lb.jam_mulai, lbp.start_time, lbp.user_id from liveclass_booking lb 
        LEFT JOIN liveclass_booking_participant lbp ON lbp.booking_id = lb.id 
        where lb.id = '${data.activity_id}' AND lbp.user_id = '${item}'
      `);

      if (resData.length) {
        if (resData[0].start_time) {
          desc = data.desc.replace(resData[0].jam_mulai, moment(resData[0].start_time).format("HH:mm"));
        }
      }
    }
    db.query(`
          INSERT INTO
              notification (user_id, company_id, type, activity_id, description, destination, isread )
              VALUES('${item}', '${companyId}', '${data.type}', '${data.activity_id}', '${desc ? desc : data.desc}', '${data.dest}', '0')`);
  }))
  res.json({ error: false, result: 'OK' });
};

exports.readAllToRead = (req, res) => {
  let form = {
    userId: req.body.userId,
    notifIds: req.body.notifIds
  };

  let sql = `UPDATE notification SET isread = '1' WHERE id IN (${form.notifIds.toString()})`;

  db.query(sql, (err, rows) => {
    console.log(rows)
    res.json({ error: false, result: rows })
  })
}


exports.deleteAllNotif = (req, res) => {
  db.query(`DELETE FROM notification WHERE id IN (${req.body.notifIds.toString()})`, (err, rows) => {
    res.json({ error: false, result: rows })
  })
}

exports.deleteNotif = (req, res) => {
  db.query(`DELETE FROM notification WHERE id = '${req.params.id}'`, (err, rows) => {
    res.json({ error: false, result: rows })
  })
}


exports.getAccess = (req, res) => {
  let sql = `SELECT n.*, c.company_id FROM notification_alert n JOIN company c ON c.company_id = n.company_id WHERE n.role = '${req.params.role}' AND n.company_id = '${req.params.company_id}'`;

  db.query(sql, (err, rows, fields) => {
    if (err) {
      res.json({ error: true, result: err });
    } else {
      res.json({ error: false, result: rows })
    }
  })
}



exports.checkAccess = (req, res) => {

  let sql = '', data = [];

  sql = `SELECT na.code, na.status_website, n.types FROM notification_alert na JOIN notification n WHERE company_id = ? AND code IN (?)`;
  data = [req.query.company_id, req.query.param];

  db.query(sql, data, (err, rows) => {
    if (err) {
      res.json({ error: true, result: err })
    } else {

      rows.map(item => {
        item.status_website = item.status_website === 1 ? true : false
      })

      if (!rows.length) {
        for (let i = 0; i < req.query.param.length; i++) {
          rows[i] = { code: req.query.param[i], status_website: true }
        }
      }
      res.json({ error: false, result: rows })
    }
  })

}

exports.getNotif = (req, res) => {

  let { user_id, type, tag, types } = req.query;

  let sql = `SELECT * FROM notification WHERE user_id = ${user_id} `;
  if (type) {
    sql += `AND type = ${type} `
  }
  else if (tag) {
    sql += `AND tag = ${tag}`
  }
  else {
    sql += `AND types = ${types}`
  }

  db.query(sql, (err, rows) => {
    if (err) {
      res.json({ error: true, result: err });
    } else {
      res.json({ error: false, result: rows });
    }
  })


}

exports.patchStatus = async (req, res) => {


  // if(req.params.type === 'email'){
  //   let sqlEmail = `UPDATE notification_alert SET status_email = '${req.body.status_email}' WHERE id_access = 
  // '${req.params.id_access}'`;

  //   db.query(sqlEmail, (err, rows) => {
  //   if(err){
  //       res.json({error: true, result: err});
  //     }else{
  //       res.json({error: false, result : {status_email : req.body.status_email, id_access: req.params.id_access}});
  //     }
  //   })
  // }
  if (req.params.type === 'website') {
    let sqlWebsite = `UPDATE notification_alert SET status_website = '${req.body.status_website}' WHERE id_access = 
    '${req.params.id_access}'`;

    db.query(sqlWebsite, (err, rows) => {
      if (err) {
        res.json({ error: true, result: err });
      } else {
        res.json({ error: false, result: { status_website: req.body.status_website, id_access: req.params.id_access } });
      }
    })
  } else {

    let sqlMobile = `UPDATE notification_alert SET status_mobile = '${req.body.status_mobile}' WHERE id_access = 
    '${req.params.id_access}'`;

    db.query(sqlMobile, (err, rows) => {
      if (err) {
        res.json({ error: true, result: err });
      } else {
        res.json({ error: false, result: { status_mobile: req.body.status_mobile, id_access: req.params.id_access } });
      }
    })
  }
}
