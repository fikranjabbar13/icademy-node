const path = require('path');
const multer = require('multer');
const moment = require("moment");
const AWS = require('aws-sdk');
const fs = require('fs');

const env = require('../env.json');
const model = require("../repository").crmProject;
const validator = require("../validator").crmTask;

const s3 = new AWS.S3({
    accessKeyId: env.AWS_ACCESS,
    secretAccessKey: env.AWS_SECRET
  });
const storage = multer.diskStorage({
  filename: function (req, file, cb) {
    cb(null, file.originalname);
  }
})
const storageExcel = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, './public/company');
  },
  filename: (req, file, cb) => {
    cb(null, 'excel-' + Date.now() + '-' + file.originalname);
  }
});
const uploadImage = multer({ storage: storage }).single('files');
const uploadExcel = multer({storage: storageExcel}).single('excel');
const uploadMultipleFiles = multer({ storage: storage }).array('files', 10);

const s3Upload = async(req,res,path_name)=>{
	return new Promise(async (hasil)=>{
		uploadImage(req, res,async (err) => {
			if(req.file){
				let path = req.file.path;
				let keyName = `${env.AWS_BUCKET_ENV}/${path_name}`;
				let params = {
					ACL: 'public-read',
					Bucket: env.AWS_BUCKET,
					Body: fs.createReadStream(path),
					Key: `${keyName}/${req.file.originalname}`,
					Metadata: {
						'Content-Type': req.file.mimetype,
						'Content-Disposition': 'inline'
					}
				};
				s3.upload(params, (err, data) => {
					if (data) {
						fs.unlinkSync(path);
					}

					req.body.files = data.Location
					return hasil(req);
				});
			}else{
				return hasil(req);
			}
		});
	});
}

const s3UploadMultiple = async(req,res,path_name)=>{
	return new Promise(async (hasil)=>{
		uploadMultipleFiles(req, res, async (err) => {
			if(req.files){
				console.log(req.files, req.file, err,'heleh')
				// let path = req.file.path;
				// let keyName = `${env.AWS_BUCKET_ENV}/${path_name}`;
				// let params = {
				// 	ACL: 'public-read',
				// 	Bucket: env.AWS_BUCKET,
				// 	Body: fs.createReadStream(path),
				// 	Key: `${keyName}/${req.file.originalname}`,
				// 	Metadata: {
				// 		'Content-Type': req.file.mimetype,
				// 		'Content-Disposition': 'inline'
				// 	}
				// };
				// s3.upload(params, (err, data) => {
				// 	if (data) {
				// 		fs.unlinkSync(path);
				// 	}

				// 	req.body.files = data.Location
				// 	return hasil(req);
				// });
			}else{
				return hasil(req);
			}
		});
	});
}

function AsyncUploadMultiple(req,res,next){
	return new Promise(async (hasil)=>{
		uploadMultipleFiles(req, res, async (err) => {
			if(!req.files) {
				return hasil({ error: true, result: err })
			} else {
				return hasil({ error: false, result: req });
			}
		});
	})	
}

function AsyncUploadFile(req,res,next){
	return new Promise(async (hasil)=>{
		uploadExcel(req, res, async (err) => {
			if(!req.file) {
				return hasil({ error: true, result: err })
			} else {
				return hasil({ error: false, result: { file: req.file, body:req.body } });
			}
		});
	})	
}

const storeTaskChecklistItem = async(req)=>{
	let msg = { error: false, result : [] }
	try {
		msg = await validator.storeTaskChecklistItem(req.body);
		if(!msg.error){
			msg = await model.storeTaskChecklistItem(msg.result,req.params);
		}
	} catch (error) {
		console.log(error);
		msg.error = true;
		msg.result = error;
	}
	return msg;
}

exports.getProjectsFilterData = async(req,res, next)=>{
	let msg = { error: false, result : [] }
	try {
		let user = req.app.token;
		msg = await model.getProjectsFilterData(req.body,req.params);
	} catch (error) {
		console.log(error);
		msg.error = true;
	}
	res.json(msg);
}

exports.getProjects = async(req,res, next)=>{
	let msg = { error: false, result : [] }
	try {
		let user = req.app.token;
		let query = req.query;
		if(query.filter){
			query.filter = JSON.parse(query.filter);
			query.filter.deadline = query.filter.deadline;
			if(!query.filter.specific_user_id) query.filter.specific_user_id = user.user_id
			
		}else{
			query.filter = {};
		}
		params = {
			...req.params,
			user ,
			...query
		}
		//msg.result = params;
		msg = await model.getDetail({ companyId:45 },params);
	} catch (error) {
		console.log(error);
		msg.error = true;
	}
	res.json(msg);
}

exports.storeProjects = async(req,res, next)=>{
	let msg = { error: false, result : [] }
	try {
        //title:null,project_type:enum('external','internal'),client_id:null,description:null,start_date:null,deadline:null,price:0,labels:[]
		//msg = await validator.store(req.body);
		msg.result = req.body;
        if(!msg.error){
			msg.result.created_by = req.app.token.user_id;
			msg = await model.storeProject(msg.result,req.params);
		}
	} catch (error) {
		console.log(error);
		msg.error = true;
		msg.result = error;
	}
	res.json(msg);
}

exports.cloneProjects = async(req,res, next)=>{
	let msg = { error: false, result : [] }
	try {
		const attribute = req.body.attribute;
		msg.result = req.body;
        if(!msg.error){
			msg.result.created_by = req.app.token.user_id;
			msg = await model.storeProject(msg.result,req.params);
			msg.result.oldIdProject = req.body.idProject;
			msg.result.newIdProject = msg.result.newIdProject;
			msg.result.start_date_project = req.body.start_date;
			msg.result.deadline_project = req.body.deadline;
			console.log(attribute,'?')
			if(attribute.copy_task) {
				await model.cloneTaskProject(msg.result, attribute);
			}

			if(attribute.copy_milestone) {
				await model.cloneMilestoneProject(msg.result,req.params);
			}
			if(attribute.copy_members){
				await model.cloneMembers(msg.result,req.params);
			}
		}
	} catch (error) {
		console.log(error);
		msg.error = true;
		msg.result = error;
	}
	res.json(msg);
}

exports.editProjects = async(req,res, next)=>{
	let msg = { error: false, result : [] }
	try {
        //title:null,project_type:enum('external','internal'),client_id:null,description:null,start_date:null,deadline:null,price:0,labels:[]
		//msg = await validator.store(req.body);
		msg.result = req.body;
        if(!msg.error){
			msg.result.created_by = req.app.token.user_id;
			msg = await model.editProject(msg.result,req.params);
		}
	} catch (error) {
		console.log(error);
		msg.error = true;
		msg.result = error;
	}
	res.json(msg);
}

exports.deleteProjects = async(req,res, next)=>{
	let msg = { error: false, result : [] }
	try {
        //title:null,project_type:enum('external','internal'),client_id:null,description:null,start_date:null,deadline:null,price:0,labels:[]
		//msg = await validator.store(req.body);
		msg.result = req.body;
        if(!msg.error){
			msg = await model.deleteProjects(msg.result);
		}
	} catch (error) {
		console.log(error);
		msg.error = true;
		msg.result = error;
	}
	res.json(msg);
}

exports.storeProjectLabel = async(req,res, next)=>{
	let msg = { error: false, result : [] }
	try {
		msg.result = req.body;
		if(!msg.error){
			msg.result.user_id = req.app.token.user_id;
            msg.result.context = req.body.context || 'project';
			msg = await model.storeLabelProject(msg.result,req.params);
		}
	} catch (error) {
		console.log(error);
		msg.error = true;
		msg.result = error;
	}
	res.json(msg);
}

exports.getProjectsMilestone = async(req,res, next)=>{
	let msg = { error: false, result : [] }
	try {
		let user = req.app.token;
		msg = await model.getProjectsMilestone(req.body,req.params);
	} catch (error) {
		console.log(error);
		msg.error = true;
	}
	res.json(msg);
}

exports.storeProjectMilestones = async(req,res, next)=>{
	let msg = { error: false, result : [] }
	try {
		msg.result = req.body;
		if(!msg.error){
			msg.result.user_id = req.app.token.user_id;
			msg = await model.storeMilestone(msg.result,req.params);
		}
	} catch (error) {
		console.log(error);
		msg.error = true;
		msg.result = error;
	}
	res.json(msg);
}

exports.storeProjectMembers= async(req,res, next)=>{
	let msg = { error: false, result : [] }
	try {
		msg.result = req.body;
		if(!msg.error){
			msg = await model.storeProjectMember(msg.result,req.params);
		}
	} catch (error) {
		console.log(error);
		msg.error = true;
		msg.result = error;
	}
	res.json(msg);
}

exports.removeProjectMembers= async(req,res, next)=>{
	let msg = { error: false, result : [] }
	try {
		msg.result = req.body;
		if(!msg.error){
			msg = await model.removeProjectMember(msg.result,req.params);
		}
	} catch (error) {
		console.log(error);
		msg.error = true;
		msg.result = error;
	}
	res.json(msg);
}

exports.getProjectMembers= async(req,res, next)=>{
	let msg = { error: false, result : [] }
	try {
		msg.result = req.params;
		if(!msg.error){
			msg.result.user_id = req.app.token.user_id;
			msg = await model.getProjectMember(msg.result,req.params);
		}
	} catch (error) {
		console.log(error);
		msg.error = true;
		msg.result = error;
	}
	res.json(msg);
}

exports.getProjectActivity = async(req,res, next)=>{
	let msg = { error: false, result : [] }
	try {
		let params = {...req.query,...req.params}
		// params.log_type, params.log_type_id, params.log_for_id
		msg = await model.getProjectActivity(null,params);
	} catch (error) {
		console.log(error);
		msg.error = true;
	}
	res.json(msg);
}

exports.getTaskFilterData = async(req,res, next)=>{
	let msg = { error: false, result : [] }
	try {
		msg = await model.getTaskFilterData(req.app.token);
	} catch (error) {
		console.log(error);
		msg.error = true;
	}
	res.json(msg);
}

exports.getGanttData = async(req,res,ext)=>{
	let msg = { error: false, result : [] }
	try {
		$project_id = 0, $group_by = "milestones", $milestone_id = 0, $user_id = 0, $status = ""
		let query = req.params;
		query.group_by = query.group_by || 'milestones';
		query.project_id = query.project_id === '0' ? null : query.project_id;
		query.milestone_id = query.milestone_id === '0' ? null : query.milestone_id;
		query.user_id = req.app.token.user_id;
		query.status = query.status === '0' ? null : query.status.split(',');
		msg = await model.getGanttData(false,query);
	} catch (error) {
		console.log(error);
		msg.error = true;
	}
	res.json(msg);
}

exports.getAttributeProject = async(req,res, next)=>{
	let msg = { error: false, result : [] }
	try {
		req.params = { ...req.params, ...req.app.token};
		msg = await model.getAttributeProject(null,req.params);
	} catch (error) {
		console.log(error);
		msg.error = true;
	}
	res.json(msg);
}

exports.getTaskComments = async(req,res, next)=>{
	let msg = { error: false, result : [] }
	try {
		msg = await model.getTaskComments(null,{ ...req.params, user: req.app.token });
	} catch (error) {
		console.log(error);
		msg.error = true;
	}
	res.json(msg);
}

exports.getTaskActivity = async(req,res, next)=>{
	let msg = { error: false, result : [] }
	try {
		let params = '';
		if(req.params.idTask){
			params = {
				log_type_id: req.params.idTask,
				log_type: 'task',
				log_for_id: req.params.idProject || 1
			};
		}
		// params.log_type, params.log_type_id, params.log_for_id
		msg = await model.getTaskActivity(null,params);
	} catch (error) {
		console.log(error);
		msg.error = true;
	}
	res.json(msg);
}

exports.storeTask = async(req,res, next)=>{
	let msg = { error: false, result : [] }
	try {
		msg = await validator.store(req.body);
		if(!msg.error){
			let err = false;
			if(msg.result.start_date) msg.result.start_date = moment(msg.result.start_date).format("YYYY-MM-DD");
			if(msg.result.deadline) msg.result.deadline = moment(msg.result.deadline).format("YYYY-MM-DD");
			msg.result.created_by = req.app.token.user_id;

			if(req.query.subtask){
				if(!msg.result.parent_task_id){
					err = true;
					msg.error = true;
					msg.result = 'Validation is failed';
				}
			}

			if(!err) msg = await model.storeTask(msg.result,req.query);
		}
	} catch (error) {
		console.log(error);
		msg.error = true;
		msg.result = error;
	}
	res.json(msg);
}

exports.editTask = async(req,res, next)=>{
	let msg = { error: false, result : [] }
	try {
		if(req.params.singleInput === 'checklist_item'){
			msg = await storeTaskChecklistItem(req, res);
		}else{
			msg = await validator.edit({ ...req.body, id: req.params.id }, req.params.singleInput);
			if(!msg.error){

				if(msg.result.start_date) msg.result.start_date = moment(msg.result.start_date).format("YYYY-MM-DD");
				if(msg.result.deadline) msg.result.deadline = moment(msg.result.deadline).format("YYYY-MM-DD");
				msg.result.created_by = req.app.token.user_id;
				msg = await model.editTask(msg.result);
			}
		}
	} catch (error) {
		console.log(error);
		msg.error = true;
		msg.result = error;
	}
	res.json(msg);
}

exports.editTaskKanban = async(req,res, next)=>{
	let msg = { error: false, result : [] }
	try {

		msg = await validator.editKanban(req.body);
		if(!msg.error){
			let state = [];
			await Promise.all(msg.result.map(async (item, index) => {
				item.created_by = req.app.token.user_id;
				msg = await model.editTask(item);
				if(msg.error){
					state.push(msg);
					return;
				}
			}));

			if(state.length){
				msg = state[0];
			}else{
				msg.result = 'success';
			}

		}
	} catch (error) {
		console.log(error);
		msg.error = true;
		msg.result = error;
	}
	res.json(msg);
}

exports.getProjectReminder = async(req,res, next)=>{
	let msg = { error: false, result : [] }
	try {
		//msg = await model.getProjectReminder({},req.params.projectId);
	} catch (error) {
		console.log(error);
		msg.error = true;
		msg.result = error;
	}
	res.json(msg);
}

exports.storeProjectReminder = async(req,res, next)=>{
	let msg = { error: false, result : [] }
	try {
		msg = await validator.storeTaskReminder({ ...req.body, created_by: req.app.token.user_id });
		if(!msg.error){
			msg = await model.storeTaskReminder(msg.result);
		}
	} catch (error) {
		console.log(error);
		msg.error = true;
		msg.result = error;
	}
	res.json(msg);
}

exports.deleteTaskReminder = async(req,res, next)=>{
	let msg = { error: false, result : [] }
	try {
		msg = await validator.deleteTaskReminder({ created_by: req.app.token.user_id, ...req.params, deleted:1, type:'reminder' });
		if(!msg.error){
			msg = await model.deleteTaskReminder(msg.result);
		}
	} catch (error) {
		console.log(error);
		msg.error = true;
		msg.result = error;
	}
	res.json(msg);
}


exports.storeTaskComment = async(req,res, next)=>{
	let msg = { error: false, result : [] }
	try {

		req = await s3Upload(req,res,'crm_task');
		msg = await validator.storeTaskComment({ ...req.body, created_by: req.app.token.user_id });
		if(!msg.error){
			msg = await model.storeTaskComment(msg.result);
		}
	} catch (error) {
		console.log(error);
		msg.error = true;
		msg.result = error;
	}
	res.json(msg);
}

// for crud pin_comment + like_comment
exports.storeTaskCommentAttribute = async(req,res, next)=>{
	let msg = { error: false, result : [] }
	try {
		msg = await validator.storeTaskCommentAttribute({ ...req.body, created_by: req.app.token.user_id, ...req.params });
		if(!msg.error){
			let status = 'store';
			if(msg.result.id) status = 'delete';

			msg = await model.storeTaskCommentAttribute(msg.result,{ status });
		}
	} catch (error) {
		console.log(error);
		msg.error = true;
		msg.result = error;
	}
	res.json(msg);
}

exports.deleteTaskComment = async(req,res, next)=>{
	let msg = { error: false, result : [] }
	try {
		msg = await validator.deleteTaskComment({ ...req.body, created_by: req.app.token.user_id });
		if(!msg.error){
			msg = await model.deleteTaskComment(msg.result);
		}
	} catch (error) {
		console.log(error);
		msg.error = true;
		msg.result = error;
	}
	res.json(msg);
}

exports.storeTaskLogged = async(req,res, next)=>{
	let msg = { error: false, result : [] }
	try {
		msg = await validator.storeTaskLogged({ ...req.body, user_id: req.app.token.user_id });		
		if(!msg.error){
			msg = await model.storeTaskLogged(msg.result);
		}
	} catch (error) {
		console.log(error);
		msg.error = true;
		msg.result = error;
	}
	res.json(msg);
}

exports.importTaskChecking = async (req, res, next)=>{
	let msg = { error: false, result : [] }
	let AsyncUploadFileData = await AsyncUploadFile(req,res,next);
	if(AsyncUploadFileData.error){
		msg = AsyncUploadFileData;
	}else{
		try {
			const companyID = AsyncUploadFileData.result.body.companyID || 45;
			const filePath = path.resolve(__dirname, '../public/company/'+AsyncUploadFileData.result.file.filename);
			msg = await model.importTask({ companyID,filePath, userId:req.app.token.user_id });
		} catch (error) {
			msg.result = error;
		}
		res.json(msg);
	}
}

exports.importTaskStore = async(req,res, next)=>{
	let msg = { error: false, result : [] }
	try {
		msg = await validator.storeImport(req.body);
		if(!msg.error){
			let err = false;
			let state = {err:[],sucess:null};
			for(let i=0; i< msg.result.data.length; i++){
				let data = msg.result.data[i];
				if(data.start_date) data.start_date = moment(data.start_date).format("YYYY-MM-DD");
				if(data.deadline) data.deadline = moment(data.deadline).format("YYYY-MM-DD");
				data.created_by = req.app.token.user_id;
				if(!err){
					result = await model.storeTask(data,req.query);
					if(msg.error){
						state.err.push(result);
					}else{
						state.sucess = result;
					}
				}
			}
		}
	} catch (error) {
		console.log(error);
		msg.error = true;
		msg.result = error;
	}
	res.json(msg);
}

exports.getProjectNotes = async(req ,res)=>{
	let msg = { error: false, result : [] }
	try {
		if(!msg.error){
			msg = await model.getProjectNotes(req.params);
		}
	} catch (error) {
		msg.error = true;
		msg.result = error;
	}
	res.json(msg);
}

async function uploadS3MultipleNotes(files, idNotes) {
	for(let i=0;i<files.length;i++) {
		let path = files[i].path;
		let path_name = 'crm_notes'
		let keyName = `${env.AWS_BUCKET_ENV}/${path_name}`;
		let params = {
			ACL: 'public-read',
			Bucket: env.AWS_BUCKET,
			Body: fs.createReadStream(path),
			Key: `${keyName}/${files[i].originalname}`,
			Metadata: {
				'Content-Type': files[i].mimetype,
				'Content-Disposition': 'inline'
			}
		};

		s3.upload(params, async (err, data) => {
			if (data) {
				fs.unlinkSync(path);
			}

			await model.postFileProjectNotes({idNotes:idNotes, file_location: data.Location, title: files[i].originalname});
		})
	}

	return true;
}

exports.postProjectNotes = async(req ,res, next)=>{
	let msg = { error: false, result : 'Success Post Project Notes' }

	let AsyncUploadFileData = await AsyncUploadMultiple(req,res,next);
	if(AsyncUploadFileData.error){
		msg = AsyncUploadFileData;
	}else{
		try {
			msg = await validator.storeProjectNotes({ ...req.body, files: req.files, created_by: req.app.token.user_id, user_id: req.app.token.user_id });
			if(!msg.error){
				msg = await model.postProjectNotes({ ...req.body, created_by: req.app.token.user_id, user_id: req.app.token.user_id });
			}
			// Insert DB & Upload to AWS S3
			await uploadS3MultipleNotes(req.files, msg.result.insertId)

		} catch (error) {
			msg.result = error;
		}
		res.json(msg);
	}
}

exports.updateProjectNotes = async(req ,res, next)=>{
	let msg = { error: false, result : 'Success Post Project Notes' }
	let AsyncUploadFileData = await AsyncUploadMultiple(req,res,next);
	if(AsyncUploadFileData.error){
		msg = AsyncUploadFileData;
	}else{
		try {
			msg = await validator.updateProjectNotes({ ...req.body, files: req.files, created_by: req.app.token.user_id, user_id: req.app.token.user_id });
			// Insert DB & Upload to AWS S3
			const response = await uploadS3MultipleNotes(req.files, req.body.id);
			
			if(!msg.error && response){
				msg = await model.updateProjectNotes({ ...req.body, created_by: req.app.token.user_id, user_id: req.app.token.user_id });
				console.log(msg, response,'ini apa?')
				res.json(msg);
			}

		} catch (error) {
			msg.result = error;
			res.json(msg)
		}
		
	}
}

exports.removeProjectNotes = async(req ,res)=>{
	let msg = { error: false, result : 'Success Remove Project Notes' }
	try {
		await model.removeProjectNotes(req.params);
		await model.removeProjectNotesFileByIdNotes(req.params);
	} catch (error) {
		msg.error = true;
		msg.result = error;
	}
	res.json(msg);
}

exports.removeProjectNotesFile = async(req ,res)=>{
	let msg = { error: false, result : 'Success Remove Project Notes File' }
	try {
		await model.removeProjectNotesFile(req.params);
	} catch (error) {
		msg.error = true;
		msg.result = error;
	}
	res.json(msg);
}
