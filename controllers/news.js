var env = require('../env.json');
const db = require('../configs/database');
var multer = require('multer');
const AWS = require('aws-sdk');
const fs = require('fs');
var moment = require('moment');
const s3 = new AWS.S3({
  accessKeyId: env.AWS_ACCESS,
  secretAccessKey: env.AWS_SECRET
});
const storage = multer.diskStorage({
	destination: (req, file, cb) => {
    cb(null, './public/news');
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname);
  }
})
let uploadImage = multer({ storage: storage }).single('image');

exports.uploadImage = (req, res) => {
        uploadImage(req, res, (err) => {
          if(!req.file) {
            res.json({ error: true, result: err });
          } else {
                  if(env.AWS_BUCKET) {
                    let path = req.file.path;
                    let newDate = new Date();
                    let keyName = `${env.AWS_BUCKET_ENV}/news`;
        
                    var params = {
                    ACL: 'public-read',
                    Bucket: env.AWS_BUCKET,
                    Body: fs.createReadStream(path),
                    Key: `${keyName}/${req.file.originalname}`
                    };
        
                    s3.upload(params, (err, data) => {
                    if (err) {
                        res.json({ error: true, msg: "Error Upload Image", result: err });
                    }
                    if (data) {
                        fs.unlinkSync(path);
        
                        let form = {
                            image: data.Location
                        }
        
                        db.query(`UPDATE news SET image = '${form.image}' WHERE id = '${req.params.id}'`, (error, result, fields) => {
                        if(error) {
                            res.json({error: true, result: error})
                        } else {
                            res.json({error: false, result: result})
                        }
                        })
        
                    }
                    })
                  }
                  else {
                      let form = {
                          image: `${env.APP_URL}/news/${req.file.filename}`
                      }
      
                      db.query(`UPDATE news SET image = '${form.image}' WHERE id = '${req.params.id}'`, (error, result, fields) => {
                          if(error) {
                              res.json({error: true, result: error})
                          } else {
                              res.json({error: false, result: result})
                          }
                      })
                  }
      
          }
        })
}
exports.create = (req, res) => {
    db.query(
        `INSERT INTO news
        (company_id, title, content, image, status, created_by, created_at)
        VALUES (?);`,
        [[req.body.company_id, req.body.title, req.body.content, req.body.image, 'Active', req.body.created_by, new Date()]],
        (error, result) => {
            if(error) {
                return res.json({error: true, result: error.message});
            }
            else{
                return res.json({error: false, result: result})
            }
        }
    );
};

exports.read = (req, res) => {
    db.query(
        `SELECT n.*, u.name AS author
        FROM news n
        JOIN user u ON u.user_id = n.created_by
        WHERE n.id = ?
        LIMIT 1;`,
        [req.params.id],
        (error, result) => {
            if(error) {
                return res.json({error: true, result: error.message});
            };
            result[0].name = unescape(result[0].name);
            return res.json({error: false, result: result[0]});
        }
    );
};

exports.browseByCompany = (req, res) => {
    db.query(
        `SELECT n.*, u.name AS author
        FROM news n
        JOIN user u ON u.user_id = n.created_by
        WHERE n.company_id=? AND n.status='Active'
        ORDER BY n.created_at DESC;`,
        [req.params.id],
        (error, result) => {
            if(error) {
                return res.json({error: true, result: error.message});
            };
            result.map(item=>{
                item.name = unescape(item.name);
            })
            return res.json({error: false, result: result});
        }
    );
};

exports.update = (req, res) => {
    db.query(
        `UPDATE news
        SET title = ?,
            content = ?
        WHERE id = ?;`,
        [req.body.title, req.body.content, req.params.id],
        (error, result) => {
            if(error) {
                return res.json({error: true, result: error.message});
            }
            else{
                return res.json({error: false, result: result})
            }
        }
    );
};


exports.delete = (req, res) => {
    db.query(
        `UPDATE news
        SET status='Inactive'
        WHERE id = ?;`,
        [req.params.id],
        (error, result) => {
            if(error) {
                return res.json({error: true, result: error.message});
            };

            return res.json({error: false, result});
        }
    );
};