var env = require('../env.json');
var conf = require('../configs/config');
var db = require('../configs/database');
var multer = require('multer');
var storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, './public/category');
  },
  filename: (req, file, cb) => {
    var filetype = '';
    if(file.mimetype === 'image/gif') {
      filetype = 'gif';
    }
    if(file.mimetype === 'image/png') {
      filetype = 'png';
    }
    if(file.mimetype === 'image/jpeg') {
      filetype = 'jpg';
    }
    cb(null, 'forum-' + Date.now() + '.' + filetype);
  }
});
var uploadCover = multer({storage: storage}).single('attachment_id');


exports.createPost = (req, res, next) => {
  let form = {
    forumId: req.body.forum_id,
    userId: req.body.user_id,
    konten: req.body.konten
  }

  db.query(`INSERT INTO forum_post (forum_id, user_id, konten, publish) VALUES ('${form.forumId}', '${form.userId}', '${form.konten}', '1')`, (error, result, fields) => {
    if(error) {
      res.json({error: true, result: error})
    } else {
      db.query(`SELECT * FROM forum_post WHERE post_id = '${result.insertId}'`, (error, result, fields) => {
        res.json({error: false, result: result[0]})
      })
    }
  })
}

exports.getPostById = (req, res, next) => {
  db.query(`SELECT * FROM forum_post WHERE post_id = '${req.params.post_id}'`, (error, result, fields) => {
    if(error) {
      res.json({error: true, result: error})
    } else {
      res.json({error: false, result: result})
    }
  })
}

exports.getPostByForum = (req, res, next) => {
  db.query(`SELECT * FROM forum_post WHERE forum_id = '${req.params.forum_id}' ORDER BY post_id ASC`, (error, result, fields) => {
    if(error) {
      res.json({error: true, result: error})
    } else {
      res.json({error: false, result: result})
    }
  })
}

exports.updateAttachmentPost = (req, res, next) => {
  uploadCover(req, res, (err) => {
    if(!req.file) {
      res.json({ error: true, result: err });
    } else {
      let form = {
        cover: `${env.APP_URL}/category/${req.file.filename}`
      }

      db.query(`UPDATE forum_post SET attachment_id = '${form.cover}' WHERE post_id = '${req.params.post_id}'`, (error, result, fields) => {
        if(error) {
          res.json({error: true, result: error})
        } else {
          res.json({error: false, result: form.cover})
        }
      })
    }
  })
}

exports.updatePost = (req, res, next) => {
  db.query(`UPDATE forum_post SET konten = '${req.body.konten}' WHERE post_id = '${req.params.post_id}'`, (error, result, fields) => {
    if(error) {
      res.json({error: true, result: error})
    } else {
      db.query(`SELECT * FROM forum_post WHERE post_id = '${req.params.post_id}'`, (error, result, fields) => {
        res.json({error: false, result: result[0]})
      })
    }
  })
}

exports.deletePost = (req, res, next) => {
  db.query(`DELETE FROM forum_post WHERE post_id = '${req.params.post_id}'`, (error, result, fields) => {
    if(error) {
      res.json({error: true, result: error})
    } else {
      res.json({error: false, result: result})
    }
  })
}
