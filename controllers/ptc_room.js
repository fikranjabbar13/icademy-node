var db = require('../configs/database');
var conf = require('../configs/config');
var env = require('../env.json');
var moment = require('moment-timezone');

exports.getAllPtc = function(req, res, next){

    // let query = `SELECT learning_ptc.ptc_id, learning_ptc.nama_ruangan, learning_ptc.waktu_mulai, learning_ptc.tanggal_mulai, learning_ptc.is_private, learning_ptc_peserta.peserta
    // FROM learning_ptc, learning_ptc_peserta
    // WHERE learning_ptc.ptc_id = learning_ptc_peserta.room_id
    // ORDER BY learning_ptc.ptc_id
    //              `;

    let query = `SELECT learning_ptc.*, learning_ptc_peserta.* FROM learning_ptc JOIN learning_ptc_peserta ON learning_ptc_peserta.room_id=learning_ptc.ptc_id`;

     db.query(query, (err, result) => {
         if(err){
              res.json({error: true, result:err});
         }else{
              res.json({error:false, result:(result.length !== 0) ? result: []});
         }
     })
}


exports.getAllPtcPeserta = function(req, res, next){

    let query = `SELECT * FROM learning_ptc_peserta`;
    db.query(query, (err, result) => {
        if(err){
             res.json({error: true, result:err});
        }else{
             res.json({error:false, result: result});
        }
    })
}

exports.getAllPesertaByPtc = (req, res) => {
  let query = `SELECT lpp.*, u.name, u.email, u.identity FROM learning_ptc_peserta lpp JOIN user u ON u.user_id = lpp.user_id WHERE lpp.room_id = ?`;
  db.query(query, [req.params.ptc_id], (err, result) => {
      if(err){
           res.json({error: true, result:err});
      }else{
           res.json({error:false, result: result});
      }
  })
}

exports.kehadiranPtc = (req, res) => {
  let dateNow = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
  let sql = `UPDATE learning_ptc_peserta SET join_at = ? WHERE user_id = ? AND room_id = ?`;
  db.query(sql, [dateNow, req.params.user_id, req.params.ptc_id], async (err, rows) => {
    if(err) res.json({ error: true, result: err })
    console.log(err)

    res.json({ error: false, result: rows })
  })
}

exports.cekKehadiran = (req, res) => {
  let sql = `SELECT room_id, user_id, join_at FROM learning_ptc_peserta WHERE user_id = ? AND room_id = ?`;
  let data = [req.params.user_id, req.params.ptc_id];
  db.query(sql, data, (err, rows) => {
    if(err) res.json({ error: true, result: err })

    res.json({ error: false, result: rows.length ? rows[0] : [] })
  })
}

exports.getPtcById = function(req, res, next){

    let query = `SELECT lp.*, u.name
      FROM learning_ptc lp
      	JOIN user u oN u.user_id = lp.moderator
      WHERE lp.ptc_id = '${req.params.ptc_id}'`;
    db.query(query, (err, result, fields) => {
        if(err){
             res.json({error: true, result: err});
        }else{
             res.json({error: false, result: (result.length !== 0) ? result[0] : [] });
        }
    })
}


exports.getPtcByCompanyId = function(req, res, next){

    let query = ` SELECT lp.*, u.name FROM learning_ptc lp JOIN user u ON u.user_id = lp.moderator WHERE lp.company_id = '${req.params.company_id}' `;
    db.query(query, async (err, result, fields) => {
        if(err){
             res.json({error: true, result:err});
        }else{

          if(result.length) {
            for(var i=0; i<result.length; i++) {
              let peserta = await db.query(`SELECT lp.*, u.name, u.email FROM learning_ptc_peserta lp JOIN user u ON u.user_id = lp.user_id WHERE lp.room_id = ?`, [result[i].ptc_id]);
              result[i].peserta = peserta;
            }
          }

          res.json({error: false, result: (result.length !== 0) ? result: [] });
        }
    })
}


exports.getPtcByUser = function(req, res, next){

    let query = `SELECT * FROM learning_ptc_peserta WHERE user_id = '${req.params.user_id}'`;
    db.query(query, (err, result, fields) =>{
        if(err){
             res.json({error: true, result: err});
        }else{
             res.json({error: false, result: (result.length !== 0) ? result: []});
        }
    })
}


exports.getPtcByModerator = function(req, res, next ){
  let query = ` SELECT lp.*, u.name FROM learning_ptc lp JOIN user u ON u.user_id = lp.moderator WHERE lp.moderator = '${req.params.moderator}' `;
  db.query(query, async (err, result, fields) => {
      if(err){
           res.json({error: true, result:err});
      }else{

        if(result.length) {
          for(var i=0; i<result.length; i++) {
            let peserta = await db.query(`SELECT lp.*, u.name, u.email FROM learning_ptc_peserta lp JOIN user u ON u.user_id = lp.user_id WHERE lp.room_id = ?`, [result[i].ptc_id]);
            result[i].peserta = peserta;
          }
        }

        res.json({error: false, result: (result.length !== 0) ? result: [] });
      }
  })
}

exports.getPtcByParents = function(req, res, next ){
  let query = `SELECT lpp.*, lp.*, u.name
    FROM learning_ptc_peserta lpp
    	JOIN learning_ptc lp ON lp.ptc_id = lpp.room_id
        LEFT JOIN user u ON u.user_id = lp.moderator
    WHERE lpp.user_id = ?`;
  db.query(query, [req.params.user_id], async (err, result, fields) => {
      if(err){
           res.json({error: true, result:err});
      }else{

        res.json({error: false, result: (result.length !== 0) ? result: [] });
      }
  })
}



exports.addLearningPtc = function(req, res, next){

    let data = {
        company_id: req.body.company_id,
        nama_ruangan: req.body.nama_ruangan,
        folder_id: req.body.folder_id,
        moderator: req.body.moderator,
        waktu_mulai: req.body.waktu_mulai,
        tanggal_mulai: req.body.tanggal_mulai,
        tag: req.body.tag,
        is_private: req.body.is_private ? 1 : 0,
        is_scheduled: req.body.is_scheduled ? 1 : 0
    }

    let query = `INSERT INTO learning_ptc
    (ptc_id, waktu_mulai, tanggal_mulai, company_id, nama_ruangan, folder_id, moderator,tag, is_private, is_scheduled ) VALUES
    (null, '${data.waktu_mulai}', '${data.tanggal_mulai}', '${data.company_id}','${data.nama_ruangan}','${data.folder_id}','${data.moderator}','${data.tag}','${data.is_private}','${data.is_scheduled}')`;

    console.log(query);

    db.query(query, (err, result) => {
        if(err){
             res.json({error: true, result: err});
        }else{
            let query = `SELECT * FROM learning_ptc WHERE ptc_id = '${result.insertId}'`;

            db.query(query, (error, result) => {
                 res.json({error: false, result: result[0] });
            })
        }
    })
}

exports.addLearningPtcPeserta = function (req, res, next){

    let data = {
        room_id: req.body.room_id,
        group_id: req.body.group_id,
        user_id: req.body.user_id,
        peserta: req.body.peserta
    }

    let query = `INSERT INTO learning_ptc_peserta
                (id, room_id, group_id, user_id, peserta) VALUES (
                null,'${data.room_id}','${data.group_id}','${data.user_id}','${data.peserta}')`;

    db.query(query, async (err, result) => {
        if(err){
            res.json({error:true, result:err});
        }else{

            // info ptc
            let getPtc = await db.query(`SELECT * FROM learning_ptc WHERE ptc_id = ?`, [data.room_id]);

            // add Notification
            let tanggal = new Date(Date.parse(getPtc[0].tanggal_mulai));
            let newtanggal = tanggal.getDate() + '/' + tanggal.getMonth() + '/' + tanggal.getFullYear();
            let valueNotif = [[data.user_id, '8', data.room_id, `Silahkan konfirmasi undangan PTC Anda "${getPtc[0].nama_ruangan}" pada tanggal ${newtanggal} jam ${getPtc[0].waktu_mulai}`, '']];
            await db.query(`INSERT INTO notification (user_id, type, activity_id, description, destination) VALUES (?)`, valueNotif);

            let query = `SELECT * FROM learning_ptc_peserta WHERE id= '${result.insertId}'`;
            db.query(query, (error, result) => {
                res.json({error: false, result: result[0] });
            })

        }
    })


}


exports.updatePtc = function(req, res, next ){

    let data = {
        company_id: `${req.body.company_id}`,
        nama_ruangan: `${req.body.nama_ruangan}`,
        folder_id: `${req.body.folder_id}`,
        moderator: `${req.body.moderator}`,
        tag: `${req.body.tag}`,
        is_private: req.body.is_private ? 1 : 0,
        is_scheduled: req.body.is_scheduled ? 1 : 0,
        waktu_mulai: req.body.waktu_mulai,
        tanggal_mulai: req.body.tanggal_mulai,
    }

    let query = `UPDATE learning_ptc SET
    company_id = '${data.company_id}',
    nama_ruangan = '${data.nama_ruangan}',
    folder_id = '${data.folder_id}',
    moderator = '${data.moderator}',
    tag = '${data.tag}',
    waktu_mulai = '${data.waktu_mulai}',
    tanggal_mulai = '${data.tanggal_mulai}',
    is_private = '${data.is_private}',
    is_scheduled = '${data.is_scheduled}'
    WHERE ptc_id = '${req.params.ptc_id}'`;

    db.query(query, (err, result, fields) => {
        if(err){
             res.json({error: true, result: err});
        }else{

            let query = `SELECT * FROM learning_ptc WHERE ptc_id = '${req.params.ptc_id}'`;
            db.query(query, (err, result, fields) => {
                 res.json({error: false, result: result[0]});
            })
        }
    })
}


exports.updatePesertaPtc = function(req, res, next){

    let data = {

        room_id: `${req.body.room_id}`,
        group_id: `${req.body.group_id}`,
        user_id: `${req.body.user_id}`,
        peserta: `${req.body.peserta}`
    }

    let query = `UPDATE learning_ptc_peserta SET

    room_id = '${data.room_id}',
    group_id = '${data.group_id}',
    user_id = '${data.user_id}',
    peserta = '${data.peserta}'
    WHERE id = '${req.params.id}'`;

    db.query(query, (err, result, fields) => {
        if(err){
             res.json({error: true, result: err});
        }else{

            let query = `SELECT * FROM learning_ptc_peserta WHERE id = '${req.params.id}'`;
            db.query(query, (err, result, fields) => {
                 res.json({error: false, result: result[0]});
            })
        }
    })
}


exports.deletePtc = function(req, res, next){

    let query = `DELETE FROM learning_ptc WHERE ptc_id = '${req.params.ptc_id}' `;
    db.query(query, (err, result) => {
        if(err){
             res.json({error: true, result:err});
        }else {
            res.json({error: false, result: result});
          }
    })
}


exports.deletePesertaPtc = function(req, res, next){

    let query = `DELETE FROM learning_ptc_peserta WHERE id = '${req.params.id}'`;
    db.query(query, (err, result) => {
        if(err){
             res.json({error: true, result:err});
        }else {
            res.json({error: false, result: result});
          }
    })

}

exports.konfirmasiPtc = (req, res) => {
  let roomId = req.params.ptc_id, userId = req.params.user_id;

  let sql = `UPDATE learning_ptc_peserta SET is_confirm = '1' WHERE user_id = ? AND room_id = ?`;
  db.query(sql, [userId, roomId], async (err, rows) => {
    if(err) res.json({ error: true, result: err})

    // add Notification
    let getUser = await db.query(`SELECT name FROM user WHERE user_id = ?`, [userId]);
    let getPtc = await db.query(`SELECT moderator FROM learning_ptc WHERE ptc_id = ?`, [roomId]);
    let valueNotif = [[getPtc[0].moderator, '0', roomId, `"${getUser[0].name}" sudah mengkonfirmasi kehadiran.`, '']];
    await db.query(`INSERT INTO notification (user_id, type, activity_id, description, destination) VALUES (?)`, valueNotif);

    res.json({ error: false, result: "Confirmed" })
  })
}
