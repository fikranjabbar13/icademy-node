const appRoot = process.cwd(),
  db = require(appRoot + '/schema');

exports.read = (req, res, next) => {
  console.log(req.params, db);
  db.transcriptsJson
    .find({
      room_name: req.params.room_name.toLowerCase(),
    })
    .exec((error, value) => {
      if (error) {
        res.json({ error: true, result: error });
      } else {
        for (let i = 0; i <= value.length - 1; i++) {
          for (let j = value[i].events.length - 1; j >= 0; j--) {
            if (value[i].events[j].event !== 'SPEECH') {
              console.log(value[i].events[j]);
              value[i].events.splice(j, 1);
            }
          }
        }

        res.json({ error: false, result: value });
      }
    });
};
