var env = require('../env.json');
var conf = require('../configs/config');
var db = require('../configs/database');

exports.create = (req, res) => {
  let form = {
    companyId: req.body.companyId,
    kelasNama: req.body.kelasNama,
    semesterId: req.body.semesterId,
    kurikulum: req.body.kurikulum,
    tahunAjaran: req.body.tahunAjaran,
    kapasitas: req.body.kapasitas,
  };

  let sql = `INSERT INTO learning_kelas (kelas_id, company_id, kelas_nama, semester_id, kurikulum, tahun_ajaran, kapasitas) VALUES (null, ?, ?, ?, ?, ?, ?)`;
  db.query(sql, [form.companyId, form.kelasNama, form.semesterId, form.kurikulum, form.tahunAjaran, form.kapasitas], (error, rows) => {
    if(error) res.json({ error: true, result: error })

    let sql = `SELECT * FROM learning_kelas WHERE kelas_id = ?`;
    db.query(sql, [rows.insertId], (error, rows) => {
      res.json({ error: false, result: rows[0] })
    })
  })
}

exports.getKelasByUserMuridId = (req, res) => {
  let { userId } = req.params;
  let sql = `
    SELECT
    	 lm.id AS murid_id, lm.nama, lm.no_induk, lm.email, lm.user_id,
       lk.kelas_id, lk.kelas_nama, lk.semester_id, lk.tahun_ajaran,
       s.semester_name
    FROM learning_kelas_murid lkm
    	JOIN learning_kelas lk ON lk.kelas_id = lkm.kelas_id
      JOIN learning_murid lm ON lm.id = lkm.murid_id
      JOIN user u ON u.user_id = lm.user_id
      JOIN semester s ON s.semester_id = lk.semester_id
    WHERE
    	lm.user_id = ?`
  let data = [userId];

  db.query(sql, data, (err, rows) => {
    if(err) {
      res.json({ error: true, result: err })
    }
    else {
      res.json({ error: false, result: rows })
    }
  })
}

exports.getByCompanyId = (req, res) => {
  let sql = `SELECT lk.*, s.semester_name AS semester FROM learning_kelas lk JOIN semester s ON s.semester_id = lk.semester_id WHERE lk.company_id = ?`;
  db.query(sql, [req.params.companyId], (error, rows) => {
    if(error) res.json({ error: true, result: error })

    res.json({ error: false, result: rows })
  })
}

exports.getBySemesterId = (req, res) => {
  let sql = `SELECT
    lk.*, s.semester_name AS semester
    FROM learning_kelas lk JOIN semester s ON s.semester_id = lk.semester_id
    WHERE lk.company_id = ? AND lk.semester_id = ?`;
  db.query(sql, [req.params.companyId, req.params.semesterId], (error, rows) => {
    if(error) res.json({ error: true, result: error })

    res.json({ error: false, result: rows })
  })
}

exports.getById = (req, res) => {
  let sql = `SELECT * FROM learning_kelas WHERE kelas_id = ?`;
  db.query(sql, [req.params.id], (error, rows) => {
    if(error) res.json({ error: true, result: error })

    res.json({ error: false, result: rows.length ? rows[0] : [] })
  })
}

exports.updateById = (req, res) => {
  let form = {
    kelasNama: req.body.kelasNama,
    semesterId: req.body.semesterId,
    kurikulum: req.body.kurikulum,
    tahunAjaran: req.body.tahunAjaran,
    kapasitas: req.body.kapasitas,
  }

  let sql = `UPDATE learning_kelas SET kelas_nama = ?, semester_id = ?, kurikulum = ?, tahun_ajaran = ?, kapasitas = ? WHERE kelas_id = ?`;
  db.query(sql, [form.kelasNama, form.semesterId, form.kurikulum, form.tahunAjaran, form.kapasitas, req.params.id], (error, rows) => {
    if(error) res.json({ error: true, result: error })

    let sql = `SELECT * FROM learning_kelas WHERE kelas_id = ?`;
    db.query(sql, [req.params.id], (error, rows) => {
      res.json({ error: false, result: rows[0] })
    })
  })
}

exports.deleteById = (req, res) => {
  let sql = `DELETE FROM learning_kelas WHERE kelas_id = ?`;
  db.query(sql, [req.params.id], (err, rows) => {
    if(err) res.json({ error: true, result: err })

    res.json({ error: false, result: rows })
  })
}
