var db = require('../configs/database');


exports.createTags = (req, res) => {
	let form = {
		project_id: req.body.project_id,
		color: req.body.color,
		label: req.body.label
	};

	let sql = `INSERT INTO project_tags (project_id, color, label) VALUES (?, ?, ?)`;
	db.query(sql, [form.project_id, form.color, form.label], (error, result) => {
		if (error) res.json({ error: true, result: error });
		res.json({ error: false, result: result })
	});
}

exports.setTags = (req, res) => {
	let form = {
		tags_id: req.body.tags_id,
		task_id: req.body.task_id
	};

	let sql = `UPDATE tasks SET tags_id='${form.tags_id}' WHERE id='${form.task_id}'`;
	db.query(sql, (error, result) => {
		if (error) res.json({ error: true, result: error });
		res.json({ error: false, result: result })
	});
}

exports.getProjectTags = (req, res) => {
	let sql = `SELECT * FROM project_tags WHERE project_id='${req.params.id}'`;
	db.query(sql, (error, result) => {
		if (error) res.json({ error: true, result: error });
		res.json({ error: false, result: result })
	});
}

exports.getProjectShare = (req, res) => {
	let sql = `SELECT s.id, u.name, u.email FROM project_share s JOIN user u ON u.user_id=s.user_id WHERE s.project_id='${req.params.id}'`;
	db.query(sql, (error, result) => {
		if (error) res.json({ error: true, result: error });
		res.json({ error: false, result: result })
	});
}

exports.deleteProjectShare = (req, res) => {
	let sql = `DELETE FROM project_share WHERE id='${req.params.id}'`;
	db.query(sql, (error, result) => {
		if (error) res.json({ error: true, result: error });
		res.json({ error: false, result: result })
	});
}

exports.addProjectShare = (req, res) => {
	let form = {
		project_id: req.body.project_id,
		email: req.body.email,
	};

	let sql = `SELECT user_id FROM user WHERE email='${form.email}' AND status='active'`;
	db.query(sql, (error, result) => {
		if (error) res.json({ error: true, result: error });

		if (result.length > 0) {
			let sql2 = `INSERT INTO project_share (project_id, user_id) VALUES (?, ?)`;
			db.query(sql2, [form.project_id, result[0].user_id], (error, result) => {
				if (error) res.json({ error: true, result: error });
				res.json({ error: false, result: 'success' })
			});
		}
		else {
			res.json({ error: false, result: 'email not found' })
		}
	});
}

exports.getUserProject = async (req, res, next) => {
	try {

		let company = await db.query(`SELECT company_id FROM files_folder WHERE id='${req.params.id}'`);
		let sql = `
		SELECT DISTINCT u.*, ifnull((select x.company_name from company x where x.company_id = u.company_id), 
		(select x.company_name from company x where x.company_id = uc.company_id)) as company_name 
		FROM user u 
		LEFT JOIN files_folder p ON p.company_id=u.company_id 
		LEFT JOIN project_share s ON s.user_id=u.user_id 
		LEFT JOIN user_company uc ON uc.user_id = u.user_id 
		WHERE ((p.id='${req.params.id}' AND u.status='active') 
		OR (s.project_id='${req.params.id}' AND u.status='active') 
		OR (uc.company_id='${company[0].company_id}' AND u.status='active')) 
		AND u.email NOT IN (SELECT email FROM training_user)`;

		db.query(sql, (error, result) => {
			if (error) res.json({ error: true, result: error });

			res.json({ error: false, result: result })
		});
	} catch (e) {
		res.json({ error: true, result: e })
	}
};
exports.getProjectCompany = (req, res, next) => {
	let sql = `SELECT DISTINCT c.company_id AS 'key', c.company_name AS label FROM user u LEFT JOIN files_folder p ON p.company_id=u.company_id LEFT JOIN project_share s ON s.user_id=u.user_id LEFT JOIN company c ON c.company_id=u.company_id WHERE (p.id IN (${req.params.id}) AND u.status='active') OR (s.project_id IN (${req.params.id}) AND u.status='active')`;
	db.query(sql, (error, result) => {
		if (error) res.json({ error: true, result: error });
		res.json({ error: false, result: result })
	});
};
exports.getProjectCompanyByUserReport = (req, res, next) => {
	let sql = `SELECT DISTINCT c.company_id AS 'key', c.company_name AS label FROM company c LEFT JOIN (SELECT t.*, gu.user_id FROM gantt_user gu LEFT JOIN gantt_tasks t ON t.id=gu.task_id) t ON t.company=c.company_id WHERE t.user_id IN (${req.params.id})`;
	db.query(sql, (error, result) => {
		if (error) res.json({ error: true, result: error });
		res.json({ error: false, result: result })
	});
};

exports.getUserProjectGantt = (req, res, next) => {
	let sql = `SELECT DISTINCT u.user_id AS id, u.name AS text FROM user u LEFT JOIN files_folder p ON p.company_id=u.company_id LEFT JOIN project_share s ON s.user_id=u.user_id WHERE ((p.id IN (${req.params.id > 0 ? req.params.id : 0}) AND u.status='active') OR (s.project_id IN (${req.params.id > 0 ? req.params.id : 0}) AND u.status='active')) AND u.email NOT IN (SELECT email FROM training_user) ORDER BY text ASC`;
	db.query(sql, (error, result) => {
		if (error) res.json({ error: true, result: error });
		res.json({ error: false, result: result })
	});
};

exports.getUserGanttByCompany = (req, res, next) => {
	let sql = `SELECT company_id FROM user WHERE user_id IN (${req.params.userId})`;
	db.query(sql, (error, resultUserCompany) => {
		if (error) res.json({ error: true, result: error });
		let sql = `SELECT DISTINCT u.user_id AS id, u.name AS text FROM user u WHERE (u.company_id='${resultUserCompany[0].company_id}' AND u.status='active') ORDER BY text ASC`;
		db.query(sql, (error, result) => {
			if (error) res.json({ error: true, result: error });
			res.json({ error: false, result: result })
		});
	});
};