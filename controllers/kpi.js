var env = require('../env.json');
var conf = require('../configs/config');
var db = require('../configs/database');

var multer = require('multer');
const storage = multer.diskStorage({
  filename: function (req, file, cb) {
    cb(null, file.originalname);
  }
})
const AWS = require('aws-sdk');
const fs = require('fs');
const s3 = new AWS.S3({
  accessKeyId: env.AWS_ACCESS,
  secretAccessKey: env.AWS_SECRET
});

var uploadLogo = multer({ storage: storage }).single('file');

exports.getByCompanyId = (req, res) => {
  let sql = `SELECT
      lk.company_id,
      lk.semester_id,
      s.semester_name AS semester,
      lk.kurikulum,
      lk.tahun_ajaran,
      lj.jadwal_id,
      lj.ruangan_id,
      rm.pengajar,
      lg.nama,
      lg.no_induk AS noinduk,
      lj.pelajaran_id,
      lp.nama_pelajaran AS pelajaran,
      lk.kelas_id,
      lk.kelas_nama AS kelas,
      lgk.file
    FROM learning_kelas lk JOIN learning_jadwal lj ON lj.kelas_id = lk.kelas_id
    	JOIN semester s ON s.semester_id = lk.semester_id
    	JOIN ruangan_mengajar rm ON rm.id = lj.ruangan_id
      JOIN learning_guru lg ON lg.id = rm.pengajar
      JOIN learning_pelajaran lp ON lp.pelajaran_id = lj.pelajaran_id
      LEFT JOIN learning_guru_kpi lgk ON lg.id = lgk.guru_id AND lgk.jadwal_id = lj.jadwal_id
    WHERE lk.company_id = ?
  `;
  let data = [req.params.companyId];

  db.query(sql, data, (err, rows) => {
    if(err) res.json({ error: true, result: err })

    res.json({ error: false, result: rows })
  })
}

exports.uploadKPI = (req, res) => {
	uploadLogo(req, res, (err) => {
    if(!req.file) {
      res.json({ error: true, result: err });
    } else {
      let path = req.file.path;
      let newDate = new Date();
      let keyName = `${env.AWS_BUCKET_ENV}/kpi`;

      var params = {
        ACL: 'public-read',
        Bucket: env.AWS_BUCKET,
        Body: fs.createReadStream(path),
        Key: `${keyName}/${req.file.originalname}`
      };

      s3.upload(params, (err, data) => {
        if (err) {
          res.json({ error: true, msg: "Error Upload KPI", result: err });
        }
        if (data) {
          fs.unlinkSync(path);

          let formData = [
            data.Location,
						req.body.guruId,
						req.body.jadwalId,
						req.body.semesterId,
						req.body.kelasId,
						req.body.pelajaranId
          ];

          let sql = `INSERT INTO learning_guru_kpi (file, guru_id, jadwal_id, semester_id, kelas_id, pelajaran_id) VALUES (?)`;

          db.query(sql, [formData], (error, result, fields) => {
            if(error) {
              res.json({error: true, result: error});
            } else {
              db.query(`SELECT * FROM learning_guru_kpi WHERE id_kpi = '${result.insertId}'`, (error, result, fields) => {
                res.json({error: false, result: result[0]});
              })
            }
          });

        }
      });
    }
  });
}

exports.get = function ( req, res) {
  let sql =   `SELECT * FROM learning_nilai WHERE company_id = ? ORDER BY huruf ASC`;
  db.query(sql, [req.params.company_id], (err, rows) => {
    if(err) throw err;
    res.json({error: false, result: rows});
  });
}


exports.create = function ( req, res){
  let form = {
    min:req.body.min,
    max: req.body.max,
    huruf: req.body.huruf,
    company_id: req.body.company_id
  }

  let sql = `INSERT INTO learning_nilai (min. max, huruf, company_id) VALUES (?)`;
  let data = [form.min, form.max, form.huruf, form.company_id]
  db.query(sql, data, (err, res) => {
    if(err) throw err;

    form.id = rows.insertId;
    res.json({ error: false, result: form})
  })
}


exports.patch = function ( req, res){
  let { min, max, huruf} = req.body;
  let { id } = req.params

  let sql = `UPDATE learning_murid SET min=?, max=?, huruf=? WHERE id = ?`;
  db.query(sql, (err, res) => {
    if(err) throw err;

    res.json({ error: false, result: { min, max , huruf, id}});
  })
}


exports.delete = function(req, res){

  let sql = `DELETE FROM learning_nilai WHERE id = ?`
  db.query(sql, [req.params.id] , (err, rows) => {
    if(err) throw err;

    res.json({ error: false, result: rows});
  })
}