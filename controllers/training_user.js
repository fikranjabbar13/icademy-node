var env = require('../env.json');
const db = require('../configs/database');
var multer = require('multer');
const AWS = require('aws-sdk');
var conf = require('../configs/config');
const moduleDB = require('../repository');
const validatorRequest = require('../validator');
const axios = require("axios");
var { randomString } = require('./helper/generate');
var { sendEmailPassword } = require('./helper/sendEmail')
const util = require('../utils/user_registration');

const fs = require('fs');
var md5 = require('md5');
const { format } = require('../configs/database');
var moment = require('moment');
const s3 = new AWS.S3({
    accessKeyId: env.AWS_ACCESS,
    secretAccessKey: env.AWS_SECRET
});
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        let paths = './public/training/user';
        if (!fs.existsSync(paths)) {
            fs.mkdirSync(paths);
        }
        cb(null, paths);
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname);
    }
})

const storageMedia = multer.diskStorage({
    destination: (req, file, cb) => {
        let paths = "./public/training/user/registration-user";
        if (!fs.existsSync(paths)) {
            fs.mkdirSync(paths);
        }
        cb(null, paths);
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname);
    }
})

let uploadImage = multer({ storage: storage }).single('image');
let uploadImageIdentity = multer({ storage: storage }).single('image');
let uploadMedia = multer({ storage: storageMedia }).single('media');

var storageEx = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, './public/training/user/excel');
    },
    filename: (req, file, cb) => {
        cb(null, 'excel-' + Date.now() + '-' + file.originalname);
    }
});
var uploadExcel = multer({ storage: storageEx }).single('file');

exports.import = (req, res) => {
    uploadExcel(req, res, (err) => {
        if (!req.file) {
            res.json({ error: true, result: err });
        } else {
            // create class Excel
            var Excel = require('exceljs');
            var wb = new Excel.Workbook();
            var path = require('path');
            var filePath = path.resolve(__dirname, '../public/training/user/excel/' + req.file.filename);

            var form = {
                company_id: req.body.company_id,
                level: req.body.level,
                status: 'active',
                created_by: req.body.created_by,
                created_at: moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
            };

            wb.xlsx.readFile(filePath).then(async function () {
                var sh = wb.getWorksheet("Sheet1");
                console.log('countRow: ', sh.rowCount);

                var tempArray = [];
                let inserted = [];
                let insertedUser = [];
                for (let i = 4; i <= sh.rowCount; i++) {
                    if (sh.getRow(i).getCell(1).value !== null) {
                        let emailParsed = typeof sh.getRow(i).getCell(13).value === 'object' && typeof sh.getRow(i).getCell(13).value !== null ? sh.getRow(i).getCell(13).value.text : sh.getRow(i).getCell(13).value;
                        // check email exists
                        let checkEmail = await db.query(`SELECT email FROM training_user WHERE email = '${emailParsed}' AND status='active'`);
                        let checkEmailUser = await db.query(`SELECT email FROM user WHERE email = '${emailParsed}' AND status='active'`);
                        let checkIdentity = await db.query(`SELECT identity FROM training_user WHERE identity = '${sh.getRow(i).getCell(6).value}' AND status='active'`);

                        if (true) {
                            if (checkEmail.length < 1 && checkEmailUser.length < 1) {
                                // check training_training_company_id
                                let training_company_id = await db.query(`SELECT id FROM training_company WHERE company_id = '${form.company_id}' AND name = '${sh.getRow(i).getCell(1).value}' AND status = 'active'`);
                                if (!training_company_id.length) {
                                    await db.query(`DELETE FROM training_user WHERE id IN (${inserted})`);
                                    await db.query(`DELETE FROM user WHERE user_id IN (${insertedUser})`);
                                    res.json({ error: true, result: `Error : failed to import row ${i}, all row has been canceled`, row: i });
                                    return true;
                                }
                                else {
                                    let generatePassword = randomString(8);
                                    let sqlString = `INSERT INTO training_user (training_company_id, name, born_place, born_date, gender, identity, tin, license_number, address, city, phone, email, initial_password, level, status, created_by, created_at, source)
                                    VALUES (?)`;

                                    db.query(sqlString,
                                        [[
                                            training_company_id[0].id,
                                            sh.getRow(i).getCell(2).value ? sh.getRow(i).getCell(2).value : '',
                                            sh.getRow(i).getCell(3).value ? sh.getRow(i).getCell(3).value : '',
                                            moment(sh.getRow(i).getCell(4).value, 'DD-MM-YYYY', true).format('YYYY-MM-DD'),
                                            sh.getRow(i).getCell(5).value ? sh.getRow(i).getCell(5).value : '',
                                            sh.getRow(i).getCell(6).value ? sh.getRow(i).getCell(6).value : '',
                                            sh.getRow(i).getCell(7).value === null ? '' : sh.getRow(i).getCell(7).value,
                                            sh.getRow(i).getCell(8).value === null ? '' : sh.getRow(i).getCell(8).value,
                                            sh.getRow(i).getCell(10).value ? sh.getRow(i).getCell(10).value : '',
                                            sh.getRow(i).getCell(11).value ? sh.getRow(i).getCell(11).value : '',
                                            sh.getRow(i).getCell(12).value ? sh.getRow(i).getCell(12).value : '',
                                            emailParsed,
                                            generatePassword,
                                            form.level,
                                            form.status,
                                            form.created_by,
                                            form.created_at,
                                            'Internal'
                                        ]],
                                        async (error, result, fields) => {
                                            if (error) {
                                                await db.query(`DELETE FROM training_user WHERE id IN (${inserted})`);
                                                await db.query(`DELETE FROM user WHERE user_id IN (${insertedUser})`);
                                                res.json({ error: true, result: `Error : failed to import row ${i}, all row has been canceled`, row: i });
                                                return true;
                                            }
                                            else {
                                                if (sh.getRow(i).getCell(8).value && sh.getRow(i).getCell(9).value) {
                                                    let form = [
                                                        training_user_id = result.insertId,
                                                        expired = moment(sh.getRow(i).getCell(9).value, 'DD-MM-YYYY', true).format('YYYY-MM-DD'),
                                                        photo = '',
                                                        license_card = '',
                                                        license_number = sh.getRow(i).getCell(8).value,
                                                        status = 'Active',
                                                        created_at = new Date()
                                                    ];
                                                    db.query(`INSERT INTO training_membership (training_user_id, expired, photo, license_card, license_number, status, created_at) VALUES(?)`, [form], (error, result) => {
                                                        if (error) {
                                                            res.json({ error: true, result: error.message })
                                                        }
                                                        else {
                                                        }
                                                    });
                                                }
                                                inserted.push(result.insertId)
                                                let checkGrup = await db.query(`SELECT grup_id FROM grup WHERE grup_name = '${form.level === 'user' ? 'User' : 'Admin'} Training' AND company_id='${form.company_id}'`);
                                                let grup_id = '';
                                                if (checkGrup.length) {
                                                    grup_id = checkGrup[0].grup_id;
                                                }
                                                else {
                                                    await db.query(`
                                                INSERT INTO grup
                                                (grup_id, company_id, grup_name, activity, course, manage_course, forum, group_meeting, manage_group_meeting)
                                                VALUES
                                                (null, '${form.company_id}', '${form.level === 'user' ? 'User' : 'Admin'} Training', '1', '1', '0', '1', '1', '0')`);
                                                    let checkGrup = await db.query(`SELECT grup_id FROM grup WHERE grup_name = '${form.level === 'user' ? 'User' : 'Admin'} Training' AND company_id='${form.company_id}'`);
                                                    grup_id = checkGrup[0].grup_id;
                                                }

                                                var formData = {
                                                    company_id: form.company_id,
                                                    branch_id: 0,
                                                    grup_id: grup_id,
                                                    group: [],
                                                    identity: sh.getRow(i).getCell(6).value,
                                                    name: sh.getRow(i).getCell(2).value,
                                                    email: emailParsed,
                                                    phone: sh.getRow(i).getCell(12).value,
                                                    address: sh.getRow(i).getCell(10).value,
                                                    password: md5(generatePassword),
                                                    level: 'client',
                                                    status: 'active',
                                                    unlimited: 1,
                                                    validity: moment(new Date()).format('YYYY-MM-DD')
                                                };
                                                // create user
                                                db.query(`INSERT INTO user
                                            (user_id, company_id, branch_id, grup_id, identity, name, email, phone, address, password, level, status, registered, unlimited, validity) VALUES (?)`,
                                                    [[null, formData.company_id, formData.branch_id, formData.grup_id, formData.identity, formData.name, formData.email,
                                                        formData.phone, formData.address, formData.password, formData.level, formData.status,
                                                        conf.dateTimeNow(), formData.unlimited, formData.validity]],
                                                    async (error, result, fields) => {
                                                        if (error) {
                                                            await db.query(`DELETE FROM training_user WHERE id IN (${inserted})`);
                                                            await db.query(`DELETE FROM user WHERE user_id IN (${insertedUser})`);
                                                            res.json({ error: true, result: `Error : failed to import row ${i}, all row has been canceled`, row: i });
                                                            return true;
                                                        }
                                                        else {
                                                            insertedUser.push(result.insertId)

                                                            // send email
                                                            let payload = {
                                                                name: formData.name,
                                                                password: generatePassword,
                                                                email: formData.email,
                                                                phone: formData.phone
                                                            };
                                                            sendEmailPassword(payload);
                                                        }
                                                    });
                                            }
                                        })
                                }

                            }
                            else {
                                if (inserted.length) {
                                    await db.query(`DELETE FROM training_user WHERE id IN (${inserted})`);
                                    await db.query(`DELETE FROM user WHERE user_id IN (${insertedUser})`);
                                }
                                res.json({ error: true, result: `Error : failed to import row ${i}, all row has been canceled`, row: i });
                            }
                        }
                        else {
                            if (inserted.length) {
                                await db.query(`DELETE FROM training_user WHERE id IN (${inserted})`);
                                await db.query(`DELETE FROM user WHERE user_id IN (${insertedUser})`);
                            }
                            res.json({ error: true, result: `Error : failed to import row ${i}, all row has been canceled`, row: i });
                        }
                    }
                    else {
                        break;
                    }
                }

                res.json({ error: false, result: 'success' });
            });

        }
    })
}

exports.uploadImage = (req, res) => {
    uploadImage(req, res, (err) => {
        if (!req.file) {
            res.json({ error: true, result: err });
        } else {
            if (env.AWS_BUCKET) {
                let path = req.file.path;
                let newDate = new Date();
                let keyName = `${env.AWS_BUCKET_ENV}/training/user`;

                var params = {
                    ACL: 'public-read',
                    Bucket: env.AWS_BUCKET,
                    Body: fs.createReadStream(path),
                    Key: `${keyName}/${Date.now()}-${req.file.originalname}`,
                    ContentDisposition: 'inline',
                    ContentType: req.file.mimetype
                };

                s3.upload(params, (err, data) => {
                    if (err) {
                        res.json({ error: true, msg: "Error Upload Image", result: err });
                    }
                    if (data) {
                        fs.unlinkSync(path);

                        let form = {
                            image: data.Location
                        }

                        db.query(`UPDATE training_user SET image = '${form.image}' WHERE id = '${req.params.id}'`, (error, result, fields) => {
                            if (error) {
                                res.json({ error: true, result: error })
                            } else {
                                res.json({ error: false, result: result })
                            }
                        })

                    }
                })
            }
            else {
                let form = {
                    image: `${env.APP_URL}/training/user/${req.file.filename}`
                }

                db.query(`UPDATE training_user SET image = '${form.image}' WHERE id = '${req.params.id}'`, (error, result, fields) => {
                    if (error) {
                        res.json({ error: true, result: error })
                    } else {
                        res.json({ error: false, result: result })
                    }
                })
            }

        }
    })
}
exports.uploadImageIdentity = (req, res) => {
    uploadImageIdentity(req, res, (err) => {
        if (!req.file) {
            res.json({ error: true, result: err });
        } else {
            if (env.AWS_BUCKET) {
                let path = req.file.path;
                let newDate = new Date();
                let keyName = `${env.AWS_BUCKET_ENV}/training/user/identity`;

                var params = {
                    ACL: 'public-read',
                    Bucket: env.AWS_BUCKET,
                    Body: fs.createReadStream(path),
                    Key: `${keyName}/${Date.now()}-${req.file.originalname}`,
                    ContentDisposition: 'inline',
                    ContentType: req.file.mimetype
                };

                s3.upload(params, (err, data) => {
                    if (err) {
                        res.json({ error: true, msg: "Error Upload Image", result: err });
                    }
                    if (data) {
                        fs.unlinkSync(path);

                        let form = {
                            image: data.Location
                        }

                        db.query(`UPDATE training_user SET identity_image = '${form.image}' WHERE id = '${req.params.id}'`, (error, result, fields) => {
                            if (error) {
                                res.json({ error: true, result: error })
                            } else {
                                res.json({ error: false, result: result })
                            }
                        })

                    }
                })
            }
            else {
                let form = {
                    image: `${env.APP_URL}/training/user/identity/${req.file.filename}`
                }

                db.query(`UPDATE training_user SET identity_image = '${form.image}' WHERE id = '${req.params.id}'`, (error, result, fields) => {
                    if (error) {
                        res.json({ error: true, result: error })
                    } else {
                        res.json({ error: false, result: result })
                    }
                })
            }

        }
    })
}
exports.create = async (req, res) => {
    let checkEmail = await db.query(`SELECT email FROM training_user WHERE email = '${req.body.email}' AND status='active'`);
    let checkEmailUser = await db.query(`SELECT email FROM user WHERE email = '${req.body.email}' AND status='active'`);
    console.log(checkEmail, checkEmailUser)

    let checkIdentity = await db.query(`SELECT identity FROM training_user WHERE identity = '${req.body.identity}' AND status='active'`);

    if (true) {
        if (checkEmail.length < 1 && checkEmailUser.length < 1) {
            let generatePassword = randomString(8);
            db.query(
                `INSERT INTO training_user
                    (tag,training_company_id, name, born_place, born_date, gender, identity, tin, address, city, phone, email, level, status, license_number, initial_password, created_by, created_at, source)
                VALUES (?);`,
                [[req.body.tag,req.body.training_company_id, req.body.name, req.body.born_place, req.body.born_date, req.body.gender, req.body.identity, req.body.tin, req.body.address, req.body.city, req.body.phone, req.body.email, req.body.level, 'active', req.body.license_number, generatePassword, req.body.created_by, new Date(), 'Internal']],
                async (error, resultUserTraining) => {
                    if (error) {
                        return res.json({ error: true, result: error.message });
                    }
                    else {
                        if (req.body.license_number && req.body.expired) {
                            let form = [
                                training_user_id = resultUserTraining.insertId,
                                expired = moment(new Date(req.body.expired)).format('YYYY-MM-DD'),
                                photo = '',
                                license_card = '',
                                license_number = req.body.license_number,
                                status = 'Active',
                                created_at = new Date()
                            ];
                            db.query(`INSERT INTO training_membership (training_user_id, expired, photo, license_card, license_number, status, created_at) VALUES(?)`, [form], (error, result) => {
                                if (error) {
                                    res.json({ error: true, result: error.message })
                                }
                                else {
                                }
                            });
                        }
                        let company_id = await db.query(`SELECT company_id FROM training_company WHERE id='${req.body.training_company_id}'`);
                        let checkGrup = await db.query(`SELECT grup_id FROM grup WHERE grup_name = '${req.body.level === 'user' ? 'User' : 'Admin'} Training' AND company_id='${company_id[0].company_id}'`);
                        let grup_id = '';
                        if (checkGrup.length) {
                            grup_id = checkGrup[0].grup_id;
                        }
                        else {
                            await db.query(`
                                INSERT INTO grup
                                (grup_id, company_id, grup_name, activity, course, manage_course, forum, group_meeting, manage_group_meeting)
                                VALUES
                                (null, '${company_id[0].company_id}', '${req.body.level === 'user' ? 'User' : 'Admin'} Training', '1', '1', '0', '1', '1', '0')`);
                            let checkGrup = await db.query(`SELECT grup_id FROM grup WHERE grup_name = '${req.body.level === 'user' ? 'User' : 'Admin'} Training' AND company_id='${company_id[0].company_id}'`);
                            grup_id = checkGrup[0].grup_id;
                        }

                        var formData = {
                            company_id: company_id[0].company_id,
                            branch_id: 0,
                            grup_id: grup_id,
                            group: [],
                            identity: req.body.identity,
                            name: req.body.name,
                            email: req.body.email,
                            phone: req.body.phone,
                            address: req.body.address,
                            password: md5(generatePassword),
                            level: 'client',
                            status: 'active',
                            unlimited: 1,
                            validity: moment(new Date()).format('YYYY-MM-DD')
                        };
                        // create user
                        db.query(`INSERT INTO user
                            (user_id, company_id, branch_id, grup_id, identity, name, email, phone, address, password, level, status, registered, unlimited, validity) VALUES (?)`,
                            [[null, formData.company_id, formData.branch_id, formData.grup_id, formData.identity, formData.name, formData.email,
                                formData.phone, formData.address, formData.password, formData.level, formData.status,
                                conf.dateTimeNow(), formData.unlimited, formData.validity]],
                            (error, result, fields) => {
                                if (error) {
                                    res.json({ error: true, result: error });
                                } else {
                                    // send email
                                    let payload = {
                                        name: formData.name,
                                        password: generatePassword,
                                        email: formData.email,
                                        phone: formData.phone
                                    };
                                    sendEmailPassword(payload);

                                    db.query(`INSERT INTO user_setting (setting_id, user_id) VALUES (null, '${result.insertId}')`);

                                    for (let i = 0; i < formData.group.length; i++) {
                                        db.query(`INSERT INTO user_group (id, user_id, branch_id) VALUES (null, '${result.insertId}', '${formData.group[i]}')`);
                                    }
                                    db.query(`SELECT * FROM user WHERE user_id = '${result.insertId}'`, (error, result, fields) => {
                                        return res.json({ error: false, result: resultUserTraining })
                                    })
                                }
                            });
                    }
                }
            );
        }
        else {
            return res.json({ error: true, result: 'Email already exists' });
        }
    }
    else {
        return res.json({ error: true, result: 'Identity Number already exists' });
    }
};
exports.createInactive = async (req, res) => {
    let checkEmail = await db.query(`SELECT email FROM training_user WHERE email = '${req.body.email}' AND status='active'`);
    let checkEmailUser = await db.query(`SELECT email FROM user WHERE email = '${req.body.email}' AND status='active'`);
    let checkIdentity = await db.query(`SELECT identity FROM training_user WHERE identity = '${req.body.identity}' AND status='active'`);

    if (true) {
        if (checkEmail.length < 1 && checkEmailUser.length < 1) {
            let generatePassword = randomString(8);
            db.query(
                `INSERT INTO training_user
                    (tag,training_company_id, name, born_place, born_date, gender, identity, tin, address, city, phone, email, level, status, license_number, initial_password, created_by, created_at, source)
                VALUES (?);`,
                [[req.body.tag,req.body.training_company_id, req.body.name, req.body.born_place, req.body.born_date, req.body.gender, req.body.identity, req.body.tin, req.body.address, req.body.city, req.body.phone, req.body.email, req.body.level, 'inactive', req.body.license_number, generatePassword, req.body.created_by, new Date(), 'Registration']],
                async (error, resultUserTraining) => {
                    if (error) {
                        return res.json({ error: true, result: error.message });
                    }
                    else {
                        if (req.body.license_number && req.body.expired) {
                            let form = [
                                training_user_id = resultUserTraining.insertId,
                                expired = moment(new Date(req.body.expired)).format('YYYY-MM-DD'),
                                photo = '',
                                license_card = '',
                                license_number = req.body.license_number,
                                status = 'Inactive',
                                created_at = new Date()
                            ];
                            db.query(`INSERT INTO training_membership (training_user_id, expired, photo, license_card, license_number, status, created_at) VALUES(?)`, [form], (error, result) => {
                                if (error) {
                                    res.json({ error: true, result: error.message })
                                }
                                else {
                                }
                            });
                        }
                        let company_id = await db.query(`SELECT company_id FROM training_company WHERE id='${req.body.training_company_id}'`);
                        let checkGrup = await db.query(`SELECT grup_id FROM grup WHERE grup_name = '${req.body.level === 'user' ? 'User' : 'Admin'} Training' AND company_id='${company_id[0].company_id}'`);
                        let grup_id = '';
                        if (checkGrup.length) {
                            grup_id = checkGrup[0].grup_id;
                        }
                        else {
                            await db.query(`
                                INSERT INTO grup
                                (grup_id, company_id, grup_name, activity, course, manage_course, forum, group_meeting, manage_group_meeting)
                                VALUES
                                (null, '${company_id[0].company_id}', '${req.body.level === 'user' ? 'User' : 'Admin'} Training', '1', '1', '0', '1', '1', '0')`);
                            let checkGrup = await db.query(`SELECT grup_id FROM grup WHERE grup_name = '${req.body.level === 'user' ? 'User' : 'Admin'} Training' AND company_id='${company_id[0].company_id}'`);
                            grup_id = checkGrup[0].grup_id;
                        }

                        var formData = {
                            company_id: company_id[0].company_id,
                            branch_id: 0,
                            grup_id: grup_id,
                            group: [],
                            identity: req.body.identity,
                            name: req.body.name,
                            email: req.body.email,
                            phone: req.body.phone,
                            address: req.body.address,
                            password: md5(generatePassword),
                            level: 'client',
                            status: 'pasive',
                            unlimited: 1,
                            validity: moment(new Date()).format('YYYY-MM-DD')
                        };
                        // create user
                        db.query(`INSERT INTO user
                            (user_id, company_id, branch_id, grup_id, identity, name, email, phone, address, password, level, status, registered, unlimited, validity) VALUES (?)`,
                            [[null, formData.company_id, formData.branch_id, formData.grup_id, formData.identity, formData.name, formData.email,
                                formData.phone, formData.address, formData.password, formData.level, formData.status,
                                conf.dateTimeNow(), formData.unlimited, formData.validity]],
                            (error, result, fields) => {
                                if (error) {
                                    res.json({ error: true, result: error });
                                } else {
                                    // send email
                                    let payload = {
                                        name: formData.name,
                                        password: generatePassword,
                                        email: formData.email,
                                        phone: formData.phone
                                    };
                                    // sendEmailPassword(payload);

                                    db.query(`INSERT INTO user_setting (setting_id, user_id) VALUES (null, '${result.insertId}')`);

                                    for (let i = 0; i < formData.group.length; i++) {
                                        db.query(`INSERT INTO user_group (id, user_id, branch_id) VALUES (null, '${result.insertId}', '${formData.group[i]}')`);
                                    }
                                    db.query(`SELECT * FROM user WHERE user_id = '${result.insertId}'`, (error, result, fields) => {
                                        return res.json({ error: false, result: resultUserTraining })
                                    })
                                }
                            });
                    }
                }
            );
        }
        else {
            return res.json({ error: true, result: 'Email already exists' });
        }
    }
    else {
        return res.json({ error: true, result: 'Identity Number already exists' });
    }
};

exports.read = (req, res) => {
    db.query(
        `SELECT tu.*, c.name AS training_company_name, c.image AS training_company_image,
            (SELECT t.name FROM training_exam_result r JOIN training_exam e ON e.id = r.exam_id JOIN training_licenses_type t ON t.id = e.licenses_type_id WHERE r.training_user_id = '${req.params.id}' ORDER BY r.created_at DESC LIMIT 1) AS latest_license
        FROM training_user tu
        LEFT JOIN training_company c ON c.id = tu.training_company_id
        WHERE tu.id = ?
        LIMIT 1;`,
        [req.params.id],
        (error, result) => {
            if (error) {
                return res.json({ error: true, result: error.message });
            };

            result[0].born_date = moment(new Date(result[0].born_date)).format('YYYY-MM-DD');
            return res.json({ error: false, result: result[0] });
        }
    );
};

exports.readByUserId = (req, res) => {
    db.query(
        `SELECT tu.*, c.name AS training_company_name, c.image AS training_company_image,
            (SELECT t.name FROM training_exam_result r JOIN training_exam e ON e.id = r.exam_id JOIN training_licenses_type t ON t.id = e.licenses_type_id JOIN training_user tu ON tu.id = r.training_user_id JOIN user u ON u.email = tu.email WHERE u.user_id = '${req.params.id}' AND tu.status='active' ORDER BY r.created_at DESC LIMIT 1) AS latest_license
        FROM training_user tu
        LEFT JOIN training_company c ON c.id = tu.training_company_id
        LEFT JOIN user u ON u.email = tu.email
        WHERE u.user_id = ?
        AND tu.status='active'
        LIMIT 1;`,
        [req.params.id],
        (error, result) => {
            if (error) {
                return res.json({ error: true, result: error.message });
            };

            if (result.length) {
                result[0].born_date = moment(new Date(result[0].born_date)).format('YYYY-MM-DD');
                return res.json({ error: false, result: result[0] });
            } else {
                return res.json({ error: true, result: result });
            }
        }
    );
};

exports.browseByCompany = (req, res) => {
    db.query(
        `SELECT tu.*, tc.name AS company
        FROM training_user tu
        JOIN training_company tc ON tc.id = tu.training_company_id
        JOIN company c ON c.company_id = tc.company_id
        WHERE c.company_id=? AND tu.status='Active' AND level=? ORDER BY tu.created_at DESC;`,
        [req.params.id, req.params.level],
        (error, result) => {
            if (error) {
                return res.json({ error: true, result: error.message });
            };

            return res.json({ error: false, result: result });
        }
    );
};
exports.browseByCompanyArchived = (req, res) => {
    db.query(
        `SELECT tu.*, tc.name AS company
        FROM training_user tu
        JOIN training_company tc ON tc.id = tu.training_company_id
        JOIN company c ON c.company_id = tc.company_id
        WHERE c.company_id=? AND tu.status!='Active' AND level=? ORDER BY tu.created_at DESC;`,
        [req.params.id, req.params.level],
        (error, result) => {
            if (error) {
                return res.json({ error: true, result: error.message });
            };

            return res.json({ error: false, result: result });
        }
    );
};

exports.browseByTrainingCompany = (req, res) => {
    db.query(
        `SELECT tu.*, tc.name AS company
        FROM training_user tu
        JOIN training_company tc ON tc.id = tu.training_company_id
        WHERE tu.training_company_id=? AND tu.status='Active' AND level=? ORDER BY tu.created_at DESC;`,
        [req.params.id, req.params.level],
        (error, result) => {
            if (error) {
                return res.json({ error: true, result: error.message });
            };

            return res.json({ error: false, result: result });
        }
    );
};
exports.browseByTrainingCompanyArchived = (req, res) => {
    db.query(
        `SELECT tu.*, tc.name AS company
        FROM training_user tu
        JOIN training_company tc ON tc.id = tu.training_company_id
        WHERE tu.training_company_id=? AND tu.status!='Active' AND level=? ORDER BY tu.created_at DESC;`,
        [req.params.id, req.params.level],
        (error, result) => {
            if (error) {
                return res.json({ error: true, result: error.message });
            };

            return res.json({ error: false, result: result });
        }
    );
};

exports.update = async (req, res) => {
    let currentEmail = await db.query(`SELECT email, training_company_id FROM training_user WHERE status='active' AND id='${req.params.id}'`);
    let checkEmail = await db.query(`SELECT email FROM training_user WHERE email = '${req.body.email}' AND status='active' AND id!='${req.params.id}'`);
    let checkEmailUser = await db.query(`SELECT email FROM user WHERE email = '${req.body.email}' AND status='active'`);

    if ((currentEmail[0].email === req.body.email) || (currentEmail[0].email !== req.body.email && checkEmail.length != 1 && checkEmailUser.length != 1)) {
        if (currentEmail[0].email !== req.body.email) {
            await db.query(`UPDATE user SET email='${req.body.email}' WHERE email='${currentEmail[0].email}'`);
        }
        db.query(
            `UPDATE training_user
            SET 
                tag = ?,
                training_company_id = ?,
                name = ?,
                born_place = ?,
                born_date = ?,
                gender = ?,
                identity = ?,
                tin = ?,
                license_number = ?,
                address = ?,
                city = ?,
                phone = ?,
                email = ?
            WHERE id = ?;`,
            [req.body.tag, req.body.training_company_id, req.body.name, req.body.born_place, req.body.born_date, req.body.gender, req.body.identity, req.body.tin, req.body.license_number, req.body.address, req.body.city, req.body.phone, req.body.email, req.params.id],
            (error, result) => {
                if (error) {
                    return res.json({ error: true, result: error });
                }
                else {
                    if (currentEmail[0].training_company_id !== req.body.training_company_id) {
                        db.query(`INSERT INTO training_user_log (training_user_id, company_from, company_to, created_at, created_by) VALUES (?)`, [[req.params.id, currentEmail[0].training_company_id, req.body.training_company_id, new Date(), req.body.created_by]],
                            (error, result) => {
                                if (error) {
                                    return res.json({ error: true, result: error });
                                }
                                else {
                                    return res.json({ error: false, result: result })
                                }
                            }
                        );
                    }
                    else {
                        return res.json({ error: false, result: result })
                    }
                }
            }
        );
    }
    else {
        return res.json({ error: true, result: 'Email already exists' });
    }
};

exports.updateStatusTrainingUserBulk = async (req, res) => {
    try {

        //Validate Request 
		const retrieveValidRequest = await validatorRequest.validateUpdateStatusUserBulkRequest(req.body);
	
        /**
         * Bulk Training User (active, inactive)
         * Bulk User (active, pasive)
         * Bulk Membership User (Active, Inactive)
         */
        await moduleDB.updateStatusBulkTrainingUser(retrieveValidRequest);
        await moduleDB.updateStatusBulkUser({...retrieveValidRequest, status: retrieveValidRequest.status === 'inactive' ? 'pasive' : 'active'});
        await moduleDB.updateStatusBulkMembershipUser({...retrieveValidRequest, status: retrieveValidRequest.status === 'inactive' ? 'Inactive' : 'Active'});

        //Activate Status Only Send Email Password
        if(retrieveValidRequest.status === 'active') {
            let listUserSource = await moduleDB.getSourceDataTrainingUser(retrieveValidRequest);

            if (listUserSource.length) {
                listUserSource.forEach((datauser) => {
                    let payload = {
                        name: datauser.name,
                        password: datauser.initial_password,
                        email: datauser.email,
                        phone: datauser.phone
                    };
                    sendEmailPassword(payload);
                })
            }
        }

		res.json({ error: false, result: 'Success Updated Status User' })
    } catch (error) {
        res.json({ error: true, result: error.message })
    }
};


exports.delete = (req, res) => {
    db.query(
        `UPDATE training_user
        SET status='inactive'
        WHERE id = ?;`,
        [req.params.id, req.params.id],
        (error, result) => {
            if (error) {
                return res.json({ error: true, result: error.message });
            };

            db.query(
                `UPDATE user
                INNER JOIN training_user ON user.email = training_user.email
                SET user.status = 'pasive'
                WHERE training_user.id = ?;`,
                [req.params.id],
                (error, result) => {
                    if (error) {
                        return res.json({ error: true, result: error.message });
                    };

                    db.query(
                        `UPDATE training_membership
                        SET status='Inactive'
                        WHERE training_user_id = ?;`,
                        [req.params.id],
                        (error, result) => {
                            if (error) {
                                return res.json({ error: true, result: error.message });
                            };

                            return res.json({ error: false, result });
                        }
                    );
                }
            );
        }
    );
};

exports.activate = (req, res) => {
    db.query(
        `UPDATE training_user
        SET status='active'
        WHERE id = ?;`,
        [req.params.id],
        (error, result) => {
            if (error) {
                return res.json({ error: true, result: error.message });
            };

            db.query(
                `UPDATE user
                INNER JOIN training_user ON user.email = training_user.email
                SET user.status = 'active'
                WHERE training_user.id = ?;`,
                [req.params.id],
                (error, result) => {
                    if (error) {
                        return res.json({ error: true, result: error.message });
                    };

                    db.query(
                        `UPDATE training_membership
                        SET status='Active'
                        WHERE training_user_id = ?;`,
                        [req.params.id],
                        async (error, result) => {
                            if (error) {
                                return res.json({ error: true, result: error.message });
                            };

                            let checkSource = await db.query(`SELECT source, name, initial_password, email, phone FROM training_user WHERE id=?`, [req.params.id]);
                            if (checkSource[0].source === 'Registration') {
                                let payload = {
                                    name: checkSource[0].name,
                                    password: checkSource[0].initial_password,
                                    email: checkSource[0].email,
                                    phone: checkSource[0].phone
                                };
                                sendEmailPassword(payload);
                                return res.json({ error: false, result });
                            }
                            else {
                                return res.json({ error: false, result });
                            }
                        }
                    );
                }
            );
        }
    );
};

exports.getUserForWebinar = (req, res, next) => {
    let sql = `SELECT u.* FROM user u WHERE u.status='active' AND u.company_id = '${req.params.id}'`;
    db.query(sql, (error, result) => {
        if (error) res.json({ error: true, result: error });
        res.json({ error: false, result: result })
    });
};

exports.changePassword = (req, res, next) => {
    let sql = `UPDATE user
    INNER JOIN training_user ON user.email = training_user.email
    SET user.password = '${md5(req.body.password)}'
    WHERE training_user.id = '${req.body.training_user_id}';`;
    db.query(sql, (error, result) => {
        if (error) res.json({ error: true, result: error });
        res.json({ error: false, result: result })
    });
};

exports.history = (req, res, next) => {
    let sql = `SELECT l.*, c.name AS source, c2.name AS destination, u.name AS creator
    FROM training_user_log l
    LEFT JOIN training_company c ON c.id=l.company_from
    LEFT JOIN training_company c2 ON c2.id=l.company_to
    LEFT JOIN user u ON u.user_id = l.created_by
    WHERE l.training_user_id = '${req.params.id}'
    ORDER BY created_at ASC`;
    db.query(sql, (error, result) => {
        if (error) res.json({ error: true, result: error });
        res.json({ error: false, result: result })
    });
};

exports.userRegistration = async (req, res) => {
    try {
        const validRequest = await validatorRequest.validatePostRegistrationForm(req.body);
        let retrieveDetailRegistrationSchema = await moduleDB.getTrainingRegistrationSchemaByIdTrainingCompany({idTrainingCompany: req.body.idTrainingCompany});
        if(retrieveDetailRegistrationSchema.length) {
            retrieveDetailRegistrationSchema = await util.FuncCheckRegistrationSchema(retrieveDetailRegistrationSchema);
            if(retrieveDetailRegistrationSchema[0].isRegistrationClosed) {
                res.json({ error: true, result: 'Registration period has closed / Quota exceeded limit...' });
            }else {
                // Period
                let period = null;
                if(retrieveDetailRegistrationSchema[0].close_registration){
                    period = moment(retrieveDetailRegistrationSchema[0].close_registration).format('YYYY-MM-DD');
                };
                
                const response = await moduleDB.createFormRegistrationUser({...validRequest, period: period});
                const getIDCompany = await moduleDB.getIDCompanyByTrainingCompany(validRequest);
        
                // Send Notification
                if(getIDCompany.length) {
                    const dtoAdminCompany = getIDCompany[0];
                    const getAdminCompany = await moduleDB.getAdminCompany(dtoAdminCompany);
                    const getAdminTraining = await moduleDB.getAdminTrainingByIDCompany(dtoAdminCompany);
                    const allAdminAndAdminTraining = getAdminCompany.concat(getAdminTraining);
                    const getAllUsers = await moduleDB.readAllRegistrationUserBySuperAdmin(dtoAdminCompany);
        
                    if(getAllUsers.length) {
                        const now = moment(new Date());
                        const end = moment(getAllUsers[0].created_at);
                        const duration = moment.duration(now.diff(end));
                        const hours = Math.round(duration.asHours());
                        const sendNotif = {
                            type: 13,
                            company_id: getIDCompany[0].idCompany,
                            user_id: allAdminAndAdminTraining.map(data => data.user_id),
                            activity_id: 1,
                            desc: `You have ${getAllUsers.length} Pending Users, Please Menu Registration User.`,
                            dest: `${env.WEB_URL}/training/list-registration-user`,
                          };
                        const confAxios = { headers: { "Authorization": req.headers.authorization } };
                        let respData = await axios.post(`${env.APP_URL}/v1/notification/broadcast-bulk/private`, sendNotif, confAxios);
                    }  
                };
        
                res.json({ error: false, result: 'Success Create User Registration.', insertId: response.insertId });
            }
        }else {
            throw { message: 'Error on Get Schema. Please Save Your Schema.'};
        };
    } catch (error) {
        res.json({ error: true, result: error.message });
    }
}

exports.readUserRegistrationByTrainingCompany = async (req, res) => {
    try {
        const isUserSuperAdmin = req.app.token.level == 'superadmin';
        const isAdminCompany = req.app.token.level == 'admin';
        let response = [];
        if((isUserSuperAdmin || isAdminCompany)){
            const validRequest = await validatorRequest.validateReadRegistrationUserByTrainingCompany({...req.params, ...req.query});
            if(validRequest.isTrainingCompany){
                // ID Company disini sebagai ID Training Company masuk logic sini ketika filternya menunjukan berdasarkan training company
                response = await moduleDB.readRegistrationUserByTrainingCompany({idTrainingCompany: validRequest.idCompany});
            }else {
                response = await moduleDB.readAllRegistrationUserBySuperAdmin({idCompany: validRequest.idCompany});
            }
        }else{
            response = await moduleDB.readRegistrationUserByTrainingCompany({idTrainingCompany: req.app.token.training_company_id});
        }
        
        res.json({ error: false, result: response });
    } catch (error) {
        res.json({ error: true, result: error.message });
    }
}

exports.readUserRegistrationByIdTraining = async (req, res) => {
    try {

        let response = [];
        if(req.params.idTrainingUser){
            response = await moduleDB.readDetailRegistrationUserByIdTraining({idTrainingUser : req.params.idTrainingUser});
        }
        
        res.json({ error: false, result: response });
    } catch (error) {
        res.json({ error: true, result: error.message });
    }
}

exports.activateUserRegistration = async (req, res) => {
    try {
        const validRequest = await validatorRequest.validateActivateUserByIdUserRegistration(req.body);
        // return console.log(validRequest,'berak?')
        const retreiveDetailUserRegistration = await moduleDB.retrieveUserRegistrationDetailById(validRequest);
        let responError = [];
        if(retreiveDetailUserRegistration.length){
            for(let i=0;i<retreiveDetailUserRegistration.length;i++) {
                const detailDataForm = JSON.parse(retreiveDetailUserRegistration[i].data);
                const primaryDataUserRegistration = {
                    training_company_id: retreiveDetailUserRegistration[i].training_company_id,
                    name: detailDataForm.name || '-',
                    born_place: detailDataForm.born_place || '-',
                    born_date: detailDataForm.born_date || moment().format('YYYY-MM-DD'),
                    gender: detailDataForm.gender || '-',
                    identity: detailDataForm.identity || '-',
                    tin: detailDataForm.tin || '-',
                    license_number: detailDataForm.license_number || '-',
                    address: detailDataForm.address || '-',
                    city: detailDataForm.city || '-',
                    phone: detailDataForm.phone || '-',
                    email: detailDataForm.email || '-',
                    level: 'user',
                    created_by: req.app.token.user_id
                }
                const confAxios = { headers: { "Authorization": req.headers.authorization } };
                console.log(primaryDataUserRegistration,'???');
                
                let respData = await axios.post(`${env.APP_URL}/v2/training/user`, primaryDataUserRegistration, confAxios);
                if(respData.data.error){
                    responError.push({ message: (`${detailDataForm.email} ${respData.data.result}`) || (`${detailDataForm.email} Error on Process.. Please try again...`) })
                    // throw {message: respData.data.result || 'Error on Process.. Please try again...'}
                }else{
                    const responseUpdate = await moduleDB.updateStatusUserRegistrationById({idUserRegistration: retreiveDetailUserRegistration[i].id});
                }
            }
        }

        if(responError.length){
            res.json({ error: true, result: responError });
        }else{
            res.json({ error: false, result: 'Success Create Form Registration.' });
        }
    } catch (error) {
        res.json({ error: true, result: error.message });
    }
}

exports.removeUserRegistration = async (req, res) => {
    try {
        const validRequest = await validatorRequest.validateActivateUserByIdUserRegistration(req.body);
        const retreiveDetailUserRegistration = await moduleDB.updateStatusToRemovedUserRegistration(validRequest);
        res.json({ error: false, result: 'Success Remove User..' });
       
    } catch (error) {
        res.json({ error: true, result: error.message });
    }
}

exports.uploadMedia = (req, res) => {
    uploadMedia(req, res, (err) => {
        console.log(req.file,'???')
        if (!req.file) {
            res.json({ error: true, result: err });
        } else {
            if (env.AWS_BUCKET) {
                
                let path = req.file.path;
                let newDate = new Date();
                let keyName = `${env.AWS_BUCKET_ENV}/training/user/registration/media`;

                var params = {
                    ACL: 'public-read',
                    Bucket: env.AWS_BUCKET,
                    Body: fs.createReadStream(path),
                    Key: `${keyName}/${Date.now()}-${req.file.originalname}`,
                    ContentDisposition: 'inline',
                    ContentType: req.file.mimetype
                };

                s3.upload(params, (err, data) => {
                    console.log(err, data,'???')
                    if (err) {
                        res.json({ error: true, msg: "Error Upload Media", result: err });
                    }
                    if (data) {
                        fs.unlinkSync(path);

                        let form = {
                            image: data.Location,
                            type: ''
                        }

                        switch (req.file.mimetype) {
                            case 'application/pdf': form.type = 'PDF'; break;
                            case 'application/vnd.ms-powerpoint': form.type = 'PowerPoint'; break;
                            case 'application/vnd.openxmlformats-officedocument.presentationml.presentation': form.type = 'PowerPoint'; break;
                            case 'application/vnd.ms-excel': form.type = 'Excel'; break;
                            case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet': form.type = 'Excel'; break;
                            case 'image/jpeg': form.type = 'Image'; break;
                            case 'image/png': form.type = 'Image'; break;
                            case 'video/mp4': form.type = 'Video'; break;
                            case 'audio/mp4': form.type = 'Audio'; break;
                            case 'video/x-msvideo': form.type = 'Video'; break;
                            case 'application/msword': form.type = 'Word'; break;
                            default: form.type = 'Others'
                        }

                        db.query(`INSERT INTO training_registration_media (name, type, url) VALUES('${req.file.originalname}','${form.type}','${form.image}')`, async (error, result, fields) => {
                            console.log(err, result,'mencret?')
                            if (error) {
                                res.json({ error: true, result: error })
                            } else {
                                let mediaUploaded = await db.query(`SELECT * FROM training_registration_media WHERE id='${result.insertId}'`);
                                res.json({ error: false, result: mediaUploaded[0] })
                            }
                        })

                    }
                })
            }
            else {
                let form = {
                    image: `${env.APP_URL}/training/user/registration-user/${req.file.filename}`
                }
                switch (req.file.mimetype) {
                    case 'application/pdf': form.type = 'PDF'; break;
                    case 'application/vnd.ms-powerpoint': form.type = 'PowerPoint'; break;
                    case 'application/vnd.openxmlformats-officedocument.presentationml.presentation': form.type = 'PowerPoint'; break;
                    case 'application/vnd.ms-excel': form.type = 'Excel'; break;
                    case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet': form.type = 'Excel'; break;
                    case 'image/jpeg': form.type = 'Image'; break;
                    case 'image/png': form.type = 'Image'; break;
                    case 'video/mp4': form.type = 'Video'; break;
                    case 'audio/mp4': form.type = 'Audio'; break;
                    case 'video/x-msvideo': form.type = 'Video'; break;
                    case 'application/msword': form.type = 'Word'; break;
                    default: form.type = 'Others'
                }

                db.query(`INSERT INTO training_registration_media (name, type, url) VALUES('${req.file.originalname}','${form.type}','${form.image}')`, async (error, result, fields) => {
                    if (error) {
                        res.json({ error: true, result: error })
                    } else {
                        let mediaUploaded = await db.query(`SELECT * FROM training_registration_media WHERE id='${result.insertId}'`);
                        res.json({ error: false, result: mediaUploaded[0] })
                    }
                })
            }

        }
    })
}

exports.deleteMedia = (req, res) => {
    db.query(
        `DELETE FROM training_registration_media
        WHERE id = ?;`,
        [req.params.id],
        (error, result) => {
            if (error) {
                return res.json({ error: true, result: error.message });
            };

            return res.json({ error: false, result });
        }
    );
};
