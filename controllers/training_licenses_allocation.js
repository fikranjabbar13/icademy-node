const db = require('../configs/database');
var moment = require('moment');

exports.allocation = async (req, res) => {
    let idCompany = await db.query(`SELECT company_id FROM training_company WHERE id='${req.params.training_company_id}'`);
    db.query(`SELECT a.id, t.id AS licenses_type_id, t.name AS type, IF (a.remaining_allocation IS NULL, 0, a.remaining_allocation) AS remaining_allocation
    FROM training_licenses_type t
    LEFT JOIN (SELECT * FROM training_licenses_allocation WHERE training_company_id='${req.params.training_company_id}') a ON a.licenses_type_id=t.id
    WHERE t.status='active' AND t.company_id='${idCompany[0].company_id}'`, (error, result) => {
        if (error){
            res.json({error: true, result: error.message})
        }
        else{
            res.json({error: false, result: result})
        }
    })
}

exports.allocationTransaction = async (req, res) => {
    let balanceCompany = await db.query(`SELECT a.id, a.remaining_allocation FROM training_company_licenses_allocation a JOIN training_licenses_type t ON t.id = a.licenses_type_id WHERE t.id='${req.body.licenses_type_id}' LIMIT 1`);
    let balance = await db.query(`SELECT remaining_allocation FROM training_licenses_allocation WHERE training_company_id='${req.body.training_company_id}' AND licenses_type_id='${req.body.licenses_type_id}' LIMIT 1`);
    if (req.body.type === 'addition' && (!balanceCompany.length || balanceCompany[0].remaining_allocation < req.body.amount)){
        return res.json({error: true, result: 'Your company have no enough balance'});
    };
    if (req.body.type === 'reduction' && (!balance.length || balance[0].remaining_allocation < req.body.amount)){
        return res.json({error: true, result: 'Could not reduce amount below remaining balance'});
    };
    let amountReverse = req.body.type === 'addition' ? Math.abs(req.body.amount) * -1 : req.body.amount;
    req.body.amount = req.body.type === 'reduction' ? Math.abs(req.body.amount) * -1 : req.body.amount;
    if (!req.body.licenses_allocation_id || req.body.licenses_allocation_id === null){
        let license_allocation = await db.query(`INSERT INTO training_licenses_allocation (training_company_id, licenses_type_id, remaining_allocation) VALUES(?)`,
        [[req.body.training_company_id, req.body.licenses_type_id, '0']]);

        db.query(`INSERT INTO training_licenses_transaction (licenses_allocation_id, amount, type, note, created_at, created_by) VALUES(?)`,
        [[license_allocation.insertId, req.body.amount, req.body.type, req.body.note, moment(new Date()).format('YYYY-MM-DD HH:mm:ss'), req.body.created_by]], async (error, result) => {
            if (error){
                res.json({error: true, result: error.message})
            }
            else{
                db.query(`UPDATE training_licenses_allocation a SET a.remaining_allocation = (SELECT SUM(amount) FROM training_licenses_transaction WHERE licenses_allocation_id='${license_allocation.insertId}') WHERE a.id='${license_allocation.insertId}'`, async (error, result) => {
                    if (error){
                        res.json({error: true, result: error.message})
                    }
                    else{
                        if (req.body.type === 'reduction' || req.body.type === 'addition'){
                            await db.query(`INSERT INTO training_company_licenses_transaction (licenses_allocation_id, amount, type, training_company_id, note, created_at, created_by) VALUES(?)`,[[balanceCompany[0].id, amountReverse, 'usage', req.body.training_company_id, req.body.note, moment(new Date()).format('YYYY-MM-DD HH:mm:ss'), req.body.created_by]]);
                            await db.query(`UPDATE training_company_licenses_allocation a SET a.remaining_allocation = (SELECT SUM(amount) FROM training_company_licenses_transaction WHERE licenses_allocation_id='${balanceCompany[0].id}') WHERE a.id='${balanceCompany[0].id}'`);
                            res.json({error: false, result: result})
                        }
                        else{
                            res.json({error: false, result: result})
                        }
                    }
                })
            }
        })
    }
    else{
        db.query(`INSERT INTO training_licenses_transaction (licenses_allocation_id, amount, type, note, created_at, created_by) VALUES(?)`,
        [[req.body.licenses_allocation_id, req.body.amount, req.body.type, req.body.note, moment(new Date()).format('YYYY-MM-DD HH:mm:ss'), req.body.created_by]], (error, result) => {
            if (error){
                res.json({error: true, result: error.message})
            }
            else{
                db.query(`UPDATE training_licenses_allocation a SET a.remaining_allocation = (SELECT SUM(amount) FROM training_licenses_transaction WHERE licenses_allocation_id='${req.body.licenses_allocation_id}') WHERE a.id='${req.body.licenses_allocation_id}'`, async (error, result) => {
                    if (error){
                        res.json({error: true, result: error.message})
                    }
                    else{
                        if (req.body.type === 'reduction' || req.body.type === 'addition'){
                            await db.query(`INSERT INTO training_company_licenses_transaction (licenses_allocation_id, amount, type, training_company_id, note, created_at, created_by) VALUES(?)`,[[balanceCompany[0].id, amountReverse, 'usage', req.body.training_company_id, req.body.note, moment(new Date()).format('YYYY-MM-DD HH:mm:ss'), req.body.created_by]]);
                            await db.query(`UPDATE training_company_licenses_allocation a SET a.remaining_allocation = (SELECT SUM(amount) FROM training_company_licenses_transaction WHERE licenses_allocation_id='${balanceCompany[0].id}') WHERE a.id='${balanceCompany[0].id}'`);
                            res.json({error: false, result: result})
                        }
                        else{
                            res.json({error: false, result: result})
                        }
                    }
                })
            }
        })
    }
}

exports.transactionHistory = (req, res) => {
    db.query(`SELECT t.created_at AS created_at, t.amount, lt.name AS type, t.note, u.name AS created_by
    FROM training_licenses_transaction t
    JOIN training_licenses_allocation a ON a.id=t.licenses_allocation_id
    JOIN training_licenses_type lt ON lt.id=a.licenses_type_id
    JOIN user u ON u.user_id=t.created_by
    WHERE a.training_company_id='${req.params.training_company_id}'
    ORDER BY t.created_at DESC`, (error, result) => {
        if (error){
            res.json({error: true, result: error.message})
        }
        else{
            res.json({error: false, result: result})
        }
    })
}

exports.getCompanyList = (req, res) => {
    db.query(`SELECT
    *,
    COALESCE((SELECT SUM(t.amount)
        FROM training_company_licenses_allocation a
        LEFT JOIN training_company_licenses_transaction t ON t.licenses_allocation_id = a.id
        WHERE a.company_id=c.company_id AND t.type IN ('reduction', 'addition')), 0) AS total_allocated,
    COALESCE((SELECT SUM(t.amount)
        FROM training_company_licenses_allocation a
        LEFT JOIN training_company_licenses_transaction t ON t.licenses_allocation_id = a.id
        WHERE a.company_id=c.company_id AND t.type IN ('usage')), 0) AS total_used,
    COALESCE((SELECT SUM(t.amount)
        FROM training_company_licenses_allocation a
        LEFT JOIN training_company_licenses_transaction t ON t.licenses_allocation_id = a.id
        WHERE a.company_id=c.company_id), 0) AS total_amount
 FROM company c
 WHERE status='active' AND access_training='1'
 ORDER BY company_name ASC`, (error, result) => {
        if (error){
            res.json({error: true, result: error.message})
        }
        else{
            res.json({error: false, result: result})
        }
    })
}

exports.allocationCompany = async (req, res) => {
    db.query(`SELECT a.id, t.id AS licenses_type_id, t.name AS type, IF (a.remaining_allocation IS NULL, 0, a.remaining_allocation) AS remaining_allocation
    FROM training_licenses_type t
    LEFT JOIN (SELECT * FROM training_company_licenses_allocation WHERE company_id='${req.params.id}') a ON a.licenses_type_id=t.id
    WHERE t.status='active' AND t.company_id='${req.params.id}'`, (error, result) => {
        if (error){
            res.json({error: true, result: error.message})
        }
        else{
            res.json({error: false, result: result})
        }
    })
}

exports.transactionHistoryCompany = (req, res) => {
    db.query(`SELECT t.created_at AS created_at, t.amount, lt.name AS type, t.type AS transaction_type, c.name AS training_company, t.note, u.name AS created_by
    FROM training_company_licenses_transaction t
    JOIN training_company_licenses_allocation a ON a.id=t.licenses_allocation_id
    JOIN training_licenses_type lt ON lt.id=a.licenses_type_id
    JOIN user u ON u.user_id=t.created_by
    LEFT JOIN training_company c ON c.id = t.training_company_id
    WHERE a.company_id='${req.params.id}'
    ORDER BY t.created_at DESC`, (error, result) => {
        if (error){
            res.json({error: true, result: error.message})
        }
        else{
            res.json({error: false, result: result})
        }
    })
}

exports.allocationTransactionCompany = async (req, res) => {
    let balance = await db.query(`SELECT remaining_allocation FROM training_company_licenses_allocation WHERE company_id='${req.body.company_id}' AND licenses_type_id='${req.body.licenses_type_id}' LIMIT 1`);
    if (req.body.type === 'reduction' && (!balance.length || balance[0].remaining_allocation < req.body.amount)){
        return res.json({error: true, result: 'Could not reduce amount below remaining balance'});
    };
    req.body.amount = req.body.type === 'reduction' ? Math.abs(req.body.amount) * -1 : req.body.amount;
    if (!req.body.licenses_allocation_id || req.body.licenses_allocation_id === null){
        let license_allocation = await db.query(`INSERT INTO training_company_licenses_allocation (company_id, licenses_type_id, remaining_allocation) VALUES(?)`,
        [[req.body.company_id, req.body.licenses_type_id, '0']]);

        db.query(`INSERT INTO training_company_licenses_transaction (licenses_allocation_id, amount, type, note, created_at, created_by) VALUES(?)`,
        [[license_allocation.insertId, req.body.amount, req.body.type, req.body.note, moment(new Date()).format('YYYY-MM-DD HH:mm:ss'), req.body.created_by]], (error, result) => {
            if (error){
                res.json({error: true, result: error.message})
            }
            else{
                db.query(`UPDATE training_company_licenses_allocation a SET a.remaining_allocation = (SELECT SUM(amount) FROM training_company_licenses_transaction WHERE licenses_allocation_id='${license_allocation.insertId}') WHERE a.id='${license_allocation.insertId}'`, (error, result) => {
                    if (error){
                        res.json({error: true, result: error.message})
                    }
                    else{
                        res.json({error: false, result: result})
                    }
                })
            }
        })
    }
    else{
        db.query(`INSERT INTO training_company_licenses_transaction (licenses_allocation_id, amount, type, note, created_at, created_by) VALUES(?)`,
        [[req.body.licenses_allocation_id, req.body.amount, req.body.type, req.body.note, moment(new Date()).format('YYYY-MM-DD HH:mm:ss'), req.body.created_by]], (error, result) => {
            if (error){
                res.json({error: true, result: error.message})
            }
            else{
                db.query(`UPDATE training_company_licenses_allocation a SET a.remaining_allocation = (SELECT SUM(amount) FROM training_company_licenses_transaction WHERE licenses_allocation_id='${req.body.licenses_allocation_id}') WHERE a.id='${req.body.licenses_allocation_id}'`, (error, result) => {
                    if (error){
                        res.json({error: true, result: error.message})
                    }
                    else{
                        res.json({error: false, result: result})
                    }
                })
            }
        })
    }
}