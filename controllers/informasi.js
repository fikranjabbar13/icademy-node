var env = require('../env.json');
var conf = require('../configs/config');
var db = require('../configs/database');

var multer = require('multer');
var storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, './public/informasi');
  },
  filename: (req, file, cb) => {
    var filetype = '';
    if(file.mimetype === 'audio/mp4' || file.mimetype === 'application/mp4' || file.mimetype === 'video/mp4') {
      filetype = 'mp4';
    }
    if(file.mimetype === 'application/pdf') {
      filetype = 'pdf';
    }
    if(file.mimetype === 'image/gif') {
      filetype = 'gif';
    }
    if(file.mimetype === 'image/png') {
      filetype = 'png';
    }
    if(file.mimetype === 'image/jpeg') {
      filetype = 'jpg';
    }
    if(file.mimetype === 'application/pdf') {
      filetype = 'pdf';
    }
    cb(null, 'info' + '-' + Date.now() + '.' + filetype);
  }
});

var uploadFiles = multer({storage: storage}).array('files', 10);


exports.create = (req, res, next) => {
  let formData = {
    judul: req.body.judul,
    company: req.body.company,
    deskripsi: req.body.deskripsi,
    files: null
  };

  console.log(formData)

  db.query(`INSERT INTO informasi (informasi_id, company_id, informasi_judul, informasi_deskripsi, informasi_files)
  	VALUES (null, '${formData.company}', '${formData.judul}', '${formData.deskripsi}', '${formData.files}')`, (error, result, fields) => {
  		if(error) {
				res.json({error: true, result: error});
  		} else {
  			db.query(`SELECT * FROM informasi WHERE informasi_id = '${result.insertId}'`, (error, result, fields) => {
  				res.json({ error: false, result: result[0] });
  			})
  		}
	});
};

exports.get = (req, res, next) => {
	db.query(`SELECT * FROM informasi ORDER BY informasi_id DESC`, (error, result, fields) => {
		res.json({ error: false, result: result });
	})
};

exports.getByCompany = (req, res, next) => {
  db.query(`SELECT * FROM informasi WHERE company_id = '${req.params.company}' ORDER BY informasi_id DESC`, (error, result, fields) => {
    res.json({ error: false, result: result });
  })
};

exports.getOne = (req, res, next) => {
	db.query(`SELECT * FROM informasi WHERE informasi_id = '${req.params.id}' ORDER BY informasi_id DESC`, (error, result, fields) => {
		res.json({ error: false, result: result });
	})
};

exports.update = (req, res, next) => {
	let formData = {
		judul: req.body.judul,
    deskripsi: req.body.deskripsi,
    company: req.body.company,
  };

  console.log(formData)

  db.query(`UPDATE informasi SET 
    informasi_judul = '${formData.judul}', 
    company_id = '${formData.company}', 
    informasi_deskripsi = '${formData.deskripsi}' 
    WHERE informasi_id = '${req.params.value}'`, (error, result, fields) => {
  		if(error) {
				res.json({error: true, result: error});
  		} else {
  			db.query(`SELECT * FROM informasi WHERE informasi_id = '${req.params.value}'`, (error, result, fields) => {
  				res.json({ error: false, result: result[0] });
  			})
  		}
  	});
};

exports.updateFile = (req, res, next) => {
	uploadFiles(req, res, (err) => {
    if(!req.files) {
      res.json({ error: true, result: err });
    } else {
    	let nameFiles = '';
      for(let i=0; i<req.files.length; i++) {
        nameFiles += env.APP_URL + '/informasi/' + req.files[i].filename + ',';
      }
      nameFiles = nameFiles.substring(0, nameFiles.length - 1);

      let formData = {
        files: nameFiles
      };

      db.query(`UPDATE informasi SET informasi_files = '${formData.files}' WHERE informasi_id = '${req.params.value}'`, (error, result, fields) => {
      		if(error) {
						res.json({error: true, result: error});
      		} else {
      			db.query(`SELECT * FROM informasi WHERE informasi_id = '${req.params.value}'`, (error, result, fields) => {
      				res.json({ error: false, result: result[0] });
      			})
      		}
      	});
    }
  });
};

exports.delete = (req, res, next) => {
	db.query(`DELETE FROM informasi WHERE informasi_id = '${req.params.value}'`, (error, result, fields) => {
    if(error) {
      res.json({error: true, result: error});
    } else {
      res.json({error: false, result: result});
    }
  })
};

