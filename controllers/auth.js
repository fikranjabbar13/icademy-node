var env = require('../env.json');
var conf = require('../configs/config');
var db = require('../configs/database');
var jwt = require('jsonwebtoken');
var md5 = require('md5');

const OS = (e) => {
	var ua = e,
		$ = {};

	if (/mobile/i.test(ua))
		$.Mobile = true;

	if (/like Mac OS X/.test(ua)) {
		$.iOS = /CPU( iPhone)? OS ([0-9\._]+) like Mac OS X/.exec(ua)[2].replace(/_/g, '.');
		$.iPhone = /iPhone/.test(ua);
		$.iPad = /iPad/.test(ua);
	}

	if (/Android/.test(ua))
		$.Android = /Android ([0-9\.]+)[\);]/.exec(ua)[1];

	if (/webOS\//.test(ua))
		$.webOS = /webOS\/([0-9\.]+)[\);]/.exec(ua)[1];

	if (/(Intel|PPC) Mac OS X/.test(ua))
		$.Mac = /(Intel|PPC) Mac OS X ?([0-9\._]*)[\)\;]/.exec(ua)[2].replace(/_/g, '.') || true;

	if (/Windows NT/.test(ua))
		$.Windows = /Windows NT ([0-9\._]+)[\);]/.exec(ua)[1];

	return `${Object.keys($)} ${$[Object.keys($)]}`
}

const Browser = (e) => {
	var ua = e, tem,
		M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
	if (/trident/i.test(M[1])) {
		tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
		return 'IE ' + (tem[1] || '');
	}
	if (M[1] === 'Chrome') {
		tem = ua.match(/\b(OPR|Edge)\/(\d+)/);
		if (tem != null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
	}
	M = M[2] ? [M[1], M[2]] : ['navigator.appName', 'navigator.appVersion', '-?'];
	if ((tem = ua.match(/version\/(\d+)/i)) != null) M.splice(1, 1, tem[1]);
	M.join(' ');

	return `${M[0]} ${M[1]}`;
}

exports.sidebar_access = (req, res) => {
	try {

		let sidebar = {
			top_sidebar: {

				Project: {
					route: '/sidebar/project',
					name: 'Project',
					icon: '#URI'
				},
				Notification: {
					route: '/sidebar/notification',
					name: 'Notification',
					icon: '#URI'
				},
				Calendar: {
					route: '/sidebar/calendar',
					name: 'Calendar',
					icon: '#URI'
				},
				Files: {
					route: '/sidebar/files',
					name: 'Files',
					icon: '#URI'
				},
				Report: {
					route: '/sidebar/report',
					name: 'Report',
					icon: '#URI'
				},
				Company: {
					route: '/sidebar/company',
					name: 'Company',
					icon: '#URI'
				},
				News: {
					route: '/sidebar/news',
					name: 'News',
					icon: '#URI'
				},
			},
			bottom_sidebar: {

				Setting: {
					route: '/sidebar/setting',
					name: 'Setting',
					icon: '#URI'
				},
				Support: {
					route: '/sidebar/support',
					name: 'Support',
					icon: '#URI'
				},
				Logout: {
					route: '/sidebar/logout',
					name: 'Logout',
					icon: '#URI'
				},
			}
		}

		if (req.app.token.user_id) {

			const query = `select LOWER(a.level) as level from user a where a.user_id = ?;`;
			db.query(query, [req.app.token.user_id], (err, result) => {
				if (err) {
					res.json({ error: true, result: 'Sidebar menu access denied.' });
				} else {
					if (result[0].level === 'user') {
						delete sidebar.top_sidebar.Company;
					}
					res.json({ error: false, result: sidebar });
				}
			})
		} else {
			res.json({ error: true, result: 'Sidebar menu access denied.' });
		}

	} catch (e) {
		console.log(e, "CATCH");
		res.json({ error: true, result: 'Sidebar menu access denied.' });
	}
}

function auth(req, res) {
	const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
	const os = OS(req.headers['user-agent']);
	const browser = Browser(req.headers['user-agent']);
	const currentposition = req.headers.currentcompany;

	if (req.body.email == '') res.json({ error: true, result: 'Email required' });
	if (req.body.password == '') res.json({ error: true, result: 'Password required' });

	var inputEmail = req.body.email;
	var inputPasswrod = md5(req.body.password);

	let append_select1 = '';
	let append_update_token = '';
	let append_select2 = '';
	let append_update = '';
	let query = `
	SELECT
		u.user_id, u.unlimited AS userunl, u.validity AS uv, u.email, u.level, u.token,
		u.company_id,u.name, u.avatar, u.level, u.branch_id, u.grup_id,g.grup_name, 
		c.validity AS cv, c.unlimited AS unl, c.status AS status, u.is_new_password,
		c.access_training,c.access_meeting,c.access_webinar,c.access_calendar,
		c.access_filemanager,c.access_report,c.access_news,c.access_project 
	FROM user u
	left join grup g on g.grup_id = u.grup_id
	JOIN company c
	ON	c.company_id = u.company_id
	WHERE
		email = '${inputEmail}'
	AND
		password = '${inputPasswrod}'
	AND
		u.status = 'active' `;

	if (currentposition) {
		append_select1 = `AND u.company_id = '${currentposition}';`
		append_update = `AND company_id = '${currentposition}';`
		append_select2 = `AND t1.company_id = '${currentposition}';`
	}

	query += append_select1;

	db.query(query,
		async (error, result, fields) => {
			//return console.log(result)
			if (error != null) {
				res.json({ error: true, result: error });
			} else {
				if (result.length != 0) {

					let now = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()).getTime();
					let uservalid = result[0].uv && new Date(result[0].uv).getTime() < now;
					let companyvalid = result[0].cv && new Date(result[0].cv).getTime() < now;
					if ((uservalid && result[0].userunl == 0) || (companyvalid && result[0].unl == 0) || (result[0].status == 'nonactive')) {
						res.json({ error: true, result: { status: 'expired', message: 'Akun ini sudah kadaluarsa' } });
					} else {

						let training_company_id = null;
						if (result[0].grup_name === 'Admin Training') {
							let ress = await db.query(`Select tu.training_company_id from training_user tu where tu.email='${result[0].email}'`)
							training_company_id = ress[0].training_company_id;
						}
						var token = jwt.sign(
							{
								email: req.body.email,
								user_id: result[0].user_id,
								training_company_id: training_company_id,
								company_id: result[0].company_id,
								level: result[0].level,
								avatar: result[0].avatar,
								grup_name: result[0].grup_name,
								access_training: result[0].access_training,
								access_meeting: result[0].access_meeting,
								access_webinar: result[0].access_webinar,
								access_calendar: result[0].access_calendar,
								access_filemanager: result[0].access_filemanager,
								access_report: result[0].access_report,
								access_news: result[0].access_news,
								access_project: result[0].access_project,

							},
							env.APP_KEY,
							{ expiresIn: '7d', algorithm: 'HS384' }
						);

						if (result[0].validity == null) {
							db.query(`UPDATE user SET validity = '${conf.dateTimeNow()}' WHERE email = '${inputEmail}' AND password = '${inputPasswrod}' ${append_update}`);
						}

						db.query(`UPDATE user SET token = '${token}', last_login = '${conf.dateTimeNow()}' WHERE email = '${inputEmail}' AND password = '${inputPasswrod}' ${append_update}`, (error, result, fields) => {
							db.query(`
							SET @user_id = null;

							SELECT @user_id := t1.user_id AS user_id, tu.id AS training_user_id, t1.name, c.company_id, c.company_type, t1.grup_id, t2.grup_name, t1.validity, t1.email, t1.level, t1.token,
								t2.activity, t2.course, t2.manage_course, t2.forum, t2.group_meeting, t2.manage_group_meeting, t1.is_new_password,
								c.access_training,c.access_meeting,c.access_webinar,c.access_calendar,c.access_filemanager,c.access_report,c.access_news,c.access_project 
							FROM user t1
							LEFT JOIN (SELECT * FROM training_user WHERE status='Active') tu ON tu.email = t1.email
							INNER JOIN grup t2
								ON t1.grup_id = t2.grup_id
							JOIN company c
								ON c.company_id = t1.company_id
							WHERE t1.email = '${inputEmail}' AND t1.password = '${inputPasswrod}' ${append_select2};

							INSERT INTO login_history (user_id, type, ip, os, browser, login, action, created_at)
							VALUE (@user_id, 'EMAIL', '${ip}', '${os}', '${browser}', NOW(), 'LOGIN', NOW());
							`,
								(error, result, fields) => {
									result[1][0].training_company_id = training_company_id;
									if (result[1][0].level === 'superadmin') {
										result[1][0].access_training = 1;
										result[1][0].access_meeting = 1;
										result[1][0].access_calendar = 1;
										result[1][0].access_filemanager = 1;
										result[1][0].access_report = 1;
										result[1][0].access_webinar = 1;
										result[1][0].access_news = 1;
										result[1][0].access_project = 1;
									}
									res.json({ error: false, result: result[1][0] });
								});
						});
					}
				} else {
					res.json({ error: true, result: 'User not found' });
				}
			}
		});
}
exports.auth = (req, res) => {
	auth(req, res);
};

exports.authVoucher = (req, res, next) => {
	const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
	const os = OS(req.headers['user-agent']);
	const browser = Browser(req.headers['user-agent']);

	if (req.body.voucher == '') res.json({ error: true, result: 'Voucher required' });

	db.query(`SELECT email, password FROM user WHERE voucher = '${req.body.voucher}' AND status='active'`, (error, result, fields) => {
		if (error) {
			res.json({ error: true, result: error });
		} else {
			if (result.length == 1) {
				db.query(`
					SELECT
						u.user_id,u.name, u.unlimited AS userunl, u.validity AS uv, u.email, u.avatar, u.company_id,u.level, u.token, c.validity AS cv, c.unlimited AS unl, c.status AS status,g.grup_name, 
						c.access_training,c.access_meeting,c.access_webinar,c.access_calendar,c.access_filemanager,c.access_report,c.access_news,c.access_project 
					FROM user u 
					left join grup g on g.grup_id = u.grup_id 
					JOIN company c
					ON	c.company_id = u.company_id
					WHERE
						email = '${result[0].email}'
					AND
						password = '${result[0].password}'
					AND
						u.status = 'active'`,
					async (error, result, fields) => {
						if (error) {
							res.json({ error: true, result: error });
						} else {
							if (result.length == 1) {

								let now = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()).getTime();
								let uservalid = result[0].uv && new Date(result[0].uv).getTime() < now;
								let companyvalid = result[0].cv && new Date(result[0].cv).getTime() < now;
								if ((uservalid && result[0].userunl == 0) || (companyvalid && result[0].unl == 0) || (result[0].status == 'nonactive')) {
									res.json({ error: true, result: { status: 'expired', message: 'Akun ini sudah kadaluarsa' } });
								} else {

									let training_company_id = null;
									if (result[0].grup_name === 'Admin Training') {
										let ress = await db.query(`Select tu.training_company_id from training_user tu where tu.email='${result[0].email}'`)
										training_company_id = ress.length ? ress[0].training_company_id : null;
									}
									var token = jwt.sign(
										{
											email: result[0].email,
											training_company_id: training_company_id,
											user_id: result[0].user_id,
											company_id: result[0].company_id,
											grup_name: result[0].grup_name,
											level: result[0].level,
											avatar: result[0].avatar,
											name: result[0].name,
											access_training: result[0].access_training,
											access_meeting: result[0].access_meeting,
											access_webinar: result[0].access_webinar,
											access_calendar: result[0].access_calendar,
											access_filemanager: result[0].access_filemanager,
											access_report: result[0].access_report,
											access_news: result[0].access_news,
											access_project: result[0].access_project,
										},
										env.APP_KEY,
										{ expiresIn: '7d', algorithm: 'HS384' }
									);

									db.query(`UPDATE user SET token = '${token}', last_login = '${conf.dateTimeNow()}' WHERE voucher = '${req.body.voucher}'`, (error, result, fields) => {
										db.query(`
										SET @user_id = null;

										SELECT @user_id := t1.user_id AS user_id, tu.id AS training_user_id, t1.name,c.company_id, c.company_type, t1.grup_id, t2.grup_name, t1.validity, t1.email, t1.level, t1.token,
											t2.activity, t2.course, t2.manage_course, t2.forum, t2.group_meeting, t2.manage_group_meeting, t1.is_new_password,
											c.access_training,c.access_meeting,c.access_webinar,c.access_calendar,c.access_filemanager,c.access_report,c.access_news,c.access_project 
										FROM user t1
										LEFT JOIN (SELECT * FROM training_user WHERE status='Active') tu ON tu.email = t1.email
										INNER JOIN grup t2
											ON t1.grup_id = t2.grup_id
										JOIN company c
											ON c.company_id = t1.company_id
										WHERE t1.voucher = '${req.body.voucher}';

										INSERT INTO login_history (user_id, type, ip, os, browser, login, action, created_at)
										VALUE (@user_id, 'VOUCHER', '${ip}', '${os}', '${browser}', NOW(), 'LOGIN', NOW());
										`,
											(error, result, fields) => {
												result[1][0].training_company_id = training_company_id;
												if (result[1][0].level === 'superadmin') {
													result[1][0].access_training = 1;
													result[1][0].access_meeting = 1;
													result[1][0].access_calendar = 1;
													result[1][0].access_filemanager = 1;
													result[1][0].access_report = 1;
													result[1][0].access_webinar = 1;
													result[1][0].access_news = 1;
													result[1][0].access_project = 1;
												}
												//console.log(result[1][0], "RESPONSE AUTHVOUCHER")
												res.json({ error: false, result: result[1][0] });
											});
									});
								}
							} else {
								res.json({ error: true, result: 'User not found' });
							}
						}
					});
			} else {
				res.json({ error: true, result: 'Voucher not found' });
			}
		}
	});
};

exports.authMe = (req, res, next) => {

	let append = ' ,NULL AS data_multi_company';
	let case_dup_email = '';
	if (typeof parseInt(req.headers.currentposition) === 'number' && req.headers.currentposition !== '[Object Object]') {
		append += ` ,if(u.company_id != '${req.headers.currentposition}' AND u.level != 'superadmin', 			
						(	
							SELECT if(uc.company_id, if(uc.role is null, 'client', uc.role), NULL)  
							FROM user_company uc WHERE uc.user_id = u.user_id AND uc.company_id = '${req.headers.currentposition}'
						),
					NULL)  AS data_multi_company`;


	}
	// zurrich
	if (typeof parseInt(req.headers.currentcompany) === 'number' && req.headers.currentcompany !== '[Object Object]') {

		if (req.headers.currentcompany === '96') {
			case_dup_email = `AND u.company_id = '${req.headers.currentcompany}'`;
		}
	}


	// OLD
	// const querys =	`SELECT 
	// 	u.user_id, u.company_id, c.company_name, c.company_type, c.logo, u.branch_id, u.grup_id, u.identity, u.voucher, u.name, u.email, u.phone, u.address, 
	// 	u.avatar, u.level, u.status, u.registered, u.last_login, u.unlimited, u.validity, g.grup_name, 
	// 	IF(uc.user_id IS NULL, 0,1) AS 'is_multi_company', 
	// 	IF(uc.user_id IS NULL, NULL, IF(uc.role IS NULL, 'client',uc.role)) AS 'multi_level'
	// 	FROM user_company uc
	// 	JOIN user u ON uc.user_id = u.user_id
	// 	JOIN company c ON uc.company_id = c.company_id
	// 	JOIN branch b ON u.branch_id = b.branch_id
	// 	JOIN grup g ON u.grup_id = g.grup_id
	// 	WHERE ${append} u.email = '${req.params.email}' AND u.status='active';`;

	const querys =
		`SELECT 
		u.user_id, u.company_id, u.first_open_exam as 'firstOpenExam', 
		c.company_name, c.company_type, c.logo, 
		u.branch_id, u.grup_id, u.identity, u.voucher, u.name, u.email, u.phone, u.address,
		u.avatar, u.level, u.status, u.registered, u.last_login,  u.unlimited, u.validity, 
		g.grup_name ,
		c.access_training,c.access_meeting,c.access_webinar,c.access_calendar,
		c.access_filemanager,c.access_report,c.access_news,c.access_project 
		${append} 
		FROM user u JOIN company c ON c.company_id = u.company_id
		JOIN grup g ON u.grup_id = g.grup_id
		WHERE u.email = '${req.params.email}' ${case_dup_email} AND u.status='active';`;

	db.query(querys, async (error, result, fields) => {
		if (error) {
			res.json({ error: true, result: error });
		} else {
			if (result.length > 0) {
				
				result.forEach((str) => {
					if (str.data_multi_company) {
						str.level = str.data_multi_company;
					}

					if(str.firstOpenExam > 0){
						str.firstOpenExam = true;
					}else{
						str.firstOpenExam = false;
					}
				})

				let groups = await db.query(`SELECT * FROM user_group WHERE user_id = '${result[0].user_id}'`);
				let groupsId = [];
				if (groups.length > 1) {
					for (var i = 0; i < groups.length; i++) {
						groupsId.push(groups[i].group_id);
					}
				}
				result[0].group = groupsId;
				// result[0].group = groups;
				res.json({ error: false, result: result[0] });
			} else {
				res.json({ error: false, result: 'User not found' });
			}
		}
	});
};

exports.logout = (req, res, next) => {
	const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
	const os = OS(req.headers['user-agent']);
	const browser = Browser(req.headers['user-agent']);

	db.query(`UPDATE login_history
	SET logout = NOW(),
		action = 'LOGOUT'
	WHERE user_id = '${req.params.user_id}'
	AND ip = '${ip}'
	AND os = '${os}'
	AND browser = '${browser}'
	ORDER BY id DESC
	LIMIT 1;`, (error, result) => {
		if (error) {
			res.json({ error: true, result: error });
		} else {
			if (result.affectedRows === 1) {
				res.json({ error: false, result: 'success logout' })
			} else {
				res.json({ error: true, result: 'data history not found' })
			}
		}
	})
}
