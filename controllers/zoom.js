const crypto = require('crypto')
const env = require('../env.json');
const request = require('request');
var db = require('../configs/database');
var fetch = require('node-fetch')

exports.getUser = async (req, res) => {

  const { email } = req.app.token
  const user = await db.query(`SELECT user_id FROM user WHERE email = ?`, [email])

  const ifExists = await db.query(`SELECT id, refresh FROM user_zoom WHERE user_id = ?`, user.length ? user[0].user_id : null)
  if (ifExists.length) {
    const b = Buffer.from(env.ZOOM_KEY + ":" + env.ZOOM_SECRET);
    const zoomRes = await fetch(`https://zoom.us/oauth/token?grant_type=refresh_token&refresh_token=${ifExists[0].refresh}`, {
      method: "POST",
      headers: {
        Authorization: `Basic ${b.toString("base64")}`,
      },
    });

    if(!zoomRes.ok)
      return res.status(401).send("Could not connect with Zoom");
    
    const zoomData = await zoomRes.json();
    if (zoomData.error)
      return res.status(401).send("Could not connect with Zoom");
    
    let dataQuery = [zoomData.access_token, zoomData.refresh_token, zoomData.expires_in, ifExists[0].id]
    let execQuery = await db.query(`UPDATE user_zoom SET token = ?, refresh = ?, expires = ? WHERE id = ?`, dataQuery)
    res.json({ error: false, result: execQuery })

  }
  else {
    const b = Buffer.from(env.ZOOM_KEY + ":" + env.ZOOM_SECRET);
    const zoomRes = await fetch(`https://zoom.us/oauth/token?grant_type=authorization_code&code=${req.body.code}&redirect_uri=${env.ZOOM_REDIRECT}`, {
      method: "POST",
      headers: {
        Authorization: `Basic ${b.toString("base64")}`,
      },
    });
  
    if(!zoomRes.ok)
      return res.json({ error: true, result: "Could not connect with Zoom" });
    
    const zoomData = await zoomRes.json();

    if (zoomData.error)
      return res.json({ error: true, result: "Could not connect with Zoom" });
    
      // Retreive user details
    const zoomUserRes = await fetch("https://api.zoom.us/v2/users/me", {
      method: "GET",
      headers: {
        Authorization: `Bearer ${zoomData.access_token}`,
      },
    });
  
    const zoomUserData = await zoomUserRes.json();
    
    let dataQuery = [[user.length ? user[0].user_id : null, zoomUserData.email, zoomUserData.account_id, zoomData.access_token, zoomData.refresh_token, zoomData.expires_in, zoomUserData.personal_meeting_url]]
    let insertQuery = await db.query(`INSERT INTO user_zoom (user_id, email, account_id, token, refresh, expires, link) VALUES (?)`, dataQuery)
    return res.json(insertQuery);
  }
  
}

exports.getUserById = (req, res) => {
  let { userId } = req.params
  console.log('userId', userId)
  let sql = `SELECT * FROM user_zoom WHERE user_id = ?`
  db.query(sql, [userId], (error, result, fields) => {
    if (error) {
      res.json({ error: true, result: error })
    }
    else {
      res.json({ error: false, result, result })
    }
  })
}

exports.deleteUser = (req, res) => {
  let { userId } = req.params
  let sql = `DELETE FROM user_zoom where user_id = ?`
  db.query(sql, [userId], (error, result, fields) => {
    if (error) {
      res.json({ error: true, result: error })
    }
    else {
      res.json({ error: false, result: result })
    }
  })
}

exports.signature = (req, res) => {
  const timestamp = new Date().getTime() - 30000
  const msg = Buffer.from(env.ZOOM_KEY + req.body.meetingNumber + timestamp + req.body.role).toString('base64')
  const hash = crypto.createHmac('sha256', env.ZOOM_SECRET).update(msg).digest('base64')
  const signature = Buffer.from(`${env.ZOOM_KEY}.${req.body.meetingNumber}.${timestamp}.${req.body.role}.${hash}`).toString('base64')

  res.json({
    signature: signature
  })
}

exports.callback = (req, res) => {

  var options = {
    method: 'POST',
    url: 'https://zoom.us/oauth/token',
    qs: {
      grant_type: 'authorization_code',
     code: req.query.code,
     redirect_uri: 'http://localhost:3200/v2/zoom/callback'
    },
    headers: {
     Authorization: 'Basic ' + Buffer.from(env.ZOOM_CLIENT_ID + ':' + env.ZOOM_CLIENT_SECRET).toString('base64')
    }
  };

  request(options, function(error, response, body) {
    if (error) throw new Error(error);

    body = JSON.parse(body);

    let options = {
      method: 'GET',
      url: 'https://api.zoom.us/v2/users/me/meetings',
      headers: {
        Authorization: 'Bearer ' + body.access_token
      }
    }

    request(options, (error, response, result) => {
      if (error) throw new Error(error);

      result = JSON.parse(result);

      res.json({
       request: req.query,
       response: body,
       result: result,
     })

    })

  });
}

exports.refreshToken = (req, res) => {

  var options = {
    method: 'POST',
    url: 'https://zoom.us/oauth/token',
    qs: {
     grant_type: 'refresh_token',
     refresh_token: req.body.refresh_token,
    },
    headers: {
     Authorization: 'Basic ' + Buffer.from(env.ZOOM_CLIENT_ID + ':' + env.ZOOM_CLIENT_SECRET).toString('base64')
    }
  };

  request(options, (error, response, body) => {
    if (error) throw new Error(error);

    body = JSON.parse(body)

    res.json(body)
  })
}

exports.createMeeting = (req, res) => {
  // bearer: "access_token"
  // topic: "CreateMeetingFromAPI"
  // start_time: "2021-02-20T06:00:00Z"
  // duration: 120
  // timezone: "Asia/Jakarta"
  let options = {
    method: 'POST',
    url: 'https://api.zoom.us/v2/users/me/meetings',
    json: {
      topic: req.body.topic,
      start_time: req.body.start_time,
      duration: req.body.duration,
      timezone: 'Asia/Jakarta',
      password: 'hoster'
    },
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + req.body.bearer
    }
  }

  request(options, (error, response, body) => {
    if (error) throw new Error(error);

    res.json(body)
  })
}

exports.getMeetings = (req, res) => {
  let options = {
    method: 'GET',
    url: 'https://api.zoom.us/v2/users/me/meetings',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + req.query.bearer
    }
  }

  request(options, (error, response, body) => {
    if (error) throw new Error(error);

    body = JSON.parse(body)

    res.json(body)
  })
}

exports.getMeeting = (req, res) => {
  let options = {
    method: 'GET',
    url: 'https://api.zoom.us/v2/meetings/'+req.params.id,
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + req.query.bearer
    }
  }

  request(options, (error, response, body) => {
    if (error) throw new Error(error);

    body = JSON.parse(body)

    res.json(body)
  })
}

exports.liveclassZoom = async (req, res) => {
  let id = req.params.id;
  let sql = `SELECT * FROM liveclass_zoom WHERE class_id = ?`;
  let data = await db.query(sql, [id]);

  if(data.length === 1) {
    res.json({ error: false, result: data })
  }
  else {
    res.json({ error: false, result: data })
  }
}

exports.webinarZoom = async (req, res) => {
  let id = req.params.id;
  let sql = `SELECT * FROM webinar_zoom WHERE class_id = ?`;
  let data = await db.query(sql, [id]);

  if(data.length === 1) {
    res.json({ error: false, result: data })
  }
  else {
    res.json({ error: false, result: data })
  }
}
