var env = require('../env.json');
var conf = require('../configs/config');
var db = require('../configs/database');

exports.getByUser = (req, res) => {
	let userId = req.params.userId;
	if(!userId) {
		res.json({ error: true, result: "User ID required"});
	}

	let sql = `SELECT t.*, u.name 
		FROM todo t JOIN user u ON u.user_id = t.user_id 
		WHERE t.user_id = '${userId}'`;

	db.query(sql, (error, result) => {
		if(error) {
			res.json({ error: true, result: error });
		}

		res.json({ error: false, result: result });
	})
};

exports.createTodo = (req, res) => {
	let form = {
		userId: req.body.userId,
		authorId: req.body.authorId,
		title: req.body.title,
		description: req.body.description,
		type: req.body.type
	};

	let sql = `INSERT INTO todo (user_id, author_id, title, description, type) 
		VALUES ('${form.userId}', '${form.authorId}', '${form.title}', '${form.description}', '${form.type}')`;

	db.query(sql, (error, result) => {
		if(error) {
			res.json({ error: true, result: error });
		}

		let sql = `SELECT t.*, u.name 
			FROM todo t JOIN user u ON u.user_id = t.user_id 
			WHERE t.id = '${result.insertId}'`;

		db.query(sql, (error, result) => {
			res.json({ error: false, result: result[0] });
		});
	});
};

exports.doneToDo = async (req, res) => {
	let id = req.params.id;
	if(!id) {
		res.json({ error: true, result: "ID required" });
	}

	let status = await db.query(`SELECT * FROM todo WHERE id = '${id}'`);
	let statusNya = status[0].status == 1 ? 2 : 1;

	let sql = `UPDATE todo SET status = '${statusNya}' WHERE id = '${id}'`;
	db.query(sql, (error, reuslt) => {
		if(error) {
			res.json({ error: true, result: error });
		}

		let sql = `SELECT t.*, u.name 
			FROM todo t JOIN user u ON u.user_id = t.user_id 
			WHERE t.id = '${id}'`;
			
		db.query(sql, (error, result) => {
			res.json({ error: false, result: result.length > 0 ? result[0] : result });
		});
	});
};

exports.deleteToDo = (req, res) => {
	let id = req.params.id;
	if(!id) {
		res.json({ error: true, result: "ID required" });
	}

	let sql = `DELETE FROM todo WHERE id = '${id}'`;
	db.query(sql, (error, result) => {
		if(error) {
			res.json({ error: true, result: error });
		}

		res.json({ error: false, result: result });
	});
};
