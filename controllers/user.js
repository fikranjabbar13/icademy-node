var env = require('../env.json');
var conf = require('../configs/config');
var db = require('../configs/database');
var md5 = require('md5');

var multer = require('multer');
var storage = multer.diskStorage({
	destination: (req, file, cb) => {
		cb(null, './public/user');
	},
	filename: (req, file, cb) => {
		console.log(file);
		var filetype = '';
		if (file.mimetype === 'image/gif') {
			filetype = 'gif';
		}
		if (file.mimetype === 'image/png') {
			filetype = 'png';
		}
		if (file.mimetype === 'image/jpeg') {
			filetype = 'jpg';
		}
		cb(null, 'img-' + Date.now() + '.' + filetype);
	}
});
var uploadLogo = multer({ storage: storage }).single('avatar');

var { randomString } = require('./helper/generate');
var { sendEmailPassword } = require('./helper/sendEmail')

var storageEx = multer.diskStorage({
	destination: (req, file, cb) => {
		cb(null, './public/user');
	},
	filename: (req, file, cb) => {
		cb(null, 'excel-' + Date.now() + '-' + file.originalname);
	}
});
var uploadExcel = multer({ storage: storageEx }).single('excel');

function formatDate(date) {
	var d = new Date(date),
		month = '' + (d.getMonth() + 1),
		day = '' + d.getDate(),
		year = d.getFullYear();

	if (month.length < 2)
		month = '0' + month;
	if (day.length < 2)
		day = '0' + day;

	return [year, month, day].join('-');
}

function NOW() {

	var date = new Date();
	var aaaa = date.getFullYear();
	var gg = date.getDate();
	var mm = (date.getMonth() + 1);

	if (gg < 10)
		gg = "0" + gg;

	if (mm < 10)
		mm = "0" + mm;

	var cur_day = aaaa + "-" + mm + "-" + gg;

	var hours = date.getHours()
	var minutes = date.getMinutes()
	var seconds = date.getSeconds();

	if (hours < 10)
		hours = "0" + hours;

	if (minutes < 10)
		minutes = "0" + minutes;

	if (seconds < 10)
		seconds = "0" + seconds;

	return cur_day + " " + hours + ":" + minutes + ":" + seconds;
}

function lowerCase(value) {
	if (value) {
		return value.toString().toLowerCase();
	}
}

exports.testEmailPassword = async (req, res) => {
	let data = {
		name: 'Ahmad Ardiansyah',
		email: 'ardiansyah3ber@gmail.com',
		password: randomString(8),
		phone: '082334093822'
	}
	await sendEmailPassword(data)
	res.json({ status: 'OK' })
}

exports.konfirmasiMeeting = (req, res, next) => {
	var id = req.params.id;
	res.render('konfirmasi', { id: id });
}

exports.importUsers = (req, res, next) => {
	uploadExcel(req, res, (err) => {
		if (!req.file) {
			res.json({ error: true, result: err });
		} else {
			// create class Excel
			var Excel = require('exceljs');
			var wb = new Excel.Workbook();
			var path = require('path');
			var filePath = path.resolve(__dirname, '../public/user/' + req.file.filename);

			var form = {
				company_id: req.body.company_id,
				status: 'active',
				registered: NOW(),
				last_login: NOW(),
				validity: formatDate(Date.now())
			};

			wb.xlsx.readFile(filePath).then(async function () {
				var sh = wb.getWorksheet("Sheet1");
				console.log('countRow: ', sh.rowCount);

				var tempArray = [];
				for (i = 2; i <= sh.rowCount; i++) {
					// bikin excelnya dulu
					// 1     2       3      4         5        6         7       8       9         10        11
					// NO | GROUP | ROLE | NAMA | IDENTITY | VOUCHER | EMAIL | PHONE | ADDRESS | PASSWORD | LEVEL

					let emailParsed = ''
					if (sh.getRow(i).getCell(7).value){
						emailParsed = typeof sh.getRow(i).getCell(7).value === 'object' && typeof sh.getRow(i).getCell(7).value !== null ? sh.getRow(i).getCell(7).value.text : sh.getRow(i).getCell(7).value;
					}
					
					// check email exists
					let checkEmail = await db.query(`SELECT email FROM user WHERE email = '${emailParsed}' AND status='active'`);
					console.log('checkEmail: ', checkEmail);

					if (checkEmail.length != 1) {

						// check level
						let level = lowerCase(sh.getRow(i).getCell(11).value) === 'superadmin' ? 'superadmin' : lowerCase(sh.getRow(i).getCell(11).value) === 'admin' ? 'admin' : 'client';
						console.log('level: ', level);

						// check branch
						let branchId = await db.query(`SELECT branch_id FROM branch WHERE company_id = '${form.company_id}' AND branch_name = '${sh.getRow(i).getCell(2).value}'`);
						console.log('branchId: ', branchId);

						// check grup
						let grupId = await db.query(`SELECT grup_id FROM grup WHERE company_id = '${form.company_id}' AND grup_name = '${sh.getRow(i).getCell(3).value}'`);
						console.log('grupId: ', grupId)

						// check branch and grup must be exists
						if (branchId.length == 1 && grupId.length == 1) {
							console.log('Proses Insert');

							let sqlString = `INSERT INTO user (user_id, company_id, branch_id, grup_id,
				    		identity, voucher, name,
				    		email, phone, address, password,
				    		avatar, level,
				    		status, registered, last_login, validity, unlimited)
				    		VALUES (null, '${form.company_id}', '${branchId[0].branch_id}', '${grupId[0].grup_id}',
				    		'${sh.getRow(i).getCell(5).value}', '${sh.getRow(i).getCell(6).value === '' || sh.getRow(i).getCell(6).value === 'null' || sh.getRow(i).getCell(6).value === 'undefined' || sh.getRow(i).getCell(6).value === null ? '' : sh.getRow(i).getCell(6).value}', '${sh.getRow(i).getCell(4).value}',
				    		'${emailParsed}', '${sh.getRow(i).getCell(8).value}', '${sh.getRow(i).getCell(9).value}', '${md5(sh.getRow(i).getCell(10).value)}',
				    		'/assets/images/user/avatar-1.png', '${level}', '${form.status}', '${form.registered}', '${form.last_login}', '${form.validity}', '1')`;

							let userId = await db.query(sqlString);
							await db.query(`INSERT INTO user_group VALUES (null, '${userId.insertId}', '${branchId[0].branch_id}')`)
						}
					}

				}

				res.json({ error: false, result: 'Sukses import semua user.' });
			});

		}
	})
};

exports.createUser = (req, res, next) => {
	var formData = {
		company_id: req.body.company_id,
		branch_id: req.body.branch_id,
		grup_id: req.body.grup_id,
		group: req.body.group,
		identity: req.body.identity,
		name: req.body.name,
		email: req.body.email,
		phone: req.body.phone,
		address: req.body.address,
		password: md5(req.body.password),
		level: req.body.level,
		status: req.body.status,
		unlimited: req.body.unlimited,
		validity: req.body.validity
	};

	// checking email
	db.query(`SELECT email FROM user WHERE email = '${formData.email}' AND status='active'`, (error, result, fields) => {
		if (error) {
			res.json({ error: true, result: error });
		} else {
			if (result.length == 1) {
				res.json({ error: true, result: 'Email already taken.' });
			} else {
				// disable fungsi ini permintaan indigo
				// checking phone
				// db.query(`SELECT phone FROM user WHERE phone = '${formData.phone}'`, (error, result, fields) => {
				// 	if(error) {
				// 		res.json({error: true, result: error});
				// 	} else {
				// 		if(result.length == 1) {
				// 			res.json({error: true, result: 'Phone already taken.'})
				// 		} else {

				// create user
				db.query(`INSERT INTO user
								(user_id, company_id, branch_id, grup_id, identity, name, email, phone, address, password, level, status, registered, unlimited, validity) VALUES (
								null, '${formData.company_id}', '${formData.branch_id}', '${formData.grup_id}', '${formData.identity}', '${formData.name}', '${formData.email}',
								'${formData.phone}','${formData.address}','${formData.password}','${formData.level}','${formData.status}',
								'${conf.dateTimeNow()}','${formData.unlimited}','${formData.validity}')`, (error, result, fields) => {
					if (error) {
						res.json({ error: true, result: error });
					} else {
						db.query(`INSERT INTO user_setting (setting_id, user_id) VALUES (null, '${result.insertId}')`);

						for (let i = 0; i < formData.group.length; i++) {
							db.query(`INSERT INTO user_group (id, user_id, branch_id) VALUES (null, '${result.insertId}', '${formData.group[i]}')`);
						}
						db.query(`SELECT * FROM user WHERE user_id = '${result.insertId}'`, (error, result, fields) => {
							res.json({ error: false, result: result[0] });
						})
					}
				});

				// 	}
				// }

				// });
			}
		}
	});
};

exports.createUserPasswordEmail = async (req, res, next) => {
	let genPassword = randomString(8);
	var formData = {
		company_id: req.body.company_id,
		branch_id: req.body.branch_id,
		grup_id: req.body.grup_id,
		group: req.body.group,
		identity: req.body.identity,
		name: req.body.name,
		email: req.body.email,
		phone: req.body.phone,
		address: req.body.address,
		password: md5(genPassword),
		level: req.body.level,
		status: req.body.status,
		unlimited: req.body.unlimited,
		validity: req.body.validity
	};

	// checking email
	db.query(`SELECT email FROM user WHERE email = '${formData.email}' AND status='active'`, async (error, result, fields) => {
		if (error) {
			res.json({ error: true, result: error });
		} else {
			if (result.length == 1) {
				res.json({ error: true, result: 'Email already taken.' });
			} else {

				let state = true;
				if(req.body.level == 'admin'){
					let cekAdmin = await db.query(`
						SELECT COUNT(a.user_id) as count_admin FROM user a WHERE a.company_id ='${formData.company_id}' AND LOWER(a.level) = 'admin' AND LOWER(a.status) = 'active';
						SELECT COUNT(uc.user_id) as count_admin FROM  user_company uc  WHERE uc.company_id ='${formData.company_id}' and LOWER(uc.role) = 'admin';
					`);
					try {
						if (cekAdmin[0].length > 0) {
							if(cekAdmin[0][0].count_admin > 0) state = false;
						}
						if (cekAdmin[1].length > 0) {
							if(cekAdmin[1][0].count_admin > 0) state = false;
						}
					} catch (error) {
						
					}
					console.log(cekAdmin,"????")
				}
				if(state){

						// create user
						db.query(`INSERT INTO user
							(user_id, company_id, branch_id, grup_id, identity, name, email, phone, address, password, level, status, registered, unlimited, validity) VALUES (
							null, '${formData.company_id}', '${formData.branch_id}', '${formData.grup_id}', '${formData.identity}', '${formData.name}', '${formData.email}',
							'${formData.phone}','${formData.address}','${formData.password}','${formData.level}','${formData.status}',
							'${conf.dateTimeNow()}','${formData.unlimited}','${formData.validity}')`, (error, result, fields) => {
							if (error) {
								res.json({ error: true, result: error });
							} else {
								// send email
								let payload = {
									name: formData.name,
									password: genPassword,
									email: formData.email,
									phone: formData.phone
								};
								sendEmailPassword(payload);

								db.query(`INSERT INTO user_setting (setting_id, user_id) VALUES (null, '${result.insertId}')`);

								for (let i = 0; i < formData.group.length; i++) {
									db.query(`INSERT INTO user_group (id, user_id, branch_id) VALUES (null, '${result.insertId}', '${formData.group[i]}')`);
								}
								db.query(`SELECT * FROM user WHERE user_id = '${result.insertId}'`, (error, result, fields) => {
									res.json({ error: false, result: result[0] });
								})
							}
						});
					
				}else{
					return res.json({ error: true, result: 'Cannot duplicate admin level' });
				}
			}
		}
	});
};

exports.postCek = (req, res, next) => {
	if (req.query.search === '') {
		res.json({ error: false, result: `Please Insert Form.` })
	} else {
		db.query(`SELECT ${req.params.field} FROM user WHERE ${req.params.field} = '${req.query.search}' AND status = 'active'`, (error, result, field) => {
			if (error !== null) {
				res.json({ error: true, result: error });
			} else {
				if (result.length === 0) {
					res.json({ error: false, result: `${req.params.field} bisa digunakan.` })
				} else {
					res.json({ error: true, result: `${req.params.field} telah dipakai.` })
				}
			}
		})
	}
};

exports.getUserList = (req, res, next) => {
	db.query(`SELECT user.company_id, company.company_name, user.branch_id, branch.branch_name, grup.grup_id, grup.grup_name, user.*
						FROM user JOIN company ON company.company_id = user.company_id
						JOIN branch ON branch.branch_id = user.branch_id
						LEFT JOIN grup ON grup.grup_id = user.grup_id WHERE user.status = 'active'`, (error, result, fields) => {
		if (error) {
			res.json({ error: true, result: error });
		} else {
			res.json({
				error: false,
				result: result
			});
		}
	});
};

exports.getUserByCompany = (req, res, next) => {

	let usersbycompany = [];
	db.query(`SELECT company.company_name, GROUP_CONCAT(branch.branch_name) AS branch_name, grup.grup_name, GROUP_CONCAT(ug.branch_id) AS branch_id, user.*, 'internal' AS source
	FROM user JOIN company ON company.company_id = user.company_id
	LEFT JOIN user_group ug ON ug.user_id = user.user_id
	LEFT JOIN branch ON branch.branch_id = ug.branch_id
	LEFT JOIN grup ON grup.grup_id = user.grup_id
	WHERE company.company_id = '${req.params.company_id}' AND user.status = 'active' AND user.email NOT IN (SELECT email FROM training_user WHERE status = 'active') 
	-- AND ( user.validity >= NOW() OR user.unlimited = 1)
	GROUP BY user.user_id`, (error, result, fields) => {
		if (error) {
			res.json({ error: true, result: error });
		} else {
			usersbycompany.push(result)
			db.query(`SELECT company.company_name, GROUP_CONCAT(branch.branch_name) AS branch_name, grup.grup_name, GROUP_CONCAT(ug.branch_id) AS branch_id, user.*, uc.role,'external' AS source
			FROM user_company uc
			JOIN user ON user.user_id = uc.user_id
			JOIN company ON company.company_id = user.company_id
			LEFT JOIN user_group ug ON ug.user_id = user.user_id
			LEFT JOIN branch ON branch.branch_id = ug.branch_id
			LEFT JOIN grup ON grup.grup_id = user.grup_id
			WHERE uc.company_id = '${req.params.company_id}' AND user.status = 'active' AND user.email NOT IN (SELECT email FROM training_user) 
			-- AND ( user.validity >= NOW() OR user.unlimited = 1)
			AND uc.company_id != user.company_id
			GROUP BY user.user_id`, (error, result, fields) => {
				if (error) {
					res.json({ error: true, result: error });
				} else {
					if(result.length){
						if(result[0].role){
							result[0].level = result[0].role;
						}
					}
					usersbycompany.push(result)
					let obj = usersbycompany[0].concat(usersbycompany[1]);
					let results = obj.sort((a, b) => {
						if (a.name.toLowerCase() < b.name.toLowerCase()) {
							return -1;
						}
						if (a.name.toLowerCase() > b.name.toLowerCase()) {
							return 1;
						}
						return 0;
					})

					res.json({
						error: false,
						result: results
					});
				}
			})
		}
	});
};

exports.getAllUserByCompany = (req, res, next) => {

	let usersbycompany = [];
	db.query(`SELECT company.company_name, GROUP_CONCAT(branch.branch_name) AS branch_name, grup.grup_name, GROUP_CONCAT(ug.branch_id) AS branch_id, user.*
	FROM user JOIN company ON company.company_id = user.company_id
	LEFT JOIN user_group ug ON ug.user_id = user.user_id
	LEFT JOIN branch ON branch.branch_id = ug.branch_id
	LEFT JOIN grup ON grup.grup_id = user.grup_id
	WHERE company.company_id = '${req.params.company_id}' AND user.status = 'active'
	GROUP BY user.user_id`, (error, result, fields) => {
		if (error) {
			res.json({ error: true, result: error });
		} else {
			usersbycompany.push(result)
			db.query(`SELECT c.company_name, b.branch_name, g.grup_name, u.*
			FROM user_company uc
			JOIN user u ON uc.user_id = u.user_id
			JOIN company c ON uc.company_id = c.company_id
			JOIN branch b ON u.branch_id = b.branch_id
			JOIN grup g ON u.grup_id = g.grup_id
			WHERE uc.company_id = '${req.params.company_id}' AND u.status='active'`, (error, result, fields) => {
				if (error) {
					res.json({ error: true, result: error });
				} else {
					usersbycompany.push(result)
					res.json({
						error: false,
						result: usersbycompany[0].concat(usersbycompany[1])
					});
				}
			})
		}
	});
};

exports.getUserByGroup = (req, res, next) => {

	db.query(`SELECT GROUP_CONCAT(ug.user_id) AS user_id
	FROM user_group ug
	JOIN user u ON u.user_id=ug.user_id
	WHERE ug.branch_id='${req.params.group_id}' AND u.status='active'`, (error, result, fields) => {
		if (error) {
			res.json({ error: true, result: error });
		} else {
			res.json({ error: false, result: result[0] });
		}
	});
};

exports.getUserOne = (req, res, next) => {
	db.query(`SELECT user.*, (select distinct x.role from user_company x where x.user_id = user.user_id) as 'level_multicompany', 
	user.company_id, company.company_name, GROUP_CONCAT(branch.branch_name) AS group_name, GROUP_CONCAT(ug.branch_id) AS group_id 
	FROM user 
	LEFT JOIN company ON company.company_id = user.company_id 
	LEFT JOIN user_group ug ON ug.user_id=user.user_id 
	LEFT JOIN branch ON branch.branch_id = ug.branch_id 
	WHERE user.user_id = '${req.params.user_id}'`,
		(error, result, fields) => {
			if (error) {
				res.json({ error: true, result: result });
			} else {
				if (result.length == 0) {
					res.json({ error: false, result: result });
				} else {
					res.json({
						error: false,
						result: result[0]
					});
				}
			}
		})
};

exports.updateUser = async (req, res, next) => {
	var formData = {
		company_id: req.body.company_id,
		// branch_id: req.body.branch_id,
		group: req.body.group,
		grup_id: req.body.grup_id,
		identity: req.body.identity,
		name: req.body.name,
		email: req.body.email,
		phone: req.body.phone,
		validity: req.body.validity,
		unlimited: req.body.unlimited,
		address: req.body.address,
		level: req.body.level,
		status: req.body.status
	};
	// res.json(formData)
	// /**
	let result = await db.query(`select email,user_id from user where email='${req.body.email}'`);

	let state = true;
	if (result.length) {
		if ((result[0].user_id != req.params.user_id)) {
			state = false
		}
	}
	db.query(`
		SELECT
		COUNT(a.user_id) as count_admin
		FROM user a 
		WHERE a.company_id =? AND LOWER(a.level) = 'admin' AND LOWER(a.status) = 'active';
		SELECT
		lower(b.level) as level
		FROM user b
		where b.user_id = ? AND LOWER(b.status) = 'active';
		select email,user_id from user where email='${req.body.email}' AND LOWER(user.status) = 'active';
		`,
		[formData.company_id, req.params.user_id],
		async (err, result_check) => {
			if (err) {
				console.log(err, "ERROR")
				return res.json({ error: true, result: e });
			} else {

				if (result_check[2].length) {
					if (result_check[2][0].user_id != req.params.user_id) {
						return res.json({ error: true, result: 'Email has been already in used.' });
					}
				}
				//return console.log(result_check[1][0].level !== 'admin', formData.level === 'admin', result_check[0][0].count_admin > 0, formData, 9090)
				if (result_check[1][0].level !== 'admin' && formData.level === 'admin' && result_check[0][0].count_admin > 0) {

					return res.json({ error: true, result: 'Admin Level has been already in used.' });
				} else {

					await db.query(`UPDATE training_user
					SET name = ?,
						identity = ?,
						address = ?,
						phone = ?
					WHERE email = ?`, [formData.name, formData.identity, formData.address, formData.phone, formData.email])

					db.query(`UPDATE user SET
						company_id = '${formData.company_id}',
						grup_id = '${formData.grup_id}',
						identity = '${formData.identity}',
						name = '${formData.name}',
						email = '${formData.email}',
						phone = '${formData.phone}',
						address = '${formData.address}',
						level = '${formData.level}',
						unlimited = '${formData.unlimited}',
						validity = '${formData.validity}',
						status = '${formData.status}' WHERE user_id = '${req.params.user_id}'`,
						async (error, result, fields) => {
							if (error) {
								res.json({ error: true, result: error });
							} else {
								await db.query(`DELETE FROM user_group WHERE user_id ='${req.params.user_id}'`);
								for (let i = 0; i < formData.group.length; i++) {
									await db.query(`INSERT INTO user_group (id, user_id, branch_id) VALUES (null, '${req.params.user_id}', '${formData.group[i]}')`);
								}
								db.query(`SELECT * FROM user WHERE user_id = '${req.params.user_id}'`, (error, result, fields) => {
									res.json({ error: false, result: result[0] });
								})
							}
						});
					// */
				}
			}
		}
	);
};

exports.cekPassword = (req, res, next) => {
	var formData = {
		password: md5(req.body.password),
		user_id: req.body.user_id
	};

	db.query(`SELECT password FROM user WHERE
		password = '${formData.password}' AND user_id = '${formData.user_id}'`,
		(error, result, fields) => {
			if (error) {
				res.json({ error: true, result: error });
			} else {
				res.json({ error: false, result: result });
			}
		});
};

exports.updatePasswordUser = (req, res, next) => {
	var formData = {
		password: md5(req.body.password),
	};

	db.query(`UPDATE user SET
		password = '${formData.password}', is_new_password='1' WHERE user_id = '${req.params.user_id}'`,
		(error, result, fields) => {
			if (error) {
				res.json({ error: true, result: error });
			} else {
				res.json({ error: false, result: result });
			}
		});
};

exports.updateEmailUser = async (req, res, next) => {
	var formData = {
		email: req.body.email
	};

	let result = await db.query(`select email,user_id from user where email='${req.body.email}'`);

	let state = true;
	if (result.length) {
		if ((result[0].user_id != req.params.user_id)) {
			state = false
		}
	}

	if (state) {
		db.query(`UPDATE user SET
			email = '${formData.email}' WHERE user_id = '${req.params.user_id}'`,
			(error, result, fields) => {
				if (error) {
					res.json({ error: true, result: error });
				} else {
					res.json({ error: false, result: formData.email });
				}
			});
	} else {
		res.json({ error: true, result: "Cannot duplicate email" });
	}
};

exports.updateVoucherUser = (req, res, next) => {
	var formData = {
		voucher: req.body.voucher
	};

	db.query(`UPDATE user SET
		voucher = '${formData.voucher}' WHERE user_id = '${req.params.user_id}'`,
		(error, result, fields) => {
			if (error) {
				res.json({ error: true, result: error });
			} else {
				res.json({ error: false, result: formData.voucher });
			}
		});
};

exports.updateValidityUser = (req, res, next) => {
	var formData = {
		validity: conf.dateTimeNow()
	};

	db.query(`UPDATE user SET
		validity = '${formData.validity}' WHERE user_id = '${req.params.user_id}'`,
		(error, result, fields) => {
			if (error) {
				res.json({ error: true, result: error });
			} else {
				res.json({ error: false, result: result });
			}
		});
};

exports.updateActivePasive = (req, res, next) => {
	var form = { active: req.body.active }
	db.query(`UPDATE user SET
		status = '${form.active}' WHERE user_id = '${req.params.user_id}'`,
		(error, result, fields) => {
			if (error) {
				res.json({ error: true, result: error });
			} else {
				res.json({ error: false, result: form.active });
			}
		});
}


exports.updateAvatarUser = (req, res, next) => {
	uploadLogo(req, res, (err) => {
		if (!req.file) {
			db.query(`UPDATE user SET avatar=NULL WHERE user_id = '${req.params.user_id}'`, (error, result, fields) => {
				if (error) {
					res.json({ error: true, result: error });
				} else {
					res.json({ error: false, result: '' });
				}
			});
		} else {
			var formData = {
				avatar: `${env.APP_URL}/user/${req.file.filename}`,
			};

			db.query(`UPDATE user SET avatar = '${formData.avatar}' WHERE user_id = '${req.params.user_id}'`, (error, result, fields) => {
				if (error) {
					res.json({ error: true, result: error });
				} else {
					res.json({ error: false, result: formData.avatar });
				}
			});
		}
	});
};

exports.deleteUser = (req, res, next) => {
	db.query(`DELETE FROM user_setting WHERE user_id = '${req.params.user_id}'`);
	db.query(`DELETE FROM user WHERE user_id = '${req.params.user_id}'`, (error, result, next) => {
		if (error) {
			res.json({ error: true, result: error });
		} else {
			res.json({ error: false, result: result });
		}
	})
};

exports.getUserActivity = async (req, res, next) => {
	let totalEnrollCourse = await db.query(`SELECT * FROM user_course WHERE user_id = '${req.params.user_id}'`);
	let totalReplyForum = await db.query(`SELECT * FROM forum_post WHERE user_id = '${req.params.user_id}'`);
	let totalMeeting = await db.query(`SELECT * FROM user_activity WHERE tipe = 'liveclass' AND user_id = '${req.params.user_id}'`);

	res.json({
		error: false,
		result: {
			total_enroll_course: totalEnrollCourse.length != 0 ? totalEnrollCourse.length : 0,
			total_reply_forum: totalReplyForum.length != 0 ? totalReplyForum.length : 0,
			total_meeting: totalMeeting.length != 0 ? totalMeeting.length : 0
		}
	});
}

exports.getAssignUsers = (req, res, next) => {
	db.query(`SELECT uc.*, c.* FROM user_company uc
		JOIN company c ON c.company_id = uc.company_id
		JOIN user u ON u.user_id = uc.user_id
		WHERE uc.user_id = '${req.params.user_id}' AND uc.company_id != u.company_id`, (err, result, fields) => {
		if (err) {
			res.json({ error: true, result: err })
		} else {
			db.query(`SELECT uc.user_id, c.* FROM user uc JOIN company c ON c.company_id = uc.company_id
				WHERE uc.user_id = '${req.params.user_id}'`, (err2, result2, fields2) => {

				if (err2) {
					res.json({ error: true, result: err2 })
				} else {

					if (result2.length > 0) {
						result = result.concat(result2[0]);
					}

					let multiCompany = [];
					for (let i = 0; i < result.length; i++) {
						multiCompany.push(result[i].company_id)
					}
					multiCompany = [...new Set(multiCompany)];
					console.info(multiCompany)
					let formResult = {
						company: result,
						multi_company: multiCompany
					}
					res.json({ error: false, result: formResult })
				}
			})
		}
	})
}

exports.assignUsers = async (req, res, next) => {
	let form = {
		user_id: req.body.user_id,
		company_id: req.body.company_id,
		level_multicompany: req.body.level_multicompany || null,
		from_company: req.body.from_company
	};

	if(form.from_company == form.company_id){
		res.json({error: true, result: 'Cant Assign Same Company..'})
		return;
	};

	let state = true;
	if (form.level_multicompany === 'admin') {
		let cekAdmin = await db.query(`
			SELECT COUNT(a.user_id) as count_admin FROM user a WHERE a.company_id ='${form.company_id}' AND LOWER(a.level) = 'admin';
			SELECT COUNT(uc.user_id) as count_admin FROM  user_company uc  WHERE uc.company_id ='${form.company_id}' and LOWER(uc.role) = 'admin';
		`);
		try {
			if (cekAdmin[0].length > 0) {
				if(cekAdmin[0][0].count_admin > 0) state = false;
			}
			if (cekAdmin[1].length > 0) {
				if(cekAdmin[1][0].count_admin > 0) state = false;
			}
		} catch (error) {
			
		}
		console.log(cekAdmin,"????")
	}
	let cekAssign = await db.query(`SELECT * FROM user_company WHERE user_id = '${form.user_id}' AND company_id = '${form.company_id}'`);
	if (cekAssign.length == 0) {
		if (state) {
			db.query(`INSERT INTO user_company (user_company_id, user_id, company_id, role) VALUES (null, '${form.user_id}', '${form.company_id}','${form.level_multicompany}')`, (err, result, fields) => {
				if (err) {
					res.json({ error: true, result: err })
				} else {
					db.query(`SELECT * FROM user_company WHERE user_company_id = '${result.insertId}'`, (err, result, fields) => {
						res.json({ error: false, result: result[0] })
					})
				}
			})
		} else {
			res.json({ error: true, result: 'Cannot duplicate level admin' });
		}
	} else {
		res.json({ error: true, result: 'Assign already.' })
	}
}

exports.deleteAssignUsers = (req, res, next) => {
	db.query(`DELETE FROM user_company WHERE user_id = '${req.params.user_id}'`, (err, result, fields) => {
		if (err) {
			res.json({ error: true, result: err })
		} else {
			res.json({ error: false, result: result })
		}
	})
}

const nodemailer = require('nodemailer');
const makeid = (length) => {
	var result = '';
	var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
	var charactersLength = characters.length;
	for (var i = 0; i < length; i++) {
		result += characters.charAt(Math.floor(Math.random() * charactersLength));
	}
	return result;
}

exports.forgotPassword = (req, res) => {
	let transporter = nodemailer.createTransport({
		host: env.MAIL_HOST,
		port: env.MAIL_PORT,
		secure: env.MAIL_SECURE,
		auth: {
			user: env.MAIL_USER,
			pass: env.MAIL_PASS
		}
	});

	db.query(`SELECT user_id, name FROM user WHERE email = '${req.params.email}' AND status='active';`, (err, result) => {
		if (err) {
			res.json({ error: true, result: err })
		} else {
			if (result.length === 1) {
				result = result[0];
				result['OTP'] = makeid(4);

				db.query(`UPDATE user
					SET OTP = '${result.OTP}|${new Date().getTime()}'
					WHERE user_id = '${result.user_id}';`, (_err, _result) => {
					if (_err) {
						res.json({ error: true, result: _err })
					} else {
						transporter.sendMail({
							from: env.MAIL_USER,
							to: req.params.email,
							subject: 'Reset Password Akun ICADEMY',
							text: 'Reset Password Akun ICADEMY',
							html: `
								<b>Hi ${result.name},</b><br>
								Please follow the link below to change your password.<br>
								To maintain security, please change the password in the settings menu periodically.<br>
								<br>
								<b>OTP Code : ${result.OTP}</b>
								<br>
								Enter the OTP code on the forgot password page or click the link or copy paste it in your browser:<br>
								${env.WEB_URL}/reset-password/${result.user_id}/${result.OTP}
								`
						}).then(response => {
							if (!response.messageId) {
								res.json({ error: true, result: 'Gagal mengirim email' })
							} else {
								res.json({ error: false, result: { id: result.user_id, message: 'Email terkirim, Periksa email Anda.' } })
							}
						})
					}
				})
			} else {
				res.json({ error: true, result: 'Akun dengan email tersebut tidak ditemukan' })
			}
		}
	})


}

exports.resetPassword = (req, res) => {
	const params = req.params;
	/**
	 * confirm password checking
	 */
	if (params.password === params.confirm) {
		/**
		 * confirm password correct with password
		 */
		db.query(
			// `select SUBSTRING(OTP, 1, 4),  from user where user_id = '${params.id}';`
			`UPDATE user
			SET password = '${md5(params.password)}' ,
			otp = null
			WHERE user_id = '${params.id}'
			AND SUBSTRING(OTP, 1, 4) = '${params.otp}'
			AND ('${params.time}' - SUBSTRING(OTP, 6, LENGTH(OTP))) / (1000 * 60) < 1;`
			, (err, result) => {
				if (err) {
					res.json({ error: true, result: err })
				} else {
					if (result.affectedRows === 1) {
						res.json({ error: false, result: 'Password success updated' })
					} else {
						res.json({ error: true, result: 'Failed update password, please check again your OTP code' })
					}
				}
			})
	} else {
		/**
		 * confirm password incorrect with
		 */
		res.json({ error: true, result: 'incorect password' })
	}
}

exports.checkOTP = async (req, res) => {
	const otp = req.body.otp;
	const result = await db.query(`
		select a.user_id, a.email 
		from user a 
		where 
		SUBSTRING(OTP, 1, 4) = '${otp}' limit 1;`, [otp]);

	if (result.length > 0) {
		res.json({ error: false, result: { ...result[0], otp: req.body.otp } });
	} else {
		res.json({ error: true, result: 'OTP not verified' });
	}
}