const model = require("../repository").crmOperators;
const validator = require("../validator").crmOperators;

exports.storeOperators = async(req,res, next)=>{
	let msg = { error: false, result : [] }
	try {
        msg = await validator.store(req.body);
		if(!msg.error){
			msg.result.user_id = req.app.token.user_id;
			msg = await model.storeOperators(msg.result,req.params);
		}
	} catch (error) {
		console.log(error);
		msg.error = true;
		msg.result = error;
	}
	res.json(msg);
}

exports.getOperators = async(req,res, next)=>{
	let msg = { error: false, result : [] }
	try {
        msg = await model.getOperators(false,req.params);
	} catch (error) {
		console.log(error);
		msg.error = true;
		msg.result = error;
	}
	res.json(msg);
}