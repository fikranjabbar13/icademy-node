var env = require('../env.json');
var conf = require('../configs/config');
var db = require('../configs/database');
var multer = require('multer');
const send = require('./helper/sendEmail');
const moment = require('moment-timezone')
const bbb = require('bigbluebutton-js')
const BBB_URL = env.BBB_URL;
const BBB_KEY = env.BBB_KEY;
const moduleDB = require('../repository');
const validatorRequest = require('../validator');
const middleware = require("../middleware");

const nodemailer = require('nodemailer');

exports.sendMailGmail = async (req, res) => {
  const payload = { ...req.body };
  await send.sendEmailMeeting(payload);

  return await res.json({ error: false, result: 'success send email' });
}

const storage = multer.diskStorage({
  filename: function (req, file, cb) {
    cb(null, file.originalname);
  }
})
const AWS = require('aws-sdk');
const fs = require('fs');
const s3 = new AWS.S3({
  accessKeyId: env.AWS_ACCESS,
  secretAccessKey: env.AWS_SECRET
});
let uploadCover = multer({ storage: storage }).single('cover');

exports.getLiveClassByCompany = (req, res, next) => {
  db.query(`SELECT
              l.*,
              u.name,
              GROUP_CONCAT(p.user_id) AS participant
            FROM
              liveclass l
            LEFT JOIN user u ON (l.moderator IS NOT NULL AND l.moderator = u.user_id)
            LEFT JOIN liveclass_participant p ON l.class_id = p.class_id
            WHERE l.company_id = '${req.params.company_id}'
            GROUP BY l.class_id`, (error, result, fields) => {
    if (error) {
      res.json({ error: true, result: error })
    } else {
      res.json({ error: false, result: result })
    }
  })
}

exports.getLiveClassByCompanyUser = (req, res, next) => {
  let queryUser = `SELECT
      IF(ff.name IS NOT NULL, ff.name, '-') AS project_name,
      l.*,
      u.name,
      GROUP_CONCAT(p.user_id) AS participant,
      COUNT(lp.user_id) AS total_participant,
      LEFT(l.schedule_start, 10) AS tanggal,
      MID(l.schedule_start, 12, 8) AS waktu_start,
    MID(l.schedule_end,12, 8) AS waktu_end
    FROM
      liveclass l
    LEFT JOIN user u ON (l.moderator IS NOT NULL AND l.moderator = u.user_id)
    LEFT JOIN liveclass_participant p ON l.class_id = p.class_id
    LEFT JOIN liveclass_participant lp ON lp.class_id = l.class_id
    LEFT JOIN files_folder ff ON l.folder_id = ff.id
    WHERE l.company_id='${req.params.company_id}' AND ((l.is_private='1' AND p.user_id='${req.params.user_id}') OR l.is_private='0')
    GROUP BY l.class_id
    ORDER BY l.class_id DESC`;

  let queryAdmin = `SELECT
      IF(ff.name IS NOT NULL, ff.name, '-') AS project_name,
      l.*,
      u.name,
      GROUP_CONCAT(p.user_id) AS participant,
      COUNT(p.user_id) AS total_participant,
      LEFT(l.schedule_start, 10) AS tanggal,
      MID(l.schedule_start, 12, 8) AS waktu_start,
    MID(l.schedule_end,12, 8) AS waktu_end
    FROM
      liveclass l
    LEFT JOIN user u ON (l.moderator IS NOT NULL AND l.moderator = u.user_id)
    LEFT JOIN liveclass_participant p ON l.class_id = p.class_id
    LEFT JOIN files_folder ff ON l.folder_id = ff.id
    WHERE l.company_id = '${req.params.company_id}'
    GROUP BY l.class_id
    ORDER BY l.class_id DESC`

  db.query(req.params.level == 'client' ? queryUser : queryAdmin, async (error, result, fields) => {
    if (error) {
      res.json({ error: true, result: error })
    } else {
      for (var i = 0; i < result.length; i++) {
        // ===================================
        let getListBooking = await db.query(`
          SELECT
            b.*, u.name, um.name as moderator_name,
            b.date_start as tgl_mulai,
            b.date_end as tgl_selesai,
            (IF(NOW() BETWEEN cast(concat(b.tanggal, ' ', b.jam_mulai) as DATETIME) AND cast(concat(b.tanggal, ' ', b.jam_selesai) as DATETIME) != '1', '0', '1')) AS on_schedule
          FROM liveclass_booking b
            LEFT JOIN user u ON b.user_id=u.user_id
            LEFT JOIN user um ON b.moderator = um.user_id
          WHERE b.meeting_id='${result[i].class_id}' AND b.tanggal>=CURDATE()
          ORDER BY b.tanggal DESC`)

        result[i].on_schedule = false;
        for (var j = 0; j < getListBooking.length; j++) {
          const tanggal = getListBooking[j].tanggal
          getListBooking[j].tanggal = moment.tz(tanggal, 'Asia/Jakarta').format('DD-MM-YYYY')

          let api = bbb.api(BBB_URL, BBB_KEY)
          let http = bbb.http
          let checkUrl = api.monitoring.isMeetingRunning(getListBooking[j].id)
          let { returncode, running } = await http(checkUrl)
          getListBooking[j].running = returncode === 'SUCCESS' ? running : false

          // Check on schedule & user as participant
          let dataParticipants = await db.query(`SELECT p.* FROM liveclass_booking_participant p WHERE p.booking_id='${getListBooking[j].id}' AND p.user_id='${req.params.user_id}'`);
          if (getListBooking[j].on_schedule === "1" && dataParticipants.length) {
            result[i].on_schedule = true;
            result[i].on_schedule_id = getListBooking[j].id;
            result[i].isJoin = true;
          } else if (getListBooking[j].running === true && dataParticipants.length > 0) {
            result[i].on_schedule = true;
            result[i].on_schedule_id = getListBooking[j].id;
            result[i].isJoin = true;
          }

          //test
          // if (result[i].room_name === 'test agustriad') {
          //   if (getListBooking[j].id == 324 && req.params.user_id == 65) {
          //     console.log(returncode, running, "ON_SCHEDULE", 'BBB');
          //     console.log(getListBooking[j], "getListBooking");
          //     console.log(dataParticipants, 'dataParticipants');
          //     console.log(result[i], 'AGUS getLiveClassByCompanyUser2');
          //   }
          // }
        }


        let thisNow = moment()

        result[i].booking_count = getListBooking.length
        result[i].booking_upcoming = getListBooking.filter(x => x.running === true || thisNow.isBetween(x.tgl_mulai, x.tgl_selesai)).length
        result[i].booking_schedule = getListBooking.filter(x => x.running === true).length ? `${getListBooking.filter(x => x.running === true)[0].tanggal} ${getListBooking.filter(x => x.running === true)[0].jam_mulai} - ${getListBooking.filter(x => x.running === true)[0].jam_selesai}` : '-'
        //result[i].status = getListBooking.length === 0 ? 'Empty' : getListBooking.filter(x => x.running === true).length ? `Active` : `${result[i].booking_upcoming > 0 ? result[i].booking_upcoming : 'No'} upcoming meetings`;
        //result[i].status = getListBooking.length === 0 ? 'Empty' : getListBooking.filter(x => x.running === true).length ? `Active` : result[i].booking_upcoming > 0 && !getListBooking.filter(x => thisNow.isBetween(x.tgl_mulai, x.tgl_selesai))[0].running ? 'Open' : `${result[i].booking_upcoming > 0 ? result[i].booking_upcoming : 'No'} upcoming meetings`;

        if (getListBooking.length === 0) {

          result[i].status = 'Empty';
        } else if (getListBooking.filter(x => x.running === true).length > 0) {

          result[i].status = 'Active';
        } else {
          if (result[i].on_schedule) {
            result[i].status = 'Open'
          } else {

            let thisNow = new Date();
            let count_upcoming2 = getListBooking.filter(x => {
              let split_date = x.tanggal.split("-");
              if (new Date(x.tgl_mulai) >= thisNow && thisNow <= new Date(x.tgl_selesai)) {
                return true;
              } else {
                return false;
              }
            }).length;
            result[i].status = `${count_upcoming2 > 0 ? count_upcoming2 : 'No'} upcoming meetings`;
          }

        }

        result[i].room_status = result[i].status === 'Active' ? 'On going' : '-';

        // ===================================
      }

      // sort active status
      let active = [];
      let non_active = [];
      for (var i = 0; i < result.length; i++) {
        if (result[i].status === 'Active') {
          active.push(result[i])
        } else {
          non_active.push(result[i])
        }
      }

      result = active.concat(non_active);

      res.json({ error: false, result: result })
    }
  })
}

exports.getInfoLiveclass = (req, res, next) => {
  db.query(`SELECT
              l.*,
              u.name,
              GROUP_CONCAT(p.user_id) AS participant
            FROM
              liveclass l
            LEFT JOIN user u ON (l.moderator IS NOT NULL AND l.moderator = u.user_id)
            LEFT JOIN liveclass_participant p ON l.class_id = p.class_id
            WHERE l.class_id = '${req.params.class_id}'
            GROUP BY l.class_id`, (error, result, fields) => {
    if (error) {
      res.json({ error: true, result: error })
    } else {
      db.query(`SELECT u.user_id, u.name, p.participant_id, p.class_id, p.confirmation, p.actual
      FROM liveclass_participant p
      INNER JOIN user u ON u.user_id = p.user_id
      WHERE p.class_id='${req.params.class_id}'`, (error, result2, fields) => {
        if (error) {
          res.json({ error: true, result: error })
        } else {
          let participant = result2;
          result.push(participant)

          res.json({ error: false, result: result })
        }
      })
    }
  })
}

exports.getInfoLiveclassBooking = async (req, res, next) => {
  let checkMeetingId = await db.query(`SELECT meeting_id FROM liveclass_booking WHERE id = '${req.body.booking_id}'`)
  let meeting_id = req.body.meeting_id ? req.body.meeting_id : checkMeetingId[0].meeting_id;
  db.query(`SELECT
      l.class_id, l.room_name, l.cover, l.folder_id, l.engine,
        lb.*,
        DATE_FORMAT(lb.date_start,'%Y-%m-%d %H:%i:%s') AS tgl_mulai,
        DATE_FORMAT(lb.date_end ,'%Y-%m-%d %H:%i:%s') AS tgl_selesai, 
        u.name as moderator_name, u1.name as by_name,
        GROUP_CONCAT(lbp.user_id) as participants
    FROM
      liveclass l
        JOIN liveclass_booking lb ON l.class_id = lb.meeting_id AND l.class_id = ? AND lb.id = ?
        LEFT JOIN liveclass_booking_participant lbp ON lbp.booking_id = lb.id
        LEFT JOIN user u ON u.user_id = lb.moderator
        LEFT JOIN user u1 ON u1.user_id = lb.user_id`, [meeting_id, req.body.booking_id], (error, result, fields) => {
    if (error) {
      res.json({ error: true, result: error })
    }
    else {
      db.query(`SELECT u.user_id, CONCAT(u.name,' (',u.email,')') as name, u.email,p.participant_id, p.participant_id, if(p.actual = 'Hadir', 'Hadir', if(p.confirmation = 'Hadir', 'Hadir','')) as 'confirmation', p.actual, p.start_time 
      FROM liveclass_booking_participant p
      INNER JOIN user u ON u.user_id = p.user_id
      WHERE p.booking_id='${req.body.booking_id ? req.body.booking_id : 0}'`, async (error, result2, fields) => {
        if (error) {
          res.json({ error: true, result: error })
        } else {

          let guests = await db.query(`SELECT p.id, p.meeting_id as booking_id, p.email, IF(p.name is null, p.email ,concat(p.name, '(',p.email,')')) as name, IF(status=2,'Hadir','') AS confirmation, IF(status=2,'Hadir','') AS actual
            FROM liveclass_booking_tamu p WHERE p.meeting_id='${req.body.booking_id ? req.body.booking_id : 0}'`)
          let participant = result2.concat(guests);
          result.push(participant)

          try {

            let api = bbb.api(BBB_URL, BBB_KEY)
            let http = bbb.http
            let checkUrl = api.monitoring.isMeetingRunning(req.body.booking_id)
            let { returncode, running } = await http(checkUrl)
            result[0].running = returncode === 'SUCCESS' ? running : false

            result[0].isJoin = false;
            let split_participant = result[0].participants.split(",");
            let idx = split_participant.findIndex(str => {
              return parseInt(str) == req.app.token.user_id;
            });

            let delDuplicate = [];
            result[1].forEach((chk) => {
              if (chk.user_id) {

                let idx2 = delDuplicate.findIndex((chk2) => { return chk2.user_id == chk.user_id });
                if (idx2 == -1) {
                  delDuplicate.push(chk);
                }
              } else {
                delDuplicate.push(chk);
              }
            });

            result[1] = delDuplicate;

            if (idx > 0 && result[0].running == true) {
              result[0].isJoin = true;
            }

            let tz = result[0].timezone || 'Asia/Jakarta';
            result[0].tgl_mulai = moment.tz(result[0].tgl_mulai, tz)
            result[0].tgl_selesai = moment.tz(result[0].tgl_selesai, tz)
            result[0].date_start = moment.tz(result[0].date_start, tz)
            result[0].date_end = moment.tz(result[0].date_end, tz)
          } catch (e) {

          }

          res.json({ error: false, result: result })
        }
      })
    }
  })
}

exports.confirmAttendance = (req, res, next) => {
  let form = {
    class_id: req.params.class_id,
    user_id: req.params.user_id,
    confirmation: req.body.confirmation
  }
  db.query(`UPDATE liveclass_booking_participant SET confirmation = '${form.confirmation}' WHERE booking_id = '${req.params.class_id}' AND user_id = '${req.params.user_id}'`, (error, result, fields) => {
    if (error) {
      res.json({ error: true, result: error })
    } else {
      res.json({ error: false, result: result })
    }
  })
}

exports.actualAttendance = (req, res, next) => {
  let form = {
    class_id: req.params.class_id,
    user_id: req.params.user_id,
    confirmation: req.body.confirmation
  }
  db.query(`UPDATE liveclass_booking_participant SET actual = '${form.confirmation}' WHERE booking_id = '${req.params.class_id}' AND user_id = '${req.params.user_id}'`, (error, result, fields) => {
    if (error) {
      res.json({ error: true, result: error })
    } else {
      res.json({ error: false, result: result })
    }
  })
}

exports.updateRecentProject = (req, res, next) => {
  console.log(req.params, 'hasill')
  db.query(`UPDATE files_folder SET update_at = now() WHERE id = '${req.params.project_id}'`, (error, result, fields) => {
    if (error) {
      res.json({ error: true, result: error })
    } else {
      res.json({ error: false, result: { message: 'success' } })
    }
  })
}

exports.getLiveClassById = (req, res, next) => {

  const getDataQuery = `SELECT liveclass.*, files_folder.name AS project_name 
  FROM liveclass LEFT JOIN files_folder ON files_folder.id=liveclass.folder_id 
  WHERE liveclass.class_id = '${req.params.class_id}' LIMIT 1`;

  db.query(getDataQuery, (error, result, fields) => {
    if (error) {
      res.json({ error: true, result: error })
    } else {
      res.json({ error: false, result: result.length ? result[0] : [] })
    }
  })

  // db.query(getDataQuerywithupdate, (error, result, fields) => {
  //   if (error) {
  //     res.json({ error: true, result: error })
  //   } else {
  //     res.json({ error: false, result: result.length ? result[0] : [] })
  //   }
  // })
}

exports.getMeetPubById = (req, res, next) => {

  /*const accessGroup = [
    "SEND_MEETING",
    "SHARE_FILES",
    "R_TASK_TIMELINE",
    "R_FILES_FOLDER",
    "R_MEETING",
    "C_MOM",
    "SEND_CONFIRMATION",
    "CD_FILE_FOLDER"];*/

  let isLogin = req.app;

  const getDataQuery = `
    SELECT
      liveclass_booking.id AS booking_id,
      liveclass.room_name,
      liveclass.folder_id,
      liveclass.file_show,
      liveclass.share_gantt,
      files_folder.name AS project_name,
      liveclass_booking.*,
      DATE_FORMAT(liveclass_booking.date_start,'%Y-%m-%d %H:%i:%s') AS tgl_mulai,
      DATE_FORMAT(liveclass_booking.date_end ,'%Y-%m-%d %H:%i:%s') AS tgl_selesai,
      user.name AS moderator_name
    FROM liveclass
      LEFT JOIN files_folder ON files_folder.id = liveclass.folder_id
      LEFT JOIN liveclass_booking ON liveclass_booking.meeting_id = liveclass.class_id
      LEFT JOIN user ON user.user_id = liveclass_booking.moderator
    WHERE
      liveclass_booking.id = ?`;

  db.query(getDataQuery, [req.params.booking_id], async (error, result, fields) => {
    if (error) {
      res.json({ error: true, result: error })
    } else {
      if (result.length) {

        result[0].isTask = false;
        result[0].isShareFile = false;
        result[0].isFilesProject = false;
        result[0].isJoin = false;
        result[0].isInvite = false;
        result[0].isMoM = false;
        result[0].isSendConfirmation = false;
        result[0].isRole = 'participant';

        let sqlParti = `SELECT lbc.*, u.name FROM liveclass_booking_participant lbc JOIN user u ON u.user_id = lbc.user_id WHERE lbc.booking_id = ? ORDER BY lbc.participant_id ASC`;
        let dataParti = [req.params.booking_id]
        let participants = await db.query(sqlParti, dataParti)
        result[0].participants = participants

        let api = bbb.api(BBB_URL, BBB_KEY)
        let http = bbb.http
        let checkUrl = api.monitoring.isMeetingRunning(result[0].booking_id)
        let { returncode, running } = await http(checkUrl)
        result[0].is_running = returncode === 'SUCCESS' ? running : false

        let tz = result[0].timezone || 'Asia/Jakarta';

        result[0].tanggal = moment(result[0].tanggal).format('YYYY-MM-DD')
        result[0].tgl_mulai = moment.tz(result[0].tgl_mulai, tz)
        result[0].tgl_selesai = moment.tz(result[0].tgl_selesai, tz)
        //return console.log(result[0]);

        // Action
        if (req.app.token.user_id == result[0].user_id || (req.app.token.level === 'superadmin' || req.app.token.level === 'admin')) {
          result[0].isTask = true;
          result[0].isShareFile = true;
          result[0].isFilesProject = true;
          result[0].isJoin = true;
          result[0].isMoM = true;
          result[0].isSendConfirmation = true;
          result[0].isInvite = true;

          if (req.app.token.level === 'superadmin' || req.app.token.level === 'admin') {
            result[0].isRole = req.app.token.level;
          } else {
            result[0].isRole = 'owner';
          }
        }

        // isModerator
        if (result[0].moderator.toString() === req.app.token.user_id.toString()) {
          result[0].isRole = 'moderator';
          if (isLogin.accessInfo) {
            isLogin.accessInfo.forEach(arg => {
              if (arg.role === 'moderator' && arg.sub === 'meeting') {
                if (arg.code === 'SEND_MEETING' && arg.status) {
                  result[0].isInvite = true;
                }
                if (arg.code === 'SHARE_FILES' && arg.status) {
                  result[0].isShareFile = true;
                }
                if (arg.code === 'R_TASK_TIMELINE' && arg.status) {
                  result[0].isTask = true;
                }
                if (arg.code === 'CD_FILE_FOLDER' && arg.status) {
                  result[0].isFilesProject = true;
                }
                if (arg.code === 'C_MOM' && arg.status) {
                  result[0].isMoM = true;
                }
                if (arg.code === 'SEND_CONFIRMATION' && arg.status) {
                  result[0].isSendConfirmation = true;
                }
                if (arg.code === 'R_MEETING' && arg.status) {
                  result[0].isJoin = true;
                }

              }
            })
          }
        } else {
          isLogin.accessInfo.forEach(arg => {
            if (arg.role === 'participant' && arg.sub === 'meeting') {
              result[0].isRole = 'participant';
              if (arg.code === 'SEND_MEETING' && arg.status) {
                result[0].isInvite = true;
              }
              if (arg.code === 'SHARE_FILES' && arg.status) {
                result[0].isShareFile = true;
              }
              if (arg.code === 'R_TASK_TIMELINE' && arg.status) {
                result[0].isTask = true;
              }
              if (arg.code === 'CD_FILE_FOLDER' && arg.status) {
                result[0].isFilesProject = true;
              }
              if (arg.code === 'C_MOM' && arg.status) {
                result[0].isMoM = true;
              }
              if (arg.code === 'SEND_CONFIRMATION' && arg.status) {
                result[0].isSendConfirmation = true;
              }
              if (arg.code === 'R_MEETING' && arg.status) {
                result[0].isJoin = true;
              }

            }
          })
        }

        res.json({ error: false, result: result.length ? result[0] : [] })
      }
      else {
        res.json({ error: true, result: 'Booking meeting not found.' })
      }
    }
  })
}

exports.getMeetPubById_public = (req, res, next) => {

  /*const accessGroup = [
    "SEND_MEETING",
    "SHARE_FILES",
    "R_TASK_TIMELINE",
    "R_FILES_FOLDER",
    "R_MEETING",
    "C_MOM",
    "SEND_CONFIRMATION",
    "CD_FILE_FOLDER"];*/

  const getDataQuery = `
    SELECT
      liveclass_booking.id AS booking_id,
      liveclass.room_name,
      liveclass.folder_id,
      liveclass.file_show,
      liveclass.share_gantt,
      files_folder.name AS project_name,
      liveclass_booking.*,
      DATE_FORMAT(liveclass_booking.date_start,'%Y-%m-%d %H:%i:%s') AS tgl_mulai,
      DATE_FORMAT(liveclass_booking.date_end ,'%Y-%m-%d %H:%i:%s') AS tgl_selesai,
      user.name AS moderator_name
    FROM liveclass
      LEFT JOIN files_folder ON files_folder.id = liveclass.folder_id
      LEFT JOIN liveclass_booking ON liveclass_booking.meeting_id = liveclass.class_id
      LEFT JOIN user ON user.user_id = liveclass_booking.moderator
    WHERE
      liveclass_booking.id = ?`;

  db.query(getDataQuery, [req.params.booking_id], async (error, result, fields) => {
    if (error) {
      res.json({ error: true, result: error })
    } else {
      if (result.length) {

        result[0].isTask = false;
        result[0].isShareFile = false;
        result[0].isFilesProject = false;
        result[0].isJoin = false;
        result[0].isInvite = false;
        result[0].isMoM = false;
        result[0].isSendConfirmation = false;
        result[0].isRole = 'participant';

        let sqlParti = `SELECT lbc.*, u.name FROM liveclass_booking_participant lbc JOIN user u ON u.user_id = lbc.user_id WHERE lbc.booking_id = ? ORDER BY lbc.participant_id ASC`;
        let dataParti = [req.params.booking_id]
        let participants = await db.query(sqlParti, dataParti)
        result[0].participants = participants

        let api = bbb.api(BBB_URL, BBB_KEY)
        let http = bbb.http
        let checkUrl = api.monitoring.isMeetingRunning(result[0].booking_id)
        let { returncode, running } = await http(checkUrl)
        result[0].is_running = returncode === 'SUCCESS' ? running : false

        let tz = result[0].timezone || 'Asia/Jakarta';

        result[0].tanggal = moment(result[0].tanggal).format('YYYY-MM-DD')
        result[0].tgl_mulai = moment.tz(result[0].tgl_mulai, tz)
        result[0].tgl_selesai = moment.tz(result[0].tgl_selesai, tz)
        //return console.log(result[0]);

        isLogin = req.app;
        console.log(isLogin.accessInfo)
        if (isLogin.accessInfo) {

          isLogin.accessInfo.forEach(arg => {
            if (arg.role === 'participant' && arg.sub === 'meeting') {
              result[0].isRole = 'participant';
              if (arg.code === 'SEND_MEETING' && arg.status) {
                result[0].isInvite = true;
              }
              if (arg.code === 'SHARE_FILES' && arg.status) {
                result[0].isShareFile = true;
              }
              if (arg.code === 'R_TASK_TIMELINE' && arg.status) {
                result[0].isTask = true;
              }
              if (arg.code === 'CD_FILE_FOLDER' && arg.status) {
                result[0].isFilesProject = true;
              }
              if (arg.code === 'C_MOM' && arg.status) {
                result[0].isMoM = true;
              }
              if (arg.code === 'SEND_CONFIRMATION' && arg.status) {
                result[0].isSendConfirmation = true;
              }
              if (arg.code === 'R_MEETING' && arg.status) {
                result[0].isJoin = true;
              }

            }
          })
        }

        res.json({ error: false, result: result.length ? result[0] : [] })
      }
      else {
        res.json({ error: true, result: 'Booking meeting not found.' })
      }
    }
  })
}

exports.getLiveClassByProject = (req, res, next) => {
  let queryUser = `SELECT
      IF(ff.name IS NOT NULL, ff.name, '-') AS project_name,
      l.*,
      u.name,
      GROUP_CONCAT(p.user_id) AS participant,
      COUNT(lp.user_id) AS total_participant,
      LEFT(l.schedule_start, 10) AS tanggal,
      MID(l.schedule_start, 12, 8) AS waktu_start,
    MID(l.schedule_end,12, 8) AS waktu_end
    FROM
      liveclass l
    LEFT JOIN user u ON (l.moderator IS NOT NULL AND l.moderator = u.user_id)
    LEFT JOIN liveclass_participant p ON l.class_id = p.class_id
    LEFT JOIN liveclass_participant lp ON lp.class_id = l.class_id
    LEFT JOIN files_folder ff ON l.folder_id = ff.id
    WHERE l.folder_id='${req.params.project_id}' AND ((l.is_private='1' AND p.user_id='${req.params.user_id}') OR l.is_private='0')
    GROUP BY l.class_id
    ORDER BY l.class_id DESC`;

  let queryAdmin = `SELECT
      IF(ff.name IS NOT NULL, ff.name, '-') AS project_name,
      l.*,
      u.name,
      GROUP_CONCAT(p.user_id) AS participant,
      COUNT(p.user_id) AS total_participant,
      LEFT(l.schedule_start, 10) AS tanggal,
      MID(l.schedule_start, 12, 8) AS waktu_start,
    MID(l.schedule_end,12, 8) AS waktu_end
    FROM
      liveclass l
    LEFT JOIN user u ON (l.moderator IS NOT NULL AND l.moderator = u.user_id)
    LEFT JOIN liveclass_participant p ON l.class_id = p.class_id
    LEFT JOIN files_folder ff ON l.folder_id = ff.id
    WHERE l.folder_id='${req.params.project_id}'
    GROUP BY l.class_id
    ORDER BY l.class_id DESC`
  db.query(req.params.level == 'client' ? queryUser : queryAdmin, async (error, result, fields) => {
    if (error) {
      res.json({ error: true, result: error })
    } else {

      let AccessObj = {
        isCreateMeeting: false,
        isCreateWebinar: false,
        isRole: 'user'
      }
      // Action
      if (req.app.token.level === 'superadmin' || req.app.token.level === 'admin') {
        AccessObj.isCreateMeeting = true;
        AccessObj.isCreateWebinar = true;
        AccessObj.isRole = req.app.token.level;
      }

      if (req.app.accessInfo) {
        req.app.accessInfo.forEach((arg) => {

          if (arg.role === AccessObj.isRole && arg.sub === 'general' && arg.code === 'CD_WEBINAR' && arg.status) {
            AccessObj.isCreateWebinar = true;
          }
          if (arg.role === AccessObj.isRole && arg.sub === 'general' && arg.code === 'CD_MEETING' && arg.status) {
            AccessObj.isCreateMeeting = true;
          }
        })
      }

      for (var i = 0; i < result.length; i++) {

        // ===================================
        let getListBooking = await db.query(`
          SELECT
            b.*, u.name, um.name as moderator_name,
            b.date_start as tgl_mulai,
            b.date_end as tgl_selesai,
            (IF(NOW() BETWEEN cast(concat(b.tanggal, ' ', b.jam_mulai) as DATETIME) AND cast(concat(b.tanggal, ' ', b.jam_selesai) as DATETIME) != '1', '0', '1')) AS on_schedule
          FROM liveclass_booking b
            LEFT JOIN user u ON b.user_id=u.user_id
            LEFT JOIN user um ON b.moderator = um.user_id
          WHERE b.meeting_id='${result[i].class_id}' AND b.tanggal>=CURDATE()
          ORDER BY b.tanggal DESC`)

        result[i].on_schedule = false;
        for (var j = 0; j < getListBooking.length; j++) {
          const tanggal = getListBooking[j].tanggal
          getListBooking[j].tanggal = moment.tz(tanggal, 'Asia/Jakarta').format('DD-MM-YYYY')

          let api = bbb.api(BBB_URL, BBB_KEY)
          let http = bbb.http
          let checkUrl = api.monitoring.isMeetingRunning(getListBooking[j].id)
          let { returncode, running } = await http(checkUrl)
          getListBooking[j].running = returncode === 'SUCCESS' ? running : false

          // Check on schedule & user as participant
          let dataParticipants = await db.query(`SELECT p.* FROM liveclass_booking_participant p WHERE p.booking_id='${getListBooking[j].id}' AND p.user_id='${req.params.user_id}'`);
          if (getListBooking[j].on_schedule === "1" && dataParticipants.length) {
            result[i].on_schedule = true;
            result[i].on_schedule_id = getListBooking[j].id;
            result[i].isJoin = true;
          } else if (getListBooking[j].running === true && dataParticipants.length > 0) {
            result[i].on_schedule = true;
            result[i].on_schedule_id = getListBooking[j].id;
            result[i].isJoin = true;
          }
        }

        let thisNow = moment()

        result[i].booking_count = getListBooking.length
        result[i].booking_upcoming = getListBooking.filter(x => x.running === true || thisNow.isBetween(x.tgl_mulai, x.tgl_selesai)).length
        result[i].booking_schedule = getListBooking.filter(x => x.running === true).length ? `${getListBooking.filter(x => x.running === true)[0].tanggal} ${getListBooking.filter(x => x.running === true)[0].jam_mulai} - ${getListBooking.filter(x => x.running === true)[0].jam_selesai}` : '-'
        //result[i].status = getListBooking.length === 0 ? 'Empty' : getListBooking.filter(x => x.running === true).length ? `Active` : `${result[i].booking_upcoming > 0 ? result[i].booking_upcoming : 'No'} upcoming meetings`;
        //result[i].status = getListBooking.length === 0 ? 'Empty' : getListBooking.filter(x => x.running === true).length ? `Active` : result[i].booking_upcoming > 0 && !check_open ? 'Open' : `${result[i].booking_upcoming > 0 ? result[i].booking_upcoming : 'No'} upcoming meetings`;

        if (getListBooking.length === 0) {

          result[i].status = 'Empty';
        } else if (getListBooking.filter(x => x.running === true).length > 0) {

          result[i].status = 'Active';
        } else {
          if (result[i].on_schedule) {
            result[i].status = 'Open'
          } else {

            let thisNow = new Date();
            let count_upcoming2 = getListBooking.filter(x => {
              let split_date = x.tanggal.split("-");
              if (new Date(x.tgl_mulai) >= thisNow && thisNow <= new Date(x.tgl_selesai)) {
                return true;
              } else {
                return false;
              }
            }).length;
            result[i].status = `${count_upcoming2 > 0 ? count_upcoming2 : 'No'} upcoming meetings`;
          }

        }

        result[i].room_status = result[i].status === 'Active' ? 'On going' : '-';

        // ===================================

      }

      // sort active status
      let active = [];
      let non_active = [];
      for (var i = 0; i < result.length; i++) {
        if (result[i].status === 'Active') {
          active.push(result[i])
        } else {
          non_active.push(result[i])
        }
      }

      result = active.concat(non_active);

      res.json({ error: false, result: result.length ? result : [], access: AccessObj })
    }
  })
}

exports.getLiveClassByWebinar = (req, res, next) => {
  let queryUser = `SELECT
      IF(ff.name IS NOT NULL, ff.name, '-') AS project_name,
      l.*,
      u.name,
      GROUP_CONCAT(p.user_id) AS participant,
      COUNT(lp.user_id) AS total_participant,
      LEFT(l.schedule_start, 10) AS tanggal,
      MID(l.schedule_start, 12, 8) AS waktu_start,
    MID(l.schedule_end,12, 8) AS waktu_end
    FROM
      liveclass l
    LEFT JOIN user u ON (l.moderator IS NOT NULL AND l.moderator = u.user_id)
    LEFT JOIN liveclass_participant p ON l.class_id = p.class_id
    LEFT JOIN liveclass_participant lp ON lp.class_id = l.class_id
    LEFT JOIN files_folder ff ON l.folder_id = ff.id
    WHERE l.folder_id='${req.params.webinar_id}' AND ((l.is_private='1' AND p.user_id='${req.params.user_id}') OR l.is_private='0')
    GROUP BY l.class_id
    ORDER BY l.class_id DESC`;

  let queryAdmin = `SELECT
      IF(ff.name IS NOT NULL, ff.name, '-') AS project_name,
      l.*,
      u.name,
      GROUP_CONCAT(p.user_id) AS participant,
      COUNT(p.user_id) AS total_participant,
      LEFT(l.schedule_start, 10) AS tanggal,
      MID(l.schedule_start, 12, 8) AS waktu_start,
    MID(l.schedule_end,12, 8) AS waktu_end
    FROM
      liveclass l
    LEFT JOIN user u ON (l.moderator IS NOT NULL AND l.moderator = u.user_id)
    LEFT JOIN liveclass_participant p ON l.class_id = p.class_id
    LEFT JOIN files_folder ff ON l.folder_id = ff.id
    WHERE l.webinar_id='${req.params.webinar_id}'
    GROUP BY l.class_id
    ORDER BY l.class_id DESC`
  db.query(req.params.level == 'client' ? queryUser : queryAdmin, async (error, result, fields) => {
    if (error) {
      res.json({ error: true, result: error })
    } else {
      for (var i = 0; i < result.length; i++) {
        // ===================================
        let getListBooking = await db.query(`
          SELECT
            b.*, u.name, um.name as moderator_name,
            b.date_start as tgl_mulai,
            b.date_end as tgl_selesai,
            (IF(NOW() BETWEEN cast(concat(b.tanggal, ' ', b.jam_mulai) as DATETIME) AND cast(concat(b.tanggal, ' ', b.jam_selesai) as DATETIME) != '1', '0', '1')) AS on_schedule
          FROM liveclass_booking b
            LEFT JOIN user u ON b.user_id=u.user_id
            LEFT JOIN user um ON b.moderator = um.user_id
          WHERE b.meeting_id='${result[i].class_id}' AND b.tanggal>=CURDATE()
          ORDER BY b.tanggal DESC`)

        result[i].on_schedule = false;
        for (var j = 0; j < getListBooking.length; j++) {
          const tanggal = getListBooking[j].tanggal
          getListBooking[j].tanggal = moment.tz(tanggal, 'Asia/Jakarta').format('DD-MM-YYYY')

          let api = bbb.api(BBB_URL, BBB_KEY)
          let http = bbb.http
          let checkUrl = api.monitoring.isMeetingRunning(getListBooking[j].id)
          let { returncode, running } = await http(checkUrl)
          getListBooking[j].running = returncode === 'SUCCESS' ? running : false

          // Check on schedule & user as participant
          let dataParticipants = await db.query(`SELECT p.* FROM liveclass_booking_participant p WHERE p.booking_id='${getListBooking[j].id}' AND p.user_id='${req.params.user_id}'`);
          if (getListBooking[j].on_schedule === "1" && dataParticipants.length) {
            result[i].on_schedule = true;
            result[i].on_schedule_id = getListBooking[j].id;
            result[i].isJoin = true;
          } else if (getListBooking[j].running === true && dataParticipants.length > 0) {
            result[i].on_schedule = true;
            result[i].on_schedule_id = getListBooking[j].id;
            result[i].isJoin = true;
          }
        }

        let thisNow = moment()

        result[i].booking_count = getListBooking.length
        result[i].booking_upcoming = getListBooking.filter(x => x.running === true || thisNow.isBetween(x.tgl_mulai, x.tgl_selesai)).length
        result[i].booking_schedule = getListBooking.filter(x => x.running === true).length ? `${getListBooking.filter(x => x.running === true)[0].tanggal} ${getListBooking.filter(x => x.running === true)[0].jam_mulai} - ${getListBooking.filter(x => x.running === true)[0].jam_selesai}` : '-'
        //result[i].status = getListBooking.length === 0 ? 'Empty' : getListBooking.filter(x => x.running === true).length ? `Active` : `${result[i].booking_upcoming > 0 ? result[i].booking_upcoming : 'No'} upcoming meetings`;
        //result[i].status = getListBooking.length === 0 ? 'Empty' : getListBooking.filter(x => x.running === true).length ? `Active` : result[i].booking_upcoming > 0 && !getListBooking.filter(x => thisNow.isBetween(x.tgl_mulai, x.tgl_selesai))[0].running ? 'Open' : `${result[i].booking_upcoming > 0 ? result[i].booking_upcoming : 'No'} upcoming meetings`;

        if (getListBooking.length === 0) {

          result[i].status = 'Empty';
        } else if (getListBooking.filter(x => x.running === true).length > 0) {

          result[i].status = 'Active';
        } else {
          if (result[i].on_schedule) {
            result[i].status = 'Open'
          } else {

            let thisNow = new Date();
            let count_upcoming2 = getListBooking.filter(x => {
              let split_date = x.tanggal.split("-");
              if (new Date(x.tgl_mulai) >= thisNow && thisNow <= new Date(x.tgl_selesai)) {
                return true;
              } else {
                return false;
              }
            }).length;
            result[i].status = `${count_upcoming2 > 0 ? count_upcoming2 : 'No'} upcoming meetings`;
          }

        }

        result[i].room_status = result[i].status === 'Active' ? 'On going' : '-';

        // ===================================
      }

      // sort active status
      let active = [];
      let non_active = [];
      for (var i = 0; i < result.length; i++) {
        if (result[i].status === 'Active') {
          active.push(result[i])
        } else {
          non_active.push(result[i])
        }
      }

      result = active.concat(non_active);

      res.json({ error: false, result: result.length ? result : [] })
    }
  })
}

exports.postLiveClassByCompany = async (req, res, next) => {

  let checks = await middleware.checkAccess({
    company_id: req.body.company_id.toString(),
    sub: 'general',
    role: req.app.token.level === 'superadmin' ? 'admin' : req.app.token.level,
    code: ['CD_MEETING']
  }, req);

  checks.map((str) => {
    if (!str.status) {
      return res.json({ error: true, result: `You dont have permission for this action, please contact your administrator.`, access: checks });
    }
  });

  let form = {
    userId: req.body.user_id,
    companyId: req.body.company_id,
    roomName: req.body.room_name,
    folderId: req.body.folder_id.length ? 0 : req.body.folder_id,

    // speaker: req.body.speaker,
    // moderator: req.body.is_akses === 1 ? req.body.moderator.length <= 0 ? [0] : req.body.moderator : [0],
    webinar_id: req.body.webinar_id.length <= 0 ? 0 : req.body.webinar_id,
    // is_private: req.body.is_private,
    // is_akses: req.body.is_akses,
    // is_required_confirmation: req.body.is_required_confirmation,
    // is_scheduled: req.body.is_scheduled,
    // schedule_start: req.body.schedule_start,
    // schedule_end: req.body.schedule_end,
    // peserta: req.body.peserta,

    engine: req.body.engine,
    // mode: req.body.mode
  }

  // console.log(form)

  db.query(`INSERT INTO liveclass (company_id, user_id, room_name, folder_id, engine, webinar_id) VALUES (?)`,
    [[form.companyId, form.userId, form.roomName, form.folderId, form.engine, form.webinar_id]],
    (error, result, fields) => {
      if (error) {
        res.json({ error: true, result: error, access: checks })
      } else {

        // insert to liveclass_zoom
        if (form.engine === 'zoom') {
          db.query(`INSERT INTO liveclass_zoom (class_id, zoom_id, pwd) VALUES (?)`, [[result.insertId, '4912503275', 'hoster']])
        }

        db.query(`SELECT * FROM liveclass WHERE class_id = '${result.insertId}'`, (error, result, fields) => {
          res.json({ error: false, result: result[0], access: checks })
        })

        /**
        db.query(`SELECT * FROM liveclass WHERE class_id = '${result.insertId}'`, (error, result, fields) => {
          if (form.is_private == 1) {
            if (form.moderator != null && form.moderator != '' && form.moderator != 0 && form.is_akses == 1) {
              // db.query(`INSERT INTO liveclass_participant (class_id, user_id, confirmation, actual)
              // SELECT * FROM (SELECT '${result[0].class_id}', '${form.moderator}', '' AS confirmation, '' AS actual) AS tmp
              // WHERE NOT EXISTS (
              //     SELECT class_id, user_id FROM liveclass_participant WHERE class_id = '${result[0].class_id}' AND user_id = '${form.moderator}'
              // ) LIMIT 1;`)
              db.query(`INSERT INTO liveclass_participant (class_id, user_id, confirmation, actual) VALUES ('${result[0].class_id}','${form.moderator}', '', '')`);
            }
            for (let i = 0; i < form.peserta.length; i++) {
              // db.query(`INSERT INTO liveclass_participant (class_id, user_id, confirmation, actual)
              // SELECT * FROM (SELECT '${result[0].class_id}', '${form.peserta[i]}', '' AS confirmation, '' AS actual) AS tmp
              // WHERE NOT EXISTS (
              //     SELECT class_id, user_id FROM liveclass_participant WHERE class_id = '${result[0].class_id}' AND user_id = '${form.peserta[i]}'
              // ) LIMIT 1;`);
              db.query(`DELETE FROM liveclass_participant WHERE class_id='${result[0].class_id}' AND user_id='${form.peserta[i]}'`, (error, result2, fields) => {
                if (error) {
                  console.log('error:', error)
                }
                else {
                  db.query(`INSERT INTO liveclass_participant (class_id, user_id, confirmation, actual) VALUES ('${result[0].class_id}','${form.peserta[i]}', '', '')`);
                }
              });
              if (i === form.peserta.length - 1) {
                res.json({ error: false, result: result[0] })
              }
            }
          }
          else {
            res.json({ error: false, result: result[0] })
          }
        })
        */
      }
    })
}

exports.updateLiveClass = (req, res, next) => {
  let form = {
    roomName: req.body.room_name,
    // moderator: req.body.moderator.length <= 0 ? [0] : req.body.moderator,
    folderId: req.body.folder_id.length <= 0 ? [0] : req.body.folder_id,
    // is_akses: req.body.is_akses,
    // is_private: req.body.is_private,
    // is_required_confirmation: req.body.is_required_confirmation,
    // is_scheduled: req.body.is_scheduled,
    // schedule_start: req.body.schedule_start,
    // schedule_end: req.body.schedule_end,
    // peserta: req.body.peserta,
    engine: req.body.engine
    // mode: req.body.mode
  }

  db.query(`UPDATE liveclass SET 
              room_name = ?,
              folder_id = ?,
              engine = ?
            WHERE class_id = ?`, [form.roomName, form.folderId, form.engine, req.params.class_id], (error, result, fields) => {
    if (error) {
      res.json({ error: true, result: error })
    } else {

      // delete liveclass zoom if not zoom
      if (form.engine === 'zoom') {
        db.query(`SELECT * FROM liveclass_zoom WHERE class_id = ?`, [req.params.class_id], (err, ress, fiel) => {
          if (!ress.length) {
            db.query(`INSERT INTO liveclass_zoom (class_id, zoom_id, pwd) VALUES (?)`, [[req.params.class_id, '4912503275', 'hoster']])
          }
        })
      }
      else {
        db.query(`DELETE FROM liveclass_zoom WHERE class_id = ?`, [req.params.class_id])
      }

      db.query(`SELECT * FROM liveclass WHERE class_id = '${req.params.class_id}'`, (error, result, fields) => {
        res.json({ error: false, result: result[0] })
      })


      /**
      db.query(`SELECT * FROM liveclass WHERE class_id = '${req.params.class_id}'`, (error, result, fields) => {
        if (form.is_private == 1) {
          for (let i = 0; i < form.peserta.length; i++) {
            db.query(`DELETE FROM liveclass_participant WHERE class_id='${req.params.class_id}' AND user_id='${form.peserta[i]}'`, (error, result, fields) => {
              if (error) {
                console.log('error:', error)
              }
              else {
                db.query(`INSERT INTO liveclass_participant (class_id, user_id, confirmation, actual) VALUES ('${req.params.class_id}','${form.peserta[i]}', '', '')`);
              }
            });
          }
          if (form.moderator !== null && form.moderator !== '' && form.moderator !== 0 && form.is_akses == 1) {
            db.query(`DELETE FROM liveclass_participant WHERE class_id='${req.params.class_id}' AND user_id='${form.moderator}'`, (error, result, fields) => {
              if (error) {
                console.log('error:', error)
              }
              else {
                db.query(`INSERT INTO liveclass_participant (class_id, user_id, confirmation, actual) VALUES ('${req.params.class_id}','${form.moderator}', '', '')`)
              }
            });
          }
        }
        res.json({ error: false, result: result[0] })
      })
      */
    }
  })
}

exports.updateCoverLiveClass = (req, res, next) => {
  uploadCover(req, res, (err) => {
    if (!req.file) {
      res.json({ error: true, result: err });
    } else {

      let path = req.file.path;
      let newDate = new Date();
      let keyName = `${env.AWS_BUCKET_ENV}/liveclass`;

      var params = {
        ACL: 'public-read',
        Bucket: env.AWS_BUCKET,
        Body: fs.createReadStream(path),
        Key: `${keyName}/${req.file.originalname}`
      };

      s3.upload(params, (err, data) => {
        if (err) {
          res.json({ error: true, msg: "Error Upload Image", result: err });
        }
        if (data) {
          fs.unlinkSync(path);

          let form = {
            cover: data.Location
          }

          db.query(`UPDATE liveclass SET cover = '${form.cover}' WHERE class_id = '${req.params.class_id}'`, (error, result, fields) => {
            if (error) {
              res.json({ error: true, result: error })
            } else {
              res.json({ error: false, result: form.cover })
            }
          })

        }
      })
    }
  })
}

exports.updateIsLiveClass = (req, res, next) => {
  let form = {
    isLive: req.body.is_live
  }

  db.query(`UPDATE liveclass_booking SET is_live = '${form.isLive}' WHERE id = '${req.params.class_id}'`, (error, result, fields) => {
    if (error) {
      res.json({ error: true, result: error })
    } else {
      db.query(`SELECT * FROM liveclass_booking WHERE id = '${req.params.class_id}'`, (error, result, fields) => {
        res.json({ error: false, result: result[0] })
      })
    }
  })
}

exports.lockMeeting = (req, res, next) => {
  let form = {
    lock: req.body.lock
  }

  db.query(`UPDATE liveclass_booking SET is_locked = '${form.lock}' WHERE id = '${req.params.class_id}'`, (error, result, fields) => {
    if (error) {
      res.json({ error: true, result: error })
    } else {
      db.query(`SELECT * FROM liveclass_booking WHERE id = '${req.params.class_id}'`, (error, result, fields) => {
        res.json({ error: false, result: result[0] })
      })
    }
  })
}

exports.updateShareGantt = (req, res, next) => {
  db.query(`UPDATE liveclass SET share_gantt = '${req.body.projectId}' WHERE class_id = '${req.params.class_id}'`, (error, result, fields) => {
    if (error) {
      res.json({ error: true, result: error })
    } else {
      db.query(`SELECT * FROM liveclass WHERE class_id = '${req.params.class_id}'`, (error, result, fields) => {
        res.json({ error: false, result: result[0] })
      })
    }
  })
}

exports.updateFileShow = (req, res, next) => {
  db.query(`UPDATE liveclass SET file_show = '${req.body.selectedFileShow}' WHERE class_id = '${req.params.class_id}'`, (error, result, fields) => {
    if (error) {
      res.json({ error: true, result: error })
    } else {
      db.query(`SELECT * FROM liveclass WHERE class_id = '${req.params.class_id}'`, (error, result, fields) => {
        res.json({ error: false, result: result[0] })
      })
    }
  })
}

exports.updateActiveParticipants = (req, res, next) => {
  let form = {
    numberOfParticipants: req.body.numberOfParticipants
  }
  let activeParticipants = form.numberOfParticipants < 1 ? 0 : form.numberOfParticipants;
  db.query(`UPDATE liveclass SET active_participants = ${activeParticipants} WHERE class_id = ${req.params.class_id}`, (error, result, fields) => {
    if (error) {
      res.json({ error: true, result: error })
    } else {
      db.query(`SELECT * FROM liveclass WHERE class_id = '${req.params.class_id}'`, (error, result, fields) => {
        res.json({ error: false, result: result[0] })
      })
    }
  })
}

exports.getActiveParcicipantByRoomId = (req, res, next) => {
  db.query(`SELECT * FROM liveclass WHERE class_id = '${req.params.class_id}'`, (error, result, fields) => {
    res.json({ error: false, result: result[0] })
  })
}

exports.deleteLiveClassById = (req, res, next) => {
  db.query(`DELETE FROM liveclass WHERE class_id = '${req.params.class_id}'`, (error, result, fields) => {
    if (error) {
      res.json({ error: true, result: error })
    } else {
      db.query(`DELETE FROM calendar WHERE type='3' AND activity_id='${req.params.class_id}'`, (error, result, fields) => {
        if (error) {
          res.json({ error: true, result: error })
        } else {
          res.json({ error: false, result: result })
        }
      })
    }
  })
}

let uploadFile = multer({ storage: storage }).single('file');

exports.uploadFile = (req, res, next) => {
  uploadFile(req, res, (err) => {
    if (!req.file) {
      res.json({ error: true, result: err });
    } else {

      let path = req.file.path;
      let newDate = new Date();
      let keyName = `${env.AWS_BUCKET_ENV}/attachment`;

      var params = {
        ACL: 'public-read',
        Bucket: env.AWS_BUCKET,
        Body: fs.createReadStream(path),
        Key: `${keyName}/${req.file.originalname}`
      };

      s3.upload(params, (err, data) => {
        console.log('err', err)
        console.log('data', data)
        if (err) {
          res.json({ error: true, msg: "Error Upload Image", result: err });
        }
        if (data) {
          fs.unlinkSync(path);

          let form = {
            file: data.Location,
            pengirim: req.body.pengirim,
            class_id: req.body.class_id,
          }

          db.query(`INSERT INTO liveclass_file (class_id, pengirim, attachment) VALUES ('${form.class_id}', '${form.pengirim}', '${form.file}')`, (error, result, fields) => {
            if (error) {
              res.json({ error: true, result: error })
            } else {
              db.query(`SELECT liveclass_file.*, user.name FROM liveclass_file JOIN user ON liveclass_file.pengirim=user.user_id WHERE file_id = '${result.insertId}'`, (error, result, fields) => {
                res.json({ error: false, result: result[0] })
              })
            }
          })

        }
      })
    }
  })
}

exports.deleteFile = (req, res, next) => {
  db.query(`DELETE FROM liveclass_file WHERE class_id = '${req.params.class_id}'`, (error, result, fields) => {
    if (error) {
      res.json({ error: true, result: error })
    } else {
      res.json({ error: false, result: result })
    }
  })
}

exports.deleteFileByAttachment = (req, res, next) => {
  db.query(`DELETE FROM liveclass_file WHERE attachment = '${req.body.attachment}'`, (error, result, fields) => {
    if (error) {
      res.json({ error: true, result: error })
    } else {
      res.json({ error: false, result: result })
    }
  })
}

exports.getFileSharingById = (req, res, next) => {
  db.query(`SELECT liveclass_file.*, user.name FROM liveclass_file JOIN user ON liveclass_file.pengirim=user.user_id WHERE class_id = '${req.params.class_id}'`, (error, result, fields) => {
    if (error) {
      res.json({ error: true, result: error })
    } else {
      res.json({ error: false, result: result })
    }
  })
}

exports.getFilePubById = (req, res, next) => {
  let sql = `
    SELECT
      liveclass_booking.id as booking_id,
      liveclass_booking.meeting_id,
      liveclass_file.*, user.name
    FROM
      liveclass_file
      JOIN user ON liveclass_file.pengirim = user.user_id
      JOIN liveclass_booking ON liveclass_booking.id = liveclass_file.class_id
    WHERE
      liveclass_booking.id = ?`;

  db.query(sql, [req.params.booking_id], (error, result, fields) => {
    if (error) {
      res.json({ error: true, result: error })
    } else {
      res.json({ error: false, result: result })
    }
  })
}

exports.addParticipantMeetPub = (req, res) => {
  let { user_id, meeting_id, name, email } = req.body
  if (user_id) {
    let sqlCheck = "SELECT * FROM liveclass_booking_participant WHERE booking_id = ? AND user_id = ?"
    let dataCheck = [meeting_id, user_id]
    db.query(sqlCheck, dataCheck, (error, result) => {
      if (error) {
        res.json({ error: true, result: error })
      }
      else {
        if (result.length === 0) {
          let sql = "INSERT INTO liveclass_booking_participant (booking_id, user_id) VALUES (?)"
          let form = [[meeting_id, user_id]]
          db.query(sql, form, (error, result) => {
            if (error) {
              res.json({ error: true, result: error })
            }
            else {
              res.json({ error: false, result: result })
            }
          })
        }
        else {
          res.json({ error: true, result: result, message: 'You are ready with this meeting.' })
        }
      }
    })
  }
  else {
    let sqlCheck = "SELECT * FROM liveclass_booking_tamu WHERE meeting_id = ? AND (name = ? OR email = ?)"
    let dataCheck = [meeting_id, name, email]
    db.query(sqlCheck, dataCheck, (error, result) => {
      if (error) {
        res.json({ error: true, result: error })
      }
      else {
        if (result.length === 0) {
          let sql = "INSERT INTO liveclass_booking_tamu (meeting_id, name, email, status) VALUES (?)"
          let form = [[meeting_id, name, email, 2]]
          db.query(sql, form, (error, result) => {
            if (error) {
              res.json({ error: true, result: error })
            }
            else {
              res.json({ error: false, result: result })
            }
          })
        }
        else {
          result.forEach((str) => {
            if (!str.name && !str.voucher && str.status == 1) {
              db.query(`UPDATE liveclass_booking_tamu SET status=2, name='${name}' where email='${email}' and meeting_id='${meeting_id}';`, (err, result) => {
                if (error) {
                  res.json({ error: true, result: error })
                }
                else {
                  res.json({ error: false, result: result })
                }
              });
            } else {
              res.json({ error: true, result: result, message: 'You are ready with this meeting.' })
            }
          });
        }
      }
    })
  }
}

exports.getClassByCompany = async (req, res, next) => {

  let ifInvite = await db.query(`SELECT lp.*, lp.user_id AS user_invited, l.*, u.name
    FROM liveclass_participant lp
    JOIN liveclass l ON l.class_id = lp.class_id
    LEFT JOIN user u ON (l.moderator IS NOT NULL AND l.moderator = u.user_id)
    WHERE l.is_private = '1' AND l.company_id = ? AND lp.user_id = ?`, [req.params.company_id, req.params.user_id]);

  let ifNotInvite = await db.query(`SELECT
                                      l.*,
                                      u.name,
                                      GROUP_CONCAT(p.user_id) AS participant
                                    FROM
                                      liveclass l
                                    LEFT JOIN user u ON (l.moderator IS NOT NULL AND l.moderator = u.user_id)
                                    LEFT JOIN liveclass_participant p ON l.class_id = p.class_id
                                    WHERE l.company_id = ? AND is_private = '0'
                                    GROUP BY l.class_id`, [req.params.company_id]);

  res.json({
    error: false,
    invited: ifInvite,
    not_invited: ifNotInvite
  })
}


exports.addMOM = (req, res, next) => {
  let form = {
    classId: req.body.classId,
    title: req.body.title,
    content: req.body.content,
    time: req.body.time,
    created_by: req.body.created_by
  }

  db.query(`INSERT INTO liveclass_mom (
              liveclass_id,
              title,
              content,
              time,
              created_by
              )
            VALUES (
              ?
            )`, [[form.classId,
  form.title,
  form.content,
  form.time,
  form.created_by]], (error, result, fields) => {
    if (error) {
      res.json({ error: true, result: error })
    } else {
      res.json({ error: false, result: result })
    }
  })
}

exports.editMOM = (req, res, next) => {
  let form = {
    classId: req.body.classId,
    title: req.body.title,
    content: req.body.content,
    time: req.body.time,
    created_by: req.body.created_by
  }

  db.query(`UPDATE liveclass_mom SET
              title = ?,
              content = ?,
              time = ?,
              created_by = ?
            WHERE id= ?
            `, [form.title, form.content, form.time, form.created_by, req.params.id], (error, result, fields) => {
    if (error) {
      res.json({ error: true, result: error })
    } else {
      res.json({ error: false, result: result })
    }
  })
}

exports.editTitleMOM = (req, res, next) => {
  let form = {
    title: req.body.title,
  }

  db.query(`UPDATE liveclass_mom SET title = ? WHERE id= ?`, [form.title, req.params.id], (error, result, fields) => {
    if (error) {
      res.json({ error: true, result: error })
    } else {
      res.json({ error: false, result: result })
    }
  })
}

exports.deleteMOM = (req, res, next) => {
  db.query(`DELETE FROM liveclass_mom WHERE id=?`, [req.params.id], (error, result, fields) => {
    if (error) {
      res.json({ error: true, result: error })
    } else {
      res.json({ error: false, result: result })
    }
  })
}

exports.deleteParticipant = (req, res, next) => {
  db.query(`DELETE FROM liveclass_participant WHERE participant_id=?`, [req.params.participant_id], (error, result, fields) => {
    if (error) {
      res.json({ error: true, result: error })
    } else {
      res.json({ error: false, result: result })
    }
  })
}

exports.getMOM = (req, res, next) => {
  db.query(`SELECT * FROM liveclass_mom WHERE liveclass_id='${req.params.id}' ORDER BY time ASC`, (error, result, fields) => {
    if (error) {
      res.json({ error: true, result: error })
    } else {
      res.json({ error: false, result: result })
    }
  })
}

exports.getBooking = async (req, res, next) => {
  const accessInfo = req.app.accessInfo;

  let tz = 'Asia/Jakarta';
  db.query(`SELECT
    b.*, u.name, um.name as moderator_name,
    DATE_FORMAT(b.date_start,'%Y-%m-%d %H:%i:%s') AS tgl_mulai,
    DATE_FORMAT(b.date_end ,'%Y-%m-%d %H:%i:%s') AS tgl_selesai
  FROM liveclass_booking b
    LEFT JOIN user u ON b.user_id=u.user_id
    LEFT JOIN user um ON b.moderator = um.user_id
  WHERE b.meeting_id = ? AND b.tanggal>=CURDATE()
  ORDER BY b.tanggal DESC`, [req.params.meetingId], async (error, result, fields) => {
    if (error) {
      res.json({ error: true, result: error })
    } else {

      for (var i = 0; i < result.length; i++) {
        const tanggal = result[i].tanggal;
        result[i].isModify = false;
        result[i].isShare = false;
        result[i].expired = false;
        result[i].isJoin = false;
        result[i].isRole = 'participant';

        // if (result[i].timezone) {
        //   tz = result[i].timezone;
        // }
        // result[i].tanggal = moment.tz(tanggal, tz).format('DD-MM-YYYY')
        // result[i].date_start = moment.tz(tanggal, tz).format('HH:mm')
        // result[i].date_end = moment.tz(tanggal, tz).format('HH:mm')

        let tz = result[i].timezone || 'Asia/Jakarta';
        result[i].tanggal = moment(tanggal).format('YYYY-MM-DD')
        result[i].tgl_mulai = moment.tz(result[i].tgl_mulai, tz)
        result[i].tgl_selesai = moment.tz(result[i].tgl_selesai, tz)
        result[i].date_start = moment.tz(result[i].date_start, tz)
        result[i].date_end = moment.tz(result[i].date_end, tz)

        let participants = []

        let parti = await db.query(`
          SELECT lb.*, concat(u.name, '(',u.email,')') as name, u.email 
          FROM liveclass_booking_participant lb
            JOIN user u ON u.user_id = lb.user_id
          WHERE lb.booking_id = ?`, [result[i].id])

        let guests = await db.query(`
          SELECT p.id, p.meeting_id as booking_id, p.email, IF(p.name is null, p.email ,concat(p.name, '(',p.email,')')) as name, IF(status=2,'Hadir','') AS confirmation, IF(status=2,'Hadir','') AS actual
          FROM liveclass_booking_tamu p
          WHERE p.meeting_id = ?`, [result[i].id])

        result[i].participants = parti.concat(guests)

        let api = bbb.api(BBB_URL, BBB_KEY)
        let http = bbb.http
        let checkUrl = api.monitoring.isMeetingRunning(result[i].id)
        let { returncode, running } = await http(checkUrl)
        result[i].running = returncode === 'SUCCESS' ? running : false



        let idx = result[i].participants.findIndex(str => {
          return str.user_id == req.app.token.user_id;
        })

        let delDuplicate = [];
        result[i].participants.forEach((chk) => {
          if (chk.user_id) {
            let idx2 = delDuplicate.findIndex((chk2) => { return chk2.user_id == chk.user_id });
            if (idx2 == -1) {
              delDuplicate.push(chk);
            }
          } else {
            delDuplicate.push(chk);
          }
        });

        result[i].participants = delDuplicate;

        //console.log(idx, req.app, 'TEST AGUS');
        // if (req.params.user_id == 65) {
        // }

        if (idx > -1) {
          if (result[i].running == true) {
            result[i].isJoin = true;
          }
        }


        let today = new Date();
        try {

          let split_end = result[i].tgl_selesai.toISOString().split('T')[0];
          let end_time = new Date(`${split_end} ${result[i].jam_selesai}:00`);
          //console.log(`${split_end} ${result[i].jam_selesai}:00`, end_time, today, "TIME");
          if (today > end_time) {
            //result[i].expired = true;
          }
        } catch (e) {
          console.log(e, "ERR check expired");
        }

        // Action
        if (req.app.token.user_id == result[i].user_id) {
          result[i].isModify = true;
          result[i].isShare = true;
          result[i].isRole = 'owner';
        }
        if (req.app.token.level === 'superadmin' || req.app.token.level === 'admin') {
          result[i].isModify = true;
          result[i].isShare = true;
          result[i].isRole = req.app.token.level;

        }
        // isModerator
        if (result[i].moderator.toString() === req.app.token.user_id.toString()) {
          result[i].isRole = 'moderator';
          accessInfo.forEach(arg => {
            if (arg.role === 'moderator' && arg.sub === 'meeting' && arg.code === 'SEND_MEETING' && arg.status) {
              result[i].isShare = true;
            }
            if (arg.role === 'moderator' && arg.sub === 'meeting' && arg.code === 'CD_MEETING' && arg.status) {
              result[i].isModify = true;
            }
          })
        }

      }

      res.json({ error: false, result: result })
    }
  })
}

exports.addBooking = async (req, res, next) => {
  let form = req.body;
  let moderator = form.is_akses === 1 ? form.moderator.length <= 0 ? [0] : req.body.moderator : [0];

  form.offset = "+07:00";
  if (form.timezone) {
    let tzInfo = await middleware.getTimezoneInfoByUTC(form.timezone);
    form.offset = tzInfo.offset_string || null;
  }

  function dateRangeOverlaps(a_start, a_end, b_start, b_end) {
    if (a_start <= b_start && b_start <= a_end) return true; // b starts in a
    if (a_start <= b_end && b_end <= a_end) return false; // b ends in a
    if (b_start < a_start && a_end < b_end) return true; // a in b
    return false;
  }
  let dataBook = await db.query(`SELECT * FROM liveclass_booking WHERE meeting_id = ? AND tanggal = ?`, [form.meeting_id, form.tanggal]);
  let overlap = false;
  await Promise.all(dataBook.map((item) => {
    if (dateRangeOverlaps(form.jam_mulai, form.jam_selesai, item.jam_mulai, item.jam_selesai)) {
      overlap = true;
    }
  }))
  if (overlap) {
    res.json({ error: true, result: 'Cannot create your meeting schedule. Your schedule is overlaping other schedule in this meeting room.', type: 'warning' });
  }
  else {

    // handle for mobile
    if (!form.hasOwnProperty('date_start') && !form.hasOwnProperty('date_end') && !form.hasOwnProperty('engine') && !form.hasOwnProperty('mode')) {
      form = { ...form, date_start: `${form.tanggal} ${form.jam_mulai}`, date_end: `${form.tanggal} ${form.jam_selesai}`, engine: 'bbb', mode: 'web' }
    }

    db.query(`INSERT INTO liveclass_booking
      (engine, mode, meeting_id, tanggal, date_start, date_end, jam_mulai, jam_selesai, user_id, keterangan, moderator, is_akses, is_private, is_required_confirmation, timezone,offset)
      VALUES
      (?)`,
      [[form.engine || 'bbb', form.mode || 'web', form.meeting_id, form.tanggal, form.date_start, form.date_end, form.jam_mulai, form.jam_selesai, form.user_id, form.keterangan, moderator, form.is_akses, form.is_private, form.is_required_confirmation, form.timezone, form.offset]],
      (error, result, fields) => {
        if (error) {
          res.json({ error: true, result: error })
        } else {

          let bookingId = result.insertId;

          // delete liveclass zoom if not zoom
          if (form.engine === 'zoom') {
            db.query(`SELECT * FROM liveclass_zoom WHERE class_id = ?`, [result.insertId], (err, ress, fiel) => {
              if (!ress.length) {
                db.query(`INSERT INTO liveclass_zoom (class_id, zoom_id, pwd) VALUES (?)`, [[result.insertId, '4912503275', 'hoster']])
              }
            })
          }
          else {
            db.query(`DELETE FROM liveclass_zoom WHERE class_id = ?`, [result.insertId])
          }

          db.query(`SELECT * FROM liveclass_booking WHERE id = ?`, [bookingId], (error, result, fields) => {
            if (form.is_private == 1) {


              if (form.guest) {
                if (form.guest.length) {
                  let tmpQueryGuest = ``;
                  let qDelete = `delete from liveclass_booking_tamu where meeting_id = ${bookingId}`;
                  for (let i = 0; i < form.guest.length; i++) {
                    if (i == form.guest.length - 1) {
                      tmpQueryGuest += `('${bookingId}','${form.guest[i]}',1);`
                    } else {
                      tmpQueryGuest += `('${bookingId}','${form.guest[i]}',1),`
                    }
                  }
                  db.query(`${qDelete}; insert into liveclass_booking_tamu(meeting_id,email,status) values${tmpQueryGuest};`);
                }
              }
              for (let i = 0; i < form.peserta.length; i++) {
                db.query(`DELETE FROM liveclass_booking_participant WHERE booking_id = ? AND user_id = ?`, [bookingId, form.peserta[i]], async (error, result, fields) => {
                  if (error) {
                    console.log('error:', error)
                  }
                  else {

                    let params = [bookingId, form.peserta[i], '', ''];
                    let idx = form.dataParticipantSelected.findIndex((str) => { return str.id == form.peserta[i] });
                    let time = null;
                    if (idx > -1) {
                      if (form.dataParticipantSelected[idx].time) {
                        time = moment(form.dataParticipantSelected[idx].time).format("HH:mm") + ':00'
                        params.push(moment(form.dataParticipantSelected[idx].time).format("YYYY-MM-DD HH:mm") + ':00');
                      } else {
                        params.push(null);

                      }
                    }

                    db.query(`INSERT INTO liveclass_booking_participant (booking_id, user_id, confirmation, actual,start_time) VALUES (?)`, [params]);

                    // insert to calendar
                    let getRoomName = await db.query(`SELECT l.room_name, lb.keterangan, lb.tanggal, lb.jam_mulai, lb.jam_selesai, CAST(CONCAT(lb.tanggal, ' ', lb.jam_mulai) AS DATETIME) as tanggal_mulai, CAST(CONCAT(lb.tanggal, ' ', lb.jam_selesai) AS DATETIME) as tanggal_selesai FROM liveclass l JOIN liveclass_booking lb ON lb.meeting_id = l.class_id WHERE lb.id = ?`, [bookingId])
                    if (time) {
                      getRoomName = await db.query(`SELECT l.room_name, lb.keterangan, lb.tanggal, lb.jam_mulai, lb.jam_selesai, CAST(CONCAT(lb.tanggal, ' ', '${time}') AS DATETIME) as tanggal_mulai, CAST(CONCAT(lb.tanggal, ' ', lb.jam_selesai) AS DATETIME) as tanggal_selesai FROM liveclass l JOIN liveclass_booking lb ON lb.meeting_id = l.class_id WHERE lb.id = ?`, [bookingId])
                    }
                    let sqlCalendar = "INSERT INTO calendar (user_id, type, activity_id, description, destination, start, end) VALUES (?)"
                    let dataCalendar = [[form.peserta[i], 3, bookingId, `${getRoomName[0].room_name} - ${getRoomName[0].keterangan}`, `${env.APPS_URL}/meet/${bookingId}`, getRoomName[0].tanggal_mulai, getRoomName[0].tanggal_selesai]]

                    db.query(sqlCalendar, dataCalendar);
                  }
                });
              }
              if (form.moderator != null && form.moderator != '' && form.moderator != 0 && form.is_akses == 1 && form.peserta.filter(x => x === form.moderator[0]).length === 0) {
                db.query(`DELETE FROM liveclass_booking_participant WHERE booking_id=? AND user_id=?`, [bookingId, form.moderator], async (error, result, fields) => {
                  if (error) {
                    console.log('error:', error)
                  }
                  else {
                    db.query(`INSERT INTO liveclass_booking_participant (booking_id, user_id, confirmation, actual) VALUES (?)`, [[bookingId, form.moderator, '', '']])

                    // insert to calendar
                    let getRoomName = await db.query(`SELECT l.room_name, lb.keterangan, lb.tanggal, lb.jam_mulai, lb.jam_selesai, CAST(CONCAT(lb.tanggal, ' ', lb.jam_mulai) AS DATETIME) as tanggal_mulai, CAST(CONCAT(lb.tanggal, ' ', lb.jam_selesai) AS DATETIME) as tanggal_selesai FROM liveclass l JOIN liveclass_booking lb ON lb.meeting_id = l.class_id WHERE lb.id = ?`, [bookingId])
                    let sqlCalendar = "INSERT INTO calendar (user_id, type, activity_id, description, destination, start, end) VALUES (?)"
                    let dataCalendar = [[form.moderator, 3, bookingId, `${getRoomName[0].room_name} - ${getRoomName[0].keterangan}`, `${env.APPS_URL}/meet/${bookingId}`, getRoomName[0].tanggal_mulai, getRoomName[0].tanggal_selesai]]
                    db.query(sqlCalendar, dataCalendar);
                  }
                });
              }
            }

            res.json({ error: false, result: result[0] })
          })

          // res.json({ error: false, result: result })
        }
      })
  }

}

exports.addClientBooking = async (req, res, next) => {
  let form = req.body;
  let moderator = form.is_akses === 1 ? form.moderator.length <= 0 ? [0] : req.body.moderator : [0];

  form.offset = "+07:00";

  if (form.timezone) {
    let tzInfo = await middleware.getTimezoneInfoByUTC(form.timezone);
    form.offset = tzInfo.offset_string || null;
  }

  function dateRangeOverlaps(a_start, a_end, b_start, b_end) {
    if (a_start <= b_start && b_start <= a_end) return true; // b starts in a
    if (a_start <= b_end && b_end <= a_end) return true; // b ends in a
    if (b_start < a_start && a_end < b_end) return true; // a in b
    return false;
  }
  let dataBook = await db.query(`SELECT * FROM liveclass_booking WHERE meeting_id = ? AND tanggal = ?`, [form.meeting_id, form.tanggal]);
  let overlap = false;
  await Promise.all(dataBook.map((item) => {
    if (dateRangeOverlaps(form.jam_mulai, form.jam_selesai, item.jam_mulai, item.jam_selesai)) {
      overlap = true;
    }
  }))
  if (overlap) {
    res.json({ error: true, result: 'Cannot create your meeting schedule. Your schedule is overlaping other schedule in this meeting room.', type: 'warning' });
  }
  else {

    // handle for mobile
    if (!form.hasOwnProperty('date_start') && !form.hasOwnProperty('date_end') && !form.hasOwnProperty('engine') && !form.hasOwnProperty('mode')) {
      form = { ...form, date_start: `${form.tanggal} ${form.jam_mulai}`, date_end: `${form.tanggal} ${form.jam_selesai}`, engine: 'bbb', mode: 'web' }
    }

    db.query(`INSERT INTO liveclass_booking
      (engine, mode, meeting_id, tanggal, date_start, date_end, jam_mulai, jam_selesai, user_id, keterangan, moderator, is_akses, is_private, is_required_confirmation, timezone,offset,additional_name,additional_agency)
      VALUES
      (?)`,
      [[form.engine || 'bbb', form.mode || 'web', form.meeting_id, form.tanggal, form.date_start, form.date_end, form.jam_mulai, form.jam_selesai, form.user_id, form.keterangan, moderator, form.is_akses, form.is_private, form.is_required_confirmation, form.timezone, form.offset, form.additional_name, form.additional_agency]],
      (error, result, fields) => {
        if (error) {
          res.json({ error: true, result: error })
        } else {

          let bookingId = result.insertId;

          // delete liveclass zoom if not zoom
          if (form.engine === 'zoom') {
            db.query(`SELECT * FROM liveclass_zoom WHERE class_id = ?`, [result.insertId], (err, ress, fiel) => {
              if (!ress.length) {
                db.query(`INSERT INTO liveclass_zoom (class_id, zoom_id, pwd) VALUES (?)`, [[result.insertId, '4912503275', 'hoster']])
              }
            })
          }
          else {
            db.query(`DELETE FROM liveclass_zoom WHERE class_id = ?`, [result.insertId])
          }

          db.query(`SELECT * FROM liveclass_booking WHERE id = ?`, [bookingId], (error, result, fields) => {
            if (form.is_private == 1) {


              if (form.guest) {
                console.log("GUEST:", form.guest)
                let tmpQueryGuest = ``;
                let qDelete = `delete from liveclass_booking_tamu where meeting_id = '${bookingId}'`;
                for (let i = 0; i < form.guest.length; i++) {
                  if (i == form.guest.length - 1) {
                    tmpQueryGuest += `('${bookingId}','${form.guest[i]}',1);`
                  } else {
                    tmpQueryGuest += `('${bookingId}','${form.guest[i]}',1),`
                  }
                }
                db.query(`${qDelete}; insert into liveclass_booking_tamu(meeting_id,email,status) values${tmpQueryGuest};`);
              }
              for (let i = 0; i < form.peserta.length; i++) {
                db.query(`DELETE FROM liveclass_booking_participant WHERE booking_id = ? AND user_id = ?`, [bookingId, form.peserta[i]], async (error, result, fields) => {
                  if (error) {
                    console.log('error:', error)
                  }
                  else {
                    db.query(`INSERT INTO liveclass_booking_participant (booking_id, user_id, confirmation, actual) VALUES (?)`, [[bookingId, form.peserta[i], '', '']]);

                    // insert to calendar
                    let getRoomName = await db.query(`SELECT l.room_name, lb.keterangan, lb.tanggal, lb.jam_mulai, lb.jam_selesai, CAST(CONCAT(lb.tanggal, ' ', lb.jam_mulai) AS DATETIME) as tanggal_mulai, CAST(CONCAT(lb.tanggal, ' ', lb.jam_selesai) AS DATETIME) as tanggal_selesai FROM liveclass l JOIN liveclass_booking lb ON lb.meeting_id = l.class_id WHERE lb.id = ?`, [bookingId])
                    let sqlCalendar = "INSERT INTO calendar (user_id, type, activity_id, description, destination, start, end) VALUES (?)"
                    let dataCalendar = [[form.peserta[i], 3, bookingId, `${getRoomName[0].room_name} - ${getRoomName[0].keterangan}`, `${env.APPS_URL}/meet/${bookingId}`, getRoomName[0].tanggal_mulai, getRoomName[0].tanggal_selesai]]
                    db.query(sqlCalendar, dataCalendar);
                  }
                });
              }
              if (form.moderator != null && form.moderator != '' && form.moderator != 0 && form.is_akses == 1 && form.peserta.filter(x => x === form.moderator[0]).length === 0) {
                db.query(`DELETE FROM liveclass_booking_participant WHERE booking_id=? AND user_id=?`, [bookingId, form.moderator], async (error, result, fields) => {
                  if (error) {
                    console.log('error:', error)
                  }
                  else {
                    db.query(`INSERT INTO liveclass_booking_participant (booking_id, user_id, confirmation, actual) VALUES (?)`, [[bookingId, form.moderator, '', '']])

                    // insert to calendar
                    let getRoomName = await db.query(`SELECT l.room_name, lb.keterangan, lb.tanggal, lb.jam_mulai, lb.jam_selesai, CAST(CONCAT(lb.tanggal, ' ', lb.jam_mulai) AS DATETIME) as tanggal_mulai, CAST(CONCAT(lb.tanggal, ' ', lb.jam_selesai) AS DATETIME) as tanggal_selesai FROM liveclass l JOIN liveclass_booking lb ON lb.meeting_id = l.class_id WHERE lb.id = ?`, [bookingId])
                    let sqlCalendar = "INSERT INTO calendar (user_id, type, activity_id, description, destination, start, end) VALUES (?)"
                    let dataCalendar = [[form.moderator, 3, bookingId, `${getRoomName[0].room_name} - ${getRoomName[0].keterangan}`, `${env.APPS_URL}/meet/${bookingId}`, getRoomName[0].tanggal_mulai, getRoomName[0].tanggal_selesai]]
                    db.query(sqlCalendar, dataCalendar);
                  }
                });
              }
            }

            res.json({ error: false, result: result[0] })
          })

          // res.json({ error: false, result: result })
        }
      })
  }

}

exports.inviteParticipants = async (req, res, next) => {
  try {
    const dtoInviteUser = {
      idBooking: req.params.idBooking,
      participants: req.body.participants,
      email: req.body.email,
    };

    const retrieveValidRequest = await validatorRequest.postParticipants(dtoInviteUser);
    if (retrieveValidRequest.participants.length) {
      for (let i = 0; i < retrieveValidRequest.participants.length; i++) {
        const dtoPost = {
          idBooking: retrieveValidRequest.idBooking,
          idParticipant: retrieveValidRequest.participants[i],
        }

        const checkRedudantUser = await moduleDB.retrieveUserParticipantsByIdBooking(dtoPost);
        if (checkRedudantUser.length === 0) {
          await moduleDB.postParticipantsByIdUser(dtoPost);
        };
      }
    }

    if (retrieveValidRequest.email.length) {
      for (let i = 0; i < retrieveValidRequest.email.length; i++) {
        const dtoPostEmail = {
          idBooking: retrieveValidRequest.idBooking,
          emailUser: retrieveValidRequest.email[i],
        }

        const checkRedudantUser = await moduleDB.retrieveUserGuestByEmail(dtoPostEmail);
        console.log(checkRedudantUser, '????')
        if (checkRedudantUser.length === 0) {
          await moduleDB.postUserGuestByEmail(dtoPostEmail);
        };
      }
    }

    res.json({ error: false, result: 'Success' })

  } catch (error) {
    res.json({ error: true, result: error.message })
  }
};

exports.editBooking = async (req, res, next) => {
  let form = req.body;
  let moderator = form.is_akses === 1 ? form.moderator.length <= 0 ? [0] : req.body.moderator : [0];
  // res.json({ body: req.body, moder: moderator })

  form.offset = "+07:00";
  if (form.timezone) {
    let tzInfo = await middleware.getTimezoneInfoByUTC(form.timezone);
    form.offset = tzInfo.offset_string || null;
  }

  function dateRangeOverlaps(a_start, a_end, b_start, b_end) {
    if (a_start <= b_start && b_start <= a_end) return true; // b starts in a
    if (a_start <= b_end && b_end <= a_end) return true; // b ends in a
    if (b_start < a_start && a_end < b_end) return true; // a in b
    return false;
  }
  let dataBook = await db.query(`SELECT * FROM liveclass_booking WHERE meeting_id= ? AND tanggal=? AND id != ?`, [form.meeting_id, form.tanggal, req.params.id]);
  let overlap = false;
  await Promise.all(dataBook.map((item) => {
    if (dateRangeOverlaps(form.jam_mulai, form.jam_selesai, item.jam_mulai, item.jam_selesai)) {
      overlap = true;
    }
  }))
  if (overlap) {
    res.json({ error: true, result: 'Cannot create your meeting schedule. Your schedule is overlaping other schedule in this meeting room.', type: 'warning' });
  }
  else {

    let qParam = [form.tanggal, form.jam_mulai, form.jam_selesai, form.keterangan, moderator, 'bbb', 'web', form.is_akses, form.is_required_confirmation, form.date_start, form.date_end];
    let qTimezone = ``;
    if (form.timezone) {
      qParam.push(form.timezone, form.offset, req.params.id);
      qTimezone = ` , timezone=?,offset= ?`;
    } else {
      qParam.push(req.params.id);
    }
    db.query(`
    UPDATE liveclass_booking SET
      tanggal = '${form.tanggal}',
      jam_mulai = '${form.jam_mulai}',
      jam_selesai = '${form.jam_selesai}',
      keterangan = '${form.keterangan}',
      moderator = '${moderator}',
      engine = '${form.engine || 'bbb'}',
      mode = '${form.mode || 'web'}',
      is_akses = '${form.is_akses}',
      is_required_confirmation = '${form.is_required_confirmation}',
      date_start = '${form.date_start}',
      date_end = '${form.date_end}',
      timezone = '${form.timezone}',
      offset = '${form.offset}' 
    WHERE id='${req.params.id}'
    `, (error, result, fields) => {
      if (error) {
        res.json({ error: true, result: error })
      } else {
        let bookingId = req.params.id;

        // delete liveclass zoom if not zoom
        if (form.engine === 'zoom') {
          db.query(`SELECT * FROM liveclass_zoom WHERE class_id = ?`, [bookingId], (err, ress, fiel) => {
            if (!ress.length) {
              db.query(`INSERT INTO liveclass_zoom (class_id, zoom_id, pwd) VALUES (?)`, [[bookingId, '4912503275', 'hoster']])
            }
          })
        }
        else {
          db.query(`DELETE FROM liveclass_zoom WHERE class_id = ?`, [bookingId])
        }

        db.query(`SELECT * FROM liveclass_booking WHERE id = ?`, [bookingId], async (error, result, fields) => {
          if (form.is_private == 1) {

            if (form.guest.length) {
              let tmpQueryGuest = ``;
              let qDelete = `delete from liveclass_booking_tamu where meeting_id = '${bookingId}'`;
              for (let i = 0; i < form.guest.length; i++) {
                if (i == form.guest.length - 1) {
                  tmpQueryGuest += `('${bookingId}','${form.guest[i]}',1);`
                } else {
                  tmpQueryGuest += `('${bookingId}','${form.guest[i]}',1),`
                }
              }
              db.query(`${qDelete}; insert into liveclass_booking_tamu(meeting_id,email,status) values${tmpQueryGuest};`);
            }

            db.query(`DELETE FROM liveclass_booking_participant WHERE booking_id=?`, [bookingId]);
            db.query(`delete from calendar where activity_id= ?`, [bookingId]);

            for (let i = 0; i < form.peserta.length; i++) {
              if (form.peserta[i]) {

                db.query(`DELETE FROM liveclass_booking_participant WHERE booking_id=? AND user_id=?`, [bookingId, form.peserta[i]], async (error, result, fields) => {
                  if (error) {
                    console.log('error:', error)
                  }
                  else {

                    let params = [bookingId, form.peserta[i], '', ''];
                    let idx = form.dataParticipantSelected.findIndex((str) => { return str.id == form.peserta[i] });

                    let times = null;
                    if (idx > -1) {
                      if (form.dataParticipantSelected[idx].time) {
                        times = moment(form.dataParticipantSelected[idx].time).format("HH:mm") + ':00'
                        params.push(moment(form.dataParticipantSelected[idx].time).format("YYYY-MM-DD HH:mm") + ':00');
                      } else {
                        params.push(null);
                      }
                    }

                    db.query(`INSERT INTO liveclass_booking_participant (booking_id, user_id, confirmation, actual, start_time) VALUES (?)`, [params]);

                    // insert to calendar
                    let getRoomName = await db.query(`SELECT l.room_name, lb.keterangan, lb.tanggal, lb.jam_mulai, lb.jam_selesai, CAST(CONCAT(lb.tanggal, ' ', ${times ? `'${times}'` : 'lb.jam_mulai'} ) AS DATETIME) as tanggal_mulai, CAST(CONCAT(lb.tanggal, ' ', lb.jam_selesai) AS DATETIME) as tanggal_selesai FROM liveclass l JOIN liveclass_booking lb ON lb.meeting_id = l.class_id WHERE lb.id = ?`, [bookingId])

                    let sqlCalendar = "INSERT INTO calendar (user_id, type, activity_id, description, destination, start, end) VALUES (?)"
                    let dataCalendar = [[form.peserta[i], 3, bookingId, `${getRoomName[0].room_name} - ${getRoomName[0].keterangan}`, `${env.APPS_URL}/meet/${bookingId}`, getRoomName[0].tanggal_mulai, getRoomName[0].tanggal_selesai]]

                    let delCalendar = `delete from calendar where activity_id = ?`;
                    // delete before insert
                    db.query(delCalendar, [bookingId], (err) => {
                      if (!err) {
                        db.query(sqlCalendar, dataCalendar);
                      } else {
                        console.log(err, 'ERROR EDIT')
                      }
                    });

                  }
                });
              }
            }
            if (form.moderator != null && form.moderator != '' && form.moderator != 0 && form.is_akses == 1 && form.peserta.filter(x => x === form.moderator[0]).length === 0) {
              db.query(`DELETE FROM liveclass_booking_participant WHERE booking_id=? AND user_id=?`, [bookingId, form.moderator], async (error, result, fields) => {
                if (error) {
                  console.log('error:', error)
                }
                else {
                  db.query(`INSERT INTO liveclass_booking_participant (booking_id, user_id, confirmation, actual) VALUES (?)`, [[bookingId, form.moderator, '', '']])

                  // insert to calendar
                  let getRoomName = await db.query(`SELECT l.room_name, lb.tanggal, lb.jam_mulai, lb.jam_selesai, CAST(CONCAT(lb.tanggal, ' ', lb.jam_mulai) AS DATETIME) as tanggal_mulai, CAST(CONCAT(lb.tanggal, ' ', lb.jam_selesai) AS DATETIME) as tanggal_selesai FROM liveclass l JOIN liveclass_booking lb ON lb.meeting_id = l.class_id WHERE lb.id = ?`, [bookingId])
                  let sqlCalendar = "INSERT INTO calendar (user_id, type, activity_id, description, destination, start, end) VALUES (?)"
                  let dataCalendar = [[form.moderator, 3, bookingId, `${getRoomName[0].room_name} - ${getRoomName[0].keterangan}`, `${env.APPS_URL}/meet/${bookingId}`, getRoomName[0].tanggal_mulai, getRoomName[0].tanggal_selesai]]

                  let delCalendar = `delete from calendar where activity_id= ?`;
                  // delete before insert
                  db.query(delCalendar, [bookingId], (err) => {
                    if (!err) {
                      db.query(sqlCalendar, dataCalendar);
                    } else {
                      console.log(err, 'ERROR EDIT')
                    }
                  });
                  //db.query(sqlCalendar, dataCalendar);
                }
              });
            }
          }

          res.json({ error: false, result: result[0] })
        })

        // res.json({ error: false, result: result })
      }
    })
  }

}

exports.cancelBooking = async (req, res, next) => {
  let form = req.body;
  db.query(`SELECT lbp.user_id, u.company_id FROM liveclass_booking_participant lbp LEFT JOIN user u ON lbp.user_id = u.user_id WHERE lbp.booking_id = ?`, [req.params.id], (error, result, fields) => {
    if (error) {
      res.json({ error: true, result: error })
    } else {
      if (result.length) {
        result.map(async (item) => {
          let getRoomName = await db.query(`SELECT liveclass.room_name FROM liveclass JOIN liveclass_booking ON liveclass.class_id = liveclass_booking.meeting_id WHERE liveclass_booking.id = ?`, [req.params.id]);
          let roomName = getRoomName.length ? getRoomName[0].room_name : '-';
          let sql = `INSERT INTO notification (company_id, user_id, type, tag, activity_id, description, isread, types) VALUES (?)`;
          let form = [[item.company_id, item.user_id, 3, 1, req.params.id, `Sorry, meeting "${roomName}" cancelled.`, 0, 1]];
          db.query(sql, form);
        })
      }
      db.query(`DELETE FROM liveclass_booking WHERE id=?`, [req.params.id], (error, result, fields) => {
        if (error) {
          res.json({ error: true, result: error })
        } else {
          db.query(`DELETE FROM calendar WHERE activity_id='${req.params.id}'`, (error, result, fields) => {
            if (error) {
              res.json({ error: true, result: error })
            } else {
              res.json({ error: false, result: result })
            }
          })
        }
      })
    }
  });
}