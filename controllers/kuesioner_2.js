var env = require("../env.json");
var conf = require("../configs/config");
var db = require("../configs/database");

/**
 * create kuesioner
 * @param {Number} req.body.id  id webinar
 * @param {Array.<{tanya: String, a: String, b: String, c: String, ...}>} req.body.kuesioner
 * @param {Boolean} res.body.error
 * @param {String} res.body.result
 */
let create = (data, callback) => {
    data.kuesioner.map(async (elem) => {
        let questions = [data.id, elem.tanya];
        let options = [];

        let insert_questions = async () => {
            try {
                return await db.query(
                    `INSERT INTO kuesioner_questions (webinar_id, pertanyaan)
                        VALUES (?);`,
                    [questions]
                );
            } catch (error) {
                return callback({ error: true, result: await error.message });
            }
        };

        let last_insert_id = await insert_questions();

        await Object.keys(elem).map((value, id) => {
            if (id !== 0 && elem[value] !== '') {
                options.push([last_insert_id.insertId, value, elem[value]]);
            }
        });

        let insert_options = async () => {
            try {
                await db.query(
                    `INSERT INTO kuesioner_options (kuesioner_id, options, answer)
                        VALUES ?;`,
                    [options]
                );

                return callback({ error: false, result: "success create kuesioner" });
            } catch (error) {
                return callback({ error: true, result: await error.message });
            }
        };

        await insert_options();
    });
};

exports.create = async (req, res) => {
    create(req.body, (result) => res.json(result));
};

/**
 * delete kuesioner
 * @param {String} req.params.idÍ
 * @param {Boolean} res.body.error
 * @param {String} res.body.result
 */
let remove = (data, callback) => {
    db.query(
        `DELETE t1, t2 
        FROM kuesioner_questions t1
        INNER JOIN kuesioner_options t2 
        ON t1.id = t2.kuesioner_id
        WHERE t1.webinar_id = ${data.id}`,
        (error, result) => {
            if (error) {
                return result({ error: true, result: error.message });
            } else {
                if (result.affectedRows !== 0) {
                    return callback({ error: false, result: "success delete kuesioner" });
                } else {
                    return callback({ error: true, result: "failed delete kuesioner" });
                }
            }
        }
    );
};

exports.delete = (req, res) => {
    remove(req.params, (result) => res.json(result));
};

exports.update = (req, res) => {
    remove(req.body, (result) => {
        if (result.error) {
            res.json({ error: true, result: "failed update kuesioner" });
        } else {
            create(req.body, (callback) => {
                if (callback.error) {
                    res.json({ error: true, result: "failed update kuesioner" });
                } else {
                    res.json({ error: false, result: "success update kuesioner" });
                }
            });
        }
    });
};

exports.readPeserta = (req, res) => {
    db.query(
        `SELECT t1.*, t1.id AS question_id, t2.*, t2.id AS option_id FROM kuesioner_questions t1
        INNER JOIN kuesioner_options t2
        ON t1.id = t2.kuesioner_id
        WHERE t1.webinar_id = ${req.params.id};`,
        (error, result) => {
            if (error) {
                res.json({ error: true, result: error.message });
            } else {
                let group = result.reduce((accumulator, current) => {
                    if (!accumulator[current.kuesioner_id]) {
                        accumulator[current.kuesioner_id] = {};
                        accumulator[current.kuesioner_id]["tanya"] = current.pertanyaan;
                    }
                    accumulator[current.kuesioner_id][current.options] = [current.option_id, current.answer];
                    accumulator[current.kuesioner_id]["question_id"] = current.question_id;

                    return accumulator;
                }, {});

                let kuesioner = [];
                Object.keys(group).map((elem) => {
                    kuesioner.push(group[elem]);
                });

                res.json({ error: false, result: kuesioner });
            }
        }
    );
};
exports.read = (req, res) => {
    db.query(
        `SELECT * FROM kuesioner_questions t1
        INNER JOIN kuesioner_options t2
        ON t1.id = t2.kuesioner_id
        WHERE t1.webinar_id = ${req.params.id};`,
        (error, result) => {
            if (error) {
                res.json({ error: true, result: error.message });
            } else {
                let group = result.reduce((accumulator, current) => {
                    if (!accumulator[current.kuesioner_id]) {
                        accumulator[current.kuesioner_id] = {};
                        accumulator[current.kuesioner_id]["tanya"] = current.pertanyaan;
                    }
                    accumulator[current.kuesioner_id][current.options] = current.answer;

                    return accumulator;
                }, {});

                let kuesioner = [];
                Object.keys(group).map((elem) => {
                    kuesioner.push(group[elem]);
                });

                res.json({ error: false, result: kuesioner });
            }
        }
    );
};

/**
 * create kuesioner
 * @param {Number} req.body.id  id webinar
 * @param {Array.<{tanya: String, jawab: String}>} req.body.kuesioner
 * @param {Boolean} res.body.error
 * @param {String} res.body.result
 */
exports.input = async (req, res) => {
    let data = [];
    await req.body.kuesioner.map((elem) => {
        data.push([req.body.id, req.body.user_id, req.body.pengguna, elem.tanya, elem.jawab]);
    });

    await db.query(
        `INSERT INTO kuesioner_answer
            (webinar_id, user_id, pengguna, webinar_questions, webinar_options) 
        VALUES ?;`,
        [data],
        (error, result) => {
            if (error) {
                res.json({ error: true, result: error.message });
            } else {
                res.json({ error: false, result: "success input kuesioner" });
            }
        }
    );
};

exports.result = (req, res) => {
    db.query(
        `SELECT t2.id, t2.pertanyaan, t3.options, t3.answer, count(t3.answer) AS count
        FROM kuesioner_answer t1
        INNER JOIN kuesioner_questions t2
        ON t1.webinar_questions = t2.id
        INNER JOIN kuesioner_options t3
        ON t1.webinar_options = t3.id
        WHERE t1.webinar_id = '${req.params.id}'
        GROUP BY t2.id, t3.id;`,
        (error, result) => {
            if (error) {
                res.json({ error: true, result: error.message });
            } else {
                let group = result.reduce((accumulator, current) => {
                    if (!accumulator[current.id]) {
                        accumulator[current.id] = {};
                        accumulator[current.id]["tanya"] = current.pertanyaan;
                    }
                    accumulator[current.id][current.options] = current.answer;
                    accumulator[current.id]["count"] = current.count;

                    return accumulator;
                }, {});

                let kuesioner = [];
                Object.keys(group).map((elem) => {
                    kuesioner.push(group[elem]);
                });

                res.json({ error: false, result: kuesioner });
            }
        }
    );
};
