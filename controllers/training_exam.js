const axios = require("axios");
const env = require("../env.json");
const db = require("../configs/database");
const moduleDB = require("../repository");
const multer = require("multer");
const pdf = require("html-pdf");
const AWS = require("aws-sdk");
const path = require("path");
const fs = require("fs");
const { format } = require("../configs/database");
const exceljsRichText = require("../middleware/exceljsRichText");
const PORT = require("../env.json").PORT;
const moment = require("moment");
var mysql = require("mysql");
const validatorRequest = require("../validator");
const featureQuota = require('../package.json')["feature-quota"];

const testPool = mysql.createPool({
  host: env.DB_HOST,
  port: env.DB_PORT,
  user: env.DB_USER,
  password: env.DB_PASS,
  database: env.DB_NAME,
});

/**
 *  QUEUE
 */
const sendMailQueue = require("../configs/bullqueue");
const {
  artifactregistry,
} = require("googleapis/build/src/apis/artifactregistry");
const options_queue = {
  delay: 4000,
  attempts: 2,
  backoff: 20000,
};
/**
 *  END QUEUE
 */
const s3 = new AWS.S3({
  accessKeyId: env.AWS_ACCESS,
  secretAccessKey: env.AWS_SECRET,
});
const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "./public/training/exam");
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname);
  },
});
let uploadImage = multer({ storage: storage }).single("image");

var storageEx = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "./public/training/exam/excel");
  },
  filename: (req, file, cb) => {
    cb(null, "excel-" + Date.now() + "-" + file.originalname);
  },
});
var uploadExcel = multer({ storage: storageEx }).single("file");

exports.import = (req, res) => {
  uploadExcel(req, res, async err => {
    try {
      if (!req.file) {
        res.json({ error: true, result: err });
      } else {
        // create class Excel
        var Excel = require("exceljs");
        var wb = new Excel.Workbook();
        var path = require("path");
        var filePath = path.resolve(
          __dirname,
          "../public/training/exam/excel/" + req.file.filename
        );

        const requestObject = {
          exam_id: req.body.exam_id,
        };

        const retrieveRequest = await validatorRequest.importFileQuestions(
          requestObject
        );
        wb.xlsx.readFile(filePath).then(async function () {
          let throwerError = [];
          let finalRow = [];
          var sh = wb.getWorksheet("Sheet1");
          for (let i = 5; i <= sh.rowCount; i++) {
            if (sh.getRow(i).getCell(1).value !== null) {

              let questionValue = sh.getRow(i).getCell(2).value;
              if (typeof questionValue === 'object' || exceljsRichText.isRichValue(questionValue)) {
                questionValue = exceljsRichText.richToHtml(questionValue);
              }

              /**
               * brackets 0 => multiple choice
               * brackets > 1 and must genap, berati dia short answer
               * selain dua diatas ditolak
               */

              let countBrackets = 0;
              let typeQuestion = null;
              for (let x = 0; x < questionValue.length; x++) {
                if (questionValue[x] === "{" || questionValue[x] === "}") {
                  countBrackets++;
                }
              }

              if (countBrackets === 0) {
                typeQuestion = "1"; // multiple choice
              } else if (countBrackets % 2 === 0) {
                typeQuestion = "2"; //short answer
              }

              if (typeQuestion === null) {
                throwerError.push("Error at: ", sh.getRow(i).getCell(1).value);
                break;
              }

              const objectDuplicateQuestion = {
                idExam: retrieveRequest.exam_id,
                question: questionValue,
                answer: sh.getRow(i).getCell(8).value,
                sort: sh.getRow(i).getCell(1).value - 1,
              };

              const isDuplicateQuestion =
                await moduleDB.retrieveQuestionByExamIdAndQuestionAndAnswer(
                  objectDuplicateQuestion
                );

              if (isDuplicateQuestion.length === 0) {
                let sqlString = `INSERT INTO training_exam_question (exam_id, category_id, question, answer, sort, type)
                                VALUES (
                                    '${retrieveRequest.exam_id}',
                                    '0',
                                    '${questionValue}',
                                    '${sh.getRow(i).getCell(8).value}',
                                    '${sh.getRow(i).getCell(1).value - 1}',
                                    ${typeQuestion}
                                    )`;

                const result = await new Promise((resolve, reject) => {
                  db.query(sqlString, (err, res) => {
                    if (err) reject(res);
                    resolve(res);
                  });
                });

                finalRow.push({
                  ...objectDuplicateQuestion,
                  status: "success import",
                });

                if (result.insertId) {
                  for (let x = 3; x < 8; x++) {
                    if (
                      sh.getRow(i).getCell(x).value !== "" &&
                      sh.getRow(i).getCell(x).value !== null
                    ) {

                      let optText = sh.getRow(i).getCell(x).value;
                      if (typeof optText === 'object' || exceljsRichText.isRichValue(optText)) {
                        optText = exceljsRichText.richToHtml(optText);
                      }

                      await new Promise((resolve, reject) => {
                        db.query(
                          `INSERT INTO training_exam_option (question_id, option_label, option_text) VALUES('${result.insertId
                          }','${sh.getRow(1).getCell(x).value}','${optText
                          }')`,
                          (err, res) => {
                            if (err) reject(res);
                            resolve(res);
                          }
                        );
                      });
                    }
                  }
                } else {
                  //console.log("Error at: ", sh.getRow(i));
                  throwerError.push(
                    "Error at: ",
                    sh.getRow(i).getCell(1).value
                  );
                }
              } else {
                finalRow.push({
                  ...objectDuplicateQuestion,
                  status: "already import",
                });
              }
            } else {
              break;
            }
          }

          if (throwerError.length) {
            res.json({
              error: true,
              result: "Something Wrong with Format Excel!",
            });
          } else {
            res.json({ error: false, result: finalRow });
          }
        });
      }
    } catch (error) {
      res.json({ error: true, result: error.message });
    }
  });
};
exports.uploadImage = (req, res) => {
  uploadImage(req, res, err => {
    if (!req.file) {
      res.json({ error: true, result: err });
    } else {
      if (env.AWS_BUCKET) {
        let path = req.file.path;
        let newDate = new Date();
        let keyName = `${env.AWS_BUCKET_ENV}/training/exam`;

        var params = {
          ACL: "public-read",
          Bucket: env.AWS_BUCKET,
          Body: fs.createReadStream(path),
          Key: `${keyName}/${Date.now()}-${req.file.originalname}`,
          ContentDisposition: "inline",
          ContentType: req.file.mimetype,
        };

        s3.upload(params, (err, data) => {
          if (err) {
            res.json({ error: true, msg: "Error Upload Image", result: err });
          }
          if (data) {
            fs.unlinkSync(path);

            let form = {
              image: data.Location,
            };

            db.query(
              `UPDATE training_exam SET image = '${form.image}' WHERE id = '${req.params.id}'`,
              (error, result, fields) => {
                if (error) {
                  res.json({ error: true, result: error });
                } else {
                  res.json({ error: false, result: result });
                }
              }
            );
          }
        });
      } else {
        let form = {
          image: `${env.APP_URL}/training/exam/${req.file.filename}`,
        };

        db.query(
          `UPDATE training_exam SET image = '${form.image}' WHERE id = '${req.params.id}'`,
          (error, result, fields) => {
            if (error) {
              res.json({ error: true, result: error });
            } else {
              res.json({ error: false, result: result });
            }
          }
        );
      }
    }
  });
};
exports.create = async (req, res) => {
  try {
    const defaultObjectCreateExam = {
      company_id: req.body.company_id,
      title: req.body.title,
      licenses_type_id: req.body.licenses_type_id,
      time_limit: req.body.time_limit,
      minimum_score: req.body.minimum_score,
      repeatable: req.body.repeatable,
      consume_quota: req.body.consume_quota,
      generate_question: req.body.generate_question,
      // composition: [{ total: 0, course_id: [], tag: [] }],
      composition: !req.body.composition ? [{ total: 0, course_id: [], tag: [] }] : req.body.composition,
      course_id: req.body.course_id ? req.body.course_id : 0,
      scheduled: req.body.scheduled,
      generate_membership: req.body.generate_membership,
      see_correct_answer: req.body.see_correct_answer,
      multiple_assign: req.body.multiple_assign,
      start_time: req.body.start_time,
      end_time: req.body.end_time,
      exam: req.body.exam,
      created_by: req.app.token.user_id,
    };

    const responseCreateExam = await moduleDB.createTrainingExam(
      defaultObjectCreateExam
    );
    for (let i = 0; i < defaultObjectCreateExam.composition.length; i++) {
      const detailObject = defaultObjectCreateExam.composition[i];
      const defaultObjectComposition = {
        idExam: responseCreateExam.insertId,
        course_id: !detailObject.course_id || detailObject.course_id.length === 0 ? null : Number(detailObject.course_id),
        total: detailObject.total,
        tag: !detailObject.tag || detailObject.tag.length === 0 ? null : detailObject.tag,
      };
      await moduleDB.createTrainingExamComposition(defaultObjectComposition);
    }

    res.json({ error: false, result: responseCreateExam });
  } catch (error) {
    res.json({ error: true, result: error.message });
  }
};

exports.read = async (req, res) => {
  try {
    const readTrainingExam = await moduleDB.readTrainingExam(req);
    const readComposition = await moduleDB.readComposition(req);
    const readQuestion = await moduleDB.readQuestion(req);
    readTrainingExam[0].composition = readComposition;
    readTrainingExam[0].question = readQuestion;

    // await Promise.all(readQuestion.map(async (item, index) => {
    //     const readOptions = await moduleDB.readOptions(item);
    //     readTrainingExam[0].question[index].option = readOptions
    // }))

    const readOptions = await Promise.all(
      readQuestion.map(item => moduleDB.readOptions(item))
    );
    readQuestion.forEach((_, index) => {
      readTrainingExam[0].question[index].option = readOptions[index];
    });

    res.json({ error: false, result: readTrainingExam[0] });
  } catch (error) {
    res.json({ error: true, result: error });
  }
};

exports.readByUser = async (req, res) => {
  let exam_id = await db.query(
    `SELECT exam_id, status,training_user_id FROM training_exam_assignee WHERE id='${req.params.assignee_id}' LIMIT 1`
  );

  db.query(
    `SELECT *,
        (SELECT SUM(total) FROM training_exam_composition WHERE exam_id=?) AS number_of_question
        FROM training_exam
        WHERE id = ?
        LIMIT 1;`,
    [exam_id[0].exam_id, exam_id[0].exam_id],
    async (error, result) => {
      if (error) {
        return res.json({ error: true, result: error.message });
      }

      let composition = await db.query(
        `SELECT total, course_id,tag FROM training_exam_composition WHERE exam_id='${exam_id[0].exam_id}'`
      );
      result[0].composition = composition;
      result[0].warning = false;
      result[0].questionAnswer = null;
      if (result[0].generate_question === 0) {
        db.query(
          `SELECT q1.id, c2.id AS category_id, q1.category_id AS subcategory_id, q1.question, q1.answer, q1.sort,q1.type 
                    FROM training_exam_question q1
                    LEFT JOIN training_exam_category c ON c.id = q1.category_id
                    LEFT JOIN training_exam_category c2 ON c2.id = c.parent
                    WHERE q1.exam_id = ?
                    ORDER BY q1.sort ASC;`,
          [exam_id[0].exam_id],
          async (error, resultQ) => {
            if (error) {
              return res.json({ error: true, result: error.message });
            }

            let getId = [];
            if (resultQ.length > 0) {
              let tmp = [];
              resultQ.forEach(str => {
                str.short_answer = false;


                if (str.type == 2) str.short_answer = true;
                tmp.push(str);

                getId.push({ ...str,question_id: str.id, option_label: "" });
              });
              resultQ = tmp;
            }

            result[0].question = resultQ;
            await Promise.all(
              result[0].question.map(async (item, index) => {
                let options = await db.query(
                  `SELECT * FROM training_exam_option WHERE question_id='${item.id}' ORDER BY option_label ASC`
                );

                if(result[0].question[index].short_answer){
                  let tmpOp = [];
                  //console.log('???')
                  options.forEach((str)=>{ str.option_text = ''; tmpOp.push(str); });
                  options = tmpOp;
                }
                result[0].question[index].option = options;
              })
            );
            // if (exam_id[0].status === 'Open'){
            let start_time = moment(
              new Date()
            ).format("YYYY-MM-DD HH:mm:ss");
            let end_time = moment(start_time).add(result[0].time_limit,'minutes');
            end_time = moment(end_time).format("YYYY-MM-DD HH:mm:ss");
            result[0].submission_start = start_time;
            result[0].submission_end = end_time;

            await db.query(
              `UPDATE training_exam_assignee SET status='Start', start_time='${start_time}' WHERE id='${req.params.assignee_id
              }'`
            );
            // }
            // get data temp
            if (result[0].question.length) {
              getId = [];
              result[0].question.forEach((str) => {
                getId.push({ ...str,question_id: str.id, option_label: "" });
              })
              let check = await db.query(`select id, submission_start,answer from temporary_exam_result where training_exam_id = ? and training_user_id = ? and training_assignee_id = ?`,[exam_id[0].exam_id, exam_id[0].training_user_id, req.params.assignee_id]);
              let dateNow = moment(new Date());
              if(req.query.web == 1){                
                try {
                  
                  let startTimes = moment(check[0].submission_start);
                  let getMinutes = 0;
                  let time_limit = result[0].time_limit;
    
                  getMinutes = moment.duration(dateNow.diff(startTimes)).asMinutes();
    
                  if (getMinutes > time_limit) {
                    result[0].warning = 'time expired';
                    result[0].time_limit = 0;
                    result[0].questionAnswer = JSON.parse(check[0].answer);
                    db.query(`delete from temporary_exam_result where id = ?`, [check[0].id]);
                  }else{
                    result[0].time_limit -= getMinutes;
                    result[0].questionAnswer = JSON.parse(check[0].answer);
                    result[0].submission_start = moment(check[0].submission_start).format("YYYY-MM-DD HH:mm:ss");
                    result[0].submission_end = dateNow.add(result[0].time_limit,'minutes');
                    result[0].submission_end = moment(result[0].submission_end).format("YYYY-MM-DD HH:mm:ss");
                  }
      
                } catch (error) { 
  
                  result[0].submission_start = moment(dateNow).format("YYYY-MM-DD HH:mm:ss");
                  result[0].submission_end = moment(result[0].submission_start).add(result[0].time_limit,'minutes');
                  result[0].submission_end = moment(result[0].submission_end).format("YYYY-MM-DD HH:mm:ss");
                  db.query(`insert into temporary_exam_result(training_exam_id,training_user_id,training_assignee_id,submission_condition,submission_start,answer,question) value(?,?,?,?,?,?,?)`, [exam_id[0].exam_id, exam_id[0].training_user_id, req.params.assignee_id, "Normal", start_time, JSON.stringify(getId),JSON.stringify(getId)]);
                }
              }else{
                result[0].submission_start = moment(dateNow).format("YYYY-MM-DD HH:mm:ss");
                result[0].submission_end = moment(result[0].submission_start).add(result[0].time_limit,'minutes');
                result[0].submission_end = moment(result[0].submission_end).format("YYYY-MM-DD HH:mm:ss");
              }
            }

            try{
              let getUser = await db.query(`
                select u.user_id, u.first_open_exam 
                from training_user tu
                left join user u on u.email = tu.email and lower(u.status) = 'active'
                where tu.id = ?
              `,[exam_id[0].training_user_id]);

              if(getUser[0].first_open_exam == 0){
                db.query(`update user set first_open_exam=1 where user_id='${getUser[0].user_id}'`)
              }
              
            }catch(e){}

            return res.json({ error: false, result: result[0] });
          }
        );
      } else {
        result[0].question = [];
        
        await Promise.all(
          composition.map(async item => {
            let condition = '';
            if (item.course_id && item.course_id > 0) {
              condition += `q1.training_course_id = '${item.course_id}' AND `;

            } if (item.tag) {
              condition += `q1.tag = '${item.tag}' AND `;
            }


            let sqlQuery = `SELECT q1.id, q1.question, q1.answer, q1.type 
            FROM training_questions q1
            LEFT JOIN training_course c ON c.id = q1.training_course_id
            WHERE ${condition}
            q1.licenses_type_id = ? AND q1.status='Active' 
            ORDER BY RAND() LIMIT ?;`;

            let resultQ = await db.query(sqlQuery, [result[0].licenses_type_id, item.total]);

            if (resultQ.length > 0) {
              let tmp = [];
              resultQ.forEach(str => {


                str.short_answer = false;
                if (str.type == 2) str.short_answer = true;
                tmp.push(str);
              });
              resultQ = tmp;
            }
            result[0].question = result[0].question.concat(resultQ);
          })
        );
        await Promise.all(
          result[0].question.map(async (item, index) => {
            let options = await db.query(
              `SELECT * FROM training_questions_options WHERE question_id='${item.id}' ORDER BY option_label ASC`
            );

            if(result[0].question[index].short_answer){
              let tmpOp = [];
              options.forEach((str)=>{ str.option_text = ''; tmpOp.push(str); });
              options = tmpOp;
            }
            result[0].question[index].option = options;
          })
        );
        let start_time = moment(
          new Date()
        ).format("YYYY-MM-DD HH:mm:ss");
        //if (exam_id[0].status === "Open") {
          // let start_time = moment(
          //   new Date()
          // ).format("YYYY-MM-DD HH:mm:ss");

          let end_time = moment(start_time).add(result[0].time_limit,'minutes');
            end_time = moment(end_time).format("YYYY-MM-DD HH:mm:ss");
            result[0].submission_start = start_time;
            result[0].submission_end = end_time;
          await db.query(
            `UPDATE training_exam_assignee SET status='Start', start_time='${start_time}' WHERE id='${req.params.assignee_id
            }'`
          );
        //}

        if (result[0].repeatable > 0) {
          let checkResult = await db.query(`SELECT e.id FROM training_exam_result e WHERE e.assignee_id=? limit 1;`, [req.params.assignee_id]);

          try {

            result[0].is_read = checkResult[0].id ? 1 : 0;
          } catch (error) {
            result[0].is_read = 0
          }
          result[0].on_schedule = "1";
        }

        // get data temp
        if (result[0].question.length) {
          let getId = [];
          result[0].question.forEach((str) => {
            getId.push({ ...str,question_id: str.id, option_label: "" });
          })
          if (getId.length) {
            let check = await db.query(`select id, submission_start,answer from temporary_exam_result where training_exam_id = ? and training_user_id = ? and training_assignee_id = ?`,[exam_id[0].exam_id, exam_id[0].training_user_id, req.params.assignee_id]);           
            let dateNow = moment(new Date());

            if(req.query.web == 1){
              try {
                
                let startTimes = moment(check[0].submission_start);
                let getMinutes = 0;
                let time_limit = result[0].time_limit;
  
                getMinutes = moment.duration(dateNow.diff(startTimes)).asMinutes();
                
                if (getMinutes > time_limit) {
                  result[0].warning = 'time expired';
                  result[0].time_limit = 0;
                  result[0].questionAnswer = JSON.parse(check[0].answer);
                  db.query(`delete from temporary_exam_result where id = ?`, [check[0].id]);
                }else{
                  result[0].time_limit -= getMinutes;
                  result[0].questionAnswer = JSON.parse(check[0].answer);
                  result[0].submission_start = moment(check[0].submission_start).format("YYYY-MM-DD HH:mm:ss");
                  result[0].submission_end = dateNow.add(result[0].time_limit,'minutes');
                  result[0].submission_end = moment(result[0].submission_end).format("YYYY-MM-DD HH:mm:ss");
                }
    
              } catch (error) { 
                result[0].submission_start = moment(dateNow).format("YYYY-MM-DD HH:mm:ss");
                result[0].submission_end = moment(result[0].submission_start).add(result[0].time_limit,'minutes');
                result[0].submission_end = moment(result[0].submission_end).format("YYYY-MM-DD HH:mm:ss");
                db.query(`insert into temporary_exam_result(training_exam_id,training_user_id,training_assignee_id,submission_condition,submission_start,answer,question) value(?,?,?,?,?,?,?)`, [exam_id[0].exam_id, exam_id[0].training_user_id, req.params.assignee_id, "Normal", start_time, JSON.stringify(getId),JSON.stringify(getId)]);
              }
            }else{
              result[0].submission_start = moment(dateNow).format("YYYY-MM-DD HH:mm:ss");
              result[0].submission_end = moment(result[0].submission_start).add(result[0].time_limit,'minutes');
              result[0].submission_end = moment(result[0].submission_end).format("YYYY-MM-DD HH:mm:ss");
            }
          }
        }

        try{
          let getUser = await db.query(`
            select u.user_id, u.first_open_exam 
            from training_user tu
            left join user u on u.email = tu.email and lower(u.status) = 'active'
            where tu.id = ?
          `,[exam_id[0].training_user_id]);

          if(getUser[0].first_open_exam == 0){
            db.query(`update user set first_open_exam=1 where user_id='${getUser[0].user_id}'`)
          }
          
        }catch(e){}
        
        return res.json({ error: false, result: result[0] });
      }
    }
  );
};

exports.browseByCompany = (req, res) => {
  db.query(
    `SELECT e.*, (SELECT SUM(total) FROM training_exam_composition WHERE exam_id=e.id) AS number_of_question, l.name AS licenses_type,  IF(e.scheduled=1,'Yes','No') AS is_scheduled,
    (SELECT distinct COUNT(tea.training_user_id) FROM training_exam_assignee tea WHERE tea.exam_id = e.id) AS total_user     
    FROM training_exam e
        LEFT JOIN training_licenses_type l ON l.id = e.licenses_type_id
        WHERE e.company_id=? AND e.status='Active' AND e.exam=?
        ORDER BY e.created_at DESC;`,
    [req.params.id, req.params.exam],
    (error, result) => {
      if (error) {
        return res.json({ error: true, result: error });
      }

      return res.json({ error: false, result: result });
    }
  );
};

exports.browseByTrainingUserId = (req, res) => {
  db.query(
    `SELECT e.*, (SELECT SUM(total) FROM training_exam_composition WHERE exam_id=e.id) AS number_of_question, l.name AS licenses_type,  IF(e.scheduled=1,'Yes','No') AS is_scheduled, tu.id
        FROM training_exam e
        LEFT JOIN training_licenses_type l ON l.id = e.licenses_type_id
        JOIN training_course_company tcc ON tcc.training_course_id = e.course_id
        JOIN training_user tu ON tu.training_company_id = tcc.training_company_id
        WHERE tu.id=? AND e.status='Active' AND tcc.active=1
        ORDER BY e.created_at DESC;
        
        SELECT e.*, (SELECT SUM(total) FROM training_exam_composition WHERE exam_id=e.id) AS number_of_question, l.name AS licenses_type,  IF(e.scheduled=1,'Yes','No') AS is_scheduled, tu.id
        FROM training_user tu
        LEFT JOIN training_company tc ON tc.id = tu.training_company_id 
        left join training_exam e ON e.company_id = tc.company_id
        LEFT JOIN training_licenses_type l ON l.id = e.licenses_type_id
        WHERE 
        tu.id = ? AND e.course_id = 0 and 
        e.status='Active'
        ORDER BY e.created_at DESC;
        `,
    [req.params.training_user_id, req.params.training_user_id],
    (error, result) => {
      if (error) {
        return res.json({ error: true, result: error });
      }

      let tmp = [];
      try {
        if (result[0].length > 0) {
          result[0].forEach(str => {
            str.access = false;
            if (str.is_scheduled === "Yes") {
              str.access = true;
            }
            tmp.push(str);
          });
        }

        if (result[1].length > 0) {
          result[1].forEach(str => {
            str.access = false;
            if (str.is_scheduled === "Yes") {
              str.access = true;
            }
            tmp.push(str);
          });
        }
        result = tmp;
      } catch (e) {
        result = [];
      }

      return res.json({ error: false, result: result });
    }
  );
};

exports.browseByCompanyCourse = (req, res) => {
  db.query(
    `
        /*
        OLD
        SELECT e.*, (SELECT SUM(total) FROM training_exam_composition WHERE exam_id=e.id) AS number_of_question, l.name AS licenses_type,  IF(e.scheduled=1,'Yes','No') AS is_scheduled
        FROM training_exam e
        LEFT JOIN training_licenses_type l ON l.id = e.licenses_type_id
        WHERE e.company_id=? AND e.status='Active' AND e.exam=?
        */ 
        SELECT 
        distinct e.*, (SELECT SUM(total) FROM training_exam_composition WHERE exam_id=e.id) AS number_of_question, l.name AS licenses_type,  IF(e.scheduled=1,'Yes','No') AS is_scheduled 
        FROM training_company tc 
        LEFT JOIN training_course_company tcc ON tcc.training_company_id = tc.id AND tcc.active=1
        LEFT JOIN training_exam e ON e.course_id = tcc.training_course_id 
        LEFT JOIN training_licenses_type l ON l.id = e.licenses_type_id 
        WHERE tc.id='${req.params.id}' AND e.status='Active' AND e.exam='${req.params.exam}'
        ORDER BY e.created_at DESC;
        
        SELECT e.*, (SELECT SUM(total) FROM training_exam_composition WHERE exam_id=e.id) AS number_of_question, l.name AS licenses_type,  IF(e.scheduled=1,'Yes','No') AS is_scheduled
        FROM training_company tc
        LEFT JOIN training_exam e on e.company_id = tc.company_id
        LEFT JOIN training_licenses_type l ON l.id = e.licenses_type_id
        WHERE tc.id='${req.params.id}' AND e.course_id = 0 and e.status='Active' AND e.exam='${req.params.exam}';
        `,
    (error, result) => {
      if (error) {
        return res.json({ error: true, result: error });
      }

      let tmp = [];
      if (result[0].length > 0) {
        result[0].forEach(str => {
          tmp.push(str);
        });
      }

      if (result[1].length > 0) {
        result[1].forEach(str => {
          tmp.push(str);
        });
      }
      result = tmp;
      return res.json({ error: false, result: result });
    }
  );
};

exports.browseByCompanyArchived = (req, res) => {
  db.query(
    `SELECT e.*, (SELECT SUM(total) FROM training_exam_composition WHERE exam_id=e.id) AS number_of_question, l.name AS licenses_type,  IF(e.scheduled=1,'Yes','No') AS is_scheduled
        FROM training_exam e
        LEFT JOIN training_licenses_type l ON l.id = e.licenses_type_id
        WHERE e.company_id=? AND e.status='Inactive' AND e.exam=?
        ORDER BY e.created_at DESC;`,
    [req.params.id, req.params.exam],
    (error, result) => {
      if (error) {
        return res.json({ error: true, result: error });
      }

      return res.json({ error: false, result: result });
    }
  );
};

exports.browseByUser = (req, res) => {
  db.query(
    `SELECT e.*, (SELECT SUM(total) FROM training_exam_composition WHERE exam_id=e.id) AS number_of_question, l.name AS licenses_type,  IF(e.scheduled=1,'Yes','No') AS is_scheduled, a.id AS assignee_id,
    (IF(NOW() BETWEEN e.start_time AND e.end_time != '1' && e.scheduled = 1, '0', '1')) AS on_schedule,
    (IF(NOW() BETWEEN e.start_time AND e.end_time != '1' && e.scheduled = 1, '0', '1')) AS access,
    lower(a.status) as 'status_assignee' ,
    (IF((SELECT r.pass AS hadir FROM training_exam_result r WHERE r.exam_id=e.id AND r.training_user_id='${req.params.training_user_id}' AND r.assignee_id=a.id and r.pass=1 limit 1)>0,1,0)) AS is_read, 
    (IF((SELECT r.id AS hadir FROM training_exam_result r WHERE r.exam_id=e.id AND r.training_user_id='${req.params.training_user_id}' AND r.assignee_id=a.id limit 1)>0,1,0)) AS is_read_repeat 
    FROM training_exam e
        LEFT JOIN training_licenses_type l ON l.id = e.licenses_type_id
        LEFT JOIN training_exam_assignee a ON a.exam_id = e.id
        WHERE e.company_id=? AND e.status='Active' AND e.exam=? AND a.training_user_id=?
        AND (a.status != 'Finish' OR e.repeatable = '1')
        ORDER BY e.created_at DESC;`,
    [req.params.company_id, req.params.exam, req.params.training_user_id],
    async (error, result) => {
      if (error) {
        return res.json({ error: true, result: error.message });
      }

      let tmp = [];
      if (result.length > 0) {
        result.forEach(str => {
          if (str.repeatable == 1) {
            if (str.is_read_repeat > 0) {
              str.is_read = 1;
            }
            tmp.push(str);
          } else {
            if (str.assignee !== "finish") {
              tmp.push(str);
            }
          }
        });
        result = tmp;
      }

      const { email } = req.app.token;
      await Promise.all(
        result.map(async (item, index) => {

          /**
           * Get Temporary Exam Result By Assignee Id
           */
          const isExamProgressing = await moduleDB.readTemporaryExamResultByIdAssignee({
            idAssignee: item.assignee_id,
          });

          if(isExamProgressing.length) {
            result[index].availability = 'Processing'
          }else{
            result[index].availability = 'Active'
          }

          result[index].access = item.access === '0' ? false : true;
          let Requirements = false;
          if (item.course_id !== 0 && item.course_id !== null) {
            let schedule_course = await db.query(
              `SELECT c.require_course_id FROM training_course c WHERE c.id = ${item.course_id} LIMIT 1;`
            );
            if (
              schedule_course[0].require_course_id !== 0 &&
              schedule_course[0].require_course_id !== null
            ) {
              let requireCheck =
                await db.query(`SELECT MIN(r.pass_exam) AS require_check
                        FROM (
                        SELECT e.title, (SELECT COUNT(r.id) FROM training_exam_result r JOIN training_user tu ON tu.id = r.training_user_id WHERE r.exam_id=e.id AND r.pass=1 AND tu.email='${email}') AS pass_exam
                        FROM training_exam e
                        WHERE e.course_id = ${schedule_course[0].require_course_id} AND e.exam=1
                        ) AS r`);
              
              if (requireCheck[0].require_check === 0) {
                let reqCourse = await db.query(
                  `SELECT title FROM training_course WHERE id=${schedule_course[0].require_course_id}`
                );
                result[index].on_schedule = "0";
                result[index].access = false;
                Requirements = schedule_course[0].require_course_id;
                result[
                  index
                ].message = `This exam is require you to pass course '${reqCourse[0].title}'`;
              }
            }
          }
          if(!Requirements){
            let agenda = await getAgendaCourse(item.course_id);
            if (agenda.length) {
              let idx = agenda.findIndex(x => x.id === item.id && x.type === "2");
              if (
                idx > 0 &&
                agenda[idx - 1].type === "1" &&
                agenda[idx - 1].is_mandatory
              ) {
                let idPrev = agenda[idx - 1].id;
                const { email } = req.app.token;
                let user_id = await db.query(
                  `SELECT user_id FROM user WHERE email=?`,
                  [email]
                );
                let checkReadLiveclass = await db.query(
                  `
                          SELECT (IF((SELECT COUNT(p.id) AS hadir FROM webinar_peserta p WHERE p.webinar_id=w.id AND p.user_id=? AND p.status='2')>0 AND w.status=3,1,0)) AS is_read
                          FROM webinars w
                          WHERE w.id=?`,
                  [user_id[0].user_id, idPrev]
                );
                if (
                  checkReadLiveclass.length &&
                  checkReadLiveclass[0].is_read === 1
                ) {
                  result[index].access = true;
                } else if (
                  checkReadLiveclass.length &&
                  checkReadLiveclass[0].is_read === 0
                ) {
                  result[index].access = false;
                }
              }
              else if (
                idx > 0 &&
                agenda[idx - 1].type !== "1"
              ) {
                let idPrev = agenda[idx - 1].id;
                let typePrev = agenda[idx - 1].type;
                let sql = '';
                if (typePrev === "2") {
                  sql = `SELECT
                  (
                      IF(
                          (
                              SELECT COUNT(r.id) AS hadir FROM training_exam_result r WHERE r.exam_id=e.id AND r.training_user_id=? AND r.pass = 1 order by r.score DESC
                          )>0,1,0)
                  ) AS is_read,
                  (
                    IF(
                        (
                            SELECT COUNT(r.id) AS hadir FROM training_exam_result r WHERE r.exam_id=e.id AND r.training_user_id='${req.params.training_user_id}' AND r.pass = 1 order by r.score DESC
                        )>0,1,0)
                ) AS is_read_repeat 
                  FROM training_exam e
                  WHERE e.id=?`;
                }
                else if (typePrev === "3") {
                  sql = `SELECT IF(s.id IS NOT NULL, 1, 0) AS is_read
                  FROM training_course_session_read s
                  WHERE s.training_user_id=? AND s.session_id=?`;
                }
                else if (typePrev === "4") {
                  sql = `SELECT IF(r.is_read IS NULL, 0, r.is_read) AS is_read
                  FROM training_submission_answer r
                  WHERE r.training_user_id=? AND submission_id=?`;
                }
                let checkPrevRead = await db.query(sql, [req.params.training_user_id, idPrev]);
                if (item.repeatable === 1) {
                  result[index].is_read =
                    result[index].is_read_repeat > 0 ? 1 : result[index].is_read ? result[index].is_read : 0;
                  if (item.scheduled === 0) {
                    if (checkPrevRead.length && checkPrevRead[0].is_read === 1) {
                      result[index].on_schedule = "1";
                      result[index].access = true;
                    }
                    else {
                      result[index].on_schedule = "0";
                      result[index].access = false;
                    }
                  } else {
                    if (
                      item.start_time <= new Date() &&
                      item.end_time >= new Date()
                    ) {
                      if (checkPrevRead.length && checkPrevRead[0].is_read === 1) {
                        result[index].on_schedule = "1";
                        result[index].access = true;
                      }
                      else {
                        result[index].on_schedule = "0";
                        result[index].access = false;
                      }
                    }
                  }
                } else {
                  if (item.scheduled === 0) {
                    if(result[index].id == 175){
                      console.log(checkPrevRead,typePrev,agenda,"999")
                    }
                    if (checkPrevRead.length && checkPrevRead[0].is_read === 1) {
                      result[index].on_schedule = "1";
                      result[index].access = true;
                    }
                    else {
                      result[index].on_schedule = "0";
                      result[index].access = false;
                    }
                  } else {
                    if (
                      item.start_time <= new Date() &&
                      item.end_time >= new Date()
                    ) {
                      if (checkPrevRead.length && checkPrevRead[0].is_read === 1) {
                        result[index].on_schedule = "1";
                        result[index].access = true;
                      }
                      else {
                        result[index].on_schedule = "0";
                        result[index].access = false;
                      }
                    }
                  }
                }
              }
            }
          }
        })
      );
      return res.json({ error: false, result: result });
    }
  );
};

exports.browseByCourse = (req, res) => {
  db.query(
    `SELECT e.*, (SELECT SUM(total) FROM training_exam_composition WHERE exam_id=e.id) AS number_of_question, l.name AS licenses_type,  IF(e.scheduled=1,'Yes','No') AS is_scheduled, a.id AS assignee_id
        FROM training_exam e
        LEFT JOIN training_licenses_type l ON l.id = e.licenses_type_id
        LEFT JOIN training_exam_assignee a ON a.exam_id = e.id
        WHERE e.company_id=? AND e.status='Active' AND e.exam=? AND a.training_user_id=? AND e.course_id=?
        AND (e.scheduled = 0 OR (e.scheduled = 1 AND (NOW() BETWEEN e.start_time AND e.end_time)))
        AND a.status !='Finish'
        ORDER BY e.created_at DESC;`,
    [
      req.params.company_id,
      req.params.exam,
      req.params.training_user_id,
      req.params.course_id,
    ],
    async (error, result) => {
      if (error) {
        return res.json({ error: true, result: error.message });
      }

      const { email } = req.app.token;
      await Promise.all(
        result.map(async (item, index) => {
          if (item.course_id !== 0 && item.course_id !== null) {
            let schedule_course = await db.query(
              `SELECT c.require_course_id FROM training_course c WHERE c.id = ${item.course_id} LIMIT 1;`
            );
            if (
              schedule_course[0].require_course_id !== 0 &&
              schedule_course[0].require_course_id !== null
            ) {
              let requireCheck =
                await db.query(`SELECT MIN(r.pass_exam) AS require_check
                        FROM (
                        SELECT e.title, (SELECT COUNT(r.id) FROM training_exam_result r JOIN training_user tu ON tu.id = r.training_user_id WHERE r.exam_id=e.id AND r.pass=1 AND tu.email='${email}') AS pass_exam
                        FROM training_exam e
                        WHERE e.course_id = ${schedule_course[0].require_course_id} AND e.exam=1
                        ) AS r`);
              if (requireCheck[0].require_check === 0) {
                let reqCourse = await db.query(
                  `SELECT title FROM training_course WHERE id=${schedule_course[0].require_course_id}`
                );
                result[index].on_schedule = "0";
                result[
                  index
                ].message = `This exam is require you to pass course '${reqCourse[0].title}'`;
              }
            }
          }
        })
      );
      return res.json({ error: false, result: result });
    }
  );
};

exports.browseHistoryByUser = (req, res) => {
  db.query(
    /* sql */ `SELECT
        e.id,
        e.company_id,
        e.title,
        e.image,
        e.licenses_type_id,
        e.time_limit,
        e.minimum_score,
        e.generate_question,
        e.course_id,
        e.scheduled,
        e.generate_membership,
        e.see_correct_answer,
        e.multiple_assign,
        e.start_time,
        e.end_time,
        e.exam,
        e.repeatable,
        e.status,
        e.created_by,
        r.created_at AS created_at,
        ( SELECT SUM( total ) FROM training_exam_composition WHERE exam_id = e.id ) AS number_of_question,
        l.NAME AS licenses_type,
    IF
        ( e.scheduled = 1, 'Yes', 'No' ) AS is_scheduled,
        a.id AS assignee_id,
        r.score,
        r.pass,-- 	rec.id AS record_id,
        r.id AS result_id,
    IF
        ( rec.start_time IS NULL, a.start_time, rec.start_time ) AS work_start_time,
    IF
        ( rec.submission_time IS NULL, a.submission_time, rec.submission_time ) AS submission_time,
        TIMESTAMPDIFF(
            MINUTE,
        IF
            ( rec.start_time IS NULL, a.start_time, rec.start_time ),
        IF
        ( rec.submission_time IS NULL, a.submission_time, rec.submission_time )) AS work_time 
    FROM
        training_exam_result r
        JOIN training_exam e ON e.id = r.exam_id
        JOIN training_licenses_type l ON l.id = e.licenses_type_id
        LEFT JOIN training_exam_record rec ON rec.exam_result_id = r.id
        LEFT JOIN training_exam_assignee a ON ( a.exam_id = r.assignee_id AND a.STATUS = 'Finish' ) 
    WHERE
        e.company_id = ? 
        AND e.exam = ?
        AND r.training_user_id = ?
    ORDER BY
        r.created_at DESC;`,
    [req.params.company_id, req.params.exam, req.params.training_user_id],
    (error, result) => {
      if (error) {
        return res.json({ error: true, result: error.message });
      }

      if (result.length > 0) {
        result.forEach(str => {
          if (str.repeatable > 0) {
            str.is_read = str.created_at ? 1 : 0;
            str.on_schedule = "1";
          }
        });
        result = result.sort((a, b) => {
          return new Date(b.created_at) - new Date(a.created_at);
        });
      }

      return res.json({ error: false, result: result });
    }
  );
};

exports.update = async (req, res) => {
  try {
    if (!req.body.repeatable) {
      req.body.repeatable = 0;
    }
    let values = [
      req.body.repeatable,
      req.body.consume_quota,
      req.body.title,
      req.body.licenses_type_id,
      req.body.time_limit,
      req.body.minimum_score,
      req.body.generate_question,
      req.body.course_id === "" || req.body.course_id === null
        ? 0
        : req.body.course_id,
      req.body.scheduled,
      req.body.generate_membership,
      req.body.see_correct_answer,
      req.body.multiple_assign,
      req.body.start_time,
      req.body.end_time,
      req.params.id,
    ];
    db.query(
      `UPDATE training_exam
            SET 
                title = ?,
                licenses_type_id = ?,
                time_limit = ?,
                minimum_score = ?,
                generate_question = ?,
                course_id = ?,
                scheduled = ?,
                generate_membership = ?,
                see_correct_answer = ?,
                multiple_assign = ?,
                start_time = ?,
                end_time = ?,
                repeatable = ?,
                consume_quota = ?
            WHERE id = ?;`,
      [
        req.body.title,
        req.body.licenses_type_id,
        req.body.time_limit,
        req.body.minimum_score,
        req.body.generate_question,
        req.body.course_id === "" || req.body.course_id === null
          ? 0
          : req.body.course_id,
        req.body.scheduled,
        req.body.generate_membership,
        req.body.see_correct_answer,
        req.body.multiple_assign,
        req.body.start_time,
        req.body.end_time,
        req.body.repeatable,
        req.body.consume_quota,
        req.params.id,
      ],
      async (error, result) => {
        if (error) {
          return res.json({ error: true, result: error.message });
        } else {
          if (req.body.composition.length) {
            await db.query(
              `DELETE FROM training_exam_composition WHERE exam_id='${req.params.id}'`
            );
            req.body.composition.map(async item => {
              if (item.total) {
                let course_id = null;
                let tag = null;
                if (item.tag.length) {
                  if (item.tagList.length) {

                    let idx = item.tagList.findIndex((list) => { return list.value === item.tag[0] });
                    if (idx > -1) {
                      tag = item.tagList[idx].label;
                    }
                  } else {
                    tag = item.tag[0];

                  }
                }

                if (item.course_id.length) {
                  course_id = item.course_id[0]
                }

                if (course_id || tag) {
                  await db.query(
                    `INSERT INTO training_exam_composition (exam_id, course_id, total, tag) VALUES(?)`,
                    [[req.params.id, course_id, item.total, tag]]
                  );
                }
              }
            });
          }
          if (req.body.question.length) {
            await req.body.question.map(async (item, index) => {
              item.type = item.type ? item.type : "1";
              if (item.type.toString() === "2") {
                let answer = [];
                item.option.forEach(str => {
                  answer.push(str.option_label);
                });
                item.answer = answer.toString();
              }
              item.type = Number(item.type);
              await db.query(
                `UPDATE training_exam_question SET category_id=?, question=?, answer=?, sort=?, type='${item.type}' WHERE id=?`,
                [
                  item.category_id === "" || item.category_id === null
                    ? 0
                    : item.category_id,
                  item.question,
                  item.answer,
                  index,
                  item.id,
                ]
              );

              if (item.option.length) {
                await db.query(
                  `DELETE FROM training_exam_option WHERE question_id='${item.id}'`
                );
                await item.option.map(async itemO => {
                  if (itemO.option_text !== "") {
                    await db.query(
                      `INSERT INTO training_exam_option (question_id, option_label, option_text) VALUES('${item.id}','${itemO.option_label}','${itemO.option_text}')`
                    );
                  }
                });
              }
            });
            return res.json({ error: false, result: result });
          } else {
            return res.json({ error: false, result: result });
          }
        }
      }
    );
  } catch (error) {
    console.log(error, 99887);
  }
};

exports.addQuestion = (req, res) => {
  req.body.type = req.body.type ? req.body.type : "1";
  if (req.body.type.toString() === "2") {
    let answer = [];
    req.body.option.forEach(str => {
      answer.push(str.option_label);
    });
    req.body.answer = answer.toString();
  }
  req.body.type = Number(req.body.type);
  db.query(
    `INSERT INTO training_exam_question
        (exam_id, category_id, question, answer, sort, type)
        VALUES (?);`,
    [
      [
        req.body.exam_id,
        req.body.category_id,
        req.body.question,
        req.body.answer,
        req.body.sort,
        req.body.type || 1,
      ],
    ],
    (error, result) => {
      if (error) {
        return res.json({ error: true, result: error.message });
      } else {
        return res.json({ error: false, result: result });
      }
    }
  );
};

exports.delete = (req, res) => {
  db.query(
    `UPDATE training_exam
        SET status='Inactive'
        WHERE id = ?;`,
    [req.params.id],
    (error, result) => {
      if (error) {
        return res.json({ error: true, result: error.message });
      }

      return res.json({ error: false, result });
    }
  );
};

exports.activate = (req, res) => {
  db.query(
    `UPDATE training_exam
        SET status='Active'
        WHERE id = ?;`,
    [req.params.id],
    (error, result) => {
      if (error) {
        return res.json({ error: true, result: error.message });
      }

      return res.json({ error: false, result });
    }
  );
};

exports.deleteQuestion = (req, res) => {
  db.query(
    `DELETE FROM training_exam_question
        WHERE id = ?;`,
    [req.params.id],
    (error, result) => {
      if (error) {
        return res.json({ error: true, result: error.message });
      }

      db.query(
        `DELETE FROM training_exam_option
                WHERE question_id = ?;`,
        [req.params.id],
        (error, result) => {
          if (error) {
            return res.json({ error: true, result: error.message });
          }

          return res.json({ error: false, result });
        }
      );
    }
  );
};

exports.submit = async (req, res) => {
  let form = {
    exam_id: req.body.exam_id,
    training_user_id: req.body.training_user_id,
    assignee_id: req.body.assignee_id,
    answer: req.body.answer,
    submission_condition: req.body.submission_condition
      ? req.body.submission_condition
      : "Normal",
  };

  if (
    !req.body.exam_id ||
    !req.body.training_user_id ||
    !req.body.assignee_id
  ) {
    return res.json({ error: true, result: "Invalid input, please try again" });
  }

  let getTemporaryQuestion = await db.query(`select id, answer from temporary_exam_result where training_assignee_id=? and training_user_id=? order by id DESC limit 1;`,[form.assignee_id, form.training_user_id]);
  try{
        let questions = JSON.parse(getTemporaryQuestion[0].answer);
        
        let answerMerge = [];
        questions.forEach((answer) => {
            const idx = form.answer.findIndex((answeruser) => { return answeruser.question_id == answer.question_id });
            if(idx > -1){
              if(answer.type == 2 && answer.option.length > 1){

                // sync multi answer short_answer
                let optionTemp = []
                answer.option.forEach((temp)=>{
                  let idx2 = form.answer[idx].option.findIndex((check)=>{ 
                    return check.option_label.toLowerCase() === temp.option_label.toLowerCase() 
                  });
                  if(idx2 > -1){
                    optionTemp.push(form.answer[idx].option[idx2]);
                  }else{
                    optionTemp.push(temp);
                  }
                });
                form.answer[idx].option = optionTemp;
              }
              answerMerge.push(form.answer[idx])
            }else{
                answerMerge.push({...answer, question_id: answer.id, option_label: answer.userAnswer || answer.option_label});
            }
        });
        form.answer = answerMerge;
  }catch(e){}
  //return console.log(JSON.stringify(form.answer,null,null,2),'99999')
  
  let checkSubmission = await db.query(
    `SELECT r.id, repeatable, e.generate_membership FROM training_exam_result r JOIN training_exam e ON e.id = r.exam_id WHERE r.assignee_id = '${form.assignee_id}' AND r.training_user_id = '${form.training_user_id}'`
  );
  if (checkSubmission.length && checkSubmission[0].repeatable === 0) {
    db.query(
      "SELECT e.*, (SELECT SUM(total) FROM training_exam_composition WHERE exam_id=e.id) AS number_of_question FROM training_exam e WHERE e.id=?",
      [form.exam_id],
      async (error, result) => {
        let accumulator = 0;
        let incorrects = 0;
        let corrects = 0;
        let tempAsnwer = [];
        let accumulator_short = 0;
        await Promise.all(
          form.answer.map(async item => {
            // jawaban
            if (item.type == 2) {
              let tmp = ``;
              if (item.option.length) {
                let keyShortAnswer = await db.query(
                  result[0].generate_question === 1
                    ? `select * from training_questions_options where question_id=?`
                    : `select * from training_exam_option where question_id=?`,
                  item.question_id
                );
                if (keyShortAnswer.length) {
                  for (let i = 0; i < item.option.length; i++) {
                    // loop option
                    //accumulator_short = 0;
                    if (i == item.option.length - 1) {
                      tmp += `${item.option[i].option_text}`;
                    } else {
                      tmp += `${item.option[i].option_text}|`;
                    }

                    let state = [];

                    let idx = keyShortAnswer.findIndex(arg => {
                      return item.option[i].option_label === arg.option_label;
                    });
                    if (idx > -1) {
                      let clearYourText = item.option[i].option_text
                        .toLowerCase()
                        .trim()
                        .split(",");
                      let clearKeyText = keyShortAnswer[idx].option_text
                        .toLowerCase()
                        .trim()
                        .split(",");

                      let multiAnswer = [];
                      let tmpAnswer = { incorrect: 0, correct: 0 };
                      clearKeyText.forEach(arg => {
                        let idx = clearYourText.findIndex(arg2 => {
                          return arg2 == arg;
                        });
                        let ada = false;
                        if (idx > -1) {
                          ada = true;
                          tmpAnswer.correct++;
                        } else {
                          ada = false;
                          tmpAnswer.incorrect++;
                        }
                        multiAnswer.push(ada);
                      });

                      if (multiAnswer.indexOf(true) > -1) {
                        state.push(true);
                        accumulator_short += parseFloat(
                          (1 / item.option.length).toFixed(2)
                        );
                        corrects++;
                      } else {
                        incorrects++;
                        state.push(false);
                      }
                      // if (state.indexOf(true) > -1) {
                      // }
                    }
                  }
                }
              }
              db.query(
                `INSERT INTO training_exam_answer (exam_id, training_user_id, assignee_id, question_id, option_label) VALUES (?)`,
                [
                  [
                    form.exam_id,
                    form.training_user_id,
                    form.assignee_id,
                    item.question_id,
                    tmp,
                  ],
                ],
                async (error, result) => {
                  
                  tempAsnwer.push(result.insertId);
                }
              );

              //accumulator = accumulator + accumulator_short;
              //console.log("TEST 2========= ", accumulator_short, accumulator, item)
            } else {
              item.option_label = item.option_label || '';
              db.query(
                `INSERT INTO training_exam_answer (exam_id, training_user_id, assignee_id, question_id, option_label) VALUES (?)`,
                [
                  [
                    form.exam_id,
                    form.training_user_id,
                    form.assignee_id,
                    item.question_id,
                    item.option_label,
                  ],
                ],
                async (error, result) => {
                  
                  tempAsnwer.push(result.insertId);
                }
              );

              let sql =
                result[0].generate_question === 1
                  ? `SELECT answer FROM training_questions WHERE id=?`
                  : `SELECT answer FROM training_exam_question WHERE id=?`;
              let check = await db.query(sql, [item.question_id]);

              accumulator =
                check[0].answer === item.option_label
                  ? accumulator + 1
                  : accumulator;
            }
          })
        );
        let examTitle = result[0].title;
        let examType = result[0].exam === 1 ? "exam" : "quiz";
        let checkTotal = await db.query(
          `SELECT COUNT(id) AS total FROM training_exam_question WHERE exam_id=?`,
          [form.exam_id]
        );
        let total =
          result[0].generate_question === 1
            ? result[0].number_of_question
            : checkTotal[0].total;
        let minScore = await db.query(
          `SELECT minimum_score FROM training_exam WHERE id=?`,
          [form.exam_id]
        );
        const generate_membership = await db.query(
          `SELECT generate_membership FROM training_exam WHERE id=?`,
          [form.exam_id]
        );
        // let correct = accumulator;
        // let incorrect = total - correct;
        // //let score = (correct / total) * 100;
        // let score = (correct / total) * 100;
        let correct = accumulator + corrects;
        let incorrect = total - correct;
        let score = ((accumulator + accumulator_short) / total) * 100;

        score = score === "NaN" ? 0 : score;
        let pass = score < minScore[0].minimum_score ? 0 : 1;

        const isUserCreateMembership =
          result[0].exam === 1 &&
          pass === 1 &&
          generate_membership[0].generate_membership === 1;
        const idMembership = await moduleDB.getIdMembershipByTrainingUser({
          idTrainingUser: req.body.training_user_id,
        });

        try {
            db.query('delete from temporary_exam_result where training_assignee_id=? and training_user_id=?',[form.assignee_id, form.training_user_id]);
            //console.log(test,66,'delete from temporary_exam_result where training_assignee_id=? and training_user_id=?',[form.assignee_id, form.training_user_id])
        } catch (error) { console.log(error,99) }

        return res.json({
          error: false,
          result: {
            result_id: checkSubmission[0].id,
            idMembership:
              generate_membership[0].generate_membership === 1
                ? idMembership.length
                  ? idMembership[0].id
                  : null
                : null,
            take_photo: isUserCreateMembership ? true : false,
          },
        });
      }
    );
  } else {
    //return console.log(2);
    db.query(
      "SELECT e.*, (SELECT SUM(total) FROM training_exam_composition WHERE exam_id=e.id) AS number_of_question FROM training_exam e WHERE e.id=?",
      [form.exam_id],
      async (error, result) => {
        let accumulator = 0;
        let incorrects = 0;
        let corrects = 0;
        let tempAsnwer = [];
        let accumulator_short = 0;
        await Promise.all(
          form.answer.map(async (item, index) => {
            // jawaban
            if (item.type == 2) {
              let tmp = ``;
              if (item.option.length) {
                let keyShortAnswer = await db.query(
                  result[0].generate_question === 1
                    ? `select * from training_questions_options where question_id=?`
                    : `select * from training_exam_option where question_id=?`,
                  item.question_id
                );
                if (keyShortAnswer.length) {
                  for (let i = 0; i < item.option.length; i++) {
                    // loop option
                    //accumulator_short = 0;
                    if (i == item.option.length - 1) {
                      tmp += `${item.option[i].option_text}`;
                    } else {
                      tmp += `${item.option[i].option_text}|`;
                    }

                    let state = [];

                    let idx = keyShortAnswer.findIndex(arg => {
                      return item.option[i].option_label === arg.option_label;
                    });
                    if (idx > -1) {
                      let clearYourText = item.option[i].option_text
                        .toLowerCase()
                        .trim()
                        .split(",");
                      let clearKeyText = keyShortAnswer[idx].option_text
                        .toLowerCase()
                        .trim()
                        .split(",");

                      let multiAnswer = [];
                      let tmpAnswer = { incorrect: 0, correct: 0 };
                      clearKeyText.forEach(arg => {
                        let idx = clearYourText.findIndex(arg2 => {
                          return arg2 == arg;
                        });
                        let ada = false;
                        if (idx > -1) {
                          ada = true;
                          tmpAnswer.correct++;
                        } else {
                          ada = false;
                          tmpAnswer.incorrect++;
                        }
                        multiAnswer.push(ada);
                      });

                      if (multiAnswer.indexOf(true) > -1) {
                        state.push(true);
                        accumulator_short += parseFloat(
                          (1 / item.option.length).toFixed(2)
                        );
                        corrects++;
                      } else {
                        incorrects++;
                        state.push(false);
                      }
                      // if (state.indexOf(true) > -1) {
                      // }
                    }
                  }
                }
              }
              db.query(
                `INSERT INTO training_exam_answer (exam_id, training_user_id, assignee_id, question_id, option_label) VALUES (?)`,
                [
                  [
                    form.exam_id,
                    form.training_user_id,
                    form.assignee_id,
                    item.question_id,
                    tmp,
                  ],
                ],
                async (error, result) => {
                  tempAsnwer.push(result.insertId);
                }
              );

              //accumulator = accumulator + accumulator_short;
              //console.log("TEST 2========= ", accumulator_short, accumulator, item)
            } else {
              item.option_label = item.option_label || '';
              db.query(
                `INSERT INTO training_exam_answer (exam_id, training_user_id, assignee_id, question_id, option_label) VALUES (?)`,
                [
                  [
                    form.exam_id,
                    form.training_user_id,
                    form.assignee_id,
                    item.question_id,
                    item.option_label,
                  ],
                ],
                async (error, result) => {

                  tempAsnwer.push(result.insertId);
                }
              );

              let sql =
                result[0].generate_question === 1
                  ? `SELECT answer FROM training_questions WHERE id=?`
                  : `SELECT answer FROM training_exam_question WHERE id=?`;
              let check = await db.query(sql, [item.question_id]);

              accumulator =
                check[0].answer === item.option_label
                  ? accumulator + 1
                  : accumulator;
            }
          })
        );
        let examTitle = result[0].title;
        let examType = result[0].exam === 1 ? "exam" : "quiz";
        let checkTotal = await db.query(
          `SELECT COUNT(id) AS total FROM training_exam_question WHERE exam_id=?`,
          [form.exam_id]
        );
        let total =
          result[0].generate_question === 1
            ? result[0].number_of_question
            : checkTotal[0].total;
        let minScore = await db.query(
          `SELECT minimum_score,time_limit FROM training_exam WHERE id=?`,
          [form.exam_id]
        );
        let timeLimit = minScore[0].time_limit;
        const generate_membership = await db.query(
          `SELECT generate_membership FROM training_exam WHERE id=?`,
          [form.exam_id]
        );
        let correct = accumulator + corrects;
        let incorrect = total - correct;
        //let score = Math.round((correct / total) * 100);
        let score = ((accumulator + accumulator_short) / total) * 100;

        score = score === "NaN" ? 0 : score;
        let pass = score < minScore[0].minimum_score ? 0 : 1;

        const isUserCreateMembership =
          result[0].exam === 1 &&
          pass === 1 &&
          generate_membership[0].generate_membership === 1;
        db.query(
          `INSERT INTO training_exam_result (training_user_id, exam_id, assignee_id, submission_condition, total, correct, incorrect, score, pass, created_at) VALUES (?)`,
          [
            [
              form.training_user_id,
              form.exam_id,
              form.assignee_id,
              form.submission_condition,
              total,
              correct,
              incorrect,
              score,
              pass,
              new Date(),
            ],
          ],
          async (error, result2) => {
            if (error) {
              return res.json({ error: true, result: error });
            }

            //Create Membership with API
            if (isUserCreateMembership) {
              const dataDetailMembership = {
                idTrainingUser: req.body.training_user_id,
                idAssignee: result2.insertId,
              };
              const response = await createMembershipFunction(
                dataDetailMembership
              );
            }

            let dataRecord = await db.query(`SELECT * FROM training_exam_assignee WHERE id ='${form.assignee_id}'`)
            let dateNow = moment(new Date());
            let startTimes = null;
            let submissionTimes = dateNow;
            let getMinutes = 0;

            if (dataRecord.length) {
              startTimes = moment(dataRecord[0].start_time);
            }

            getMinutes = moment.duration(dateNow.diff(startTimes)).asMinutes();

            if (getMinutes > timeLimit) {
              submissionTimes = startTimes.add(timeLimit, 'minutes');
            }
            submissionTimes = submissionTimes.format("YYYY-MM-DD HH:mm:ss");

            db.query(
              `UPDATE training_exam_assignee SET status='Finish', submission_time='${submissionTimes}' WHERE id='${form.assignee_id}'`,
              async (error, result) => {
                if (error) {
                  return res.json({ error: true, result: error });
                }

                if (dataRecord.length > 0) {
                  db.query(
                    `INSERT INTO training_exam_record (start_time, submission_time, exam_result_id, exam_answer_id) VALUES (?)`,
                    [
                      [
                        dataRecord[0].start_time,
                        submissionTimes,
                        result2.insertId,
                        tempAsnwer.toString(),
                      ],
                    ]
                  );
                }

                const { email } = req.app.token;
                const user = await db.query(
                  `SELECT user_id FROM user WHERE email = ?`,
                  [email]
                );
                let data = {
                  user_id: user[0].user_id,
                  type: 7,
                  activity_id: form.assignee_id,
                  desc: `Your result is done. ${pass ? "You passed the" : `Sorry, you haven't passed the`
                    } ${examType} ${examTitle}`,
                  dest: ``,
                };

                const idMembership =
                  await moduleDB.getIdMembershipByTrainingUser({
                    idTrainingUser: req.body.training_user_id,
                  });
                await db.query(
                  `
                        INSERT INTO
                            notification (user_id, type, activity_id, description, destination, isread )
                            VALUES(?, ?, ?, ?, ?, ?)`,
                  [
                    data.user_id,
                    data.type,
                    data.activity_id,
                    data.desc,
                    data.dest,
                    "0",
                  ]
                );

                try {
                  db.query('delete from temporary_exam_result where training_assignee_id=? and training_user_id=?',[form.assignee_id, req.body.training_user_id]);
                } catch (error) {console.log(error,99)}

                return res.json({
                  error: false,
                  result: {
                    result_id: result2.insertId,
                    idMembership:
                      generate_membership[0].generate_membership === 1
                        ? idMembership.length
                          ? idMembership[0].id
                          : null
                        : null,
                    take_photo: isUserCreateMembership ? true : false,
                  },
                });
              }
            );
          }
        );
      }
    );
  }
};

exports.result = (req, res) => {
  let form = {
    assignee_id: req.params.assignee_id,
  };

  db.query(
    "SELECT r.*, a.start_time, a.submission_time, TIMESTAMPDIFF(MINUTE, a.start_time, a.submission_time) AS work_time FROM training_exam_result r JOIN training_exam_assignee a ON a.id = r.assignee_id WHERE r.assignee_id=? ORDER by r.created_at DESC",
    [form.assignee_id],
    (error, result) => {
      if (error) {
        return res.json({ error: true, result: error.message });
      }

      return res.json({ error: false, result: result[0] });
    }
  );
};
exports.resultByExamResult = (req, res) => {
  let form = {
    result_id: req.params.result_id,
  };

  db.query(
    `SELECT r.*, rec.start_time, rec.submission_time, TIMESTAMPDIFF(MINUTE, rec.start_time, rec.submission_time) AS work_time
    FROM training_exam_result r
    JOIN training_exam_record rec ON rec.exam_result_id = r.id
    WHERE r.id=?`,
    [form.result_id],
    (error, result) => {
      if (error) {
        return res.json({ error: true, result: error.message });
      }

      return res.json({ error: false, result: result[0] });
    }
  );
};

exports.readAnswer = async (req, res) => {
  let form = {
    assignee_id: req.params.assignee_id,
    result_date: req.query.result_date || false,
  };

  let exam = await db.query(
    "SELECT e.* FROM training_exam_assignee a JOIN training_exam e ON e.id = a.exam_id WHERE a.id=?",
    [form.assignee_id]
  );
  let generate = exam[0].generate_question === 1 ? true : false;
  let assign = await db.query(
    `SELECT * FROM training_exam_assignee WHERE id=?`,
    [form.assignee_id]
  );

  if (generate) {
    let default_q = "SELECT * FROM training_exam_answer WHERE assignee_id=?";
    let default_params = [form.assignee_id];
    if (form.result_date) {
      default_q = `SELECT t1.* FROM training_exam_answer t1 LEFT JOIN (SELECT MAX(x.id) as id FROM training_exam_answer x GROUP BY x.question_id) t2 ON t1.id = t2.id where t1.assignee_id=? and t1.created_at=?`;
      let dates = new Date(form.result_date);
      let times = dates.toString().split(" ")[4];
      dates =
        dates.getFullYear() +
        "-" +
        (parseInt(dates.getMonth()) + 1) +
        "-" +
        dates.getDate() +
        " " +
        times;
      default_params = [form.assignee_id, dates];

      // default_q2 = `SELECT t1.* FROM training_exam_answer t1
      // LEFT JOIN (SELECT MAX(x.id) as id FROM training_exam_answer x GROUP BY x.question_id) t2 ON t1.id = t2.id WHERE t1.assignee_id=?`
    }
    db.query(default_q, default_params, async (error, result) => {
      if (error) {
        return res.json({ error: true, result: error.message });
      }

      let question = [];
      await Promise.all(
        result.map(async (item, index) => {
          let itemquestion = await db.query(
            `SELECT * FROM training_questions WHERE id=?`,
            [item.question_id]
          );
          question.push(itemquestion[0]);
          question[question.length - 1].option = await db.query(
            `SELECT * FROM training_questions_options WHERE question_id=?`,
            [item.question_id]
          );
          question[index].user_answer = item.option_label;
        })
      );
      return res.json({ error: false, result: question });
    });
  } else {
    let question = await db.query(
      `SELECT * FROM training_exam_question WHERE exam_id=?`,
      [assign[0].exam_id]
    );

    let default_q = "SELECT * FROM training_exam_answer WHERE assignee_id=?";
    let default_params = [form.assignee_id];
    if (form.result_date) {
      default_q = `SELECT t1.* FROM training_exam_answer t1 LEFT JOIN (SELECT MAX(x.id) as id 
            FROM training_exam_answer x GROUP BY x.question_id) t2 ON t1.id = t2.id where t1.assignee_id=? and TIME_TO_SEC(TIMEDIFF(?,t1.created_at)) <= 2 `;

      // TIME_TO_SEC(TIMEDIFF('2010-08-20 12:02:00', '2010-08-20 12:01:59'))
      let dates = new Date(form.result_date);
      let times = dates.toString().split(" ")[4];
      // let dates2 = new Date(form.result_date);
      // let times2 = times.split(":");
      // sec = parseInt(times2[2]) - 1;
      // if (sec < 10) {
      //     sec = "0" + sec;
      // }

      //times2 = times2[0] + ":" + times2[1] + ":" + sec;
      dates =
        dates.getFullYear() +
        "-" +
        (parseInt(dates.getMonth()) + 1) +
        "-" +
        dates.getDate() +
        " " +
        times;
      default_params = [form.assignee_id, dates];
      //dates2 = dates2.getFullYear() + "-" + (parseInt(dates2.getMonth()) + 1) + "-" + dates2.getDate() + " " + times2;
      //default_params = [form.assignee_id, dates, form.assignee_id, dates, dates2];

      // default_q2 = `SELECT t1.* FROM training_exam_answer t1
      // LEFT JOIN (SELECT MAX(x.id) as id FROM training_exam_answer x GROUP BY x.question_id) t2 ON t1.id = t2.id WHERE t1.assignee_id=?`
    }
    db.query(default_q, default_params, async (error, result) => {
      if (error) {
        return res.json({ error: true, result: error.message });
      }

      try {
        //console.log(default_q, default_params, "TEST")
        await Promise.all(
          question.map(async (item, index) => {
            question[index].option = await db.query(
              `SELECT * FROM training_exam_option WHERE question_id=?`,
              [item.id]
            );
            question[index].user_answer = result.filter(
              items => items.question_id === item.id
            )[0].option_label;
          })
        );
      } catch (e) { }

      return res.json({ error: false, result: question });
    });
  }
};

async function readAnswerByResult(req, res){
  return new Promise(async (hasil)=>{

    let form = {
      result_id: req.params.result_id,
    };
  
    let sqlString = `SELECT e.*, rec.exam_answer_id 
      FROM training_exam_result r 
      JOIN training_exam e ON e.id = r.exam_id 
      JOIN training_exam_record rec ON rec.exam_result_id = r.id 
      WHERE r.id=?`;
  
    if (req.query.by === "assignee") {
      sqlString = ` 
              SELECT e.*, rec.exam_answer_id  
              FROM training_exam_result r 
              JOIN training_exam e ON e.id = r.exam_id 
              JOIN training_exam_record rec ON rec.exam_result_id = r.id 
              WHERE r.id=? order by r.created_at desc limit 1`;
    }
  
    let exam = await db.query(sqlString, [form.result_id]);
    if (exam.length) {
      let generate = exam[0].generate_question === 1 ? true : false;
      if (generate) {
        db.query(
          `SELECT * FROM training_exam_answer WHERE id IN(?) ORDER BY id ASC`,
          [
            exam[0].exam_answer_id === null || exam[0].exam_answer_id === ""
              ? ""
              : exam[0].exam_answer_id.split(","),
          ],
          async (error, result) => {
            if (error) {
              return hasil({ error: true, result: error.message });
              return res.json({ error: true, result: error.message });
            }
            let question = [];
            for (let i = 0; i < result.length; i++) {
              let itemquestion = await db.query(
                `SELECT * FROM training_questions WHERE id=?`,
                [result[i].question_id]
              );
              let qTemp = itemquestion[0];
              qTemp.option = await db.query(
                `SELECT * FROM training_questions_options WHERE question_id=? ORDER BY option_label ASC`,
                [result[i].question_id]
              );
              qTemp.user_answer = result[i].option_label;
  
              question.push(qTemp);
            }
  
            await Promise.all(
              question.map(async (item, index) => {
                question[index].option = await db.query(
                  `SELECT * FROM training_questions_options WHERE question_id=? ORDER BY option_label ASC`,
                  [item.id]
                );
                // console.log(result.filter(items => items.question_id === item.id), item.id)
                question[index].isShortedAnswer = item.type === 2 ? true : false;
                try {
                  const userAnswer = result.filter(
                    items => items.question_id === item.id
                  );
                  question[index].user_answer = userAnswer.length
                    ? userAnswer[0].option_label
                    : "";
                  question[index].isShortedAnswer =
                    item.type === 2 ? true : false;
  
                  let objects = {
                    id: item.id,
                    question: item.question,
                    pass: false,
                    answer: {
                      keyAnswer: null,
                      yourAnswer: null,
                    },
                    option: [],
                    type: item.type,
                  };
  
                  if (item.type === 2) {
                    //Change Questions {ANSWER} to ....
                    const regExpAnswerParam = /{ANSWER}/g;
                    item.question = item.question.replace(
                      regExpAnswerParam,
                      "...."
                    );
  
                    //str.answer
                    let arr_answer = item.answer.split(",");
                    let tmp_answer = [];
                    let origin = item.user_answer.split("|");
                    let arr_user_answer = item.user_answer
                      .toLocaleLowerCase()
                      .replace(/\s/g, "");
                    arr_user_answer = arr_user_answer.split("|");
  
                    objects.answer.keyAnswer = [];
                    objects.answer.yourAnswer = [];
                    let state_pass = [];
  
                    item.option = item.option.map((str_op, indks) => {
                      // sync option dengan answer
                      let idx = arr_answer.indexOf(str_op.option_label);
                      if (idx > -1) {
                        let op_text_lower = str_op.option_text
                          .toLocaleLowerCase()
                          .replace(/\s/g, "");
                        tmp_answer.push(op_text_lower);
  
                        // checking user_answer
                        let splits = op_text_lower.split(",");
                        let state_pass = false;
                        for (let i = 0; i < splits.length; i++) {
                          let idx = arr_user_answer.indexOf(splits[i]);
                          if (idx > -1) state_pass = true;
                        }
                        let idx_arr_user_answer =
                          arr_user_answer.indexOf(op_text_lower);
                        let userAnswer = null;
                        if (idx_arr_user_answer > -1) {
                          //build keyAnswer static
                          userAnswer = {
                            id: 0,
                            question_id: item.id,
                            option_label: str_op.option_label,
                            // "option_text": str_op.option_text,//arr_user_answer[idx_arr_user_answer],
                            option_text: arr_user_answer[idx_arr_user_answer], //arr_user_answer[idx_arr_user_answer],
                            pass: true,
                          };
                        } else {
                          userAnswer = {
                            id: 0,
                            question_id: item.id,
                            option_label: str_op.option_label,
                            // "option_text": str_op.option_text,//arr_user_answer[indks] || '',
                            option_text: arr_user_answer[indks] || "", //arr_user_answer[indks] || '',
                            pass: false,
                          };
                        }
                        objects.answer.yourAnswer.push(userAnswer);
                        objects.answer.keyAnswer.push({ ...str_op, pass: true });
                        objects.option.push(str_op);
  
                        let jawaban = "";
                        if (idx_arr_user_answer > -1) {
                          jawaban = arr_user_answer[idx_arr_user_answer];
                        } else {
                          jawaban = arr_user_answer[indks] || "";
                        }
  
                        return {
                          ...str_op,
                          user_answer: jawaban,
                          pass:
                            idx_arr_user_answer > -1
                              ? true
                              : state_pass
                                ? true
                                : false,
                        };
                      } else {
                        console.log("ANSWER NOT FOUND ============");
                      }
                    });
  
                    // set pass
                    let temp = null;
                    let isPassedQuestion = false;
                    item.option.forEach(option => {
  
                      temp = option.pass;
                      if (temp) {
                        isPassedQuestion = true;
                      }
                    });
                    // console.log(item.option,'eleg?')
  
                    question[index].passQuestion = isPassedQuestion;
                  }
                } catch (e) {
                  question[index].user_answer = null;
                }
              })
            );

            return hasil({ error: false, result: question });
            return res.json({ error: false, result: question });
          }
        );
      } else {
        let question = await db.query(
          `SELECT * FROM training_exam_question WHERE exam_id=?`,
          [exam[0].id]
        );
        db.query(
          "SELECT * FROM training_exam_answer WHERE id IN(?)",
          [exam[0].exam_answer_id.split(",")],
          async (error, result) => {
            if (error) {
              return hasil({ error: true, result: error.message });
              return res.json({ error: true, result: error.message });
            }
  
            await Promise.all(
              question.map(async (item, index) => {
                question[index].option = await db.query(
                  `SELECT * FROM training_exam_option WHERE question_id=? ORDER BY option_label ASC`,
                  [item.id]
                );
                // console.log(result.filter(items => items.question_id === item.id), item.id)
                question[index].isShortedAnswer = item.type === 2 ? true : false;
                try {
                  const userAnswer = result.filter(
                    items => items.question_id === item.id
                  );
                  question[index].user_answer = userAnswer.length
                    ? userAnswer[0].option_label
                    : "";
                  question[index].isShortedAnswer =
                    item.type === 2 ? true : false;
  
                  let objects = {
                    id: item.id,
                    question: item.question,
                    pass: false,
                    answer: {
                      keyAnswer: null,
                      yourAnswer: null,
                    },
                    option: [],
                    type: item.type,
                  };
  
                  if (item.type == 2) {
                    //Change Questions {ANSWER} to ....
                    const regExpAnswerParam = /{ANSWER}/g;
                    item.question = item.question.replace(
                      regExpAnswerParam,
                      "...."
                    );
  
                    //str.answer
                    let arr_answer = item.answer.split(",");
                    let tmp_answer = [];
                    let origin = item.user_answer.split("|");
                    let arr_user_answer = item.user_answer
                      .toLocaleLowerCase()
                      .replace(/\s/g, "");
                    arr_user_answer = arr_user_answer.split("|");
  
                    objects.answer.keyAnswer = [];
                    objects.answer.yourAnswer = [];
                    let state_pass = [];
  
                    item.option = item.option.map((str_op, indks) => {
                      // sync option dengan answer
                      let idx = arr_answer.indexOf(str_op.option_label);
                      if (idx > -1) {
                        let op_text_lower = str_op.option_text
                          .toLocaleLowerCase()
                          .replace(/\s/g, "");
                        tmp_answer.push(op_text_lower);
  
                        // checking user_answer
                        let splits = op_text_lower.split(",");
                        let state_pass = false;
                        for (let i = 0; i < splits.length; i++) {
                          let idx = arr_user_answer.indexOf(splits[i]);
                          if (idx > -1) state_pass = true;
                        }
                        let idx_arr_user_answer =
                          arr_user_answer.indexOf(op_text_lower);
                        let userAnswer = null;
                        if (idx_arr_user_answer > -1) {
                          //build keyAnswer static
                          userAnswer = {
                            id: 0,
                            question_id: item.id,
                            option_label: str_op.option_label,
                            // "option_text": str_op.option_text,//arr_user_answer[idx_arr_user_answer],
                            option_text: arr_user_answer[idx_arr_user_answer], //arr_user_answer[idx_arr_user_answer],
                            pass: true,
                          };
                        } else {
                          userAnswer = {
                            id: 0,
                            question_id: item.id,
                            option_label: str_op.option_label,
                            // "option_text": str_op.option_text,//arr_user_answer[indks] || '',
                            option_text: arr_user_answer[indks] || "", //arr_user_answer[indks] || '',
                            pass: false,
                          };
                        }
                        objects.answer.yourAnswer.push(userAnswer);
                        objects.answer.keyAnswer.push({ ...str_op, pass: true });
                        objects.option.push(str_op);
  
                        let jawaban = "";
                        if (idx_arr_user_answer > -1) {
                          jawaban = arr_user_answer[idx_arr_user_answer];
                        } else {
                          jawaban = arr_user_answer[indks] || "";
                        }
  
                        return {
                          ...str_op,
                          user_answer: jawaban,
                          pass:
                            idx_arr_user_answer > -1
                              ? true
                              : state_pass
                                ? true
                                : false,
                        };
                      } else {
                        console.log("ANSWER NOT FOUND ============");
                      }
                    });
  
                    // set pass
                    let temp = null;
                    let isPassedQuestion = false;
                    item.option.forEach(option => {
                      temp = option.pass;
                      if (temp) {
                        isPassedQuestion = true;
                      }
                    });
  
                    question[index].passQuestion = isPassedQuestion;
                  }
                } catch (e) {
                  question[index].user_answer = null;
                }
              })
            );
            return hasil({ error: false, result: question });
            return res.json({ error: false, result: question });
          }
        );
      }
    } else {
      return hasil({ error: true, result: "result id not found" });
      return res.json({ error: true, result: "result id not found" });
    }
  });
};

exports.readAnswerByResult = async (req, res)=>{
  let result = await readAnswerByResult(req)
  if(req.params.inner){
    return result;
  }else{
    return res.json(result)
  }
}

exports.assign = async (req, res) => {
  let training_company_id = await db.query(
    `SELECT training_company_id FROM training_user WHERE id='${req.body.training_user_id}'`
  );
  let form = {
    exam_id: req.body.exam_id,
    training_user_id: req.body.training_user_id,
    status: "Open",
    created_by: req.body.created_by,
    created_at: new Date(),
    licenses_allocation_id: await db.query(
      `SELECT a.id, e.exam, e.generate_membership, e.multiple_assign FROM training_exam e LEFT JOIN training_licenses_allocation a ON e.licenses_type_id = a.licenses_type_id WHERE e.id = ${req.body.exam_id} AND a.training_company_id='${training_company_id[0].training_company_id}'`
    ),
  };

  let remaining = form.licenses_allocation_id.length
    ? await db.query(
      `SELECT remaining_allocation FROM training_licenses_allocation WHERE id='${form.licenses_allocation_id[0].id}'`
    )
    : [];
  let examDetail = await db.query(
    `SELECT * FROM training_exam WHERE id = ${req.body.exam_id}`
  );
  if (remaining.length || examDetail[0].exam === 0 || examDetail[0].consume_quota === 0 || !featureQuota) {
    const isRemainingAllocation = remaining.length ? remaining[0].remaining_allocation > 0 : !featureQuota ? true : false;
    let checkPass = await db.query(
      `SELECT pass FROM training_exam_result WHERE training_user_id=? AND exam_id=?`,
      [form.training_user_id, form.exam_id]
    );
    let checkLicense = await db.query(
      `SELECT * FROM training_membership WHERE training_user_id=? AND status='Active'`,
      [form.training_user_id]
    );
    if (
      examDetail[0].generate_membership === 0 ||
      (examDetail[0].generate_membership === 1 &&
        examDetail[0].multiple_assign === 1) ||
      (examDetail[0].generate_membership === 1 &&
        examDetail[0].multiple_assign === 0 &&
        !checkLicense.length) ||
      (examDetail[0].generate_membership === 1 &&
        examDetail[0].multiple_assign === 0 &&
        checkLicense.length &&
        !checkPass.length)
    ) {
      let checkOpen = await db.query(
        `SELECT a.* FROM training_exam_assignee a WHERE a.training_user_id=? AND a.exam_id=? AND a.status='Open' ORDER BY a.created_at DESC`,
        [form.training_user_id, form.exam_id]
      );
      if (!checkOpen.length) {
        if (
          examDetail[0].exam === 0 || examDetail[0].consume_quota === 0
            ? true
            : isRemainingAllocation || examDetail[0].exam === 0
        ) {
          db.query(
            "INSERT INTO training_exam_assignee (exam_id, training_user_id, status, created_at) VALUES (?)",
            [
              [
                form.exam_id,
                form.training_user_id,
                form.status,
                form.created_at,
              ],
            ],
            (error, result) => {
              if (error) {
                return res.json({ error: true, result: error.message });
              }

              if(featureQuota) {
                let formAllocation = {
                  licenses_allocation_id: form.licenses_allocation_id,
                  amount: -1,
                  type: "usage",
                  created_at: moment(new Date()).format("YYYY-MM-DD HH:mm:ss"),
                  created_by: req.body.created_by,
                };
                if (examDetail[0].exam === 1 && examDetail[0].consume_quota === 1) {
                  db.query(
                    `INSERT INTO training_licenses_transaction (licenses_allocation_id, amount, type, note, created_at, created_by) VALUES(?)`,
                    [
                      [
                        formAllocation.licenses_allocation_id[0].id,
                        formAllocation.amount,
                        formAllocation.type,
                        "",
                        formAllocation.created_at,
                        formAllocation.created_by,
                      ],
                    ],
                    (error, result) => {
                      if (error) {
                        res.json({ error: true, result: error.message });
                      } else {
                        //console.log(`UPDATE training_licenses_allocation a SET a.remaining_allocation = (SELECT SUM(amount) FROM training_licenses_transaction WHERE licenses_allocation_id='${formAllocation.licenses_allocation_id[0].id}') WHERE a.id='${formAllocation.licenses_allocation_id[0].id}'`, '2217')
                        db.query(
                          `UPDATE training_licenses_allocation a SET a.remaining_allocation = (SELECT SUM(amount) FROM training_licenses_transaction WHERE licenses_allocation_id='${formAllocation.licenses_allocation_id[0].id}') WHERE a.id='${formAllocation.licenses_allocation_id[0].id}'`,
                          (error, result) => {
                            if (error) {
                              res.json({ error: true, result: error.message });
                            } else {
                              res.json({ error: false, result: result });
                            }
                          }
                        );
                      }
                    }
                  );
                } else {
                  return res.json({ error: false, result: result });
                }
              }else {
                return res.json({ error: false, result: result });
              }
            }
          );
        } else {
          return res.json({
            error: false,
            result: "validationError",
            message: "Have no enough quota",
          });
        }
      } else {
        return res.json({
          error: false,
          result: "validationError",
          message:"User has been assigned."
          //message: "There is an open assignment with same exam",
        });
      }
    } else {
      return res.json({
        error: false,
        result: "validationError",
        message: "This user already passed this exam",
      });
    }
  } else {
    return res.json({
      error: false,
      result: "validationError",
      message: "Have no enough quota2",
    });
  }
};

exports.assignBulk = async (req, res) => {
  let form = {
    exam_id: req.body.exam_id,
    training_user_id: req.body.training_user_id,
    training_company_id: req.body.training_company_id,
    status: "Open",
    created_by: req.body.created_by,
    created_at: new Date(),
    licenses_allocation_id: await db.query(
      `SELECT a.id, e.exam, e.generate_membership, e.multiple_assign FROM training_exam e LEFT JOIN training_licenses_allocation a ON e.licenses_type_id = a.licenses_type_id WHERE e.id = ${req.body.exam_id} AND a.training_company_id='${req.body.training_company_id}'`
    ),
  };

  let examDetail = await db.query(
    `SELECT * FROM training_exam WHERE id = ${req.body.exam_id}`
  );
  let remaining = form.licenses_allocation_id.length
    ? await db.query(
      `SELECT remaining_allocation FROM training_licenses_allocation WHERE id='${form.licenses_allocation_id[0].id}'`
    )
    : [];

  if (remaining.length || examDetail[0].exam === 0 || !featureQuota) {
    const isRemainingAllocation = remaining.length ? remaining[0].remaining_allocation > 0 : !featureQuota ? true : false;
    if (
      examDetail[0].exam === 0 || examDetail[0].consume_quota === 0
        ? true
        : isRemainingAllocation > form.training_user_id.length ||
        examDetail[0].exam === 0
    ) {
      let inserted = [];
      for (let i = 0; i < form.training_user_id.length; i++) {
        let checkLicense = await db.query(
          `SELECT * FROM training_membership WHERE training_user_id=? AND status='Active'`,
          [form.training_user_id[i]]
        );
        let checkPass = await db.query(
          `SELECT pass FROM training_exam_result WHERE training_user_id=? AND exam_id=?`,
          [form.training_user_id[i], form.exam_id]
        );
        if (
          examDetail[0].generate_membership === 0 ||
          (examDetail[0].generate_membership === 1 &&
            examDetail[0].multiple_assign === 1) ||
          (examDetail[0].generate_membership === 1 &&
            examDetail[0].multiple_assign === 0 &&
            !checkLicense.length) ||
          (examDetail[0].generate_membership === 1 &&
            examDetail[0].multiple_assign === 0 &&
            checkLicense.length &&
            !checkPass.length)
        ) {
          let checkOpen = await db.query(
            `SELECT a.* FROM training_exam_assignee a WHERE a.training_user_id=? AND a.exam_id=? AND (a.status='Open' or a.status = 'Start') ORDER BY a.created_at DESC`,
            [form.training_user_id[i], form.exam_id]
          );
          if (!checkOpen.length) {
            db.query(
              "INSERT INTO training_exam_assignee (exam_id, training_user_id, status, created_at) VALUES (?)",
              [
                [
                  form.exam_id,
                  form.training_user_id[i],
                  form.status,
                  form.created_at,
                ],
              ],
              (error, result) => {
                inserted.push(result.insertId);
              }
            );
          } else {
            if (inserted.length) {
              await db.query(
                `DELETE FROM training_exam_assignee WHERE id IN (?)`,
                [inserted]
              );
            }
            let errorUser = await db.query(
              `SELECT name FROM training_user WHERE id='${form.training_user_id[i]}'`
            );
            return res.json({
              error: false,
              result: "validationError",
              message: `There is an open assignment with same exam for user '${errorUser[0].name}'. User has been assigned.`,
            });
          }
        } else {
          if (inserted.length) {
            await db.query(
              `DELETE FROM training_exam_assignee WHERE id IN (?)`,
              [inserted]
            );
          }
          let errorUser = await db.query(
            `SELECT name FROM training_user WHERE id='${form.training_user_id[i]}'`
          );
          return res.json({
            error: false,
            result: "validationError",
            message: `There is an passed submission with same exam for user '${errorUser[0].name}'. User has been assigned.`,
          });
        }
      }

      if(featureQuota) {
        let formAllocation = {
          licenses_allocation_id: form.licenses_allocation_id,
          amount: form.training_user_id.length * -1,
          type: "usage",
          created_at: moment(new Date()).format("YYYY-MM-DD HH:mm:ss"),
          created_by: form.created_by,
        };
        if (examDetail[0].exam === 1 && examDetail[0].consume_quota === 1) {
          db.query(
            `INSERT INTO training_licenses_transaction (licenses_allocation_id, amount, type, note, created_at, created_by) VALUES(?)`,
            [
              [
                formAllocation.licenses_allocation_id[0].id,
                formAllocation.amount,
                formAllocation.type,
                "",
                formAllocation.created_at,
                formAllocation.created_by,
              ],
            ],
            (error, result) => {
              if (error) {
                res.json({ error: true, result: error.message });
              } else {
                db.query(
                  `UPDATE training_licenses_allocation a SET a.remaining_allocation = (SELECT SUM(amount) FROM training_licenses_transaction WHERE licenses_allocation_id='${formAllocation.licenses_allocation_id[0].id}') WHERE a.id='${formAllocation.licenses_allocation_id[0].id}'`,
                  (error, result) => {
                    if (error) {
                      res.json({ error: true, result: error.message });
                    }
                  }
                );
              }
            }
          );
        }
      }
    } else {
      return res.json({
        error: false,
        result: "validationError",
        message: "Have no enough quota",
      });
    }
  } else {
    return res.json({
      error: false,
      result: "validationError",
      message: "Have no enough quota",
    });
  }
  res.json({ error: false, result: "success" });
};

exports.deleteAssigneeBulk = async (req, res) => {
  let form = {
    id: req.query.id,
    created_by: req.query.created_by,
  };
  let msg = { error: false, result: "success" };
  try {
    form = await validatorRequest.deleteBulkTrainingExam(form);
    let dataAssignee = await db.query(
      `SELECT e.id AS exam_id, c.id AS training_company_id, GROUP_CONCAT(a.id) AS assignee_id, e.exam AS exam, e.consume_quota AS consume_quota
        FROM training_exam_assignee a
        JOIN training_exam e ON e.id = a.exam_id
        JOIN training_user u ON u.id = a.training_user_id
        JOIN training_company c ON c.id = u.training_company_id
        WHERE a.id IN (?)
        GROUP BY e.id, c.id`,
      [form.id]
    );

    console.log(dataAssignee,'?')

    for (let i = 0; i < dataAssignee.length; i++) {
      if(featureQuota) {
        let formAllocation = {
          licenses_allocation_id: await db.query(
            `SELECT a.id, e.exam FROM training_exam e LEFT JOIN training_licenses_allocation a ON e.licenses_type_id = a.licenses_type_id WHERE e.id = ${dataAssignee[i].exam_id} AND a.training_company_id=${dataAssignee[i].training_company_id}`
          ),
          amount: dataAssignee[i].assignee_id.split(",").length,
          type: "return",
          created_at: moment(new Date()).format("YYYY-MM-DD HH:mm:ss"),
          created_by: form.created_by,
        };
        if (dataAssignee[0].exam === 1 && dataAssignee[0].consume_quota === 1) {
          try {
            await db.query(
              `INSERT INTO training_licenses_transaction (licenses_allocation_id, amount, type, note, created_at, created_by) VALUES(?)`,
              [
                [
                  formAllocation.licenses_allocation_id[0].id,
                  formAllocation.amount,
                  formAllocation.type,
                  "",
                  formAllocation.created_at,
                  formAllocation.created_by,
                ],
              ]
            );
  
            await db.query(
              `UPDATE training_licenses_allocation a SET a.remaining_allocation = (SELECT SUM(amount) FROM training_licenses_transaction WHERE licenses_allocation_id='${formAllocation.licenses_allocation_id[0].id}') WHERE a.id='${formAllocation.licenses_allocation_id[0].id}'`
            );
          } catch (e) {
            msg = { error: true, result: e.message };
            break;
          }
        }
      }
    }
    if (!msg.error) {
      await db.query("DELETE FROM training_exam_assignee WHERE id IN (?)", [
        form.id,
      ]);
    }
  } catch (e) {
    msg = { error: true, result: e };
  }
  res.json(msg);
};

exports.readAssignee = (req, res) => {
  let form = {
    training_user_id: req.params.training_user_id,
  };

  db.query(
    "SELECT a.*, e.title, e.exam FROM training_exam_assignee a LEFT JOIN training_exam e ON e.id = a.exam_id WHERE a.training_user_id=? ORDER BY a.created_at DESC",
    [form.training_user_id],
    (error, result) => {
      if (error) {
        return res.json({ error: true, result: error.message });
      }

      db.query(
        "SELECT name FROM training_user WHERE id=? LIMIT 1",
        [form.training_user_id],
        (error, result2) => {
          if (error) {
            return res.json({ error: true, result: error.message });
          }

          return res.json({
            error: false,
            result: { name: result2[0].name, assignee: result },
          });
        }
      );
    }
  );
};

exports.readAssigneeByExam = (req, res) => {
  let form = {
    exam_id: req.params.exam_id,
  };

  db.query(
    "SELECT a.status AS assignment_status, a.created_at AS assignment_date, u.*, c.name AS company, a.id FROM training_exam_assignee a LEFT JOIN training_user u ON u.id = a.training_user_id LEFT JOIN training_company c ON c.id = u.training_company_id WHERE a.exam_id=? ORDER BY a.created_at DESC",
    [form.exam_id],
    (error, result) => {
      if (error) {
        return res.json({ error: true, result: error.message });
      }

      return res.json({ error: false, result: result });
    }
  );
};

exports.deleteAssignee = async (req, res) => {
  let form = {
    id: req.params.id,
    created_by: req.params.created_by,
  };

  let exam = await db.query(
    "SELECT e.*, u.training_company_id FROM training_exam_assignee a JOIN training_exam e ON e.id = a.exam_id JOIN training_user u ON u.id = a.training_user_id WHERE a.id=?",
    [form.id]
  );
  db.query(
    "DELETE FROM training_exam_assignee WHERE id=?",
    [form.id],
    async (error, result) => {
      if (error) {
        return res.json({ error: true, result: error.message });
      }

      if(featureQuota) {
        let formAllocation = {
          licenses_allocation_id: await db.query(
            `SELECT a.id, e.exam FROM training_exam e LEFT JOIN training_licenses_allocation a ON e.licenses_type_id = a.licenses_type_id WHERE e.id = ${exam[0].id} AND a.training_company_id=${exam[0].training_company_id}`
          ),
          amount: 1,
          type: "return",
          created_at: moment(new Date()).format("YYYY-MM-DD HH:mm:ss"),
          created_by: form.created_by,
        };
        if (exam[0].exam === 1 && exam[0].consume_quota === 1) {
          db.query(
            `INSERT INTO training_licenses_transaction (licenses_allocation_id, amount, type, note, created_at, created_by) VALUES(?)`,
            [
              [
                formAllocation.licenses_allocation_id[0].id,
                formAllocation.amount,
                formAllocation.type,
                "",
                formAllocation.created_at,
                formAllocation.created_by,
              ],
            ],
            (error, result) => {
              if (error) {
                res.json({ error: true, result: error.message });
              } else {
                db.query(
                  `UPDATE training_licenses_allocation a SET a.remaining_allocation = (SELECT SUM(amount) FROM training_licenses_transaction WHERE licenses_allocation_id='${formAllocation.licenses_allocation_id[0].id}') WHERE a.id='${formAllocation.licenses_allocation_id[0].id}'`,
                  (error, result) => {
                    if (error) {
                      res.json({ error: true, result: error.message });
                    } else {
                      res.json({ error: false, result: result });
                    }
                  }
                );
              }
            }
          );
        } else {
          return res.json({ error: false, result: result });
        }
      }else {
        return res.json({ error: false, result: result });
      }
    }
  );
};

exports.report = (req, res) => {
  let form = {
    start: moment(req.body.start).format("YYYY-MM-DD"),
    end: moment(req.body.end).format("YYYY-MM-DD"),
    licenses_type: req.body.licenses_type,
    company: req.body.company,
    training_company: req.body.training_company,
    pass: req.body.pass,
    cert: req.body.cert,
  };
  let queryFilterLicenses = `AND e.licenses_type_id IN (${String(
    form.licenses_type
  )})`;
  let queryFilterTrainingCompany = `AND u.training_company_id IN (${String(
    form.training_company
  )})`;
  let queryFilterPass = `AND r.pass IN (${String(form.pass)})`;
  let queryFilterCert = `AND (r.certificate_status IN ('${form.cert.join(
    `','`
  )}') ${form.cert.filter(item => item === "No").length
    ? `OR r.certificate_status IS NULL)`
    : ")"
    } `;
  db.query(
    `SELECT r.*,
        IF(rec.start_time IS NULL, a.start_time, rec.start_time) AS start_time,
        IF(rec.submission_time IS NULL, a.submission_time, rec.submission_time) AS submission_time,
        -- SEC_TO_TIME(TIMESTAMPDIFF(SECOND, IF(rec.start_time IS NULL, a.start_time, rec.start_time), IF(rec.submission_time IS NULL, a.submission_time, rec.submission_time))) AS work_time,
        if(
            	
          TIMESTAMPDIFF(MINUTE, IF(rec.start_time IS NULL, a.start_time, rec.start_time), IF(rec.submission_time IS NULL, a.submission_time, rec.submission_time)) > e.time_limit
          ,
          SEC_TO_TIME(TIMESTAMPDIFF(SECOND, IF(rec.start_time IS NULL, a.start_time, rec.start_time), date_add(IF(rec.start_time IS NULL, a.start_time, rec.start_time), INTERVAL e.time_limit MINUTE)))
          ,
          SEC_TO_TIME(TIMESTAMPDIFF(SECOND, IF(rec.start_time IS NULL, a.start_time, rec.start_time), IF(rec.submission_time IS NULL, a.submission_time, rec.submission_time)))
     
        ) as work_time,
        u.name,
        u.email,
        c.title AS course_name,
        e.title AS exam_name,
        (
        	CASE
            WHEN e.exam = 1 && e.status = 'Inactive' THEN 'Inactive Exam'
            WHEN e.exam = 0 && e.status = 'Inactive' THEN 'Inactive Quiz'
            WHEN e.exam = 1 && e.status = 'Active' THEN 'Exam'
            WHEN e.exam = 0 && e.status = 'Active' THEN 'Quiz'
			ELSE 'Exam'
            END
        ) AS exam_type,
        e.minimum_score,
        lt.name AS licenses_type,
        tc.name AS training_company,
        u.license_number
    FROM training_exam_result r
    LEFT JOIN training_exam_assignee a ON a.id = r.assignee_id
    LEFT JOIN training_user u ON u.id = r.training_user_id
    LEFT JOIN training_company tc ON tc.id = u.training_company_id
    LEFT JOIN training_exam e ON e.id = r.exam_id
    LEFT JOIN training_licenses_type lt ON lt.id = e.licenses_type_id
    LEFT JOIN training_course c ON c.id = e.course_id
    LEFT JOIN training_exam_record rec ON rec.exam_result_id = r.id
    WHERE r.created_at BETWEEN '${moment(new Date(form.start)).format(
      "YYYY-MM-DD"
    )}' AND '${moment(new Date(form.end)).format("YYYY-MM-DD")} 23:59:59'
        AND e.company_id='${form.company}'
        ${form.licenses_type.length ? queryFilterLicenses : ""}
        ${form.training_company.length ? queryFilterTrainingCompany : ""}
        ${form.pass.length ? queryFilterPass : ""}
        ${form.cert.length ? queryFilterCert : ""}
    ORDER BY r.created_at DESC`,
    (error, result) => {
      if (error) {
        return res.json({ error: true, result: error });
      }

      return res.json({ error: false, result: result });
    }
  );
};

let storages = multer.diskStorage({
  destination: (req, file, cb) => {
    if (file.fieldname === "signature") {
      cb(null, "./public/training/certificate/signature");
    } else if (file.fieldname === "cert_logo") {
      cb(null, "./public/training/certificate/cert_logo");
    }
  },
  filename: (req, file, cb) => {
    let filetype = "";
    if (file.mimetype === "image/gif") {
      filetype = "gif";
    }
    if (file.mimetype === "image/png") {
      filetype = "png";
    }
    if (file.mimetype === "image/jpeg") {
      filetype = "jpg";
    }
    cb(null, "img-" + Date.now() + "." + filetype);
  },
});
let uploadSignature = multer({ storage: storages }).fields([
  { name: "signature", maxCount: 10 },
  { name: "cert_logo", maxCount: 1 },
]);

let html = (
  nama,
  cert_title,
  tanggal,
  signature,
  sign,
  logo,
  cert_subtitle,
  cert_description,
  cert_topic,
  cert_background,
  cert_orientation,
  bodyNewPage
) => {
  console.log(sign, signature,'???')
  let TTD2 = [];
  signature.map(x => {
    TTD2.push(`file://${path.resolve(__dirname, `../${x.path}`)}`);
  });
  let signData = "";
  sign.map((item, index) => {
    signData =
      signData +
      `<span style="display: table-cell;">
		 <img style="height: 192px" src="${TTD2[index]}" /><br>
		 <span style="font-size:25px">${item.cert_sign_name}</span><br>
		 <span style="font-size:25px">${item.cert_sign_title}</span>
		</span>`;
  });
  let Group1 =
    logo.substring(0, 4) === "http"
      ? logo
      : `file://${path.resolve(__dirname, `../${logo}`)}`;
  // BG = path.normalize(BG);
  return `<div style="width:${cert_orientation === "landscape" ? "1675px" : "1160px"
}; height:${cert_orientation === "landscape" ? "1160px" : "1675px"
}; padding:20px; text-align:center; border: ${cert_background ? "none" : "10px solid #787878"
}; font-size:25px; position: relative">
<img src="${cert_background}" style="width: 100%; height: 100%; position: absolute; top: 0px; left: 0px; z-index: 0" />
<div style="width:${cert_orientation === "landscape" ? "1625px" : "1110px"
}; height:${cert_orientation === "landscape" ? "1110px" : "1475px"
}; padding:20px; ${cert_orientation === "portrait" && "padding-top:150px;"
} text-align:center; border: ${cert_background ? "none" : "5px solid #787878"
}; position: relative"><br><br>
       <img style="height: 100px" src="${Group1}" /><br><br>
   <span style="font-size:50px; font-weight:bold">${cert_title}</span>
   <br><br>
   <span style="font-size:35px">${cert_subtitle}</span>
   <br><br>
   <span style="font-size:83px"><b>${nama}</b></span><br/><br/><br>
   <span style="font-size:35px">${cert_description}</span> <br/><br/>
   <span style="font-size:45px"><b>${cert_topic}</b></span> <br/><br/><br>
   <span style="font-size:25px">${tanggal}</span><br><br>
       <div style="display:table;width:100%">
   ${signData}
       </div>
</div>
</div>
${bodyNewPage != "" ? 
`<div style="width:${cert_orientation === "landscape" ? "1675px" : "1160px"
}; height:${cert_orientation === "landscape" ? "1160px" : "1675px"
}; margin-top:40px;padding:20px; text-align:center; border: ${cert_background ? "none" : "10px solid #787878"
}; font-size:25px; position: relative">
<img src="${cert_background}" style="width: 100%; height: 100%; position: absolute; top: 0px; left: 0px; z-index: 0" />
<div style="width:${cert_orientation === "landscape" ? "1625px" : "1110px"
}; height:${cert_orientation === "landscape" ? "1110px" : "1475px"
}; padding:20px; ${cert_orientation === "portrait" && "padding-top:150px;"
} border: ${cert_background ? "none" : "5px solid #787878"
}; position: relative"><br><br>
  ${bodyNewPage}
</div>
</div>` : ""}
`;
};
let options = {
  width: "1755px",
  height: "1240px",
};
let optionsPortrait = {
  width: "1240px",
  height: "1755px",
};

exports.certificate = (req, res) => {
  uploadSignature(req, res, async err => {
    let signData = JSON.parse(req.body.sign);
    JSON.parse(req.body.data).map(async (elem, idx) => {
      let dataResult = await db.query(`
            SELECT * FROM training_exam_result
            WHERE id='${elem.result_id}'
            LIMIT 1
            `);
      let logo = await db.query(
        `SELECT logo FROM company WHERE company_id='${req.body.company_id}'`
      );
      let randomStr = Math.random().toString(36).substring(7);
      pdf
        .create(
          html(
            elem.nama,
            req.body.cert_title,
            moment(new Date(dataResult[0].created_at)).format("dddd, MMMM Do YYYY"),
            req.files.signature,
            signData,
            req.files.cert_logo ? req.files.cert_logo[0].path : logo[0].logo,
            req.body.cert_subtitle,
            req.body.cert_description,
            elem.cert_topic,
            req.body.certificate_background,
            req.body.certificate_orientation,
            req.body.bodyNewPage
          ),
          req.body.certificate_orientation === "landscape"
            ? options
            : optionsPortrait
        )
        .toFile(
          `./public/training/certificate/${elem.nama + "-" + randomStr}.pdf`,
          function (err, res) {
            if (err) return console.log(err, "HTML-PDF");

            let obj_mail = {
              company_id: req.body.company_id,
              training_exam_result: elem.result_id,
              user_id: dataResult[0].training_user_id,
              type: "sendmail_certificate_training",
              filepath_cert_user: `${env.APP_URL}/training/certificate/${elem.nama + "-" + randomStr
                }.pdf`,
              from: env.MAIL_USER,
              to: elem.email,
              subject: "Training Certificate : " + elem.cert_topic,
              text: "Training Certificate : " + elem.cert_topic,
              attachments: [
                {
                  filename: `Sertifikat-${elem.cert_topic}.pdf`,
                  path: res.filename,
                  contentType: "application/pdf",
                },
              ],
              html: `
					<b>Hello ${elem.nama},</b><br><br>
					Congratulations ! You get a training certificate <b>${elem.cert_topic}</b><br>
					You can download the certificate we attached.`,
            };
            sendMailQueue.add(obj_mail, {
              ...options_queue,
              delay: options_queue.delay * idx,
            });

            // transporter.sendMail().then(response => {
            //     let success = null;
            //     if (!response.messageId) {
            //         // failed send email
            //         success = 'Failed';
            //     } else {
            //         // success send email
            //         success = 'Sent';
            //     };

            //     db.query(
            //         `UPDATE training_exam_result
            //             SET certificate_status = '${success}',
            //             certificate = '${env.APP_URL}/training/certificate/${elem.nama + '-' + randomStr}.pdf'
            //         WHERE id='${elem.result_id}';`, (error, result) => {
            //         if (error) {
            //             console.log(error.message)
            //         }
            //     }
            //     )
            // });
          }
        );
    });
    await Promise.all(
      JSON.parse(req.body.data).map(item => {
        db.query(
          `UPDATE training_exam_result
                    SET certificate_status = 'Processing'
                WHERE id='${item.result_id}';`,
          (error, result) => {
            if (error) {
              console.log(error.message);
            }
          }
        );
      })
    );
    return res.json({ error: false, result: "success" });
  });
};

exports.updateAnswerExam = async (req, res) => {

  let check = await db.query(`
  select * from temporary_exam_result where 
  training_assignee_id = '${req.body.training_assignee_id}' limit 1;
  select time_limit from training_exam where id = '${req.body.training_exam_id}';`);
  
  let dateNow = moment(new Date());
  let startTimes = moment(check[0][0].submission_start);
  let getMinutes = 0;
  let time_limit = check[1][0].time_limit;

  getMinutes = moment.duration(dateNow.diff(startTimes)).asMinutes();

  if (getMinutes > time_limit) {
    db.query(`delete from  temporary_exam_result where training_assignee_id=?;`,[req.body.training_assignee_id]);
    res.json({ error: true, result: "session time is expired" })
  } else {

    db.query(`
    update temporary_exam_result 
    set answer = ? 
    where 
    training_assignee_id = ?`,[JSON.stringify(req.body.questionAnswer),req.body.training_assignee_id]);

    res.json({ error: false, result: "success" })
  }
}

exports.readExamByCourse = async (req, res) => {
  try {
    const dtoDetailReadExamByCourse = {
      idCourse: req.params.idCourse,
      idCompany: req.params.idCompany,
    };

    //Validate Request
    const retrieveValidRequest =
      await validatorRequest.retrieveRequestExamByCourse(
        dtoDetailReadExamByCourse
      );
    const retrieveData = await moduleDB.retrieveRepoExamByCourse(
      retrieveValidRequest
    );

    res.json({ error: false, result: retrieveData });
  } catch (error) {
    res.json({ error: true, result: error.message });
  }
};

async function createMembershipFunction(dataDetail) {
  // dataDetail.assignee_id itu exam result id
  const format = await moduleDB.getFormatExamResultByAssigneeId(dataDetail);
  const result = await moduleDB.getTrainingExamResultByAssigneeId(dataDetail);
  if (result[0].pass === 1) {
    let now = new Date();
    let licenseFormat = format[0].format
      ? format[0].format
      : "[YYYY][MM][DD].A0[GENDER]-[NUMBER]";
    const dataDetailPassGender = {
      gender: result[0].gender,
      idCompany: result[0].company_id,
    };
    const totalPassByGender = await moduleDB.getTotalPassByGender(
      dataDetailPassGender
    );

    let increment = totalPassByGender[0].number;
    //format
    let YYYY = moment(new Date(result[0].created_at)).format("YYYY");
    let MM = moment(new Date(result[0].created_at)).format("MM");
    let DD = moment(new Date(result[0].created_at)).format("DD");
    let GENDER = result[0].gender === "Male" ? "1" : "2";
    let NUMBER = increment.toString().padStart(9, "0");

    let parseFormat = licenseFormat.replace(/\[/g, "${").replace(/\]/g, "}");
    let license_number = eval("`" + parseFormat + "`");

    let form = [
      (training_user_id = dataDetail.idTrainingUser),
      (expired = moment(
        new Date(now.setFullYear(now.getFullYear() + 2))
      ).format("YYYY-MM-DD")),
      (photo = ""),
      (license_card = ""),
      (license_number = license_number),
      (status = "Active"),
      (created_at = new Date()),
    ];
    const dataDetailTrainingMembership = {
      idTrainingUser: dataDetail.idTrainingUser,
    };
    const check = await moduleDB.getTrainingMembership(
      dataDetailTrainingMembership
    );
    if (check.length) {
      //New Formula For Expired Time
      // From Today + 2 Years extended
      // To: Expired Time + 2 Years Extended
      const expTime = new Date(check[0].expired);
      const expiredNewTimeRule = moment(
        new Date(expTime.setFullYear(expTime.getFullYear() + 2))
      ).format("YYYY-MM-DD");

      if (license_number.length === check[0].license_number.length) {
        const dataDetailQueryExtend = {
          expired: expiredNewTimeRule,
          status: form[5],
          idMembership: check[0].id,
        };

        await moduleDB.extendMembership(dataDetailQueryExtend);
      } else {
        const dataDetailQueryReplace = {
          licenseNumber: license_number,
          expired: expiredNewTimeRule,
          status: form[5],
          idMembership: check[0].id,
        };

        await moduleDB.replaceMembership(dataDetailQueryReplace);
      }

      let licenseWillSave =
        license_number.length === check[0].license_number.length
          ? check[0].license_number
          : license_number;

      const dataDetailTrainingUser = {
        licenseNumber: licenseWillSave,
        idTrainingUser: form[0],
      };

      await moduleDB.updateTrainingUser(dataDetailTrainingUser);
      return true;
    } else {
      const dataDetailTrainingMembership = {
        data: form,
      };

      await moduleDB.insertTrainingMembership(dataDetailTrainingMembership);

      const dataDetailTrainingUser = {
        licenseNumber: license_number,
        idTrainingUser: form[0],
      };

      await moduleDB.updateTrainingUser(dataDetailTrainingUser);
      return true;
    }
  } else {
    return false;
  }
}
/**
 * getAgendaCourse
 *
 * @param {number} id = course_id (mandatory)
 * @returns {object}
 *
 */
function getAgendaCourse(id) {
  return new Promise(async resolve => {
    try {
      // liveclass , materi , exam
      let queryString = `
                SELECT a.id ,'1' as 'type','Liveclass' as 'label',a.judul as 'title',
                a.start_time as 'start_time',a.end_time AS 'end_time',NULL AS content 
                FROM training_course tc 
                LEFT JOIN webinars a ON a.training_course_id = tc.id 
                WHERE tc.id = '${id}' and a.id IS NOT NULL
                UNION 

                SELECT b.id,'2' as 'type','Exam' as 'label',b.title as 'title',
                b.start_time as 'start_time',b.end_time as 'end_time',NULL AS content 
                FROM training_course tc 
                LEFT JOIN training_exam b ON b.course_id = tc.id 
                WHERE tc.id = '${id}' and b.id IS NOT NULL
                UNION 

                SELECT c.id ,'3' as 'type','Theory' as 'label',c.title as 'title',
                null as 'start_time',NULL AS 'end_time',NULL AS content 
                FROM training_course tc 
                LEFT JOIN training_course_session c ON c.course_id = tc.id 
                WHERE tc.id = '${id}' and c.id IS NOT NULL

                UNION
                SELECT c.id ,'4' as 'type','Submission' as 'label',c.title,
                c.start_time,c.end_time,content  
                FROM training_course tc 
                LEFT JOIN training_course_submission c ON c.course_id = tc.id 
                WHERE tc.id = '${id}' and c.id IS NOT NULL;

                SELECT ac.course_id , ac.agenda from agenda_course ac 
                where ac.course_id = '${id}';
            `;

      let resultData = await db.query(queryString);
      let agendaData = resultData[1];
      let submission = resultData[0];

      // console.log(submission, 8887);

      if (agendaData.length) {
        agendaData = JSON.parse(agendaData[0].agenda.toString());
      }
      let tmp = [];
      agendaData.forEach((str, i) => {
        if (str.id) {
          tmp.push(str);
        }
      });
      agendaData = tmp;

      agendaData.forEach((str, i) => {
        let idx = resultData[0].findIndex(str1 => {
          return str.id == str1.id && str.type == str1.type;
        });
        if (idx > -1) {
          if (agendaData[i].type === "1") {
            resultData[0][idx].is_mandatory =
              agendaData[i].is_mandatory == 1 ? true : false;
          }
          agendaData[i] = resultData[0][idx];
        }
      });

      // console.log(agendaData, 999877);
      return resolve(agendaData);
    } catch (e) {
      console.error({
        error: true,
        data: e,
        function: "read",
        filename: __filename,
      });
      return resolve(null);
    }
  });
}
