var env = require('../env.json');
var conf = require('../configs/config');
var db = require('../configs/database');
var moment = require('moment');
var momenttz = require('moment-timezone');

exports.getByProject = (req, res) => {
	let projectId = req.params.projectId;
	let visibility = req.params.visibility === 'public' ? 'Public' : req.params.visibility === 'private' ? 'Private' : 'Public'+"','"+'Private';

    let sql = `SELECT t.*, GROUP_CONCAT(u.user_id) AS owner_id, (SELECT time FROM gantt_logs l WHERE l.task_id=t.id AND l.description LIKE '%to Done%' ORDER BY l.time DESC LIMIT 1) AS done_time FROM gantt_tasks t LEFT JOIN gantt_user u ON u.task_id=t.id LEFT JOIN user us ON u.user_id=us.user_id WHERE t.project_id IN (${projectId}) AND (t.visibility IN ('${visibility}'${visibility !== 'Private' ? ",'','NULL'" : ''}) ${visibility !== 'Private' ? 'OR t.visibility IS NULL' : ''}) GROUP BY t.id ORDER BY t.start_date ASC`;
    db.query(sql, async (error, result) => {
        if(error) res.json({ error: true, result: error });
        result.map( async (item,index)=>{
            result[index].start_date = moment(new Date(item.start_date)).format('DD-MM-YYYY HH:mm')
            result[index].end_date = moment(new Date(item.end_date)).format('DD-MM-YYYY HH:mm')
            if(item.owner_id != null) {
                let assigned_user = await db.query(`SELECT u.user_id, u.name, u.avatar FROM user u WHERE u.user_id IN (${item.owner_id})`);
                result[index].assigned_user = assigned_user;
            }else{
                result[index].assigned_user = [];
            }
        })
        let sql = `SELECT * FROM gantt_links WHERE project_id IN (${projectId})`;
        db.query(sql, async (error, resultLink) => {
            if(error) res.json({ error: true, result: error });
            
        let progress = await db.query(`SELECT
        ROUND((SELECT SUM(progress) FROM gantt_tasks WHERE project_id='${projectId}' AND (visibility IN ('${visibility}'${visibility !== 'Private' ? ",'','NULL'" : ''}) ${visibility !== 'Private' ? 'OR visibility IS NULL' : ''}) AND type!='project') /
        (SELECT COUNT(id) FROM gantt_tasks WHERE project_id='${projectId}' AND (visibility IN ('${visibility}'${visibility !== 'Private' ? ",'','NULL'" : ''}) ${visibility !== 'Private' ? 'OR visibility IS NULL' : ''}) AND type!='project') * 100, 2)
        AS progress`)
            res.json({ tasks : result, links: resultLink, progress: progress[0].progress });
        })
	})
}

exports.getByUser = (req, res) => {
	let userId = req.params.userId;
	let projectId = req.params.projectId > 0 ? `AND t.project_id=${req.params.projectId}` : '';
	let visibility = req.params.visibility === 'public' ? 'Public' : req.params.visibility === 'private' ? 'Private' : ['Public', 'Private'];

	let sql = `SELECT t.*, GROUP_CONCAT(u.user_id) AS owner_id, (SELECT time FROM gantt_logs l WHERE l.task_id=t.id AND l.description LIKE '%to Done%' ORDER BY l.time DESC LIMIT 1) AS done_time FROM gantt_tasks t LEFT JOIN gantt_tasks t2 ON t2.parent=t.id LEFT JOIN gantt_user u ON u.task_id=t.id LEFT JOIN user us ON u.user_id=us.user_id WHERE u.user_id IN (${userId}) ${projectId} AND (t.visibility IN ('${visibility}'${visibility !== 'Private' ? ",'','NULL'" : ''}) ${visibility !== 'Private' ? 'OR t.visibility IS NULL' : ''}) GROUP BY t.id ORDER BY t.start_date ASC`;
    db.query(sql, async (error, result) => {
        if(error) res.json({ error: true, result: error });
        result.map( async (item,index)=>{
            result[index].start_date = moment(new Date(item.start_date)).format('DD-MM-YYYY HH:mm')
            result[index].end_date = moment(new Date(item.end_date)).format('DD-MM-YYYY HH:mm')
            let assigned_user = await db.query(`SELECT u.user_id, u.name, u.avatar FROM user u WHERE u.user_id IN (${item.owner_id})`)
            result[index].assigned_user = assigned_user;
        })
        let sql = `SELECT l.* FROM gantt_links l LEFT JOIN gantt_user u ON u.task_id=l.source LEFT JOIN gantt_user u2 ON u2.task_id=l.target WHERE u.user_id IN (${userId}) AND u2.user_id IN (${userId})`;
        db.query(sql, async (error, resultLink) => {
            if(error) res.json({ error: true, result: error });
            let progress = await db.query(`SELECT ROUND(
            (SELECT SUM(t.progress) FROM gantt_tasks t JOIN gantt_user u ON t.id=u.task_id WHERE u.user_id='${userId}' ${projectId} AND (t.visibility IN ('${visibility}'${visibility !== 'Private' ? ",'','NULL'" : ''}) ${visibility !== 'Private' ? 'OR t.visibility IS NULL' : ''}) AND t.type!='project') /
            (SELECT COUNT(t.id) FROM gantt_tasks t JOIN gantt_user u ON t.id=u.task_id WHERE u.user_id='${userId}' ${projectId} AND (t.visibility IN ('${visibility}'${visibility !== 'Private' ? ",'','NULL'" : ''}) ${visibility !== 'Private' ? 'OR t.visibility IS NULL' : ''})) * 100
            , 2) AS progress`)
            res.json({ tasks : result, links: resultLink, progress: progress[0].progress });
        })
	})
}


exports.update = (req, res) => {

    let form = req.body;
    let start = moment(new Date(form.start_date.replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"))).format('YYYY-MM-DD HH:mm');
    let end = moment(new Date(form.end_date.replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"))).format('YYYY-MM-DD HH:mm');

	let sqlSelect = `SELECT * FROM gantt_tasks WHERE id='${req.params.id}'`;
	db.query(sqlSelect, (error, resultBefore) => {
        if(error) res.json({ error: true, result: error });

        let sql = `UPDATE gantt_tasks SET text = ?, description = ?, parent = ?, start_date = ?, end_date = ? , duration = ?, company = ?, status = ?, type = ?, progress = ?, visibility = ?, lock_data = ? WHERE id = ?`;
        db.query(sql, [form.text, form.description, form.parent, start, end, form.duration, form.company, form.status, form.type, form.progress, form.visibility, form.lock_data, req.params.id], (error, result) => {
            if(error) res.json({ error: true, result: error });
            
            if (resultBefore[0].status !== form.status){
                let now = momenttz.tz(new Date(), 'Asia/Jakarta').format('YYYY-MM-DD HH:mm:ss');
                let sql = `INSERT INTO gantt_logs (task_id, user_id, description, time) VALUES(?,?,?,?)`;
                db.query(sql, [req.params.id, req.params.userId, `Edited from ${resultBefore[0].status} to ${form.status}`, now], (error, result) => {
                    if(error) res.json({ error: true, result: error });
                })
            }
        
            if (form.owner_id){
                let sql = `DELETE FROM gantt_user WHERE task_id = ?; INSERT INTO gantt_user (task_id, user_id) VALUES(?,?)`;
                db.query(sql, [req.params.id, req.params.id, form.owner_id], (error, result) => {
                    if(error) res.json({ error: true, result: error });
                    res.json({ action: "updated" });
                })
            }
            else{
                res.json({ action: "updated" });
            }
        })

    })
}

exports.insert = (req, res) => {

    let form = req.body;
    let start = moment(new Date(form.start_date.replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"))).format('YYYY-MM-DD HH:mm');
    let end = moment(new Date(form.end_date.replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"))).format('YYYY-MM-DD HH:mm');
	let sql = `INSERT INTO gantt_tasks (project_id, text, description, parent, start_date, end_date, duration, company, status, type, progress, visibility, lock_data) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)`;
	db.query(sql, [req.params.projectId, form.text, form.description, form.parent, start, end, form.duration, form.company, form.status, form.type, form.progress, form.visibility, form.lock_data ? form.lock_data : 'Unlocked'], (error, result) => {
        if(error) res.json({ error: true, result: error });
        
        let now = momenttz.tz(new Date(), 'Asia/Jakarta').format('YYYY-MM-DD HH:mm:ss');
        let sql = `INSERT INTO gantt_logs (task_id, user_id, description, time) VALUES(?,?,?,?)`;
        db.query(sql, [result.insertId, req.params.userId, 'Created', now], (error, result) => {
            if(error) res.json({ error: true, result: error });
        })

        if (form.owner_id){
            let sql = `INSERT INTO gantt_user (task_id, user_id) VALUES(?,?)`;
            db.query(sql, [result.insertId, form.owner_id], (error, result2) => {
                if(error) res.json({ error: true, result: error });
                res.json({ action: "inserted", tid: result.insertId });
            })
        }
        else{
            res.json({ action: "inserted", tid: result.insertId });
        }
	})
}

exports.delete = (req, res) => {

	let sql = `DELETE FROM gantt_tasks WHERE id = ? OR parent = ?; DELETE FROM gantt_user WHERE task_id=?;`;
	db.query(sql, [req.params.id, req.params.id, req.params.id], (error, result) => {
		if(error) res.json({ error: true, result: error });

		res.json({ action: "deleted" });
	})
}

exports.insertLink = (req, res) => {

    let form = req.body;
	let sql = `INSERT INTO gantt_links (project_id, source, target, type) VALUES(?,?,?,?)`;
	db.query(sql, [req.params.projectId, form.source, form.target, form.type], (error, result) => {
		if(error) res.json({ error: true, result: error });

		res.json({ action: "inserted", tid: result.insertId });
	})
}

exports.updateLink = (req, res) => {

    let form = req.body;
	let sql = `UPDATE gantt_links SET source = ?, target = ?, type = ? WHERE id = ?`;
	db.query(sql, [form.source, form.target, form.type, req.params.id], (error, result) => {
		if(error) res.json({ error: true, result: error });

		res.json({ action: "updated" });
	})
}

exports.deleteLink = (req, res) => {

	let sql = `DELETE FROM gantt_links WHERE id = ?`;
	db.query(sql, [req.params.id], (error, result) => {
		if(error) res.json({ error: true, result: error });

		res.json({ action: "deleted" });
	})
}

exports.getHistory = (req, res) => {
    let sql = `SELECT u.name, l.* FROM gantt_logs l JOIN user u ON u.user_id=l.user_id WHERE l.task_id = ? ORDER BY l.time ASC`;
    db.query(sql, [req.params.id], (error, result) => {
        if (error) res.json({ error: true, result: error });

        res.json({ error: false, result: result})
    })
}