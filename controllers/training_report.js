const moduleDB = require('../repository');
const moment = require('moment');
const validatorRequest = require('../validator');
const db = require('../configs/database');
const { readAnswerByResult } = require('./training_exam');
const axios = require("axios");


async function getTotalUserFromBegining(company_id){
    return new Promise(async(res)=>{
  
      let listExam = [];
      let listCompanyTraining = [];
      let total = 0;
      let lastUpdated = '--';
      try {
        
        if(!company_id) return res([]);
    
        let sql =`
          select ac.agenda 
          from training_course tc
          LEFT JOIN agenda_course ac ON ac.course_id = tc.id
          WHERE 
          lower(tc.status) = 'active' AND tc.company_id = '${company_id}' AND ac.agenda IS NOT NULL AND ac.agenda LIKE '%"type":"2"%';
          
          SELECT 
          te.id
          FROM training_exam te 
          WHERE te.company_id = '${company_id}' AND lower(te.status) = 'active';
    
          SELECT
          distinct 
          tc.id
          FROM training_company tc
          LEFT JOIN training_user tu ON tu.training_company_id = tc.id
          where
          tc.company_id = '${company_id}' ORDER BY tc.id DESC;
        `;
    
        let sqlExam = `
          SELECT 
          COUNT(ter.id) as total, MAX(ter.created_at) as lastUpdated
          FROM training_exam te
          left join training_exam_assignee tea on tea.exam_id = te.id
          left join training_exam_result ter on ter.assignee_id = tea.id
          left join training_user tu on tu.id = tea.training_user_id and tu.training_company_id in(#COMPANY)
          WHERE te.id IN(#EXAM) and tu.training_company_id IS NOT NULL
          ORDER BY tea.training_user_id DESC;
        `;
        sql = await db.query(sql);
        if(sql.length){
          // agenda get exam only
          sql.forEach((str, index)=>{
            if(index == 0){
              str[index].agenda = JSON.parse(str[index].agenda);
              str[index].agenda.forEach((item)=>{
                if(item.type == 2) listExam.push(item.id);
              });
            }
            else if(index == sql.length-1){
              str.forEach((item)=>{
                listCompanyTraining.push(item.id)
              });
            }
            else{
              str.forEach((item)=>{
                listExam.push(item.id);
              });
            }
          });
    
          listExam = [...new Set(listExam)];
    
          if(listExam.length && listCompanyTraining.length){
            sqlExam = sqlExam.replace("#COMPANY", listCompanyTraining.toString());
            sqlExam = sqlExam.replace("#EXAM", listExam.toString());
    
            let result = await db.query(sqlExam); // arr_company, arr_exam
            lastUpdated = result[0].lastUpdated;
            total = result[0].total;
          }
        }
      } catch (error) {
        console.log(error);
      }
      res({total, lastUpdated});
    });
  }

async function getExamOverviewByCompany(company_id){
  return new Promise(async(hasil)=>{

    let analitic = [];
    let listExam = [];
    try {      
      let getExam = `
        -- assign to course
        select ac.agenda 
        from training_course tc
        LEFT JOIN agenda_course ac ON ac.course_id = tc.id
        WHERE 
        lower(tc.status) = 'active' AND tc.company_id ='${company_id}' AND ac.agenda IS NOT NULL AND ac.agenda LIKE '%"type":"2"%';
        -- unassign to course
        SELECT 
        distinct
        te.id
        FROM training_exam te 
        left join training_exam_assignee tea on tea.exam_id = te.id
        left join training_exam_result ter on ter.assignee_id = tea.id
        left join training_user tu on tu.id = tea.training_user_id
        left join training_company tc on tc.id = tu.training_company_id and tc.company_id = te.company_id
        WHERE 
        te.company_id = '${company_id}' AND lower(te.status) = 'active';
      `;
      // run after get data collection exam
      let getAnalitic = `      
        SELECT
        te.id as 'exam_id', 
        te.title,
        SUM(ter.correct) AS 'correct', 
        SUM(ter.incorrect) AS 'incorrect',
        SUM(IF(ter.submission_condition='Normal',1,0)) AS 'submission_condition_normal', 
        SUM(IF(ter.submission_condition='Timeout',1,0)) AS 'submission_condition_timeout',
        SUM(IF(ter.submission_condition='Exit Apps',1,0)) AS 'submission_condition_exit'
        
        FROM training_exam te
        left join training_exam_assignee tea on tea.exam_id = te.id
        left join training_exam_result ter on ter.assignee_id = tea.id
        LEFT JOIN training_exam_record ter2 ON ter2.exam_result_id = ter.id
        left join training_user tu on tu.id = tea.training_user_id
        LEFT JOIN training_company tc ON tc.id = tu.training_company_id AND tc.company_id=te.company_id
        WHERE 
        te.company_id = '${company_id}' 
        AND te.id IN(?) 
        AND tu.training_company_id IS NOT NULL AND tc.company_id IS NOT null
        AND ter2.id IS NOT NULL
        AND tea.id IS NOT NULL AND ter.training_user_id IS NOT NULL 
        GROUP BY te.id
        ORDER BY te.title ASC;
      `; 
      let resultGetExam = await db.query(getExam);
      let agenda = resultGetExam[0];
      let examUnassign = resultGetExam[1];
      
      if(agenda.length){
        agenda.forEach((str)=>{
          str.agenda = JSON.parse(str.agenda);
          str.agenda.forEach(({type, id})=>{
            if(parseInt(type) == 2){
              //console.log(id)
              let exist = listExam.findIndex((findstr)=>{ return findstr ==  id});
              if(exist == -1) listExam.push(id);
            }
          });
        });
      }

      if(examUnassign.length){
        examUnassign.forEach(({id})=>{
          let exist = listExam.findIndex((findstr)=>{ return findstr ==  id});
          if(exist == -1) listExam.push(id);
        })
      }

      if(listExam.length){
        analitic = await db.query(getAnalitic,[listExam]);
        
        if(analitic.length){
          analitic.forEach((str)=>{
            let total = 100 / (str.correct+str.incorrect);
            str.correctPercent = Math.round(total*str.correct);
            str.incorrectPercent = Math.round(total*str.incorrect); 
          });
        }
      }
      
    } catch (error) {
      console.log(error);
    }

    return hasil(analitic);
  })
}

async function postSelectedExamOverview(company_id, { examId, startDate, endDate,bySubmissionCondition }){
  return new Promise(async(hasil)=>{

    let listAssigneeResult = [];
    let resultGetExam = [];
    let title = '';
    try {      
      // run after get data collection exam
      let byDate = '';
      if(startDate && endDate){
        byDate += ` AND date(created_at) BETWEEN  date('${startDate}') AND date('${endDate}') `;
      }

      let querySubmissionCondition = '';
      if(bySubmissionCondition){
        querySubmissionCondition += ` AND ter.submission_condition = '${bySubmissionCondition}' `;
      }
      let getRecord = `      
        SELECT
        ter.exam_id,te.title,
        tea.training_user_id,tu.name,tu.email,tu.training_company_id,tc.company_id,
        ter.id as 'result_id', ter.assignee_id,ter2.exam_answer_id,ter.submission_condition,ter.created_at 
        FROM training_exam te
        left join training_exam_assignee tea on tea.exam_id = te.id
        left join training_exam_result ter on ter.assignee_id = tea.id
        LEFT JOIN training_exam_record ter2 ON ter2.exam_result_id = ter.id
        left join training_user tu on tu.id = tea.training_user_id
        LEFT JOIN training_company tc ON tc.id = tu.training_company_id AND tc.company_id=te.company_id
        WHERE 
        te.company_id = '${company_id}' 
        AND te.id IN(?) 
        AND tu.training_company_id IS NOT NULL AND tc.company_id IS NOT null
        AND ter2.id IS NOT NULL 
        AND tea.id IS NOT NULL AND ter.training_user_id IS NOT NULL 
        ${byDate} 
        ${querySubmissionCondition}
        ORDER BY tea.exam_id DESC;
      `;

      resultGetExam = await db.query(getRecord,[examId]);
      title = resultGetExam[0].title;

      resultGetExam.forEach(({ result_id, assignee_id,training_user_id,name,email,training_company_id,company_id,submission_condition,created_at })=>{ 
        listAssigneeResult.push({ result_id, assignee_id,training_user_id,name,email,training_company_id,company_id,submission_condition,created_at}) 
      });

    } catch (error) {
      console.log(error);
    }

    return hasil({resultGetExam, listAssigneeResult, title});
  })
}

exports.postSelectedExamOverview = async(req,res)=>{
  
  let listQuestion = []; // { id, question, answer, correct, incorrect, user_answer:{ correct,incorrect } }
  if(req.params.company_id && typeof req.body.examId == 'number'){
    
    let {title, listAssigneeResult} = await postSelectedExamOverview(req.params.company_id, req.body);
    
    if(listAssigneeResult.length){
      
      let startDate = moment(listAssigneeResult[0].created_at).format("DD MMM YYYY");
      let endDate = null;
      let now = moment(new Date()).format("DD MMM YYYY");
      let totalParticipant = [];

      for (let index = 0; index < listAssigneeResult.length; index++) { // result

        const element = listAssigneeResult[index];
        let params = {result_id: element.result_id,inner:true};

        let indexParticipant = totalParticipant.findIndex((items)=> items.training_user_id == element.training_user_id);
        if(indexParticipant == -1) totalParticipant.push({ training_user_id: element.training_user_id, name: element.name });

        if(index == listAssigneeResult.length-1){
          let end = moment(element.created_at).format("DD MMM YYYY")
          if(now == end){
            endDate = 'Still Ongoing';
          }else{
            endDate = end;
          }
        }


        let read = await readAnswerByResult({params, query: {by:'result'} }); // result detail
        
        if(read.error == false && read.result.length){
          
          for(let i=0; i < read.result.length; i++){

            // group question by result user
            let dataResult = read.result[i];
            // remove tag html
            dataResult.option.forEach((str)=>{
              str.option_text = str.option_text.replace( /(<([^>]+)>)/ig, '');
            });

            let getOp = dataResult.option.findIndex((str)=>{ return str.option_label ==  dataResult.answer});
            let getOpUser = dataResult.option.findIndex((str)=>{ return str.option_label ==  dataResult.user_answer});
            let exist = listQuestion.findIndex((str)=>{return str.id == dataResult.id});
            
            if(exist == -1){

              let dataQuestion = {
                id: dataResult.id,
                question: dataResult.question,
                optionQuestion: dataResult.option,
                answerQuestion:dataResult.option[getOp],
                isShortedAnswer:dataResult.isShortedAnswer,
                correctPercent:0,
                incorrectPercent:0,
                correct:0,
                incorrect:0,
                mostAnswerUser:{ A:0,B:0,C:0,D:0,E:0 },
                mostAnswerUserObject:null,
                tag:dataResult.tag ? dataResult.tag : null,
                submission_condition:{ Normal:0,Timeout:0,"Exit Exam":0,"Exit Apps":0,count:0 },
                totalView:1,
                data:null,
                listUserAnswerCorrect:[],
                listUserAnswerIncorrect:[]
              };

              let mostAnswerUserObject = [];
              dataResult.option.forEach((str)=>{
                str.count = 0;
                str.countPercent = 0;
                mostAnswerUserObject.push(str);
              });

              dataQuestion.mostAnswerUserObject = mostAnswerUserObject

              if(dataResult.user_answer != ''){
                if(Object.keys(dataQuestion.mostAnswerUser).indexOf(dataResult.user_answer) > -1){
                  dataQuestion.mostAnswerUser[dataResult.user_answer]++;

                  let check = dataQuestion.mostAnswerUserObject.findIndex((items)=> items.option_label == dataResult.user_answer);
                  if(check > -1) dataQuestion.mostAnswerUserObject[check].count++;
                }

                if(Object.keys(dataQuestion.submission_condition).indexOf(element.submission_condition) > -1){
                  dataQuestion.submission_condition[element.submission_condition]++;
                  dataQuestion.submission_condition.count++;
                }
  
                if(dataResult.user_answer.toLowerCase() == dataResult.option[getOp].option_label.toLowerCase()){
                  dataQuestion.correct++;
                  dataQuestion.listUserAnswerCorrect.push({ ...element,user_answer:dataResult.option[getOpUser], isCorrect: 1 });
                }else{
                  dataQuestion.incorrect++;
                  dataQuestion.listUserAnswerIncorrect.push({ ...element,user_answer:dataResult.option[getOpUser], isCorrect: 0 });
                }
  
                listQuestion.push(dataQuestion);
              }

            }else{
              if(dataResult.user_answer != ''){
                listQuestion[exist].totalView++;
                if(Object.keys(listQuestion[exist].mostAnswerUser).indexOf(dataResult.user_answer) > -1){
                  listQuestion[exist].mostAnswerUser[dataResult.user_answer]++;
                 
                  let check = listQuestion[exist].mostAnswerUserObject.findIndex((items)=> items.option_label == dataResult.user_answer);
                  if(check > -1) listQuestion[exist].mostAnswerUserObject[check].count++;
                }

                if(Object.keys(listQuestion[exist].submission_condition).indexOf(element.submission_condition) > -1){
                  listQuestion[exist].submission_condition[element.submission_condition]++
                  listQuestion[exist].submission_condition.count++;
                }
  
                if(dataResult.user_answer.toLowerCase() == listQuestion[exist].answerQuestion.option_label.toLowerCase()){
                  listQuestion[exist].correct++;
                  listQuestion[exist].listUserAnswerCorrect.push({ ...element,user_answer:dataResult.option[getOpUser], isCorrect: 1 });
                }else{
                  listQuestion[exist].incorrect++;
                  listQuestion[exist].listUserAnswerIncorrect.push({ ...element,user_answer:dataResult.option[getOpUser], isCorrect: 0 });
                }

                endDate = moment(element.created_at).format("DD MMM YYYY") == now ? 'Still Ongoing' : moment(element.created_at).format("DD MMM YYYY");
              }
            }
          }
        }
      }

      let group = {
        examId : req.body.examId,
        examTitle: title,
        submission_condition:{ Normal:0,Timeout:0,"Exit Exam":0,"Exit Apps":0,count:0 }, // per exam
        startDate,
        endDate,
        totalParticipant,
        incorrect:{
          "all":[], // for 100% false
          "mid":[], // for >50% false
          "low":[] // for <50% false
        },
        correct:{
          "all":[], // for 100% true
          "mid":[], // for >50% true
          "low":[] // for <50% true
        }
      };

      if(listQuestion.length){

        listQuestion.forEach((str)=>{

          let total = 100 / (str.correct+str.incorrect);
          str.correctPercent = Math.round(total*str.correct) > 100 ? 100 : Math.round(total*str.correct);
          str.incorrectPercent = Math.round(total*str.incorrect) > 100 ? 100 : Math.round(total*str.incorrect);

          let entries = Object.entries(str.mostAnswerUser);
          entries.sort((a,b)=> b[1]-a[1]);
          str.mostAnswerUser = entries[0][0];

          let totalmost = 0;
          str.mostAnswerUserObject.forEach((items)=>{
            if(items.option_label == str.mostAnswerUser){
              str.mostAnswerUser += `. ${items.option_text}`;
            }
            if(items.count > -1){
              totalmost += items.count;
            }else{
              items.count = 0;
            }
          });

          totalmost = 100/totalmost; 
          str.mostAnswerUserObject.forEach((items)=>{
            if(items.countPercent > -1){
              items.countPercent = (totalmost*items.count).toFixed(2);
            }else{
              items.count = 0;
            }
          });

          let keysSubmission = Object.keys(str.submission_condition);
          let percentSubmission = 100 / str.submission_condition.count;
          let submission = {};
          for(let i=0; i < keysSubmission.length-1; i++){
            
            if(req.query.grouping){
              group.submission_condition[keysSubmission[i]] += str.submission_condition[keysSubmission[i]]
              group.submission_condition.count += str.submission_condition[keysSubmission[i]]
            }

            str.submission_condition[keysSubmission[i]] = percentSubmission * str.submission_condition[keysSubmission[i]];
            submission[keysSubmission[i]] = str.submission_condition[keysSubmission[i]]; 
          }
          str.submission_condition = submission;

          if(req.query.grouping){

            if(str.incorrectPercent >= 100){
              group.incorrect.all.push(str);
            }else if(str.incorrectPercent >= 50){
              group.incorrect.mid.push(str);
            }else if(str.incorrectPercent < 50){
              group.incorrect.low.push(str);
            }

            if(str.correctPercent >= 100){
              group.correct.all.push(str);
            }else if(str.correctPercent >= 50){
              group.correct.mid.push(str);
            }else if(str.correctPercent < 50){
              group.correct.low.push(str);
            }
          }
        });
        
        if(req.query.grouping){
          let keysSubmission = Object.keys(group.submission_condition);
          let percentSubmission = 100 / group.submission_condition.count;
          for(let i=0; i < keysSubmission.length-1; i++){
            group.submission_condition[keysSubmission[i]] = percentSubmission * group.submission_condition[keysSubmission[i]];
          }
          delete group.submission_condition.count;
          listQuestion = group;
        }else{
          listQuestion = { header: { examId: group.examId, examTitle: group.examTitle, startDate, endDate, totalParticipant },  detail: listQuestion }
          if(req.body.questionId){
            listQuestion.detail = listQuestion.detail.filter(str=> str.id == req.body.questionId);
          }
        }
      }

    }

    return res.json({ error: false, result:listQuestion })
  }
  return res.json({ error : false, result:listQuestion });
}

exports.getExamOverviewByCompany = async(req,res)=>{
  let result = [];
  if(req.params.company_id){
    result = await getExamOverviewByCompany(req.params.company_id)
  }
  return res.json({ error : false, result });
}

exports.detailReportTraining = async (req, res) => {
	try {
		const dtoDetailReportTraining = {
			idCompany: req.body.company_id,
            grupName: req.body.grup_name,
            start: req.body.start,
            end: req.body.end,
            licenses_type: req.body.licenses_type,
            company: req.body.company,
            training_company: req.body.training_company,
            pass: req.body.pass,
            cert: req.body.cert,
		};

        // get lastUpdated & total user
        let {total, lastUpdated} = await getTotalUserFromBegining(req.body.company);

        let response = {
            finalTotalUserTraining: null,
            finalTotalExam: null,
            finalTotalUserPassedExam: null,
            finalTotalAvgScore: null,
            finalTotalAvgDuration: null,
            lastUpdated: '--'
        };

        let totalUserPassedExam = 0;

        let totalUserDoExam = 0;
        let totalUserScoreDoExam = 0;
        let totalTimeInSecondsUserDoExam = 0;

        //Validate Request
		const retrieveValidRequest = await validatorRequest.validateDetailReportRequest(dtoDetailReportTraining);

        //Get Id Training Company
		let retrieveListTrainingCompany = await moduleDB.retrieveIdTrainingCompany(retrieveValidRequest);
        
        const dtoDataTrainingUser = {
            email: req.app.token.email
        };
        const retrieveDataAdminTrainingUser = await moduleDB.retrieveDataTrainingUserByEmail(dtoDataTrainingUser);

        //Logic if Admin Training
        if(dtoDetailReportTraining.grupName == "Admin Training"){

            if(retrieveListTrainingCompany.length) {
                if(retrieveDataAdminTrainingUser.length){
                    retrieveListTrainingCompany = retrieveListTrainingCompany.filter(dataTraining => dataTraining.id === retrieveDataAdminTrainingUser[0].training_company_id);
                }
            }
        }

        if(retrieveListTrainingCompany.length){

            // for(let i=0;i<retrieveListTrainingCompany.length;i++){
            //     const dtoRequestListUserTraining = {
            //         idTrainingCompany: retrieveListTrainingCompany[i].id
            //     };
            //     //Get Data Total Training User
            //     const retrieveTotalDataTrainingUser = await moduleDB.retrieveTotalTrainingUser(dtoRequestListUserTraining);
            //     response.finalTotalUserTraining += retrieveTotalDataTrainingUser[0].totalTrainingUser;
            // }

            //Get Total Done Exam, User Pass
            if(dtoDetailReportTraining.grupName == "Admin Training"){
                //Get Data Exam Training Company if Admin Training
                const dtoDetailTotalExamTraining = {
                    idTrainingCompany: retrieveListTrainingCompany[0].id,
                    start: req.body.start,
                    end: req.body.end,
                    licenses_type: req.body.licenses_type,
                    company: req.body.company,
                    training_company: req.body.training_company,
                    pass: req.body.pass,
                    cert: req.body.cert,
                }
                const retrieveTotalExamTrainingCompany = await moduleDB.retrieveTotalExamTrainingCompany(dtoDetailTotalExamTraining);
                const retrieveTotalPassedExamTrainingCompany = await moduleDB.retrieveUserPassedByIdTrainingCompany(dtoDetailTotalExamTraining);
                const retrieveAverageTime = await moduleDB.retrieveAverageTimeExamTraining(dtoDetailTotalExamTraining);

                response.finalTotalUserTraining = retrieveTotalExamTrainingCompany.totalUser;
                response.finalTotalExam = retrieveTotalExamTrainingCompany.totalExamCompany;
                response.finalTotalUserPassedExam = retrieveTotalPassedExamTrainingCompany[0].TotalPassedExam;
                response.finalTotalAvgScore = (retrieveTotalExamTrainingCompany.totalScore / retrieveTotalExamTrainingCompany.totalUserDoExam  || 0).toFixed(2);
                response.finalTotalAvgDuration = retrieveAverageTime[0].AverageTime;
                response.lastUpdated = retrieveTotalExamTrainingCompany.lastUpdated;


            }else{
                //Get Data Exam Company
                const retrieveTotalExamCompany = await moduleDB.retrieveTotalExamCompany(dtoDetailReportTraining);
                const retrieveDataExamCompany = await moduleDB.retrieveUserPassedByIdCompany(dtoDetailReportTraining);
                const retrieveAverageTime = await moduleDB.retrieveAverageTimeExamCompany(dtoDetailReportTraining);
                response.finalTotalUserTraining = retrieveTotalExamCompany.totalUser;
                response.finalTotalExam = retrieveTotalExamCompany.totalExamCompany;
                response.finalTotalUserPassedExam = retrieveDataExamCompany[0].TotalPassedExam;
                response.finalTotalAvgScore = (retrieveTotalExamCompany.totalScore / retrieveTotalExamCompany.totalUserDoExam  || 0).toFixed(2);
                response.finalTotalAvgDuration = retrieveAverageTime[0].AverageTime;
                response.lastUpdated = retrieveTotalExamCompany.lastUpdated;
            }

        };
        
        //if(response.lastUpdated == '--') response.lastUpdated = lastUpdated;
        response.totalUserFromBegining = total;
		res.json({ error: false, result: response})

	} catch (error) {
		res.json({ error: true, result: error.message })
	}
}

exports.detailReportTrainingUser = async (req, res) => {
	try {
		const dtoDetailReportTraining = {
			idTrainingUser: req.query.idTrainingUser,
		};

        //Validate Request
		const retrieveValidRequest = await validatorRequest.validateHistoryUserTrainingExam(dtoDetailReportTraining);
        const retrieveDataReportTrainingUser = await moduleDB.retrieveHistoryUserTrainingExam(retrieveValidRequest);
        
		res.json({ error: false, result: retrieveDataReportTrainingUser})

	} catch (error) {
		res.json({ error: true, result: error.message })
	}
}