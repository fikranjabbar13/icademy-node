var conf = require('../configs/config');
var db = require('../configs/database');

exports.createSemester = (req, res, next) => {
	var formData = {
		company_id: req.body.company_id,
		semester_name: req.body.semester_name
	};
	db.query(`SELECT * FROM semester WHERE semester_name = '${formData.semester_name}' AND company_id='${formData.company_id}'`, (error, result, fields) => {
		if (result.length==1){
			res.json({
				error: false,
				result: 'Double data'
			});
		}
		else{
			// create Semester
			db.query(`INSERT INTO semester
				(semester_id, company_id, semester_name) VALUES (null, '${formData.company_id}', '${formData.semester_name}')`, (error, result, fields) => {
				if(error) {
					res.json({ error: true, result: error });
				} else {
					db.query(`SELECT * FROM semester WHERE semester_id = '${result.insertId}'`, (error, result, fields) => {
						res.json({
							error: false,
							result: result[0]
						});
					})
				}
			});
		}
	})

};

exports.getSemesterList = (req, res, next) => {
	db.query(`SELECT b.*, c.company_name FROM semester b JOIN company c ON c.company_id = b.company_id`, (error, result, fields) => {
		if(error) {
			res.json({error: true, result: error});
		} else {
		  res.json({
		  	error: false,
		  	result: result
		  });
		}
	});
};

exports.getSemesterByCompany = (req, res, next) => {
	db.query(`SELECT b.*, c.company_name FROM semester b JOIN company c ON c.company_id = b.company_id WHERE b.company_id = '${req.params.company_id}'`, (error, result, fields) => {
		if(error) {
			res.json({error: true, result: error});
		} else {
		  res.json({
		  	error: false,
		  	result: result
		  });
		}
	});
}

exports.getSemesterOne = (req, res, next) => {
	db.query(`SELECT b.*, c.company_name FROM semester b JOIN company c ON c.company_id = b.company_id WHERE semester_id = '${req.params.semester_id}'`,
		(error, result, fields) => {
			if(error) {
				res.json({error: true, result: result});
			} else {
				if(result.length == 0) {
					res.json({error: false, result: result});
				} else {
					res.json({
				  	error: false,
				  	result: result[0]
				  });
				}
			}
		})
};

exports.updateSemester = (req, res, next) => {
	var formData = {
		company_id: req.body.company_id,
		semester_name: req.body.semester_name
	};

	db.query(`UPDATE semester SET
		company_id = '${formData.company_id}', semester_name = '${formData.semester_name}' WHERE semester_id = '${req.params.semester_id}'`,
		(error, result, fields) => {
		if(error) {
			res.json({error: true, result: error});
		} else {
			db.query(`SELECT * FROM semester WHERE semester_id = '${result.insertId}'`, (error, result, fields) => {
				res.json({error: false, result: result[0]});
			})
		}
	});
};

exports.getSemesterByCompanyId = (req, res) => {
	let sql = `SELECT s.*, sk.id, sk.nilai
							FROM semester s
								LEFT JOIN semester_kelulusan sk ON sk.semester_id = s.semester_id WHERE s.company_id = ?`
	db.query(sql, [req.params.companyId], (err, rows) => {
		if(err) {
			res.json({error: true, result: error});
		}
		else {
			res.json({ error: false, result: rows.length ? rows : [] })
		}
	})
}

exports.postSemesterByCompanyId = (req, res) => {
	let from = req.body;
	if(from.id) {
		let sql = `UPDATE semester_kelulusan SET nilai = ? WHERE id = ?`
		db.query(sql, [from.nilai, from.id], (err, rows) => {
			if(err) {
				res.json({error: true, result: error});
			}
			else {
				res.json({ error: false, result: from })
			}
		})
	}
	else {
		let sql = `INSERT INTO semester_kelulusan (semester_id, nilai) VALUES (?)`
		db.query(sql, [[from.semester_id, from.nilai]], (err, rows) => {
			if(err) {
				res.json({error: true, result: error});
			}
			else {
				from.id = rows.insertId
				res.json({ error: false, result: from })
			}
		})
	}
}

exports.deleteSemester = (req, res, next) => {
	db.query(`DELETE FROM semester WHERE semester_id = '${req.params.semester_id}'`, (error, result, next) => {
		if(error) {
			res.json({error: true, result: error});
		} else {
			res.json({error: false, result: result});
		}
	})
}
