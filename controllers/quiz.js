var env = require('../env.json');
var conf = require('../configs/config');
var db = require('../configs/database');

exports.createQuiz = (req, res, next) => {
	let form = {
		random: req.body.random,
		exam_title: req.body.quiz_title,
		exam_description: req.body.quiz_description,
		exam_publish: req.body.quiz_publish,
		
		company_id: req.body.company_id,
		course_id: req.body.course_id,
		user_id: req.body.user_id
	};

	db.query(`INSERT INTO exam (exam_id, company_id, course_id, user_id, quiz, random, exam_title, exam_description, exam_publish, created_at) VALUES 
			(null, '${form.company_id}', '${form.course_id}', '${form.user_id}', '1', '${form.random}', '${form.exam_title}', '${form.exam_description}', '${form.exam_publish}', '${conf.dateTimeNow()}')`, 
		(error, result, next) => {
		if(error) {
			res.json({ error: true, result: error });
		} else {
			db.query(`SELECT * FROM exam WHERE exam_id = '${result.insertId}'`, (error, result, fields) => {
				res.json({ error: false, result: result[0] });
			});
		}
	});
};

exports.getAllQuiz = (req, res, next) => {
	db.query(`SELECT * FROM exam WHERE quiz = '1'`, (error, result, fields) => {
		if(error) {
			res.json({ error: true, result: error });
		} else {
			res.json({ error: false, result: result });
		}
	})
};

exports.getOneQuizById = (req, res, next) => {
	db.query(`SELECT * FROM exam WHERE quiz = '1' AND exam_id = '${req.params.quiz_id}'`, (error, result, fields) => {
		if(error) {
			res.json({ error: true, result: error });
		} else {
			res.json({ error: false, result: result[0] });
		}
	});
};

exports.getQuizByCourse = (req, res, next) => {
	db.query(`SELECT * FROM exam WHERE quiz = '1' AND company_id = '${req.params.company_id}' AND course_id = '${req.params.course_id}'`, async (error, result, fields) => {
		if(error) {
			res.json({ error: true, result: error });
		} else {
			let tempArray = result;
			for(let i=0; i<tempArray.length; i++) {
				let numRows = await db.query(`SELECT * FROM exam_question WHERE exam_id = '${tempArray[i].exam_id}'`);
				tempArray[i].soal = numRows.length;
			}

			res.json({ error: false, result: tempArray });
		}
	});
};

exports.getQuizByCompany = (req, res, next) => {
	db.query(`SELECT * FROM exam WHERE quiz = '1' AND company_id = '${req.params.company_id}'`, (error, result, fields) => {
		if(error) {
			res.json({ error: true, result: error });
		} else {
			res.json({ error: false, result: result });
		}
	});
};

exports.updateQuizById = (req, res, next) => {
	let formData = {
		random: req.body.random,
		exam_title: req.body.quiz_title,
		exam_description: req.body.quiz_description,
		exam_publish: req.body.quiz_publish
	};

	db.query(`UPDATE exam SET 
		random = '${formData.random}',
		exam_title = '${formData.exam_title}',
		exam_description = '${formData.exam_description}',
		exam_publish = '${formData.exam_publish}'
		WHERE exam_id = ${req.params.quiz_id}`,
			(error, result, fields) => {
		if(error) {
			res.json({ error: true, result: error });
		} else {
			db.query(`SELECT * FROM exam WHERE exam_id = '${req.params.quiz_id}'`, (error, result, fields) => {
				res.json({
					error: false,
					result: result[0]
				});
			})
		}
	});
};

exports.updateQuizAtById = (req, res, next) => {
	let form = {
		quiz_at: req.body.quiz_at,
	}
	
	db.query(`UPDATE exam SET quiz_at = '${form.quiz_at}' WHERE exam_id = ${req.params.quiz_id}`, (error, result, fields) => {
		if(error) {
			res.json({ error: true, result: error });
		} else {
			db.query(`SELECT * FROM exam WHERE exam_id = '${req.params.quiz_id}'`, (error, result, fields) => {
				res.json({
					error: false,
					result: result[0]
				});
			})
		}
	})
}

exports.deleteQuizById = (req, res, next) => {
	db.query(`DELETE FROM exam WHERE exam_id = '${req.params.quiz_id}'`, (error, result, fields) => {
		if(error) {
			res.json({ error: true, result: error });
		} else {
			res.json({
				error: false,
				result: result
			});
		}
	});
};
