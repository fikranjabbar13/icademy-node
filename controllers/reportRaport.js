var env = require("../env.json");
var db = require("../configs/database");
var conf = require("../configs/config");
var pdf = require("html-pdf");
var path = require("path");
var fs = require("fs");

const html = ` <div className="row">
    <div className="col-sm-12">
      <div className="card" style={{ height: '650px', paddingBottom: 10 }}>
        <h3 className="f-24 fc-skyblue f-w-800 mb-3 mt-3 p-l-20">
          Report / Raport
          </h3>

        <table className="table " style={{ textTransform: 'uppercase' }}>
          <tr>
            <td className="f-w-800 f-14 text-c-grey" style={{ width: '30%' }}> Name </td>
            <td style={{ width: '1%' }} > : </td>
            <td>Jo</td>
          </tr>
          <tr>
            <td className="f-w-800 f-14 text-c-grey" style={{ width: '30%' }}>Date of birth</td>
            <td style={{ width: '1%' }}> : </td>
            <td>54</td>
          </tr>
          <tr>
            <td className="f-w-800 f-14 text-c-grey" style={{ width: '30%' }}>Class</td>
            <td style={{ width: '1%' }}> : </td>
            <td>Jo</td>
          </tr>
          <tr>
            <td className="f-w-800 f-14 text-c-grey" style={{ width: '30%' }}>Semester</td>
            <td style={{ width: '1%' }}> : </td>
            <td>54</td>
          </tr>
          <tr>
            <td className="f-w-800 f-14 text-c-grey" style={{ width: '30%' }}>School year</td>
            <td style={{ width: '1%' }}> : </td>
            <td>54</td>
          </tr>
        </table>
</div>`;

const options = {
  format: "Potrait",
};


exports.reportRaport = (req, res) => {
  pdf.create(html, options).toFile('./download-Raport.pdf', (err, result) => {
    if (err) {
      res.json({ error: true, result: err });
    } else {
      res.json({ error: false, result: `${env.APP_URL}` + fileName  });
    }
  });
};
