const db = require('../configs/database');

function getThisWeek() {
  var curr = new Date; // get current date
  var first = curr.getDate() - curr.getDay(); // First day is the day of the month - the day of the week
  var last = first + 6; // last day is the first day + 6

  var firstday = new Date(curr.setDate(first));
  var lastday = new Date(curr.setDate(last));

  return { firstday, lastday };
}

exports.readByJadwalId = (req, res) => {
  let sql = `
  SELECT lj.*, lp.nama_pelajaran, lj.deskripsi, lk.kelas_nama, rm.nama_ruangan, lg.nama AS pengajar, rm.folder
  FROM learning_jadwal lj JOIN learning_pelajaran lp ON lp.pelajaran_id = lj.pelajaran_id
	  JOIN learning_kelas lk ON lk.kelas_id = lj.kelas_id
    JOIN ruangan_mengajar rm ON rm.id = lj.ruangan_id
    JOIN learning_guru lg ON lg.id = rm.pengajar
  WHERE lj.jadwal_id = ?`;
  db.query(sql, [req.params.id], (error, rows) => {
    if(error) res.json({ error: true, result: error })

    res.json({ error: false, result: rows.length ? rows[0] : [] })
  })
}

exports.create = (req, res) => {
  let data = [];
  if(req.query.bulk) {
    // insert bulk
    for (var i = 0; i < req.body.days.length; i++) {
      data.push([
        req.body.company_id, req.body.pelajaran_id, req.body.kelas_id, req.body.ruangan_id,
        req.body.days[i].day, req.body.days[i].start, req.body.days[i].end,
        req.body.jumlah_pertemuan, req.body.kapasitas
      ])
    }
  }
  else {
    // insert one
    data.push([
      req.body.company_id, req.body.pelajaran_id, req.body.kelas_id, req.body.ruangan_id,
      req.body.hari, req.body.jam_mulai, req.body.jam_selesai,
      req.body.jumlah_pertemuan, req.body.kapasitas
    ]);
  }

  // res.json(data)

  let sql = `INSERT INTO learning_jadwal (company_id, pelajaran_id, kelas_id, ruangan_id, hari, jam_mulai, jam_selesai, jumlah_pertemuan, kapasitas) VALUES ?`;
  db.query(sql, [data], (error, result) => {
    if(error) {
      return res.json({error: true, result: error.message});
    };

    let sql = `SELECT * FROM learning_jadwal WHERE jadwal_id = ?`;
    db.query(sql, [result.insertId], (error, result) => {
      return res.json({error: false, result: result.length ? result[0] : []});
    })
  });
};

exports.read = (req, res) => {
  let sql = `SELECT lk.kelas_id, lj.deskripsi, lk.kelas_nama, lk.kurikulum, lk.kapasitas, lj.jadwal_id, lj.company_id, lj.pelajaran_id, lj.ruangan_id, lj.hari, lj.jam_mulai, lj.jam_selesai, lj.jumlah_pertemuan, p.pengajar AS pengajar_id
    FROM learning_jadwal lj
    	JOIN learning_pelajaran k ON k.pelajaran_id = lj.pelajaran_id
      JOIN ruangan_mengajar p ON p.id = lj.ruangan_id
      JOIN learning_kelas lk ON lk.kelas_id = lj.kelas_id
    WHERE lj.jadwal_id = ?`;
  db.query(sql, [req.params.id], async (error, rows) => {
    if(error) res.json({ error: true, result: error })

    if(rows.length) {
      for(var i=0; i<rows.length; i++) {
        let getSilabus = await db.query(`SELECT id FROM learning_pelajaran_silabus WHERE pelajaran_id = ?`, [rows[i].pelajaran_id])
        rows[i].silabus = getSilabus.length;
      }
    }

    res.json({ error: false, result: rows.length ? rows[0] : [] })
  })
}

exports.readByCompany = (req, res) => {
  let sql = `
  SELECT lj.*, lp.nama_pelajaran, lk.kelas_nama, rm.nama_ruangan, lg.nama AS pengajar
  FROM learning_jadwal lj JOIN learning_pelajaran lp ON lp.pelajaran_id = lj.pelajaran_id
	  JOIN learning_kelas lk ON lk.kelas_id = lj.kelas_id
    JOIN ruangan_mengajar rm ON rm.id = lj.ruangan_id
    JOIN learning_guru lg ON lg.id = rm.pengajar
  WHERE lj.company_id = ?`;
  db.query(sql, [req.params.id], (error, rows) => {
    if(error) res.json({ error: true, result: error })

    res.json({ error: false, result: rows })
  })
}

exports.eventMuridByUserId = async (req, res) => {

  let sqlKelasMurid = `
    SELECT lkm.kelas_id, lk.kelas_nama, lk.semester_id, s.semester_name, lk.tahun_ajaran, lm.id, lm.user_id, lm.nama, lm.no_induk
    FROM learning_kelas_murid lkm
      JOIN learning_murid lm ON lm.id = lkm.murid_id
      JOIN learning_kelas lk ON lk.kelas_id = lkm.kelas_id AND lk.tahun_ajaran = ?
      JOIN semester s ON s.semester_id = lk.semester_id
    WHERE lm.user_id = ?
  `;

  let tahunAjaran = '';
  if(req.query.tahunAjaran) {
    tahunAjaran = req.query.tahunAjaran;
  }
  else {
    let d = new Date();
    // bulan diawali dengan 0 = januari, 11 = desember
    let month = d.getMonth();
    tahunAjaran = month < 6 ? (d.getFullYear()-1)+'/'+d.getFullYear() : d.getFullYear()+'/'+(d.getFullYear()+1);
  }

  let getKelasMurid = await db.query(sqlKelasMurid, [tahunAjaran, req.params.userId]);

  let sqlJadwal = `
  SELECT cc.course_id, cc.chapter_id, cc.chapter_title, ljk.user_id, ljk.absen_jam, cc.start_date, cc.end_date, lj.jadwal_id, lj.ruangan_id, rm.nama_ruangan, lg.id AS guru_id, lg.nama AS nama_guru, lj.pelajaran_id, lp.nama_pelajaran, lj.kelas_id, lk.kelas_nama, lj.hari, lj.jam_mulai, lj.jam_selesai, lj.jumlah_pertemuan AS sesi
  FROM learning_jadwal lj
    JOIN learning_pelajaran lp ON lp.pelajaran_id = lj.pelajaran_id
    JOIN learning_kelas lk ON lk.kelas_id = lj.kelas_id
    JOIN ruangan_mengajar rm ON rm.id = lj.ruangan_id
    JOIN learning_guru lg ON lg.id = rm.pengajar
    JOIN course_chapter cc ON cc.course_id = lj.jadwal_id AND cc.jenis_pembelajaran = 'learning'
    LEFT JOIN learning_jadwal_kehadiran ljk ON ljk.sesi_id = cc.chapter_id AND ljk.event = 'materi' AND ljk.user_id = ?
  WHERE lj.kelas_id = ?
  ORDER BY cc.start_date ASC`;
  let mengajar = await db.query(sqlJadwal, [req.params.userId, getKelasMurid.length ? getKelasMurid[0].kelas_id : 0]);

  let ptc = [];

  let sqlTugas = `
    SELECT e.exam_id, e.course_id, lp.pelajaran_id, lp.nama_pelajaran, e.exam_title, e.time_start, e.time_finish
    FROM exam e
    	JOIN learning_jadwal lj ON lj.jadwal_id = e.course_id
      JOIN learning_pelajaran lp ON lp.pelajaran_id = lj.pelajaran_id
    WHERE e.jenis = '1'
      AND e.quiz = '2'
      AND lj.kelas_id = ?
  `;
  let tugas = await db.query(sqlTugas, [getKelasMurid[0].kelas_id]);
  if(tugas.length > 0) {
    for(var i=0; i<tugas.length; i++) {
      let sql = `SELECT * FROM exam_tugas_answer WHERE exam_id = ? AND user_id = ?`;
      let submitted = await db.query(sql, [tugas[i].exam_id, req.params.userId]);
      tugas[i].submitted = submitted;
    }
  }

  let sqlQuiz = `
  SELECT e.exam_id, e.course_id AS jadwal_id, ljk.absen_jam, eta.score, eta.user_id, eta.total_correct, eta.total_uncorrect, e.course_id, lp.pelajaran_id, lp.nama_pelajaran, e.exam_title, e.time_start, e.time_finish
    FROM exam e
      JOIN learning_jadwal lj ON lj.jadwal_id = e.course_id
      JOIN learning_pelajaran lp ON lp.pelajaran_id = lj.pelajaran_id
      LEFT JOIN exam_result_learning eta ON eta.exam_id =  e.exam_id AND eta.user_id = ?
      LEFT JOIN learning_jadwal_kehadiran ljk ON ljk.sesi_id = e.exam_id AND ljk.event = 'kuis' and ljk.user_id = ?
    WHERE e.jenis = '1'
      AND e.quiz = '1'
      AND lj.kelas_id = ?
  `;
  let quiz = await db.query(sqlQuiz, [req.params.userId, req.params.userId, getKelasMurid[0].kelas_id]);
  for(var i=0; i<quiz.length; i++) {
    let soal = await db.query(`SELECT exam_id FROM exam_question WHERE exam_id = ?`, quiz[i].exam_id);
    quiz[i].soal = soal.length;
  }

  let sqlUjian = `
  SELECT e.exam_id, e.course_id AS jadwal_id, ljk.absen_jam, eta.score, eta.user_id, eta.total_correct, eta.total_uncorrect, e.course_id, lp.pelajaran_id, lp.nama_pelajaran, e.exam_title, e.time_start, e.time_finish
    FROM exam e
      JOIN learning_jadwal lj ON lj.jadwal_id = e.course_id
      JOIN learning_pelajaran lp ON lp.pelajaran_id = lj.pelajaran_id
      LEFT JOIN exam_result_learning eta ON eta.exam_id =  e.exam_id AND eta.user_id = ?
      LEFT JOIN learning_jadwal_kehadiran ljk ON ljk.sesi_id = e.exam_id AND ljk.event = 'kuis' and ljk.user_id = ?
    WHERE e.jenis = '1'
      AND e.quiz = '0'
      AND lj.kelas_id = ?
  `;
  let ujian = await db.query(sqlUjian, [req.params.userId, req.params.userId, getKelasMurid[0].kelas_id]);
  for(var i=0; i<ujian.length; i++) {
    let soal = await db.query(`SELECT exam_id FROM exam_question WHERE exam_id = ?`, ujian[i].exam_id);
    ujian[i].soal = soal.length;
  }

  res.json({
    error: false,
    result: {
      mengajar: mengajar,
      ptc: ptc,
      tugas: tugas,
      quiz: quiz,
      ujian: ujian,
    }
  })
}

exports.readMuridByUserId = (req, res) => {
  // INFO MURID
  let sql = `
    SELECT lkm.kelas_id, lk.kelas_nama, lk.semester_id, s.semester_name, lk.tahun_ajaran, lm.id, lm.user_id, lm.nama, lm.no_induk
    FROM learning_kelas_murid lkm
      JOIN learning_murid lm ON lm.id = lkm.murid_id
      JOIN learning_kelas lk ON lk.kelas_id = lkm.kelas_id AND lk.tahun_ajaran = ?
      JOIN semester s ON s.semester_id = lk.semester_id
    WHERE lm.user_id = ?
  `;

  let tahunAjaran = '';
  if(req.query.tahunAjaran) {
    tahunAjaran = req.query.tahunAjaran;
  }
  else {
    let d = new Date();
    // bulan diawali dengan 0 = januari, 11 = desember
    let month = d.getMonth();
    tahunAjaran = month < 6 ? (d.getFullYear()-1)+'/'+d.getFullYear() : d.getFullYear()+'/'+(d.getFullYear()+1);
  }

  db.query(sql, [tahunAjaran, req.params.userId], async (err, rows) => {
    if(err) res.json({ error: true, result: err })

    // GET JADWAL
    for(var i=0; i<rows.length; i++) {

      let sqlJadwal = `
      SELECT lj.jadwal_id, lj.ruangan_id, rm.nama_ruangan, lg.id AS guru_id, lg.nama AS nama_guru, lj.pelajaran_id, lp.nama_pelajaran, lj.kelas_id, lk.kelas_nama, lj.hari, lj.jam_mulai, lj.jam_selesai, lj.jumlah_pertemuan AS sesi
      FROM learning_jadwal lj
      JOIN learning_pelajaran lp ON lp.pelajaran_id = lj.pelajaran_id
        JOIN learning_kelas lk ON lk.kelas_id = lj.kelas_id
        JOIN ruangan_mengajar rm ON rm.id = lj.ruangan_id
        JOIN learning_guru lg ON lg.id = rm.pengajar
      WHERE lj.kelas_id = ?
      `;

      let jadwal = await db.query(sqlJadwal, [rows[i].kelas_id]);
      rows[i].jadwal = jadwal;

      let sqlTugas = `
        SELECT e.exam_id, e.course_id, lp.pelajaran_id, lp.nama_pelajaran, e.exam_title AS title, e.time_start, e.time_finish
        FROM exam e
        JOIN learning_jadwal lj ON lj.jadwal_id = e.jadwal_id
          JOIN learning_pelajaran lp ON lp.pelajaran_id = lj.pelajaran_id
        WHERE e.jenis = '1'
          AND e.quiz = '2'
          AND lj.kelas_id = ?
      `;
      let tugas = await db.query(sqlTugas, [rows[i].kelas_id]);
      rows[i].tugas = tugas;

      let sqlUjian = `
        SELECT e.exam_id, e.course_id, lp.pelajaran_id, lp.nama_pelajaran, e.exam_title AS title, e.time_start, e.time_finish
        FROM exam e
        JOIN learning_jadwal lj ON lj.jadwal_id = e.jadwal_id
          JOIN learning_pelajaran lp ON lp.pelajaran_id = lj.pelajaran_id
        WHERE e.jenis = '1'
          AND e.quiz = '0'
          AND lj.kelas_id = ?
      `;
      let ujian = await db.query(sqlUjian, [rows[i].kelas_id]);
      rows[i].ujian = ujian;
    }

    // console.log(rows[0])

    res.json({ error: false, result: rows[0] })
  })
}

exports.eventGuruByUserId = async (req, res) => {
  let sql = `
  SELECT
  	cc.chapter_id, cc.chapter_title, cc.start_date, cc.end_date,
      lg.id AS guru_id, lg.user_id, lg.nama AS pengajar,
      lj.*, lp.nama_pelajaran,
      lk.kelas_nama, lk.semester_id, rm.nama_ruangan

  FROM learning_jadwal lj
  	JOIN learning_pelajaran lp ON lp.pelajaran_id = lj.pelajaran_id
      JOIN learning_pelajaran_silabus lps ON lps.pelajaran_id = lp.pelajaran_id
  	JOIN learning_kelas lk ON lk.kelas_id = lj.kelas_id
  	JOIN ruangan_mengajar rm ON rm.id = lj.ruangan_id
  	JOIN learning_guru lg ON lg.id = rm.pengajar

  	JOIN course_chapter cc ON cc.jadwal_id = lj.jadwal_id AND cc.jenis_pembelajaran = 'learning' AND cc.course_id = lp.pelajaran_id AND cc.silabus_id = lps.id

  WHERE lg.user_id = ?
  `;
  let mengajar = await db.query(sql, [req.params.userId]);

  let sqlPtc = `SELECT lp.*, u.name FROM learning_ptc lp JOIN user u ON u.user_id = lp.moderator WHERE lp.moderator = ?`;
  let ptc = await db.query(sqlPtc, [req.params.userId]);

  let sqlTugas = `SELECT lp.nama_pelajaran, lk.kelas_nama, lk.semester_id, rm.nama_ruangan, e.exam_id, e.exam_title, e.time_start, e.time_finish, lg.id AS guru_id, lg.user_id, lg.nama AS pengajar, lj.*
    FROM learning_jadwal lj JOIN learning_pelajaran lp ON lp.pelajaran_id = lj.pelajaran_id
      JOIN learning_kelas lk ON lk.kelas_id = lj.kelas_id
      JOIN ruangan_mengajar rm ON rm.id = lj.ruangan_id
      JOIN learning_guru lg ON lg.id = rm.pengajar
		  JOIN exam e ON e.course_id = lj.jadwal_id AND e.jenis = 1 AND e.quiz = 2
    WHERE lg.user_id = ?`
  let tugas = await db.query(sqlTugas, [req.params.userId]);

  let sqlQuiz = `SELECT lp.nama_pelajaran, lk.kelas_nama, lk.semester_id, rm.nama_ruangan, e.exam_id, e.exam_title, e.time_start, e.time_finish, lg.id AS guru_id, lg.user_id, lg.nama AS pengajar, lj.*
    FROM learning_jadwal lj JOIN learning_pelajaran lp ON lp.pelajaran_id = lj.pelajaran_id
      JOIN learning_kelas lk ON lk.kelas_id = lj.kelas_id
      JOIN ruangan_mengajar rm ON rm.id = lj.ruangan_id
      JOIN learning_guru lg ON lg.id = rm.pengajar
		  JOIN exam e ON e.course_id = lj.jadwal_id AND e.jenis = 1 AND e.quiz = 1
    WHERE lg.user_id = ?`
  let quiz = await db.query(sqlQuiz, [req.params.userId]);

  let sqlUjian = `SELECT lp.nama_pelajaran, lk.kelas_nama, lk.semester_id, rm.nama_ruangan, e.exam_id, e.exam_title, e.time_start, e.time_finish, lg.id AS guru_id, lg.user_id, lg.nama AS pengajar, lj.*
    FROM learning_jadwal lj JOIN learning_pelajaran lp ON lp.pelajaran_id = lj.pelajaran_id
      JOIN learning_kelas lk ON lk.kelas_id = lj.kelas_id
      JOIN ruangan_mengajar rm ON rm.id = lj.ruangan_id
      JOIN learning_guru lg ON lg.id = rm.pengajar
      JOIN exam e ON e.course_id = lj.jadwal_id AND e.jenis = 1 AND e.quiz = 0
    WHERE lg.user_id = ?`
  let ujian = await db.query(sqlUjian, [req.params.userId]);

  let events = [];
  res.json({
    error: false,
    result: {
      mengajar: mengajar,
      ptc: ptc,
      tugas: tugas,
      quiz: quiz,
      ujian: ujian,
    }
  })
}

exports.readGuruByUserId = (req, res) => {
  let sql = `
  SELECT lg.id AS guru_id, lg.user_id, lg.nama AS pengajar, lj.*, lp.nama_pelajaran, lk.kelas_nama, lk.tahun_ajaran, lk.semester_id, s.semester_name, rm.nama_ruangan

  FROM learning_jadwal lj JOIN learning_pelajaran lp ON lp.pelajaran_id = lj.pelajaran_id
    JOIN learning_kelas lk ON lk.kelas_id = lj.kelas_id AND lk.tahun_ajaran = ?
    JOIN ruangan_mengajar rm ON rm.id = lj.ruangan_id
    JOIN learning_guru lg ON lg.id = rm.pengajar
    JOIN semester s ON s.semester_id = lk.semester_id

  WHERE lg.user_id = ?
  `;

  let tahunAjaran = '';
  if(req.query.tahunAjaran) {
    tahunAjaran = req.query.tahunAjaran;
  }
  else {
    let d = new Date();
    // bulan diawali dengan 0 = januari, 11 = desember
    let month = d.getMonth();
    tahunAjaran = month < 6 ? (d.getFullYear()-1)+'/'+d.getFullYear() : d.getFullYear()+'/'+(d.getFullYear()+1);
  }

  db.query(sql, [tahunAjaran, req.params.userId], (err, rows) => {
    if(err) res.json({ error: true, result: err })

    res.json({error: false, result: rows})
  })
}

exports.update = (req, res) => {
  let sql = `UPDATE learning_jadwal SET pelajaran_id = ?, kelas_id = ?, ruangan_id = ?, hari = ?, jam_mulai = ?, jam_selesai = ?, jumlah_pertemuan = ?, kapasitas = ?
    WHERE jadwal_id = ?`;
  let data = [req.body.pelajaran_id, req.body.kelas_id, req.body.ruangan_id, req.body.hari, req.body.jam_mulai, req.body.jam_selesai, req.body.jumlah_pertemuan, req.body.kapasitas, req.params.id];
  db.query(sql, data, (error, rows) => {
    if(error) res.json({ error: true, result: error })

    let sql = `SELECT * FROM learning_jadwal WHERE jadwal_id = ?`;
    db.query(sql, [req.params.id], (error, result) => {
      return res.json({error: false, result});
    })
  })
}

exports.delete = (req, res) => {
  let sql = `DELETE FROM learning_jadwal WHERE jadwal_id = ?`;
  db.query(sql, [req.params.id], (error, rows) => {
    if(error) res.json({ error: true, result: error })

    res.json({ error: false, result: rows })
  })
}

exports.deleteByCompany = (req, res) => {
  let sql = `DELETE FROM learning_jadwal WHERE company_id = ?`;
  db.query(sql, [req.params.id], (error, rows) => {
    if(error) res.json({ error: true, result: error })

    res.json({ error: false, result: rows })
  })
}


// exports.eventGuruByUserId =  async ( req, res) => {

//   let sql = `
//   SELECT cc.chapter_id, cc.chapter_title, cc.start_date, cc.end_date, lg.id AS guru_id, lg.user_id, lg.nama AS pengajar, lj.*, lp.nama_pelajaran, lk.kelas_nama, lk.semester_id, rm.nama_ruangan
//     FROM learning_jadwal lj JOIN learning_pelajaran lp ON lp.pelajaran_id = lj.pelajaran_id
//       JOIN learning_kelas lk ON lk.kelas_id = lj.kelas_id
//       JOIN ruangan_mengajar rm ON rm.id = lj.ruangan_id
//       JOIN learning_guru lg ON lg.id = rm.pengajar
// 		  JOIN course_chapter cc ON cc.course_id = lj.jadwal_id AND cc.jenis_pembelajaran = 'learning'
//     WHERE lg.user_id = ?
//   `;
//   let mengajar = await db.query(sql, [req.params.userId]);

//   let sqlPtc = `SELECT lp.*, u.name FROM learning_ptc lp JOIN user u ON u.user_id = lp.moderator WHERE lp.moderator = ?`;
//   let ptc = await db.query(sqlPtc, [req.params.userId]);

//   let events = [];
//   res.json({
//     error: false,
//     result: {
//       mengajar: mengajar,
//       ptc: ptc
//     }
//   })
// }