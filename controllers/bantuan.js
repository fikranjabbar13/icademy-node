var env = require('../env.json');
var conf = require('../configs/config');
var db = require('../configs/database');
var path = require('path');

const AWS = require('aws-sdk');
const fs = require('fs');
const s3 = new AWS.S3({
	accessKeyId: env.AWS_ACCESS,
	secretAccessKey: env.AWS_SECRET
});

var multer = require('multer');
var storage = multer.diskStorage({
	destination: (req, file, cb) => {
		cb(null, './public/bantuan');
	},
	filename: (req, file, cb) => {
		cb(null, file.mimetype.split('/')[1] + '-' + Math.floor(Date.now() / 1000) + '-' + file.originalname);
	}
});
var uploadFile = multer({ storage: storage }).single('file');

exports.createBantuan = (req, res, next) => {
	uploadFile(req, res, (err) => {
		if (!req.file) {
			res.json({ error: true, result: err });
		} else {
			// console.log(req.file);
			const filenameType = req.file.filename.split('.');
			const formData = {
				original_name: req.file.filename,
				file_type: filenameType[filenameType.length - 1],
				full_path_name: `${env.APP_URL}/${req.body.folder}/${req.file.filename}`,
				folder: req.body.folder,
				tag: req.body.tag,
				page: req.body.page,
				description: req.body.description,
				created_at: conf.dateNow(),
			};

			db.query(`INSERT INTO bantuan 
				(original_name, file_type, full_path_name, folder, tag, page, description, created_at) 
				VALUES 
				('${formData.original_name}', '${formData.file_type}', '${formData.full_path_name}', '${formData.folder}', '${formData.tag}', '${formData.page}', '${formData.description}', '${formData.created_at}')`,
				(error, result, fields) => {
					if (error) {
						res.json({ error: true, result: error });
					} else {
						db.query(`SELECT * FROM bantuan WHERE bantuan_id = '${result.insertId}'`, (error, result, fields) => {
							res.json({
								error: false,
								result: result[0]
							});
						})
					}
				});
		}
	});
};

exports.uploadFilesBantuanS3 = (req, res, next) => {
	uploadFile(req, res, (err) => {
		if (!req.file) {
			res.json({ error: true, result: err });
		} else {
			let path = req.file.path;
			let keyName = `${env.AWS_BUCKET_ENV}/${req.body.folder}`;

			var params = {
				ACL: 'public-read',
				Bucket: env.AWS_BUCKET,
				Body: fs.createReadStream(path),
				Key: `${keyName}/${req.file.originalname}`,
				Metadata: {
					'Content-Type': req.file.mimetype,
					'Content-Disposition': 'inline'
				}
			};

			s3.upload(params, (err, data) => {
				//return console.log(req.file, req.body, data, 'gasskes');
				if (err) {
					res.json({ error: true, msg: "Error Upload Image", result: err });
				}
				if (data) {
					fs.unlinkSync(path);

					const filenameType = req.file.filename.split('.');
					const formData = {
						original_name: req.file.filename,
						file_type: filenameType[filenameType.length - 1],
						full_path_name: data.Location,
						folder: keyName,
						tag: req.body.tag,
						page: req.body.page,
						description: req.body.description,
						created_at: conf.dateNow(),
					};

					db.query(`INSERT INTO bantuan 
						(original_name, file_type, full_path_name, folder, tag, page, description, created_at) 
						VALUES 
						('${formData.original_name}', '${formData.file_type}', '${formData.full_path_name}', '${formData.folder}', '${formData.tag}', '${formData.page}', '${formData.description}', '${formData.created_at}')`,
						(error, result, fields) => {
							if (error) {
								res.json({ error: true, result: error });
							} else {
								db.query(`SELECT * FROM bantuan WHERE bantuan_id = '${result.insertId}'`, (error, result, fields) => {
									res.json({
										error: false,
										result: result[0]
									});
								})
							}
						});

				}
			});


		}
	});
};

exports.getAllBantuan = (req, res, next) => {
	db.query(`SELECT * FROM bantuan`, (error, result, fields) => {
		if (error) {
			res.json({ error: true, result: error });
		} else {
			res.json({
				error: false,
				result: result
			});
		}
	});
};

exports.browseTutorial = (req, res, next) => {
	db.query(`SELECT * FROM tutorial ORDER BY sort ASC`, (error, result, fields) => {
		if (error) {
			res.json({ error: true, result: error });
		} else {
			res.json({
				error: false,
				result: result
			});
		}
	});
};

exports.viewFile = (req, res, next) => {
	console.log(req.params)
	db.query(`SELECT * FROM bantuan WHERE original_name = '${req.params.filename}'`, (error, result, fields) => {
		if (error) {
			res.json({ error: true, result: error });
		} else {
			res.sendFile(path.join(__dirname, '../public/bantuan') + `/${req.params.filename}`);
			// res.json({
			// 	error: false,
			// 	result: path.join(__dirname, '../public/bantuan')
			// });
		}
	});
};

exports.updateAttachment = (req, res, next) => {
	uploadLogo(req, res, (err) => {
		if (!req.file) {
			res.json({ error: true, result: err });
		} else {
			var filenameType = req.file.filename.split('.');
			var formData = {
				created_at: conf.dateNow(),
				attachment_type: filenameType[filenameType.length - 1],
				attachment_name: `${env.APP_URL}/attachment/${req.file.filename}`
			};

			db.query(`UPDATE attachment 
				SET attachment_name = '${formData.attachment_name}', attachment_type = '${formData.attachment_type}' 
				WHERE attachment_id = '${req.params.attachment_id}'`,
				(error, result, fields) => {
					if (error) {
						res.json({ error: true, result: error });
					} else {
						db.query(`SELECT * FROM attachment WHERE attachment_id = '${req.params.attachment_id}'`, (error, result, fields) => {
							res.json({
								error: false,
								result: result[0]
							});
						})
					}
				});
		}
	});
};

exports.deleteAttachment = (req, res, next) => {
	db.query(`DELETE FROM attachment WHERE attachment_id = '${req.params.attachment_id}'`, (error, result, fields) => {
		if (error) {
			res.json({ error: true, result: error });
		} else {
			res.json({
				error: false,
				result: result
			});
		}
	});
};
