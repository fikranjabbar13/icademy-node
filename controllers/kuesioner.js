var env = require("../env.json");
var conf = require("../configs/config");
var db = require("../configs/database");
var fs = require('fs');

var multer = require('multer');
var storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, './public/company');
    },
    filename: (req, file, cb) => {
        cb(null, 'excel-' + Date.now() + '-' + file.originalname);
    }
});
var uploadExcel = multer({ storage: storage }).single('excel');

/**
 * create kuesioner
 * @param {Number} req.body.id  id webinar
 * @param {Array.<{tanya: String, a: String, b: String, c: String, ...}>} req.body.kuesioner
 * @param {Boolean} res.body.error
 * @param {String} res.body.result
 */
let create = (data, callback) => {
    data.kuesioner.map(async (elem) => {
        let questions = [data.id, elem.tanya];
        let options = [];

        let insert_questions = async () => {
            try {
                return await db.query(
                    `INSERT INTO kuesioner_questions (webinar_id, pertanyaan)
                        VALUES (?);`,
                    [questions]
                );
            } catch (error) {
                return callback({ error: true, result: await error.message });
            }
        };

        let last_insert_id = await insert_questions();

        await Object.keys(elem).map((value, id) => {
            if (id !== 0 && elem[value] !== "" && value !== 'id') {
                options.push([last_insert_id.insertId, value, elem[value]]);
            }
        });

        let insert_options = async () => {
            try {
                await db.query(
                    `INSERT INTO kuesioner_options (kuesioner_id, options, answer)
                        VALUES ?;`,
                    [options]
                );

                return callback({ error: false, result: "success create kuesioner" });
            } catch (error) {
                return callback({ error: true, result: await error.message });
            }
        };

        await insert_options();
    });
};

exports.create = async (req, res) => {
    create(req.body, (result) => res.json(result));
};

/**
 * delete kuesioner
 * @param {String} req.params.idÍ
 * @param {Boolean} res.body.error
 * @param {String} res.body.result
 */
let remove = (data, callback) => {
    db.query(
        `DELETE t1, t2
        FROM kuesioner_questions t1
        INNER JOIN kuesioner_options t2
        ON t1.id = t2.kuesioner_id
        WHERE t1.webinar_id = ${data.id}`,
        (error, result) => {
            if (error) {
                return result({ error: true, result: error.message });
            } else {
                if (result.affectedRows !== 0) {
                    return callback({ error: false, result: "success delete kuesioner" });
                } else {
                    return callback({ error: true, result: "failed delete kuesioner" });
                }
            }
        }
    );
};

exports.delete = (req, res) => {
    remove(req.params, (result) => res.json(result));
};

exports.update = (req, res) => {
    remove(req.body, (result) => {
        if (result.error) {
            res.json({ error: true, result: "failed update kuesioner" });
        } else {
            create(req.body, (callback) => {
                if (callback.error) {
                    res.json({ error: true, result: callback.result });
                } else {
                    res.json({ error: false, result: "success update kuesioner" });
                }
            });
        }
    });
};


exports.readPeserta = (req, res) => {
    db.query(
        `SELECT t1.*, t1.id AS question_id, t2.*, t2.id AS option_id FROM kuesioner_questions t1
        INNER JOIN kuesioner_options t2
        ON t1.id = t2.kuesioner_id
        WHERE t1.webinar_id = ${req.params.id};`,
        (error, result) => {
            if (error) {
                res.json({ error: true, result: error.message });
            } else {
                let group = result.reduce((accumulator, current) => {
                    if (!accumulator[current.kuesioner_id]) {
                        accumulator[current.kuesioner_id] = {};
                        accumulator[current.kuesioner_id]["tanya"] = current.pertanyaan;
                    }
                    accumulator[current.kuesioner_id][current.options] = [current.option_id, current.answer];
                    accumulator[current.kuesioner_id]["question_id"] = current.question_id;

                    return accumulator;
                }, {});

                let kuesioner = [];
                Object.keys(group).map((elem) => {
                    kuesioner.push(group[elem]);
                });

                res.json({ error: false, result: kuesioner });
            }
        }
    );
};
exports.read = (req, res) => {
    db.query(
        `SELECT t1.id as idpertanyaan, t1.*, t2.* FROM kuesioner_questions t1
        INNER JOIN kuesioner_options t2
        ON t1.id = t2.kuesioner_id
        WHERE t1.webinar_id = ${req.params.id};`,
        (error, result) => {
            if (error) {
                res.json({ error: true, result: error.message });
            } else {
                let group = result.reduce((accumulator, current) => {
                    if (!accumulator[current.kuesioner_id]) {
                        accumulator[current.kuesioner_id] = {};
                        accumulator[current.kuesioner_id]["tanya"] = current.pertanyaan;
                    }
                    accumulator[current.kuesioner_id]["id"] = current.idpertanyaan;
                    accumulator[current.kuesioner_id][current.options] = current.answer;

                    return accumulator;
                }, {});

                let kuesioner = [];
                Object.keys(group).map((elem) => {
                    kuesioner.push(group[elem]);
                });

                res.json({ error: false, result: kuesioner });
            }
        }
    );
};

/**
 * create kuesioner
 * @param {Number} req.body.id  id webinar
 * @param {Array.<{tanya: String, jawab: String}>} req.body.kuesioner
 * @param {Boolean} res.body.error
 * @param {String} res.body.result
 */
exports.input = async (req, res) => {
    let data = [];
    await req.body.kuesioner.map((elem) => {
        data.push([req.body.id, req.body.user_id, req.body.pengguna, elem.questions_id, elem.options_id, req.body.feedbackDetails ||'-']);
    });

    await db.query(
        `INSERT INTO kuesioner_answer
            (webinar_id, user_id, pengguna, webinar_questions, webinar_options, feedback_details)
        VALUES ?;`,
        [data],
        (error, result) => {
            if (error) {
                res.json({ error: true, result: error.message });
            } else {
                res.json({ error: false, result: "success input kuesioner" });
            }
        }
    );
};

// exports.result = (req, res) => {
//     db.query(
//         `SELECT t2.id, t2.pertanyaan, t3.options, t3.answer, count(t3.answer) AS count
//         FROM kuesioner_answer t1
//         INNER JOIN kuesioner_questions t2
//         ON t1.webinar_questions = t2.id
//         INNER JOIN kuesioner_options t3
//         ON t1.webinar_options = t3.id
//         WHERE t1.webinar_id = '${req.params.id}'
//         GROUP BY t2.id, t3.id;`,
//         (error, result) => {
//             if (error) {
//                 res.json({ error: true, result: error.message });
//             } else {
//                 let group = result.reduce((accumulator, current) => {
//                     if (!accumulator[current.id]) {
//                         accumulator[current.id] = {};
//                         accumulator[current.id]["tanya"] = current.pertanyaan;
//                     }
//                     accumulator[current.id][current.options] = current.answer;
//                     accumulator[current.id]["count"] = current.count;

//                     return accumulator;
//                 }, {});

//                 let kuesioner = [];
//                 Object.keys(group).map((elem) => {
//                     kuesioner.push(group[elem]);
//                 });

//                 res.json({ error: false, result: kuesioner });
//             }
//         }
//     );
// };

exports.result = (req, res) => {
    db.query(
        `SELECT b.id AS question_id, b.pertanyaan, c.id AS answer_id, c.answer, a.user_id, IF(a.pengguna,d.name,e.name) AS name, feedback_details
        FROM kuesioner_answer a
        INNER JOIN kuesioner_questions b
        ON a.webinar_questions = b.id
        INNER JOIN kuesioner_options c
        ON a.webinar_options = c.id
        LEFT JOIN user d
        ON a.user_id = d.user_id
        LEFT JOIN webinar_tamu e
        ON a.user_id = e.voucher
        WHERE a.webinar_id = '${req.params.id}'
        ORDER BY user_id, question_id`,
        async (error, result) => {
            if (error) {
                res.json({ error: true, result: error.message });
            } else {
                let group = await result.reduce(
                    (accumulator, current) => {
                        if (!accumulator.jawaban[current.user_id]) {
                            accumulator.jawaban[current.user_id] = {};
                            accumulator.jawaban[current.user_id]["nama"] = current.name;
                            accumulator.jawaban[current.user_id]["jawaban"] = [];
                            accumulator.jawaban[current.user_id]["feedback_details"] = "";
                        }

                        if (!accumulator.pertanyaan[current.question_id]) {
                            accumulator.pertanyaan[current.question_id] = {};
                            accumulator.pertanyaan[current.question_id]["pertanyaan"] = current.pertanyaan;
                        }

                        accumulator.jawaban[current.user_id]["jawaban"].push(current.answer);
                        accumulator.jawaban[current.user_id]["feedback_details"] = current.feedback_details || '-';

                        return accumulator;
                    },
                    { pertanyaan: {}, jawaban: {} }
                );

                let pertanyaan = [],
                    jawaban = [];
                Object.keys(await group.pertanyaan).map((elem) => {
                    pertanyaan.push(group.pertanyaan[elem].pertanyaan);
                });
                Object.keys(await group.jawaban).map((elem) => {
                    jawaban.push(group.jawaban[elem]);
                });

                res.json({ error: false, result: { pertanyaan: pertanyaan, jawaban: jawaban } });
            }
        }
    );
};

exports.import = (req, res, next) => {
    uploadExcel(req, res, (err) => {
        if (!req.file) {
            res.json({ error: true, result: err });
        } else {

            // create class Excel
            var Excel = require('exceljs');
            var wb = new Excel.Workbook();
            var path = require('path');
            var filePath = path.resolve(__dirname, '../public/company/' + req.file.filename);

            wb.xlsx.readFile(filePath).then(async function () {
                var sh = wb.getWorksheet("Sheet1");

                //Get all the rows data [1st and 2nd column]
                var webinarId = req.body.webinarId;
                var tempArray = [];
                for (i = 2; i <= sh.rowCount; i++) {

                    console.log('==========================================================')

                    var querySoal = `INSERT INTO kuesioner_questions (webinar_id, pertanyaan) VALUES ('${webinarId}', '${sh.getRow(i).getCell(2).value}')`;
                    console.log(querySoal)

                    // proses insert soal
                    const result = await db.query(querySoal);

                    // create variable
                    let aId = { insertId: null }, bId = { insertId: null }, cId = { insertId: null }, dId = { insertId: null }, eId = { insertId: null };

                    // pilihan jawaban A
                    if (sh.getRow(i).getCell(3).value) {
                        var queryJawab = `INSERT INTO kuesioner_options (kuesioner_id, options, answer) VALUES ('${result.insertId}', 'a', '${sh.getRow(i).getCell(3).value}')`;
                        console.log(queryJawab)
                        aId = await db.query(queryJawab)
                    }

                    // B
                    if (sh.getRow(i).getCell(4).value) {
                        var queryJawab = `INSERT INTO kuesioner_options (kuesioner_id, options, answer) VALUES ('${result.insertId}', 'b', '${sh.getRow(i).getCell(4).value}')`;
                        console.log(queryJawab)
                        bId = await db.query(queryJawab)
                    }

                    // C
                    if (sh.getRow(i).getCell(5).value) {
                        var queryJawab = `INSERT INTO kuesioner_options (kuesioner_id, options, answer) VALUES ('${result.insertId}', 'c', '${sh.getRow(i).getCell(5).value}')`;
                        console.log(queryJawab)
                        cId = await db.query(queryJawab)
                    }

                    // D
                    if (sh.getRow(i).getCell(6).value) {
                        var queryJawab = `INSERT INTO kuesioner_options (kuesioner_id, options, answer) VALUES ('${result.insertId}', 'd', '${sh.getRow(i).getCell(6).value}')`;
                        console.log(queryJawab)
                        dId = await db.query(queryJawab)
                    }

                    // E
                    if (sh.getRow(i).getCell(7).value) {
                        var queryJawab = `INSERT INTO kuesioner_options (kuesioner_id, options, answer) VALUES ('${result.insertId}', 'e', '${sh.getRow(i).getCell(7).value}')`;
                        console.log(queryJawab)
                        eId = await db.query(queryJawab)
                    }
                    console.log('==========================================================')

                    tempArray.push({
                        webinarId: webinarId,
                        kuesionerId: result.insertId,
                        pertanyaan: sh.getRow(i).getCell(2).value,
                        a: sh.getRow(i).getCell(3).value,
                        aId: sh.getRow(i).getCell(3).value ? aId.insertId : null,

                        b: sh.getRow(i).getCell(4).value,
                        bId: sh.getRow(i).getCell(4).value ? bId.insertId : null,

                        c: sh.getRow(i).getCell(5).value,
                        cId: sh.getRow(i).getCell(5).value ? cId.insertId : null,

                        d: sh.getRow(i).getCell(6).value,
                        dId: sh.getRow(i).getCell(6).value ? dId.insertId : null,

                        e: sh.getRow(i).getCell(7).value,
                        eId: sh.getRow(i).getCell(7).value ? eId.insertId : null,
                    });
                }

                fs.unlinkSync(filePath);

                res.json({ error: false, result: tempArray });
            });

        }
    })
}

exports.deleteByIdPertanyaan = (req, res) => {
    // delete on table kuesioner options
    let sql = `DELETE FROM kuesioner_options WHERE kuesioner_id = ?`;
    db.query(sql, [req.params.id], (error, rows) => {
        if (error) res.json({ error: true, result: error })

        let sql = `DELETE FROM kuesioner_questions WHERE id = ?`;
        db.query(sql, [req.params.id], (error, rows) => {
            if (error) res.json({ error: true, result: error })

            res.json({ error: false, result: rows })
        })
    })
}

exports.getKuesionerSender = (req, res, next) => {
    let data = [];
    db.query(`SELECT DISTINCT u.name AS sender FROM kuesioner_answer k JOIN user u ON u.user_id=k.user_id WHERE k.pengguna=1 AND k.webinar_id=${req.params.id}`, (error, result, fields) => {
        if (error) {
            res.json({ error: true, result: error })
        } else {
            result.map(item => {
                data.push(item.sender)
            })

            db.query(`SELECT DISTINCT u.name AS sender FROM kuesioner_answer k JOIN webinar_tamu u ON u.voucher=k.user_id WHERE k.pengguna=0 AND k.webinar_id=${req.params.id}`, (error, result, fields) => {
                if (error) {
                    res.json({ error: true, result: error })
                } else {
                    result.map(item => {
                        data.push(item.sender)
                    })
                    res.json({ error: false, result: data })
                }
            })
        }
    })
}
