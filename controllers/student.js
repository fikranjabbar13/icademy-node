var env = require('../env.json');
var conf = require('../configs/config');
var db = require('../configs/database');
var md5 = require('md5');

var multer = require('multer');
var storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, './public/user');
  },
  filename: (req, file, cb) => {
    console.log(file);
    var filetype = '';
    if(file.mimetype === 'image/gif') {
      filetype = 'gif';
    }
    if(file.mimetype === 'image/png') {
      filetype = 'png';
    }
    if(file.mimetype === 'image/jpeg') {
      filetype = 'jpg';
    }
    if(file.mimetype === 'application/pdf') {
      filetype = 'pdf';
    }
    cb(null, 'img-' + Date.now() + '.' + filetype);
  }
});
var Attachment = multer({storage: storage}).array('attachments', 10);

exports.create = (req, res, next) => {
  let form = {
    nama: req.body.nama,
    email: req.body.email,
    phone: req.body.phone,
    address: req.body.address,
    status: 'candidate',
    company_id: req.body.company_id,
    grade_id: req.body.grade_id,
    major_id: req.body.major_id
  };

  let sql = `INSERT INTO student (nama, email, phone, address, status, company_id, grade_id, major_id) 
    VALUES ('${form.nama}','${form.email}','${form.phone}','${form.address}','${form.status}','${form.company_id}','${form.grade_id}','${form.major_id}')`;
  
  db.query(sql, (error, result, fields) => {
    if(error) {
      res.json({ error: true, result: error });
    } else {
      db.query(`SELECT * FROM student WHERE student_id = '${result.insertId}'`, (error, result, fields) => {
        res.json({ error: false, result: result[0] });
      })
    }
  });
}

exports.getAll = (req, res, next) => {
  db.query(`SELECT * FROM student ORDER BY student_id DESC`, (error, result, fields) => {
    if(error) {
      res.json({ error: true, result: error });
    } else {
      res.json({ error: false, result: result });
    }
  })
}

exports.getOne = (req, res, next) => {
  db.query(`SELECT * FROM student WHERE student_id = '${req.params.student_id}'`, (error, result, fields) => {
    if(error) {
      res.json({ error: true, result: error });
    } else {
      res.json({ error: false, result: result.length ? result[0] : [] });
    }
  })
}

exports.getByCompany = (req, res, next) => {
  db.query(`SELECT * FROM student WHERE company_id = '${req.params.company_id}'`, (error, result, fields) => {
    if(error) {
      res.json({ error: true, result: error });
    } else {
      res.json({ error: false, result: result });
    }
  })
}

exports.update = (req, res, next) => {
  let form = {
    nama: req.body.nama,
    email: req.body.email,
    phone: req.body.phone,
    address: req.body.address,
    company_id: req.body.company_id,
    grade_id: req.body.grade_id,
    major_id: req.body.major_id
  };

  let sql = `UPDATE student SET 
    nama = '${form.nama}', 
    email = '${form.email}', 
    phone = '${form.phone}', 
    address = '${form.address}', 
    company_id = '${form.company_id}', 
    grade_id = '${form.grade_id}', 
    major_id = '${form.major_id}' 
    WHERE student_id = '${req.params.student_id}'`;

  db.query(sql, (error, result, fields) => {
    if(error) {
      res.json({ error: true, result: error });
    } else {
      db.query(`SELECT * FROM student WHERE student_id = '${req.params.student_id}'`, (error, result, fields) => {
        res.json({ error: false, result: result[0] });
      })
    }
  })
}

exports.updateToStudent = (req, res, next) => {

  db.query(`SELECT * FROM student WHERE student_id = '${req.params.student_id}'`, (error, result, fields) => {
    if(error) {
      res.json({ error: true, result: error });
    }

    if(result.length) {
      db.query(`SELECT * FROM user WHERE email = '${result[0].email}'`, (error, result, fields) => {
        if(error) {
          res.json({ error: true, result: error });
        }

        if(result.length) {
          res.json({error: true, result: 'Email sudah digunakan.'});
        } else {
          // create new user

          // update candidate to student
          let sql = `UPDATE student SET status = 'student' WHERE student_id = '${req.params.student_id}'`;
          db.query(sql, (error, result, fields) => {
            if(error) {
              res.json({ error: true, result: error });
            } else {
              db.query(`SELECT * FROM student WHERE student_id = '${req.params.student_id}'`, (error, result, fields) => {
                res.json({ error: false, result: result[0] });
              })
            }
          })
        }
      })
    } else {
      res.json({ error: true, result: 'Siswa tidak ditemukan.'})
    }
  })
}

exports.updateAttachment = (req, res, next) => {
  Attachment(req, res, (err) => {
    if(!req.files) {
      res.json({error: true, result: err});
    } else {
      let attachmentId = '';
      for(let i=0; i<req.files.length; i++) {
        attachmentId += env.APP_URL + '/user/' + req.files[i].filename + ',';
      }
      attachmentId = attachmentId.substring(0, attachmentId.length - 1);
      let formData = {
        attachments: attachmentId
      };

      db.query(`UPDATE student SET attachments = '${formData.attachments}' WHERE student_id = '${req.params.student_id}'`, (error, result, fields) => {
        if(error) {
          res.json({error: true, result: error});
        } else {
          res.json({error: false, result: formData.attachments});
        }
      })
    }
  });
}

exports.delete = (req, res, next) => {
  db.query(`DELETE FROM student WHERE student_id = '${req.params.student_id}'`, (error, result, fields) => {
    if(error) {
      res.json({ error: true, result: error });
    } else {
      res.json({ error: false, result: result });
    }
  })
}