var env = require('../env.json');
var conf = require('../configs/config');
var db = require('../configs/database');

const bbb = require('bigbluebutton-js')
const BBB_URL = env.BBB_URL;
const BBB_KEY = env.BBB_KEY;
const moment = require('moment-timezone');

exports.getUserAgenda = (req, res, next) => {
  db.query(
    `SELECT *, CAST(start AS char) AS string_start,
    CAST(end AS char) AS string_end
    FROM calendar 
    WHERE user_id = '${req.params.user_id}' AND start >= CURDATE() 
    ORDER BY created_at DESC`,
    (error, result, fields) => {
      if (error) {
        res.json({ error: true, result: error });
      } else {
        res.json({ error: false, result: result });
      }
    }
  );
};

exports.getUserAgenda_sidebar_old = (req, res, next) => {
  const asyncs = require('async');

  let start_date = req.query.start_date;
  let end_date = req.query.end_date

  if (!start_date || !end_date) {
    return res.json({ error: true, result: 'Date is required' });
  }
  let append_q = `DATE(start) between '${start_date}' and '${end_date}' `

  // if (types === 'weekly') {
  //   append_q = 'start between date_sub(now(),INTERVAL 1 WEEK) and now() '
  // } else if (types === 'daily') {
  //   if (req.params.date) {
  //     append_q = `DATE(start) = '${req.params.date}' `
  //   } else {
  //     append_q = 'start >= CURDATE() '
  //   }
  // }

  const query = `SELECT *, CAST(start AS char) AS string_start,
  CAST(end AS char) AS string_end
  FROM calendar 
  WHERE user_id = '${req.params.userId}' AND ${append_q} 
  ORDER BY created_at DESC`;
  db.query(
    query,
    async (error, result, fields) => {
      if (error) {
        res.json({ error: true, result: error });
      } else {

        let tmp = [];
        asyncs.eachSeries(result, async (str, cb) => {

          let participant = [];
          if (str.type == 3) { // meeting

            let getListparticipant = await db.query(`
            SELECT 
              distinct u.user_id ,u.name, u.avatar as 'image', 'participant' as 'type'
            FROM liveclass_booking b 
              LEFT JOIN user u ON b.user_id=u.user_id 
            WHERE b.meeting_id='${str.activity_id}'; 
            select 
              distinct a.name ,
              a.voucher as 'user_id',
              null as 'image' ,
              'guest' as 'type'
            from liveclass_booking_tamu a 
            where a.meeting_id = ${str.activity_id};
            `
            );

            if (getListparticipant.length > 0) {
              getListparticipant[0].forEach((get1) => {
                participant.push(get1);
              });
              getListparticipant[1].forEach((get1) => {
                participant.push(get1);
              });
            }
            str.participants = participant;
            str.type_label = 'Meeting';

          } else if (str.type == 4) { // webinar

            let peserta = await db.query(`SELECT w.*, u.name FROM webinar_peserta w JOIN user u ON u.user_id = w.user_id WHERE webinar_id = '${str.activity_id}'`)
            let tamu = await db.query(`SELECT * FROM webinar_tamu WHERE webinar_id='${str.activity_id}'`);
            let participant = [];

            if (peserta.length > 0) {
              peserta.forEach(arg => participant.push(arg))
            }
            if (tamu.length > 0) {
              tamu.forEach(arg => participant.push(arg))
            }

            str.participants = participant;
            str.type_label = 'Webinar';
          }
          tmp.push(str);
          //cb(null);

        }, (err) => {
          console.log(err, "ERR")
          if (err) {
            res.json({ error: true, result: err });
          } else {
            res.json({ error: false, result: tmp });
          }
        });
      }
    }
  );
};

exports.getUserAgenda_sidebar = (req, res) => {

  // function dateFormat(paramsDate) {
  //   var date = paramsDate.split("-");
  //   var resDate = date[2] + "-" + date[1] + "-" + date[0];
  //   return resDate;
  // }

  let start_date = req.query.start_date;
  let end_date = req.query.end_date

  if (!start_date || !end_date) {
    return res.json({ error: true, result: 'Date is required' });
  }
  let append_q = ` between '${start_date}' and '${end_date}' `

  let queryMeeting = `
  SELECT
    lb.id AS id,
    l.class_id AS meeting_id,
    lb.id AS booking_id,
    l.room_name AS title, 
    IF(lb.is_akses=1, u.name, null) AS moderator,
    lb.keterangan as 'description',
    lb.tanggal AS date, 
    lb.timezone,
    lb.offset,
    DATE_FORMAT(lb.date_start,'%Y-%m-%d %H:%i:%s') AS startTime,
    DATE_FORMAT(lb.date_end ,'%Y-%m-%d %H:%i:%s') AS endTime,
    -- CAST(CONCAT(lb.tanggal, ' ', lb.jam_mulai) AS DATETIME) AS startTime, 
    -- CAST(CONCAT(lb.tanggal, ' ', lb.jam_selesai) AS DATETIME) AS endTime, 
    'meeting' AS type
  FROM 
  	liveclass l
    LEFT JOIN liveclass_booking lb ON lb.meeting_id = l.class_id
    LEFT JOIN liveclass_booking_participant lbp ON lbp.booking_id = lb.id
  	LEFT JOIN user u ON lb.moderator = u.user_id
  WHERE 
  	lb.tanggal ${append_q} 
    AND (lb.user_id = '${req.params.userId}' OR lb.moderator = '${req.params.userId}' OR lbp.user_id = '${req.params.userId}')
    AND l.company_id = '${req.params.companyId}' `;

  //return console.log(queryMeeting)

  let queryWebinar = `SELECT w.id AS id, w.judul AS title, w.pembicara, w.start_time AS date, w.start_time AS startTime, w.end_time AS endTime, 'webinar' AS type , w.status 
  FROM webinars w
  LEFT JOIN webinar_peserta p ON w.id=p.webinar_id 
  WHERE 
  DATE(w.start_time) ${append_q} 
  AND w.company_id='${req.params.companyId}' AND w.publish='1' 
  AND (p.user_id='${req.params.userId}' OR (w.sekretaris LIKE '%${req.params.userId}%' OR w.moderator LIKE '%${req.params.userId}%' OR w.pembicara LIKE '%${req.params.userId}%' OR w.owner LIKE '%${req.params.userId}%'))
  GROUP BY w.id`;

  let queryTrainingExam = `SELECT a.id AS id, e.title AS title, c.title AS course, e.start_time AS startTime, e.end_time AS endTime, IF(e.exam=1, 'training exam', 'training quiz') AS type
  FROM training_exam e
  JOIN training_exam_assignee a ON a.exam_id=e.id
  LEFT JOIN training_user tu ON tu.id = a.training_user_id
  LEFT JOIN user u ON u.email = tu.email
  LEFT JOIN training_course c ON c.id = e.course_id
  WHERE e.scheduled = 1 AND  
  DATE(e.start_time) ${append_q} 
  AND u.user_id='${req.params.userId}' 
  AND u.status='active' AND e.status='Active' AND a.status IN ('Open','Start')`;

  let queryTrainingCourse = `SELECT c.id AS id, c.title AS title, c.start_time AS startTime, c.end_time AS endTime, 'training course' AS type, (IF(NOW() BETWEEN c.start_time AND c.end_time != '1' && c.scheduled = 1, '0', '1')) AS on_schedule 
  FROM training_course c
  WHERE c.scheduled = 1 AND 
  DATE(c.start_time) ${append_q} 
  AND c.company_id='${req.params.companyId}' 
  AND c.status='Active'`;

  let data = [];
  db.query(queryMeeting, async (error, result, fields) => {
    if (error) {
      res.json({ error: true, result: error })
    }
    else {
      for (let i = 0; i < result.length; i++) {
        let participant = await db.query(`SELECT u.user_id AS userId, u.avatar AS avatar, u.name AS name, p.start_time 
        FROM user u
        JOIN liveclass_booking_participant p ON u.user_id=p.user_id
        WHERE p.booking_id='${result[i].id}'`);

        let tamu = await db.query(`
          select 
            distinct a.name ,
            a.voucher as 'userId',
            null as 'image' ,
            'guest' as 'type'
          from liveclass_booking_tamu a 
          where a.meeting_id = ${result[i].id};
          `
        );

        let tmp = [];

        let tz = result[i].timezone || 'Asia/Jakarta';

        let times = null;
        if (participant.length) {
          let idx = participant.findIndex((str) => { if (str.start_time && str.userId == req.app.token.user_id) { return true; } else { return false; } });
          if (idx > -1) { times = participant[idx].start_time; }
        }
        result[i].startTime = moment.tz(times ? times : result[i].startTime, tz);
        result[i].endTime = moment.tz(result[i].endTime, tz);

        if (tamu.length > 0) {

          result[i].participant = participant.concat(tamu);
        } else {
          result[i].participant = participant;
        }

        if (result[i].participant.length > 0) {

          result[i].participant.forEach(str => {
            let idx = tmp.findIndex(str2 => str2.userId == str.userId);
            if (idx == -1) {
              tmp.push(str);
            }
          });
        }
        result[i].participant = tmp;

        // CHECKING STATUS
        let api = bbb.api(BBB_URL, BBB_KEY)
        let http = bbb.http
        let checkUrl = api.monitoring.isMeetingRunning(result[i].id)
        let { returncode, running } = await http(checkUrl);
        let runnings = returncode === 'SUCCESS' ? running : false

        if (result[i].participant.length == 0) {
          result[i].status = 'Empty';
        } else if (runnings) {
          result[i].status = 'On Going';
        } else {
          if (result[i].participant.length > 0 && runnings) {
            result[i].status = 'On Going';
          } else {
            result[i].status = `${result[i].participant.length > 0 ? result[i].participant.length : 'No'} Upcoming`;
          }
        }

      }
      data = result;
      db.query(queryWebinar, async (error, result, fields) => {
        if (error) {
          res.json({ error: true, result: error })
        }
        else {
          for (let i = 0; i < result.length; i++) {

            let dataPembicara = result[i].pembicara.split(',');
            let pembicaraUser = await db.query(`SELECT user_id, name FROM user WHERE user_id IN (${dataPembicara})`);
            let pembicaraGuest = await db.query(`SELECT voucher AS user_id, name FROM webinar_tamu WHERE voucher IN (${dataPembicara})`);
            let pembicara = pembicaraUser.concat(pembicaraGuest);

            result[i].pembicara = pembicara.length != 0 ? pembicara : null;

            let participant = await db.query(`SELECT u.user_id AS userId, u.avatar AS avatar, u.name AS name
            FROM user u
            JOIN webinar_peserta p ON u.user_id=p.user_id
            WHERE p.webinar_id='${result[i].id}'`);

            let binartamu = await db.query(`SELECT name, voucher as 'userId', null as avatar FROM webinar_tamu WHERE webinar_id='${result[i].id}'`);

            result[i].participant = participant.concat(binartamu);

            let tmp = [];
            // result[i].participant = participant.concat(tamu);
            result[i].participant.forEach(str => {
              let idx = tmp.findIndex(str2 => str2.userId == str.userId);
              if (idx < 0) {
                tmp.push(str);
              }
            });

            if (result[i].status == 0) { result[i].status = 'Empty' }
            else if (result[i].status == 1) { result[i].status = 'Upcoming' }
            else if (result[i].status == 2) { result[i].status = 'On Going' }
            else if (result[i].status == 3) { result[i].status = 'Finished' }
            else { result[i].status = 'Empty' }

            result[i].participant = tmp;
          }
          data = data.concat(result);
          db.query(queryTrainingExam, async (error, result, fields) => {
            if (error) {
              res.json({ error: true, result: error })
            }
            else {
              data = data.concat(result);
              let isTraining = await db.query(`SELECT tu.level FROM user u JOIN training_user tu ON tu.email = u.email WHERE u.user_id='${req.params.userId}'`);
              if (isTraining.length && isTraining[0].level === 'user') {
                db.query(queryTrainingCourse, async (error, result, fields) => {
                  if (error) {
                    res.json({ error: true, result: error })
                  }
                  else {
                    res.json({ error: false, result: data.concat(result), request: { start_date: start_date, end_date: end_date } })
                  }
                })
              }
              else {
                res.json({ error: false, result: data, request: { start_date: start_date, end_date: end_date } })
              }
            }
          })
        }
      })
    }
  })
};

exports.createUserAgenda = (req, res) => {
  let { user_id } = req.params
  let { type, activity_id, description, destination, start, end } = req.body

  let check = "SELECT * FROM calendar WHERE user_id = ? AND type = ? AND activity_id = ? AND start = ? AND end = ?";
  db.query(check, [user_id, type, activity_id, start, end], (error, result) => {
    if (error) {
      res.json({ error: true, result: error });
    }
    else {
      if (result.length === 1) {
        res.json({ error: true, result: result, message: 'Already added to calendar.' });
      }
      else {
        let sql = "INSERT INTO calendar (user_id, type, activity_id, description, destination, start, end) VALUES (?)"
        let dara = [[user_id, type, activity_id, description, destination, start, end]]
        db.query(sql, dara, (error, result) => {
          if (error) {
            res.json({ error: true, result: error });
          }
          else {
            res.json({ error: false, result: result });
          }
        })
      }
    }
  })
}