var env = require("../env.json");
var conf = require("../configs/config");
var db = require("../configs/database");
var fs = require('fs');
const asyncs = require('async');

var multer = require('multer');
const { strictEqual } = require("assert");
var storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, './public/company');
    },
    filename: (req, file, cb) => {
        cb(null, 'excel-' + Date.now() + '-' + file.originalname);
    }
});
var uploadExcel = multer({ storage: storage }).single('excel');

/**
 * create webinar test
 * @param {Number} req.body.id  id webinar
 * @param {Array.<{tanya: String, a: String, b: String, c: String, ...}>} req.body.webinar_poll
 * @param {Boolean} res.body.error
 * @param {String} res.body.result
 */
let create = (data, callback) => {
    //console.log('agus data : ', data)
    let arr_questions = ['tanya', 'jawab'];
    let arr_option = ['a', 'b', 'c', 'd', 'e'];
    let query_question = [];
    let query_option = [];

    // build data or validate
    //let questions = [data.id, elem.tanya, data.jenis, elem.jawab, 'Active'];
    data.webinar_poll.forEach(str => {
        let tmp = [data.id, data.jenis, 'Active'];

        arr_questions.forEach(key => {
            tmp.push(str[key])
        });

        query_question.push(tmp);
    });

    data.webinar_poll.forEach(str => {
        tmp = [];

        arr_option.forEach(key => {
            tmp.push([0, key, str[key]])
        });
        query_option.push(tmp);
    });

    // return console.log(query_option, 9090)
    // insert
    db.getConnection((err, con) => {
        con.beginTransaction(err => {
            let x = 0;
            asyncs.eachSeries(query_question, (str, next) => {
                if (err) {
                    con.rollback(function () {
                        con.release();
                        console.log(err, 'err 1')
                        return next({ error: true, result: "failed create webinar poll" });
                    });
                } else {

                    con.query(
                        `INSERT INTO webinar_poll_questions (webinar_id, jenis, status, pertanyaan) VALUES (?,?,?,?);`,
                        str,
                        (err2, res) => {
                            if (err2) {
                                con.rollback(function () {
                                    con.release();
                                    console.log(err2, x, 'rollback err 2')
                                    return next({ error: true, result: "failed create webinar poll" });
                                    //Failure
                                });
                            } else {

                                let values = '';
                                query_option[x].forEach(keys => {
                                    keys[0] = res.insertId;

                                    con.query(
                                        `INSERT INTO webinar_poll_options (webinar_poll_id, answer) VALUES(?,?) `,
                                        keys
                                    );
                                })

                                //con.rollback();
                                //console.log(res, query_option[x], x, values, 'AFTER INSERT quest')

                                x++;
                                next(null);

                            }
                        });
                }
            }, (e) => {
                if (e) {
                    return callback(e);
                } else {
                    con.commit(function (err) {
                        if (err) {
                            con.rollback();
                            return callback({ error: true, result: "failed create webinar test" });
                        } else {
                            return callback({ error: false, result: "success create webinar test" });
                        }
                    });
                }
            })
        });

    });
    // data.webinar_poll.map(async (elem) => {
    //     let questions = [data.id, elem.tanya, data.jenis, elem.jawab, 'Active'];
    //     console.log('agus : ', questions)
    //     let options = [];

    //     let insert_questions = async () => {
    //         try {
    //             return await db.query(
    //                 `INSERT INTO webinar_poll_questions (webinar_id, pertanyaan, jenis, jawab, status)
    //                             VALUES (?);`,
    //                 [questions]
    //             );
    //         } catch (error) {
    //             return callback({ error: true, result: await error.message });
    //         }
    //     };

    //     let last_insert_id = await insert_questions();

    //     await Object.keys(elem).map((value, id) => {
    //         if (value !== 'tanya' && value !== 'jawab' && value !== 'id' && elem[value] !== "") {
    //             options.push([last_insert_id.insertId, value, elem[value]]);
    //         }
    //     });

    //     db.query(
    //         `UPDATE webinars SET ${data.jenis === 0 ? 'waktu_pretest' : 'waktu_posttest'}=${data.waktu} WHERE id='${data.id}'`
    //     );

    //     let insert_options = async () => {
    //         try {
    //             await db.query(
    //                 `INSERT INTO webinar_poll_options (webinar_poll_id, options, answer)
    //                             VALUES ?;`,
    //                 [options]
    //             );
    //         } catch (error) {
    //             return callback({ error: true, result: await error.message });
    //         }
    //     };

    //     await insert_options();
    // });
    // return callback({ error: false, result: "success create webinar test" });
};

exports.create = async (req, res) => {
    create(req.body, (result) => res.json(result));
};

/**
 * delete webinar test
 * @param {String} req.params.idÍ
 * @param {Boolean} res.body.error
 * @param {String} res.body.result
 */
let remove = (data, callback) => {
    db.query(
        `UPDATE webinar_poll_questions SET status='Inactive'
        WHERE webinar_id=${data.id}
        AND jenis=${data.jenis};`,
        (error, result) => {
            if (error) {
                return callback({ error: true, result: error.message });
            } else {
                if (result.affectedRows !== 0) {
                    return callback({ error: false, result: "success delete webinar test" });
                } else {
                    return callback({ error: true, result: "failed delete webinar test" });
                }
            }
        }
    );
};

exports.delete = (req, res) => {
    remove(req.params, (result) => res.json(result));
};

exports.update = (req, res) => {
    remove(req.body, (result) => {
        if (result.error) {
            res.json({ error: true, result: "failed update webinar test" });
        } else {
            create(req.body, (callback) => {
                if (callback.error) {
                    res.json({ error: true, result: "failed update webinar test" });
                } else {
                    res.json({ error: false, result: "success update webinar test" });
                }
            });
        }
    });
};

exports.readPeserta = (req, res) => {
    let { jenis, user_id, id } = req.params;
    if (jenis == 'essay') {
        db.query(`SELECT
                webinar_essay_answer.*, If(webinar_tamu.name is not null, webinar_tamu.name, user.name) as name
            FROM 
                webinar_essay_answer
                JOIN webinars ON webinars.id = webinar_essay_answer.webinar_id
                LEFT JOIN user ON user.user_id = webinar_essay_answer.user_id
                LEFT JOIN webinar_tamu ON webinar_tamu.voucher = webinar_essay_answer.user_id
            WHERE 
                webinar_essay_answer.user_id = ?
                AND webinar_essay_answer.webinar_id = ?`, [user_id, id], (error, result) => {
            if (error) {
                res.json({ error: true, result: error.message });
            } else {
                res.json({ error: false, result: result, terjawab: result.length > 0 ? true : false });
            }
        })
    } else {
        db.query(
            `SELECT t1.*, t1.id AS question_id, t2.*, t2.id AS option_id FROM webinar_poll_questions t1
            INNER JOIN webinar_poll_options t2
            ON t1.id = t2.webinar_poll_id
            WHERE t1.webinar_id = ${req.params.id}
            AND t1.status ='Active'
            AND t1.jenis = ${req.params.jenis}
            ORDER BY t1.id ASC;`,
            (error, result) => {
                if (error) {
                    res.json({ error: true, result: error.message });
                } else {
                    let group = result.reduce((accumulator, current) => {
                        if (!accumulator[current.webinar_poll_id]) {
                            accumulator[current.webinar_poll_id] = {};
                            accumulator[current.webinar_poll_id]["tanya"] = current.pertanyaan;
                        }
                        accumulator[current.webinar_poll_id][current.options] = [current.option_id, current.answer];
                        accumulator[current.webinar_poll_id]["question_id"] = current.question_id;

                        return accumulator;
                    }, {});

                    let kuesioner = [];
                    Object.keys(group).map((elem) => {
                        kuesioner.push(group[elem]);
                    });
                    db.query(
                        `SELECT COUNT(a.id) AS jawaban FROM webinar_poll_answer a JOIN webinar_poll_questions q ON a.webinar_questions=q.id WHERE a.user_id='${req.params.user_id}' AND a.webinar_id='${req.params.id}' AND q.jenis='${req.params.jenis}'`,
                        (error, result) => {
                            let jawaban = result[0].jawaban;
                            db.query(
                                `SELECT COUNT(q.id) AS pertanyaan FROM webinar_poll_questions q WHERE q.webinar_id='${req.params.id}' AND q.jenis='${req.params.jenis}'`,
                                (error, result) => {
                                    if (error) { res.json({ error: true, result: error }) }
                                    let pertanyaan = result[0].pertanyaan;
                                    db.query(
                                        `SELECT ${req.params.jenis === '0' ? 'waktu_pretest' : 'waktu_posttest'} AS waktu FROM webinars WHERE id='${req.params.id}'`,
                                        (error, result) => {
                                            if (error) { res.json({ error: true, result: error }) }
                                            res.json({ error: false, result: kuesioner, waktu: result[0].waktu, enable: pertanyaan > 0 ? true : false, terjawab: jawaban > 0 ? true : false });
                                        })
                                })
                        })
                }
            }
        );
    }
};
exports.read = (req, res) => {
    db.query(
        `SELECT t1.id as idpertanyaan, t1.*, t2.* FROM webinar_poll_questions t1
        INNER JOIN webinar_poll_options t2
        ON t1.id = t2.webinar_poll_id
        WHERE t1.webinar_id = ${req.params.id}
        AND t1.status = 'Active'
        AND t1.jenis = ${req.params.jenis}
        ORDER BY t1.id ASC;`,
        (error, result) => {
            if (error) {
                res.json({ error: true, result: error.message });
            } else {
                let group = result.reduce((accumulator, current) => {
                    if (!accumulator[current.webinar_poll_id]) {
                        accumulator[current.webinar_poll_id] = {};
                        accumulator[current.webinar_poll_id]["tanya"] = current.pertanyaan;
                        accumulator[current.webinar_poll_id]["jawab"] = current.jawab;
                    }
                    accumulator[current.webinar_poll_id]["id"] = current.idpertanyaan;
                    accumulator[current.webinar_poll_id][current.options] = current.answer;

                    return accumulator;
                }, {});

                let kuesioner = [];
                Object.keys(group).map((elem) => {
                    kuesioner.push(group[elem]);
                });

                db.query(
                    `SELECT ${req.params.jenis === '0' ? 'waktu_pretest' : 'waktu_posttest'} AS waktu FROM webinars WHERE id='${req.params.id}'`,
                    (error, result) => {
                        if (error) { res.json({ error: true, result: error }) }
                        res.json({ error: false, result: kuesioner, waktu: result[0].waktu });
                    })
            }
        }
    );
};

exports.webinarEssay = (req, res) => {
    const { id, user_id, answer } = req.body
    let sql = `SELECT * FROM webinar_essay_answer WHERE webinar_id = ? AND user_id = ?`
    db.query(sql, [id, user_id], (error, result) => {
        if (error) {
            res.json({ error: true, result: error.message });
        } else {
            if (result.length > 0) {
                let sql = `UPDATE webinar_essay_answer SET answer = ? WHERE webinar_id = ? AND user_id = ?`
                db.query(sql, [answer, id, user_id], (error, result) => {
                    if (error) {
                        res.json({ error: true, result: error.message });
                    } else {
                        res.json({ error: false, result: result });
                    }
                })
            } else {
                let sql = `INSERT INTO webinar_essay_answer (answer, webinar_id, user_id) VALUES (?)`
                db.query(sql, [[answer, id, user_id]], (error, result) => {
                    if (error) {
                        res.json({ error: true, result: error.message });
                    } else {
                        res.json({ error: false, result: result });
                    }
                })
            }
        }
    })
}

/**
 * create webinar test
 * @param {Number} req.body.id  id webinar
 * @param {Array.<{questions_id: String, options_id: String}>} req.body.webinar_poll
 * @param {Boolean} res.body.error
 * @param {String} res.body.result
 */
exports.input = async (req, res) => {
    let data = [];
    if (req.body.webinar_poll.length >= 1) {
        await req.body.webinar_poll.map((elem) => {
            data.push([req.body.id, req.body.user_id, req.body.pengguna, elem.questions_id, elem.options_id]);
        });
        await db.query(
            `INSERT INTO webinar_poll_answer
                (webinar_id, user_id, pengguna, webinar_questions, webinar_options)
            VALUES ?;`,
            [data],
            (error, result) => {
                if (error) {
                    res.json({ error: true, result: error.message });
                } else {
                    res.json({ error: false, result: "success input webinar test" });
                }
            }
        );
    }
    else {
        res.json({ error: false, result: "success input webinar test tanpa menjawab" });
    }
};

// exports.result = (req, res) => {
//     db.query(
//         `SELECT t2.id, t2.pertanyaan, t3.options, t3.answer, count(t3.answer) AS count
//         FROM kuesioner_answer t1
//         INNER JOIN kuesioner_questions t2
//         ON t1.webinar_questions = t2.id
//         INNER JOIN kuesioner_options t3
//         ON t1.webinar_options = t3.id
//         WHERE t1.webinar_id = '${req.params.id}'
//         GROUP BY t2.id, t3.id;`,
//         (error, result) => {
//             if (error) {
//                 res.json({ error: true, result: error.message });
//             } else {
//                 let group = result.reduce((accumulator, current) => {
//                     if (!accumulator[current.id]) {
//                         accumulator[current.id] = {};
//                         accumulator[current.id]["tanya"] = current.pertanyaan;
//                     }
//                     accumulator[current.id][current.options] = current.answer;
//                     accumulator[current.id]["count"] = current.count;

//                     return accumulator;
//                 }, {});

//                 let kuesioner = [];
//                 Object.keys(group).map((elem) => {
//                     kuesioner.push(group[elem]);
//                 });

//                 res.json({ error: false, result: kuesioner });
//             }
//         }
//     );
// };

exports.result = (req, res) => {
    db.query(
        `SELECT a.id, a.jenis, b.id
        FROM webinar_poll_questions a
        INNER JOIN webinar_poll_options b
        ON a.id = b.webinar_poll_id
        AND a.jawab = b.options
        AND a.status = 'Active'
        WHERE a.webinar_id = '${req.params.id}';
        
        SELECT a.user_id, IF(a.pengguna, d.name, e.name) AS name, a.pengguna, b.jenis, a.webinar_options, c.id
        FROM webinar_poll_answer a 
        INNER JOIN webinar_poll_questions b
        ON a.webinar_questions = b.id
        INNER JOIN webinar_poll_options c
        ON b.id = c.webinar_poll_id AND b.jawab = c.options
        LEFT JOIN user d
        ON a.user_id = d.user_id
        LEFT JOIN webinar_tamu e
        ON a.user_id = e.voucher
        WHERE a.webinar_id = '${req.params.id}'`,

        /*`SELECT IF(a.jenis = 0, @jenis0 := COUNT(a.id), @jenis1 := COUNT(a.id)), a.jenis 
        FROM webinar_poll_questions a 
        WHERE a.webinar_id = '${req.params.id}'
        GROUP BY a.jenis;
        
        SELECT a.webinar_id, b.jenis, a.user_id, u.name, IF(b.jenis = 0, (COUNT(a.id)/@jenis0)*100, (COUNT(a.id)/@jenis1)*100) AS 'betul'
        FROM webinar_poll_answer a
        INNER JOIN (
            SELECT user_id, name FROM user
            UNION
            SELECT voucher AS user_id, name FROM webinar_tamu
        ) u
        ON u.user_id = a.user_id
        INNER JOIN webinar_poll_questions b
        ON a.webinar_questions = b.id
        INNER JOIN webinar_poll_options c
        ON a.webinar_options = c.id
        AND c.options = b.jawab
        WHERE a.webinar_id = '${req.params.id}'
        GROUP BY b.jenis, a.user_id
        ORDER BY b.jenis;`,*/
        async (error, result) => {
            if (error) {
                res.json({ error: true, result: error.message });
            } else {
                console.log(result)
                let c = result[0].reduce((accumulator, current) => {
                    if (current.jenis === 0) {
                        accumulator.pretest = accumulator.pretest + 1;
                    } else if (current.jenis === 1) {
                        accumulator.posttest = accumulator.posttest + 1;
                    };

                    return accumulator;
                }, { pretest: 0, posttest: 0 });

                let b = result[1].reduce((accumulator, current) => {
                    if (!accumulator[current.user_id]) {
                        accumulator[current.user_id] = {};
                        accumulator[current.user_id]['user_id'] = current.user_id;
                        accumulator[current.user_id]['name'] = current.name;
                        if (current.jenis === 0) {
                            accumulator[current.user_id]['pretest'] = { benar: 0, salah: 0, total: c.pretest, nilai: 0 };
                            accumulator[current.user_id]['posttest'] = { benar: 0, salah: 0, total: c.posttest, nilai: 0 }
                            if (current.webinar_options === current.id) {
                                accumulator[current.user_id]['pretest']['benar'] = accumulator[current.user_id]['pretest']['benar'] + 1;
                            } else {
                                accumulator[current.user_id]['pretest']['salah'] = accumulator[current.user_id]['pretest']['salah'] + 1;
                            };
                        } else if (current.jenis === 1) {
                            accumulator[current.user_id]['pretest'] = { benar: 0, salah: 0, total: c.pretest, nilai: 0 };
                            accumulator[current.user_id]['posttest'] = { benar: 0, salah: 0, total: c.posttest, nilai: 0 };
                            if (current.webinar_options === current.id) {
                                accumulator[current.user_id]['posttest']['benar'] = accumulator[current.user_id]['posttest']['benar'] + 1;
                            } else {
                                accumulator[current.user_id]['posttest']['salah'] = accumulator[current.user_id]['posttest']['salah'] + 1;
                            };
                        }
                    }
                    else {
                        if (current.jenis === 0) {
                            if (current.webinar_options === current.id) {
                                accumulator[current.user_id]['pretest']['benar'] = accumulator[current.user_id]['pretest']['benar'] + 1;
                            } else {
                                accumulator[current.user_id]['pretest']['salah'] = accumulator[current.user_id]['pretest']['salah'] + 1;
                            };
                        } else if (current.jenis === 1) {
                            if (current.webinar_options === current.id) {
                                accumulator[current.user_id]['posttest']['benar'] = accumulator[current.user_id]['posttest']['benar'] + 1;
                            } else {
                                accumulator[current.user_id]['posttest']['salah'] = accumulator[current.user_id]['posttest']['salah'] + 1;
                            };
                        }
                    }

                    accumulator[current.user_id]['pretest'].nilai = (accumulator[current.user_id]['pretest']['benar'] / accumulator[current.user_id]['pretest'].total) * 100;
                    accumulator[current.user_id]['posttest'].nilai = (accumulator[current.user_id]['posttest']['benar'] / accumulator[current.user_id]['posttest'].total) * 100;
                    accumulator[current.user_id]['selisih'] = accumulator[current.user_id]['posttest'].nilai - accumulator[current.user_id]['pretest'].nilai


                    accumulator[current.user_id]['pretest'].nilai = accumulator[current.user_id]['pretest'].nilai ? accumulator[current.user_id]['pretest'].nilai : 0;
                    accumulator[current.user_id]['posttest'].nilai = accumulator[current.user_id]['posttest'].nilai ? accumulator[current.user_id]['posttest'].nilai : 0;
                    accumulator[current.user_id]['selisih'] = accumulator[current.user_id]['selisih'] ? accumulator[current.user_id]['selisih'] : 0;
                    return accumulator;
                }, {});

                return res.json({ error: false, result: await b });

                // let hasil = result[1].reduce((accumulator, current) => {
                //     if(!accumulator[current.user_id]) {
                //         accumulator[current.user_id] = {};
                //         accumulator[current.user_id]['user_id'] = current.user_id;
                //         accumulator[current.user_id]['name'] = current.name;
                //         if(current.jenis === 0) {
                //             accumulator[current.user_id]['pretest'] = current.betul;
                //             accumulator[current.user_id]['posttest'] = 0;
                //         } else {
                //             accumulator[current.user_id]['pretest'] = 0;
                //             accumulator[current.user_id]['posttest'] = current.betul;
                //         };
                //     };

                //     if(current.jenis === 0) {
                //         accumulator[current.user_id]['pretest'] = current.betul;
                //         accumulator[current.user_id]['selisih'] = accumulator[current.user_id]['posttest'] - accumulator[current.user_id]['pretest'];
                //     } else {
                //         accumulator[current.user_id]['posttest'] = current.betul;
                //         accumulator[current.user_id]['selisih'] = accumulator[current.user_id]['posttest'] - accumulator[current.user_id]['pretest'];
                //     };

                //     return accumulator;
                // }, {})

                // res.json({ error: false, result: await hasil });
            }
        }
    );
};

// exports.result = (req, res) => {
//     db.query(
//         `SELECT b.id AS question_id, b.pertanyaan, c.id AS answer_id, c.answer, a.user_id, IF(a.pengguna,d.name,e.name) AS name
//         FROM webinar_poll_answer a
//         INNER JOIN webinar_poll_questions b
//         ON a.webinar_questions = b.id
//         INNER JOIN webinar_poll_options c
//         ON a.webinar_options = c.id
//         LEFT JOIN user d
//         ON a.user_id = d.user_id
//         LEFT JOIN webinar_tamu e
//         ON a.user_id = e.id
//         WHERE a.webinar_id = '${req.params.id}'
//         AND b.jenis = '${req.params.jenis}'
//         ORDER BY user_id, question_id`,
//         async (error, result) => {
//             console.log(result)
//             if (error) {
//                 res.json({ error: true, result: error.message });
//             } else {
//                 let group = await result.reduce(
//                     (accumulator, current) => {
//                         if (!accumulator.jawaban[current.user_id]) {
//                             accumulator.jawaban[current.user_id] = {};
//                             accumulator.jawaban[current.user_id]["nama"] = current.name;
//                             accumulator.jawaban[current.user_id]["jawaban"] = [];
//                         }

//                         if (!accumulator.pertanyaan[current.question_id]) {
//                             accumulator.pertanyaan[current.question_id] = {};
//                             accumulator.pertanyaan[current.question_id]["pertanyaan"] = current.pertanyaan;
//                         }

//                         accumulator.jawaban[current.user_id]["jawaban"].push(current.answer);

//                         return accumulator;
//                     },
//                     { pertanyaan: {}, jawaban: {} }
//                 );

//                 let pertanyaan = [],
//                     jawaban = [];
//                 Object.keys(await group.pertanyaan).map((elem) => {
//                     pertanyaan.push(group.pertanyaan[elem].pertanyaan);
//                 });
//                 Object.keys(await group.jawaban).map((elem) => {
//                     jawaban.push(group.jawaban[elem]);
//                 });

//                 res.json({ error: false, result: { pertanyaan: await pertanyaan, jawaban: await jawaban } });
//             }
//         }
//     );
// };

exports.import = (req, res, next) => {
    uploadExcel(req, res, (err) => {
        if (!req.file) {
            res.json({ error: true, result: err });
        } else {

            // create class Excel
            var Excel = require('exceljs');
            var wb = new Excel.Workbook();
            var path = require('path');
            var filePath = path.resolve(__dirname, '../public/company/' + req.file.filename);

            wb.xlsx.readFile(filePath).then(async function () {

                var sh = wb.getWorksheet("Sheet1");
                let tmp = [];

                sh.eachRow((row, rowNumber) => {

                    if (rowNumber > 1) {
                        tmp.push(row.values);
                    }
                });

                let opsi = ["a", "b", "c", "d", "e"];
                let tempArray = [];
                let q_tanya = [];
                let q_opsi = [];


                db.getConnection((err_con, con) => {
                    if (err_con) {
                        con.release();
                        console.log(err, 'err 1')
                        return cb({ error: true, result: "failed import webinar test, EDB-CONNECT" });
                    }

                    con.beginTransaction(err_trx => {
                        if (err_trx) {
                            con.rollback(function () {
                                con.release();
                                console.log(err_trx, 'err 2')
                                return res.json({ error: true, result: "failed import webinar test EDB-TRX" });
                            });
                        }

                        asyncs.eachSeries(tmp, (str, cb) => {

                            let x = 0;
                            let builds = {
                                webinarId: req.body.webinarId,
                                kuesionerId: null,
                                pertanyaan: str[2],
                                jawaban: str[3],
                                a: null,
                                aId: null,
                                b: null,
                                bId: null,
                                c: null,
                                cId: null,
                                d: null,
                                dId: null,
                                e: null,
                                eId: null,
                            };

                            // str[2] pertanyaan
                            // str[3] jawaban
                            // str[4-n] pilihan
                            db.query(
                                `INSERT INTO webinar_poll_questions (webinar_id, jenis, status, pertanyaan) VALUES (?,?,?,?);`,
                                [req.body.webinarId, req.body.jenis, 'Active', str[2], str[3]],
                                (err_quest, res_quest) => {
                                    if (err_quest) {
                                        con.rollback(function () {
                                            con.release();
                                            console.log(err_quest, 'err 3')
                                            return cb({ error: true, result: "failed import webinar test EDB-INSERT" });
                                        });
                                    }

                                    try {

                                        builds.kuesionerId = res_quest.insertId;

                                        for (var i = 4; i < str.length; i++) {

                                            let res_opsi = db.query(
                                                `INSERT INTO webinar_poll_options (webinar_poll_id, answer) VALUE(?,?);`,
                                                [res_quest.insertId, opsi[x], str[i]],
                                            );
                                            builds[opsi[x]] = str[i];
                                            builds[`${opsi[x]}Id`] = res_opsi.insertId;
                                            x++;
                                        }
                                        tempArray.push(builds);
                                        return cb(null);

                                    } catch (e) {
                                        con.rollback(function () {
                                            con.release();
                                            console.log(e, 'err 3')
                                            return cb({ error: true, result: "failed import webinar test EDB-INSERT" });
                                        });
                                    }
                                }
                            );

                        }, (e) => {
                            if (e) { return res.json(e); }

                            con.commit(function (err_commit) {
                                if (err_commit) {
                                    con.rollback();
                                    return res.json({ error: true, result: "failed import webinar test" });
                                } else {
                                    return res.json({ error: false, result: tempArray });
                                }
                            });
                        });
                    });
                });
                fs.unlinkSync(filePath);
            });

        }
    })
}

exports.import_old = (req, res, next) => {
    uploadExcel(req, res, (err) => {
        if (!req.file) {
            res.json({ error: true, result: err });
        } else {

            // create class Excel
            var Excel = require('exceljs');
            var wb = new Excel.Workbook();
            var path = require('path');
            var filePath = path.resolve(__dirname, '../public/company/' + req.file.filename);

            wb.xlsx.readFile(filePath).then(async function () {

                var sh = wb.getWorksheet("Sheet1");

                //Get all the rows data [1st and 2nd column]    
                var webinarId = req.body.webinarId;
                var jenis = req.body.jenis;
                // var tempArray = [];
                for (i = 2; i <= sh.rowCount; i++) {

                    console.log('==========================================================')

                    var querySoal = `INSERT INTO webinar_poll_questions (webinar_id, jenis, pertanyaan, jawab, status) VALUES ('${webinarId}', '${jenis}', '${sh.getRow(i).getCell(2).value}', '${sh.getRow(i).getCell(3).value}', 'Active')`;
                    console.log(querySoal)

                    // proses insert soal
                    const result = await db.query(querySoal);

                    // create variable
                    let aId = { insertId: null }, bId = { insertId: null }, cId = { insertId: null }, dId = { insertId: null }, eId = { insertId: null };

                    // pilihan jawaban A
                    if (sh.getRow(i).getCell(4).value) {
                        var queryJawab = `INSERT INTO webinar_poll_options (webinar_poll_id, options, answer) VALUES ('${result.insertId}', 'a', '${sh.getRow(i).getCell(4).value}')`;
                        console.log(queryJawab)
                        aId = await db.query(queryJawab)
                    }

                    // B
                    if (sh.getRow(i).getCell(5).value) {
                        var queryJawab = `INSERT INTO webinar_poll_options (webinar_poll_id, options, answer) VALUES ('${result.insertId}', 'b', '${sh.getRow(i).getCell(5).value}')`;
                        console.log(queryJawab)
                        bId = await db.query(queryJawab)
                    }

                    // C
                    if (sh.getRow(i).getCell(6).value) {
                        var queryJawab = `INSERT INTO webinar_poll_options (webinar_poll_id, options, answer) VALUES ('${result.insertId}', 'c', '${sh.getRow(i).getCell(6).value}')`;
                        console.log(queryJawab)
                        cId = await db.query(queryJawab)
                    }

                    // D
                    if (sh.getRow(i).getCell(7).value) {
                        var queryJawab = `INSERT INTO webinar_poll_options (webinar_poll_id, options, answer) VALUES ('${result.insertId}', 'd', '${sh.getRow(i).getCell(7).value}')`;
                        console.log(queryJawab)
                        dId = await db.query(queryJawab)
                    }

                    // E
                    if (sh.getRow(i).getCell(8).value) {
                        var queryJawab = `INSERT INTO webinar_poll_options (webinar_poll_id, options, answer) VALUES ('${result.insertId}', 'e', '${sh.getRow(i).getCell(8).value}')`;
                        console.log(queryJawab)
                        eId = await db.query(queryJawab)
                    }
                    console.log('==========================================================')

                    tempArray.push({
                        webinarId: webinarId,
                        kuesionerId: result.insertId,
                        pertanyaan: sh.getRow(i).getCell(2).value,
                        jawaban: sh.getRow(i).getCell(3).value,
                        a: sh.getRow(i).getCell(4).value,
                        aId: sh.getRow(i).getCell(4).value ? aId.insertId : null,

                        b: sh.getRow(i).getCell(5).value,
                        bId: sh.getRow(i).getCell(5).value ? bId.insertId : null,

                        c: sh.getRow(i).getCell(6).value,
                        cId: sh.getRow(i).getCell(6).value ? cId.insertId : null,

                        d: sh.getRow(i).getCell(7).value,
                        dId: sh.getRow(i).getCell(7).value ? dId.insertId : null,

                        e: sh.getRow(i).getCell(8).value,
                        eId: sh.getRow(i).getCell(8).value ? eId.insertId : null,
                    });
                }

                fs.unlinkSync(filePath);

                res.json({ error: false, result: tempArray });
            });

        }
    })
}

exports.deleteByIdPertanyaan = (req, res) => {
    // delete on table webinar options
    let sql = `UPDATE webinar_poll_questions SET status='Inactive' WHERE id = ?`;
    db.query(sql, [req.params.id], (error, rows) => {
        if (error) res.json({ error: true, result: error })

        res.json({ error: false, result: rows })
    })
}

exports.resultByUser = (req, res) => {
    let { jenis, id } = req.params
    if (jenis == 'essay') {
        let sql = `SELECT
                webinar_essay_answer.*, If(webinar_tamu.name is not null, webinar_tamu.name, user.name) as name
            FROM 
                webinar_essay_answer
                JOIN webinars ON webinars.id = webinar_essay_answer.webinar_id
                LEFT JOIN user ON user.user_id = webinar_essay_answer.user_id
                LEFT JOIN webinar_tamu ON webinar_tamu.voucher = webinar_essay_answer.user_id
            WHERE 
                webinar_essay_answer.webinar_id = ?`
        db.query(sql, [id], (error, result) => {
            if (error) {
                res.json({ error: true, result: error.message });
            } else {
                res.json({ error: false, result: result });
            }
        })
    } else {
        db.query(`SELECT 
        b.id, b.pertanyaan, c.options, c.answer, d.options AS jawaban, b.jawab AS jawaban_benar
        FROM webinar_poll_answer a
        INNER JOIN webinar_poll_options d
        ON a.webinar_options = d.id
        INNER JOIN webinar_poll_questions b
        ON a.webinar_questions = b.id
        INNER JOIN webinar_poll_options c
        ON b.id = c.webinar_poll_id
        WHERE b.webinar_id = ?
        AND b.jenis = ?
        AND a.user_id = ?;`, [req.params.id, req.params.jenis, req.params.user], (error, result) => {
            if (error) {
                res.json({ error: true, result: error.message });
            } else {
                let hasil = result.reduce((accumulator, current) => {
                    if (!accumulator[current.id]) {
                        accumulator[current.id] = {};
                        accumulator[current.id]['pertanyaan'] = current.pertanyaan;
                        accumulator[current.id]['jawaban'] = current.jawaban;
                        accumulator[current.id]['jawaban_benar'] = current.jawaban_benar;
                        accumulator[current.id]['options'] = [];
                    }

                    accumulator[current.id]['options'].push({ options: current.options, answer: current.answer });

                    return accumulator;
                }, {});

                let benar = 0;
                let salah = 0;
                let arr = [];
                Object.keys(hasil).map(e => {
                    if (hasil[e].jawaban === hasil[e].jawaban_benar) {
                        benar++;
                    } else {
                        salah++;
                    }

                    arr.push(hasil[e]);
                });

                res.json({ error: false, result: { list: arr, benar: benar, salah: salah, nilai: ((benar / (benar + salah)) * 100).toFixed(2) } });
            };
        })
    }
}
