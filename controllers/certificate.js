var env = require('../env.json');
var conf = require('../configs/config');
var db = require('../configs/database');

var multer = require('multer');
var storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, './public/signature');
  },
  filename: (req, file, cb) => {
    var filetype = '';
    if (file.mimetype === 'image/gif') {
      filetype = 'gif';
    }
    if (file.mimetype === 'image/png') {
      filetype = 'png';
    }
    if (file.mimetype === 'image/jpeg') {
      filetype = 'jpg';
    }
    cb(null, 'img-' + Date.now() + '.' + filetype);
  },
});
var uploadSignature = multer({ storage: storage }).fields([
  { name: 'signature_1', maxCount: 1 },
  { name: 'signature_2', maxCount: 1 },
]);

exports.createCertificate = (req, res, next) => {
  /**
   * data format for create certificates
   *
   * @param {string}  title  title of certificate
   * @param {number}  template  id of template certificate
   * @param {number}  type_activity id of type activity (1=course, 2=forum, 3=meeting)
   * @param {number}  activity_id activity id of each database
   * @param {string}  signature_1 signature image 1
   * @param {string}  signature_name_1 name of signature image 1
   * @param {string}  signature_2 signature image 2
   * @param {string}  signature_name_2 name of signature image 2
   * @param {string[]}  user_id array of user id that got the certificate
   */

  uploadSignature(req, res, (err) => {
    const formData = {
      title: req.body.title,
      template: Number(req.body.template),
      type_activity: Number(req.body.type_activity),
      activity_id: Number(req.body.activity_id),
      signature_1: !req.files['signature_1']
        ? null
        : `${env.APP_URL}/signature/${req.files['signature_1'][0].filename}`,
      signature_name_1: req.body.signature_name_1,
      signature_2: !req.files['signature_2']
        ? null
        : `${env.APP_URL}/signature/${req.files['signature_2'][0].filename}`,
      signature_name_2: req.body.signature_name_2,
    };
    let listUser = JSON.parse(req.body.listUser);

    if (listUser.length !== 0) {
      db.query(
        `INSERT INTO certificate SET ?;`,
        formData,
        (error, result, fields) => {
          if (error) {
            res.json({ error: true, result: error });
          } else {
            const id = result.insertId;
            listUser = listUser.map((elem) => {
              return [id, elem];
            });
            db.query(
              `INSERT INTO certificate_user (certificate_id, user_id) VALUES ?;`,
              [listUser],
              (error, result, fields) => {
                if (error) {
                  res.json({ error: true, result: error });
                } else {
                  res.json({ error: false, result: result[0] });
                }
              }
            );
          }
        }
      );
    } else {
      res.json({ error: true, result: 'data not valid' });
    }
  });
};

exports.readCertificate = (req, res, next) => {
  /**
   * data format for read certificates
   *
   * @param {number}  type_activity
   * @param {number}  activity_id
   */
  const formData = {
    type_activity: req.params.type_activity,
    activity_id: req.params.activity_id,
  };

  db.query(
    `SET @id = null;
    SELECT @id := id, certificate.* FROM certificate 
    WHERE type_activity = ? AND activity_id = ?;
      
    SELECT * FROM certificate_user
    WHERE certificate_id = @id;`,
    [formData.type_activity, formData.activity_id],
    (error, result, fields) => {
      if (error) {
        res.json({ error: true, result: error });
      } else {
        res.json({ error: false, result: result });
      }
    }
  );
};

exports.clientCertificate = (req, res, next) => {
  /**
   * data format for client read certificate
   *
   * @param {number} type_activity
   * @param {number} user_id
   */
  const formData = [req.params.user_id, req.params.type_activity];

  db.query(
    `SELECT * FROM certificate_user t1
    INNER JOIN certificate t2
    ON t1.certificate_id = t2.id
    WHERE t1.user_id = ? AND t2.type_activity = ?`,
    formData,
    (error, result, fields) => {
      if (error) {
        res.json({ error: true, result: error });
      } else {
        res.json({ error: false, result: result });
      }
    }
  );
};

exports.detailCertificate = (req, res, next) => {
  /**
   * data format for client read certificate
   *
   * @param {number} user_id
   * @param {number} certificate_id
   */
  const formData = [req.params.user_id, req.params.certificate_id];

  db.query(
    `SELECT t1.*, t2.*, t3.name FROM certificate_user t1
    INNER JOIN certificate t2
    ON t1.certificate_id = t2.id
    INNER JOIN user t3
    ON t1.user_id = t3.user_id
    WHERE t1.user_id = ? AND t1.certificate_id = ?`,
    formData,
    (error, result, fields) => {
      if (error) {
        res.json({ error: true, result: error });
      } else {
        let type_activity = result[0].type_activity;
        let activity_id = result[0].activity_id;

        switch (type_activity) {
          case 1:
            db.query(
              `SELECT title AS title_certificate FROM course
              WHERE ?`,
              [{ course_id: activity_id }],
              (err, rslt) => {
                if (err) {
                  res.json({ error: true, result: err });
                } else {
                  result[0] = Object.assign(result[0], rslt[0]);
                  res.json({ error: false, result: result });
                }
              }
            );
            break;

          case 2:
            db.query(
              `SELECT title AS title_certificate FROM forum
              WHERE ?`,
              [{ forum_id: activity_id }],
              (err, rslt) => {
                if (err) {
                  res.json({ error: true, result: err });
                } else {
                  result[0] = Object.assign(result[0], rslt[0]);
                  res.json({ error: false, result: result });
                }
              }
            );
            break;

          case 3:
            db.query(
              `SELECT title AS title_certificate FROM grup
              WHERE ?`,
              [{ grup_id: activity_id }],
              (err, rslt) => {
                if (err) {
                  res.json({ error: true, result: err });
                } else {
                  result[0] = Object.assign(result[0], rslt[0]);
                  res.json({ error: false, result: result });
                }
              }
            );
            break;

          default:
            res.json({ error: true, result: error });
            break;
        }
      }
    }
  );
};

exports.updateCertificate = (req, res, next) => {
  /**
   * data format for update certificates
   *
   * @param {number}  id
   * @param {string}  title  title of certificate
   * @param {number}  template  id of template certificate
   * @param {number}  type_activity id of type activity (1=course, 2=forum, 3=meeting)
   * @param {number}  activity_id activity id of each database
   * @param {string}  signature_1 signature image 1
   * @param {string}  signature_name_1 name of signature image 1
   * @param {string}  signature_2 signature image 2
   * @param {string}  signature_name_2 name of signature image 2
   */
  uploadSignature(req, res, (err) => {
    const id = Number(req.params.id);
    let listUser = [];
    JSON.parse(req.body.listUser).map((elem) => {
      listUser.push([id, elem]);
    });

    let formData = [];
    if (!req.files['signature_1'] && !req.files['signature_2']) {
      formData = [
        {
          title: req.body.title,
          template: Number(req.body.template),
          signature_name_1: req.body.signature_name_1,
          signature_name_2: req.body.signature_name_2,
        },
        {
          id: id,
        },
        {
          type_activity: Number(req.body.type_activity),
        },
        {
          activity_id: Number(req.body.activity_id),
        },
        {
          certificate_id: id,
        },
      ];
    } else if (!req.files['signature_2']) {
      formData = [
        {
          title: req.body.title,
          template: Number(req.body.template),
          signature_1: !req.files['signature_1']
            ? null
            : `${env.APP_URL}/signature/${req.files['signature_1'][0].filename}`,
          signature_name_1: req.body.signature_name_1,

          signature_name_2: req.body.signature_name_2,
        },
        {
          id: id,
        },
        {
          type_activity: Number(req.body.type_activity),
        },
        {
          activity_id: Number(req.body.activity_id),
        },
        {
          certificate_id: id,
        },
      ];
    } else if (!req.files['signature_1']) {
      formData = [
        {
          title: req.body.title,
          template: Number(req.body.template),
          signature_name_1: req.body.signature_name_1,
          signature_2: !req.files['signature_2']
            ? null
            : `${env.APP_URL}/signature/${req.files['signature_2'][0].filename}`,
          signature_name_2: req.body.signature_name_2,
        },
        {
          id: id,
        },
        {
          type_activity: Number(req.body.type_activity),
        },
        {
          activity_id: Number(req.body.activity_id),
        },
        {
          certificate_id: id,
        },
      ];
    } else {
      formData = [
        {
          title: req.body.title,
          template: Number(req.body.template),
          signature_1: !req.files['signature_1']
            ? null
            : `${env.APP_URL}/signature/${req.files['signature_1'][0].filename}`,
          signature_name_1: req.body.signature_name_1,
          signature_2: !req.files['signature_2']
            ? null
            : `${env.APP_URL}/signature/${req.files['signature_2'][0].filename}`,
          signature_name_2: req.body.signature_name_2,
        },
        {
          id: id,
        },
        {
          type_activity: Number(req.body.type_activity),
        },
        {
          activity_id: Number(req.body.activity_id),
        },
        {
          certificate_id: id,
        },
      ];
    }

    db.query(
      `UPDATE certificate SET ?
      WHERE ? AND ? AND ?;

      DELETE FROM certificate_user WHERE ?;`,
      formData,
      (error, result, fields) => {
        if (error) {
          res.json({ error: true, result: error });
        } else {
          db.query(
            `INSERT INTO certificate_user (certificate_id, user_id) VALUES ?;`,
            [listUser],
            (error, result, fields) => {
              if (error) {
                res.json({ error: true, result: error });
              } else {
                res.json({ error: false, result: result[0] });
              }
            }
          );
        }
      }
    );
  });
};

exports.deleteCertificate = (req, res, next) => {
  /**
   * data format for delete certificates
   *
   * @param {string}  id
   */
  const formData = {
    id: req.params.id,
  };

  db.query(
    `DELETE FROM certificate WHERE id = ?;
    DELETE FROM certificate_user WHERE certificate_id = ?;`,
    [formData.id, formData.id],
    (error, result, fields) => {
      if (error) {
        res.json({ error: true, result: error });
      } else {
        res.json({ error: false, result: result[0] });
      }
    }
  );
};
