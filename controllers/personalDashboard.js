const conf = require("../configs/config");
const db = require("../configs/database");
const env = require("../env.json");
const moment = require("moment-timezone");
const moduleDB = require('../repository');

exports.getTodaySchedule = async (req, res) => {

  const curdate = moment(new Date());
  function dateFormat(paramsDate) {
    var date = paramsDate.split("-");
    var resDate = date[2] + "-" + date[1] + "-" + date[0];
    return resDate;
  }

  /**
  let queryMeeting = `SELECT l.class_id AS id, l.room_name AS title, IF(l.is_akses=1, u.name, null) AS moderator, l.schedule_start AS date, l.schedule_start AS startTime, l.schedule_end AS endTime, 'meeting' AS type
  FROM liveclass l
  JOIN liveclass_participant p ON l.class_id=p.class_id
  LEFT JOIN user u ON l.moderator=u.user_id
  WHERE LEFT(l.schedule_start, 10) = '${dateFormat(req.params.date)}' AND p.user_id='${req.params.userId}' AND l.company_id='${req.params.companyId}'`;
  */
  let dates = dateFormat(req.params.date);
  let queryMeeting = `
  SELECT
    lb.id AS id,
    l.class_id AS meeting_id,
    lb.id AS booking_id,
    l.room_name AS title, 
    IF(lb.is_akses=1, u.name, null) AS moderator,
    lb.tanggal AS date, 
    CAST(CONCAT(lb.tanggal, ' ', lb.jam_mulai) AS DATETIME) AS startTime, 
    CAST(CONCAT(lb.tanggal, ' ', lb.jam_selesai) AS DATETIME) AS endTime, 
    'meeting' AS type
  FROM 
  	liveclass l
    LEFT JOIN liveclass_booking lb ON lb.meeting_id = l.class_id
    LEFT JOIN liveclass_booking_participant lbp ON lbp.booking_id = lb.id
  	LEFT JOIN user u ON lb.moderator = u.user_id
  WHERE 
  	lb.tanggal = '${dates}' 
    AND lbp.user_id = '${req.params.userId}'
    AND l.company_id = '${req.params.companyId}'`;

  let queryWebinar = `SELECT w.id AS id, w.judul AS title, w.pembicara, w.start_time AS date, w.start_time AS startTime, w.end_time AS endTime, 'webinar' AS type,training_course_id 
  FROM webinars w
  LEFT JOIN webinar_peserta p ON w.id=p.webinar_id
  WHERE ('${dates}' BETWEEN SUBSTR(w.start_time,1,11) AND SUBSTR(w.end_time,1,11) )
  AND w.company_id='${req.params.companyId}' AND w.publish='1'
  AND (p.user_id='${req.params.userId}' OR (w.sekretaris LIKE '%${req.params.userId}%' OR w.moderator LIKE '%${req.params.userId}%' OR w.pembicara LIKE '%${req.params.userId}%' OR w.owner LIKE '%${req.params.userId}%'))
  GROUP BY w.id`;

  let queryTrainingExam = `SELECT a.id AS id, e.title AS title, c.title AS course, e.start_time AS startTime, e.end_time AS endTime, IF(e.exam=1, 'training exam', 'training quiz') AS type, (IF(( NOW() BETWEEN e.start_time AND e.end_time ) = 1 && e.scheduled = 1, '1', '0' )) AS on_schedule, e.repeatable, a.status AS status_assignee
  FROM training_exam e
  JOIN training_exam_assignee a ON a.exam_id=e.id
  LEFT JOIN training_user tu ON tu.id = a.training_user_id
  LEFT JOIN user u ON u.email = tu.email
  LEFT JOIN training_course c ON c.id = e.course_id
  WHERE e.scheduled = 1 AND ('${dates}' BETWEEN DATE_FORMAT(e.start_time, '%Y-%m-%d') AND DATE_FORMAT(e.end_time, '%Y-%m-%d')) AND u.user_id='${req.params.userId}' AND u.status='active' AND e.status='Active' AND a.status IN ('Open','Start','Finish')`;

  let queryTrainingCourse = `SELECT c.id AS id, c.title AS title, c.start_time AS startTime, c.end_time AS endTime, 'training course' AS type, (IF( (NOW() BETWEEN c.start_time AND c.end_time) = 1 && c.scheduled = 1, '1', '0' )) AS on_schedule 
  FROM training_course c
  WHERE c.scheduled = 1 AND (DATE('${dates}') BETWEEN DATE(c.start_time) AND DATE(c.end_time)) AND c.company_id='${req.params.companyId}' AND c.status='Active'`;

  let querySubmission = `
  SELECT a.* , a.start_time as 'startTime',a.end_time as 'endTime',
  (IF(( NOW() BETWEEN a.start_time AND a.end_time ) = 1, '1', '0' )) AS on_schedule
  FROM training_course_submission a 
  LEFT JOIN training_course b ON b.id = a.course_id 
  WHERE 
  b.company_id = '${req.params.companyId}' AND DATE(a.start_time) = DATE('${dates}')`;

  let data = [];

  db.query(queryMeeting, async (error, result, fields) => {
    if (error) {
      res.json({ error: true, result: error })
    }
    else {

      for (let i = 0; i < result.length; i++) {
        let tmp = [];
        let participant = await db.query(`SELECT u.user_id AS userId, u.avatar AS avatar, u.name AS name, p.start_time
        FROM user u
        LEFT JOIN liveclass_booking_participant p ON u.user_id=p.user_id
        WHERE p.booking_id='${result[i].id}'`);

        let guest = await db.query(`SELECT p.voucher AS userId, null AS avatar, p.name AS name, null as 'start_time', p.email
        FROM liveclass_booking_tamu p
        WHERE p.meeting_id='${result[i].id}'`);

        if (participant.length) {
          tmp = tmp.concat(participant);
        }
        if (guest.length) {
          tmp = tmp.concat(guest);
        }
        result[i].participant = tmp;
      }

      data = result;

      db.query(queryWebinar, async (error, result, fields) => {
        if (error) {
          res.json({ error: true, result: error })
        }
        else {

          for (let i = 0; i < result.length; i++) {
            //let pembicara = await db.query(`SELECT user_id, name FROM user WHERE user_id IN (${result[i].pembicara})`);

            let checkMandatory = await db.query(`SELECT agenda from agenda_course where course_id = '${result[i].training_course_id}' limit 1`);
            if (checkMandatory.length) {
              checkMandatory.forEach((item_agenda) => {
                let agenda = JSON.parse(item_agenda.agenda);
                let idx = agenda.findIndex((items) => { return items.id.toString() === result[i].id.toString() });
                if (idx > -1) {
                  result[i].is_mandatory = agenda[idx].is_mandatory;
                  result[i].is_read = result[i].is_mandatory ? 0 : 1;
                } else {
                  result[i].is_mandatory = false;
                  result[i].is_read = 1;
                }
              })
            } else {
              result[i].is_mandatory = false;
              result[i].is_read = 1;
            }

            if (result[i].pembicara.length > 1) {
              let split = result[i].pembicara.split(",");

              let tmp = { object: [], user_id: [], guest: [] }
              for (let j = 0; j < split.length; j++) {
                if (split[j] !== "") {
                  if (isNaN(Number(split[j]))) {
                    tmp.guest.push(split[j]);
                    tmp.object.push({ user_id: split[j], name: "Anonymous", type: "guest", email: null, phone: null });
                  } else {
                    tmp.user_id.push(split[j]);
                    tmp.object.push({ user_id: split[j], name: "Anonymous", type: "user", email: null, phone: null });
                  }
                }
              }

              if (split.length > 0 && tmp.object.length > 0) {

                let queryString = '';
                let params = [];
                if (tmp.user_id.length > 0) {
                  queryString = `SELECT user_id, name, email,phone FROM user WHERE user_id IN(?);`;
                  params.push(tmp.user_id);
                }
                if (tmp.guest.length > 0) {
                  queryString = `${queryString}SELECT wt.voucher as user_id, wt.name ,wt.email,wt.phone from webinar_tamu wt where wt.voucher in(?);`;
                  params.push(tmp.guest);
                }

                let pembicara = [await db.query(queryString, params)];

                if (pembicara && pembicara.length > 1 && tmp.guest.length > 0 && Array.isArray(pembicara)) {
                  pembicara = pembicara[0].concat(pembicara[1]);
                }

                try {
                  tmp.object.forEach((str, i) => {
                    let idx = pembicara.findIndex((check) => { return check.user_id == str.user_id });
                    if (idx > -1) {
                      tmp.object[i].name = pembicara[idx].name;
                      tmp.object[i].email = pembicara[idx].email;
                      tmp.object[i].phone = pembicara[idx].phone;
                    }
                  });
                } catch (e) {
                  // handle error undefined variable
                }

                // build peserta_speaker && guest_speaker
                result[i].peserta_speaker = [];
                result[i].guest_speaker = [];
                tmp.object.forEach((str) => {
                  if (str.type === "guest") {
                    result[i].guest_speaker.push(str);
                  } else {
                    result[i].peserta_speaker.push(str);
                  }
                });
                result[i].pembicara_origin = result[i].pembicara;
                result[i].pembicara = tmp.object;
              } else {
                result[i].pembicara = [];
              }
            }

            result[i].pembicara = result[i].pembicara.length > 0 ? result[i].pembicara : null;
            let dataPembicara = result[i].pembicara_origin ? result[i].pembicara_origin.split(',') : '';
            let pembicaraUser = await db.query(`SELECT user_id, name FROM user WHERE user_id IN (?)`, [dataPembicara]);
            let pembicaraGuest = await db.query(`SELECT voucher AS user_id, name FROM webinar_tamu WHERE voucher IN (?)`, [dataPembicara]);
            let pembicara = pembicaraUser.concat(pembicaraGuest);
            result[i].pembicara = pembicara.length != 0 ? pembicara : null;
            let participant = await db.query(`SELECT u.user_id AS userId, u.avatar AS avatar, u.name AS name
            FROM user u
            JOIN webinar_peserta p ON u.user_id=p.user_id
            WHERE p.webinar_id='${result[i].id}'`);
            result[i].participant = participant;
          }

          data = data.concat(result);

          db.query(queryTrainingExam, async (error, result, fields) => {
            if (error) {
              res.json({ error: true, result: error })
            }
            else {
              if (result.length > 0) {

                let tmp_exam = [];

                result.forEach((element, index) => {
                  if (result[index].repeatable > 0) {

                    result[index].is_read = 0;
                    // result[index].on_schedule = "1";

                    tmp_exam.push(result[index]);
                  } else {

                    let range = curdate.isBetween(moment(result[index].startDate), moment(result[index].endDate));

                    // result[index].on_schedule = "0";
                    result[index].is_read = 0;
                    if (result[index].status_assignee == 'Finish') {

                      // if (range) {
                      // result[index].on_schedule = "1";
                      result[index].is_read = 1;
                      // }

                    }
                    tmp_exam.push(result[index]);
                  }
                });
              }

              data = data.concat(result);
              let isTraining = await db.query(`SELECT tu.level,u.user_id, tu.id as training_user_id, tu.training_company_id FROM user u JOIN training_user tu ON tu.email = u.email WHERE u.user_id='${req.params.userId}'`);

              if (isTraining.length && isTraining[0].level === 'user') {

                db.query(queryTrainingCourse, async (error, resultCourse, fields) => {
                  if (error) {
                    res.json({ error: true, result: error })
                  }
                  else {
                    let resultSub = await db.query(querySubmission);
                    if (resultSub.length) {
                      data = data.concat(resultSub);
                    }
                    if (resultCourse.length) {
                      resultCourse = await checkUserAssignedCourseId(resultCourse, isTraining[0], dates, 'today');
                    }
                    res.json({ error: false, result: data.concat(resultCourse) })
                  }
                })
              }
              else {
                res.json({ error: false, result: data })
              }
            }
          })
        }
      })
    }
  })
};

async function checkUserAssignedCourseId(result, infoUser, currentDate, getBasedOnType) {
  return new Promise(async (resolve) => {
    let getUserAssignCourse = require('./training_course').getUserAssignCourse;
    let tmp = [];

    for (let index = 0; index < result.length; index++) {

      let courseId = result[index].id;
      let msg = { result: [], submission: [] };
      if (Number(courseId) > 0) {
        const submission = await moduleDB.getDetailSubmissionByIdCourse({ idCourse: courseId, currentDate: currentDate, type: getBasedOnType });
        let query = ` 
          SELECT
          tcc.id, 
          tcc.training_course_id,
          tcc.training_company_id,
          tc.name AS 'company',
          tc.image AS 'image_company',
          tcc.created_at AS 'assignment_date', 
          if(tcc.active > 0,'Active','Inactive') AS 'assignment_status'
          from training_course_company tcc
          left join training_company tc on tc.id = tcc.training_company_id
          where 
          tcc.training_course_id = ? AND tcc.active = 1;`;

        let checkUser = await db.query(query, [courseId]);
        msg = { error: false, result: checkUser, submission: [] };
      }

      if (msg.result.length) {

        let checkUser = await getUserAssignCourse(msg.result, 1);
        if (checkUser.length) {
          for (let index2 = 0; index2 < checkUser.length; index2++) {
            if (courseId === checkUser[index2].training_course_id) {
              // check user assign to course 
              let assigned = checkUser[index2].training_user.filter((str) => { return (str.id === infoUser.training_user_id && str.assigned == 1) });
              if (assigned.length) {
                // jika ada masukin
                let match = tmp.findIndex((item) => { return item.id === result[index].id });
                if (match == -1) {
                  tmp.push(result[index]);
                }
              }
            }
          }
        }
      }

      if (msg.submission.length > 0) {
        tmp = tmp.concat(msg.submission);
      }
    }
    console.log(9998)
    resolve(tmp);
  })
}

exports.getTaskOverdue = (req, res) => {
  db.query(`SELECT t.id, t.text AS title, t.description, t.status AS status, t.project_id , f.name AS project, t.start_date AS startDate, t.end_date AS endDate, t.progress AS progress, t.lock_data AS is_lock
  FROM gantt_tasks t
  JOIN files_folder f ON t.project_id=f.id
  JOIN gantt_user u ON t.id=u.task_id
  WHERE t.end_date < NOW() AND t.status!='Done' AND t.status!='Closed' AND u.user_id='${req.params.userId}' AND f.company_id='${req.params.companyId}'`, (error, result, fields) => {
    if (error) {
      res.json({ error: true, result: error })
    }
    else {
      res.json({ error: false, result: result })
    }
  })
};


exports.getProjectListDash = (req, res, next) => {

  let queryUser = `SELECT p.id, p.name AS title,
	(SELECT COUNT(l.class_id) FROM liveclass l LEFT JOIN liveclass_participant lp ON lp.class_id=l.class_id WHERE l.folder_id=p.id AND ((l.is_private='1' AND lp.user_id='${req.params.user_id}') OR l.is_private='0')) AS meeting,
	(SELECT COUNT(w.id) FROM webinars w WHERE w.project_id=p.id AND w.publish=1) AS webinar,
	IF(p.company_id!='${req.params.company_id}', c.company_name, null) AS share_from
	FROM files_folder p
	LEFT JOIN files_folder_user pu ON pu.folder_id=p.id
	LEFT JOIN company c ON c.company_id=p.company_id
	LEFT JOIN project_share s ON s.project_id=p.id
	WHERE (p.company_id='${req.params.company_id}' OR s.user_id='${req.params.user_id}') AND p.publish='1' AND p.mother='0' AND ((p.is_limit=1 AND (pu.user_id='${req.params.user_id}' AND pu.role='Project Admin') OR (pu.user_id='${req.params.user_id}' AND pu.role='User')) OR p.is_limit=0)
	GROUP BY p.id
	ORDER BY p.name ASC`;

  let queryAdmin = `SELECT p.id, p.name AS title, count(DISTINCT l.class_id) AS meeting, count(DISTINCT w.id) AS webinar, IF(p.company_id!='${req.params.company_id}', c.company_name, null) AS share_from
	FROM files_folder p
	LEFT JOIN liveclass l ON p.id=l.folder_id
	LEFT JOIN (SELECT * FROM webinars w WHERE w.publish=1) AS w ON w.project_id=p.id
	LEFT JOIN project_share s ON s.project_id=p.id
	LEFT JOIN company c ON c.company_id=p.company_id
	WHERE (p.company_id='${req.params.company_id}' OR s.user_id='${req.params.user_id}') AND p.mother='0' AND p.publish='1'
	GROUP BY p.id
	ORDER BY p.name ASC`;

  db.query(req.params.level == 'client' ? queryUser : queryAdmin, async (error, result, fields) => {
    if (error) {
      res.json({ error: true, result: error });
    } else {
      for (let i = 0; i < result.length; i++) {
        let related_user = await db.query(`SELECT DISTINCT u.name, u.avatar
        FROM gantt_tasks t
        JOIN gantt_user tu ON t.id = tu.task_id
        JOIN user u ON tu.user_id = u.user_id
        WHERE t.project_id='${result[i].id}'`)
        result[i].related_user = related_user.length != 0 ? related_user : null;

        let progress = await db.query(`SELECT
        ROUND((SELECT SUM(progress) FROM gantt_tasks WHERE project_id='${result[i].id}' AND visibility!='Private' AND type!='project') /
        (SELECT COUNT(id) FROM gantt_tasks WHERE project_id='${result[i].id}' AND visibility!='Private' AND type!='project') * 100, 2)
        AS progress`)
        result[i].progress = progress[0].progress;

        let total_documents = await db.query(`SELECT COUNT(f.id) AS total_documents FROM files_file f WHERE f.folder_id IN
        (
            SELECT * FROM
            (
                select  id
                from    (select * from files_folder
                         order by mother, id) files_folder,
                        (select @pv := '${result[i].id}') initialisation
                where   find_in_set(mother, @pv) > 0
                and     @pv := concat(@pv, ',', id)
          ) AS subquery
        )
        OR f.folder_id ='${result[i].id}'`)
        result[i].total_documents = total_documents[0].total_documents;
      }
      res.json({ error: false, result: result });
    }
  });
};

exports.getUpcommingSchedule = (req, res) => {

  let dates = new Date();
  if (req.params.date) {
    let split = req.params.date.split("-");
    dates = `${split[2]}-${split[1]}-${split[0]}`
  }
  const currentDateWithTime = moment(dates).format('YYYY-MM-DD HH:mm:ss');

  /**
  let queryMeeting = `SELECT l.class_id AS id, l.room_name AS title, IF(l.is_akses=1, u.name, null) AS moderator, l.schedule_start AS date, l.schedule_start AS startTime, l.schedule_end AS endTime, 'meeting' AS type
  FROM liveclass l
  JOIN liveclass_participant p ON l.class_id=p.class_id
  LEFT JOIN user u ON l.moderator=u.user_id
  WHERE LEFT(l.schedule_start, 10) = '${dateFormat(req.params.date)}' AND p.user_id='${req.params.userId}' AND l.company_id='${req.params.companyId}'`;
  */

  let queryWebinar = `SELECT w.id AS id, w.judul AS title, w.pembicara, w.start_time AS date, w.start_time AS startTime, w.end_time AS endTime, 'webinar' AS type,training_course_id 
  FROM webinars w
  LEFT JOIN webinar_peserta p ON w.id=p.webinar_id
  WHERE DATE(w.start_time) > DATE('${currentDateWithTime}')
  AND w.company_id='${req.params.companyId}' AND w.publish='1'
  AND (p.user_id='${req.params.userId}' OR (w.sekretaris LIKE '%${req.params.userId}%' OR w.moderator LIKE '%${req.params.userId}%' OR w.pembicara LIKE '%${req.params.userId}%' OR w.owner LIKE '%${req.params.userId}%'))
  GROUP BY w.id`;

  let queryTrainingExam = `SELECT a.id AS id, e.title AS title, c.title AS course, e.start_time AS startTime, e.end_time AS endTime, IF(e.exam=1, 'training exam', 'training quiz') AS type, (IF(( NOW() BETWEEN e.start_time AND e.end_time ) = 1 && e.scheduled = 1, '1', '0' )) AS on_schedule, e.repeatable, a.status AS status_assignee
  FROM training_exam e
  JOIN training_exam_assignee a ON a.exam_id=e.id
  LEFT JOIN training_user tu ON tu.id = a.training_user_id
  LEFT JOIN user u ON u.email = tu.email
  LEFT JOIN training_course c ON c.id = e.course_id
  WHERE e.scheduled = 1 AND DATE(e.start_time) > DATE('${currentDateWithTime}') AND u.user_id='${req.params.userId}' AND u.status='active' AND e.status='Active' AND a.status IN ('Open','Start','Finish')`;

  let queryTrainingCourse = `SELECT c.id AS id, c.title AS title, c.start_time AS startTime, c.end_time AS endTime, 'training course' AS type, (IF( (NOW() BETWEEN c.start_time AND c.end_time) = 1 && c.scheduled = 1, '1', '0' )) AS on_schedule 
  FROM training_course c
  WHERE c.scheduled = 1 AND DATE(c.start_time) > DATE('${currentDateWithTime}') AND c.company_id='${req.params.companyId}' AND c.status='Active'`;

  let querySubmission = `
  SELECT a.* , a.start_time as 'startTime',b.end_time as 'endTime' 
  FROM training_course_submission a 
  LEFT JOIN training_course b ON b.id = a.course_id 
  WHERE 
  b.company_id = '${req.params.companyId}' AND DATE(a.start_time) > DATE('${currentDateWithTime}')`;

  let data = [];

  db.query(queryWebinar, async (error, result, fields) => {
    if (error) {
      res.json({ error: true, result: error })
    }
    else {

      for (let i = 0; i < result.length; i++) {

        let checkMandatory = await db.query(`SELECT agenda from agenda_course where course_id = '${result[i].training_course_id}' limit 1`);
        if (checkMandatory.length) {
          checkMandatory.forEach((item_agenda) => {
            let agenda = JSON.parse(item_agenda.agenda);
            let idx = agenda.findIndex((items) => { return items.id.toString() === result[i].id.toString() });
            if (idx > -1) {
              result[i].is_mandatory = agenda[idx].is_mandatory;
              result[i].is_read = result[i].is_mandatory ? 0 : 1;
            } else {
              result[i].is_mandatory = false;
              result[i].is_read = 1;
            }
          })
        } else {
          result[i].is_mandatory = false;
          result[i].is_read = 1;
        }

        if (result[i].pembicara.length > 1) {
          let split = result[i].pembicara.split(",");

          let tmp = { object: [], user_id: [], guest: [] }
          for (let j = 0; j < split.length; j++) {
            if (split[j] !== "") {
              if (isNaN(Number(split[j]))) {
                tmp.guest.push(split[j]);
                tmp.object.push({ user_id: split[j], name: "Anonymous", type: "guest", email: null, phone: null });
              } else {
                tmp.user_id.push(split[j]);
                tmp.object.push({ user_id: split[j], name: "Anonymous", type: "user", email: null, phone: null });
              }
            }
          }

          if (split.length > 0 && tmp.object.length > 0) {

            let queryString = '';
            let params = [];
            if (tmp.user_id.length > 0) {
              queryString = `SELECT user_id, name, email,phone FROM user WHERE user_id IN(?);`;
              params.push(tmp.user_id);
            }
            if (tmp.guest.length > 0) {
              queryString = `${queryString}SELECT wt.voucher as user_id, wt.name ,wt.email,wt.phone from webinar_tamu wt where wt.voucher in(?);`;
              params.push(tmp.guest);
            }

            let pembicara = await db.query(queryString, params);

            if (pembicara && pembicara.length > 1 && tmp.guest.length > 0 && Array.isArray(pembicara)) {
              pembicara = pembicara[0].concat(pembicara[1]);
            }

            try {
              tmp.object.forEach((str, i) => {
                let idx = pembicara.findIndex((check) => { return check.user_id == str.user_id });
                if (idx > -1) {
                  tmp.object[i].name = pembicara[idx].name;
                  tmp.object[i].email = pembicara[idx].email;
                  tmp.object[i].phone = pembicara[idx].phone;
                }
              });
            } catch (e) {
              // handle error undefined variable
            }

            // build peserta_speaker && guest_speaker
            result[i].peserta_speaker = [];
            result[i].guest_speaker = [];
            tmp.object.forEach((str) => {
              if (str.type === "guest") {
                result[i].guest_speaker.push(str);
              } else {
                result[i].peserta_speaker.push(str);
              }
            });
            result[i].pembicara = tmp.object;
          } else {
            result[i].pembicara = [];
          }
        }
        let participant = await db.query(`SELECT u.user_id AS userId, u.avatar AS avatar, u.name AS name
        FROM user u
        JOIN webinar_peserta p ON u.user_id=p.user_id
        WHERE p.webinar_id='${result[i].id}'`);
        result[i].participant = participant;
      }

      data = data.concat(result);

      db.query(queryTrainingExam, async (error, result, fields) => {
        if (error) {
          res.json({ error: true, result: error })
        }
        else {
          if (result.length > 0) {

            let tmp_exam = [];

            result.forEach((element, index) => {
              if (result[index].repeatable > 0) {

                result[index].is_read = 0;
                // result[index].on_schedule = "1";

                tmp_exam.push(result[index]);
              } else {

                let range = moment(currentDateWithTime).isBetween(moment(result[index].startDate), moment(result[index].endDate));

                // result[index].on_schedule = "0";
                result[index].is_read = 0;
                if (result[index].status_assignee == 'Finish') {

                  // if (range) {
                  // result[index].on_schedule = "1";
                  result[index].is_read = 1;
                  // }

                }
                tmp_exam.push(result[index]);
              }
            });
          }

          data = data.concat(result);
          let isTraining = await db.query(`SELECT tu.level,u.user_id, tu.id as training_user_id, tu.training_company_id FROM user u JOIN training_user tu ON tu.email = u.email WHERE u.user_id='${req.params.userId}'`);
          if (isTraining.length && isTraining[0].level === 'user') {

            db.query(queryTrainingCourse, async (error, resultCourse, fields) => {
              if (error) {
                res.json({ error: true, result: error })
              }
              else {

                let resultSub = await db.query(querySubmission);
                if (resultSub.length) {
                  data = data.concat(resultSub);
                }

                if (resultCourse.length) {
                  resultCourse = await checkUserAssignedCourseId(resultCourse, isTraining[0], currentDateWithTime, 'upcomming');
                }
                res.json({ error: false, result: data.concat(resultCourse) })
              }
            })
          }
          else {
            res.json({ error: false, result: data })
          }
        }
      })
    }
  })


};