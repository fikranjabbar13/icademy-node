var env = require('../env.json');
var conf = require('../configs/config');
var db = require('../configs/database');

exports.getConfig = (req, res, next) => {
  db.query(
    `SELECT value FROM configuration WHERE variable='${req.params.variable}'`,
    (error, result, fields) => {
      if (error) {
        res.json({ error: true, result: error });
      } else {
        res.json({ error: false, result: result[0].value });
      }
    }
  );
};
