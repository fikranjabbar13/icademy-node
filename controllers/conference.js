
var env = require('../env.json');
var conf = require('../configs/config');
var db = require('../configs/database');
var moment = require('moment');

exports.create = (req, res) => {
  let form = {
    companyId: req.body.companyId,
    namaRuangan: req.body.namaRuangan,
    folderId: req.body.folderId,
    moderator: req.body.moderator,
    tag: req.body.tag,
    tanggalMulai: req.body.tanggalMulai,
    waktuMulai: req.body.waktuMulai,
    isPrivate: req.body.isPrivate,
    isScheduled: req.body.isScheduled,
  };

  let sql = `INSERT INTO learning_ptc (company_id, nama_ruangan, folder_id, moderator, tag, tanggal_mulai, waktu_mulai, is_private, is_scheduled) VALUES (?)`;
  let data = [[form.companyId, form.namaRuangan, form.folderId, form.moderator, form.tag, form.tanggalMulai, form.waktuMulai, form.isPrivate, form.isScheduled]];
  db.query(sql, data, (err, rows) => {
    if(err) res.json({error: true, result: err})

    db.query(`SELECT * FROM learning_ptc WHERE ptc_id = ?`, [rows.insertId], (err, rows) => {
      res.json({error: false, result: rows[0]})
    })
  })
}

exports.log = (req, res) => {
	let data = JSON.parse(req.body.event);
    var d = moment().format('YYYY-MM-DD H:mm:ss');
    let event = [
        'user-joined',
        'user-left',
        'user-cam-broadcast-start',
        'user-cam-broadcast-end'
    ];
    let eventMic = [
        'user-audio-voice-enabled',
        'user-audio-voice-disabled',
    ];
    if (event.filter((item)=> item == data[0].data.id).length){
            let form = {
                conference_id : data[0].data.attributes.meeting['external-meeting-id'],
                user_id : data[0].data.attributes.user['external-user-id'],
                event : data[0].data.id,
                time : d
            }
            db.query(`INSERT INTO conference_logs (conference_id, user_id, event, time) VALUES ('${form.conference_id}','${form.user_id}','${form.event}','${form.time}')`, (error, result) => {
            					if(error) {
            						res.json({ error: true, result: error.message });
                                }
                                else{
                                    res.json({ error: false, result: 'OK, success update logs conference' });
                                }
            				}
            )
    }
    else if (eventMic.filter((item)=> item == data[0].data.id).length){
            let form = {
                conference_id : data[0].data.attributes.meeting['external-meeting-id'],
                user_id : data[0].data.attributes.user['external-user-id'],
                event : data[0].data.id,
                listening_only : data[0].data.attributes.user['listening-only'] ? 1 : 0,
                sharing_mic : data[0].data.attributes.user['sharing-mic'] ? 1 : 0,
                time : d
            }
            db.query(`INSERT INTO conference_logs_audio (conference_id, user_id, event, listening_only, sharing_mic, time) VALUES ('${form.conference_id}','${form.user_id}','${form.event}','${form.listening_only}','${form.sharing_mic}','${form.time}')`, (error, result) => {
            					if(error) {
            						res.json({ error: true, result: error.message });
                                }
                                else{
                                    res.json({ error: false, result: 'OK, success update logs conference' });
                                }
            				}
            )
    }
    else{
        res.json({ error: false, result: 'OK, tidak menyimpan log' });
    }
}

exports.logs = (req, res) => {
    var d = moment().format('YYYY-MM-DD H:mm:ss');
    let event = [
        'joinedAudio',
        'notInAudio',
        // 'user-cam-broadcast-start',
        // 'user-cam-broadcast-end'
    ];
    let eventMic = [
        'selfMuted',
        'selfUnmuted',
    ];
    if (event.filter((item)=> item == req.body.event).length){
            let form = {
                conference_id : req.body.conference_id,
                user_id : req.body.user_id,
                event : req.body.event === 'joinedAudio' ? 'user-joined' : 'user-left',
                time : d
            }
            db.query(`INSERT INTO conference_logs (conference_id, user_id, event, time) VALUES ('${form.conference_id}','${form.user_id}','${form.event}','${form.time}')`, (error, result) => {
            					if(error) {
            						res.json({ error: true, result: error.message });
                                }
                                else{
                                    if (form.event === 'user-joined'){
                                      let form = {
                                          conference_id : req.body.conference_id,
                                          user_id : req.body.user_id,
                                          event : req.body.event === 'selfMuted' ? 'user-audio-voice-disabled' : 'user-audio-voice-enabled',
                                          listening_only : req.body.event === 'SelfMuted' ? 1 : 0,
                                          sharing_mic : req.body.event === 'SelfUnmuted' ? 1 : 0,
                                          time : d
                                      }
                                      db.query(`INSERT INTO conference_logs_audio (conference_id, user_id, event, listening_only, sharing_mic, time) VALUES ('${form.conference_id}','${form.user_id}','user-audio-voice-enabled','${form.listening_only}','${form.sharing_mic}','${form.time}')`, (error, result) => {
                                                if(error) {
                                                  res.json({ error: true, result: error.message });
                                                          }
                                                          else{
                                                              res.json({ error: false, result: 'OK, success update logs conference' });
                                                          }
                                              }
                                      )
                                    }
                                    else{
                                      let form = {
                                          conference_id : req.body.conference_id,
                                          user_id : req.body.user_id,
                                          event : req.body.event === 'selfMuted' ? 'user-audio-voice-disabled' : 'user-audio-voice-enabled',
                                          listening_only : req.body.event === 'SelfMuted' ? 1 : 0,
                                          sharing_mic : req.body.event === 'SelfUnmuted' ? 1 : 0,
                                          time : d
                                      }
                                      db.query(`INSERT INTO conference_logs_audio (conference_id, user_id, event, listening_only, sharing_mic, time) VALUES ('${form.conference_id}','${form.user_id}','user-audio-voice-disabled','${form.listening_only}','${form.sharing_mic}','${form.time}')`, (error, result) => {
                                                if(error) {
                                                  res.json({ error: true, result: error.message });
                                                          }
                                                          else{
                                                              res.json({ error: false, result: 'OK, success update logs conference' });
                                                          }
                                              }
                                      )
                                    }
                                }
            				}
            )
    }
    else if (eventMic.filter((item)=> item == req.body.event).length){
            let form = {
                conference_id : req.body.conference_id,
                user_id : req.body.user_id,
                event : req.body.event === 'selfMuted' ? 'user-audio-voice-disabled' : 'user-audio-voice-enabled',
                listening_only : req.body.event === 'SelfMuted' ? 1 : 0,
                sharing_mic : req.body.event === 'SelfUnmuted' ? 1 : 0,
                time : d
            }
            db.query(`INSERT INTO conference_logs_audio (conference_id, user_id, event, listening_only, sharing_mic, time) VALUES ('${form.conference_id}','${form.user_id}','${form.event}','${form.listening_only}','${form.sharing_mic}','${form.time}')`, (error, result) => {
            					if(error) {
            						res.json({ error: true, result: error.message });
                                }
                                else{
                                    res.json({ error: false, result: 'OK, success update logs conference' });
                                }
            				}
            )
    }
    else{
        res.json({ error: false, result: 'OK, tidak menyimpan log' });
    }
}