var env = require('../env.json');
var db = require('../configs/database');
var conf = require('../configs/config');
var moment = require('moment-timezone');

var io = require('socket.io-client');
var socket = io(env.SOCKET_URL);

socket.on('connect', () => {
  console.log('Connect socket...')
})

exports.meeting = (req, res) => {
  let waktu = req.params.waktu;
  let now = moment(new Date()).tz('Asia/Jakarta').format('YYYY-MM-DD HH:mm');
  let sql = `
    SELECT class_id, room_name, company_id, schedule_start AS schedule, folder_id
    FROM liveclass
    WHERE schedule_start >= ?`;
  db.query(sql, [now], async (err, rows) => {
    if(err) res.json({error: true, result: err})

    for(var i=0; i<rows.length; i++) {
      let getTimeWaktu = Date.parse(rows[i].schedule) - (parseInt(waktu) * 60 * 1000);
      rows[i].reminder = new Date(getTimeWaktu);

      let sql = `SELECT lp.user_id, u.name FROM liveclass_participant lp JOIN user u ON u.user_id = lp.user_id WHERE lp.class_id = ?`;
      let getParticipant = await db.query(sql, [rows[i].class_id]);

      let dateNow = moment(new Date()).format('YYYY-MM-DD HH:mm');
      let dateRemind = new Date(getTimeWaktu);

      rows[i].now = dateNow.toString().substr(0,16);
      rows[i].remind = moment(dateRemind).tz('Asia/Jakarta').format('YYYY-MM-DD HH:mm');

      if(dateNow.toString().substr(0,16) === moment(dateRemind).tz('Asia/Jakarta').format('YYYY-MM-DD HH:mm')) {
        for(var j=0; j<getParticipant.length; j++) {

          db.query(`INSERT INTO notification (tag, user_id, type, description, destination) VALUES (?)`, [[
            2,
            getParticipant[j].user_id,
            3,
            `Meeting "${rows[i].room_name}" akan dimulai ${waktu} menit lagi.`,
            `${env.APPS_URL}/detail-project/${rows[i].folder_id}`
          ]], (err, result) => err ? console.log(err) : console.log(result));

          getParticipant[j].isReminded = true;
        }
      }
      rows[i].participant = getParticipant;
      socket.emit('send', {companyId: rows[i].company_id})
    }

    res.json({
      error: false,
      count: rows.length,
      result: rows
    })
  })
}

exports.webinar = (req, res) => {
  let waktu = req.params.waktu;
  let now = moment(new Date()).tz('Asia/Jakarta').format('YYYY-MM-DD');
  let sql = `
    SELECT id AS class_id, company_id, judul AS room_name, CONCAT(tanggal, " ", jam_mulai) AS schedule, project_id AS folder_id
    FROM webinars
    WHERE tanggal >= ?`;
  db.query(sql, [now], async (err, rows) => {
    if(err) res.json({error: true, result: err})

    for(var i=0; i<rows.length; i++) {
      let getTimeWaktu = Date.parse(rows[i].schedule) - (parseInt(waktu) * 60 * 1000);
      rows[i].reminder = moment(new Date(getTimeWaktu)).format('YYYY-MM-DD HH:mm');

      let sql = `SELECT lp.user_id, u.name FROM webinar_peserta lp JOIN user u ON u.user_id = lp.user_id WHERE lp.webinar_id = ?`;
      let getParticipant = await db.query(sql, [rows[i].class_id]);

      let dateNow = moment(new Date()).format('YYYY-MM-DD HH:mm');
      let dateRemind = new Date(getTimeWaktu);

      console.log(dateNow.toString().substr(0,16));
      console.log(moment(dateRemind).format('YYYY-MM-DD HH:mm'));

      if(dateNow.toString().substr(0,16) === moment(dateRemind).tz('Asia/Jakarta').format('YYYY-MM-DD HH:mm')) {
        for(var j=0; j<getParticipant.length; j++) {

          db.query(`INSERT INTO notification (tag, user_id, type, description, destination) VALUES (?)`, [[
            2,
            getParticipant[j].user_id,
            7,
            `Training "${rows[i].room_name}" akan dimulai ${waktu} menit lagi.`,
            `${env.APPS_URL}/detail-project/${rows[i].folder_id}`
          ]], (err, result) => err ? console.log(err) : console.log(result));

          getParticipant[j].isReminded = true;
        }
      }

      socket.emit('send', {companyId: rows[i].company_id})
      rows[i].participant = getParticipant;
    }

    res.json({
      error: false,
      count: rows.length,
    })
  })
}

exports.ptc = async (req, res) => {
  // waktu = jam
  let dateNow = moment(new Date()).format('YYYY-MM-DD HH:mm');
  let waktu = req.params.waktu;

  let sql = `SELECT learning_ptc.ptc_id, learning_ptc.nama_ruangan, learning_ptc.tanggal_mulai, learning_ptc.waktu_mulai, learning_ptc_peserta.user_id, learning_ptc_peserta.peserta, learning_ptc.moderator, user.name
    FROM learning_ptc JOIN learning_ptc_peserta ON learning_ptc.ptc_id = learning_ptc_peserta.room_id
    	JOIN user ON user.user_id = learning_ptc_peserta.user_id
    WHERE learning_ptc.tanggal_mulai >= ?`;
  let getAllPtc = await db.query(sql, [dateNow]);

  for(var i=0; i<getAllPtc.length; i++) {
    // H- berapa jam
    let gabung = `${moment(Date.parse(getAllPtc[i].tanggal_mulai)).format('YYYY-MM-DD')} ${getAllPtc[i].waktu_mulai}`;
    console.log('gabung: ', gabung)
    let getTimeWaktu = Date.parse(gabung) - (parseInt(waktu) * 60 * 60 * 1000);
    // sementara kurang H- menit
    // let getTimeWaktu = Date.parse(rows[i].time_finish) - (parseInt(waktu) * 60 * 1000);

    let dateRemind = new Date(getTimeWaktu);

    getAllPtc[i].reminder = moment(new Date(getTimeWaktu)).format('YYYY-MM-DD HH:mm');
    getAllPtc[i].sekarang = moment(new Date()).format('YYYY-MM-DD HH:mm');

    console.log(dateNow.toString().substr(0,16));
    console.log(moment(dateRemind).format('YYYY-MM-DD HH:mm'));

    if(dateNow.toString().substr(0,16) === moment(dateRemind).tz('Asia/Jakarta').format('YYYY-MM-DD HH:mm')) {
      // user
      db.query(`INSERT INTO notification (tag, user_id, type, description, destination) VALUES (?)`, [[
        2,
        getAllPtc[i].user_id,
        8,
        `PTC "${getAllPtc[i].nama_ruangan}" akan dimulai ${waktu} jam lagi.`,
        `${env.APPS_URL}/ptc`
      ]], (err, result) => err ? console.log(err) : console.log(result));

      // moderator
      db.query(`INSERT INTO notification (tag, user_id, type, description, destination) VALUES (?)`, [[
        2,
        getAllPtc[i].moderator,
        8,
        `PTC "${getAllPtc[i].nama_ruangan}" akan dimulai ${waktu} jam lagi.`,
        `${env.APPS_URL}/ptc`
      ]], (err, result) => err ? console.log(err) : console.log(result));
    }
  }

  res.json({ error: false, result: getAllPtc })
}

exports.tugas = (req, res) => {
  // waktu = jam
  let waktu = req.params.waktu;
  let dateNow = moment(new Date()).format('YYYY-MM-DD HH:mm');

  // get all tugas
  let sql = `SELECT learning_jadwal.pelajaran_id, learning_jadwal.kelas_id, exam.exam_id, exam.course_id AS jadwal_id, exam.exam_title, exam.time_start, exam.time_finish
    FROM exam JOIN learning_jadwal ON exam.course_id = learning_jadwal.jadwal_id
    WHERE exam.jenis = '1' AND exam.quiz = '2' AND exam.time_finish >= ?`;

  db.query(sql, [dateNow], async (err, rows) => {
    if(err) res.json({ error: true, result: err })

    if(rows.length) {
      for(var i=0; i<rows.length; i++) {
        // params => [exam_id, kelas_id]
        let sqlGetMurid = `SELECT learning_kelas.kelas_id, learning_kelas.kelas_nama, learning_kelas_murid.murid_id, learning_murid.user_id, learning_murid.nama, exam_tugas_answer.user_id AS user_id_tugas, exam_tugas_answer.score
        FROM learning_kelas JOIN learning_kelas_murid ON learning_kelas.kelas_id = learning_kelas_murid.kelas_id
        	JOIN learning_murid ON learning_kelas_murid.murid_id = learning_murid.id
            LEFT JOIN exam_tugas_answer ON exam_tugas_answer.exam_id = ? AND exam_tugas_answer.user_id = learning_murid.user_id
        WHERE learning_kelas.kelas_id = ?`;
        let params = [rows[i].exam_id, rows[i].kelas_id];
        let getMurid = await db.query(sqlGetMurid, params);

        // H- berapa jam
        let getTimeWaktu = Date.parse(rows[i].time_finish) - (parseInt(waktu) * 60 * 60 * 1000);

        // sementara kurang H- menit
        // let getTimeWaktu = Date.parse(rows[i].time_finish) - (parseInt(waktu) * 60 * 1000);

        let dateRemind = new Date(getTimeWaktu);

        rows[i].reminder = moment(new Date(getTimeWaktu)).format('YYYY-MM-DD HH:mm');
        rows[i].sekarang = moment(new Date()).format('YYYY-MM-DD HH:mm');

        console.log(dateNow.toString().substr(0,16));
        console.log(moment(dateRemind).format('YYYY-MM-DD HH:mm'));

        if(dateNow.toString().substr(0,16) === moment(dateRemind).tz('Asia/Jakarta').format('YYYY-MM-DD HH:mm')) {
          for(var j=0; j<getMurid.length; j++) {
            if(getMurid[j].score === null) {
              console.log('Kirim notifikasi ke ', getMurid[i].nama, i, j);
              db.query(`INSERT INTO notification (tag, user_id, type, description, destination) VALUES (?)`, [[
                2,
                getMurid[j].user_id,
                9,
                `Batas waktu mengumpulkan tugas "${rows[i].exam_title}" akan berakhir ${waktu} jam lagi.`,
                `${env.APPS_URL}/murid/tugas`
              ]], (err, result) => err ? console.log(err) : console.log(result));
            }
          }
        }

        // rows[i].submitted = getMurid;
      }
    }

    res.json({ error: false, result: rows })
  })
}

exports.quiz = (req, res) => {
  // waktu = jam
  let waktu = req.params.waktu;
  let dateNow = moment(new Date()).format('YYYY-MM-DD HH:mm');

  // get all tugas
  let sql = `SELECT learning_jadwal.pelajaran_id, learning_jadwal.kelas_id, exam.exam_id, exam.course_id AS jadwal_id, exam.exam_title, exam.time_start, exam.time_finish
    FROM exam JOIN learning_jadwal ON exam.course_id = learning_jadwal.jadwal_id
    WHERE exam.jenis = '1' AND exam.quiz = '1' AND exam.time_finish >= ?`;

  db.query(sql, [dateNow], async (err, rows) => {
    if(err) res.json({ error: true, result: err })

    if(rows.length) {
      for(var i=0; i<rows.length; i++) {
        // params => [exam_id, kelas_id]
        let sqlGetMurid = `SELECT learning_kelas.kelas_id, learning_kelas.kelas_nama, learning_kelas_murid.murid_id, learning_murid.user_id, learning_murid.nama, exam_result_learning.user_id AS user_id_kuis, exam_result_learning.score
        FROM learning_kelas JOIN learning_kelas_murid ON learning_kelas.kelas_id = learning_kelas_murid.kelas_id
        	JOIN learning_murid ON learning_kelas_murid.murid_id = learning_murid.id
            LEFT JOIN exam_result_learning ON exam_result_learning.exam_id = ? AND exam_result_learning.user_id = learning_murid.user_id
        WHERE learning_kelas.kelas_id = ?`;
        let params = [rows[i].exam_id, rows[i].kelas_id];
        let getMurid = await db.query(sqlGetMurid, params);

        // H- berapa jam
        let getTimeWaktu = Date.parse(rows[i].time_finish) - (parseInt(waktu) * 60 * 60 * 1000);

        // sementara kurang H- menit
        // let getTimeWaktu = Date.parse(rows[i].time_finish) - (parseInt(waktu) * 60 * 1000);

        let dateRemind = new Date(getTimeWaktu);

        rows[i].reminder = moment(new Date(getTimeWaktu)).format('YYYY-MM-DD HH:mm');
        rows[i].sekarang = moment(new Date()).format('YYYY-MM-DD HH:mm');

        console.log(dateNow.toString().substr(0,16));
        console.log(moment(dateRemind).format('YYYY-MM-DD HH:mm'));

        if(dateNow.toString().substr(0,16) === moment(dateRemind).tz('Asia/Jakarta').format('YYYY-MM-DD HH:mm')) {
          for(var j=0; j<getMurid.length; j++) {
            if(getMurid[j].score === null) {
              console.log('Kirim notifikasi ke ', getMurid[i].nama, i, j);
              db.query(`INSERT INTO notification (tag, user_id, type, description, destination) VALUES (?)`, [[
                2,
                getMurid[j].user_id,
                10,
                `Quiz "${rows[i].exam_title}" akan berakhir ${waktu} jam lagi.`,
                `${env.APPS_URL}/murid/kuis`
              ]], (err, result) => err ? console.log(err) : console.log(result));
            }
          }
        }

        // rows[i].submitted = getMurid;
      }
    }

    res.json({ error: false, result: rows })
  })
}

exports.ujian = (req, res) => {
  // waktu = jam
  let waktu = req.params.waktu;
  let dateNow = moment(new Date()).format('YYYY-MM-DD HH:mm');

  // get all tugas
  let sql = `SELECT learning_jadwal.pelajaran_id, learning_jadwal.kelas_id, exam.exam_id, exam.course_id AS jadwal_id, exam.exam_title, exam.time_start, exam.time_finish
    FROM exam JOIN learning_jadwal ON exam.course_id = learning_jadwal.jadwal_id
    WHERE exam.jenis = '1' AND exam.quiz = '0' AND exam.time_finish >= ?`;

  db.query(sql, [dateNow], async (err, rows) => {
    if(err) res.json({ error: true, result: err })

    if(rows.length) {
      for(var i=0; i<rows.length; i++) {
        // params => [exam_id, kelas_id]
        let sqlGetMurid = `SELECT learning_kelas.kelas_id, learning_kelas.kelas_nama, learning_kelas_murid.murid_id, learning_murid.user_id, learning_murid.nama, exam_result_learning.user_id AS user_id_ujian, exam_result_learning.score
        FROM learning_kelas JOIN learning_kelas_murid ON learning_kelas.kelas_id = learning_kelas_murid.kelas_id
        	JOIN learning_murid ON learning_kelas_murid.murid_id = learning_murid.id
            LEFT JOIN exam_result_learning ON exam_result_learning.exam_id = ? AND exam_result_learning.user_id = learning_murid.user_id
        WHERE learning_kelas.kelas_id = ?`;
        let params = [rows[i].exam_id, rows[i].kelas_id];
        let getMurid = await db.query(sqlGetMurid, params);

        // H- berapa jam
        let getTimeWaktu = Date.parse(rows[i].time_finish) - (parseInt(waktu) * 60 * 60 * 1000);

        // sementara kurang H- menit
        // let getTimeWaktu = Date.parse(rows[i].time_finish) - (parseInt(waktu) * 60 * 1000);

        let dateRemind = new Date(getTimeWaktu);

        rows[i].reminder = moment(new Date(getTimeWaktu)).format('YYYY-MM-DD HH:mm');
        rows[i].sekarang = moment(new Date()).format('YYYY-MM-DD HH:mm');

        console.log(dateNow.toString().substr(0,16));
        console.log(moment(dateRemind).format('YYYY-MM-DD HH:mm'));

        if(dateNow.toString().substr(0,16) === moment(dateRemind).tz('Asia/Jakarta').format('YYYY-MM-DD HH:mm')) {
          for(var j=0; j<getMurid.length; j++) {
            if(getMurid[j].score === null) {
              console.log('Kirim notifikasi ke ', getMurid[i].nama, i, j);
              db.query(`INSERT INTO notification (tag, user_id, type, description, destination) VALUES (?)`, [[
                2,
                getMurid[j].user_id,
                11,
                `Ujian "${rows[i].exam_title}" akan berakhir ${waktu} jam lagi.`,
                `${env.APPS_URL}/murid/ujian`
              ]], (err, result) => err ? console.log(err) : console.log(result));
            }
          }
        }

        // rows[i].submitted = getMurid;
      }
    }

    res.json({ error: false, result: rows })
  })
}

exports.pelajaranToday = (req, res) => {
  let waktu = req.params.waktu;
  let dateNow = moment(new Date()).format('YYYY-MM-DD');

  let sql = `SELECT lj.jadwal_id, lj.hari,lj.pelajaran_id, lp.nama_pelajaran, lj.kelas_id, lkm.murid_id, lm.user_id, lm.nama, cc.chapter_id, cc.course_id, cc.chapter_title, cc.start_date
    FROM course_chapter cc JOIN learning_jadwal lj ON cc.course_id = lj.jadwal_id
    	JOIN learning_pelajaran lp ON lj.pelajaran_id = lp.pelajaran_id
    	JOIN learning_kelas_murid lkm ON lj.kelas_id = lkm.kelas_id
      JOIN learning_murid lm ON lm.id = lkm.murid_id
    WHERE cc.start_date LIKE '${dateNow}%'`;

  db.query(sql, async (err, rows) => {
    if(err) res.json({ error: true, result: err })

    if(rows.length) {
      for(var i=0; i<rows.length; i++) {
        let sqlGetMurid = ``;

        // sementara kurang H- menit
        let getTimeWaktu = Date.parse(rows[i].start_date);
        let dateRemind = new Date(getTimeWaktu);

        rows[i].reminder = moment(new Date(getTimeWaktu)).format('YYYY-MM-DD HH:mm');
        rows[i].sekarang = moment(new Date()).format('YYYY-MM-DD HH:mm');

        console.log(dateNow.toString().substr(0,16));
        console.log(moment(dateRemind).format('YYYY-MM-DD HH:mm'));

        // running sebelum {waktu} yg ditentukan
        if(dateNow.toString().substr(0,16) === moment(dateRemind).tz('Asia/Jakarta').format('YYYY-MM-DD HH:mm')) {
          db.query(`INSERT INTO notification (tag, user_id, type, description, destination) VALUES (?)`, [[
            2,
            rows[i].user_id,
            1,
            `Pelajaran ${rows[i].nama_pelajaran} akan dimulai pukul "${moment(rows[i].start_date).format('HH:mm')}" (${waktu} menit mendatang).`,
            `${env.APPS_URL}/ruangan/mengajar/${rows[i].jadwal_id}/materi/${rows[i].chapter_id}`
          ]], (err, result) => err ? console.log(err) : console.log(result));
        }

        // running tepat pada jamnya
        if(dateNow.toString().substr(0,16) === moment(dateRemind).format('YYYY-MM-DD HH:mm')) {
          db.query(`INSERT INTO notification (tag, user_id, type, description, destination) VALUES (?)`, [[
            2,
            rows[i].user_id,
            1,
            `Ruang pelajaran ${rows[i].nama_pelajaran} sudah dibuka pukul "${moment(rows[i].start_date).format('HH:mm')}".`,
            `${env.APPS_URL}/ruangan/mengajar/${rows[i].jadwal_id}/materi/${rows[i].chapter_id}`
          ]], (err, result) => err ? console.log(err) : console.log(result));
        }
      }
    }

    res.json({ error: false, result: rows })
  })
}
