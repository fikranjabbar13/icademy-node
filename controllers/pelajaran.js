var env = require('../env.json');
var conf = require('../configs/config');
var db = require('../configs/database');
var moment = require('moment-timezone');

var multer = require('multer');
const AWS = require('aws-sdk');
const fs = require('fs');

const Async = require('async');

const s3 = new AWS.S3({
  accessKeyId: env.AWS_ACCESS,
  secretAccessKey: env.AWS_SECRET
});
const storage = multer.diskStorage({
  filename: function (req, file, cb) {
    cb(null, file.originalname);
  }
})
const Attachments = multer({storage: storage}).array('files', 10);

var storageExcel = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, './public/company');
  },
  filename: (req, file, cb) => {
    cb(null, 'excel-' + Date.now() + '-' + file.originalname);
  }
});
const UploadExcel = multer({storage: storageExcel}).single('files');

exports.createSilabus = (req, res) => {
  let form = {
    pelajaranId: req.body.pelajaranId,
    sesi: req.body.sesi,
    periode: req.body.periode,
    durasi: req.body.durasi,
    jenis: req.body.jenis,
    topik: req.body.topik,
    tujuan: req.body.tujuan,
    deskripsi: req.body.deskripsi
  };

  if(form.jenis == '0') {
    let data = [[form.pelajaranId, form.sesi, form.periode, form.durasi, form.jenis, form.topik, form.tujuan, form.deskripsi]];
    let sql = `INSERT INTO learning_pelajaran_silabus (pelajaran_id, sesi, periode, durasi, jenis, topik, tujuan, deskripsi) VALUES (?)`;
    db.query(sql, data, (err, rows) => {
      if(err) res.json({error: true, result: err})

      db.query(`SELECT * FROM learning_pelajaran_silabus WHERE id = ?`, [rows.insertId], (err, rows) => {
        res.json({ error: false, result: rows[0] })
      })
    })
  } else {
    let data = [[form.pelajaranId, form.sesi, form.jenis, form.periode, form.durasi, form.deskripsi]];
    let sql = `INSERT INTO learning_pelajaran_silabus (pelajaran_id, sesi, jenis, periode, durasi, deskripsi) VALUES (?)`;
    db.query(sql, data, (err, rows) => {
      console.log(err)
      if(err) res.json({error: true, result: err})

      db.query(`SELECT * FROM learning_pelajaran_silabus WHERE id = ?`, [rows.insertId], (err, rows) => {
        res.json({ error: false, result: rows[0] })
      })
    })
  }

}

exports.getSilabusById = (req, res) => {
  db.query(`SELECT * FROM learning_pelajaran_silabus WHERE id = ? ORDER BY sesi ASC`, [req.params.id], (err, rows) => {
    res.json({ error: false, result: rows.length ? rows[0] : [] })
  })
}

exports.getSilabusByPelajaran = (req, res) => {
  db.query(`SELECT * FROM learning_pelajaran_silabus WHERE pelajaran_id = ? ORDER BY sesi ASC`, [req.params.id], (err, rows) => {
    res.json({ error: false, result: rows.length ? rows : [] })
  })
}

exports.updateSilabus = (req, res) => {
  let form = {
    sesi: req.body.sesi,
    periode: req.body.periode,
    durasi: req.body.durasi,
    jenis: req.body.jenis,
    topik: req.body.topik,
    tujuan: req.body.tujuan,
    deskripsi: req.body.deskripsi
  };

  let sql = `UPDATE learning_pelajaran_silabus SET sesi = ?, jenis = ?, periode = ?, durasi = ?, topik = ?, tujuan = ?, deskripsi = ? WHERE id = ?`;
  let data = [form.sesi, form.jenis, form.periode, form.durasi, form.topik, form.tujuan, form.deskripsi, req.params.id];
  db.query(sql, data, (err, rows) => {
    if(err) res.json({ error: true, result: err })

    db.query(`SELECT * FROM learning_pelajaran_silabus WHERE id = ?`, [req.params.id], (err, rows) => {
      res.json({ error: false, result: rows[0] })
    })
  })
}

exports.fileSilabus = (req, res) => {
  Attachments(req, res, async (err) => {
    if(!req.files) {
      res.json({error: true, result: err});
    } else {
      let attachmentId = '';
      for(let i=0; i<req.files.length; i++) {
        let path = req.files[i].path;
        let keyName = `${env.AWS_BUCKET_ENV}/silabus`;

        var params = {
          ACL: 'public-read',
          Bucket: env.AWS_BUCKET,
          Body: fs.createReadStream(path),
          Key: `${keyName}/${req.files[i].originalname}`
        };

        let dd = await s3.upload(params).promise();
        fs.unlinkSync(path);
        attachmentId += dd.Location + ',';
      }

      attachmentId = attachmentId.substring(0, attachmentId.length - 1);
      db.query(`UPDATE learning_pelajaran_silabus SET files = ? WHERE id = ?`, [attachmentId, req.params.id], (error, result, fields) => {
        if(error) {
          res.json({error: true, result: error});
        } else {
          res.json({error: false, result: attachmentId});
        }
      })
    }
  })
}

exports.deleteSilabus = (req, res) => {
  db.query(`DELETE FROM learning_pelajaran_silabus WHERE id = ?`, [req.params.id], (err, rows) => {
    if(err) res.json({error: true, result: err})

    res.json({ error: false, result: rows })
  })
}

exports.create = (req, res) => {
  let form = {
    companyId: req.body.companyId,
    namaPelajaran: req.body.namaPelajaran,
    kategori: req.body.kategori,
    kodePelajaran: req.body.kodePelajaran,
  };

  let sql = `INSERT INTO learning_pelajaran (pelajaran_id, company_id, nama_pelajaran, kategori, kode_pelajaran) VALUES (null, ?, ?, ?, ?)`;
  db.query(sql, [form.companyId, form.namaPelajaran, form.kategori, form.kodePelajaran], (error, rows) => {
    if(error) res.json({ error: true, result: error })

    let sql = `SELECT p.* FROM learning_pelajaran p WHERE p.pelajaran_id = ?`;
    db.query(sql, [rows.insertId], (error, rows) => {
      res.json({ error: false, result: rows[0] })
    })
  })
}

exports.deletePelajaranById = (req, res) => {
  db.query(`DELETE FROM learning_pelajaran WHERE pelajaran_id = ?`, [req.params.id], (err, rows) => {
    if(err) res.json({error: true, result: err})

    db.query(`DELETE FROM learning_pelajaran_silabus WHERE pelajaran_id = ?`, [req.params.id]);

    res.json({ error: false, result: rows })
  })
}

exports.getByCompanyId = (req, res) => {
  let sql = `SELECT p.*, m.id AS kurikulum, j.jadwal_id AS jadwal
  FROM learning_pelajaran p
  LEFT JOIN learning_kurikulum_mapel m ON m.mapel_id=p.pelajaran_id
  LEFT JOIN learning_jadwal j ON j.pelajaran_id=p.pelajaran_id
  WHERE p.company_id = ?`;
  db.query(sql, [req.params.companyId], async (error, rows) => {
    if(error) res.json({ error: true, result: error });

    if(rows.length) {
      for(var i=0; i<rows.length; i++) {
        let getSilabus = await db.query(`SELECT id FROM learning_pelajaran_silabus WHERE pelajaran_id = ?`, [rows[i].pelajaran_id])
        rows[i].silabus = getSilabus.length;
      }
    }

    res.json({ error: false, result: rows })
  })
}

exports.getByCompanyGuruId = (req, res) => {
  let companyId = req.params.companyId;
  let sql = `SELECT p.*, k.kelas_nama AS kelas FROM learning_pelajaran p JOIN learning_kelas k ON k.kelas_id = p.kelas_id WHERE p.company_id = ?`;
  db.query(sql, [companyId], async (error, rows) => {
    if(error) res.json({ error: true, result: error });
    if(rows.length) {
      for(var i=0; i<rows.length; i++) {
        let getChapters = await db.query(`SELECT chapter_id FROM course_chapter WHERE jenis_pembelajaran = 'learning' AND company_id = ? AND course_id = ?`, [companyId, rows[i].pelajaran_id]);
        rows[i].materi = getChapters.length

        let getKuis = await db.query(`SELECT exam_id FROM exam WHERE jenis = '1' AND company_id = ? AND course_id = ? AND quiz = ?`, [companyId, rows[i].pelajaran_id, 1]);
        rows[i].kuis = getKuis.length

        let getTugas = await db.query(`SELECT exam_id FROM exam WHERE jenis = '1' AND company_id = ? AND course_id = ? AND quiz = ?`, [companyId, rows[i].pelajaran_id, 2]);
        rows[i].tugas = getTugas.length

        let getUjian = await db.query(`SELECT exam_id FROM exam WHERE jenis = '1' AND company_id = ? AND course_id = ? AND quiz = ?`, [companyId, rows[i].pelajaran_id, 0]);
        rows[i].ujian = getUjian.length
      }
    }
    res.json({ error: false, result: rows })
  })
}

exports.getById = (req, res) => {
  let sql = `SELECT p.* FROM learning_pelajaran p WHERE p.pelajaran_id = ?`;
  db.query(sql, [req.params.id], (error, rows) => {
    if(error) res.json({ error: true, result: error });

    res.json({ error: false, result: rows.length ? rows[0] : [] })
  })
}

exports.updateById = (req, res) => {
  let form = {
    namaPelajaran: req.body.namaPelajaran,
    kategori: req.body.kategori,
    kodePelajaran: req.body.kodePelajaran,
  }

  let sql = `UPDATE learning_pelajaran SET nama_pelajaran = ?, kategori = ?, kode_pelajaran = ? WHERE pelajaran_id = ?`;
  db.query(sql, [form.namaPelajaran, form.kategori, form.kodePelajaran, req.params.id], (error, rows) => {
    if(error) res.json({ error: true, result: error })

    let sql = `SELECT p.* FROM learning_pelajaran p WHERE p.pelajaran_id = ?`;
    db.query(sql, [req.params.id], (error, rows) => {
      res.json({ error: false, result: rows[0] })
    })
  })
}

exports.updateOverviewById = (req, res) => {
  let sql = `UPDATE learning_jadwal SET deskripsi = ? WHERE jadwal_id = ?`;
  db.query(sql, [req.body.overview, req.params.id], (error, rows) => {
    if(error) res.json({ error: true, result: error })

    let sql = `SELECT jadwal_id, deskripsi FROM learning_jadwal WHERE jadwal_id = ?`;
    db.query(sql, [req.params.id], (error, rows) => {
      res.json({ error: false, result: rows[0] })
    })
  })
}

exports.updateProsentase = async (req, res) => {
  let pelajaranId = req.params.pelajaranId;
  // check exist or not
  let cek = `SELECT * FROM learning_pelajaran_nilai WHERE pelajaran_id = ?`;
  let cekRow = await db.query(cek, [pelajaranId]);
  if(cekRow.length === 1) {
    let sql = `UPDATE learning_pelajaran_nilai SET tugas = ?, kuis = ?, ujian = ? WHERE pelajaran_id = ?`;
    let data = [req.body.tugas, req.body.kuis, req.body.ujian, pelajaranId];
    db.query(sql, data, (err, rows) => {
      if(err) res.json({ error: true, result: err })
      res.json({
        error: false,
        result: {
          pelajaran_id: pelajaranId,
          tugas: req.body.tugas,
          kuis: req.body.kuis,
          ujian: req.body.ujian,
        }
      })
    })
  } else {
    let sql = `INSERT INTO learning_pelajaran_nilai (tugas, kuis, ujian, pelajaran_id) VALUES (?)`;
    let data = [req.body.tugas, req.body.kuis, req.body.ujian, pelajaranId];
    db.query(sql, [data], (err, rows) => {
      if(err) res.json({ error: false, result: err })
      res.json({
        error: false,
        result: {
          pelajaran_id: pelajaranId,
          tugas: req.body.tugas,
          kuis: req.body.kuis,
          ujian: req.body.ujian,
        }
      })
    })
  }
}

exports.getProsentase = (req, res) => {
  let sql = `SELECT * FROM learning_pelajaran_nilai WHERE pelajaran_id = ?`;
  db.query(sql, [req.params.pelajaranId], (err, rows) => {
    if(err) res.json({ error: true, result: err })

    res.json({ error: false, result: rows.length ? rows[0] : [] })
  })
}

exports.createChapter = (req, res) => {
  let form = {
    companyId: req.body.companyId,
    pelajaranId: req.body.pelajaranId,
    number: req.body.number,
    title: req.body.title,
    content: req.body.content,
    tanggal: req.body.tanggal,
    tatapmuka: req.body.tatapmuka,
    silabusId: req.body.silabusId,
    jadwalId: req.body.jadwalId
  }
  let sql = `INSERT INTO course_chapter (jadwal_id, silabus_id, tatapmuka, jenis_pembelajaran, company_id, course_id, chapter_number, chapter_title, chapter_body, start_date) VALUES (?)`;
  let data = [[form.jadwalId, form.silabusId, form.tatapmuka, 'learning', form.companyId, form.pelajaranId, form.number, form.title, form.content, form.tanggal]];
  db.query(sql, data, (err, rows) => {
    if(err) res.json({ error: true, result: err })

    let sql = `SELECT chapter_id AS id, tatapmuka, company_id, course_id AS pelajaran_id, chapter_number AS number, chapter_title AS title, chapter_body AS content, start_date AS tanggal, attachment_id AS attachments FROM course_chapter
      WHERE company_id = ? AND course_id = ? AND jenis_pembelajaran = ? AND chapter_id = ?`;
    let data = [form.companyId, form.pelajaranId, 'learning', rows.insertId];
    db.query(sql, data, (err, rows) => {
      res.json({error: false, result: rows.length ? rows[0] : []})
    })
  })
}

exports.uploadAttachments = (req, res) => {
  Attachments(req, res, async (err) => {
    if(!req.files) {
      res.json({error: true, result: err});
    } else {
      let attachmentId = '';
      for(let i=0; i<req.files.length; i++) {
        let path = req.files[i].path;
        let keyName = `${env.AWS_BUCKET_ENV}/course`;

        var params = {
          ACL: 'public-read',
          Bucket: env.AWS_BUCKET,
          Body: fs.createReadStream(path),
          Key: `${keyName}/${req.files[i].originalname}`
        };

        let dd = await s3.upload(params).promise();
        fs.unlinkSync(path);
        attachmentId += dd.Location + ',';
      }

      attachmentId = attachmentId.substring(0, attachmentId.length - 1);
      db.query(`UPDATE course_chapter SET attachment_id = ? WHERE chapter_id = ?`, [attachmentId, req.params.id], (error, result, fields) => {
        if(error) {
          res.json({error: true, result: error});
        } else {
          res.json({error: false, result: attachmentId});
        }
      })
    }
  })
}

exports.getAllChapters = (req, res) => {
  let sql = `SELECT cc.chapter_id AS id, cc.tatapmuka, cc.chapter_number AS number, cc.chapter_title AS title, cc.course_id, lj.pelajaran_id FROM course_chapter cc
	JOIN learning_jadwal lj ON lj.jadwal_id = cc.course_id
    WHERE cc.jenis_pembelajaran = ? AND cc.course_id = ?`;
  let data = ['learning', req.params.id];
  db.query(sql, data, (err, rows) => {
    res.json({error: false, result: rows})
  })
}

exports.getOneChapters = (req, res) => {
  let sql = `SELECT chapter_id AS id, tatapmuka, company_id, course_id AS pelajaran_id, chapter_number AS number, chapter_title AS title, chapter_body AS content, start_date AS tanggal, attachment_id AS attachments FROM course_chapter
    WHERE jenis_pembelajaran = ? AND chapter_id = ?`;
  let data = ['learning', req.params.id];
  db.query(sql, data, (err, rows) => {
    if(rows.length) {
      if(rows[0].attachments) {
        rows[0].attachments = rows[0].attachments.split(',')
      }
    }
    res.json({error: false, result: rows.length ? rows[0] : []})
  })
}

exports.updateChapter = (req, res) => {
  let form = {
    number: req.body.number,
    title: req.body.title,
    content: req.body.content,
    tatapmuka: req.body.tatapmuka,
    tanggal: req.body.tanggal,
  }
  let sql = `UPDATE course_chapter SET tatapmuka = ?, chapter_number = ?, chapter_title = ?, chapter_body = ?, start_date = ? WHERE chapter_id = ?`;
  let data = [form.tatapmuka, form.number, form.title, form.content, form.tanggal, req.params.id];
  db.query(sql, data, (err, rows) => {
    if(err) res.json({ error: true, result: err })

    let sql = `SELECT chapter_id AS id, tatapmuka, company_id, course_id AS pelajaran_id, chapter_number AS number, chapter_title AS title, chapter_body AS content, start_date AS tanggal, attachment_id AS attachments FROM course_chapter
      WHERE jenis_pembelajaran = ? AND chapter_id = ?`;
    let data = ['learning', req.params.id];
    db.query(sql, data, (err, rows) => {
      if(rows[0].attachments) {
        rows[0].attachments = rows[0].attachments.split(',')
      }
      res.json({error: false, result: rows.length ? rows[0] : []})
    })
  })
}

exports.deleteChapter = (req, res) => {
  let sql = `DELETE FROM course_chapter WHERE chapter_id = ?`;
  db.query(sql, [req.params.id], (err, rows) => {
    if(err) res.json({ error: true, result: err })
    res.json({error: false, result: rows})
  })
}

exports.guruGetById = (req, res) => {
  let sql = `SELECT p.*, k.kelas_nama AS kelas FROM learning_pelajaran p JOIN learning_kelas k ON k.kelas_id = p.kelas_id WHERE p.pelajaran_id = ?`;
  db.query(sql, [req.params.id], async (error, rows) => {
    if(error) res.json({ error: true, result: error });

    // if(rows.length) {
    //   for(var i=0; i<rows.length; i++) {
    //
    //   }
    // }

    res.json({ error: false, result: rows.length ? rows[0] : [] })
  })
}

exports.createKuis = (req, res) => {
  let tipe = req.params.tipe == "kuis" ? "1" : req.params.tipe == "tugas" ? "2" : "0";

  let form = {
    companyId: req.body.companyId,
    pelajaranId: req.body.pelajaranId,
    title: req.body.title,
    quizAt: req.body.quizAt,
    tatapmuka: req.body.tatapmuka,
    tanggalMulai: req.body.tanggalMulai,
    tanggalAkhir: req.body.tanggalAkhir,
    tipeJawab: req.body.tipeJawab ? req.body.tipeJawab : '2',
    jadwalId: req.body.jadwalId ? req.body.jadwalId : '0',
  }
  let sql = `INSERT INTO exam (jadwal_id, tipe_jawab, tatapmuka, jenis, quiz, quiz_at, company_id, course_id, exam_title, time_start, time_finish) VALUES (?)`;
  let data = [[form.jadwalId, form.tipeJawab, form.tatapmuka, '1', tipe, form.quizAt,  form.companyId, form.pelajaranId, form.title, form.tanggalMulai, form.tanggalAkhir]];
  db.query(sql, data, (err, rows) => {
    if(err) res.json({ error: true, result: err })

    let sql = `SELECT exam_id AS id, tipe_jawab, jenis, quiz, quiz_at, company_id, course_id, exam_title AS title, time_start, time_finish FROM exam
      WHERE company_id = ? AND course_id = ? AND jenis = ? AND quiz = ? AND exam_id = ?`;
    let data = [form.companyId, form.pelajaranId, '1', tipe, rows.insertId];
    db.query(sql, data, (err, rows) => {
      res.json({error: false, result: rows.length ? rows[0] : []})
    })
  })
}

exports.getAllKuis = (req, res) => {
  let sql = `
    SELECT lj.*, lp.nama_pelajaran, lp.deskripsi, lk.kelas_nama, rm.nama_ruangan, lg.nama AS pengajar, rm.folder
    FROM learning_jadwal lj JOIN learning_pelajaran lp ON lp.pelajaran_id = lj.pelajaran_id
  	  JOIN learning_kelas lk ON lk.kelas_id = lj.kelas_id
      JOIN ruangan_mengajar rm ON rm.id = lj.ruangan_id
      JOIN learning_guru lg ON lg.id = rm.pengajar
    WHERE lj.jadwal_id = ?`

  db.query(sql, [req.params.id], (err, rows) => {
    if(err) res.json({ error: true, result: err })

    let pelajaranId = rows.length ? rows[0].pelajaran_id : 0;
    let tipe = req.params.tipe == "kuis" ? "1" : req.params.tipe == "tugas" ? "2" : "0";

    let sql = `SELECT exam_id AS id, tipe_jawab, jenis, quiz, quiz_at, company_id, course_id, exam_title AS title, time_start, time_finish FROM exam
      WHERE jenis = ? AND quiz = ? AND course_id = ? AND jadwal_id = ?`;
    let data = ['1', tipe, pelajaranId, req.params.id];

    db.query(sql, data, (err, rows) => {
      res.json({error: false, result: rows})
    })

  })
}

exports.getSilabusByJadwal = (req, res) => {
  let { id } = req.params;

  let tahunAjaran = '';
  if(req.query.tahunAjaran) {
    tahunAjaran = req.query.tahunAjaran;
  }
  else {
    let d = new Date();
    // bulan diawali dengan 0 = januari, 11 = desember
    let month = d.getMonth();
    tahunAjaran = month < 6 ? (d.getFullYear()-1)+'/'+d.getFullYear() : d.getFullYear()+'/'+(d.getFullYear()+1);
  }

  let sql = `
    SELECT lj.*, lp.nama_pelajaran, lp.deskripsi, lk.kelas_nama, rm.nama_ruangan, lg.nama AS pengajar, rm.folder
    FROM learning_jadwal lj JOIN learning_pelajaran lp ON lp.pelajaran_id = lj.pelajaran_id
  	  JOIN learning_kelas lk ON lk.kelas_id = lj.kelas_id
      JOIN ruangan_mengajar rm ON rm.id = lj.ruangan_id
      JOIN learning_guru lg ON lg.id = rm.pengajar
    WHERE lj.jadwal_id = ?`

  db.query(sql, [id], (err, rows) => {
    if(err) res.json({ error: true, result: err })

    let pelajaranId = rows.length ? rows[0].pelajaran_id : 0;
    let sql = ``, data = [];

    if(req.query.userId) {
      // for Murid
      sql = `
        SELECT
        	lps.*, ljk.absen_jam, ljk.user_id, cc.chapter_id, cc.course_id AS jadwal_id, cc.silabus_id,
            cc.chapter_title, cc.chapter_body, cc.start_date, cc.end_date,
            cc.jenis_pembelajaran, cc.attachment_id AS attachments, cc.tatapmuka
        FROM learning_pelajaran_silabus lps
        	LEFT JOIN course_chapter cc ON cc.silabus_id = lps.id AND cc.jenis_pembelajaran = 'learning' AND cc.jadwal_id = ?
            LEFT JOIN learning_jadwal_kehadiran ljk ON ljk.sesi_id = cc.chapter_id AND ljk.user_id = ?
        WHERE lps.pelajaran_id = ?
            ORDER BY lps.sesi, cc.start_date ASC`
      data = [id, req.query.userId, pelajaranId]
    } else {
      sql = `SELECT lps.*, cc.chapter_id, cc.course_id AS jadwal_id, cc.silabus_id, cc.chapter_title, cc.chapter_body, cc.start_date, cc.end_date, cc.jenis_pembelajaran, cc.attachment_id AS attachments, cc.tatapmuka
        FROM learning_pelajaran_silabus lps
        	LEFT JOIN course_chapter cc ON cc.silabus_id = lps.id AND cc.jenis_pembelajaran = 'learning'  AND cc.jadwal_id = ?
        WHERE lps.pelajaran_id = ?
        ORDER BY lps.sesi, cc.start_date ASC`;
      data = [id, pelajaranId]
    }

    db.query(sql, data, async (err, rows) => {

      for(var i=0; i<rows.length; i++) {

        // TUGAS ===============================================================
        let sqlTugas = ``, dataTugas = [];
        if(req.query.userId) {
          sqlTugas = `SELECT erl.*, erl.created_at AS submit_at, ce.*, e.jenis, e.quiz, e.exam_title, e.time_start, e.time_finish
            FROM chapter_exam ce
            JOIN exam e ON e.exam_id = ce.exam_id AND e.jenis = '1' AND e.quiz = '2'
            LEFT JOIN exam_tugas_answer erl ON erl.exam_id = e.exam_id AND erl.user_id = ?
            WHERE ce.chapter_id = ?`
          dataTugas = [req.query.userId, rows[i].chapter_id ? rows[i].chapter_id : null]
        }
        else {
          sqlTugas = `SELECT ce.*, e.jenis, e.quiz, e.exam_title, e.time_start, e.time_finish
            FROM chapter_exam ce
            JOIN exam e ON e.exam_id = ce.exam_id AND e.jenis = '1' AND e.quiz = '2'
            WHERE ce.chapter_id = ?`
          dataTugas = [rows[i].chapter_id ? rows[i].chapter_id : null]
        }
        let tugas = await db.query(sqlTugas, dataTugas)
        rows[i].tugas = tugas;
        // END =================================================================

        // KUIS ================================================================
        let sqlKuis = ``, dataKuis = [];
        if(req.query.userId) {
          sqlKuis = `SELECT erl.*, erl.created_at AS submit_at, ce.*, e.jenis, e.quiz, e.exam_title, e.time_start, e.time_finish
            FROM chapter_exam ce
            JOIN exam e ON e.exam_id = ce.exam_id AND e.jenis = '1' AND e.quiz = '1'
            LEFT JOIN exam_result_learning erl ON erl.exam_id = e.exam_id AND erl.user_id = ?
            WHERE ce.chapter_id = ?`
          dataKuis = [req.query.userId, rows[i].chapter_id ? rows[i].chapter_id : null]
        }
        else {
          sqlKuis = `SELECT ce.*, e.jenis, e.quiz, e.exam_title, e.time_start, e.time_finish
            FROM chapter_exam ce
            JOIN exam e ON e.exam_id = ce.exam_id AND e.jenis = '1' AND e.quiz = '1'
            WHERE ce.chapter_id = ?`
          dataKuis = [rows[i].chapter_id ? rows[i].chapter_id : null]
        }
        let kuis = await db.query(sqlKuis, dataKuis)
        rows[i].kuis = kuis;
        // END =================================================================

        // UJIAN ===============================================================
        let sqlUjian = ``, dataUjian = [];
        if(req.query.userId) {
          sqlUjian = `SELECT erl.*, erl.created_at AS submit_at, ce.*, e.jenis, e.quiz, e.exam_title, e.time_start, e.time_finish
            FROM chapter_exam ce
            JOIN exam e ON e.exam_id = ce.exam_id AND e.jenis = '1' AND e.quiz = '0'
            LEFT JOIN exam_result_learning erl ON erl.exam_id = e.exam_id AND erl.user_id = ?
            WHERE ce.chapter_id = ?`
          dataUjian = [req.query.userId, rows[i].chapter_id ? rows[i].chapter_id : null]
        }
        else {
          sqlUjian = `SELECT ce.*, e.jenis, e.quiz, e.exam_title, e.time_start, e.time_finish
            FROM chapter_exam ce
            JOIN exam e ON e.exam_id = ce.exam_id AND e.jenis = '1' AND e.quiz = '0'
            WHERE ce.chapter_id = ?`
          dataUjian = [rows[i].chapter_id ? rows[i].chapter_id : null]
        }

        let ujian = await db.query(sqlUjian, dataUjian)
        rows[i].ujian = ujian;
        // END =================================================================

      }

      res.json({ error: false, result: rows.length ? rows : [] })
    })

  })

}

exports.getOneKuis = (req, res) => {
  let tipe = req.params.tipe == "kuis" ? "1" : req.params.tipe == "tugas" ? "2" : "0";

  let sql = `SELECT exam_id AS id, tipe_jawab, tatapmuka, jenis, quiz, quiz_at, is_started, company_id, course_id, exam_title AS title, time_start, time_finish FROM exam
    WHERE jenis = ? AND quiz = ? AND exam_id = ?`;
  let data = ['1', tipe, req.params.id];
  db.query(sql, data, (err, rows) => {
    res.json({error: false, result: rows.length ? rows[0] : []})
  })
}

exports.updateKuis = (req, res) => {
  let tipe = req.params.tipe == "kuis" ? "1" : req.params.tipe == "tugas" ? "2" : "0";

  let form = {
    title: req.body.title,
    quizAt: req.body.quizAt,
    tatapmuka: req.body.tatapmuka,
    tanggalMulai: req.body.tanggalMulai,
    tanggalAkhir: req.body.tanggalAkhir,
    tipeJawab: req.body.tipeJawab ? req.body.tipeJawab : '1',
  }
  let sql = `UPDATE exam SET tipe_jawab = ?, tatapmuka = ?, exam_title = ?, quiz_at = ?, time_start = ?, time_finish = ? WHERE exam_id = ?`;
  let data = [form.tipeJawab, form.tatapmuka, form.title, form.quizAt, form.tanggalMulai, form.tanggalAkhir, req.params.id];
  db.query(sql, data, (err, rows) => {
    if(err) res.json({ error: true, result: err })

    let sql = `SELECT exam_id AS id, tipe_jawab, tatapmuka, jenis, quiz, quiz_at, company_id, course_id, exam_title AS title, time_start, time_finish FROM exam
      WHERE jenis = ? AND quiz = ? AND exam_id = ?`;
    let data = ['1', tipe, req.params.id];
    db.query(sql, data, (err, rows) => {
      res.json({error: false, result: rows.length ? rows[0] : []})
    })
  })
}

exports.deleteKuis = (req, res) => {
  let sql = `DELETE FROM exam WHERE exam_id = ?`;
  db.query(sql, [req.params.id], (err, rows) => {
    if(err) res.json({ error: true, result: err })
    res.json({error: false, result: rows})
  })
}

exports.importPertanyaan = (req, res) => {
  UploadExcel(req, res, (err) => {
      if(!req.file) {
          res.json({ error: true, result: err });
      } else {

          let form = {
            examId: req.body.examId
          }

          // create class Excel
          var Excel = require('exceljs');
          var wb = new Excel.Workbook();
          var path = require('path');
          var filePath = path.resolve(__dirname, '../public/company/'+req.file.filename);

          wb.xlsx.readFile(filePath).then( async function(){
              var sh = wb.getWorksheet("Sheet1");

              //Get all the rows data [1st and 2nd column]
              var webinarId = req.body.webinarId;
              var tempArray = [];
              for (i = 2; i <= sh.rowCount; i++) {

                  console.log('==========================================================');

                  if(req.params.tipe) {
                    // jika tugas
                    // 1  |     2
                    // No | Pertanyaan
                    let dataPertanyaan = [[form.examId, sh.getRow(i).getCell(2).value]];
                    await db.query(`INSERT INTO exam_question (exam_id, question) VALUES (?)`, dataPertanyaan);
                  }
                  else {
                    // 1  |     2      | 3 | 4 | 5 | 6 | 7 | 8
                    // No | Pertanyaan | A | B | C | D | E | Jawaban
                    let dataPertanyaan = [[form.examId, sh.getRow(i).getCell(2).value, sh.getRow(i).getCell(8).value]];
                    let getPertanyaan = await db.query(`INSERT INTO exam_question (exam_id, question, correct_option) VALUES (?)`, dataPertanyaan);

                    let options = ['A','B','C','D','E'];
                    options.map(async (item, index) => {
                      let data = [[getPertanyaan.insertId, item, sh.getRow(i).getCell(index+3).value]];
                      await db.query(`INSERT INTO exam_option (question_id, exam_option, description) VALUES (?)`, data);
                    })

                  }

                  console.log('==========================================================');

              }

              fs.unlinkSync(filePath);

              res.json({ error: false, result: form.examId });

          })
      }
  });
}

exports.createPertanyaan = async (req, res) => {
  let form = {
    examId: req.body.examId,
    pertanyaan: req.body.pertanyaan
  }

  let refactory = [];
  if(form.pertanyaan.length) {
    for(var i=0; i<form.pertanyaan.length; i++) {

      if(form.pertanyaan[i].id) {
        // update
        let sql = 'UPDATE exam_question SET question = ?, correct_option = ? WHERE question_id = ?';
        let data = [form.pertanyaan[i].tanya,  form.pertanyaan[i].jawaban, form.pertanyaan[i].id];
        let getPertanyaan =  await db.query(sql, data);

        let options = ['A','B','C','D','E'];
        options.map(async item => {
          let data = [form.pertanyaan[i][item.toLowerCase()], form.pertanyaan[i].id, item];
          console.log(data)
          let get = await db.query(`UPDATE exam_option SET description = ? WHERE question_id = ? AND exam_option = ?`, data);
        })

        refactory.push(form.pertanyaan[i]);
      }
      else {
        // insert
        let dataPertanyaan = [[form.examId, form.pertanyaan[i].tanya, form.pertanyaan[i].jawaban]];
        let getPertanyaan = await db.query(`INSERT INTO exam_question (exam_id, question, correct_option) VALUES (?)`, dataPertanyaan);

        if(!req.query.tugas) {
          let options = ['A','B','C','D','E'];
          options.map(async item => {
            let data = [[getPertanyaan.insertId, item, form.pertanyaan[i][item.toLowerCase()]]];
            let get = await db.query(`INSERT INTO exam_option (question_id, exam_option, description) VALUES (?)`, data);
          })
        }

        form.pertanyaan[i].id = getPertanyaan.insertId;
        refactory.push(form.pertanyaan[i]);
      }
    }

    res.json({ error: false, result: refactory })
  }
}

exports.submitMuridKuisUjian = (req, res, next) => {
  let t1 = req.query.retake ? 'retake_exam_answer' : 'exam_answer'
  let t2 = req.query.retake ? 'retake_exam_result' : 'exam_result_learning'

	db.query(`SELECT q.exam_id, q.question_id, q.question, q.correct_option, a.*
			FROM ${t1} a RIGHT JOIN exam_question q ON q.question_id = a.question_id AND a.user_id = '${req.body.userId}'
			WHERE q.exam_id = '${req.body.examId}' ORDER BY answer_number DESC`,
			async (error, result, fields) => {
		if(error) {
			res.json({error: true, result: error})
		} else {
			// check result is 0
      let form = {};
			if(result.length != 0) {
				let correctOption = 0;
				for(let i=0; i<result.length; i++) {
					if(result[i].correct_option === result[i].answer_option) { correctOption++; }
				}
				form.benar = correctOption;
				form.salah = result.length - correctOption;
				form.score = (correctOption / result.length) * 100

        let cekHasil = await db.query(`SELECT * FROM ${t2} WHERE user_id = ? AND exam_id = ?`, [req.body.userId, req.body.examId])
        if(cekHasil.length) {
          let dataUpdate = [form.benar, form.salah, form.score, req.body.userId, req.body.examId]
          db.query(`UPDATE ${t2} SET total_correct = ?, total_uncorrect = ?, score = ? WHERE user_id = ? AND exam_id = ?`, dataUpdate)
        }
        else {
          let data = [[req.body.userId, req.body.examId, form.benar, form.salah, form.score]];
          db.query(`INSERT INTO ${t2} (user_id, exam_id, total_correct, total_uncorrect, score) VALUES (?)`, data)
        }

				res.json({ error: false, result: form })
			}
		}
	})
}

exports.jawabMuridKuisUjian = async (req, res) => {
  let t1 = ''
  if(req.query.retake) {
    t1 = 'retake_exam_answer'
  }
  else {
    t1 = 'exam_answer'
  }

  let numRow = await db.query(`SELECT * FROM ${t1} WHERE user_id = '${req.body.userId}' AND question_id = '${req.body.questionId}'`);
	if(numRow.length !== 0) {
		let sql = `UPDATE ${t1} SET answer_option = ? WHERE user_id = ? AND question_id = ?`;
    db.query(sql, [req.body.jawaban, req.body.userId, req.body.questionId], (err, rows) => {
      res.json({ error: false, result: rows });
    });
	} else {
		db.query(`INSERT INTO ${t1} (question_id, user_id, answer_option)
			VALUES ('${req.body.questionId}', '${req.body.userId}', '${req.body.jawaban}')`,
			(error, result, fields) => {
			if(error) {
				res.json({ error: true, result: error });
			} else {
				db.query(`SELECT * FROM ${t1} WHERE answer_id = '${result.insertId}'`, (error, result, fields) => {
					res.json({ error: false, result: result[0] })
				})
			}
		})
	}
}

exports.getAllPertanyaan = (req, res) => {
  console.log(req.query);
  console.log(`Query: ${req.query.tugas ? 'true' : 'false'}`)

  let examId = req.params.id;
  // res.json({ error: false, result: examId });
  let sql = `SELECT question_id AS id, question AS tanya, penjelasan, correct_option AS jawaban FROM exam_question WHERE exam_id = ?`;
  db.query(sql, [examId], async (err, rows) => {

    console.log(rows)

    if(err) res.json({error: true, result: err})

    if(!req.query.tugas) {

      if(rows.length) {
        for(var i=0; i<rows.length; i++) {
          let getJawaban = await db.query(`SELECT * FROM exam_option WHERE question_id = ?`, [rows[i].id]);
          await getJawaban.map(item => {
            rows[i][item.exam_option.toLowerCase()] = item.description;
          })
        }

        res.json({ error: false, result: rows });
      }
      else {
        res.json({ error: false, result: rows });
      }

    }
    else {
      res.json({ error: false, result: rows });
    }

  })
}

exports.deletePertanyaan = (req, res) => {
  let sql = `DELETE FROM exam_question WHERE question_id = ?`;
  db.query(sql, [req.params.id], (err, rows) => {

    db.query(`DELETE FROM exam_option WHERE question_id = ?`, [req.params.id]);

    if(err) res.json({ error: true, result: err })
    res.json({error: false, result: rows})
  })
}

exports.getPelajaranByJadwalId = (req, res) => {
  let sql = `SELECT * FROM learning_jadwal WHERE jadwal_id = ?`;
  db.query(sql, [req.params.jadwalId], async (err, rows) => {
    if(err) res.json({error: true, result: err})

    if(rows.length) {
      db.query(`SELECT * FROM course_chapter WHERE jenis_pembelajaran = 'learning' AND course_id = ?`, [req.params.jadwalId], async (err, rows) => {
        if(err) res.json({ error: true, result: err })

        rows.map(async (item) => {
          let tugas = await db.query(`SELECT * FROM exam WHERE jenis = '1' AND quiz = '2' AND quiz_at = ?`, [item.chapter_id]);
          item.tugas = tugas.length ? tugas : []
        })

        let chapters = rows;
        let quiz = await db.query(`SELECT * FROM exam WHERE jenis = '1' AND quiz = '1' AND course_id = ? ORDER BY quiz_at ASC`, [req.params.jadwalId]);
        let ujian = await db.query(`SELECT * FROM exam WHERE jenis = '1' AND quiz = '0' AND course_id = ?`, [req.params.jadwalId]);

        let refactoryChapters = [...chapters];
        for (let i = 0; i < quiz.length; i++) {
          for (let j = 0; j < chapters.length; j++) {
            if (quiz[i].quiz_at === chapters[j].chapter_id) {
              if (j === 0) {
                refactoryChapters.splice(
                  chapters.indexOf(chapters[j]) + 1,
                  0,
                  quiz[i]
                );
              } else {
                refactoryChapters.splice(
                  chapters.indexOf(chapters[j]) + 1 + i,
                  0,
                  quiz[i]
                );
              }
            }
          }
        }

        res.json({ error: false, result: refactoryChapters })
      });

    } else {
      res.json({ error: false, result: [] })
    }
  })
}

exports.getDetailPelajaranByJadwalId = (req, res) => {
  let sql = `SELECT lj.*, lp.nama_pelajaran, rm.nama_ruangan, rm.pengajar AS pengajar_id, lg.nama, lg.user_id, u.name
    FROM learning_jadwal lj
    	JOIN learning_pelajaran lp ON lp.pelajaran_id = lj.pelajaran_id
        JOIN ruangan_mengajar rm ON rm.id = lj.ruangan_id
        JOIN learning_guru lg ON lg.id = rm.pengajar
        JOIN user u ON u.user_id = lg.user_id
    WHERE lj.jadwal_id = ?`
  let data = [req.params.jadwalId];
  db.query(sql, data, (error, rows) => {
    if(error) res.json({error: true, result: err})

    res.json({ error: false, result: rows })
  })
}

exports.getMuridAllPertanyaan = (req, res) => {
  let examId = req.params.id;

  let t1 = req.query.retake ? 'retake_exam_answer' : 'exam_answer'
  // res.json({ error: false, result: examId });
  let sql = `
  SELECT eq.question_id AS id, eq.question AS tanya, eq.penjelasan, eq.correct_option AS jawaban
  FROM exam_question eq
  WHERE eq.exam_id = ?`;
  db.query(sql, [examId], async (err, rows) => {
    if(err) res.json({error: true, result: err})

    if(!req.query.tugas) {

      if(rows.length) {
        for(var i=0; i<rows.length; i++) {
          let getJawaban = await db.query(`SELECT * FROM exam_option WHERE question_id = ?`, [rows[i].id]);
          await getJawaban.map(item => {
            rows[i][item.exam_option.toLowerCase()] = item.description;
          })

          if(req.params.userId) {
            let myJawaban = await db.query(`SELECT * FROM ${t1} WHERE question_id = ? AND user_id = ?`, [rows[i].id, req.params.userId]);
            rows[i].myJawaban = myJawaban
          }
        }

        res.json({ error: false, result: rows });
      }
      else {
        res.json({ error: false, result: rows });
      }

    }
    else {
      res.json({ error: false, result: rows });
    }

  })
}

exports.getMengumpulkanTugasByExamId = (req, res) => {
  let kelas = req.query.kelas;
  let sql = `SELECT
    	lkm.murid_id, lm.user_id, lm.nama, lm.no_induk,
    	lj.jadwal_id, lj.kelas_id,
    	lps.sesi, lps.jenis,
    	ce.chapter_id, cc.jenis_pembelajaran, cc.silabus_id, cc.chapter_title, e.exam_id, e.jenis, e.quiz, e.exam_title,
    	e.time_start, e.time_finish,
        eta.answer_id, eta.answer_file, eta.answer_deskripsi, eta.score, eta.created_at AS pengumpulan, e.tipe_jawab
    FROM
    	exam e
    	JOIN chapter_exam ce ON ce.exam_id = e.exam_id
        JOIN course_chapter cc ON cc.chapter_id = ce.chapter_id
        JOIN learning_pelajaran_silabus lps ON lps.id = cc.silabus_id
        JOIN learning_jadwal lj ON lj.pelajaran_id = lps.pelajaran_id AND lj.kelas_id = ?
        JOIN learning_kelas_murid lkm ON lkm.kelas_id = lj.kelas_id
        JOIN learning_murid lm ON lm.id = lkm.murid_id
    	  LEFT JOIN exam_tugas_answer eta ON eta.exam_id = e.exam_id AND eta.user_id = lm.user_id
    WHERE
    	e.exam_id = ?`
  let data = [kelas, req.params.examId]
  db.query(sql, data, (err, rows) => {
    if(err) {
      res.json({error: true, result: err})
    } else {
      res.json({ error: false, result: rows })
    }
  })
}

exports.getMengumpulkanScoreByExamId = (req, res) => {
  let kelas = req.query.kelas;
  let sql = `SELECT
    	lkm.murid_id, lm.user_id, lm.nama, lm.no_induk,
    	lj.jadwal_id, lj.kelas_id,
    	lps.sesi, lps.jenis,
    	ce.chapter_id, cc.jenis_pembelajaran, cc.silabus_id, cc.chapter_title, e.exam_id, e.jenis, e.quiz, e.exam_title,
    	e.time_start, e.time_finish,
        eta.id, eta.total_correct, eta.total_uncorrect, eta.score, eta.created_at AS pengumpulan
    FROM
    	exam e
    	JOIN chapter_exam ce ON ce.exam_id = e.exam_id
        JOIN course_chapter cc ON cc.chapter_id = ce.chapter_id
        JOIN learning_pelajaran_silabus lps ON lps.id = cc.silabus_id
        JOIN learning_jadwal lj ON lj.pelajaran_id = lps.pelajaran_id AND lj.kelas_id = ?
        JOIN learning_kelas_murid lkm ON lkm.kelas_id = lj.kelas_id
        JOIN learning_murid lm ON lm.id = lkm.murid_id
    	  LEFT JOIN exam_result_learning eta ON eta.exam_id = e.exam_id AND eta.user_id = lm.user_id
    WHERE
    	e.exam_id = ?`
  let data = [kelas, req.params.examId]
  db.query(sql, data, (err, rows) => {
    if(err) {
      res.json({error: true, result: err})
    } else {
      res.json({ error: false, result: rows })
    }
  })
}

exports.getMengumpulkanTugas = async (req, res) => {
  let sql = `SELECT eta.*, lm.id AS murid_id, lm.nama
    FROM exam_tugas_answer eta
        JOIN learning_murid lm ON lm.user_id = eta.user_id
    WHERE eta.exam_id = ?`
  let data = [req.params.examId];
  let mengumpulkan = await db.query(sql, data);
  let getUserId = [];
  mengumpulkan.map(item => getUserId.push(item.user_id));

  let sqlMurid = `SELECT lj.jadwal_id, lj.kelas_id, lm.*
    FROM learning_jadwal lj JOIN learning_kelas_murid lkm ON lkm.kelas_id = lj.kelas_id JOIN learning_murid lm ON lm.id = lkm.murid_id
    WHERE lj.jadwal_id = ?`;
  let getMurid = await db.query(sqlMurid, [req.params.jadwalId]);

  let tidakmengumpulkan = getMurid.filter(item => !getUserId.includes(item.user_id))

  res.json({
    error: false,
    result: {
      sudahMengumpulkan: mengumpulkan,
      belumMengumpulkan: tidakmengumpulkan
    }
  })
}

exports.getMengumpulkanKuis = async (req, res) => {
  let sql = `SELECT eta.*, lm.id AS murid_id, lm.nama
    FROM exam_result_learning eta
        JOIN learning_murid lm ON lm.user_id = eta.user_id
    WHERE eta.exam_id = ?`
  let data = [req.params.examId];
  let mengumpulkan = await db.query(sql, data);
  let getUserId = [];
  mengumpulkan.map(item => getUserId.push(item.user_id));

  let sqlMurid = `SELECT lj.jadwal_id, lj.kelas_id, lm.*
    FROM learning_jadwal lj JOIN learning_kelas_murid lkm ON lkm.kelas_id = lj.kelas_id JOIN learning_murid lm ON lm.id = lkm.murid_id
    WHERE lj.jadwal_id = ?`;
  let getMurid = await db.query(sqlMurid, [req.params.jadwalId]);

  let tidakmengumpulkan = getMurid.filter(item => !getUserId.includes(item.user_id))

  res.json({
    error: false,
    result: {
      sudahMengumpulkan: mengumpulkan,
      belumMengumpulkan: tidakmengumpulkan
    }
  })
}

exports.getDetailTugas = (req, res) => {
  let sql = `SELECT u.name, eta.*
    FROM exam_tugas_answer eta
    	JOIN user u ON u.user_id = eta.user_id
    WHERE eta.answer_id = ?`;
  db.query(sql, [req.params.id], (err, rows) => {
    if(err) res.json({ error: true, result: err })

    res.json({ error: false, result: rows.length ? rows[0] : [] })
  })
}


exports.getNilaiMurid = async (req, res) => {
  let tahunAjaran = '';
  if(req.query.tahunAjaran) {
    tahunAjaran = req.query.tahunAjaran;
  }
  else {
    let d = new Date();
    // bulan diawali dengan 0 = januari, 11 = desember
    let month = d.getMonth();
    tahunAjaran = month < 6 ? (d.getFullYear()-1)+'/'+d.getFullYear() : d.getFullYear()+'/'+(d.getFullYear()+1);
  }

  // :semesterId/:kelasId/:userId
  let { semesterId, kelasId, userId } = req.params;

  // GET PELAJARAN
  let sql = `SELECT DISTINCT lj.pelajaran_id, lj.jadwal_id, lp.nama_pelajaran, lj.kelas_id, lk.kelas_nama
  FROM learning_jadwal lj
  JOIN learning_pelajaran lp ON lp.pelajaran_id = lj.pelajaran_id
  JOIN learning_kelas lk ON lk.kelas_id = lj.kelas_id AND lk.tahun_ajaran = ?
  WHERE lj.kelas_id = ?`;

  db.query(sql, [tahunAjaran, kelasId], async (err, rows) => {
    if(err) res.json({ error: true, result: rows })

    // LOOPING PELAJARAN
    let pTugas = 0.3, pKuis = 0.3, pUjian = 0.4;

    for(var i=0; i<rows.length; i++) {
      // ===================== PROSENTASE =================================== //
      let prosentase = await db.query(`SELECT * FROM learning_pelajaran_nilai WHERE pelajaran_id = ?`, [rows[i].pelajaran_id])
      if(prosentase.length === 1) {
        pTugas = prosentase[0].tugas / 100;
        pKuis = prosentase[0].kuis / 100;
        pUjian = prosentase[0].ujian / 100;
      }

      // ===================== TUGAS ======================================== //
      let sqlTotalTugas = `SELECT e.exam_id, e.course_id, lp.pelajaran_id, lp.nama_pelajaran, e.exam_title AS title, e.time_start, e.time_finish
        FROM exam e
      	  JOIN chapter_exam ce ON ce.exam_id = e.exam_id AND e.jadwal_id = ?
          JOIN course_chapter cc ON cc.chapter_id = ce.chapter_id AND cc.jadwal_id = ?
          JOIN learning_pelajaran_silabus lps ON lps.id = cc.silabus_id
          JOIN learning_jadwal lj ON lj.pelajaran_id = lps.pelajaran_id
          JOIN learning_kelas lk ON lk.kelas_id = lj.kelas_id AND lk.tahun_ajaran = ?

          JOIN learning_pelajaran lp ON lps.pelajaran_id = lp.pelajaran_id
         WHERE e.jenis = '1'
            AND e.quiz = '2'
            AND lj.kelas_id = ?
            AND lj.pelajaran_id = ?`
      let totalTugas = await db.query(sqlTotalTugas, [rows[i].jadwal_id, rows[i].jadwal_id, tahunAjaran, rows[i].kelas_id, rows[i].pelajaran_id]);
      rows[i].totalTugas = totalTugas;

      let sqlKumpulTugas = `SELECT e.exam_id, e.course_id, lp.pelajaran_id, lp.nama_pelajaran, e.exam_title AS title, e.time_start, e.time_finish, eta.user_id, eta.score
        FROM exam e
          JOIN chapter_exam ce ON ce.exam_id = e.exam_id  AND e.jadwal_id = ?
          JOIN course_chapter cc ON cc.chapter_id = ce.chapter_id AND cc.jadwal_id = ?
          JOIN learning_pelajaran_silabus lps ON lps.id = cc.silabus_id
          JOIN learning_jadwal lj ON lj.pelajaran_id = lps.pelajaran_id
          JOIN learning_kelas lk ON lk.kelas_id = lj.kelas_id AND lk.tahun_ajaran = ?

          JOIN learning_pelajaran lp ON lps.pelajaran_id = lp.pelajaran_id
          JOIN learning_kelas_murid lkm ON lkm.kelas_id = lj.kelas_id
          JOIN learning_murid lm ON lm.id = lkm.murid_id AND lm.user_id = ?
          LEFT JOIN exam_tugas_answer eta ON eta.exam_id = e.exam_id AND eta.user_id = lm.user_id
        WHERE e.jenis = '1'
            AND e.quiz = '2'
            AND lj.kelas_id = ?
            AND lj.pelajaran_id = ?`;
      let totalKumpulTugas = await db.query(sqlKumpulTugas, [rows[i].jadwal_id, rows[i].jadwal_id, tahunAjaran, userId, rows[i].kelas_id, rows[i].pelajaran_id]);
      rows[i].kumpulTugas = totalKumpulTugas;

      let totalScoreTugas = 0;
      totalKumpulTugas.map(item => totalScoreTugas += item.score);
      let totalAkhirScoreTugas = (totalScoreTugas/totalTugas.length) * pTugas;
      rows[i].totalAkhirScoreTugas = totalAkhirScoreTugas ? totalAkhirScoreTugas : 0;
      // ===================== END TUGAS ==================================== //

      // ===================== KUIS ========================================= //
      let sqlTotalKuis = `SELECT e.exam_id, e.course_id, lp.pelajaran_id, lp.nama_pelajaran, e.exam_title AS title, e.time_start, e.time_finish
        FROM exam e
          JOIN chapter_exam ce ON ce.exam_id = e.exam_id AND e.jadwal_id = ?
          JOIN course_chapter cc ON cc.chapter_id = ce.chapter_id AND cc.jadwal_id = ?
          JOIN learning_pelajaran_silabus lps ON lps.id = cc.silabus_id
          JOIN learning_jadwal lj ON lj.pelajaran_id = lps.pelajaran_id
          JOIN learning_kelas lk ON lk.kelas_id = lj.kelas_id AND lk.tahun_ajaran = ?

          JOIN learning_pelajaran lp ON lps.pelajaran_id = lp.pelajaran_id
        WHERE e.jenis = '1'
          AND e.quiz = '1'
          AND lj.kelas_id = ?
          AND lj.pelajaran_id = ?`
      let totalKuis = await db.query(sqlTotalKuis, [rows[i].jadwal_id, rows[i].jadwal_id, tahunAjaran, rows[i].kelas_id, rows[i].pelajaran_id]);
      rows[i].totalKuis = totalKuis;

      let sqlKumpulKuis = `SELECT e.exam_id, e.course_id, lp.pelajaran_id, lp.nama_pelajaran, e.exam_title AS title, e.time_start, e.time_finish, eta.user_id, eta.score
        FROM exam e
          JOIN chapter_exam ce ON ce.exam_id = e.exam_id AND e.jadwal_id = ?
          JOIN course_chapter cc ON cc.chapter_id = ce.chapter_id AND cc.jadwal_id = ?
          JOIN learning_pelajaran_silabus lps ON lps.id = cc.silabus_id
          JOIN learning_jadwal lj ON lj.pelajaran_id = lps.pelajaran_id
          JOIN learning_kelas lk ON lk.kelas_id = lj.kelas_id AND lk.tahun_ajaran = ?

          JOIN learning_pelajaran lp ON lps.pelajaran_id = lp.pelajaran_id
          JOIN learning_kelas_murid lkm ON lkm.kelas_id = lj.kelas_id
          JOIN learning_murid lm ON lm.id = lkm.murid_id AND lm.user_id = ?
          LEFT JOIN exam_result_learning eta ON eta.exam_id = e.exam_id AND eta.user_id = lm.user_id
        WHERE e.jenis = '1'
          AND e.quiz = '1'
          AND lj.kelas_id = ?
          AND lj.pelajaran_id = ?`;
      let totalKumpulKuis = await db.query(sqlKumpulKuis, [rows[i].jadwal_id, rows[i].jadwal_id, tahunAjaran, userId, rows[i].kelas_id, rows[i].pelajaran_id]);
      rows[i].kumpulKuis = totalKumpulKuis;

      let totalScoreKuis = 0;
      totalKumpulKuis.map(item => totalScoreKuis += item.score);
      let totalAkhirScoreKuis = (totalScoreKuis/totalKuis.length) * pKuis;
      rows[i].totalAkhirScoreKuis = totalAkhirScoreKuis ? totalAkhirScoreKuis : 0;
      // ===================== END KUIS ==================================== //

      // ===================== UJIAN ========================================= //
      let sqlTotalUjian = `SELECT e.exam_id, e.course_id, lp.pelajaran_id, lp.nama_pelajaran, e.exam_title AS title, e.time_start, e.time_finish
        FROM exam e
          JOIN chapter_exam ce ON ce.exam_id = e.exam_id AND e.jadwal_id = ?
          JOIN course_chapter cc ON cc.chapter_id = ce.chapter_id AND cc.jadwal_id = ?
          JOIN learning_pelajaran_silabus lps ON lps.id = cc.silabus_id
          JOIN learning_jadwal lj ON lj.pelajaran_id = lps.pelajaran_id
          JOIN learning_kelas lk ON lk.kelas_id = lj.kelas_id AND lk.tahun_ajaran = ?

          JOIN learning_pelajaran lp ON lps.pelajaran_id = lp.pelajaran_id
        WHERE e.jenis = '1'
          AND e.quiz = '0'
          AND lj.kelas_id = ?
          AND lj.pelajaran_id = ?`
      let totalUjian = await db.query(sqlTotalUjian, [rows[i].jadwal_id, rows[i].jadwal_id, tahunAjaran, rows[i].kelas_id, rows[i].pelajaran_id]);
      rows[i].totalUjian = totalUjian;

      let sqlKumpulUjian = `SELECT e.exam_id, e.course_id, lp.pelajaran_id, lp.nama_pelajaran, e.exam_title AS title, e.time_start, e.time_finish, eta.user_id, eta.score
        FROM exam e
          JOIN chapter_exam ce ON ce.exam_id = e.exam_id AND e.jadwal_id = ?
          JOIN course_chapter cc ON cc.chapter_id = ce.chapter_id AND cc.jadwal_id = ?
          JOIN learning_pelajaran_silabus lps ON lps.id = cc.silabus_id
          JOIN learning_jadwal lj ON lj.pelajaran_id = lps.pelajaran_id
          JOIN learning_kelas lk ON lk.kelas_id = lj.kelas_id AND lk.tahun_ajaran = ?

          JOIN learning_pelajaran lp ON lps.pelajaran_id = lp.pelajaran_id
          JOIN learning_kelas_murid lkm ON lkm.kelas_id = lj.kelas_id
          JOIN learning_murid lm ON lm.id = lkm.murid_id AND lm.user_id = ?
          LEFT JOIN exam_result_learning eta ON eta.exam_id = e.exam_id AND eta.user_id = lm.user_id
        WHERE e.jenis = '1'
          AND e.quiz = '0'
          AND lj.kelas_id = ?
          AND lj.pelajaran_id = ?`;
      let totalKumpulUjian = await db.query(sqlKumpulUjian, [rows[i].jadwal_id, rows[i].jadwal_id, tahunAjaran, userId, rows[i].kelas_id, rows[i].pelajaran_id]);
      rows[i].kumpulUjian = totalKumpulUjian;

      let totalScoreUjian = 0;
      totalKumpulUjian.map(item => totalScoreUjian += item.score);
      let totalAkhirScoreUjian = (totalScoreUjian/totalUjian.length) * pUjian;
      rows[i].totalAkhirScoreUjian = totalAkhirScoreUjian ? totalAkhirScoreUjian : 0;
      // ===================== END UJIAN ==================================== //

      // ===================== PERSENSI ===================================== //
      let sqlCountSesi = `
        SELECT
          cc.company_id, lj.jadwal_id, cc.chapter_id, cc.course_id, cc.chapter_title,
          lj.pelajaran_id, lj.kelas_id, lj.ruangan_id, rm.pengajar, lp.nama_pelajaran,
          ljk.sesi_id, ljk.user_id, ljk.absen_jam

        FROM course_chapter cc
          JOIN learning_pelajaran_silabus lps ON lps.id = cc.silabus_id  AND cc.jenis_pembelajaran = 'learning' AND cc.jadwal_id = ?
          JOIN learning_jadwal lj ON lj.pelajaran_id = lps.pelajaran_id AND lj.jadwal_id = ?
          JOIN learning_kelas lk ON lk.kelas_id = lj.kelas_id AND lk.tahun_ajaran = ?

          JOIN learning_pelajaran lp ON lps.pelajaran_id = lp.pelajaran_id
          JOIN ruangan_mengajar rm ON rm.id = lj.ruangan_id
          LEFT JOIN learning_jadwal_kehadiran ljk ON ljk.sesi_id = cc.chapter_id AND ljk.event = 'materi' AND ljk.user_id = ?

        WHERE
          lj.kelas_id = ? AND
          lj.pelajaran_id = ?`;

      let getSesi = await db.query(sqlCountSesi, [rows[i].jadwal_id, rows[i].jadwal_id, tahunAjaran, userId, rows[i].kelas_id, rows[i].pelajaran_id])
      let countSesi = getSesi.length;
      let absen     = getSesi.filter(item => item.absen_jam != null).length;

      let absenPersen = absen / countSesi * 100;
      rows[i].persensi = absenPersen ? absenPersen.toFixed(2) : 0;
      // ==================== END PERSENSI ================================== //
    }

    rows.map(item => {
      item.rata = 0;
      item.tugas = 0;
      item.kuis = 0;
      item.ujian = 0;
    })

    res.json({ error: false, result: rows })
  })
}

exports.beriNilaiTugas = (req, res) => {
  let sql = `UPDATE exam_tugas_answer SET score = ? WHERE answer_id = ?`;
  db.query(sql, [req.body.score, req.params.id], (err, rows) => {
    if(err) res.json({ error: true, result: err })
    rows.score = req.body.score;
    res.json({ error: false, result: rows })
  })
}

exports.getNilaiKelas = (req, res) => {
  // :kelasId / :pelajaranId

  let tahunAjaran = '';
  if(req.query.tahunAjaran) {
    tahunAjaran = req.query.tahunAjaran;
  }
  else {
    let d = new Date();
    // bulan diawali dengan 0 = januari, 11 = desember
    let month = d.getMonth();
    tahunAjaran = month < 6 ? (d.getFullYear()-1)+'/'+d.getFullYear() : d.getFullYear()+'/'+(d.getFullYear()+1);
  }

  let sql = `SELECT lkm.kelas_id, lk.kelas_nama, rm.id AS ruangan_id, lg.id AS guru_id, lg.user_id AS user_gur_id, lg.nama, lkm.murid_id, lm.user_id, lm.nama, lm.no_induk, lj.pelajaran_id, lj.jadwal_id
    FROM learning_kelas_murid lkm
        JOIN learning_kelas lk ON lk.kelas_id = lkm.kelas_id AND lk.tahun_ajaran = ?
        JOIN learning_murid lm ON lm.id = lkm.murid_id
        JOIN learning_jadwal lj ON lj.kelas_id = lkm.kelas_id AND lj.pelajaran_id = ?
    WHERE lkm.kelas_id = ?`;

  let data = [tahunAjaran, req.params.pelajaranId, req.params.kelasId];

  db.query(sql, data, async (err, rows) => {
    if(err) res.json({ error: true, result: err })

    let pTugas = 0.3, pKuis = 0.3, pUjian = 0.4;

    for(var i=0; i<rows.length; i++) {

      let prosentase = await db.query(`SELECT * FROM learning_pelajaran_nilai WHERE pelajaran_id = ?`, [rows[i].pelajaran_id])
      if(prosentase.length === 1) {
        pTugas = prosentase[0].tugas / 100;
        pKuis = prosentase[0].kuis / 100;
        pUjian = prosentase[0].ujian / 100;
      }

      let sqlTask = `SELECT e.exam_id, e.jenis, e.course_id AS jadwal_id, e.quiz, e.exam_title, e.time_start, e.time_finish, eta.user_id, eta.score
        FROM exam e
        	  JOIN chapter_exam ce ON ce.exam_id = e.exam_id AND e.jadwal_id = ?
            JOIN course_chapter cc ON cc.chapter_id = ce.chapter_id AND cc.jadwal_id = ?
            JOIN learning_pelajaran_silabus lps ON lps.id = cc.silabus_id
            JOIN learning_jadwal lj ON lj.pelajaran_id = lps.pelajaran_id AND lj.jadwal_id = ?
            JOIN learning_kelas lk ON lk.kelas_id = lj.kelas_id AND lk.tahun_ajaran = ?
            JOIN learning_pelajaran lp ON lps.pelajaran_id = lp.pelajaran_id
            LEFT JOIN exam_tugas_answer eta ON eta.exam_id = e.exam_id AND eta.user_id = ?
        WHERE e.jenis = 1 AND e.quiz = 2`;
      let scoreTask = await db.query(sqlTask, [rows[i].jadwal_id, rows[i].jadwal_id, rows[i].jadwal_id, tahunAjaran, rows[i].user_id]);
      let fTask = 0;
      await scoreTask.map(item => fTask += item.score);

      let sqlQuiz = `SELECT e.exam_id, e.jenis, e.course_id AS jadwal_id, e.quiz, e.exam_title, e.time_start, e.time_finish, eta.user_id, eta.score
        FROM exam e
        	  JOIN chapter_exam ce ON ce.exam_id = e.exam_id AND e.jadwal_id = ?
            JOIN course_chapter cc ON cc.chapter_id = ce.chapter_id AND cc.jadwal_id = ?
            JOIN learning_pelajaran_silabus lps ON lps.id = cc.silabus_id
            JOIN learning_jadwal lj ON lj.pelajaran_id = lps.pelajaran_id AND lj.jadwal_id = ?
            JOIN learning_kelas lk ON lk.kelas_id = lj.kelas_id AND lk.tahun_ajaran = ?
            JOIN learning_pelajaran lp ON lps.pelajaran_id = lp.pelajaran_id
            LEFT JOIN exam_result_learning eta ON eta.exam_id = e.exam_id AND eta.user_id = ?
        WHERE e.jenis = 1 AND e.quiz = 1`;
      let scoreQuiz = await db.query(sqlQuiz, [rows[i].jadwal_id, rows[i].jadwal_id, rows[i].jadwal_id, tahunAjaran, rows[i].user_id]);
      let fQuiz = 0;
      await scoreQuiz.map(item => fQuiz += item.score);

      let sqlExam = `SELECT e.exam_id, e.jenis, e.course_id AS jadwal_id, e.quiz, e.exam_title, e.time_start, e.time_finish, eta.user_id, eta.score
        FROM exam e
        	  JOIN chapter_exam ce ON ce.exam_id = e.exam_id AND e.jadwal_id = ?
            JOIN course_chapter cc ON cc.chapter_id = ce.chapter_id AND cc.jadwal_id = ?
            JOIN learning_pelajaran_silabus lps ON lps.id = cc.silabus_id
            JOIN learning_jadwal lj ON lj.pelajaran_id = lps.pelajaran_id AND lj.jadwal_id = ?
            JOIN learning_kelas lk ON lk.kelas_id = lj.kelas_id AND lk.tahun_ajaran = ?
            JOIN learning_pelajaran lp ON lps.pelajaran_id = lp.pelajaran_id
            LEFT JOIN exam_result_learning eta ON eta.exam_id = e.exam_id AND eta.user_id = ?
        WHERE e.jenis = 1 AND e.quiz = 0`;
      let scoreExam = await db.query(sqlExam, [rows[i].jadwal_id, rows[i].jadwal_id, rows[i].jadwal_id, tahunAjaran, rows[i].user_id]);
      let fUjian = 0;
      await scoreExam.map(item => fUjian += item.score);

      rows[i].task = (fTask / scoreTask.length) * pTugas;
      rows[i].quiz = (fQuiz / scoreQuiz.length) * pKuis;
      rows[i].exam = (fUjian / scoreExam.length) * pUjian;
    }

    res.json({ error: false, result: rows })
  })
}

exports.getNilaiRata = (req, res) => {
  let tahunAjaran = '';
  if(req.query.tahunAjaran) {
    tahunAjaran = req.query.tahunAjaran;
  }
  else {
    let d = new Date();
    // bulan diawali dengan 0 = januari, 11 = desember
    let month = d.getMonth();
    tahunAjaran = month < 6 ? (d.getFullYear()-1)+'/'+d.getFullYear() : d.getFullYear()+'/'+(d.getFullYear()+1);
  }

  let sql = `SELECT lkm.kelas_id, lk.kelas_nama, lkm.murid_id, lm.user_id, lm.nama, lm.no_induk, lj.pelajaran_id, lp.nama_pelajaran, lj.jadwal_id
    FROM learning_kelas_murid lkm
        JOIN learning_kelas lk ON lk.kelas_id = lkm.kelas_id AND lk.tahun_ajaran = ?
        JOIN learning_murid lm ON lm.id = lkm.murid_id
        JOIN learning_pelajaran lp ON lp.pelajaran_id = ?
        JOIN learning_jadwal lj ON lj.kelas_id = lkm.kelas_id AND lj.pelajaran_id = ?
    WHERE lkm.kelas_id = ?`;
  let data = [tahunAjaran, req.params.pelajaranId, req.params.pelajaranId, req.params.kelasId];

  db.query(sql, data, async (err, rows) => {
    if(err) res.json({ error: true, result: err })

    let pTugas = 0.3, pKuis = 0.3, pUjian = 0.4;

    let totalTask = 0, totalQuiz = 0, totalExam = 0;

    for(var i=0; i<rows.length; i++) {

      let prosentase = await db.query(`SELECT * FROM learning_pelajaran_nilai WHERE pelajaran_id = ?`, [rows[i].pelajaran_id])
      if(prosentase.length === 1) {
        pTugas = prosentase[0].tugas / 100;
        pKuis = prosentase[0].kuis / 100;
        pUjian = prosentase[0].ujian / 100;
      }

      let sqlTask = `SELECT e.exam_id, e.jenis, e.course_id AS jadwal_id, e.quiz, e.exam_title, e.time_start, e.time_finish, eta.user_id, eta.score
        FROM exam e
            JOIN chapter_exam ce ON ce.exam_id = e.exam_id AND e.jadwal_id = ?
            JOIN course_chapter cc ON cc.chapter_id = ce.chapter_id
            JOIN learning_pelajaran_silabus lps ON lps.id = cc.silabus_id
            JOIN learning_jadwal lj ON lj.pelajaran_id = lps.pelajaran_id AND lj.jadwal_id = ?
            JOIN learning_kelas lk ON lk.kelas_id = lj.kelas_id AND lk.tahun_ajaran = ?

            JOIN learning_pelajaran lp ON lps.pelajaran_id = lp.pelajaran_id
            LEFT JOIN exam_tugas_answer eta ON eta.exam_id = e.exam_id AND eta.user_id = ?
        WHERE e.jenis = 1 AND e.quiz = 2`;
      let scoreTask = await db.query(sqlTask, [rows[i].jadwal_id, rows[i].jadwal_id, tahunAjaran, rows[i].user_id]);
      let fTask = 0;
      await scoreTask.map(item => fTask += item.score);

      let sqlQuiz = `SELECT e.exam_id, e.jenis, e.course_id AS jadwal_id, e.quiz, e.exam_title, e.time_start, e.time_finish, eta.user_id, eta.score
        FROM exam e
            JOIN chapter_exam ce ON ce.exam_id = e.exam_id AND e.jadwal_id = ?
            JOIN course_chapter cc ON cc.chapter_id = ce.chapter_id
            JOIN learning_pelajaran_silabus lps ON lps.id = cc.silabus_id
            JOIN learning_jadwal lj ON lj.pelajaran_id = lps.pelajaran_id AND lj.jadwal_id = ?
            JOIN learning_kelas lk ON lk.kelas_id = lj.kelas_id AND lk.tahun_ajaran = ?

            JOIN learning_pelajaran lp ON lps.pelajaran_id = lp.pelajaran_id
            LEFT JOIN exam_result_learning eta ON eta.exam_id = e.exam_id AND eta.user_id = ?
        WHERE e.jenis = 1 AND e.quiz = 1`;
      let scoreQuiz = await db.query(sqlQuiz, [rows[i].jadwal_id, rows[i].jadwal_id, tahunAjaran, rows[i].user_id]);
      let fQuiz = 0;
      await scoreQuiz.map(item => fQuiz += item.score);

      let sqlExam = `SELECT e.exam_id, e.jenis, e.course_id AS jadwal_id, e.quiz, e.exam_title, e.time_start, e.time_finish, eta.user_id, eta.score
        FROM exam e
            JOIN chapter_exam ce ON ce.exam_id = e.exam_id AND e.jadwal_id = ?
            JOIN course_chapter cc ON cc.chapter_id = ce.chapter_id
            JOIN learning_pelajaran_silabus lps ON lps.id = cc.silabus_id
            JOIN learning_jadwal lj ON lj.pelajaran_id = lps.pelajaran_id AND lj.jadwal_id = ?
            JOIN learning_kelas lk ON lk.kelas_id = lj.kelas_id AND lk.tahun_ajaran = ?

            JOIN learning_pelajaran lp ON lps.pelajaran_id = lp.pelajaran_id
            LEFT JOIN exam_result_learning eta ON eta.exam_id = e.exam_id AND eta.user_id = ?
        WHERE e.jenis = 1 AND e.quiz = 0`;
      let scoreExam = await db.query(sqlExam, [rows[i].jadwal_id, rows[i].jadwal_id, tahunAjaran, rows[i].user_id]);
      let fUjian = 0;
      await scoreExam.map(item => fUjian += item.score);

      rows[i].task = (fTask / scoreTask.length) * pTugas;
      rows[i].quiz = (fQuiz / scoreQuiz.length) * pKuis;
      rows[i].exam = (fUjian / scoreExam.length) * pUjian;

      totalTask += (fTask / scoreTask.length) * pTugas;
      totalQuiz += (fQuiz / scoreQuiz.length) * pKuis;
      totalExam += (fUjian / scoreExam.length) * pUjian;
    }

    let rebuild = [
      {
        kelas_nama: rows[0].kelas_nama,
        pelajaran_nama: rows[0].nama_pelajaran,
        task: totalTask / rows.length,
        quiz: totalQuiz / rows.length,
        exam: totalExam / rows.length,
      }
    ];

    res.json({ error: false, result: rebuild })
  })
}

exports.getSemuaTugas = (req, res) => {
  let sql = `SELECT
	  lj.jadwal_id, lg.id AS guru_id, lg.user_id, lg.nama AS pengajar,
    lp.nama_pelajaran, lk.kelas_nama, lk.tahun_ajaran, lk.semester_id, rm.nama_ruangan, e.exam_id, e.jenis, e.quiz, e.exam_title, e.time_finish

  FROM learning_jadwal lj JOIN learning_pelajaran lp ON lp.pelajaran_id = lj.pelajaran_id

    JOIN learning_kelas lk ON lk.kelas_id = lj.kelas_id
    JOIN ruangan_mengajar rm ON rm.id = lj.ruangan_id
    JOIN learning_guru lg ON lg.id = rm.pengajar
    JOIN exam e ON e.jadwal_id = lj.jadwal_id AND e.quiz = '2' AND e.time_finish >= ?
    JOIN chapter_exam ce ON ce.exam_id = e.exam_id
    JOIN course_chapter cc ON cc.chapter_id = ce.chapter_id

  WHERE lg.user_id = ?`

  let dateNow = moment(new Date()).format('YYYY-MM-DD');

  db.query(sql, [dateNow, req.params.userId], (err, rows) => {
    if(err) res.json({ error: true, result: err })

    res.json({ error: false, result: rows })
  })
}
