var env = require('../env.json');
var db = require('../configs/database');
var conf = require('../configs/config');

var multer = require('multer');
const AWS = require('aws-sdk');
const fs = require('fs');

const s3 = new AWS.S3({
  accessKeyId: env.AWS_ACCESS,
  secretAccessKey: env.AWS_SECRET
});
const storage = multer.diskStorage({
  filename: function (req, file, cb) {
    cb(null, file.originalname);
  }
})
const Attachments = multer({storage: storage}).array('files', 10);
var uploadPengumuman = multer({storage: storage}).single('attachments');

//Create
exports.createPengumuman = (req, res, next) => {
  let data = {
       companyId : req.body.companyId,
       title : req.body.title,
       isi : req.body.isi,
       penerima : req.body.penerima
   }

   let query = 'INSERT INTO pengumuman (id_pengumuman, company_id, title, isi, penerima) VALUES (null, ?, ?, ?, ? )';
   db.query(query, [data.companyId, data.title, data.isi, data.penerima], async (err, rows) => {
       if(err)  res.json({error: true, result:err})

       let getPenerima = data.penerima.split(',');
       // looping role user
       for(var i=0; i<getPenerima.length; i++) {
         let getUsers = await db.query(`SELECT user_id FROM user WHERE company_id = ? AND grup_id = ?`, [data.companyId, getPenerima[i]]);
         // looping user with that role
         for(var j=0; j<getUsers.length; j++) {
           let sql = `INSERT INTO notification (user_id, type, activity_id, description, destination) VALUES (?)`;
           await db.query(sql, [[`${getUsers[j].user_id}`, '4', '0', "Ada pengumuman baru untuk kamu", `${env.APPS_URL}/pengumuman`]]);
         }
       }

       let query = 'SELECT * FROM pengumuman WHERE id_pengumuman = ?';
       db.query(query, [rows.insertId], (err, rows) => {
            res.json({ error: false, result: rows[0] });
       })
   })
}

// Show by id

exports.getById = (req, res) => {

    let query = 'SELECT * FROM pengumuman WHERE id_pengumuman = ?';
    db.query(query, [req.params.id], (err, rows) => {
        if(err)  res.json({error: true, result : err})

         res.json({error: false, result: rows.length ? rows[0] : [] });
    })
}


// Update

exports.updatePengumuman = (req, res) => {
    let data = {
      title: req.body.title,
      isi: req.body.isi,
      penerima : req.body.penerima
    }

    let sql = 'UPDATE pengumuman SET title = ?, isi = ?, penerima = ? WHERE id_pengumuman = ?';
    db.query(sql, [data.title, data.isi, data.penerima, req.params.id], (error, rows) => {
      if(error) res.json({ error: true, result: error })

      let sql = 'SELECT * FROM pengumuman WHERE id_pengumuman = ?';
      db.query(sql, [req.params.id], (error, rows) => {
        res.json({ error: false, result: rows[0] })
      })
    })
}


exports.deletePengumuman = function (req, res) {
    let queryDelete = 'DELETE FROM pengumuman WHERE id_pengumuman = ?';

    db.query(queryDelete, [req.params.id], (err, result) => {
        if(err){
             res.json({error: true, result: err});
        }else{
             res.json({error: false, result: err});
        }
    })
}

exports.getByCompanyId = function ( req, res) {

    let query = 'SELECT * FROM pengumuman WHERE company_id = ?';

    db.query(query, [req.params.id], async (err,rows) => {
        if(err){
            console.log('Failed')
            res.json({ error: true, result: err })
        }else{
            for(var i=0; i<rows.length; i++) {
              let getRoles = await db.query(`SELECT grup_id, grup_name FROM grup WHERE grup_id IN (${rows[i].penerima})`);
              rows[i].penerima = getRoles;
            }
            res.json({ error: false, result: rows})
            console.log('Success')
        }
    })
}

exports.getByUserId = (req, res) => {
  let query = `SELECT * FROM pengumuman WHERE penerima LIKE '%${req.params.id}%'`;

  db.query(query, async (err,rows) => {
      if(err){
          console.log('Failed')
          res.json({ error: true, result: err })
      }else{
          for(var i=0; i<rows.length; i++) {
            let getRoles = await db.query(`SELECT grup_id, grup_name FROM grup WHERE grup_id IN (${rows[i].penerima})`);
            rows[i].penerima = getRoles;
          }
          res.json({ error: false, result: rows})
          console.log('Success')
      }
  })
}


exports.filesPengumuman = (req, res) => {
  Attachments(req, res, async (err) => {
    if(!req.files) {
      res.json({error: true, result: err});
    } else {
      let attachmentId = '';
      for(let i=0; i<req.files.length; i++) {
        let path = req.files[i].path;
        let keyName = `${env.AWS_BUCKET_ENV}/pengumuman`;

        var params = {
          ACL: 'public-read',
          Bucket: env.AWS_BUCKET,
          Body: fs.createReadStream(path),
          Key: `${keyName}/${req.files[i].originalname}`
        };

        let dd = await s3.upload(params).promise();
        fs.unlinkSync(path);
        attachmentId += dd.Location + ',';
      }

      attachmentId = attachmentId.substring(0, attachmentId.length - 1);
      db.query(`UPDATE pengumuman SET attachments = ? WHERE id_pengumuman = ?`, [attachmentId, req.params.id], (error, result, fields) => {
        if(error) {
          res.json({error: true, result: error});
        } else {
          res.json({error: false, result: attachmentId});
        }
      })
    }
  })
}
