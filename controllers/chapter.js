var env = require('../env.json');
var conf = require('../configs/config');
var db = require('../configs/database');

var multer = require('multer');
const storage = multer.diskStorage({
  filename: function (req, file, cb) {
    cb(null, file.originalname);
  }
})
const AWS = require('aws-sdk');
const fs = require('fs');
const s3 = new AWS.S3({
  accessKeyId: env.AWS_ACCESS,
  secretAccessKey: env.AWS_SECRET
});

var uploadLogo = multer({ storage: storage }).single('chapter_video');
var uploadThumbnail = multer({storage: storage}).single('thumbnail');
var uploadFile = multer({storage: storage}).single('file');
var uploadAttachment = multer({storage: storage}).array('attachment_id', 10);

exports.onlyUploadFile = (req, res) => {
  uploadFile(req, res, (err) => {
    if(!req.file) {
      res.json({ error: true, result: err });
    } else {
      let path = req.file.path;
      let newDate = new Date();
      let keyName = `${env.AWS_BUCKET_ENV}/files`;

      var params = {
        ACL: 'public-read',
        Bucket: env.AWS_BUCKET,
        Body: fs.createReadStream(path),
        Key: `${keyName}/${req.file.originalname}`,
        Metadata: {
          'Content-Type': req.file.mimetype,
          'Content-Disposition': 'inline'
        }
      };

      s3.upload(params, (err, data) => {
        if (err) {
          res.json({ error: true, msg: "Error Upload Image", result: err });
        }
        if (data) {
          fs.unlinkSync(path);

          let formData = {
            url: data.Location
          };

          res.json({error: false, result: formData});

        }
      });

    }
  });
}

exports.createChapter = (req, res, next) => {
  uploadLogo(req, res, (err) => {
    if(!req.file) {
      res.json({ error: true, result: err });
    } else {
      let path = req.file.path;
      let newDate = new Date();
      let keyName = `${env.AWS_BUCKET_ENV}/chapter`;

      var params = {
        ACL: 'public-read',
        Bucket: env.AWS_BUCKET,
        Body: fs.createReadStream(path),
        Key: `${keyName}/${req.file.originalname}`
      };

      s3.upload(params, (err, data) => {
        if (err) {
          res.json({ error: true, msg: "Error Upload Image", result: err });
        }
        if (data) {
          fs.unlinkSync(path);

          let formData = {
            course_id: req.body.course_id,
            company_id: req.body.company_id,
            chapter_number: req.body.chapter_number,
            waktu_mulai: req.body.waktu_mulai,
            waktu_selesai: req.body.waktu_selesai,
            title: req.body.chapter_title,
            body: req.body.chapter_body,
            video: data.Location,
            attachment_id: req.body.attachment_id
          };

          let sql = `INSERT INTO course_chapter
          (chapter_id, waktu_mulai, waktu_selesai, course_id, company_id, chapter_number, chapter_title, chapter_body, chapter_video, attachment_id) VALUES
          (null, '${formData.waktu_mulai}', '${formData.waktu_selesai}', '${formData.course_id}', '${formData.company_id}', '${formData.chapter_number}', '${formData.title}', '${formData.body}', '${formData.video}', null)`;

          console.log(sql);

          db.query(sql, (error, result, fields) => {
            if(error) {
              res.json({error: true, result: error});
            } else {
              db.query(`SELECT * FROM course_chapter WHERE chapter_id = '${result.insertId}'`, (error, result, fields) => {
                res.json({error: false, result: result[0]});
              })
            }
          });

        }
      });

    }
  });
};

exports.addCourseChapter = (req, res, next) => {
      let formData = {
        courseId: req.body.courseId,
        chapterId: req.body.chapterId,
        userId: req.body.userId
      };

      const id = formData.courseId+formData.chapterId+formData.userId;

      let sql = `
        INSERT IGNORE INTO user_course_chapter
          (user_course_chapter_id, course_id, chapter_id, user_id)
        VALUES
          ('${id}','${formData.courseId}','${formData.chapterId}','${formData.userId}')
        `;

      db.query(sql, (error, result, fields) => {
        if(error) {
          res.json({error: true, result: result});
        } else {
          res.json({error: false, result: result});
        }
      });
};

exports.getAllChapter = (req, res, next) => {
  db.query(`SELECT * FROM course_chapter`, (error, result, fields) => {
    if(error) {
      res.json({error: true, result: error});
    } else {
      res.json({error: false, result: result});
    }
  });
};

exports.getOneChapter = (req, res, next) => {
  db.query(`SELECT * FROM course_chapter WHERE chapter_id = '${req.params.chapter_id}'`, (error, result, fields) => {
    if(error) {
      res.json({error: true, result: error});
    } else {
      res.json({error: false, result: (result.length !== 0 ) ? result[0] : [] });
    }
  });
};

exports.getChapterWithTugasAndKuis = (req, res) => {
  let sql = `SELECT lps.id AS silabus_id, lps.jenis, lps.topik, lps.tujuan, lps.durasi, cc.*
    FROM course_chapter cc
    	JOIN learning_pelajaran_silabus lps ON lps.id = cc.silabus_id
    WHERE chapter_id = ?`;
  let data = [req.params.id];
  db.query(sql, data, async (error, result, fields) => {
    if(error) {
      res.json({error: true, result: error});
    } else {
      for(var i=0; i<result.length; i++) {

        if(req.query.userId) {
            // for Murid
            let sqlTugas = `
              SELECT e.*, ce.*,
                eta.answer_id, eta.user_id, eta.answer_file, eta.answer_deskripsi, eta.score, eta.created_at AS pengumpulan
              FROM exam e
            	 JOIN chapter_exam ce ON ce.exam_id = e.exam_id AND ce.chapter_id = ? AND e.quiz = 2
               LEFT JOIN exam_tugas_answer eta ON eta.exam_id = e.exam_id AND eta.user_id = ?`
            let runTugas = await db.query(sqlTugas, [result[i].chapter_id, req.query.userId]);

            let sqlKuis = `
              SELECT e.*, ce.*,
                eta.id, eta.user_id, eta.total_correct, eta.total_uncorrect, eta.score, eta.created_at AS pengumpulan
              FROM exam e
            	 JOIN chapter_exam ce ON ce.exam_id = e.exam_id AND ce.chapter_id = ? AND e.quiz = 1
               LEFT JOIN exam_result_learning eta ON eta.exam_id = e.exam_id AND eta.user_id = ?`
            let runKuis = await db.query(sqlKuis, [result[i].chapter_id, req.query.userId]);

            let sqlUjian = `
              SELECT e.*, ce.*,
                eta.id, eta.user_id, eta.total_correct, eta.total_uncorrect, eta.score, eta.created_at AS pengumpulan
              FROM exam e
            	 JOIN chapter_exam ce ON ce.exam_id = e.exam_id AND ce.chapter_id = ? AND e.quiz = 0
               LEFT JOIN exam_result_learning eta ON eta.exam_id = e.exam_id AND eta.user_id = ?`
            let runUjian = await db.query(sqlUjian, [result[i].chapter_id, req.query.userId]);

            result[i].tugas = runTugas
            result[i].kuis = runKuis
            result[i].ujian = runUjian
        }
        else {
          let sql = `SELECT e.*, ce.*
            FROM exam e
            	JOIN chapter_exam ce ON ce.exam_id = e.exam_id AND ce.chapter_id = ?`
          let run = await db.query(sql, [result[i].chapter_id]);
          result[i].tugas = run.filter(item => item.quiz == 2)
          result[i].kuis = run.filter(item => item.quiz == 1)
          result[i].ujian = run.filter(item => item.quiz == 0)
        }
      }

      res.json({error: false, result: (result.length !== 0 ) ? result[0] : [] });
    }
  });
}

exports.getChapterByCourse = (req, res, next) => {
  db.query(`SELECT * FROM course_chapter WHERE course_id = '${req.params.course_id}' ORDER BY chapter_number ASC`, (error, result, fields) => {
    if(error) {
      res.json({error: true, result: error});
    } else {
      res.json({error: false, result: (result.length !== 0 ) ? result : [] });
    }
  });
};

exports.updateChapter = (req, res, next) => {
  let formData = {
    course_id: req.body.course_id,
    company_id: req.body.company_id,
    chapter_number: req.body.chapter_number,
    chapter_title: req.body.chapter_title,
    waktu_mulai: req.body.waktu_mulai,
    waktu_selesai: req.body.waktu_selesai,
    chapter_body: req.body.chapter_body
  };

  db.query(`UPDATE course_chapter SET
    course_id = '${formData.course_id}',
    company_id = '${formData.company_id}',
    waktu_selesai = '${formData.waktu_selesai}',
    waktu_mulai = '${formData.waktu_mulai}',
    chapter_number = '${formData.chapter_number}',
    chapter_title = '${formData.chapter_title}',
    chapter_body = '${formData.chapter_body}'
    WHERE chapter_id = '${req.params.chapter_id}'`, (error, result, fields) => {
    if(error) {
      res.json({error: true, result: error});
    } else {
      db.query(`SELECT * FROM course_chapter WHERE chapter_id = '${req.params.chapter_id}'`, (error, result, fields) => {
        res.json({error: false, result: result[0]});
      })
    }
  })
};

exports.updateVideoChapter = (req, res, next) => {
  uploadLogo(req, res, (err) => {
    if(!req.file) {
      res.json({error: true, result: err});
    } else {

      let path = req.file.path;
      let newDate = new Date();
      let keyName = `${env.AWS_BUCKET_ENV}/course`;

      var params = {
        ACL: 'public-read',
        Bucket: env.AWS_BUCKET,
        Body: fs.createReadStream(path),
        Key: `${keyName}/${req.file.originalname}`
      };

      s3.upload(params, (err, data) => {
        if (err) {
          res.json({ error: true, msg: "Error Upload Image", result: err });
        }
        if (data) {
          fs.unlinkSync(path);

          let formData = {
            video: data.Location
          };

          db.query(`UPDATE course_chapter SET chapter_video = '${formData.video}' WHERE chapter_id = '${req.params.chapter_id}'`, (error, result, fields) => {
            if(error) {
              res.json({error: true, result: error});
            } else {
              res.json({error: false, result: formData.video});
            }
          })

        }
      });

    }
  })
};

exports.updateTumbnailChapter = (req, res, next) => {
  uploadThumbnail(req, res, (err) => {
    if(!req.file) {
      res.json({error: true, result: err});
    } else {

      let path = req.file.path;
      let newDate = new Date();
      let keyName = `${env.AWS_BUCKET_ENV}/course`;

      var params = {
        ACL: 'public-read',
        Bucket: env.AWS_BUCKET,
        Body: fs.createReadStream(path),
        Key: `${keyName}/${req.file.originalname}`
      };

      s3.upload(params, (err, data) => {
        if (err) {
          res.json({ error: true, msg: "Error Upload Image", result: err });
        }
        if (data) {
          fs.unlinkSync(path);

          let formData = {
            video: data.Location
          };

          db.query(`UPDATE course_chapter SET thumbnail = '${formData.video}' WHERE chapter_id = '${req.params.chapter_id}'`, (error, result, fields) => {
            if(error) {
              res.json({error: true, result: error});
            } else {
              res.json({error: false, result: formData.video});
            }
          })

        }
      })

    }
  })
};

exports.updateAttachmentChapter = (req, res, next) => {
  uploadAttachment(req, res, async (err) => {
    if(!req.files) {
      res.json({error: true, result: err});
    } else {
      let attachmentId = '';
      for(let i=0; i<req.files.length; i++) {
        let path = req.files[i].path;
        let newDate = new Date();
        let keyName = `${env.AWS_BUCKET_ENV}/course`;

        var params = {
          ACL: 'public-read',
          Bucket: env.AWS_BUCKET,
          Body: fs.createReadStream(path),
          Key: `${keyName}/${req.files[i].originalname}`
        };

        let dd = await s3.upload(params).promise();
        fs.unlinkSync(path);
        attachmentId += dd.Location + ',';
      }

      attachmentId = attachmentId.substring(0, attachmentId.length - 1);
      let formData = {
        video: attachmentId
      };

      db.query(`UPDATE course_chapter SET attachment_id = '${formData.video}' WHERE chapter_id = '${req.params.chapter_id}'`, (error, result, fields) => {
        if(error) {
          res.json({error: true, result: error});
        } else {
          res.json({error: false, result: formData.video});
        }
      })
    }
  })
};

exports.deleteChapter = (req, res, next) => {
  db.query(`DELETE FROM course_chapter WHERE chapter_id = '${req.params.chapter_id}'`, (error, result, fields) => {
    if(error) {
      res.json({error: true, result: error});
    } else {
      res.json({error: false, result: result});
    }
  })
};

exports.createChapterForum = (req, res, next) => {
  uploadLogo(req, res, async (err) => {
    if(!req.file) {
      res.json({ error: true, result: err });
    } else {
      let getChapterNumber = await db.query(`SELECT chapter_number FROM course_chapter WHERE course_id = '${req.body.course_id}' AND company_id = '${req.body.company_id}'`);
      let chapterNumber = getChapterNumber.length ? getChapterNumber[getChapterNumber.length-1].chapter_number : 0;

      let path = req.file.path;
      let newDate = new Date();
      let keyName = `${env.AWS_BUCKET_ENV}/course`;

      var params = {
        ACL: 'public-read',
        Bucket: env.AWS_BUCKET,
        Body: fs.createReadStream(path),
        Key: `${keyName}/${req.file.originalname}`
      };

      s3.upload(params, (err, data) => {
        if (err) {
          res.json({ error: true, msg: "Error Upload Image", result: err });
        }
        if (data) {
          fs.unlinkSync(path);

          let formData = {
            course_id: req.body.course_id,
            company_id: req.body.company_id,
            chapter_number: chapterNumber + 1,
            waktu_mulai: req.body.waktu_mulai,
            waktu_selesai: req.body.waktu_selesai,
            title: req.body.chapter_title,
            body: req.body.chapter_body,
            video: data.Location,
            tags: req.body.tags
          };

          let sql = `INSERT INTO course_chapter
          (chapter_id, waktu_selesai, waktu_mulai, jenis_pembelajaran, tags_forum, close_forum, course_id, company_id, chapter_number, chapter_title, chapter_body, chapter_video, attachment_id) VALUES
          (null, '${formData.waktu_selesai}', '${formData.waktu_mulai}', 'forum', '${formData.tags}', '0', '${formData.course_id}', '${formData.company_id}', '${formData.chapter_number}', '${formData.title}', '${formData.body}', '${formData.video}', null)`;

          db.query(sql, (error, result, fields) => {
            if(error) {
              res.json({error: true, result: error});
            } else {
              db.query(`SELECT * FROM course_chapter WHERE chapter_id = '${result.insertId}'`, (error, result, fields) => {
                res.json({error: false, result: result[0]});
              })
            }
          });

        }
      });

    }
  });
};

exports.updateChapterForum = (req, res, next) => {
  let formData = {
    course_id: req.body.course_id,
    company_id: req.body.company_id,
    chapter_title: req.body.chapter_title,
    chapter_body: req.body.chapter_body,
    waktu_mulai: req.body.waktu_mulai,
    waktu_selesai: req.body.waktu_selesai,
    tags: req.body.tags
  };

  db.query(`UPDATE course_chapter SET
    course_id = '${formData.course_id}',
    company_id = '${formData.company_id}',
    waktu_selesai = '${formData.waktu_selesai}',
    waktu_mulai = '${formData.waktu_mulai}',
    chapter_title = '${formData.chapter_title}',
    chapter_body = '${formData.chapter_body}',
    tags_forum = '${formData.tags}'
    WHERE chapter_id = '${req.params.chapter_id}'`, (error, result, fields) => {
    if(error) {
      res.json({error: true, result: error});
    } else {
      db.query(`SELECT * FROM course_chapter WHERE chapter_id = '${req.params.chapter_id}'`, (error, result, fields) => {
        res.json({error: false, result: result[0]});
      })
    }
  })
};

exports.createChapterMeeting = (req, res, next) => {
  uploadLogo(req, res, async (err) => {
    if(!req.file) {
      res.json({ error: true, result: err });
    } else {
      let getChapterNumber = await db.query(`SELECT chapter_number FROM course_chapter WHERE course_id = '${req.body.course_id}' AND company_id = '${req.body.company_id}'`);
      let chapterNumber = getChapterNumber.length ? getChapterNumber[getChapterNumber.length-1].chapter_number : 0;

      let path = req.file.path;
      let newDate = new Date();
      let keyName = `${env.AWS_BUCKET_ENV}/course`;

      var params = {
        ACL: 'public-read',
        Bucket: env.AWS_BUCKET,
        Body: fs.createReadStream(path),
        Key: `${keyName}/${req.file.originalname}`
      };

      s3.upload(params, (err, data) => {
        if (err) {
          res.json({ error: true, msg: "Error Upload Image", result: err });
        }
        if (data) {
          fs.unlinkSync(path);

          let formData = {
            course_id: req.body.course_id,
            company_id: req.body.company_id,
            chapter_number: chapterNumber + 1,
            title: req.body.chapter_title,
            waktu_mulai: req.body.waktu_mulai,
            waktu_selesai: req.body.waktu_selesai,
            body: req.body.chapter_body,
            video: data.Location,
            moderator: req.body.moderator,
            start_date: req.body.start_date,
            end_date: req.body.end_date,
          };

          console.log(formData);

          let sql = `INSERT INTO course_chapter
          (chapter_id, waktu_selesai, waktu_mulai, jenis_pembelajaran, moderator, start_date, end_date, course_id, company_id, chapter_number, chapter_title, chapter_body, chapter_video, attachment_id) VALUES
          (null, '${formData.waktu_selesai}', '${formData.waktu_mulai}', 'group meeting', '${formData.moderator}', '${formData.start_date}', '${formData.end_date}', '${formData.course_id}', '${formData.company_id}', '${formData.chapter_number}', '${formData.title}', '${formData.body}', '${formData.video}', null)`;

          db.query(sql, (error, result, fields) => {
            if(error) {
              res.json({error: true, result: error});
            } else {
              db.query(`SELECT * FROM course_chapter WHERE chapter_id = '${result.insertId}'`, (error, result, fields) => {
                res.json({error: false, result: result[0]});
              })
            }
          });

        }
      });

    }
  });
}

exports.updateChapterMeeting = (req, res, next) => {
  let formData = {
    course_id: req.body.course_id,
    company_id: req.body.company_id,
    chapter_title: req.body.chapter_title,
    chapter_body: req.body.chapter_body,
    moderator: req.body.moderator,
    waktu_mulai: req.body.waktu_mulai,
    waktu_selesai: req.body.waktu_selesai,
    start_date: req.body.start_date,
    end_date: req.body.end_date,
  };

  console.log(formData);

  db.query(`UPDATE course_chapter SET
    course_id = '${formData.course_id}',
    company_id = '${formData.company_id}',
    waktu_selesai = '${formData.waktu_selesai}',
    waktu_mulai = '${formData.waktu_mulai}',
    chapter_title = '${formData.chapter_title}',
    chapter_body = '${formData.chapter_body}',
    moderator = '${formData.moderator}',
    start_date = '${formData.start_date}',
    end_date = '${formData.end_date}'
    WHERE chapter_id = '${req.params.chapter_id}'`, (error, result, fields) => {
    if(error) {
      res.json({error: true, result: error});
    } else {
      db.query(`SELECT * FROM course_chapter WHERE chapter_id = '${req.params.chapter_id}'`, (error, result, fields) => {
        res.json({error: false, result: result[0]});
      })
    }
  })
}
