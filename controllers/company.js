var env = require('../env.json');
var conf = require('../configs/config');
var db = require('../configs/database');
var multer = require('multer');
const { checkAccess } = require('../middleware');
const storage = multer.diskStorage({
	filename: function (req, file, cb) {
		cb(null, file.originalname);
	}
});
const AWS = require('aws-sdk');
const fs = require('fs');
const s3 = new AWS.S3({
	accessKeyId: env.AWS_ACCESS,
	secretAccessKey: env.AWS_SECRET
});

const bbb = require('bigbluebutton-js')
const BBB_URL = env.BBB_URL;
const BBB_KEY = env.BBB_KEY;
let uploadLogo = multer({ storage: storage }).single('logo');
const moment = require('moment-timezone');
const asyncs = require('async');

exports.getListActivity = (req, res) => {

	const user_id = req.app.token.user_id;
	let now = new Date();
	let range = new Date();

	range.setDate(range.getDate() - 3);
	now.setDate(now.getDate() + 1)
	range = range.toJSON().split('T')[0];
	now = now.toJSON().split('T')[0];

	let start_date = range;
	let end_date = now;
	let limit = '';
	let stateWebinar = ['Empty', 'Upcoming', 'On Going', 'Done'];

	if (req.query.limit && typeof parseInt(req.query.limit) === 'number' && req.query.limit !== 'max') {
		limit = `LIMIT ${req.query.limit}`;
	}

	if (!start_date || !end_date) {
		return res.json({ error: true, result: 'Date is required' });
	}

	// let uri = `http://localhost:3200/v1/sidebar/agenda/${req.params.company_id}/${req.params.user_id}?start_date=${start_date}&end_date=${end_date}`;
	// axios.get(uri, { headers: req.headers }).then(res => {
	// 	if (res.status == 200) {
	// 		console.log(res.data, "TEST");
	// 	}
	// });
	// return;
	let append_q = ` between '${start_date}' and '${end_date}' `
	// console.log(append_q, '9090')

	let queryMeeting = `
	SELECT
	  distinct
	  lb.id AS id,
	  l.class_id AS meeting_id,
	  l.user_id as created_by,
	  lb.id AS booking_id,
	  lb.moderator,
	  l.room_name AS title, 
	  IF(lb.is_akses=1, u.name, null) AS moderator,
	  lb.keterangan as 'description',
	  lb.tanggal AS date, 
	  lb.timezone, lb.offset, 
	  DATE_FORMAT(lb.date_start,'%Y-%m-%d %H:%i:%s') AS startTime,
      DATE_FORMAT(lb.date_end ,'%Y-%m-%d %H:%i:%s') AS endTime,
	  -- CAST(CONCAT(lb.tanggal, ' ', lb.jam_mulai) AS DATETIME) AS startTime, 
	  -- CAST(CONCAT(lb.tanggal, ' ', lb.jam_selesai) AS DATETIME) AS endTime, 
	  IF(
		TIMESTAMPDIFF(MINUTE, NOW(), CAST(CONCAT(lb.tanggal, ' ', lb.jam_selesai) AS DATETIME)) < 0,
		'Done',
		'Upcoming'
	  ) as status,
	  
	  'meeting' AS type,
	  lm.id as file_mom_id,
	  lm.title as file_mom_title,
	  l.folder_id
	FROM 
	liveclass_booking lb
	LEFT JOIN  liveclass l ON l.class_id = lb.meeting_id
	  LEFT JOIN liveclass_booking_participant lbp ON lbp.booking_id = lb.id
	  left join liveclass_mom lm ON (lm.liveclass_id = l.class_id or lm.liveclass_id = lb.id) AND TIMESTAMPDIFF(HOUR,lb.date_end, lm.time) BETWEEN 0 AND 1
	  LEFT JOIN user u ON lb.moderator = u.user_id
	WHERE
		lb.tanggal ${append_q} 
		AND (lbp.user_id = '${user_id}' OR l.user_id = '${user_id}' OR l.speaker = '${user_id}' OR l.moderator = '${user_id}')
	  -- AND lbp.user_id = '${user_id}' 
	  AND l.company_id = '${req.params.companyId}' ${limit}`;

	// return console.log(queryMeeting)

	let queryWebinar = `SELECT distinct w.id AS id, w.judul AS title, w.isi as keterangan, w.company_id, w.project_id as folder_id, w.pembicara, w.sekretaris,w.start_time AS date, w.start_time AS startTime, w.end_time AS endTime, 'webinar' AS type , w.status ,
	IF(
		TIMESTAMPDIFF(MINUTE, NOW(), w.end_time ) < 0,
		'Done',
		'Upcoming'
	  ) as expired
	FROM webinars w
	left JOIN webinar_peserta p ON w.id=p.webinar_id 
	WHERE
	( 
		DATE(w.start_time) ${append_q} 
		OR DATE(w.created_at) ${append_q} 
	)
	AND w.company_id='${req.params.companyId}' AND w.publish='1' 
	AND (p.user_id='${user_id}' OR (w.sekretaris IN (${user_id}) OR w.moderator IN (${user_id}) OR w.pembicara IN (${user_id}) OR w.owner IN (${user_id})))
	GROUP BY w.id ${limit};`;

	let queryTrainingExam = `SELECT a.id AS id, e.title AS title, c.title AS course, e.start_time AS startTime, e.end_time AS endTime, IF(e.exam=1, 'training exam', 'training quiz') AS type
	FROM training_exam e
	JOIN training_exam_assignee a ON a.exam_id=e.id
	LEFT JOIN training_user tu ON tu.id = a.training_user_id
	LEFT JOIN user u ON u.email = tu.email
	LEFT JOIN training_course c ON c.id = e.course_id
	WHERE e.scheduled = 1 AND  
	DATE(e.start_time) ${append_q} 
	AND u.user_id='${user_id}' 
	AND u.status='active' AND e.status='Active' AND a.status IN ('Open','Start')`;

	let queryTrainingCourse = `SELECT c.id AS id, c.title AS title, c.start_time AS startTime, c.end_time AS endTime, 'training course' AS type, (IF(NOW() BETWEEN c.start_time AND c.end_time != '1' && c.scheduled = 1, '0', '1')) AS on_schedule 
	FROM training_course c
	WHERE c.scheduled = 1 AND 
	DATE(c.start_time) ${append_q} 
	AND c.company_id='${req.params.companyId}' 
	AND c.status='Active'`;

	let data = [];
	db.query(queryMeeting, async (error, result, fields) => {
		if (error) {
			res.json({ error: true, result: error })
		}
		else {
			//return console.log(result, 9090)
			let tmp = [];
			for (let i = 0; i < result.length; i++) {

				result[i].labels = [];
				let tz = result[i].timezone || 'Asia/Jakarta';
				result[i].startTime = moment.tz(result[i].startTime, tz)
				result[i].endTime = moment.tz(result[i].endTime, tz)
				if (result[i].status === 'Done') {
					if (result[i].created_by == user_id) {
						if (!result[i].folder_id) {
							result[i].labels.push('Select Folder');
						}
						if (!result[i].file_mom_id) {
							result[i].labels.push('Create File MoM');
						}
					}
					if (result[i].labels.length > 0) {
						// result[i].labels = result[i].labels.toString();
					} else {
						result[i].labels = [result[i].status];
					}

					//result[i].labels = result[i].labels.replace(new RegExp(',', 'g'), ', ');

					let idx = tmp.findIndex((str) => {
						return (str.id == result[i].id && str.meeting_id == result[i].meeting_id);
					})
					if (idx < 0 && result[i].labels !== 'Upcoming') {
						tmp.push({
							id: result[i].id,
							meeting_id: result[i].meeting_id,
							folder_id: result[i].folder_id,
							title: result[i].title,
							description: result[i].description,
							startTime: result[i].startTime,
							endTime: result[i].endTime,
							status: result[i].status,
							type: result[i].type,
							label: result[i].labels,
							created_by: result[i].created_by
						});
					}
				}


			}
			data = tmp;
			db.query(queryWebinar, async (error, result, fields) => {
				if (error) {
					res.json({ error: true, result: error })
				}
				else {
					let tmp = [];
					for (let i = 0; i < result.length; i++) {

						result[i].labels = [];
						if (result[i].sekretaris) {

							let split = result[i].sekretaris.split(',');
							split.forEach((str) => {

								if (user_id === str) {
									if (result[i].status <= 2) {
										result[i].labels.push('Go to Details')
										result[i].status = 'Upcoming'
									}
									if (result[i].status == 3) {
										result[i].labels.push('Send Certificate')
										result[i].labels.push('View Report')
										result[i].status = 'Done'
									}
								}
							})
						}

						if (result[i].labels.length > 0) {
							//result[i].labels = result[i].labels.toString();
						} else {
							result[i].status = stateWebinar[result[i].status];
							result[i].labels = [result[i].status];
						}

						// result[i].labels = result[i].labels.replace(new RegExp(',', 'g'), ', ');

						let idx = tmp.findIndex((str) => {
							return str.id == result[i].id;
						})
						if (idx < 0 && result[i].labels !== 'Upcoming') {
							tmp.push({
								id: result[i].id,
								folder_id: result[i].folder_id,
								title: result[i].title,
								description: result[i].description,
								startTime: result[i].startTime,
								endTime: result[i].endTime,
								status: result[i].status,
								type: 'webinar',
								label: result[i].labels,
							})
						}

					}

					data = data.concat(tmp);
					if (data.length > 0) {
						data = data.sort((a, b) => { b.startTime - a.startTime });
					}
					data.sort(function (a, b) {
						return new Date(b.startTime) - new Date(a.startTime);
					});
					res.json({ error: false, result: data, request: { start_date: start_date, end_date: end_date } })
				}
			})
		}
	})
};

exports.createCompany = (req, res, next) => {
	uploadLogo(req, res, (err) => {
		if (!req.file) {
			res.json({ error: true, result: err });
		} else {

			if (env.AWS_BUCKET) {
				let path = req.file.path;
				let newDate = new Date();
				let keyName = `${env.AWS_BUCKET_ENV}/company`;

				var params = {
					ACL: 'public-read',
					Bucket: env.AWS_BUCKET,
					Body: fs.createReadStream(path),
					Key: `${keyName}/${req.file.originalname}`,
					Metadata: {
						'Content-Type': req.file.mimetype,
						'Content-Disposition': 'inline'
					}
				};

				s3.upload(params, (err, data) => {
					if (err) {
						res.json({ error: true, msg: "Error Upload Image", result: err });
					}
					if (data) {
						fs.unlinkSync(path);

						var formData = {
							name: conf.sanitize(req.body.company_name),
							type: req.body.company_type,
							logo: data.Location,
							validity: req.body.validity,
							status: conf.sanitize(req.body.status),
							unlimited: req.body.unlimited,
							limituser: req.body.limituser ? req.body.limituser : 'NULL',
							limitmeeting: req.body.limitmeeting ? req.body.limitmeeting : 'NULL',
							limitwebinar: req.body.limitwebinar ? req.body.limitwebinar : 'NULL',
							access_training: req.body.access_training.toString() === "true" ? 1 : 0,
							access_meeting: req.body.access_meeting.toString() === "true" ? 1 : 0,
							access_webinar: req.body.access_webinar.toString() === "true" ? 1 : 0,
							access_calendar: req.body.access_calendar.toString() === "true" ? 1 : 0,
							access_filemanager: req.body.access_filemanager.toString() === "true" ? 1 : 0,
							access_report: req.body.access_report.toString() === "true" ? 1 : 0,
							access_news: req.body.access_news.toString() === "true" ? 1 : 0,
							access_project: req.body.access_project.toString() === "true" ? 1 : 0

						};

						db.query(`INSERT INTO company (company_id, company_name, company_type, logo, validity, status, unlimited, limit_user, limit_meeting, limit_webinar,
							 access_training,access_meeting,access_webinar,access_calendar,access_filemanager,access_report,access_news,access_project)
							  VALUES (null,'${formData.name}','${formData.type}','${formData.logo}','${formData.validity}',
								  '${formData.status}','${formData.unlimited}',${formData.limituser},${formData.limitmeeting},${formData.limitwebinar},
								  ${formData.access_training},${formData.access_meeting},${formData.access_webinar},${formData.access_calendar},
								  ${formData.access_filemanager},${formData.access_report},${formData.access_news},${formData.access_project})`,
							async (error, result, fields) => {
								if (error) {
									res.json({ error: true, result: error });
								} else {
									let defaultGS = await db.query(`SELECT * FROM global_settings_template`);
									defaultGS.map((item) => {
										db.query(`INSERT INTO global_setting (id_global_settings_template,company_id,status) VALUES (?)`, [[item.id_access, result.insertId, item.status]]);
									})
									db.query(`SELECT * FROM company WHERE company_id = '${result.insertId}'`, (error, result, fields) => {
										res.json({ error: false, result: result[0] });
									})
								}
							});

					}
				});
			}
			else {
				var formData = {
					name: conf.sanitize(req.body.company_name),
					type: req.body.company_type,
					logo: `${env.APP_URL}/company/${req.file.originalname}`,
					validity: req.body.validity,
					status: conf.sanitize(req.body.status),
					unlimited: req.body.unlimited,
					limituser: req.body.limituser ? req.body.limituser : 'NULL',
					limitmeeting: req.body.limitmeeting ? req.body.limitmeeting : 'NULL',
					limitwebinar: req.body.limitwebinar ? req.body.limitwebinar : 'NULL',
					access_training: req.body.access_training.toString() === "true" ? 1 : 0,
					access_meeting: req.body.access_meeting.toString() === "true" ? 1 : 0,
					access_webinar: req.body.access_webinar.toString() === "true" ? 1 : 0,
					access_calendar: req.body.access_calendar.toString() === "true" ? 1 : 0,
					access_filemanager: req.body.access_filemanager.toString() === "true" ? 1 : 0,
					access_report: req.body.access_report.toString() === "true" ? 1 : 0,
					access_news: req.body.access_news.toString() === "true" ? 1 : 0,
					access_project: req.body.access_project.toString() === "true" ? 1 : 0,

				};

				db.query(`INSERT INTO company (company_id, company_name, company_type, logo, validity, status, unlimited, limit_user, limit_meeting, limit_webinar,
					access_training,access_meeting,access_webinar,access_calendar,access_filemanager,access_report,access_news,access_project)
					VALUES (null,'${formData.name}','${formData.type}','${formData.logo}','${formData.validity}',
					'${formData.status}','${formData.unlimited}',${formData.limituser},${formData.limitmeeting},${formData.limitwebinar},
					${formData.access_training},${formData.access_meeting},${formData.access_webinar},${formData.access_calendar},
					${formData.access_filemanager},${formData.access_report},${formData.access_news},${formData.access_project})`,
					async (error, result, fields) => {
						if (error) {
							res.json({ error: true, result: error });
						} else {
							let defaultGS = await db.query(`SELECT * FROM global_settings_template`);
							defaultGS.map((item) => {
								db.query(`INSERT INTO global_setting (id_global_settings_template,company_id,status) VALUES (?)`, [[item.id_access, result.insertId, item.status]]);
							})
							db.query(`SELECT * FROM company WHERE company_id = '${result.insertId}'`, (error, result, fields) => {
								res.json({ error: false, result: result[0] });
							})
						}
					});
			}

		}
	});
};

exports.getCompanyList = (req, res, next) => {
	db.query(`SELECT company.*, count(u.user_id) AS user
	FROM user u
	RIGHT JOIN
	(SELECT c.*, count(l.class_id) AS meeting
	FROM company c
	LEFT JOIN liveclass l ON c.company_id=l.company_id
	GROUP BY c.company_id) company
	ON u.company_id=company.company_id
	GROUP BY company.company_id`, (error, result, fields) => {
		if (error) {
			res.json({ error: true, result: error });
		} else {
			res.json({ error: false, result: result });
		}
	});
};

exports.getCompanyOne = (req, res, next) => {
	db.query(`SELECT * FROM company WHERE company_id = '${req.params.company_id}'`, (error, result, fields) => {
		if (error) {
			res.json({ error: true, result: error });
		} else {
			res.json({ error: false, result: result.length == 0 ? result : result[0] });
		}
	});
};

exports.updateCompany = (req, res, next) => {
	let accessFrom = 'mycompany';
	let check = Object.keys(req.body);
	if (req.app.token.level === 'superadmin' && check.indexOf("access_training") > -1){
		accessFrom = 'company'
	}
	var formData = {};
	let sql = '';
	console.log('ALVIN', accessFrom)
	if (accessFrom === 'company'){
		formData = {
			name: conf.sanitize(req.body.name),
			type: req.body.type,
			grade: req.body.grade,
			validity: req.body.validity,
			status: conf.sanitize(req.body.status),
			unlimited: req.body.unlimited,
			limituser: req.body.limituser ? req.body.limituser : 'NULL',
			limitmeeting: req.body.limitmeeting ? req.body.limitmeeting : 'NULL',
			limitwebinar: req.body.limitwebinar ? req.body.limitwebinar : 'NULL',
			access_training: req.body.access_training.toString() === "true" ? 1 : 0,
			access_meeting: req.body.access_meeting.toString() === "true" ? 1 : 0,
			access_webinar: req.body.access_webinar.toString() === "true" ? 1 : 0,
			access_calendar: req.body.access_calendar.toString() === "true" ? 1 : 0,
			access_filemanager: req.body.access_filemanager.toString() === "true" ? 1 : 0,
			access_report: req.body.access_report.toString() === "true" ? 1 : 0,
			access_news: req.body.access_news.toString() === "true" ? 1 : 0,
			access_project: req.body.access_project.toString() === "true" ? 1 : 0,
		};
		sql = `UPDATE company SET
		company_name = '${formData.name}',
		company_type = '${formData.type}',
		company_grade = '${formData.grade}',
		validity = '${formData.validity}',
		status = '${formData.status}',
		unlimited = '${formData.unlimited}',
		limit_user = ${formData.limituser},
		limit_meeting = ${formData.limitmeeting},
		limit_webinar = ${formData.limitwebinar},
		access_training = ${formData.access_training},
		access_meeting = ${formData.access_meeting},
		access_webinar = ${formData.access_webinar},
		access_calendar = ${formData.access_calendar},
		access_filemanager = ${formData.access_filemanager},
		access_report = ${formData.access_report},
		access_news = ${formData.access_news},
		access_project = ${formData.access_project} 

		WHERE company_id = '${req.params.company_id}'`
	}
	else{
		formData = {
			name: conf.sanitize(req.body.name),
			type: req.body.type,
			grade: req.body.grade,
		};
		sql = `UPDATE company SET
		company_name = '${formData.name}',
		company_type = '${formData.type}',
		company_grade = '${formData.grade}'

		WHERE company_id = '${req.params.company_id}'`
	}

	db.query(sql, (error, result, fields) => {
		if (error) {
			res.json({ error: true, result: error });
		} else {
			db.query(`SELECT * FROM company WHERE company_id = '${req.params.company_id}'`, (error, result, fields) => {
				res.json({ error: false, result: result[0] });
			})
		}
	});
};

exports.updateCompanyLogo = (req, res, next) => {
	uploadLogo(req, res, (err) => {
		if (!req.file) {
			res.json({ error: true, result: err });
		} else {

			let path = req.file.path;
			let newDate = new Date();
			let keyName = `${env.AWS_BUCKET_ENV}/company`;

			var params = {
				ACL: 'public-read',
				Bucket: env.AWS_BUCKET,
				Body: fs.createReadStream(path),
				Key: `${keyName}/${req.file.originalname}`
			};

			s3.upload(params, (err, data) => {
				if (err) {
					res.json({ error: true, msg: "Error Upload Image", result: err });
				}
				if (data) {
					fs.unlinkSync(path);

					db.query(`UPDATE company SET logo = '${data.Location}' WHERE company_id = '${req.params.company_id}'`, (error, result, fields) => {
						if (error) {
							res.json({ error: true, result: error });
						} else {
							res.json({ error: false, result: data.Location });
						}
					});

				}
			})

		}
	});
};

exports.deleteCompanyLogo = (req, res, next) => {
	if (req.params.company_id) {
		db.query(`UPDATE company SET logo = NULL WHERE company_id = '${req.params.company_id}'`, (error, result, fields) => {
			if (error) {
				res.json({ error: true, result: error });
			} else {
				res.json({ error: false, result: 'success' });
			}
		});
	} else {
		res.json({ error: true, result: 'Delete logo is failed' });
	}
};

exports.deleteCompany = (req, res, next) => {
	db.query(`DELETE FROM company WHERE company_id = '${req.params.company_id}'`, (error, result, fields) => {
		if (error) {
			res.json({ error: true, result: error });
		} else {
			res.json({ error: false, result: result });
		}
	});
};

exports.getEventList = (req, res, next) => {
	let queryUser = `SELECT
	count(l.class_id) AS meeting,
	(SELECT count(c.course_id) AS learning
		FROM course c
		INNER JOIN learning_category cat
		ON c.category_id=cat.category_id
		WHERE cat.company_id='${req.params.company_id}' AND c.publish='1') AS learning,
	(SELECT count(w.id) AS webinar
		FROM webinars w
		WHERE w.company_id='${req.params.company_id}' AND w.publish='1') AS webinar,
	(SELECT count(c.id) AS training
		FROM training_course c
		WHERE c.company_id='${req.params.company_id}' AND c.status='Active') AS training,
	(SELECT access_training
		FROM company
		WHERE company_id='${req.params.company_id}') AS access_training
	FROM liveclass l
    LEFT JOIN liveclass_participant p
    ON l.class_id=p.class_id
	WHERE company_id='${req.params.company_id}' AND ((l.is_private='1' AND p.user_id='${req.params.user_id}') OR l.is_private='0')
	GROUP BY company_id`;
	let queryAdmin = `SELECT
	count(liveclass.class_id) AS meeting,
	(SELECT count(c.course_id) AS learning
		FROM course c
		INNER JOIN learning_category cat
		ON c.category_id=cat.category_id
		WHERE cat.company_id='${req.params.company_id}' AND c.publish='1') AS learning,
	(SELECT count(w.id) AS webinar
		FROM webinars w
		WHERE w.company_id='${req.params.company_id}' AND w.publish='1') AS webinar,
	(SELECT count(c.id) AS training
		FROM training_course c
		WHERE c.company_id='${req.params.company_id}' AND c.status='Active') AS training,
	(SELECT access_training
		FROM company
		WHERE company_id='${req.params.company_id}') AS access_training
	FROM company
    LEFT JOIn liveclass ON liveclass.company_id = company.company_id
	WHERE company.company_id='${req.params.company_id}'
	GROUP BY company.company_id`;
	db.query(req.params.level == 'client' ? queryUser : queryAdmin, async (error, result, fields) => {
		if (error) {
			res.json({ error: true, result: error });
		} else {
			let dataEvent = [];
			if (result.length == 0) {
				dataEvent = [
					{ title: 'Meeting', total: 0, status: true }, { title: 'Webinar', total: 0, status: true }, { title: 'Learning', total: 0, status: true }, { title: 'Training', total: 0, status: false }
				];
			}
			else {
				dataEvent = [
					{ title: 'Meeting', total: result[0].meeting, status: true }, { title: 'Webinar', total: result[0].webinar, status: true }, { title: 'Learning', total: result[0].learning, status: true }, { title: 'Training', total: result[0].training, status: result[0].access_training === 1 ? true : false }
				];
			}
			let companyType = await db.query(`SELECT company_type FROM company WHERE company_id='${req.params.company_id}'`);
			if (companyType[0].company_type === 'pendidikan') {
				dataEvent.map(item => {
					if (["Learning", "Meeting"].includes(item.title)) {
						item.status = true;
					}
					else if (item.title === 'Training') {
						item.status = result[0].access_training === 1 ? true : false;
					}
					else {
						item.status = false;
					}
				})
			}
			else {
				dataEvent.map(item => {
					if (item.title === 'Learning') {
						item.status = false;
					}
					else if (item.title === 'Training') {
						item.status = result[0].access_training === 1 ? true : false;
					}
					else {
						item.status = true;
					}
				})
			}
			res.json({ error: false, result: dataEvent });
		}
	});
};

// one hit
exports.Sidebar_Project = (req, res, next) => {

	asyncs.waterfall([
		(cb) => {
			let queryUser = `SELECT p.id, p.name AS title,
			(SELECT COUNT(l.class_id) FROM liveclass l LEFT JOIN (SELECT * FROM liveclass_participant lp WHERE lp.user_id='${req.params.user_id}') AS lp ON lp.class_id=l.class_id WHERE l.folder_id=p.id AND ((l.is_private='1' AND lp.user_id='53') OR l.is_private='0')) AS meeting,
			(SELECT COUNT(w.id) FROM webinars w WHERE w.project_id=p.id AND w.publish=1) AS webinar,
			IF(p.company_id!='${req.params.company_id}', c.company_name, null) AS share_from, p.update_at as recent_project
			FROM files_folder p
			LEFT JOIN files_folder_user pu ON pu.folder_id=p.id
			LEFT JOIN company c ON c.company_id=p.company_id
			LEFT JOIN project_share s ON s.project_id=p.id
			WHERE (p.company_id='${req.params.company_id}' OR s.user_id='${req.params.user_id}') AND p.publish='1' AND p.mother='0' AND ((p.is_limit=1 AND (pu.user_id='${req.params.user_id}' AND pu.role='Project Admin') OR (pu.user_id='${req.params.user_id}' AND pu.role='User')) OR p.is_limit=0 OR s.user_id='${req.params.user_id}')
			GROUP BY p.id
			ORDER BY p.update_at DESC`;

			let queryAdmin = `SELECT p.id, p.name AS title, count(DISTINCT l.class_id) AS meeting, count(DISTINCT w.id) AS webinar, IF(p.company_id!='${req.params.company_id}', c.company_name, null) AS share_from, p.update_at as recent_project
			FROM files_folder p
			LEFT JOIN liveclass l ON p.id=l.folder_id
			LEFT JOIN (SELECT * FROM webinars w WHERE w.publish=1) AS w ON w.project_id=p.id
			LEFT JOIN project_share s ON s.project_id=p.id
			LEFT JOIN company c ON c.company_id=p.company_id
			WHERE (p.company_id='${req.params.company_id}' OR s.user_id='${req.params.user_id}') AND p.mother='0' AND p.publish='1'
			GROUP BY p.id
			ORDER BY p.update_at DESC`;

			db.query(req.params.level == 'client' ? queryUser : queryAdmin, (error, result, fields) => {
				if (error) {
					cb({ error: true, result: error });
				} else {
					let tmp = [];
					result.forEach((str) => {
						tmp.push({
							project_id: str.id,
							title: str.title,
							event: [],
							count_meet: str.meeting,
							count_webinar: str.webinar
						});

					});
					cb(false, tmp);
				}
			});

		}, (val, cb) => {
			// MEET
			let tmp = [];
			asyncs.eachSeries(val, (str, callback) => {

				let obj = str;
				let meet = [];
				let webi = [];
				asyncs.parallel([

					(cb_parallel) => {
						if (str.count_webinar > 0) {

							db.query(`SELECT *, 
									IF(tanggal IS NULL,
										'No Schedule', 
										CONCAT( 
											DATE_FORMAT(tanggal , '%Y/%m/%d'), 
											' | ', 
											DATE_FORMAT(jam_mulai , '%h:%m %p'), 
											' - ', 
											DATE_FORMAT(jam_selesai , '%h:%m %p') 
										) 
									) as datetime ,
									DATE_FORMAT(start_time , '%Y/%m/%d %h:%i:%s') as 'schedule_start',
									DATE_FORMAT(end_time , '%Y/%m/%d %h:%i:%s') as 'schedule_end'
									
									FROM webinars
									WHERE project_id = '${str.project_id}' AND publish='1'
									ORDER BY id DESC`, async (error, result, fields) => {

								try {
									for (var i = 0; i < result.length; i++) {
										let participant = [];

										let moderator = await db.query(`SELECT user_id, name FROM user WHERE user_id IN (${result[i].moderator})`);
										result[i].moderator = moderator.length != 0 ? moderator : { user_id: result[i].moderator, name: "Anonymous" };

										let sekretaris = await db.query(`SELECT user_id, name FROM user WHERE user_id IN (${result[i].sekretaris})`);
										result[i].sekretaris = sekretaris.length != 0 ? sekretaris : { user_id: result[i].sekretaris, name: "Anonymous" };

										let pembicara = await db.query(`SELECT user_id, name FROM user WHERE user_id IN (${result[i].pembicara})`);
										result[i].pembicara = pembicara.length != 0 ? pembicara : { user_id: result[i].pembicara, name: "Anonymous" };

										let owner = await db.query(`SELECT user_id, name FROM user WHERE user_id IN (${result[i].owner})`);
										result[i].owner = owner.length != 0 ? owner : { user_id: result[0].owner, name: "Anonymous" };

										let peserta = await db.query(`SELECT w.*, u.name FROM webinar_peserta w JOIN user u ON u.user_id = w.user_id WHERE webinar_id = '${result[i].id}'`)
										result[i].peserta = peserta;

										let tamu = await db.query(`SELECT * FROM webinar_tamu WHERE webinar_id='${result[i].id}'`);
										result[i].tamu = tamu;

										if (tamu.length > 0) {
											tamu.forEach(str_user => participant.push(str_user));
										}
										if (peserta.length > 0) {
											peserta.forEach(str_user => participant.push(str_user));
										}
										result[i].participant = participant;
									}

									result.forEach((str2) => {

										let access = false;
										if (str2.moderator.user_id == req.params.user_id || req.params.level === 'superadmin' || req.params.level === 'admin') {
											access = true;
										}
										var status = ['Incomplete', 'Not Started', 'On Going', 'Done']
										webi.push({
											id: str2.id,
											title: str2.judul,
											schedule_start: str2.schedule_start,
											schedule_end: str2.schedule_end,
											datetime: str2.datetime,
											participants: str2.participant,
											status: status[str2.status--],
											is_delete: access,
											is_edit: access,
											type: 'Webinar'
										});
									});
									// tmp.push(obj);
									// callback(null);
									cb_parallel(false, webi)
								} catch (e) {
									console.log(e, "TEST WEBINAR");
									cb_parallel(false, webi)
									// tmp.push(obj);
									// callback(null);
								}
							})
						} else {
							cb_parallel(false, webi)
						}
					},
					(cb_parallel) => {

						if (str.count_meet > 0) {


							let queryUser = `
							SELECT 
								-- distinct 
								l.class_id, 
								l.user_id, 
								l.room_name,  
								l.is_scheduled, 
								IF(l.schedule_start IS NULL,
									'No Schedule',
									CONCAT(
										DATE_FORMAT(l.schedule_start , '%m/%d/%Y'),
										' | ',
										DATE_FORMAT(l.schedule_start , '%h:%m %p'),
										' - ',
										DATE_FORMAT(l.schedule_end , '%h:%m %p')
									)
								) as datetime,
								DATE_FORMAT(l.schedule_start , '%Y/%m/%d %h:%i:%s') as 'schedule_start',
								DATE_FORMAT(l.schedule_end , '%Y/%m/%d %h:%i:%s') as 'schedule_end',
								u.name, 
								GROUP_CONCAT(p.user_id) AS participant, 
								COUNT(p.user_id) AS total_participant, 
								LEFT(l.schedule_start, 10) AS tanggal, 
								MID(l.schedule_start, 12, 8) AS waktu_start, 
								MID(l.schedule_end,12, 8) AS waktu_end 
							FROM 
								liveclass l 
								LEFT JOIN user u ON  (l.moderator IS NOT NULL AND l.moderator = u.user_id) OR u.user_id = l.user_id 
								LEFT JOIN liveclass_participant p ON l.class_id = p.class_id 
							WHERE l.folder_id='${str.project_id}' AND ((l.is_private='1' AND p.user_id='${req.params.user_id}') OR l.is_private='0') 
							GROUP BY l.class_id
							ORDER BY l.class_id DESC`;

							let queryAdmin = `
							SELECT 
							-- distinct 
							l.class_id, 
							l.user_id, 
							l.room_name, 
							l.is_scheduled, 
							IF(l.schedule_start IS NULL,
								'No Schedule',
								CONCAT(
									DATE_FORMAT(l.schedule_start , '%m/%d/%Y'),
									' | ',
									DATE_FORMAT(l.schedule_start , '%h:%m %p'),
									' - ',
									DATE_FORMAT(l.schedule_end , '%h:%m %p')
								)
							) as datetime, 
							DATE_FORMAT(l.schedule_start , '%Y/%m/%d %h:%i:%s') as 'schedule_start',
							DATE_FORMAT(l.schedule_end , '%Y/%m/%d %h:%i:%s') as 'schedule_end', 
							u.name, 
							GROUP_CONCAT(p.user_id) AS participant, 
							COUNT(p.user_id) AS total_participant, 
							LEFT(l.schedule_start, 10) AS tanggal, 
							MID(l.schedule_start, 12, 8) AS waktu_start, 
							MID(l.schedule_end,12, 8) AS waktu_end 
							FROM 
							liveclass l 
							LEFT JOIN user u ON  (l.moderator IS NOT NULL AND l.moderator = u.user_id) OR u.user_id = l.user_id 
							LEFT JOIN liveclass_participant p ON l.class_id = p.class_id 
							WHERE l.folder_id='${str.project_id}' 
							GROUP BY l.class_id 
							ORDER BY l.class_id DESC`;

							db.query(req.params.level == 'client' ? queryUser : queryAdmin, async (error, result, fields) => {

								//return console.log(error, "SQL")
								try {
									for (var i = 0; i < result.length; i++) {
										// ===================================
										let getListBooking = await db.query(`
											SELECT
												b.*, u.name, um.name as moderator_name,
												cast(concat(b.tanggal, ' ', b.jam_mulai) as DATETIME) as tgl_mulai,
												cast(concat(b.tanggal, ' ', b.jam_selesai) as DATETIME) as tgl_selesai,
												(IF(NOW() BETWEEN cast(concat(b.tanggal, ' ', b.jam_mulai) as DATETIME) AND cast(concat(b.tanggal, ' ', b.jam_selesai) as DATETIME) != '1', '0', '1')) AS on_schedule
											FROM liveclass_booking b
												LEFT JOIN user u ON b.user_id=u.user_id
												LEFT JOIN user um ON b.moderator = um.user_id
											WHERE b.meeting_id='${result[i].class_id}' AND b.tanggal>=CURDATE()
											ORDER BY b.tanggal DESC`
										);

										if (getListBooking.length > 0) {

											let getListparticipant = await db.query(`
												SELECT 
													distinct u.user_id ,u.name, u.avatar as 'image', 'participant' as 'type'
												FROM liveclass_booking b 
													LEFT JOIN user u ON b.user_id=u.user_id 
												WHERE b.meeting_id='${result[i].class_id}'; 
												select 
													distinct a.name ,
													a.voucher as 'user_id',
													null as 'image' ,
													'guest' as 'type'
												from liveclass_booking_tamu a 
												where a.meeting_id = ${result[i].class_id};
												`
											);

											let participants = [];
											if (getListparticipant.length > 0) {
												getListparticipant[0].forEach((get1) => {
													participants.push(get1);
												});
												getListparticipant[1].forEach((get1) => {
													participants.push(get1);
												});
											}
											result[i].participants = participants;

											result[i].on_schedule = false;
											for (var j = 0; j < getListBooking.length; j++) {
												const tanggal = getListBooking[j].tanggal
												getListBooking[j].tanggal = moment.tz(tanggal, 'Asia/Jakarta').format('DD-MM-YYYY')

												let api = bbb.api(BBB_URL, BBB_KEY)
												let http = bbb.http
												let checkUrl = api.monitoring.isMeetingRunning(getListBooking[j].id)
												let { returncode, running } = await http(checkUrl)
												getListBooking[j].running = returncode === 'SUCCESS' ? running : false

												// Check on schedule & user as participant
												let dataParticipants = await db.query(`SELECT p.* FROM liveclass_booking_participant p WHERE p.booking_id='${getListBooking[j].id}' AND p.user_id='${req.params.user_id}'`);
												if (getListBooking[j].on_schedule === "1" && dataParticipants.length) {
													result[i].on_schedule = true;
													result[i].on_schedule_id = getListBooking[j].id;
													result[i].isJoin = true;
												} else if (getListBooking[j].running === true && dataParticipants.length > 0) {
													result[i].on_schedule = true;
													result[i].on_schedule_id = getListBooking[j].id;
													result[i].isJoin = true;
												}
											}

											let thisNow = moment()

											result[i].booking_count = getListBooking.length
											result[i].booking_upcoming = getListBooking.filter(x => x.running === true || thisNow.isBetween(x.tgl_mulai, x.tgl_selesai)).length
											result[i].booking_schedule = getListBooking.filter(x => x.running === true).length ? `${getListBooking.filter(x => x.running === true)[0].tanggal} ${getListBooking.filter(x => x.running === true)[0].jam_mulai} - ${getListBooking.filter(x => x.running === true)[0].jam_selesai}` : '-'
											//result[i].status = getListBooking.length === 0 ? 'Empty' : getListBooking.filter(x => x.running === true).length ? `Active` : `${result[i].booking_upcoming > 0 ? result[i].booking_upcoming : 'No'} upcoming meetings`;
											//result[i].status = getListBooking.length === 0 ? 'Empty' : getListBooking.filter(x => x.running === true).length ? `Active` : result[i].booking_upcoming > 0 && !check_open ? 'Open' : `${result[i].booking_upcoming > 0 ? result[i].booking_upcoming : 'No'} upcoming meetings`;

											if (getListBooking.length === 0) {

												result[i].status = 'Empty';
											} else if (getListBooking.filter(x => x.running === true).length > 0) {

												result[i].status = 'Active';
											} else {
												if (result[i].on_schedule) {
													result[i].status = 'Open'
												} else {

													let thisNow = new Date();
													let count_upcoming2 = getListBooking.filter(x => {
														let split_date = x.tanggal.split("-");
														if (new Date(x.tgl_mulai) >= thisNow && thisNow <= new Date(x.tgl_selesai)) {
															return true;
														} else {
															return false;
														}
													}).length;

													result[i].status = `${count_upcoming2 > 0 ? count_upcoming2 : 'No'} upcoming meetings`;
													//result[i].status = `upcoming`;
												}

											}

											result[i].room_status = result[i].status === 'Active' ? 'On going' : '-';
										}
										result[i].status = 'Empty';
										// ===================================
									}

									// sort active status
									let active = [];
									let non_active = [];
									for (var i = 0; i < result.length; i++) {
										if (result[i].status === 'Active') {
											active.push(result[i])
										} else {
											non_active.push(result[i])
										}
									}

									result = active.concat(non_active);
									result.forEach((str2) => {

										let access = false;
										//console.log(str2.user_id, req.params)
										if (str2.user_id == req.params.user_id || req.params.level === 'superadmin' || req.params.level === 'admin') {
											access = true;
										}
										meet.push({
											id: str2.class_id,
											title: str2.room_name,
											datetime: str2.datetime,
											schedule_start: str2.schedule_start,
											schedule_end: str2.schedule_end,
											list_participants: str2.list_participants,
											status: str2.status,
											is_delete: access,
											is_edit: access,
											type: 'Meeting'
										});
									});
									// tmp.push(obj)
									// // console.log(result.length, "TEST")
									// callback(null);
									cb_parallel(false, meet)
								} catch (e) {
									cb_parallel(false, meet)
								}
							});

						} else {
							cb_parallel(false, meet)
							// tmp.push(obj);
							// callback(null);
						}
					},

				], (err_parallel, res_parallel) => {
					//return res.json({ arr: res_parallel, obj: obj });
					var a = [];
					try {
						if (res_parallel[0].length > 0) {
							res_parallel[0].forEach((arr_val) => { a.push(arr_val); })
							//a.push(obj.event.concat(res_parallel[0]));
						}
						if (res_parallel[1].length > 0) {
							res_parallel[1].forEach((arr_val) => { a.push(arr_val); })
							//a.push(obj.event.concat(res_parallel[1]));
						}

						obj.event = a;
					} catch (e) {
						console.log(arr[0], "ERROR ARR 0")
						console.log(arr[1], "ERROR ARR 1", res_parallel)
						return console.log("ERROR")
					}
					tmp.push(obj);
					callback(null);

				});


				//callback(null);

			}, (err) => {
				if (err) {
					cb(err, null)
				} else {
					cb(false, tmp);
				}
			});
		}
	], (err, val) => {
		if (err) {
			res.json(err);
		} else {
			res.json({ error: false, result: val });
		}
	});

};

exports.Sidebar_ProjectList = async (req, res, next) => {

	let checks = await checkAccess({
		company_id: req.params.company_id.toString(),
		sub: 'general',
		role: req.app.token.level === 'superadmin' ? 'admin' : req.app.token.level,
		code: ['CD_PROJECT']
	}, req);

	checks.map((str) => {
		if (!str.status) {
			return res.json({ error: true, result: `You dont have permission for this action, please contact your administrator.`, access: checks });
		}
	});

	asyncs.waterfall([
		(cb) => {
			let queryUser = `SELECT p.id, p.name AS title,
			(SELECT COUNT(l.class_id) FROM liveclass l LEFT JOIN (SELECT * FROM liveclass_participant lp WHERE lp.user_id='${req.params.user_id}') AS lp ON lp.class_id=l.class_id WHERE l.folder_id=p.id AND ((l.is_private='1' AND lp.user_id='53') OR l.is_private='0')) AS meeting,
			(SELECT COUNT(w.id) FROM webinars w WHERE w.project_id=p.id AND w.publish=1) AS webinar,
			IF(p.company_id!='${req.params.company_id}', c.company_name, null) AS share_from, p.update_at as recent_project,
			pu.role AS role
			FROM files_folder p
			LEFT JOIN (SELECT * FROM files_folder_user WHERE user_id='${req.params.user_id}') pu ON pu.folder_id=p.id
			LEFT JOIN company c ON c.company_id=p.company_id
			LEFT JOIN project_share s ON s.project_id=p.id
			WHERE (p.company_id='${req.params.company_id}' OR s.user_id='${req.params.user_id}') AND p.publish='1' AND p.mother='0' AND ((p.is_limit=1 AND (pu.role='Project Admin') OR (pu.role='User')) OR p.is_limit=0 OR s.user_id='${req.params.user_id}')
			GROUP BY p.id
			ORDER BY p.update_at DESC`;

			let queryAdmin = `SELECT p.id, p.name AS title, count(DISTINCT l.class_id) AS meeting, count(DISTINCT w.id) AS webinar, IF(p.company_id!='${req.params.company_id}', c.company_name, null) AS share_from, p.update_at as recent_project, 'Project Admin' AS role
			FROM files_folder p
			LEFT JOIN liveclass l ON p.id=l.folder_id
			LEFT JOIN (SELECT * FROM webinars w WHERE w.publish=1) AS w ON w.project_id=p.id
			LEFT JOIN project_share s ON s.project_id=p.id
			LEFT JOIN company c ON c.company_id=p.company_id
			WHERE (p.company_id='${req.params.company_id}' OR s.user_id='${req.params.user_id}') AND p.mother='0' AND p.publish='1'
			GROUP BY p.id
			ORDER BY p.update_at DESC`;

			db.query(req.params.level == 'client' ? queryUser : queryAdmin, (error, result, fields) => {
				if (error) {
					cb({ error: true, result: error });
				} else {
					let tmp = [];
					result.forEach((str) => {
						tmp.push({
							project_id: str.id,
							title: str.title,
							count_meet: str.meeting,
							count_webinar: str.webinar,
							is_project_admin: str.role === 'Project Admin' ? true : false
						});

					});
					cb(false, tmp);
				}
			});

		}
	], (err, val) => {
		if (err) {
			res.json(err);
		} else {
			res.json({ error: false, result: val, access: checks });
		}
	});

};

exports.Sidebar_ProjectDetail = async (req, res, next) => {

	let checks = await checkAccess({
		company_id: req.app.token.company_id.toString(),
		sub: 'general',
		role: req.app.token.level === 'superadmin' ? 'admin' : req.app.token.level,
		code: ['CD_PROJECT', 'CD_MEETING']
	}, req);

	checks.map((str) => {
		if (!str.status) {
			return res.json({ error: true, result: `You dont have permission for this action, please contact your administrator.`, access: checks });
		}
	});

	asyncs.waterfall([
		(cb) => {
			let queryUser = `SELECT p.id, p.name AS title,
			(SELECT COUNT(l.class_id) FROM liveclass l LEFT JOIN (SELECT * FROM liveclass_participant lp WHERE lp.user_id='${req.params.user_id}') AS lp ON lp.class_id=l.class_id WHERE l.folder_id=p.id AND ((l.is_private='1' AND lp.user_id='53') OR l.is_private='0')) AS meeting,
			(SELECT COUNT(w.id) FROM webinars w WHERE w.project_id=p.id AND w.publish=1) AS webinar, 
			p.update_at as recent_project
			FROM files_folder p
			LEFT JOIN files_folder_user pu ON pu.folder_id=p.id
			LEFT JOIN company c ON c.company_id=p.company_id
			LEFT JOIN project_share s ON s.project_id=p.id
			WHERE (p.id='${req.params.project_id}' OR s.user_id='${req.params.user_id}') AND p.publish='1' AND p.mother='0' AND ((p.is_limit=1 AND (pu.user_id='${req.params.user_id}' AND pu.role='Project Admin') OR (pu.user_id='${req.params.user_id}' AND pu.role='User')) OR p.is_limit=0 OR s.user_id='${req.params.user_id}')
			GROUP BY p.id
			ORDER BY p.update_at DESC`;

			let queryAdmin = `SELECT p.id, p.name AS title, count(DISTINCT l.class_id) AS meeting, count(DISTINCT w.id) AS webinar, p.update_at as recent_project
			FROM files_folder p
			LEFT JOIN liveclass l ON p.id=l.folder_id
			LEFT JOIN (SELECT * FROM webinars w WHERE w.publish=1) AS w ON w.project_id=p.id
			LEFT JOIN project_share s ON s.project_id=p.id
			LEFT JOIN company c ON c.company_id=p.company_id
			WHERE (p.id='${req.params.project_id}' OR s.user_id='${req.params.user_id}') AND p.mother='0' AND p.publish='1' 
			GROUP BY p.id
			ORDER BY p.update_at DESC`;

			db.query(req.params.level == 'client' ? queryUser : queryAdmin, (error, result, fields) => {
				if (error) {
					cb({ error: true, result: error });
				} else {
					let tmp = [];
					result.forEach((str) => {
						if (str.id == req.params.project_id) {
							tmp.push({
								project_id: str.id,
								title: str.title,
								event: [],
								count_meet: str.meeting,
								count_webinar: str.webinar
							});
						}

					});
					cb(false, tmp);
				}
			});

		}, (val, cb) => {
			// MEET
			let tmp = [];
			asyncs.eachSeries(val, (str, callback) => {

				let obj = str;
				let meet = [];
				let webi = [];
				asyncs.parallel([

					(cb_parallel) => {
						if (str.count_webinar > 0) {

							db.query(`SELECT *, 
									IF(tanggal IS NULL,
										IF(start_time IS NULL,
											'No Schedule',
											CONCAT( 
												DATE_FORMAT(start_time , '%Y/%m/%d'), 
												' | ', 
												DATE_FORMAT(start_time , '%h:%m %p'), 
												' - ', 
												DATE_FORMAT(end_time , '%h:%m %p') 
											)
										), 
										CONCAT( 
											DATE_FORMAT(tanggal , '%Y/%m/%d'), 
											' | ', 
											DATE_FORMAT(jam_mulai , '%h:%m %p'), 
											' - ', 
											DATE_FORMAT(jam_selesai , '%h:%m %p') 
										) 
									) as datetime ,
									DATE_FORMAT(start_time , '%Y/%m/%d %h:%i:%s') as 'schedule_start',
									DATE_FORMAT(end_time , '%Y/%m/%d %h:%i:%s') as 'schedule_end',
									IF((training_course_id IS NULL OR training_course_id = 0), 0, training_course_id) AS training_course_id 
									
									FROM webinars
									WHERE project_id = '${str.project_id}' AND publish='1'
									ORDER BY id DESC`, async (error, result, fields) => {

								try {
									for (var i = 0; i < result.length; i++) {
										let participant = [];

										let moderator = await db.query(`SELECT user_id, name FROM user WHERE user_id IN (${result[i].moderator})`);
										result[i].moderator = moderator.length != 0 ? moderator : { user_id: result[i].moderator, name: "Anonymous" };

										let sekretaris = await db.query(`SELECT user_id, name FROM user WHERE user_id IN (${result[i].sekretaris})`);
										result[i].sekretaris = sekretaris.length != 0 ? sekretaris : { user_id: result[i].sekretaris, name: "Anonymous" };

										// if (result[i].pembicara) {
										// 	let pembicara = await db.query(`SELECT user_id, name FROM user WHERE user_id IN (${result[i].pembicara})`);
										// 	result[i].pembicara = pembicara.length != 0 ? pembicara : { user_id: result[i].pembicara, name: "Anonymous" };
										// } else {
										// 	result[i].pembicara = null
										// }

										let split = result[i].pembicara.split(",");

										let tmp = { object: [], user_id: [], guest: [] }
										for (let j = 0; j < split.length; j++) {
											if (split[j] !== "") {
												if (isNaN(Number(split[j]))) {
													tmp.guest.push(split[j]);
													tmp.object.push({ user_id: split[j], name: "Anonymous", type: "guest", email: null, phone: null });
												} else {
													tmp.user_id.push(split[j]);
													tmp.object.push({ user_id: split[j], name: "Anonymous", type: "user", email: null, phone: null });
												}
											}
										}

										if (split.length > 0 && tmp.object.length > 0) {

											let queryString = '';
											let params = [];
											if (tmp.user_id.length > 0) {
												queryString = `SELECT user_id, name, email,phone FROM user WHERE user_id IN(?);`;
												params.push(tmp.user_id);
											}
											if (tmp.guest.length > 0) {
												queryString = `${queryString}SELECT wt.voucher as user_id, wt.name ,wt.email,wt.phone from webinar_tamu wt where wt.voucher in(?);`;
												params.push(tmp.guest);
											}

											let pembicara = await db.query(queryString, params);

											if (pembicara && pembicara.length > 1 && tmp.guest.length > 0 && Array.isArray(pembicara[0])) {
												pembicara = pembicara[0].concat(pembicara[1]);
											}

											try {
												tmp.object.forEach((str, i) => {
													let idx = pembicara.findIndex((check) => { return check.user_id == str.user_id });
													if (idx > -1) {
														tmp.object[i].name = pembicara[idx].name;
														tmp.object[i].email = pembicara[idx].email;
														tmp.object[i].phone = pembicara[idx].phone;
													}
												});
											} catch (e) {
												// handle error undefined variable
											}

											// build peserta_speaker && guest_speaker
											result[i].peserta_speaker = [];
											result[i].guest_speaker = [];
											tmp.object.forEach((str) => {
												if (str.type === "guest") {
													result[i].guest_speaker.push(str);
												} else {
													result[i].peserta_speaker.push(str);
												}
											});
											result[i].pembicara = tmp.object;
										} else {
											result[i].pembicara = [];
										}

										let owner = await db.query(`SELECT user_id, name FROM user WHERE user_id IN (${result[i].owner})`);
										result[i].owner = owner.length != 0 ? owner : { user_id: result[0].owner, name: "Anonymous" };

										let peserta = await db.query(`SELECT w.*, u.name, u.avatar as 'image' , u.email, 'participant' as 'type' FROM webinar_peserta w JOIN user u ON u.user_id = w.user_id WHERE webinar_id = '${result[i].id}'`)
										result[i].peserta = peserta;

										let tamu = await db.query(`SELECT webinar_id, name, email, voucher as 'user_id', null as 'image', 'tamu' as 'type' FROM webinar_tamu WHERE webinar_id='${result[i].id}'`);
										result[i].tamu = tamu;

										if (tamu.length > 0) {
											tamu.forEach(str_user => participant.push({ user_id: str_user.user_id, name: str_user.name, email: str_user.email, image: str_user.image, type: str_user.type }));
										}
										if (peserta.length > 0) {
											peserta.forEach(str_user => participant.push({ user_id: str_user.user_id, name: str_user.name, email: str_user.email, image: str_user.image, type: str_user.type }));
										}
										result[i].participant = participant;
									}

									result.forEach((str2) => {

										let access = false;
										if (str2.moderator.user_id == req.params.user_id || req.params.level === 'superadmin' || req.params.level === 'admin') {
											access = true;
										}
										var status = ['Incomplete', 'Not Started', 'On Going', 'Done']
										webi.push({
											id: str2.id,
											title: str2.judul,
											schedule_start: str2.schedule_start,
											schedule_end: str2.schedule_end,
											datetime: str2.datetime,
											participants: str2.participant,
											training_course_id: str2.training_course_id,
											status: status[str2.status--],
											is_delete: access,
											is_edit: access,
											type: 'Webinar'
										});
									});
									// tmp.push(obj);
									// callback(null);
									cb_parallel(false, webi)
								} catch (e) {
									console.log(e, "TEST WEBINAR");
									cb_parallel(false, webi)
									// tmp.push(obj);
									// callback(null);
								}
							})
						} else {
							cb_parallel(false, webi)
						}
					},
					(cb_parallel) => {

						if (str.count_meet > 0) {


							let queryUser = `
							SELECT 
								-- distinct 
								l.class_id, 
								l.user_id, 
								l.room_name,  
								l.is_scheduled, 
								IF(l.schedule_start IS NULL,
									'No Schedule',
									CONCAT(
										DATE_FORMAT(l.schedule_start , '%m/%d/%Y'),
										' | ',
										DATE_FORMAT(l.schedule_start , '%h:%m %p'),
										' - ',
										DATE_FORMAT(l.schedule_end , '%h:%m %p')
									)
								) as datetime,
								DATE_FORMAT(l.schedule_start , '%Y-%m-%d %h:%i:%s') as 'schedule_start',
								DATE_FORMAT(l.schedule_end , '%Y-%m-%d %h:%i:%s') as 'schedule_end',
								u.name, 
								GROUP_CONCAT(p.user_id) AS participant, 
								COUNT(p.user_id) AS total_participant, 
								LEFT(l.schedule_start, 10) AS tanggal, 
								MID(l.schedule_start, 12, 8) AS waktu_start, 
								MID(l.schedule_end,12, 8) AS waktu_end 
							FROM 
								liveclass l 
								LEFT JOIN user u ON  (l.moderator IS NOT NULL AND l.moderator = u.user_id) OR u.user_id = l.user_id 
								LEFT JOIN liveclass_participant p ON l.class_id = p.class_id 
							WHERE l.folder_id='${str.project_id}' AND ((l.is_private='1' AND p.user_id='${req.params.user_id}') OR l.is_private='0') 
							GROUP BY l.class_id
							ORDER BY l.class_id DESC`;

							let queryAdmin = `
							SELECT 
							-- distinct 
							l.class_id, 
							l.user_id, 
							l.room_name, 
							l.is_scheduled, 
							IF(l.schedule_start IS NULL,
								'No Schedule',
								CONCAT(
									DATE_FORMAT(l.schedule_start , '%m/%d/%Y'),
									' | ',
									DATE_FORMAT(l.schedule_start , '%h:%m %p'),
									' - ',
									DATE_FORMAT(l.schedule_end , '%h:%m %p')
								)
							) as datetime, 
							DATE_FORMAT(l.schedule_start , '%Y-%m-%d %h:%i:%s') as 'schedule_start',
							DATE_FORMAT(l.schedule_end , '%Y-%m-%d %h:%i:%s') as 'schedule_end', 
							u.name, 
							GROUP_CONCAT(p.user_id) AS participant, 
							COUNT(p.user_id) AS total_participant, 
							LEFT(l.schedule_start, 10) AS tanggal, 
							MID(l.schedule_start, 12, 8) AS waktu_start, 
							MID(l.schedule_end,12, 8) AS waktu_end 
							FROM 
							liveclass l 
							LEFT JOIN user u ON  (l.moderator IS NOT NULL AND l.moderator = u.user_id) OR u.user_id = l.user_id 
							LEFT JOIN liveclass_participant p ON l.class_id = p.class_id 
							WHERE l.folder_id='${str.project_id}' 
							GROUP BY l.class_id 
							ORDER BY l.class_id DESC`;

							db.query(req.params.level == 'client' ? queryUser : queryAdmin, async (error, result, fields) => {
								try {
									for (var i = 0; i < result.length; i++) {
										// ===================================
										let getListBooking = await db.query(`
											SELECT
												b.*, u.name, um.name as moderator_name,
												cast(concat(b.tanggal, ' ', b.jam_mulai) as DATETIME) as tgl_mulai,
												cast(concat(b.tanggal, ' ', b.jam_selesai) as DATETIME) as tgl_selesai,
												IF(b.date_start IS NULL,
													'No Schedule',
													CONCAT(
														DATE_FORMAT(b.date_start , '%m/%d/%Y'),
														' | ',
														DATE_FORMAT(b.date_start , '%h:%m %p'),
														' - ',
														DATE_FORMAT(b.date_end , '%h:%m %p')
													)
												) as datetime, 
												b.date_start as 'schedule_start',
												b.date_end as 'schedule_end',
												(IF(NOW() BETWEEN cast(concat(b.tanggal, ' ', b.jam_mulai) as DATETIME) AND cast(concat(b.tanggal, ' ', b.jam_selesai) as DATETIME) != '1', '0', '1')) AS on_schedule
											FROM liveclass_booking b
												LEFT JOIN user u ON b.user_id=u.user_id
												LEFT JOIN user um ON b.moderator = um.user_id
											WHERE b.meeting_id='${result[i].class_id}' AND b.tanggal>=CURDATE()
											ORDER BY b.tanggal DESC`
										);
										if (getListBooking.length > 0) {
											const tz = getListBooking[0].timezone || 'Asia/Jakarta';

											// result[i].schedule_start = moment.tz(getListBooking[0].schedule_start, tz);
											// result[i].schedule_end = moment.tz(getListBooking[0].schedule_end, tz);
											// console.log(result[i].datetime, getListBooking[0],'???')

											if (result[i].datetime === 'No Schedule') {
												result[i].datetime = getListBooking[0].datetime
												result[i].schedule_start = moment.tz(getListBooking[0].schedule_start, tz);
												result[i].schedule_end = moment.tz(getListBooking[0].schedule_end, tz);
											} else {
												result[i].schedule_start = moment.tz(result[i].schedule_start, tz);
												result[i].schedule_end = moment.tz(result[i].schedule_end, tz);
											}

											console.log(result[i].schedule_start, result[i].schedule_end, 'hoah?')

											let getListparticipant = await db.query(`
												SELECT 
													distinct u.user_id ,u.name, u.email,u.avatar as 'image', 'participant' as 'type', 
													cast(concat(b.tanggal, ' ', b.jam_mulai) as DATETIME) as tgl_mulai, 
													cast(concat(b.tanggal, ' ', b.jam_selesai) as DATETIME) as tgl_selesai, 
													(IF(NOW() BETWEEN cast(concat(b.tanggal, ' ', b.jam_mulai) as DATETIME) AND cast(concat(b.tanggal, ' ', b.jam_selesai) as DATETIME) != '1', '0', '1')) AS on_schedule 
												FROM liveclass_booking b 
													LEFT JOIN user u ON b.user_id=u.user_id 
													LEFT JOIN user um ON b.moderator = um.user_id 
												WHERE b.meeting_id='${result[i].class_id}' AND b.tanggal>=CURDATE(); 
												
												select 
													distinct a.name , a.email,
													a.voucher as 'user_id',
													null as 'image' ,
													'tamu' as 'type'
												from liveclass_booking_tamu a 
												where a.meeting_id = '${getListBooking[0].id}';

												SELECT 
													distinct u.user_id ,u.name, u.email,u.avatar as 'image', 'participant' as 'type' 
												FROM liveclass_booking_participant b 
													LEFT JOIN user u ON u.user_id=b.user_id 
												WHERE b.booking_id='${getListBooking[0].id};'
												`
											);

											let participants = [];
											if (getListparticipant.length > 0) {
												getListparticipant[0].forEach((get1) => {
													participants.push({ user_id: get1.user_id, name: get1.name, email: get1.email, image: get1.image, type: get1.type });
												});
												getListparticipant[1].forEach((get1) => {
													participants.push({ user_id: get1.user_id, name: get1.name, email: get1.email, image: get1.image, type: get1.type });
												});
												getListparticipant[2].forEach((get1) => {
													participants.push({ user_id: get1.user_id, name: get1.name, email: get1.email, image: get1.image, type: get1.type });
												});
											}

											let reduce = [];
											if (participants.length > 0) {
												participants.forEach((str) => {
													let idx = reduce.findIndex((str1) => { return str1.user_id == str.user_id });
													if (idx == -1) {
														reduce.push(str);
													}
												})

												participants = reduce;
											}
											result[i].participants = participants;

											result[i].on_schedule = false;
											for (var j = 0; j < getListBooking.length; j++) {
												const tanggal = getListBooking[j].tanggal
												getListBooking[j].tanggal = moment.tz(tanggal, 'Asia/Jakarta').format('DD-MM-YYYY')

												let api = bbb.api(BBB_URL, BBB_KEY)
												let http = bbb.http
												let checkUrl = api.monitoring.isMeetingRunning(getListBooking[j].id)
												let { returncode, running } = await http(checkUrl)
												getListBooking[j].running = returncode === 'SUCCESS' ? running : false

												// Check on schedule & user as participant
												let dataParticipants = await db.query(`SELECT p.* FROM liveclass_booking_participant p WHERE p.booking_id='${getListBooking[j].id}' AND p.user_id='${req.params.user_id}'`);
												if (getListBooking[j].on_schedule === "1" && dataParticipants.length) {
													result[i].on_schedule = true;
													result[i].on_schedule_id = getListBooking[j].id;
													result[i].isJoin = true;
												} else if (getListBooking[j].running === true && dataParticipants.length > 0) {
													result[i].on_schedule = true;
													result[i].on_schedule_id = getListBooking[j].id;
													result[i].isJoin = true;
												}
											}

											let thisNow = moment()

											result[i].booking_count = getListBooking.length
											result[i].booking_upcoming = getListBooking.filter(x => x.running === true || thisNow.isBetween(x.tgl_mulai, x.tgl_selesai)).length
											result[i].booking_schedule = getListBooking.filter(x => x.running === true).length ? `${getListBooking.filter(x => x.running === true)[0].tanggal} ${getListBooking.filter(x => x.running === true)[0].jam_mulai} - ${getListBooking.filter(x => x.running === true)[0].jam_selesai}` : '-'
											//result[i].status = getListBooking.length === 0 ? 'Empty' : getListBooking.filter(x => x.running === true).length ? `Active` : `${result[i].booking_upcoming > 0 ? result[i].booking_upcoming : 'No'} upcoming meetings`;
											//result[i].status = getListBooking.length === 0 ? 'Empty' : getListBooking.filter(x => x.running === true).length ? `Active` : result[i].booking_upcoming > 0 && !check_open ? 'Open' : `${result[i].booking_upcoming > 0 ? result[i].booking_upcoming : 'No'} upcoming meetings`;

											if (getListBooking.length === 0) {

												result[i].status = 'Empty';
											} else if (getListBooking.filter(x => x.running === true).length > 0) {

												result[i].status = 'Active';
											} else {
												if (result[i].on_schedule) {
													result[i].status = 'Open'
												} else {

													let thisNow = new Date();
													let count_upcoming2 = getListBooking.filter(x => {
														let split_date = x.tanggal.split("-");
														if (new Date(x.tgl_mulai) >= thisNow && thisNow <= new Date(x.tgl_selesai)) {
															return true;
														} else {
															return false;
														}
													}).length;

													if (count_upcoming2 > 0) {
														result[i].status = `${count_upcoming2} upcoming meetings`;
													} else {
														result[i].status = `No upcoming meetings`;
													}
													//result[i].status = `upcoming`;
												}

											}

											result[i].room_status = result[i].status === 'Active' ? 'On going' : '-';
										} else {
											const tz = 'Asia/Jakarta';
											result[i].status = 'Empty';
											result[i].schedule_start = moment.tz(result[i].schedule_start, tz);
											result[i].schedule_end = moment.tz(result[i].schedule_end, tz);
										}
										// ===================================
									}

									// sort active status
									let active = [];
									let non_active = [];
									for (var i = 0; i < result.length; i++) {
										if (result[i].status === 'Active') {
											active.push(result[i])
										} else {
											non_active.push(result[i])
										}
									}

									result = active.concat(non_active);
									result.forEach((str2) => {

										let access = false;
										//console.log(str2.user_id, req.params)
										if (str2.user_id == req.params.user_id || req.params.level === 'superadmin' || req.params.level === 'admin') {
											access = true;
										}
										meet.push({
											id: str2.class_id,
											title: str2.room_name,
											datetime: str2.datetime,
											schedule_start: str2.schedule_start,
											schedule_end: str2.schedule_end,
											participants: str2.participants,
											status: str2.status,
											is_delete: access,
											is_edit: access,
											type: 'Meeting'
										});
									});

									// tmp.push(obj)
									// // console.log(result.length, "TEST")
									// callback(null);
									cb_parallel(false, meet)
								} catch (e) {
									console.log(e, 'ha')
									cb_parallel(false, meet)
								}
							});

						} else {
							cb_parallel(false, meet)
							// tmp.push(obj);
							// callback(null);
						}
					},

				], (err_parallel, res_parallel) => {
					//return res.json({ arr: res_parallel, obj: obj });
					var a = [];
					try {
						if (res_parallel[0].length > 0) {
							res_parallel[0].forEach((arr_val) => { a.push(arr_val); })
							//a.push(obj.event.concat(res_parallel[0]));
						}
						if (res_parallel[1].length > 0) {
							res_parallel[1].forEach((arr_val) => { a.push(arr_val); })
							//a.push(obj.event.concat(res_parallel[1]));
						}

						obj.event = a;
					} catch (e) {
						console.log(arr[0], "ERROR ARR 0")
						console.log(arr[1], "ERROR ARR 1", res_parallel)
						return console.log("ERROR")
					}
					tmp.push(obj);
					callback(null);

				});


				//callback(null);

			}, (err) => {
				if (err) {
					cb(err, null)
				} else {
					if (tmp.length > 0) {
						tmp = tmp[0];
						tmp.event = tmp.event.sort((a, b) => a.status.includes(['Active', 'On Going', 'Open']) ? -1 : 1);
					}
					//&& a.status.includes(['Active', 'On Going'])
					cb(false, tmp);
				}
			});
		}
	], (err, val) => {
		if (err) {
			res.json(err);
		} else {
			res.json({ error: false, result: val, access: checks });
		}
	});

};

exports.homepage_list_event_ongoing = async (req, res, next) => {

	let checks = await checkAccess({
		company_id: req.app.token.company_id.toString(),
		sub: 'general',
		role: req.app.token.level === 'superadmin' ? 'admin' : req.app.token.level,
		code: ['CD_MEETING', 'CD_WEBINAR']
	}, req);

	checks.map((str) => {
		if (!str.status) {
			return res.json({ error: true, result: `You dont have permission for this action, please contact your administrator.`, access: checks });
		}
	});

	let limit = ''
	try {
		if (req.query.limit !== 'max') {
			limit = `LIMIT ${parseInt(req.query.limit)} `;
		}
	} catch (error) {
		console.log(error, `homepage_list_event_ongoing::'LIMIT' not declare`);
	}

	let q_meet = ` SELECT 
				-- distinct 
				lb.id ,
				l.class_id, 
				l.user_id, 
				l.room_name,
				lb.keterangan as 'description',  
				l.is_scheduled, 
				IF(lb.date_start IS NULL,
					'No Schedule',
					CONCAT(
						DATE_FORMAT(lb.date_start , '%m/%d/%Y'),
						' | ',
						DATE_FORMAT(lb.date_start , '%h:%i %p'),
						' - ',
						DATE_FORMAT(lb.date_end , '%h:%i %p')
					)
				) as datetime,
				lb.timezone,
				lb.offset,
				DATE_FORMAT(lb.date_start,'%Y-%m-%d %H:%i:%s') AS schedule_start,
				DATE_FORMAT(lb.date_end ,'%Y-%m-%d %H:%i:%s') AS schedule_end,
				-- lb.date_start as 'schedule_start',
				-- lb.date_end as 'schedule_end',
				u.name, 
				GROUP_CONCAT(p.user_id) AS participant, 
				COUNT(p.user_id) AS total_participant, 
				LEFT(lb.date_start, 10) AS tanggal, 
				MID(lb.date_start, 12, 8) AS waktu_start, 
				MID(lb.date_end,12, 8) AS waktu_end 
			FROM 
				liveclass_booking lb
				LEFT JOIN  liveclass l ON l.class_id = lb.meeting_id
				LEFT JOIN liveclass_booking_participant p ON p.booking_id = lb.id
				LEFT JOIN user u ON  (u.user_id = lb.moderator OR u.user_id = l.user_id OR u.user_id = p.user_id)
				-- liveclass l 
				-- LEFT JOIN liveclass_booking lb ON lb.meeting_id = l.class_id
				-- LEFT JOIN user u ON  (l.moderator IS NOT NULL AND l.moderator = u.user_id) OR u.user_id = l.user_id 
				-- LEFT JOIN liveclass_participant p ON l.class_id = p.class_id 
			WHERE 
				(p.user_id='${req.app.token.user_id}' OR lb.user_id = '${req.app.token.user_id}' OR lb.moderator = '${req.app.token.user_id}')
				-- ((l.is_private='1' AND p.user_id='${req.app.token.user_id}') OR l.is_private='0')
				-- and DATE(lb.date_start) = CURDATE()
				and ( 
					DATE_FORMAT(NOW(),'%Y-%m-%d %H:%i') >= DATE_FORMAT(lb.date_start,'%Y-%m-%d %H:%i') AND 
					DATE_FORMAT(NOW(),'%Y-%m-%d %H:%i') < DATE_FORMAT(lb.date_end,'%Y-%m-%d %H:%i')
				)
				GROUP BY l.class_id
			ORDER BY l.class_id DESC ${limit};`;
	// console.log(q_meet, 9090)
	let q_webinar = `SELECT 
			(SELECT
				group_concat(w.id)
			FROM webinars w
			WHERE 
				(
					w.sekretaris = '${req.app.token.user_id}' OR 
					w.moderator = '${req.app.token.user_id}' OR 
					w.pembicara = '${req.app.token.user_id}' OR  
					w.owner = '${req.app.token.user_id}'
				) and ( 
					DATE_FORMAT(NOW(),'%Y-%m-%d %H:%i') >= DATE_FORMAT(w.start_time,'%Y-%m-%d %H:%i') AND 
					DATE_FORMAT(NOW(),'%Y-%m-%d %H:%i') < DATE_FORMAT(w.end_time,'%Y-%m-%d %H:%i')
				) 
				-- date(w.start_time) = CURDATE() 
			${limit}
			) AS 'webinar',
			(
				SELECT group_concat(wp.webinar_id) 
				FROM webinar_peserta wp 
					LEFT JOIN webinars w2 ON w2.id = wp.webinar_id 
				WHERE 
					wp.user_id = '${req.app.token.user_id}' AND 
					( 
						DATE_FORMAT(NOW(),'%Y-%m-%d %H:%i') >= DATE_FORMAT(w2.start_time,'%Y-%m-%d %H:%i') AND 
						DATE_FORMAT(NOW(),'%Y-%m-%d %H:%i') < DATE_FORMAT(w2.end_time,'%Y-%m-%d %H:%i')
					) 
					-- date(w2.start_time) = CURDATE() 
				${limit}
			) AS 'peserta';`;

	let event_meeting = await db.query(q_meet);
	let meet = [];

	if (event_meeting.length > 0) {
		try {
			for (var i = 0; i < event_meeting.length; i++) {
				// ===================================
				let getListBooking = await db.query(`
					SELECT
						b.*, u.name, um.name as moderator_name,
						cast(concat(b.tanggal, ' ', b.jam_mulai) as DATETIME) as tgl_mulai,
						cast(concat(b.tanggal, ' ', b.jam_selesai) as DATETIME) as tgl_selesai,
						IF(b.date_start IS NULL,
							'No Schedule',
							CONCAT(
								DATE_FORMAT(b.date_start , '%m/%d/%Y'),
								' | ',
								DATE_FORMAT(b.date_start , '%h:%i %p'),
								' - ',
								DATE_FORMAT(b.date_end , '%h:%i %p')
							)
						) as datetime,
						b.timezone,
						b.offset,
						DATE_FORMAT(b.date_start,'%Y-%m-%d %H:%i:%s') AS schedule_start,
						DATE_FORMAT(b.date_end ,'%Y-%m-%d %H:%i:%s') AS schedule_end, 
						-- b.date_start as 'schedule_start',
						-- b.date_end as 'schedule_end', 

						(IF(NOW() BETWEEN cast(concat(b.tanggal, ' ', b.jam_mulai) as DATETIME) AND cast(concat(b.tanggal, ' ', b.jam_selesai) as DATETIME) != '1', '0', '1')) AS on_schedule
					FROM liveclass_booking b
						LEFT JOIN user u ON b.user_id=u.user_id
						LEFT JOIN user um ON b.moderator = um.user_id
					WHERE b.meeting_id='${event_meeting[i].class_id}' AND b.tanggal>=CURDATE()
					ORDER BY b.tanggal DESC`
				);

				if (getListBooking.length > 0) {

					let tz = event_meeting[i].timezone || 'Asia/Jakarta';
					event_meeting[i].schedule_start = moment.tz(event_meeting[i].schedule_start, tz);
					event_meeting[i].schedule_end = moment.tz(event_meeting[i].schedule_end, tz);

					if (event_meeting[i].datetime === 'No Schedule') {

						event_meeting[i].datetime = getListBooking[0].datetime

						tz = getListBooking[0].timezone || 'Asia/Jakarta';
						event_meeting[i].schedule_start = moment.tz(getListBooking[0].schedule_start, tz);
						event_meeting[i].schedule_end = moment.tz(getListBooking[0].schedule_end, tz);

					}

					let getListparticipant = await db.query(`
						SELECT 
							distinct u.user_id ,u.name, u.email,u.avatar as 'image', 'participant' as 'type', 
							cast(concat(b.tanggal, ' ', b.jam_mulai) as DATETIME) as tgl_mulai, 
							cast(concat(b.tanggal, ' ', b.jam_selesai) as DATETIME) as tgl_selesai, 
							(IF(NOW() BETWEEN cast(concat(b.tanggal, ' ', b.jam_mulai) as DATETIME) AND cast(concat(b.tanggal, ' ', b.jam_selesai) as DATETIME) != '1', '0', '1')) AS on_schedule 
						FROM liveclass_booking b 
							LEFT JOIN user u ON b.user_id=u.user_id 
							LEFT JOIN user um ON b.moderator = um.user_id 
						WHERE b.meeting_id='${event_meeting[i].class_id}' AND b.tanggal>=CURDATE(); 
						
						select 
							distinct a.name , a.email,
							a.voucher as 'user_id',
							null as 'image' ,
							'tamu' as 'type'
						from liveclass_booking_tamu a 
						where a.meeting_id = ${event_meeting[i].id};

						SELECT 
							distinct u.user_id ,u.name, u.email,u.avatar as 'image', 'participant' as 'type' 
						FROM liveclass_booking_participant b 
							LEFT JOIN user u ON u.user_id=b.user_id 
						WHERE b.booking_id='${event_meeting[i].id}';
						`
					);

					let participants = [];
					if (getListparticipant.length > 0) {
						getListparticipant[0].forEach((get1) => {
							participants.push({ user_id: get1.user_id, name: get1.name, email: get1.email, image: get1.image, type: get1.type });
						});
						getListparticipant[1].forEach((get1) => {
							participants.push({ user_id: get1.user_id, name: get1.name, email: get1.email, image: get1.image, type: get1.type });
						});

						getListparticipant[2].forEach((get1) => {
							participants.push({ user_id: get1.user_id, name: get1.name, email: get1.email, image: get1.image, type: get1.type });
						});
					}

					let reduce = [];
					// console.log(participants, 90909)
					participants.forEach((str) => {
						let idx = reduce.findIndex((chk) => { return chk.user_id == str.user_id })
						if (idx == -1) {
							reduce.push(str);
						}
					})
					event_meeting[i].participants = reduce;
					event_meeting[i].on_schedule = false;
					event_meeting[i].status = 'On Going';
				} else {
					event_meeting[i].status = 'Empty';
				}

				meet.push({
					id: event_meeting[i].id,
					title: event_meeting[i].room_name,
					description: event_meeting[i].description,
					datetime: event_meeting[i].datetime,
					schedule_start: event_meeting[i].schedule_start,
					schedule_end: event_meeting[i].schedule_end,
					participants: event_meeting[i].participants,
					type: 'Meeting',
					status: 'On Going'
				})
				// ===================================
			}

		} catch (e) {
			console.log(e, 'err parallel')
		}
	}

	let event_webinar = await db.query(q_webinar);
	let reduce = [];
	let tmp = [];

	if (event_webinar[0].webinar) {
		let split_web = event_webinar[0].webinar.split(',');
		split_web.map(str => reduce.push(str));
	}
	if (event_webinar[0].peserta) {
		let split_peserta = event_webinar[0].peserta.split(',');
		split_peserta.map(str => reduce.push(str));
	}

	if (reduce.length > 0) {
		reduce.map((str) => {
			let idx = tmp.findIndex((str2) => { return str2 == str });
			if (idx == -1) {
				tmp.push(str);
			}
		});

		for (let i = 0; i < tmp.length; i++) {

			let result = await db.query(`SELECT 
					id,
					judul as title,
					isi as description, 
					IF(tanggal IS NULL,
						IF(start_time IS NULL,
							'No Schedule',
							CONCAT( 
								DATE_FORMAT(start_time , '%Y/%m/%d'), 
								' | ', 
								DATE_FORMAT(start_time , '%h:%i %p'), 
								' - ', 
								DATE_FORMAT(end_time , '%h:%i %p') 
							)
						), 
						CONCAT( 
							DATE_FORMAT(tanggal , '%Y/%m/%d'), 
							' | ', 
							DATE_FORMAT(jam_mulai , '%h:%i %p'), 
							' - ', 
							DATE_FORMAT(jam_selesai , '%h:%i %p') 
						) 
					) as datetime ,
					start_time as 'schedule_start',
					end_time as 'schedule_end'
					
					FROM webinars
					WHERE id = '${tmp[i]}' AND publish='1'
					ORDER BY id DESC`);
			if (result.length > 0) {
				try {
					for (let j = 0; j < result.length; j++) {
						let participant = [];

						let peserta = await db.query(`SELECT w.*, u.name, u.avatar as 'image' , u.email, 'participant' as 'type' FROM webinar_peserta w JOIN user u ON u.user_id = w.user_id WHERE webinar_id = '${result[j].id}'`)

						let tamu = await db.query(`SELECT webinar_id, name, email, voucher as 'user_id', null as 'image', 'tamu' as 'type' FROM webinar_tamu WHERE webinar_id='${result[j].id}'`);

						if (tamu.length > 0) {
							tamu.forEach(str_user => participant.push({ user_id: str_user.user_id, name: str_user.name, email: str_user.email, image: str_user.image, type: str_user.type }));
						}
						if (peserta.length > 0) {
							peserta.forEach(str_user => participant.push({ user_id: str_user.user_id, name: str_user.name, email: str_user.email, image: str_user.image, type: str_user.type }));
						}
						meet.push({
							id: result[j].id,
							title: result[j].title,
							description: result[j].description,
							datetime: result[j].datetime,
							schedule_start: result[j].schedule_start,
							schedule_end: result[j].schedule_end,
							participants: result[j].participants,
							type: 'Webinar',
							status: 'On Going'
						})

					}

				} catch (e) {
					console.error(e, "TEST WEBINAR====================");
				}
			}
		}

	}

	if (meet.length > 0) {
		meet = meet.sort((a, b) => a.schedule_start - b.schedule_start)
	}
	return res.json({ error: false, result: meet, access: checks });
};

exports.homepage_list_event_upcoming = async (req, res, next) => {

	let limit = ''
	try {
		if (req.query.limit !== 'max') {
			limit = `LIMIT ${parseInt(req.query.limit)} `;
		}
	} catch (error) {
		console.log(error, `homepage_list_event_ongoing::'LIMIT' not declare`);
	}

	let q_meet = ` SELECT 
				-- distinct 
				lb.id ,
				l.class_id,
				lb.keterangan as 'description', 
				l.user_id, 
				l.room_name,
				c.company_name,  
				l.is_scheduled, 
				IF(lb.date_start IS NULL,
					'No Schedule',
					CONCAT(
						DATE_FORMAT(lb.date_start , '%m/%d/%Y'),
						' | ',
						DATE_FORMAT(lb.date_start , '%h:%i %p'),
						' - ',
						DATE_FORMAT(lb.date_end , '%h:%i %p')
					)
				) as datetime,
				-- lb.timezone,
				-- lb.offset,
				DATE_FORMAT(lb.date_start,'%Y-%m-%d %H:%i:%s') AS schedule_start,
				DATE_FORMAT(lb.date_end ,'%Y-%m-%d %H:%i:%s') AS schedule_end,
				-- lb.date_start as 'schedule_start',
				-- lb.date_end as 'schedule_end',
				u.name, 
				GROUP_CONCAT(p.user_id) AS participant, 
				COUNT(p.user_id) AS total_participant, 
				LEFT(lb.date_start, 10) AS tanggal, 
				MID(lb.date_start, 12, 8) AS waktu_start, 
				MID(lb.date_end,12, 8) AS waktu_end 
			FROM 
				liveclass_booking lb
				LEFT JOIN  liveclass l ON l.class_id = lb.meeting_id
				LEFT JOIN liveclass_booking_participant p ON p.booking_id = lb.id
				LEFT JOIN company c ON c.company_id = l.company_id
				LEFT JOIN user u ON  (u.user_id = lb.moderator OR u.user_id = l.user_id OR u.user_id = p.user_id)
				-- LEFT JOIN  liveclass l ON l.class_id = lb.meeting_id
				-- LEFT JOIN user u ON  (l.moderator IS NOT NULL AND l.moderator = u.user_id) OR u.user_id = l.user_id 
				-- LEFT JOIN liveclass_participant p ON l.class_id = p.class_id 
			WHERE
			now() < lb.date_start and
			(p.user_id='${req.app.token.user_id}' OR lb.user_id = '${req.app.token.user_id}' OR lb.moderator = '${req.app.token.user_id}' OR l.user_id = '${req.app.token.user_id}')
				-- (p.user_id='${req.app.token.user_id}' OR lb.user_id = '${req.app.token.user_id}' OR lb.moderator = '${req.app.token.user_id}')
				-- ((l.is_private='1' AND p.user_id='${req.app.token.user_id}') OR l.is_private='0') 
				-- and now() < lb.date_start 
				-- and DATE(lb.date_start) > CURDATE()
				-- and DATE_FORMAT(NOW(),'%Y-%m-%d %H:%i') < DATE_FORMAT(lb.date_start,'%Y-%m-%d %H:%i')
			GROUP BY lb.id
			ORDER BY l.class_id DESC ${limit};`;
	// console.log(q_meet, 9090)
	let q_webinar = `SELECT 
			(SELECT
				group_concat(w.id)
			FROM webinars w
			WHERE 
				(	
					-- w.sekretaris IN ('${req.app.token.user_id}') OR w.moderator IN ('${req.app.token.user_id}') OR w.pembicara IN ('${req.app.token.user_id}') OR w.owner IN ('${req.app.token.user_id}') 

					(w.sekretaris = '${req.app.token.user_id}' OR w.sekretaris like '${req.app.token.user_id},%' OR w.sekretaris like ',${req.app.token.user_id},%' OR w.sekretaris like ',${req.app.token.user_id}%') 
					OR 
					(w.moderator = '${req.app.token.user_id}' OR w.moderator like '${req.app.token.user_id},%' OR w.moderator like ',${req.app.token.user_id},%' OR w.moderator like ',${req.app.token.user_id}%')
					OR 
					(w.pembicara = '${req.app.token.user_id}' OR w.pembicara like '${req.app.token.user_id},%' OR w.pembicara like ',${req.app.token.user_id},%' OR w.pembicara like ',${req.app.token.user_id}%')
					OR
					(w.owner = '${req.app.token.user_id}' OR w.owner like '${req.app.token.user_id},%' OR w.owner like ',${req.app.token.user_id},%' OR w.owner like ',${req.app.token.user_id}%')
					
				) 
				and now() < w.start_time 
				-- DATE_FORMAT(NOW(),'%Y-%m-%d %H:%i') < DATE_FORMAT(w.start_time,'%Y-%m-%d %H:%i')  
				-- date(w.start_time) = CURDATE() 
				
			${limit}
			) AS 'webinar',
			(
				SELECT group_concat(wp.webinar_id) 
				FROM webinar_peserta wp 
					LEFT JOIN webinars w2 ON w2.id = wp.webinar_id 
				WHERE 
					wp.user_id = '${req.app.token.user_id}' AND 
					now() < w2.start_time
					-- DATE_FORMAT(NOW(),'%Y-%m-%d %H:%i') < DATE_FORMAT(w2.start_time,'%Y-%m-%d %H:%i') 
					-- date(w2.start_time) > CURDATE() 
					
				${limit}
			) AS 'peserta';`;

	let event_meeting = await db.query(q_meet);
	let meet = [];

	if (event_meeting.length > 0) {
		try {
			for (var i = 0; i < event_meeting.length; i++) {
				// ===================================
				let getListBooking = await db.query(`
					SELECT
						b.*, u.name, um.name as moderator_name,
						cast(concat(b.tanggal, ' ', b.jam_mulai) as DATETIME) as tgl_mulai,
						cast(concat(b.tanggal, ' ', b.jam_selesai) as DATETIME) as tgl_selesai,
						IF(b.date_start IS NULL,
							'No Schedule',
							CONCAT(
								DATE_FORMAT(b.date_start , '%m/%d/%Y'),
								' | ',
								DATE_FORMAT(b.date_start , '%H:%i %p'),
								' - ',
								DATE_FORMAT(b.date_end , '%H:%i %p')
							)
						) as datetime, 
						b.timezone,
						b.offset,
						DATE_FORMAT(b.date_start,'%Y-%m-%d %H:%i:%s') AS schedule_start,
						DATE_FORMAT(b.date_end ,'%Y-%m-%d %H:%i:%s') AS schedule_end,
						-- b.date_start as 'schedule_start',
						-- b.date_end as 'schedule_end', 

						(IF(NOW() BETWEEN cast(concat(b.tanggal, ' ', b.jam_mulai) as DATETIME) AND cast(concat(b.tanggal, ' ', b.jam_selesai) as DATETIME) != '1', '0', '1')) AS on_schedule
					FROM liveclass_booking b
						LEFT JOIN user u ON b.user_id=u.user_id
						LEFT JOIN user um ON b.moderator = um.user_id
					WHERE b.meeting_id='${event_meeting[i].class_id}' AND b.tanggal>=CURDATE()
					ORDER BY b.tanggal DESC`
				);

				if (getListBooking.length > 0) {

					let tz = event_meeting[i].timezone || 'Asia/Jakarta';
					event_meeting[i].schedule_start = moment.tz(event_meeting[i].schedule_start, tz);
					event_meeting[i].schedule_end = moment.tz(event_meeting[i].schedule_end, tz);
					if (event_meeting[i].datetime === 'No Schedule') {

						event_meeting[i].datetime = getListBooking[0].datetime
						tz = getListBooking[0].timezone || 'Asia/Jakarta';
						event_meeting[i].schedule_start = moment.tz(getListBooking[0].schedule_start, tz);
						event_meeting[i].schedule_end = moment.tz(getListBooking[0].schedule_end, tz);

					}

					let getListparticipant = await db.query(`
						SELECT 
							distinct u.user_id ,u.name, u.email,u.avatar as 'image', 'participant' as 'type', 
							cast(concat(b.tanggal, ' ', b.jam_mulai) as DATETIME) as tgl_mulai, 
							cast(concat(b.tanggal, ' ', b.jam_selesai) as DATETIME) as tgl_selesai, 
							(IF(NOW() BETWEEN cast(concat(b.tanggal, ' ', b.jam_mulai) as DATETIME) AND cast(concat(b.tanggal, ' ', b.jam_selesai) as DATETIME) != '1', '0', '1')) AS on_schedule 
						FROM liveclass_booking b 
							LEFT JOIN user u ON b.user_id=u.user_id 
							LEFT JOIN user um ON b.moderator = um.user_id 
						WHERE b.meeting_id='${event_meeting[i].class_id}' AND b.tanggal>=CURDATE(); 
						
						select 
							distinct a.name , a.email,
							a.voucher as 'user_id',
							null as 'image' ,
							'tamu' as 'type'
						from liveclass_booking_tamu a 
						where a.meeting_id = ${event_meeting[i].id};

						SELECT 
							distinct u.user_id ,u.name, u.email,u.avatar as 'image', 'participant' as 'type' 
						FROM liveclass_booking_participant b 
							LEFT JOIN user u ON u.user_id=b.user_id 
						WHERE b.booking_id='${event_meeting[i].id}';
						`
					);

					let participants = [];
					if (getListparticipant.length > 0) {
						getListparticipant[0].forEach((get1) => {
							participants.push({ user_id: get1.user_id, name: get1.name, email: get1.email, image: get1.image, type: get1.type });
						});
						getListparticipant[1].forEach((get1) => {
							participants.push({ user_id: get1.user_id, name: get1.name, email: get1.email, image: get1.image, type: get1.type });
						});

						getListparticipant[2].forEach((get1) => {
							participants.push({ user_id: get1.user_id, name: get1.name, email: get1.email, image: get1.image, type: get1.type });
						});
					}

					let reduce = [];
					participants.forEach((str) => {
						let idx = reduce.findIndex((chk) => { return chk.user_id == str.user_id })
						if (idx == -1) {
							reduce.push(str);
						}
					})
					event_meeting[i].participants = reduce;
					event_meeting[i].on_schedule = false;
					event_meeting[i].status = 'On Going';
				} else {
					event_meeting[i].status = 'Empty';
				}

				meet.push({
					id: event_meeting[i].id,
					title: event_meeting[i].room_name,
					description: event_meeting[i].description,
					datetime: event_meeting[i].datetime,
					schedule_start: event_meeting[i].schedule_start,
					schedule_end: event_meeting[i].schedule_end,
					participants: event_meeting[i].participants,
					company_name: event_meeting[i].company_name,
					type: 'Meeting',
					status: 'Up Comming'
				})
				// ===================================
			}

		} catch (e) {
			console.log(e, 'err parallel')
		}
	}

	let event_webinar = await db.query(q_webinar);
	let reduce = [];
	let tmp = [];

	if (event_webinar[0].webinar) {
		let split_web = event_webinar[0].webinar.split(',');
		split_web.map(str => reduce.push(str));
	}
	if (event_webinar[0].peserta) {
		let split_peserta = event_webinar[0].peserta.split(',');
		split_peserta.map(str => reduce.push(str));
	}

	if (reduce.length > 0) {
		reduce.map((str) => {
			let idx = tmp.findIndex((str2) => { return str2 == str });
			if (idx == -1) {
				tmp.push(str);
			}
		});


		for (let i = 0; i < tmp.length; i++) {


			let result = await db.query(`SELECT 
					id,
					judul as title,
					isi as description, 
					IF(tanggal IS NULL,
						IF(start_time IS NULL,
							'No Schedule',
							CONCAT( 
								DATE_FORMAT(start_time , '%Y/%m/%d'), 
								' | ', 
								DATE_FORMAT(start_time , '%h:%m %p'), 
								' - ', 
								DATE_FORMAT(end_time , '%h:%m %p') 
							)
						), 
						CONCAT( 
							DATE_FORMAT(tanggal , '%Y/%m/%d'), 
							' | ', 
							DATE_FORMAT(jam_mulai , '%h:%m %p'), 
							' - ', 
							DATE_FORMAT(jam_selesai , '%h:%m %p') 
						) 
					) as datetime ,
					start_time as 'schedule_start',
					end_time as 'schedule_end',
					c.company_name
					
					FROM webinars
					LEFT JOIN company c ON webinars.company_id = c.company_id
					WHERE id = '${tmp[i]}' AND publish='1'
					ORDER BY id DESC`);

			try {
				for (var j = 0; j < result.length; j++) {
					let participant = [];

					let peserta = await db.query(`SELECT w.*, u.name, u.avatar as 'image' , u.email, 'participant' as 'type' FROM webinar_peserta w JOIN user u ON u.user_id = w.user_id WHERE webinar_id = '${result[j].id}'`)

					let tamu = await db.query(`SELECT webinar_id, name, email, voucher as 'user_id', null as 'image', 'tamu' as 'type' FROM webinar_tamu WHERE webinar_id='${result[j].id}'`);

					if (tamu.length > 0) {
						tamu.forEach(str_user => participant.push({ user_id: str_user.user_id, name: str_user.name, email: str_user.email, image: str_user.image, type: str_user.type }));
					}
					if (peserta.length > 0) {
						peserta.forEach(str_user => participant.push({ user_id: str_user.user_id, name: str_user.name, email: str_user.email, image: str_user.image, type: str_user.type }));
					}


					meet.push({
						id: result[j].id,
						title: result[j].title,
						description: result[j].description,
						datetime: result[j].datetime,
						schedule_start: result[j].schedule_start,
						schedule_end: result[j].schedule_end,
						company_name: result[j].company_name,
						participants: participant,
						type: 'Webinar',
						status: 'Up Comming'
					})
				}

			} catch (e) {
				console.log(e, "TEST WEBINAR");
			}
		}

	}

	if (meet.length > 0) {
		meet = meet.sort((a, b) => a.schedule_start - b.schedule_start)
	}
	return res.json({ error: false, result: meet });

};

exports.getProjectList = (req, res, next) => {
	// let queryUser = `SELECT p.id, p.name AS title, count(DISTINCT l.class_id) AS meeting, count(DISTINCT w.id) AS webinar, IF(p.company_id!=${req.params.company_id}, c.company_name, null) AS share_from
	// FROM files_folder p
	// LEFT JOIN liveclass l ON p.id=l.folder_id
	// LEFT JOIN liveclass_participant lp ON lp.class_id=l.class_id
	// LEFT JOIN files_folder_user pu ON pu.folder_id=p.id
	// LEFT JOIN webinars w ON w.project_id=p.id
	// LEFT JOIN project_share s ON s.project_id=p.id
	// LEFT JOIN company c ON c.company_id=p.company_id
	// WHERE (p.company_id='${req.params.company_id}' OR s.user_id='${req.params.user_id}') AND (p.mother='0' AND p.publish='1' AND (l.is_private='0' OR (l.is_private='1' AND lp.user_id='${req.params.user_id}')) OR ((pu.user_id='${req.params.user_id}' AND pu.role='Project Admin') OR (pu.user_id='${req.params.user_id}' AND pu.role='User')) OR w.id IS NOT NULL) AND (w.publish='1' OR w.publish IS NULL OR w.publish='0') AND p.publish='1'
	// GROUP BY p.id`;

	let queryUser = `SELECT p.id, p.name AS title,
	(SELECT COUNT(l.class_id) FROM liveclass l LEFT JOIN (SELECT * FROM liveclass_participant lp WHERE lp.user_id='${req.params.user_id}') AS lp ON lp.class_id=l.class_id WHERE l.folder_id=p.id AND ((l.is_private='1' AND lp.user_id='53') OR l.is_private='0')) AS meeting,
	(SELECT COUNT(w.id) FROM webinars w WHERE w.project_id=p.id AND w.publish=1) AS webinar,
	IF(p.company_id!='${req.params.company_id}', c.company_name, null) AS share_from, p.update_at as recent_project
	FROM files_folder p
	LEFT JOIN files_folder_user pu ON pu.folder_id=p.id
	LEFT JOIN company c ON c.company_id=p.company_id
	LEFT JOIN project_share s ON s.project_id=p.id
	WHERE (p.company_id='${req.params.company_id}' OR s.user_id='${req.params.user_id}') AND p.publish='1' AND p.mother='0' AND ((p.is_limit=1 AND (pu.user_id='${req.params.user_id}' AND pu.role='Project Admin') OR (pu.user_id='${req.params.user_id}' AND pu.role='User')) OR p.is_limit=0 OR s.user_id='${req.params.user_id}')
	GROUP BY p.id
	ORDER BY p.update_at DESC`;

	let queryAdmin = `SELECT p.id, p.name AS title, count(DISTINCT l.class_id) AS meeting, count(DISTINCT w.id) AS webinar, IF(p.company_id!='${req.params.company_id}', c.company_name, null) AS share_from, p.update_at as recent_project
	FROM files_folder p
	LEFT JOIN liveclass l ON p.id=l.folder_id
	LEFT JOIN (SELECT * FROM webinars w WHERE w.publish=1) AS w ON w.project_id=p.id
	LEFT JOIN project_share s ON s.project_id=p.id
	LEFT JOIN company c ON c.company_id=p.company_id
	WHERE (p.company_id='${req.params.company_id}' OR s.user_id='${req.params.user_id}') AND p.mother='0' AND p.publish='1'
	GROUP BY p.id
	ORDER BY p.update_at DESC`;

	db.query(req.params.level == 'client' ? queryUser : queryAdmin, (error, result, fields) => {
		if (error) {
			res.json({ error: true, result: error });
		} else {
			res.json({ error: false, result: result });
		}
	});
};

exports.getProjectListV2 = (req, res, next) => {
	let sql = `SELECT ff.id, ff.name AS title, ff.company_id, ffs.user_id, ffs.role
		FROM
			files_folder ff LEFT JOIN files_folder_user ffs ON ffs.folder_id = ff.id
		WHERE ff.company_id = '${req.params.company_id}'
			AND ffs.user_id = '${req.params.user_id}'
			AND ff.mother = '0'
			AND ff.publish = '1'`;

	let resposne = [];
	db.query(sql, async (error, result, fields) => {
		if (error) res.json({ error: true, result: error });

		for (var i = 0; i < result.length; i++) {
			let countMeeting = await db.query(`SELECT l.class_id, l.room_name AS name, l.folder_id FROM liveclass l LEFT JOIN liveclass_participant lp ON lp.class_id = l.class_id WHERE l.folder_id = '${result[i].id}' AND (l.is_private = '0' OR (l.is_private = '1' AND lp.user_id = '${req.params.user_id}'))`);
			result[i].meeting = countMeeting.length;
			let countWebinar = [];
			result[i].webinar = countWebinar.length;
		}

		res.json({ error: false, result: result });
	})
}


exports.createKpi = (req, res) => {
	let sql = `INSERT INTO learning_kpi (company_id, name) VALUES (?)`;
	let data = [req.body.companyId, req.body.name];

	db.query(sql, [data], (err, rows) => {
		if (err) res.json({ error: true, result: err })

		db.query(`SELECT * FROM learning_kpi WHERE id_kpi = ?`, [rows.insertId], (err, rows) => {
			res.json({ error: false, result: rows[0] })
		})
	})
}

exports.getKpiByCompanyId = (req, res) => {
	let sql = `SELECT * FROM learning_kpi WHERE company_id = ?`;
	let data = [req.params.companyId]

	db.query(sql, data, (err, rows) => {
		if (err) res.json({ error: true, result: err })

		res.json({ error: false, result: rows })
	})
}

exports.getKpiById = (req, res) => {
	let sql = `SELECT * FROM learning_kpi WHERE id_kpi = ?`;
	let data = [req.params.id]

	db.query(sql, data, (err, rows) => {
		if (err) res.json({ error: true, result: err })

		res.json({ error: false, result: rows.length ? rows[0] : [] })
	})
}

exports.uploadKpiById = (req, res) => {
	uploadKPI(req, res, (err) => {
		if (!req.file) {
			res.json({ error: true, result: err });
		} else {

			if (env.AWS_BUCKET) {
				let path = req.file.path;
				let newDate = new Date();
				let keyName = `${env.AWS_BUCKET_ENV}/company`;

				var params = {
					ACL: 'public-read',
					Bucket: env.AWS_BUCKET,
					Body: fs.createReadStream(path),
					Key: `${keyName}/${req.file.originalname}`
				};

				s3.upload(params, (err, data) => {
					if (err) {
						res.json({ error: true, msg: "Error KPI", result: err });
					}
					if (data) {
						fs.unlinkSync(path);

						db.query(`UPDATE learning_kpi SET file = '${data.Location}' WHERE id_kpi = '${req.params.id}'`, (error, result, fields) => {
							if (error) {
								res.json({ error: true, result: error });
							} else {
								res.json({ error: false, result: data.Location });
							}
						});

					}
				})

			}
			else {
				db.query(`UPDATE learning_kpi SET file = '${env.APP_URL}/company/${req.file.filename}' WHERE id_kpi = '${req.params.id}'`, (error, result, fields) => {
					if (error) {
						res.json({ error: true, result: error });
					} else {
						res.json({ error: false, result: `${env.APP_URL}/company/${req.file.filename}` });
					}
				});
			}

		}
	});
}


exports.deleteKpi = function (req, res) {

	let sql = `DELETE FROM learning_kpi WHERE id = ?`
	let data = [req.params.id];

	db.query(sql, data, (err, rows) => {
		if (err) throw err;

		res.json({ error: false, result: rows })
	})
}

exports.checkLimit = (req, res) => {
	let data = [req.params.id, req.params.id, req.params.id, req.params.id];
	let sql = `SELECT limit_user,
					limit_meeting,
					limit_webinar,
					(SELECT COUNT(user_id) FROM user WHERE status='active' AND company_id=?) AS user,
					(SELECT COUNT(class_id) FROM liveclass WHERE company_id=?) AS meeting,
					(SELECT COUNT(id) FROM webinars WHERE publish='1' AND company_id=?) AS webinar
				FROM company WHERE company_id = ?`;

	db.query(sql, data, (err, rows) => {
		if (err) res.json({ error: true, result: err })

		result = {};
		if (rows.length) {
			result = {
				user: rows[0].limit_user === null || rows[0].user < rows[0].limit_user ? true : false,
				meeting: rows[0].limit_meeting === null || rows[0].meeting < rows[0].limit_meeting ? true : false,
				webinar: rows[0].limit_webinar === null || rows[0].webinar < rows[0].limit_webinar ? true : false
			};
		}
		res.json({ error: false, result: result })
	})
}
