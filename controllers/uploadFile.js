var env = require('../env.json');
var conf = require('../configs/config');
var db = require('../configs/database');



var multer = require('multer');
global.__basedir = __dirname;
var storageTeacher = multer.diskStorage({
	destination: (req, file, cb) => {
		cb(null, './public/teacher/');
	},
	filename: (req, file, cb) => {
		cb(null, 'excel-' + Date.now() + '-' + file.originalname);
	}
});

global.__basedir = __dirname;
var storageStudents = multer.diskStorage({
	destination: (req, file, cb) => {
		cb(null, './public/students/');
	},
	filename: (req, file, cb) => {
		cb(null, 'excel-' + Date.now() + '-' + file.originalname);
	}
});


var uploadGuru = multer({ storage: storageTeacher }).single("uploadfile");
var uploadStudents = multer({ storage: storageStudents }).single("importFile");


// Import data guru

exports.importGuru = function (req, res, next) {

	uploadGuru(req, res, (err) => {
		if (!req.file) {
			res.json({ error: true, result: err });
		} else {

			var Excel = require('exceljs');
			var wb = new Excel.Workbook();
			var path = require('path');
			var filePath = path.resolve(__dirname, '../public/teacher/' + req.file.filename);


			wb.xlsx.readFile(filePath).then(() => {
				var sh = wb.getWorksheet("Sheet1");

				var tempArray = [];
				for (i = 2; i <= sh.rowCount; i++) {

					console.log('==========================================================')

					let data = {
						user_id: req.body.user_id,
						company_id: req.body.company_id,
					}

					var queryFile = `INSERT INTO learning_guru (user_id, company_id, nama,no_induk,tempat_lahir,tanggal_lahir,jenis_kelamin,alamat, telepon, email) VALUES ('${req.body.user_id}','${req.body.company_id}','${sh.getRow(i).getCell(1).value}','${sh.getRow(i).getCell(2).value}','${sh.getRow(i).getCell(3).value}','${sh.getRow(i).getCell(4).value}','${sh.getRow(i).getCell(5).value}','${sh.getRow(i).getCell(6).value}','${sh.getRow(i).getCell(7).value}','${sh.getRow(i).getCell(8).value}')`

					db.query(queryFile, [data.user_id, data.company_id], (err, res, fields) => {
						if (err) {
							console.log(err);
						} else {
							console.log(res);
						}
					});


					tempArray.push({
						nama: sh.getRow(i).getCell(1).value,
						no_induk: sh.getRow(i).getCell(2).value,
						tempat_lahir: sh.getRow(i).getCell(3).value,
						tanggal_lahir: sh.getRow(i).getCell(4).value,
						jenis_kelamin: sh.getRow(i).getCell(5).value,
						alamat: sh.getRow(i).getCell(6).value,
						telepon: sh.getRow(i).getCell(7).value,
						email: sh.getRow(i).getCell(8).value,
					});
				}
				res.json({ error: false, result: tempArray });
			})

			//Get all the rows data [1st and 2nd column]
		}
	})
}


// Import data murid


exports.importStudents = function (req, res, next) {

	uploadStudents(req, res, (err) => {
		if (!req.file) {
			res.json({ error: true, result: err });
		} else {


			var Excel = require('exceljs');
			var wb = new Excel.Workbook();
			var path = require('path');
			var filePath = path.resolve(__dirname, '../public/students/' + req.file.filename);


			wb.xlsx.readFile(filePath).then(async () => {
				var sh = wb.getWorksheet("Sheet1");

				//Get all the rows data [1st and 2nd column]

				let data = {
					company_id: req.body.company_id,
				};
				var tempArray = [];
				var maxUpload = 31;

				if (sh.rowCount > maxUpload) {
					res.json({ error: true, 'message ': 'Batas upload Murid maksimal 30 orang / kelas' });
					console.log('Melebihi Max Upload');
				} else {
					for (i = 2; i <= sh.rowCount; i++) {

						console.log("=============================================================")

						let getName = await db.query(`SELECT name FROM user WHERE company_id = ? `, [data.company_id]);
						console.log(getName);

						let queryMurid = `INSERT INTO learning_murid (
						user_id, company_id,
						nama,no_induk,tempat_lahir,tanggal_lahir,jenis_kelamin,alamat,telepon,email)
						VALUES
						('${getName[0].user_id}','${data.company_id}',
						'${sh.getRow(i).getCell(2).value}',
						'${sh.getRow(i).getCell(3).value}',
						'${sh.getRow(i).getCell(4).value}',
						'${sh.getRow(i).getCell(5).value}',
						'${sh.getRow(i).getCell(6).value}',
						'${sh.getRow(i).getCell(7).value}',
						'${sh.getRow(i).getCell(8).value}',
						'${sh.getRow(i).getCell(9).value}')`;

						db.query(queryMurid, (err, res, fields) => {
							if (err) {
								console.log(err);
							} else {
								console.log("IMPORT SUCCESFULLY");
							}
						});


						tempArray.push({
							nama: sh.getRow(i).getCell(2).value,
							no_induk: sh.getRow(i).getCell(3).value,
							tempat_lahir: sh.getRow(i).getCell(4).value,
							tanggal_lahir: sh.getRow(i).getCell(5).value,
							jenis_kelamin: sh.getRow(i).getCell(6).value,
							alamat: sh.getRow(i).getCell(7).value,
							telepon: sh.getRow(i).getCell(8).value,
							email: sh.getRow(i).getCell(9).value,
						});
					}
					res.json({ error: false, result: tempArray })
				}
			});
		}
	});
};

//  Download template excel 

exports.downloadTemplate = function (req, res) {

	var fileName = "template-upload.xlsx";
	var path = require('path');
	var filePath = path.join(__basedir, '../public/template-excel/');
	console.log(filePath);
	console.log(fileName);



	res.download(filePath + fileName, fileName, (err) => {
		if (err) {
			res.status(500).send({
				message: "Gagal download file  " + err,
			});
		} else {
			console.log("Download file berhasil");
		}
	});
};