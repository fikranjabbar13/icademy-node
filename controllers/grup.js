var conf = require('../configs/config');
var db = require('../configs/database');
const moduleDB = require('../repository');
const validatorRequest = require('../validator');

exports.createGrup = (req, res, next) => {
	var formData = {
		company_id: req.body.company_id,
		grup_name: req.body.grup_name,
		activity: req.body.activity,
		course: req.body.course,
		manage_course: req.body.manage_course,
		forum: req.body.forum,
		group_meeting: req.body.group_meeting,
		manage_group_meeting: req.body.manage_group_meeting
	};
	db.query(`SELECT * FROM grup WHERE grup_name = '${formData.grup_name}' AND company_id='${formData.company_id}'`, (error, result, fields) => {
		if (result.length == 1) {
			res.json({
				error: false,
				result: 'double data'
			});
		}
		else {

			// create Grup
			db.query(`
				INSERT INTO grup
				(grup_id, company_id, grup_name, activity, course, manage_course, forum, group_meeting, manage_group_meeting)
				VALUES
				(null, '${formData.company_id}', '${formData.grup_name}', '${formData.activity}', '${formData.course}', '${formData.manage_course}', '${formData.forum}', '${formData.group_meeting}', '${formData.manage_group_meeting}')`,
				(error, result, fields) => {
					if (error) {
						res.json({ error: true, result: error });
					} else {
						db.query(`SELECT * FROM grup WHERE grup_id = '${result.insertId}'`, (error, result, fields) => {
							res.json({ error: false, result: result[0] });
						})
					}
				});
		}
	})
};

exports.getGrupList = (req, res, next) => {
	db.query(`SELECT g.*, c.company_name FROM grup g JOIN company c ON c.company_id = g.company_id`, (error, result, fields) => {
		if (error) {
			res.json({ error: true, result: error });
		} else {
			res.json({
				error: false,
				result: result
			});
		}
	});

};

exports.getGrupByCompany = (req, res, next) => {
	db.query(`SELECT g.*, c.company_name FROM grup g JOIN company c ON c.company_id = g.company_id WHERE g.company_id = '${req.params.company_id}'`, (error, result, fields) => {
		if (error) {
			res.json({ error: true, result: error });
		} else {
			res.json({
				error: false,
				result: result
			});
		}
	});

};


exports.getGrupOne = (req, res, next) => {
	db.query(`SELECT g.*, c.company_name FROM grup g JOIN company c ON c.company_id = g.company_id WHERE grup_id = '${req.params.grup_id}'`,
		(error, result, fields) => {
			if (error) {
				res.json({ error: true, result: result });
			} else {
				if (result.length == 0) {
					res.json({ error: false, result: result });
				} else {
					res.json({
						error: false,
						result: result[0]
					});
				}
			}
		})
};

exports.updateGrup = (req, res, next) => {
	var formData = {
		company_id: req.body.company_id,
		grup_name: req.body.grup_name,
		activity: req.body.activity,
		course: req.body.course,
		manage_course: req.body.manage_course,
		forum: req.body.forum,
		group_meeting: req.body.group_meeting,
		manage_group_meeting: req.body.manage_group_meeting
	};

	db.query(`UPDATE grup SET
		company_id = '${formData.company_id}',
		grup_name = '${formData.grup_name}',
		activity = '${formData.activity}',
		course = '${formData.course}',
		manage_course = '${formData.manage_course}',
		forum = '${formData.forum}',
		group_meeting = '${formData.group_meeting}',
		manage_group_meeting = '${formData.manage_group_meeting}'
		WHERE grup_id = '${req.params.grup_id}'`,
		(error, result, fields) => {
			if (error) {
				res.json({ error: true, result: error });
			} else {
				db.query(`SELECT * FROM grup WHERE grup_id = '${req.params.grup_id}'`, (error, result, fields) => {
					res.json({ error: false, result: result[0] });
				})
			}
		});
};


exports.deleteGrup = (req, res, next) => {
	db.query(`DELETE FROM grup WHERE grup_id = '${req.params.grup_id}'`, (error, result, next) => {
		if (error) {
			res.json({ error: true, result: error });
		} else {
			res.json({ error: false, result: result });
		}
	})
}

exports.getGrup = (req, res) => {
	db.query(`SELECT g.*, c.company_name FROM grup g JOIN company c ON c.company_id = g.company_id WHERE g.grup_id = '${req.params.grup_id}'`,
		(error, result, fields) => {
			if (error) {
				res.json({ error: true, result: error });
			} else {
				res.json({ error: false, result: result });
			}
		})
}

exports.getAccess = (req, res) => {
	db.query(`SELECT g.id_access, gst.sub, g.company_id, gst.role, gst.code, gst.name, (IF (g.status IS NULL, gst.status, g.status)) AS status, g.created_at, DATE_FORMAT(g.update_at, '%Y-%m-%d %H:%i') as update_at, c.company_name FROM global_setting g LEFT JOIN global_settings_template gst ON gst.id_access = g.id_global_settings_template JOIN company c ON c.company_id = g.company_id WHERE g.company_id = '${req.params.company_id}' AND gst.role = '${req.params.role}'`,
		(error, result, fields) => {
			if (error) {
				res.json({ error: true, result: error });
			} else {
				res.json({ error: false, result: result });
			}
		})
}

exports.checkAccess = async (req, res) => {
	try {
		const dtoAccess = {
			company_id: req.query.company_id,
			role: req.query.role === 'client' ? 'user' : req.query.role,
			code: req.query.code,
		};

		const retrieveValidRequest = await validatorRequest.retrieveAccess(dtoAccess);
		let retrieveDataAccess = await moduleDB.retrieveCheckAccessByRole(retrieveValidRequest);
		retrieveDataAccess = retrieveDataAccess.map(dataAccess => ({ ...dataAccess, status: dataAccess.status ? true : false }));
		res.json({ error: false, result: retrieveDataAccess })

	} catch (error) {
		console.info(error, 1234)
		res.json({ error: true, result: error.message })
	}
}

exports.checkAccess_function = async (data) => {
	try {
		const dtoAccess = {
			company_id: data.company_id,
			role: data.role,
			code: data.code,
		};
		const retrieveValidRequest = await validatorRequest.retrieveAccess(dtoAccess);
		let retrieveDataAccess = await moduleDB.retrieveCheckAccessByRole(retrieveValidRequest);
		retrieveDataAccess = retrieveDataAccess.map(dataAccess => ({ ...dataAccess, status: dataAccess.status ? true : false }));
		return retrieveDataAccess;

	} catch (error) {
		return [];
	}
}

exports.checkAccessBySub = async (data) => {
	try {
		const dtoAccess = data;
		// const retrieveValidRequest = await validatorRequest.retrieveAccess(dtoAccess);
		let retrieveDataAccess = await moduleDB.retrieveCheckAccessBySub(data);
		retrieveDataAccess = retrieveDataAccess.map(dataAccess => ({ ...dataAccess, status: dataAccess.status ? true : false }));
		return retrieveDataAccess;

	} catch (error) {
		return [];
	}
}

exports.checkAccessOLD = (req, res) => {
	let sql = '', data = [];

	sql = `SELECT gst.code, (IF (g.status IS NULL, gst.status, g.status)) AS status FROM global_setting g LEFT JOIN global_settings_template gst ON gst.id_access = g.id_global_settings_template JOIN company c ON c.company_id = g.company_id WHERE g.company_id = ? AND g.level = ? AND code IN (?)`;
	data = [req.query.company_id, req.query.level, req.query.param]

	db.query(sql, data, (err, rows) => {
		if (err) {
			res.json({ error: true, result: err });
		} else {
			let data = [];
			req.query.param.forEach(element => {
				data.push({
					code: element,
					status: rows.filter(x => x.code === element).length ? (rows.filter(x => x.code === element)[0].status === 1 ? true : false) : true
				})
			});

			res.json({ error: false, result: data })
		}
	})
}
exports.checkAccessAll = (req, res) => {
	let sql = ``,
		data = [];
	sql = `SELECT g.*, gst.code, gst.name FROM global_setting g LEFT JOIN global_settings_template gst ON gst.id_access = g.id_global_settings_template JOIN company c ON c.company_id = g.company_id WHERE g.company_id = ?`;
	data = [req.query.company_id]

	db.query(sql, data, (err, rows) => {
		if (err) res.json({ error: true, result: err })

		rows.map(item => {
			item.status = item.status === 1 ? true : false;
		})
		paramLength = req.query.param ? req.query.param.length : 0;
		if (!rows.length) {
			for (let i = 0; i < paramLength; i++) {
				rows[i] = { code: req.query.param[i], status: true }
			}
		}
		res.json({ error: false, result: rows });
	});
}

exports.patchStatus = (req, res) => {
	let sql = `UPDATE global_setting SET status = ?, update_at = ?
	WHERE id_access = ?`
	let date = new Date(new Date() + '+00:00').toISOString().replace("T", " ").substring(0, 19)

	let data = [req.body.status, date, req.params.id_access]
	db.query(sql, data, (err, rows) => {
		if (err) res.json({ error: true, result: err })

		res.json({ error: false, result: { id_access: req.params.id_access, status: req.body.status } })
	})
}

exports.insertGlobalSettingAllCompany = async (req, res) => {
	try {
		const retrieveAllCompany = await moduleDB.getListAllCompany();
		const retrieveAllSettings = await moduleDB.getListAllGlobalSettingsTemplate();
		await moduleDB.removeListAllGlobalSettings();

		for (let i = 0; i < retrieveAllCompany.length; i++) {
			const dataCompany = retrieveAllCompany[i];
			for (let j = 0; j < retrieveAllSettings.length; j++) {
				const dataSettings = retrieveAllSettings[j];
				const dto = {
					sub: dataSettings.sub,
					idCompany: dataCompany.company_id,
					level: dataSettings.level,
					role: dataSettings.role,
					status: 1,
					idGlobalSettingsTemplate: dataSettings.id_access
				}
				await moduleDB.insertGlobalSettings(dto);
			}
		};

		res.json({ error: false, result: 'Success!' });

	} catch (error) {
		res.json({ error: true, result: error.message })
	}
};

exports.createNewGlobalSettings = async (req, res) => {
	try {

		const dtoGlobalSettingsTemplate = {
			sub: req.body.sub,
			role: req.body.role,
			code: req.body.code,
			name: req.body.name,
			status: 1
		};

		const retrieveValidRequest = await validatorRequest.postCreateGlobalSetting(dtoGlobalSettingsTemplate);
		const checkDuplicateValue = await moduleDB.retrieveSettingsTemplate(retrieveValidRequest);
		if (checkDuplicateValue.length) {

			res.json({ error: true, result: 'Duplicate Value Code!' });

		} else {

			//Insert Template
			const insertGlobalSettingsTemplate = await moduleDB.insertGlobalSettingsTemplate(retrieveValidRequest);
			const retrieveAllCompany = await moduleDB.getListAllCompany();

			//Loops
			for (let i = 0; i < retrieveAllCompany.length; i++) {
				const dataCompany = retrieveAllCompany[i];
				const dtoGlobalSettingMasterData = {
					idCompany: dataCompany.company_id,
					status: 1,
					idGlobalSettingsTemplate: insertGlobalSettingsTemplate.insertId,
				};

				//Insert Master Data
				await moduleDB.insertGlobalSettings(dtoGlobalSettingMasterData);

			};

			res.json({ error: false, result: 'Success!' });

		}

	} catch (error) {
		res.json({ error: true, result: error.message })
	}
};
