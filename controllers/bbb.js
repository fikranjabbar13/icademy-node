var env = require('../env.json');
var conf = require('../configs/config');
var db = require('../configs/database');
var moment = require('moment');

var bbb = require('bigbluebutton-js');
const { TooManyRequests } = require('http-errors');

exports.getUrl = (req, res, next) => {
	let userId = req.body.userId;
	let name = req.body.name;

	let id = req.body.id;
	let pwd = req.body.role;
	let room = req.body.room;

	let api = bbb.api(env.BBB_URL, env.BBB_KEY);
	let http = bbb.http;

	let meetingInfo = api.monitoring.getMeetingInfo(id);
	http(meetingInfo).then((result) => {
		if (result.returncode == 'FAILED' && result.messageKey == 'notFound') {
			let meetingCreateUrl = api.administration.create(room, id, {
				attendeePW: 'peserta',
				moderatorPW: 'moderator',
				allowModsToUnmuteUsers: true,
				record: true
			})
			http(meetingCreateUrl).then((result) => {
				if (result.returncode = 'SUCCESS') {
					// Setelah create, join
					let joinUrl = api.administration.join(name, id, pwd, { userID: userId, "userdata-bbb_skip_check_audio": true });
					res.json({ error: false, result: joinUrl, joined: false });
				}
				else {
					res.json({ error: true, result: result });
				}
			});
		}
		else {
			let checkAttendee = !result.attendees.attendee ? 0 : Array.isArray(result.attendees.attendee) ?
				result.attendees.attendee.filter(x =>
					x.userID === userId &&
					(
						x.isListeningOnly ||
						x.hasJoinedVoice ||
						x.hasVideo
					)
				).length
				:
				result.attendees.attendee.userID === userId &&
				(
					result.attendees.attendee.isListeningOnly ||
					result.attendees.attendee.hasJoinedVoice ||
					result.attendees.attendee.hasVideo
				);
			if (checkAttendee) {
				let joinUrl = api.administration.join(name, id, pwd, { userID: userId, "userdata-bbb_skip_check_audio": true });
				res.json({ error: false, result: joinUrl, joined: true });
			}
			else {
				let joinUrl = api.administration.join(name, id, pwd, { userID: userId, "userdata-bbb_skip_check_audio": true });
				res.json({ error: false, result: joinUrl, joined: false });
			}
		}
	});
}

exports.isMeetingRunning = (req, res) => {
	let id = req.body.id;
	let api = bbb.api(env.BBB_URL, env.BBB_KEY);
	let http = bbb.http;

	let meetingInfo = api.monitoring.getMeetingInfo(id);
	http(meetingInfo).then((result) => {
		res.json({ error: false, result: result });
	});
}

exports.getRecordByProject = async (req, res) => {
	let level = '';
	let dataRecord = [];
	let tmp_dataRecord = [];
	let getUser = `SELECT u.level FROM user u
	WHERE u.user_id = '${req.params.user_id}' AND u.status='active'`;
	db.query(getUser, async (error, result, fields) => {
		if (error) {
			res.json({ error: true, result: error })
		}
		else {
			if (result.length > 0) {
				level = result[0].level;
				let queryUser = `SELECT
					l.*,
					u.name,
					GROUP_CONCAT(p.user_id) AS participant,
					/* CHECKING ROLES */
					IF(ffu.role IS NULL,
						(SELECT GROUP_CONCAT(b.id) FROM liveclass_booking b WHERE b.meeting_id = l.class_id AND b.date_start <= NOW() AND ( b.moderator = '${req.params.user_id}' OR b.user_id = '${req.params.user_id}' ))
						,
						(SELECT GROUP_CONCAT(b.id) FROM liveclass_booking b WHERE b.meeting_id = l.class_id AND b.date_start <= now())
					) as booking,
					COUNT(lp.user_id) AS total_participant,
					LEFT(l.schedule_start, 10) AS tanggal,
					MID(l.schedule_start, 12, 8) AS waktu_start,
				  MID(l.schedule_end,12, 8) AS waktu_end
				  FROM
					liveclass l
				  LEFT JOIN user u ON (l.moderator IS NOT NULL AND l.moderator = u.user_id)
				  LEFT JOIN liveclass_participant p ON l.class_id = p.class_id
				  LEFT JOIN liveclass_participant lp ON lp.class_id = l.class_id
				  LEFT JOIN files_folder_user ffu ON ffu.folder_id = l.folder_id and ffu.user_id = '${req.params.user_id}'
				  WHERE l.folder_id='${req.params.id}' AND 
				  (
					(l.is_private='1' AND p.user_id='${req.params.id}') 
					OR l.is_private='0' 
				  )
				  GROUP BY l.class_id
				  ORDER BY l.schedule_start DESC`;

				let queryAdmin = `SELECT
					l.*,
					u.name,
					GROUP_CONCAT(p.user_id) AS participant,
					-- GROUP_CONCAT(b.id) AS booking,
					(
						SELECT GROUP_CONCAT(b.id) FROM liveclass_booking b where b.meeting_id = l.class_id
					) as booking,
					COUNT(p.user_id) AS total_participant,
					LEFT(l.schedule_start, 10) AS tanggal,
					MID(l.schedule_start, 12, 8) AS waktu_start,
				  MID(l.schedule_end,12, 8) AS waktu_end
				  FROM
					liveclass l
				  LEFT JOIN user u ON (l.moderator IS NOT NULL AND l.moderator = u.user_id)
				  LEFT JOIN liveclass_participant p ON l.class_id = p.class_id
				  -- LEFT JOIN liveclass_booking b ON b.meeting_id = l.class_id AND b.date_start <= now()
				  WHERE l.folder_id='${req.params.id}'
				  GROUP BY l.class_id
				  ORDER BY l.schedule_start DESC`;


				let queryWebinar = `SELECT * 
				FROM webinars 
				WHERE project_id = '${req.params.id}' AND publish='1'
				ORDER BY id DESC`;

				let dataMeeting = await db.query(level == 'client' ? queryUser : queryAdmin);
				let dataWebinar = await db.query(queryWebinar);
				let checking_liveclass = { meeting: [], webinar: [] };
				// console.log(dataMeeting, 'simpen ga nih?')

				let api = bbb.api(env.BBB_URL, env.BBB_KEY);
				let http = bbb.http

				if (dataMeeting.length > 0) {
					dataMeeting.map((item) => {
						//tmp_dataRecord.push(item.class_id.toString())
						if (item.booking) {
							let split = item.booking.split(",");
							split = [...new Set(split)];
							checking_liveclass.meeting.push({ id: item.class_id, subclass: item.booking, type: 'meeting' });
							tmp_dataRecord = tmp_dataRecord.concat(split);
						} 
						// else {
						// 	checking_liveclass.meeting.push({ id: item.class_id, subclass: item.booking || item.class_id.toString(), type: 'meeting' });
						// 	tmp_dataRecord.push(item.class_id);
						// }
					})
				}
				if (dataWebinar.length > 0) {
					dataWebinar.map((item) => {
						if (item.id != null) {
							checking_liveclass.webinar.push({ subclass: item.id.toString(), type: 'webinar' });
							tmp_dataRecord.push(item.id.toString());
						}
					})
				}

				// console.log(tmp_dataRecord.toString())
				if (tmp_dataRecord.length > 0) {

					tmp_dataRecord = [...new Set(tmp_dataRecord)];

					await Promise.all(env.BBB_SERVER_LIST.map(async (x) => {
						let api = bbb.api(x.server, x.key);
						let http = bbb.http
						let getRecordingsUrl = api.recording.getRecordings({ meetingID: tmp_dataRecord.toString(), state: 'published,processing,processed' })
						// console.log(getRecordingsUrl, 'recordings url')
						await http(getRecordingsUrl).then((result) => {
							if (result.returncode === 'SUCCESS' && result.messageKey !== 'noRecordings') {
								dataRecord.push(result.recordings)
							}
						})
					}))
				}
				let dataRecordings = [];
				let sortedData = [];
				let recordName = await db.query(`SELECT * FROM record WHERE project_id = '${req.params.id}'`)

				let cek_redudant = [];
				if (dataRecord.length > 0) {
					for (let i = 0; i < dataRecord.length; i++) {
						if (dataRecord[i].recording.length > 0) {
							for (let j = 0; j < dataRecord[i].recording.length; j++) {
								// console.log(dataRecord[i].recording[j])
								let idx = cek_redudant.findIndex((str) => { return str.meetingID === dataRecord[i].recording[j].meetingID });
								if (idx < 0) {
									cek_redudant.push(dataRecord[i].recording[j]);
								}
							}
						} else {
							let idx = cek_redudant.findIndex((str) => { return str.meetingID === dataRecord[i].meetingID });
							if (idx < 0) {
								cek_redudant.push(dataRecord[i].recording || dataRecord[i]);
							}
						}
					}
				}

				dataRecord = cek_redudant;
				if (recordName.length > 0 || dataRecord.length > 0) {

					dataRecord.map((item) => {
						let class_id = null;
						let from_type = null;
						let subclass = null;

						if (checking_liveclass.meeting.length > 0) {

							for (let i = 0; i < checking_liveclass.meeting.length; i++) {
								// console.info(checking_liveclass.meeting[i]);
								if (checking_liveclass.meeting[i].subclass.search(item.meetingID) > -1) {
									class_id = checking_liveclass.meeting[i].id;
									from_type = 'meeting';
									subclass = item.meetingID;
								}
							}
						}

						if (checking_liveclass.webinar.length > 0) {

							for (let i = 0; i < checking_liveclass.webinar.length; i++) {
								if (checking_liveclass.webinar[i].subclass.search(item.meetingID) > -1) {
									//class_id = checking_liveclass.meeting[i].id;
									from_type = 'webinar';
									subclass = item.meetingID;
								}
							}
						}

						let playbacks = null;

						try {
							playbacks = item.playback.format[0].url;
						} catch (e) {
							try {
								playbacks = item.playback.format.url;
							} catch (e) {
								playbacks = item.recording.playback.format.url
							}
						}
						let checkArr = recordName.filter(rec => rec.record_id === item.recordID);
						if(checkArr.length){
							item.name = checkArr[0].name
						}
						dataRecordings.push({

							id: item.recordID,
							class_id: class_id,
							from_type: from_type,
							subclass: subclass,
							name: item.name ? item.name : 'None',
							//name: recordName.filter(rec => rec.record_id === item.recordID).length ? recordName.filter(rec => rec.record_id === item.recordID)[0].name : item.name,
							parent: false,
							content: null,
							type: 'files',
							ext: 'recording',
							icon: 'icon-files',
							creator: null,
							active: false,
							new: false,
							size: 0,
							state: item.state,
							link: playbacks,//Array.isArray(item.playback.format) ? item.playback.format[0].url : item.playback.format.url,
							totalFiles: 0,
							parentName: null,
							created_by: null,
							access: item.access || 'editor',
							date: new Date(item.startTime),
							created_at: new Date(item.startTime),
							updated_at: new Date(item.startTime),
						})
					})
					sortedData = dataRecordings.sort((a, b) => b.date - a.date)
				}
				//return resolve(sortedData)
				res.json({ error: false, result: sortedData })
			}
			else {
				res.json({ error: true, result: 'User not found' })
			}
		}
	})
}