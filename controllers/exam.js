var env = require('../env.json');
var conf = require('../configs/config');
var db = require('../configs/database');

var multer = require('multer');
const storage = multer.diskStorage({
  filename: function (req, file, cb) {
    cb(null, file.originalname);
  }
})
const AWS = require('aws-sdk');
const fs = require('fs');
const s3 = new AWS.S3({
  accessKeyId: env.AWS_ACCESS,
  secretAccessKey: env.AWS_SECRET
});

var uploadLogo = multer({ storage: storage }).single('file');

exports.createExam = (req, res, next) => {
	let form = {
		company_id: req.body.company_id,
		course_id: req.body.course_id,
		user_id: req.body.user_id,

		random: req.body.random,
	 	exam_title: req.body.exam_title,
	 	exam_description: req.body.exam_description,
	 	exam_publish: req.body.exam_publish,
	 	time_minute: req.body.time_minute,
	 	time_start: req.body.time_start,
	 	time_finish: req.body.time_finish
	};

	db.query(`INSERT INTO exam
		(exam_id, company_id, course_id, user_id, quiz, random, exam_title, exam_description, exam_publish, time_minute, time_start, time_finish, created_at) VALUES
		(null, '${req.body.company_id}', '${req.body.course_id}', '${req.body.user_id}', '0', '${req.body.random}', '${req.body.exam_title}', '${req.body.exam_description}', '${req.body.exam_publish}', '${req.body.time_minute}', '${req.body.time_start}', '${req.body.time_finish}', '${conf.dateTimeNow()}')`,
		(error, result, fields) => {
		if(error) {
			res.json({ error: true, result: error });
		} else {
			db.query(`SELECT * FROM exam WHERE exam_id = '${result.insertId}'`, (error, result, fields) => {
				res.json({ error: false, result: result[0] });
			})
		}
	});
};

exports.getListExam = (req, res, next) => {
	db.query(`SELECT * FROM exam WHERE quiz = '0'`, (error, result, fields) => {
		if(error) {
			res.json({ error: true, result: error });
		} else {
			res.json({ error: false, result: result });
		}
	});
};

exports.isQuizOrExam = (req, res, next) => {
	db.query(`SELECT quiz FROM exam WHERE exam_id = '${req.params.exam_id}'`, (error, result, fields) => {
		if(error) {
			res.json({ error: true, result: error });
		} else {
			res.json({ error: false, result: result.length !== 0 ? result[0] : [] });
		}
	})
}

exports.getOneExam = (req, res, next) => {
	db.query(`SELECT * FROM exam WHERE quiz = '0' AND exam_id = '${req.params.exam_id}'`, (error, result, fields) => {
		if(error) {
			res.json({ error: true, result: error });
		} else {
			res.json({ error: false, result: result.length !== 0 ? result[0] : [] });
		}
	})
}

exports.getOneExamById = (req, res) => {
  db.query(`
    SELECT
    	e.exam_title, e.jenis, e.quiz, e.tipe_jawab, ce.*, cc.jadwal_id, cc.course_id, cc.silabus_id, lps.*
    FROM
    	chapter_exam ce
        JOIN course_chapter cc ON cc.chapter_id = ce.chapter_id
        JOIN learning_pelajaran_silabus lps ON lps.id = cc.silabus_id
        JOIN exam e ON e.exam_id = ce.exam_id
    WHERE
    	ce.exam_id = ?`, [req.params.examId], (error, result, fields) => {
		if(error) {
			res.json({ error: true, result: error });
		} else {
			res.json({ error: false, result: result.length !== 0 ? result[0] : [] });
		}
	})
}

exports.getExamByCourse = (req, res, next) => {
	db.query(`SELECT * FROM exam WHERE quiz = '0' AND company_id = '${req.params.company_id}' AND course_id = '${req.params.course_id}'`, async (error, result, fields) => {
		if(error) {
			res.json({ error: true, result: error });
		} else {
			let tempArray = result;
			for(let i=0; i<tempArray.length; i++) {
				let numRows = await db.query(`SELECT * FROM exam_question WHERE exam_id = '${tempArray[i].exam_id}'`);
				tempArray[i].soal = numRows.length;
			}

			res.json({ error: false, result: tempArray });
		}
	})
};

exports.getExamPublishByCourse = (req, res, next) => {
	db.query(`SELECT * FROM exam WHERE quiz = '0' AND company_id = '${req.params.company_id}' AND course_id = '${req.params.course_id}' AND exam_publish = '1'`, async (error, result, fields) => {
		if(error) {
			res.json({ error: true, result: error });
		} else {
			let tempArray = result;
			for(let i=0; i<tempArray.length; i++) {
				let numRows = await db.query(`SELECT * FROM exam_question WHERE exam_id = '${tempArray[i].exam_id}'`);
				tempArray[i].soal = numRows.length;
			}

			res.json({ error: false, result: tempArray });
		}
	})
};

exports.updatePublishExam = (req, res, next) => {
 let form = {
		course_id: req.body.course_id,
		company_id: req.body.company_id
	};

	db.query(`UPDATE exam SET exam_publish = '0' WHERE course_id = '${form.course_id}' AND company_id = '${form.company_id}'`, (error, result, fields) => {
		if(error) {
			res.json({error: true, result: error})
		} else {
			db.query(`UPDATE exam SET exam_publish = '1' WHERE exam_id = '${req.params.exam_id}'`, (error, result, fields) => {
				if(error) {
					res.json({error: true, result: error})
				} else {
					db.query(`SELECT * FROM exam WHERE exam_id = ''`, (error, result, fields) => {
						res.json({error: false, result: result[0]})
					})
				}
			})
		}
	})
};


exports.updateExam = (req, res, next) => {
 let form = {
 	random: req.body.random,
 	exam_title: req.body.exam_title,
 	exam_description: req.body.exam_description,
 	exam_publish: req.body.exam_publish,
 	time_minute: req.body.time_minute,
 	time_start: req.body.time_start,
 	time_finish: req.body.time_finish
 };

 db.query(`UPDATE exam SET
 	random = '${form.random}',
 	exam_title = '${form.exam_title}',
 	exam_description = '${form.exam_description}',
 	exam_publish = '${form.exam_publish}',
 	time_minute = '${form.time_minute}',
 	time_start = '${form.time_start}',
 	time_finish = '${form.time_finish}'
 	WHERE exam_id = '${req.params.exam_id}'`, (error, result, fields) => {
 	if(error) {
 		res.json({ error: true, result: error });
 	} else {
 		db.query(`SELECT * FROM exam WHERE exam_id = '${req.params.exam_id}'`, (error, result, fields) => {
 			res.json({ error: false, result: result[0] });
 		});
 	}
 });
};

exports.deleteExam = (req, res, next) => {
	db.query(`DELETE FROM exam WHERE exam_id = '${req.params.exam_id}'`, (error, result, fields) => {
		if(error) {
			res.json({ error: true, result: error });
		} else {
			res.json({ error: false, result: result });
		}
	});
};

exports.getJadwalMuridByUserId = async (req, res) => {
  if(req.query.mapelOnly) {
    let sql = `
    SELECT lkm.kelas_id, lk.kelas_nama, lk.semester_id, s.semester_name, lk.tahun_ajaran, lm.id, lm.user_id, lm.nama, lm.no_induk
    FROM learning_kelas_murid lkm
    JOIN learning_murid lm ON lm.id = lkm.murid_id
    JOIN learning_kelas lk ON lk.kelas_id = lkm.kelas_id AND lk.tahun_ajaran = ?
    JOIN semester s ON s.semester_id = lk.semester_id
    WHERE lm.user_id = ?`;

    let tahunAjaran = '';
    if(req.query.tahunAjaran) {
      tahunAjaran = req.query.tahunAjaran;
    }
    else {
      let d = new Date();
      // bulan diawali dengan 0 = januari, 11 = desember
      let month = d.getMonth();
      tahunAjaran = month < 6 ? (d.getFullYear()-1)+'/'+d.getFullYear() : d.getFullYear()+'/'+(d.getFullYear()+1);
    }

    let getMurid = await db.query(sql, [tahunAjaran, req.params.userId]);

    let sqlJadwal = `
    SELECT
      lj.jadwal_id, lj.ruangan_id, rm.nama_ruangan, lg.id AS guru_id, lg.nama AS nama_guru,
      lj.pelajaran_id, lp.nama_pelajaran AS mapel, lj.kelas_id, lk.kelas_nama, lj.hari,
      lj.jam_mulai, lj.jam_selesai, lj.jumlah_pertemuan AS sesi
    FROM learning_jadwal lj
      JOIN learning_pelajaran lp ON lp.pelajaran_id = lj.pelajaran_id
      JOIN learning_kelas lk ON lk.kelas_id = lj.kelas_id
      JOIN ruangan_mengajar rm ON rm.id = lj.ruangan_id
      JOIN learning_guru lg ON lg.id = rm.pengajar
    WHERE
      lj.kelas_id = ?
    ORDER BY
      lj.jam_mulai ASC`;

    let jadwal = [];
    if(req.query.kelasId) {
      jadwal = await db.query(sqlJadwal, [req.query.kelasId]);
    }
    else {
      jadwal = await db.query(sqlJadwal, [getMurid.length ? getMurid[0].kelas_id : 0]);
    }

    res.json({ error: false, result: jadwal, semester: getMurid })
  }
  else {

    let hari = ["Senin","Selasa","Rabu","Kamis","Jumat","Sabtu","Minggu"];

    let sql = `
    SELECT lkm.kelas_id, lk.kelas_nama, lk.semester_id, s.semester_name, lk.tahun_ajaran, lm.id, lm.user_id, lm.nama, lm.no_induk
    FROM learning_kelas_murid lkm
    JOIN learning_murid lm ON lm.id = lkm.murid_id
    JOIN learning_kelas lk ON lk.kelas_id = lkm.kelas_id AND lk.tahun_ajaran = ?
    JOIN semester s ON s.semester_id = lk.semester_id
    WHERE lm.user_id = ?`;

    let tahunAjaran = '';
    if(req.query.tahunAjaran) {
      tahunAjaran = req.query.tahunAjaran;
    }
    else {
      let d = new Date();
      // bulan diawali dengan 0 = januari, 11 = desember
      let month = d.getMonth();
      tahunAjaran = month < 6 ? (d.getFullYear()-1)+'/'+d.getFullYear() : d.getFullYear()+'/'+(d.getFullYear()+1);
    }

    let getMurid = await db.query(sql, [tahunAjaran, req.params.userId]);

    // console.log(hari)
    // console.log(getMurid)

    // let tglMingguIni = getThisWeek();

    let response = [];
    for(var i=0; i<hari.length; i++) {
      let sqlJadwal = `
      SELECT lj.jadwal_id, lj.ruangan_id, rm.nama_ruangan, lg.id AS guru_id, lg.nama AS nama_guru, lj.pelajaran_id, lp.nama_pelajaran AS mapel, lj.kelas_id, lk.kelas_nama, lj.hari, lj.jam_mulai, lj.jam_selesai, lj.jumlah_pertemuan AS sesi
      FROM learning_jadwal lj
      JOIN learning_pelajaran lp ON lp.pelajaran_id = lj.pelajaran_id
      JOIN learning_kelas lk ON lk.kelas_id = lj.kelas_id
      JOIN ruangan_mengajar rm ON rm.id = lj.ruangan_id
      JOIN learning_guru lg ON lg.id = rm.pengajar
      WHERE lj.kelas_id = ? AND lj.hari = ?
      ORDER BY lj.jam_mulai ASC`;

      let jadwal = []
      if(req.query.kelasId) {
        jadwal = await db.query(sqlJadwal, [req.query.kelasId, hari[i]]);
      }
      else {
        jadwal = await db.query(sqlJadwal, [getMurid.length ? getMurid[0].kelas_id : 0, hari[i]]);
      }

      response.push({tanggal: hari[i], hari: hari[i], tgl: new Date(), data: jadwal})
    }

    res.json({ error: false, result: response, semester: getMurid })
  }
}

exports.getTugasMuridByUserId = (req, res) => {
	// INFO MURID
  let sql = `
    SELECT lkm.kelas_id, lk.kelas_nama, lk.semester_id, s.semester_name, lk.tahun_ajaran, lm.id, lm.user_id, lm.nama, lm.no_induk
    FROM learning_kelas_murid lkm
      JOIN learning_murid lm ON lm.id = lkm.murid_id
      JOIN learning_kelas lk ON lk.kelas_id = lkm.kelas_id AND lk.tahun_ajaran = ?
      JOIN semester s ON s.semester_id = lk.semester_id
    WHERE lm.user_id = ?
  `;

  let tahunAjaran = '';
  if(req.query.tahunAjaran) {
    tahunAjaran = req.query.tahunAjaran;
  }
  else {
    let d = new Date();
    // bulan diawali dengan 0 = januari, 11 = desember
    let month = d.getMonth();
    tahunAjaran = month < 6 ? (d.getFullYear()-1)+'/'+d.getFullYear() : d.getFullYear()+'/'+(d.getFullYear()+1);
  }

  db.query(sql, [tahunAjaran, req.params.userId], async (err, rows) => {
    if(err) res.json({ error: true, result: err })

    let sqlTugas = `
      SELECT lj.jadwal_id, lj.hari, lj.pelajaran_id, lp.nama_pelajaran, lps.jenis, lps.topik, cc.chapter_id, cc.jenis_pembelajaran, cc.silabus_id, ce.exam_id, e.exam_title AS title, e.jenis, e.quiz, e.tipe_jawab, e.time_start, e.time_finish
      FROM learning_jadwal lj
      JOIN learning_pelajaran lp ON lp.pelajaran_id = lj.pelajaran_id
        JOIN learning_pelajaran_silabus lps ON lps.pelajaran_id = lp.pelajaran_id
        LEFT JOIN course_chapter cc ON cc.silabus_id = lps.id
        JOIN chapter_exam ce ON ce.chapter_id = cc.chapter_id AND cc.jadwal_id = lj.jadwal_id
        JOIN exam e ON e.exam_id = ce.exam_id
      WHERE lj.kelas_id = ?
    `;

    let tugas = []
    if(req.query.kelasId) {
      tugas = await db.query(sqlTugas, [req.query.kelasId]);
    }
    else {
      tugas = await db.query(sqlTugas, [rows[0].kelas_id]);
    }

    if(tugas.length > 0) {
      for(var i=0; i<tugas.length; i++) {
        let sql = `SELECT * FROM exam_tugas_answer WHERE exam_id = ? AND user_id = ?`;
        let submitted = await db.query(sql, [tugas[i].exam_id, req.params.userId]);
        tugas[i].submitted = submitted;
      }
    }
    rows[0].tugas = tugas;

    res.json({ error: false, result: rows[0], semester: rows })
  })
}

exports.getKuisMuridByUserId = (req, res) => {
  // INFO MURID
  let sql = `
    SELECT lkm.kelas_id, lk.kelas_nama, lk.semester_id, s.semester_name, lk.tahun_ajaran, lm.id, lm.user_id, lm.nama, lm.no_induk
    FROM learning_kelas_murid lkm
      JOIN learning_murid lm ON lm.id = lkm.murid_id
      JOIN learning_kelas lk ON lk.kelas_id = lkm.kelas_id  AND lk.tahun_ajaran = ?
      JOIN semester s ON s.semester_id = lk.semester_id
    WHERE lm.user_id = ?
  `;

  let tahunAjaran = '';
  if(req.query.tahunAjaran) {
    tahunAjaran = req.query.tahunAjaran;
  }
  else {
    let d = new Date();
    // bulan diawali dengan 0 = januari, 11 = desember
    let month = d.getMonth();
    tahunAjaran = month < 6 ? (d.getFullYear()-1)+'/'+d.getFullYear() : d.getFullYear()+'/'+(d.getFullYear()+1);
  }

  db.query(sql, [tahunAjaran, req.params.userId], async (err, rows) => {
    if(err) res.json({ error: true, result: err })

    let sqlTugas = `
    SELECT lj.jadwal_id, lj.pelajaran_id, lp.nama_pelajaran,lj.ruangan_id,
      cc.chapter_id, cc.chapter_title, cc.start_date, lj.kelas_id, ce.exam_id, e.exam_title, e.quiz, e.jenis,
      e.time_start, e.time_finish
    FROM learning_jadwal lj
          JOIN learning_pelajaran lp ON lp.pelajaran_id = lj.pelajaran_id
        JOIN learning_pelajaran_silabus lps ON lps.pelajaran_id = lp.pelajaran_id
        LEFT JOIN course_chapter cc ON cc.silabus_id = lps.id
        JOIN chapter_exam ce ON ce.chapter_id = cc.chapter_id AND cc.jadwal_id = lj.jadwal_id
        JOIN exam e ON e.exam_id = ce.exam_id AND e.jenis = 1 AND e.quiz = 1
    WHERE lj.kelas_id = ?
    ORDER BY cc.start_date ASC
    `;

    let tugas = []
    if(req.query.kelasId) {
      tugas = await db.query(sqlTugas, [req.query.kelasId]);
    }
    else {
      tugas = await db.query(sqlTugas, [rows[0].kelas_id]);
    }

    for(var i=0; i<tugas.length; i++) {
      let soal = await db.query(`SELECT exam_id FROM exam_question WHERE exam_id = ?`, [tugas[i].exam_id]);
      tugas[i].soal = soal;

      let submitted = await db.query(`SELECT * FROM exam_result_learning WHERE exam_id = ? AND user_id = ?`, [tugas[i].exam_id, req.params.userId]);
      tugas[i].submitted = submitted;
    }
    rows[0].kuis = tugas;

    res.json({ error: false, result: rows[0], semester: rows })
  })
}

exports.getUjianMuridByUserId = (req, res) => {
  // INFO MURID
  let sql = `
    SELECT lkm.kelas_id, lk.kelas_nama, lk.semester_id, s.semester_name, lk.tahun_ajaran, lm.id, lm.user_id, lm.nama, lm.no_induk
    FROM learning_kelas_murid lkm
      JOIN learning_murid lm ON lm.id = lkm.murid_id
      JOIN learning_kelas lk ON lk.kelas_id = lkm.kelas_id AND lk.tahun_ajaran = ?
      JOIN semester s ON s.semester_id = lk.semester_id
    WHERE lm.user_id = ?
  `;

  let tahunAjaran = '';
  if(req.query.tahunAjaran) {
    tahunAjaran = req.query.tahunAjaran;
  }
  else {
    let d = new Date();
    // bulan diawali dengan 0 = januari, 11 = desember
    let month = d.getMonth();
    tahunAjaran = month < 6 ? (d.getFullYear()-1)+'/'+d.getFullYear() : d.getFullYear()+'/'+(d.getFullYear()+1);
  }


  db.query(sql, [tahunAjaran, req.params.userId], async (err, rows) => {
    if(err) res.json({ error: true, result: err })

    let sqlTugas = `
    SELECT lj.jadwal_id, lj.pelajaran_id, lp.nama_pelajaran,lj.ruangan_id,
      cc.chapter_id, cc.chapter_title, cc.start_date, lj.kelas_id, ce.exam_id, e.exam_title, e.quiz, e.jenis,
      e.time_start, e.time_finish
    FROM learning_jadwal lj
          JOIN learning_pelajaran lp ON lp.pelajaran_id = lj.pelajaran_id
        JOIN learning_pelajaran_silabus lps ON lps.pelajaran_id = lp.pelajaran_id
        LEFT JOIN course_chapter cc ON cc.silabus_id = lps.id
        JOIN chapter_exam ce ON ce.chapter_id = cc.chapter_id  AND cc.jadwal_id = lj.jadwal_id
        JOIN exam e ON e.exam_id = ce.exam_id AND e.jenis = 1 AND e.quiz = 0
    WHERE lj.kelas_id = ?
    ORDER BY cc.start_date ASC
    `;

    let tugas = []
    if(req.query.kelasId) {
      tugas = await db.query(sqlTugas, [req.query.kelasId]);
    }
    else {
      tugas = await db.query(sqlTugas, [rows[0].kelas_id]);
    }

    for(var i=0; i<tugas.length; i++) {
      let soal = await db.query(`SELECT exam_id FROM exam_question WHERE exam_id = ?`, [tugas[i].exam_id]);
      tugas[i].soal = soal;

      let submitted = await db.query(`SELECT * FROM exam_result_learning WHERE exam_id = ? AND user_id = ?`, [tugas[i].exam_id, req.params.userId]);
      tugas[i].submitted = submitted;
    }
    rows[0].kuis = tugas;

    res.json({ error: false, result: rows[0], semester: rows })
  })
}

exports.updateStarted = (req, res) => {
  let { event, id } = req.params;
  let isStarted = event === 'start' ? 1 : 0;
  let sql = `UPDATE exam SET is_started = ? WHERE exam_id = ?`;
  db.query(sql, [isStarted, id], (err, rows) => {
    if(err) res.json({ error: true, result: err });

    res.json({ error: false, result: rows })
  })
}

exports.postTugasMuridLangsung = (req, res) => {
  let sql = `INSERT INTO exam_tugas_answer (exam_id, user_id, answer_file) VALUES (?)`;
  let form = [req.body.examId, req.body.userId, req.body.jawaban];
  db.query(sql, [form], (error, result, fields) => {
    if(error) {
      res.json({error: true, result: error});
    } else {
      db.query(`SELECT * FROM exam_tugas_answer WHERE answer_id = '${result.insertId}'`, (error, result, fields) => {
        res.json({error: false, result: result[0]});
      })
    }
  });
}

exports.postTugasMurid = (req, res) => {
	uploadLogo(req, res, (err) => {
    if(!req.file) {
      res.json({ error: true, result: err });
    } else {
      let path = req.file.path;
      let newDate = new Date();
      let keyName = `${env.AWS_BUCKET_ENV}/tugas`;

      var params = {
        ACL: 'public-read',
        Bucket: env.AWS_BUCKET,
        Body: fs.createReadStream(path),
        Key: `${keyName}/${req.file.originalname}`,
        Metadata: {
          'Content-Type': req.file.mimetype,
          'Content-Disposition': 'inline'
        }
      };

      s3.upload(params, (err, data) => {
        if (err) {
          res.json({ error: true, msg: "Error Upload Image", result: err });
        }
        if (data) {
          fs.unlinkSync(path);

          let formData = [
						req.body.examId,
						req.body.userId,
            data.Location,
						req.body.deskripsi
          ];

          let sql = `INSERT INTO exam_tugas_answer (exam_id, user_id, answer_file, answer_deskripsi) VALUES (?)`;

          db.query(sql, [formData], (error, result, fields) => {
            if(error) {
              res.json({error: true, result: error});
            } else {
              db.query(`SELECT * FROM exam_tugas_answer WHERE answer_id = '${result.insertId}'`, (error, result, fields) => {
                res.json({error: false, result: result[0]});
              })
            }
          });

        }
      });
    }
  });
}

exports.createChapterExam = (req, res) => {
  let form = {
    chapterId: req.body.chapterId,
    examId: req.body.examId
  };

  let sql = `INSERT INTO chapter_exam (chapter_id, exam_id) VALUES (?)`;
  let data = [[form.chapterId, form.examId]];
  db.query(sql, data, (error, rows) => {
    if(error) {
      res.json({error: true, result: error});
    } else {
      res.json({ error: false, result: rows })
    }
  })
}

exports.deleteChapterExam = (req, res) => {
  let sql = `DELETE FROM chapter_exam WHERE id = ?`;
  let data = [req.params.id];
  db.query(sql, data, (error, rows) => {
    if(error) {
      res.json({error: true, result: error});
    } else {
      res.json({ error: false, result: rows })
    }
  })
}
