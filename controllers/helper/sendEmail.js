const mustache = require('mustache');
const moment = require('moment-timezone');
const fs = require('fs');
const db = require('../../configs/database');
const env = require('../../env.json');
const sendMailQueue = require("../../configs/bullqueue");
const gCalendar = require('../../configs/gApi');
const options_queue = {
  delay: 1000,
  attempts: 2,
  backoff: 20000
};

module.exports = {
  async sendEmailWebinar(payload) {

    const template = fs.readFileSync('./controllers/helper/email.html', 'utf8');

    let user = async () => {
      try {
        return await db.query(
          `SELECT t1.user_id, t2.email, t2.name , t1.id 
                    FROM webinar_peserta t1
                    INNER JOIN user t2
                    ON t1.user_id = t2.user_id
                    WHERE t1.webinar_id = '${payload.id}'
                    AND t1.id IN (${payload.pengguna.length ? payload.pengguna : 'NULL'});
                    
                    SELECT id, name, email, voucher , voucher as 'user_id' 
                    FROM webinar_tamu
                    WHERE webinar_id = '${payload.id}'
                    AND id IN (${payload.tamu.length ? payload.tamu : 'NULL'});`
        )
      } catch (error) {
        return res.json({ error: true, result: await error.message });
      }
    };
    let webinar = async () => {
      try {
        return await db.query(
          `SELECT * FROM webinars WHERE id='${payload.id}'`
        )
      } catch (error) {
        return error.message;
      }
    };

    let result = await user();
    let dataWebinar = await webinar();

    // TEST SIMULTAN SENDMAIL
    // let tmp = [];
    // let query_multi_insert = `INSERT INTO webinar_tamu(webinar_id,email, name, voucher, phone, status) VALUES`;
    // for (let i = 0; i < 500; i++) {
    //   if (result[1].length <= 500) {
    //     tmp.push(
    //       //user_id: result[1][0].user_id,
    //       `(${payload.id},
    //       '${result[1][0].email}',
    //       '${result[1][0].name} ${i}',
    //       '${result[1][0].voucher}${i}',
    //       '12345${i}',
    //       0)`,
    //     )
    //   }
    // }
    // query_multi_insert = `${query_multi_insert}${tmp.toString()}`;
    // let insert = db.query(query_multi_insert);
    // console.log(result[1].length, "TAMU");

    if (result[0].length > 0) {
      await result[0].map(async (elem, idx) => {
        let data = {
          ...elem,
          bodyEmail: `You are invited to a webinar : <b>${dataWebinar[0].judul}</b><br>
                    Date : ${payload.time}.<br><br>
                    Click the link or button below or log in to the Icademy to enter the webinar at the specified time.`,
          buttonValue: 'Join the Webinar Room',
          buttonUrl: `${env.APPS_URL}/redirect/webinar/live/${dataWebinar[0].id}`
        };

        let obj_mail = {
          from: env.MAIL_USER,
          to: elem.email,
          subject: 'Webinar Invitation : ' + dataWebinar[0].judul,
          text: 'Webinar Invitation : ' + dataWebinar[0].judul,
          html: mustache.render(template, { ...data }),
          payload_id: payload.id,
          user_id: elem.user_id,
          guest: false,
          voucher: false,
          socket_id: payload.socket_id || null,
          type: 'sendmail_participant_webinar',
        }

        // EVENT
        if (elem.email) {

          let meetId = data.buttonUrl;
          let eventId = dataWebinar[0].calendar_event || null;

          let dataEmail = [elem.email];
          let attendees = [];


          let start = moment(dataWebinar[0].start_time);
          let end = moment(dataWebinar[0].end_time);
          let offset = "+07:00";

          start = `${start.format("YYYY-MM-DDTHH:mm:ss")}${offset}`;
          end = `${end.format("YYYY-MM-DDTHH:mm:ss")}${offset}`;

          start = { dateTime: start, timeZone: offset };
          end = { dateTime: end, timeZone: offset };

          let summary = `Webinar Invitation : ${dataWebinar[0].judul}`;
          let description = `<p><b>Invitation Webinar Icademy</b><br/>Webinar : ${dataWebinar[0].judul}<br/>You can enter the webinar via the Icademy application or via the link below :<br/><a href="${data.buttonUrl}">Click here for join webinar</a></p>`;

          dataEmail.forEach(str => {
            if (str) {
              attendees.push({ email: str, self: true, responseStatus: "needsAction", resource: true });
            }
          });

          let resultEvent = await gCalendar.gCalendarEventInsert({
            typeRelated: 2, relatedId: payload.id, htmlLink: data.buttonUrl,
            summary, description, start, end, attendees, eventId
          });

          if (!eventId) {
            try {
              db.query(`update webinars set calendar_event='${resultEvent.data.id}' where id ='${payload.id}';`);
            } catch (e) { }
          }
        }


        sendMailQueue.add(obj_mail, { ...options_queue, delay: (options_queue.delay * idx) });
      });
    }

    // result[1] = tmp;

    try {
      await result[1].map((elem, idx) => {

        let data = {
          ...elem,
          bodyEmail: `You are invited to a webinar : <b>${dataWebinar[0].judul}</b><br>
                      Date : ${payload.time}.<br><br>
                      Click the link or button below or log in to the Icademy to enter the webinar at the specified time.`,
          buttonValue: 'Join the Webinar Room',
          buttonUrl: `${env.APPS_URL}/webinar-guest/${dataWebinar[0].id}/${elem.voucher}`
        };

        let obj_mail = {
          from: env.MAIL_USER,
          to: elem.email,
          subject: 'Webinar Invitation : ' + dataWebinar[0].judul,
          text: 'Webinar Invitation : ' + dataWebinar[0].judul,
          html: mustache.render(template, { ...data }),
          payload_id: payload.id,
          user_id: elem.voucher,
          guest: true,
          voucher: elem.voucher,
          socket_id: payload.socket_id || null,
          type: 'sendmail_participant_webinar'
        };

        sendMailQueue.add(obj_mail, { ...options_queue, delay: (options_queue.delay * idx) });
      });
    } catch (e) {
      console.log(e)
    }
    return null;
  },

  async sendEmailWebinarExternalRole(payload) {

    const template = fs.readFileSync('./controllers/helper/email.html', 'utf8');
    let { dataMail, dataTime,collectEmail,role, webinarId,judul,start_time,end_time,calendar_event, socket_id } = payload;

    await dataMail.map(async (elem, idx) => {

      let btn = `${env.APPS_URL}/redirect/webinar/live/${webinarId}`;
      let guest = false;
      let voucher = false;
      let external = false;
      if(['external','guest'].indexOf(elem.type) > -1){
        btn = `${env.APPS_URL}/webinar-guest/${webinarId}/${elem.user_id}`;
        guest = true;
        if(elem.type == 'external'){
          external=true;
          guest=false;
        }
        voucher = true;
      }
      let data = {
        ...elem,
        bodyEmail: `You are invited to a webinar : <b>${judul}</b><br>
                  Date : ${dataTime}.<br><br>
                  Role: ${role.toUpperCase()} <br><br>
                  Click the link or button below or log in to the Icademy to enter the webinar at the specified time.`,
        buttonValue: 'Join the Webinar Room',
        buttonUrl: btn
      };

      let obj_mail = {
        from: env.MAIL_USER,
        alias: 'no-replay@icademy.id',
        to: elem.email,
        typeRole:elem.type,
        subject: 'Webinar Invitation : ' + judul,
        text: 'Webinar Invitation : ' + judul,
        html: mustache.render(template, { ...data }),
        payload_id: webinarId,
        user_id: elem.user_id,
        guest,
        external,
        voucher,
        socket_id: socket_id || null,
        type: 'sendmail_role_external_webinar',
      }

      // EVENT
      if (elem.email) {

        let eventId = calendar_event || null;

        let dataEmail = [elem.email];
        let attendees = [];


        let start = moment(start_time);
        let end = moment(end_time);
        let offset = "+07:00";

        start = `${start.format("YYYY-MM-DDTHH:mm:ss")}${offset}`;
        end = `${end.format("YYYY-MM-DDTHH:mm:ss")}${offset}`;

        start = { dateTime: start, timeZone: offset };
        end = { dateTime: end, timeZone: offset };

        let summary = `Webinar Invitation : ${judul}`;
        let description = `<p><b>Invitation Webinar Icademy</b><br/>Webinar : ${judul}<br/>You can enter the webinar via the Icademy application or via the link below :<br/><a href="${data.buttonUrl}">Click here for join webinar</a></p>`;

        dataEmail.forEach(str => {
          if (str) {
            attendees.push({ email: str, self: true, responseStatus: "needsAction", resource: true });
          }
        });

        let resultEvent = await gCalendar.gCalendarEventInsert({
          typeRelated: 2, relatedId: webinarId, htmlLink: data.buttonUrl,
          summary, description, start, end, attendees, eventId
        });

        if (!eventId) {
          try {
            db.query(`update webinars set calendar_event='${resultEvent.data.id}' where id ='${payload.id}';`);
          } catch (e) { }
        }
      }


      sendMailQueue.add(obj_mail, { ...options_queue, delay: (options_queue.delay * idx) });
    });

    return true;
  },

  // INVITATION MEETING

  async sendEmailMeeting(payload) {
    const template = fs.readFileSync('./controllers/helper/email.html', 'utf8');
    // let transport = nodemailer.createTransport({
    //   service: 'gmail',
    //   auth: {
    //     user: env.MAIL_USER,
    //     pass: env.MAIL_PASS,
    //   }
    // });
    let confirmation = payload.confirmation ? payload.confirmation === "Hadir" ? "Present" : "Not Present" : null;
    let html = ``;
    let htmlEmail = ``;
    let buttonValue = '';
    let buttonUrl = '';
    let buttonValueEmail = '';
    let buttonUrlEmail = '';
    let meetId = payload.message.split("information/")[1];

    html = `
          ${confirmation
        ? '<p>You have confirmed will <b>' + confirmation + '</b> at this meeting.</p>'
        : '<p><b>' + payload.user + '</b> invites you to a meeting.</p>'
      }
      
          <p>Meeting Name : ${payload.room_name}</p>
          <p>Start Time : ${payload.schedule_start}</p>
          <p>End Time : ${payload.schedule_end}</p>
    
          ${confirmation
        ? '<p>You can enter the meeting via the Icademy application or via the link below :</p>'
        : '<p>Please confirm your attendance and join the meeting via the link below :</p>'
      }
        `;
    buttonUrl = payload.message;
    buttonValue = confirmation ? 'Join the Meeting' : 'Attendance Confirmation & Join the Meeting';

    htmlEmail = `
          <p><b>${payload.user}</b> invites you to a meeting.</p>
      
          <p>Meeting Name : ${payload.room_name}</p>
          <p>Start Time : ${payload.schedule_start}</p>
          <p>End Time : ${payload.schedule_end}</p>
    
          <p>You can enter the meeting via the Icademy application or via the link below :</p>`;
    buttonUrlEmail = payload.messageNonStaff;
    buttonValueEmail = 'Join the Meeting';
    // if (payload.is_scheduled == 1){
    //   html = `
    //     ${
    //       confirmation
    //       ? '<p>You have confirmed will <b>'+confirmation+'</b> at this meeting.</p>'
    //       : '<p><b>'+payload.user+'</b> invites you to a meeting.</p>'
    //     }

    //     <p>Meeting Name : ${payload.room_name}</p>
    //     <p>Start Time : ${payload.schedule_start}</p>
    //     <p>End Time : ${payload.schedule_end}</p>

    //     ${
    //       confirmation
    //       ? '<p>You can enter the meeting via the Icademy application or via the link below :</p>'
    //       : '<p>Please confirm your attendance and join the meeting via the link below :</p>'
    //     }
    //   `;
    //   buttonUrl = payload.message;
    //   buttonValue = confirmation ? 'Join the Meeting' : 'Attendance Confirmation & Join the Meeting';
    // }
    // else{
    //   html = `
    //   ${
    //     confirmation
    //     ? '<p>You have confirmed will <b>'+confirmation+'</b> at this meeting.</p>'
    //     : '<p><b>'+payload.user+'</b> invites you to a meeting.</p>'
    //   }

    //     <p>Subject : ${payload.room_name}</p>

    //     ${
    //       confirmation
    //       ? '<p>You can enter the meeting via the Icademy application or via the link below :</p>'
    //       : '<p>Please confirm your attendance and join the meeting via the link below :</p>'
    //     }
    //   `;
    //   buttonUrl = payload.message;
    //   buttonValue = confirmation ? 'Join the Meeting' : 'Attendance Confirmation & Join the Meeting';
    // }

    // if (payload.is_scheduled == 1){
    //   htmlEmail = `
    //     <p><b>${payload.user}</b> invites you to a meeting.</p>

    //     <p>Meeting Name : ${payload.room_name}</p>
    //     <p>Start Time : ${payload.schedule_start}</p>
    //     <p>End Time : ${payload.schedule_end}</p>

    //     <p>You can enter the meeting via the Icademy application or via the link below :</p>`;
    //   buttonUrlEmail = payload.messageNonStaff;
    //   buttonValueEmail = 'Join the Meeting';
    // }
    // else{
    //   htmlEmail = `
    //     <p><b>${payload.user}</b> invites you to a meeting.</p>

    //     <p>Subject : ${payload.room_name}</p>

    //     <p>You can enter the meeting via the Icademy application or via the link below :</p>`;
    //   buttonUrlEmail = payload.messageNonStaff;
    //   buttonValueEmail = 'Join the Meeting';
    // }

    let userEmail = [];
    let settingUserTime = [];
    if (payload.userInvite.length > 0) {
      for (let i = 0; i < payload.userInvite.length; i++) {
        if (payload.userInvite[i]) {
          let res = await db.query(`
          SELECT * FROM user WHERE user_id = '${payload.userInvite[i]}'; 
          SELECT lb.start_time from liveclass_booking_participant lb where lb.booking_id = '${meetId}' and lb.user_id = '${payload.userInvite[i]}';`);

          if (res[1].length) {
            if (res[1][0].start_time) {
              settingUserTime.push({ email: res[0][0].email, timeIn: moment(res[1][0].start_time).format("HH:mm") });
            }
            else{
              userEmail.push(res[0][0].email);
            }
          } else {
            if (res[0].length) {
              userEmail.push(res[0][0].email);
            }
          }
        }
      }
    }

    let data = {
      bodyEmail: html,
      buttonValue: buttonValue,
      buttonUrl: buttonUrl
    };


    // EVENT
    if (userEmail.length > 0) {

      meetId = Number(meetId);
      let eventId = null;
      const checkEvent = await db.query(`select lb.calendar_event from liveclass_booking lb where lb.id ='${meetId}';`);
      if (checkEvent.length > 0) {
        eventId = checkEvent[0].calendar_event;
      }

      let dataEmail = userEmail;
      let attendees = [];

      let split = payload.schedule_end.split(" ((");
      if (split[0] && split[1]) {
        let offset = split[1].substr(3, 6);
        let end = split[0].replace(" ", "T");
        let start = payload.schedule_start.split(" ((")[0];
        end = `${end}:00${offset}`
        start = start.replace(" ", "T");
        start = `${start}:00${offset}`;

        start = { dateTime: start, timeZone: offset };
        end = { dateTime: end, timeZone: offset };

        let summary = payload.room_name;
        let description = `<p><b>Invitation Meeting Icademy</b><br/>Meeting Name : ${payload.room_name}<br/>You can enter the meeting via the Icademy application or via the link below :<br/><a href="${buttonUrlEmail}">Click here for join meeting room</a></p>`;

        // GUEST NOT SEND EVENT
        // if (payload.email.length > 0) {
        //   dataEmail.concat(payload.email);
        // }

        dataEmail.forEach(str => {
          if (str) {
            attendees.push({ email: str, self: true, responseStatus: "needsAction", resource: true });
          }
        });

        let resultEvent = await gCalendar.gCalendarEventInsert({
          typeRelated: 1, relatedId: meetId, htmlLink: buttonUrlEmail,
          summary, description, start, end, attendees, eventId
        });

        if (!eventId) {
          try {
            db.query(`update liveclass_booking set calendar_event='${resultEvent.data.id}' where id ='${meetId}';`);
          } catch (e) { }
        }
      }
    }

    if (settingUserTime.length) {
      for (let i = 0; i < settingUserTime.length; i++) {
        let tmpHtml = data.bodyEmail;
        let getStart = payload.schedule_start.split(" ")[1];
        tmpHtml = tmpHtml.replace(getStart, settingUserTime[i].timeIn);

        const mailOptions = {
          type: 'sendmail_participant_meeting',
          from: env.MAIL_USER, // sender address
          to: settingUserTime[i].email, // list of receivers
          // to: 'alvinkurniakamal@gmail.com',
          subject: confirmation ? 'Attendance Confirmation Meeting Icademy : ' + payload.room_name : 'Icademy Meeting Invitation : ' + payload.room_name, // Subject line
          html: mustache.render(template, {
            bodyEmail: tmpHtml,
            buttonValue: buttonValue,
            buttonUrl: buttonUrl
          })
        };
        sendMailQueue.add(mailOptions, { ...options_queue, priority: 1, lifo: true });
      }

      // FILTER
      let tmp = [];
      userEmail.forEach((str) => {
        let idx = settingUserTime.findIndex((arg) => { return arg.email === str });
        if (idx < 0) {
          tmp.push(str);
        }
      });
      userEmail = tmp;
    }


    if (payload.userInvite.length > 0 && userEmail.length) {

      const mailOptions = {
        type: 'sendmail_participant_meeting',
        from: env.MAIL_USER, // sender address
        bcc: userEmail, // list of receivers
        // to: 'alvinkurniakamal@gmail.com',
        subject: confirmation ? 'Attendance Confirmation Meeting Icademy : ' + payload.room_name : 'Icademy Meeting Invitation : ' + payload.room_name, // Subject line
        html: mustache.render(template, { ...data })
      };
      sendMailQueue.add(mailOptions, { ...options_queue, priority: 1, lifo: true });
      // res.json({ error: false, result: 'Check your mailbox.' });
      // transport.sendMail(mailOptions, function (err, info) {
      //   if (err) {
      //     console.log(err)
      //     res.json({ error: true, result: err })
      //   } else {
      //     console.log(info);
      //     res.json({ error: false, result: 'Check your mailbox.' })
      //   }
      // });
    }

    if (payload.email.length > 0) {
      let data = {
        bodyEmail: htmlEmail,
        buttonValue: buttonValueEmail,
        buttonUrl: buttonUrlEmail
      };
      const mailOptions = {
        type: 'sendmail_participant_meeting',
        from: env.MAIL_USER, // sender address
        bcc: payload.email, // list of receivers
        // to: 'alvinkurniakamal@gmail.com',
        subject: confirmation ? 'Meeting Attendance Confirmation in Icademy : ' + payload.room_name : 'Icademy Meeting Invitation : ' + payload.room_name, // Subject line
        html: mustache.render(template, { ...data })
      };

      sendMailQueue.add(mailOptions, { ...options_queue, priority: 1, lifo: true });
      //res.json({ error: false, result: 'Check your mailbox.' });

      // transport.sendMail(mailOptions, function (err, info) {
      //   if (err) {
      //     console.log(err)
      //     res.json({ error: true, result: err })
      //   } else {
      //     console.log(info);
      //     res.json({ error: false, result: 'Check your mailbox.' })
      //   }
      // });
    }
  },

  async sendEmailPassword({ name, email, password, phone }) {
    const template = fs.readFileSync('./controllers/helper/user.html', 'utf8');

    const mailOptions = {
      type: 'sendmail_information_email_user',
      from: env.MAIL_USER,
      to: email,
      subject: 'ICADEMY Account Information',
      text: 'Information Account & Password',
      html: mustache.render(template, { name, email, password, phone })
    }
    sendMailQueue.add(mailOptions, { ...options_queue, priority: 1, lifo: true });
    //res.json({ error: false, result: 'Success' });
    // transporter.sendMail({
    //   from: env.MAIL_USER,
    //   to: email,
    //   subject: 'ICADEMY Account Information',
    //   text: 'Information Account & Password',
    //   html: mustache.render(template, { name, email, password, phone })
    // }, function (err, info) {
    //   if (err) {
    //     console.log(err)
    //   } else {
    //     console.log(info)
    //   }
    // })
  }
}
