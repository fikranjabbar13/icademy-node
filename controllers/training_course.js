var env = require('../env.json');
const db = require('../configs/database');
var multer = require('multer');
const AWS = require('aws-sdk');
const path = require("path");

const moduleDB = require('../repository');
const fs = require('fs');
const pdf = require("html-pdf");
var moment = require('moment');
const axios = require("axios");
const puppeteer = require('puppeteer');
const validatorRequest = require('../validator');
const reportHtml = require("../configs/reportHtml");
const s3 = new AWS.S3({
    accessKeyId: env.AWS_ACCESS,
    secretAccessKey: env.AWS_SECRET
});
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, './public/training/course');
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname);
    }
})
const storageMedia = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, './public/training/course/session');
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname);
    }
});


const storageSubmissionAnswer = multer.diskStorage({

    destination: (req, file, cb) => {
        let paths = './public/training/course/submission';
        if (!fs.existsSync(paths)) {
            fs.mkdirSync(paths);
        }
        cb(null, paths);
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname);
    }
});

let html = (
    nama,
    cert_title,
    tanggal,
    signature,
    sign,
    logo,
    cert_subtitle,
    cert_description,
    cert_topic,
    cert_background,
    cert_orientation,
    bodyNewPage
) => {
    let BG = `file://${path.resolve(
        __dirname,
        "../public/sertifikat/images/BG.png"
    )}`;
    let TTD2 = [];
    signature.map(x => {
        TTD2.push(`file://${path.resolve(__dirname, `../${x.path}`)}`);
    });
    let signData = "";
    sign.map((item, index) => {
        signData =
            signData +
            `<span style="display: table-cell;">
           <img style="height: 192px" src="${TTD2[index]}" /><br>
           <span style="font-size:25px">${item.cert_sign_name}</span><br>
           <span style="font-size:25px">${item.cert_sign_title}</span>
          </span>`;
    });
    let Icademy = `file://${path.resolve(
        __dirname,
        "../public/sertifikat/images/Icademy.png"
    )}`;
    let Star = `file://${path.resolve(
        __dirname,
        "../public/sertifikat/images/Star.png"
    )}`;
    // let Group1 = `file://${path.resolve(__dirname, '../public/sertifikat/images/Group1.png')}`;
    let Group1 =
        logo.substring(0, 4) === "http"
            ? logo
            : `file://${path.resolve(__dirname, `../${logo}`)}`;

    // BG = path.normalize(BG);
    return `<div style="width:${cert_orientation === "landscape" ? "1675px" : "1160px"
}; height:${cert_orientation === "landscape" ? "1160px" : "1675px"
}; padding:20px; text-align:center; border: ${cert_background ? "none" : "10px solid #787878"
}; font-size:25px; position: relative">
    <img src="${cert_background}" style="width: 100%; height: 100%; position: absolute; top: 0px; left: 0px; z-index: 0" />
<div style="width:${cert_orientation === "landscape" ? "1625px" : "1110px"
}; height:${cert_orientation === "landscape" ? "1110px" : "1475px"
}; padding:20px; ${cert_orientation === "portrait" && "padding-top:150px;"
} text-align:center; border: ${cert_background ? "none" : "5px solid #787878"
}; position: relative"><br><br>
       <img style="height: 100px" src="${Group1}" /><br><br>
       <span style="font-size:50px; font-weight:bold">${cert_title}</span>
       <br><br>
       <span style="font-size:35px">${cert_subtitle}</span>
       <br><br>
       <span style="font-size:83px"><b>${nama}</b></span><br/><br/><br>
       <span style="font-size:35px">${cert_description}</span> <br/><br/>
       <span style="font-size:45px"><b>${cert_topic}</b></span> <br/><br/><br>
       <span style="font-size:25px">${tanggal}</span><br><br>
       <div style="display:table;width:100%">
       ${signData}
       </div>
</div>
</div>
${bodyNewPage != "" ? 
`<div style="width:${cert_orientation === "landscape" ? "1675px" : "1160px"
}; height:${cert_orientation === "landscape" ? "1160px" : "1675px"
}; margin-top:40px;padding:20px; text-align:center; border: ${cert_background ? "none" : "10px solid #787878"
}; font-size:25px; position: relative">
<img src="${cert_background}" style="width: 100%; height: 100%; position: absolute; top: 0px; left: 0px; z-index: 0" />
<div style="width:${cert_orientation === "landscape" ? "1625px" : "1110px"
}; height:${cert_orientation === "landscape" ? "1110px" : "1475px"
}; padding:20px; ${cert_orientation === "portrait" && "padding-top:150px;"
} border: ${cert_background ? "none" : "5px solid #787878"
}; position: relative"><br><br>
  ${bodyNewPage}
</div>
</div>` : ""}
`;
};
let options = {
    width: "1755px",
    height: "1240px",
  };
  let optionsPortrait = {
    width: "1240px",
    height: "1755px",
  };

const sendMailQueue = require("../configs/bullqueue");
const { Console } = require('console');
const options_queue = {
    delay: 4000,
    attempts: 2,
    backoff: 20000,
};

let storagesCourse = multer.diskStorage({

    destination: (req, file, cb) => {
        if (file.fieldname === "signature") {
            let paths = './public/signature';
            if (!fs.existsSync(paths)) {
                fs.mkdirSync(paths);
            }
            cb(null, paths);
        } else if (file.fieldname === "cert_logo") {
            let paths = './public/cert_logo';
            if (!fs.existsSync(paths)) {
                fs.mkdirSync(paths);
            }
            cb(null, paths);
        } else {
            let paths = './public/others';
            if (!fs.existsSync(paths)) {
                fs.mkdirSync(paths);
            }
            cb(null, paths);
        }
    },
    filename: (req, file, cb) => {
        let filetype = "";
        if (file.mimetype === "image/gif") {
            filetype = "gif";
        }
        if (file.mimetype === "image/png") {
            filetype = "png";
        }
        if (file.mimetype === "image/jpeg") {
            filetype = "jpg";
        }
        cb(null, "img-" + Date.now() + '-' + file.originalname);
    },
});

let uploadSignature = multer({ storage: storagesCourse }).fields([
    { name: "signature", maxCount: 10 },
    { name: "cert_logo", maxCount: 1 },
    { name: "certificate_background", maxCount: 1 },
    { name: "image_left", maxCount: 1 },
    { name: "image_right", maxCount: 1 },
]);
let uploadImage = multer({ storage: storage }).single('image');
let uploadMedia = multer({ storage: storageMedia }).single('media');
let uploadSubmissionAnswer = multer({ storage: storageSubmissionAnswer, limits: { fileSize: 24000000 } }).array('media');

var storageEx = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, './public/training/course/excel');
    },
    filename: (req, file, cb) => {
        cb(null, 'excel-' + Date.now() + '-' + file.originalname);
    }
});
var uploadExcel = multer({ storage: storageEx }).single('file');

exports.checkAssignCourse = checkUserAssignedCourseId;

async function checkUserAssignedCourseId(result, infoUser) {
    return new Promise(async (resolve) => {
        let tmp = [];

        for (let index = 0; index < result.length; index++) {

            let courseId = result[index].id;
            let msg = { result: [] };
            if (Number(courseId) > 0) {

                let query = ` 
            SELECT
            tcc.id, 
            tcc.training_course_id,
            tcc.training_company_id,
            tc.name AS 'company',
            tc.image AS 'image_company',
            tcc.created_at AS 'assignment_date', 
            if(tcc.active > 0,'Active','Inactive') AS 'assignment_status'
            from training_course_company tcc
            left join training_company tc on tc.id = tcc.training_company_id
            where 
            tcc.training_course_id = ? AND tcc.active = 1;`;

                let checkUser = await db.query(query, [courseId]);
                msg = { error: false, result: checkUser };
            }

            if (msg.result.length) {

                let checkUser = await getUserAssignCourse(msg.result, 1);
                if (checkUser.length) {
                    for (let index2 = 0; index2 < checkUser.length; index2++) {
                        if (courseId === checkUser[index2].training_course_id) {
                            // check user assign to course 
                            let assigned = checkUser[index2].training_user.filter((str) => (str.id.toString() === infoUser.training_user_id.toString() && str.assigned == 1));
                            if (assigned.length) {
                                // jika ada masukin
                                let match = tmp.findIndex((item) => { return item.id === result[index].id });
                                if (match == -1) {
                                    tmp.push(result[index]);
                                }
                            }
                        }
                    }
                }
            }
        }

        resolve(tmp);
    })
}

exports.import = (req, res) => {
    uploadExcel(req, res, (err) => {
        if (!req.file) {
            res.json({ error: true, result: err });
        } else {
            // create class Excel
            var Excel = require('exceljs');
            var wb = new Excel.Workbook();
            var path = require('path');
            var filePath = path.resolve(__dirname, '../public/training/company/excel/' + req.file.filename);

            var form = {
                company_id: req.body.company_id,
                status: 'active',
                created_by: req.body.created_by,
                created_at: moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
            };

            wb.xlsx.readFile(filePath).then(async function () {
                var sh = wb.getWorksheet("Sheet1");

                var tempArray = [];
                for (let i = 4; i <= sh.rowCount; i++) {
                    if (sh.getRow(i).getCell(1).value !== null) {
                        // check company exists
                        let checkCompany = await db.query(`SELECT name FROM training_company WHERE name = '${sh.getRow(i).getCell(1).value}' AND company_id='${form.company_id}' AND status='active'`);

                        if (checkCompany.length != 1) {
                            let sqlString = `INSERT INTO training_company (company_id, name, address, telephone, fax, website, email, status, created_by, created_at)
                                VALUES ('${form.company_id}', '${sh.getRow(i).getCell(1).value}', '${sh.getRow(i).getCell(2).value}', '${sh.getRow(i).getCell(3).value}',
                                '${sh.getRow(i).getCell(4).value}', '${sh.getRow(i).getCell(5).value}', '${sh.getRow(i).getCell(6).value}',
                                '${form.status}', '${form.created_by}', '${form.created_at}')`;

                            let insertTrainingCompany = await db.query(sqlString);
                        }
                    }
                    else {
                        break;
                    }
                }

                res.json({ error: false, result: 'success' });
            });

        }
    })
}

exports.uploadImage = (req, res) => {
    uploadImage(req, res, (err) => {
        if (!req.file) {
            res.json({ error: true, result: err });
        } else {
            if (env.AWS_BUCKET) {
                let path = req.file.path;
                let newDate = new Date();
                let keyName = `${env.AWS_BUCKET_ENV}/training/course`;

                var params = {
                    ACL: 'public-read',
                    Bucket: env.AWS_BUCKET,
                    Body: fs.createReadStream(path),
                    Key: `${keyName}/${Date.now()}-${req.file.originalname}`,
                    ContentDisposition: 'inline',
                    ContentType: req.file.mimetype
                };

                s3.upload(params, (err, data) => {
                    if (err) {
                        res.json({ error: true, msg: "Error Upload Image", result: err });
                    }
                    if (data) {
                        fs.unlinkSync(path);

                        let form = {
                            image: data.Location
                        }

                        db.query(`UPDATE training_course SET image = '${form.image}' WHERE id = '${req.params.id}'`, (error, result, fields) => {
                            if (error) {
                                res.json({ error: true, result: error })
                            } else {
                                res.json({ error: false, result: result })
                            }
                        })

                    }
                })
            }
            else {
                let form = {
                    image: `${env.APP_URL}/training/course/${req.file.filename}`
                }

                db.query(`UPDATE training_course SET image = '${form.image}' WHERE id = '${req.params.id}'`, (error, result, fields) => {
                    if (error) {
                        res.json({ error: true, result: error })
                    } else {
                        res.json({ error: false, result: result })
                    }
                })
            }

        }
    })
}

function checkfileSize(file){
    let arrPDFType = ["application/pdf","application/x-pdf"]
    if(arrPDFType.includes(file.mimetype)){
        return { size:18000000,type:"pdf" };
    }else{
        if(file.mimetype.search('video') > -1){
            return { size:500000000,type:"video" };
        }else{
            // doc or image
            return { size:24000000,type:"doc/image" }
            
        }
    }
}

exports.uploadMedia = (req, res) => {
    let type = 'session_id';
    if (req.query.type === 'submission') {
        type = 'submission_id';
    }
    uploadMedia(req, res, (err, response) => {
        if (!req.file) {
            res.json({ error: true, result: "File size cannot larger than 500MB" });
        } else {

            let maxSize = checkfileSize(req.file);
            if(req.file.size > maxSize.size){
                return res.json({ error:true, result:`${maxSize.type.toUpperCase()} file size cannot larger than ${maxSize.size/1000000}MB` })
            }
            if (env.AWS_BUCKET) {
                let path = req.file.path;
                let newDate = new Date();
                let keyName = `${env.AWS_BUCKET_ENV}/training/course/session`;

                let replacename = req.file.originalname.replace(/[^A-Z0-9.]/ig, "_");
                var params = {
                    ACL: 'public-read',
                    Bucket: env.AWS_BUCKET,
                    Body: fs.createReadStream(path),
                    Key: `${keyName}/${Date.now()}-${replacename}`,
                    ContentDisposition: 'inline',
                    ContentType: req.file.mimetype
                };
                console.info(req.file.mimetype,'????')
                s3.upload(params, (err, data) => {
                    if (err) {
                        res.json({ error: true, msg: "Error Upload Media", result: err });
                    }
                    if (data) {
                        fs.unlinkSync(path);

                        let form = {
                            image: data.Location,
                            type: ''
                        }

                        switch (req.file.mimetype) {
                            case 'application/pdf': form.type = 'PDF'; break;
                            case 'application/vnd.ms-powerpoint': form.type = 'PowerPoint'; break;
                            case 'application/vnd.openxmlformats-officedocument.presentationml.presentation': form.type = 'PowerPoint'; break;
                            case 'application/vnd.ms-excel': form.type = 'Excel'; break;
                            case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet': form.type = 'Excel'; break;
                            case 'image/jpeg': form.type = 'Image'; break;
                            case 'image/png': form.type = 'Image'; break;
                            case 'video/mp4': form.type = 'Video'; break;
                            case 'audio/mp4': form.type = 'Audio'; break;
                            case 'video/x-msvideo': form.type = 'Video'; break;
                            case 'application/msword': form.type = 'Word'; break;
                            case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document': form.type = 'Word'; break;
                            default: form.type = 'Others'
                        }

                        db.query(`INSERT INTO training_course_media (${type}, name, type, url) VALUES('${req.params.id}','${req.file.originalname}','${form.type}','${form.image}')`, async (error, result, fields) => {
                            if (error) {
                                res.json({ error: true, result: error })
                            } else {
                                let mediaUploaded = await db.query(`SELECT * FROM training_course_media WHERE id='${result.insertId}'`);
                                res.json({ error: false, result: mediaUploaded[0] })
                            }
                        })

                    }
                })
            }
            else {
                let form = {
                    image: `${env.APP_URL}/training/course/session/${req.file.filename}`
                }
                switch (req.file.mimetype) {
                    case 'application/pdf': form.type = 'PDF'; break;
                    case 'application/vnd.ms-powerpoint': form.type = 'PowerPoint'; break;
                    case 'application/vnd.openxmlformats-officedocument.presentationml.presentation': form.type = 'PowerPoint'; break;
                    case 'application/vnd.ms-excel': form.type = 'Excel'; break;
                    case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet': form.type = 'Excel'; break;
                    case 'image/jpeg': form.type = 'Image'; break;
                    case 'image/png': form.type = 'Image'; break;
                    case 'video/mp4': form.type = 'Video'; break;
                    case 'audio/mp4': form.type = 'Audio'; break;
                    case 'video/x-msvideo': form.type = 'Video'; break;
                    case 'application/msword': form.type = 'Word'; break;
                    case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document': form.type = 'Word'; break;
                    default: form.type = 'Others'
                }

                db.query(`INSERT INTO training_course_media (${type}, name, type, url) VALUES('${req.params.id}','${req.file.originalname}','${form.type}','${form.image}')`, async (error, result, fields) => {
                    if (error) {
                        res.json({ error: true, result: error })
                    } else {
                        let mediaUploaded = await db.query(`SELECT * FROM training_course_media WHERE id='${result.insertId}'`);
                        res.json({ error: false, result: mediaUploaded[0] })
                    }
                })
            }

        }
    })
}

exports.deleteMedia = (req, res) => {
    db.query(
        `DELETE FROM training_course_media
        WHERE id = ?;`,
        [req.params.id],
        (error, result) => {
            if (error) {
                return res.json({ error: true, result: error.message });
            };

            return res.json({ error: false, result });
        }
    );
};

/**
 * Create Course
 * 
 * @param {*} req.body.company_id = company_id (mandatory)
 * @param {*} req.body.title = title course (mandatory)
 * @param {*} req.body.overview = content course (mandatory)
 * @param {*} req.body.start_time = start dateTime course (mandatory)
 * @param {*} req.body.end_time = end_dateTime couse (mandatory)
 * @param {*} req.body.scheduled = default 0 (mandatory)
 * @param {*} req.body.require_course_id = default [] (mandatory)
 * @param {*} req.body.created_by = user action (mandatory)
 * @param {*} req.body.session = session course default [] (mandatory)
 * @param {*} res.error = boolean 
 * @param {*} res.result = object 
 * 
 */
exports.create = async (req, res) => {
    try {
        let session = req.body.session;
        let queryParams = [[req.body.company_id, req.body.title, req.body.overview, 'Active', req.body.require_course_id.length ? req.body.require_course_id : 0, req.body.scheduled, req.body.start_time, req.body.end_time, req.body.created_by, new Date()]];
        let queryString = `INSERT INTO training_course 
        (company_id, title, overview, status, require_course_id, scheduled, start_time, end_time, created_by, created_at) 
        VALUES (?);`;

        let result = await db.query(queryString, queryParams);
        if (result) {

            let course_id = result.insertId;
            queryString = `INSERT INTO agenda_course(course_id) VALUES('${course_id}');`;


            if (session.length > 0) {

                queryString = `INSERT INTO agenda_course(course_id,agenda) `

                session.forEach((str, index) => {
                    let temp = {
                        type: str.type || null,
                        id: str.id || null,
                        is_mandatory: is_mandatory || null
                    }
                    queryString += `VALUES('${course_id}','${JSON.stringify(temp)}')`;
                    if (index == str.length--) {
                        queryString += ',';
                    }
                });
            }
            db.query(queryString);
            return res.json({ error: false, result: result })
        } else {
            return res.json({ error: true, result: "Create course is failed" });
        }
    } catch (error) {
        console.error({ function: "create", data: error, filename: __filename, error: true });
        return res.json({ error: true, result: error });
    }
};

/**
 * getAgendaCourse
 * 
 * @param {number} id = course_id (mandatory)
 * @returns {object} 
 * 
 */
function getAgendaCourse(id) {
    return new Promise(async (resolve) => {
        try {
            // liveclass , materi , exam
            let queryString = `
                SELECT a.id ,'1' as 'type','Liveclass' as 'label',a.judul as 'title',
                a.start_time as 'start_time',a.end_time AS 'end_time',NULL AS content 
                FROM training_course tc 
                LEFT JOIN webinars a ON a.training_course_id = tc.id 
                WHERE tc.id = '${id}' and a.id IS NOT NULL
                UNION 

                SELECT b.id,'2' as 'type','Exam' as 'label',b.title as 'title',
                b.start_time as 'start_time',b.end_time as 'end_time',NULL AS content 
                FROM training_course tc 
                LEFT JOIN training_exam b ON b.course_id = tc.id 
                WHERE tc.id = '${id}' and b.id IS NOT NULL
                UNION 

                SELECT c.id ,'3' as 'type','Theory' as 'label',c.title as 'title',
                null as 'start_time',NULL AS 'end_time',NULL AS content 
                FROM training_course tc 
                LEFT JOIN training_course_session c ON c.course_id = tc.id 
                WHERE tc.id = '${id}' and c.id IS NOT NULL 

                UNION
                SELECT c.id ,'4' as 'type','Submission' as 'label',c.title,
                c.start_time,c.end_time,content  
                FROM training_course tc 
                LEFT JOIN training_course_submission c ON c.course_id = tc.id 
                WHERE tc.id = '${id}' and c.id IS NOT NULL;

                SELECT ac.course_id , ac.agenda from agenda_course ac 
                where ac.course_id = '${id} and ac.agenda is not null';
            `;

            let resultData = await db.query(queryString);
            let agendaData = resultData[1];
            let submission = resultData[0];

            if (agendaData.length) {
                if (agendaData[0].agenda) {
                    agendaData = JSON.parse(agendaData[0].agenda);
                }
            }
            let tmp = [];
            agendaData.forEach((str, i) => {
                if (str.id) {
                    tmp.push(str);
                }
            });
            agendaData = tmp;

            agendaData.forEach((str, i) => {
                let idx = resultData[0].findIndex((str1) => { return (str.id == str1.id && str.type == str1.type) });
                if (idx > -1) {
                    if (agendaData[i].type === '1') {
                        resultData[0][idx].is_mandatory = agendaData[i].is_mandatory == 1 ? true : false;
                    }
                    agendaData[i] = resultData[0][idx];
                }
            });

            return resolve(agendaData);

        } catch (e) {
            console.error({ error: true, data: e, function: "read", filename: __filename });
            return resolve(null)
        }
    });
}

/**
 * Read Course
 * 
 * @param {*} req.params.id = course_id (mandatory)
 * @param {*} res.error = boolean 
 * @param {*} res.result = object 
 * 
 */
exports.read = async (req, res) => {
    let queryString = `SELECT *, (SELECT SUM(time) FROM training_course_session WHERE course_id=c.id) AS total_session_time,
    (IF(NOW() BETWEEN c.start_time AND c.end_time != '1' && c.scheduled = 1, '0', '1')) AS on_schedule 
    FROM training_course c WHERE c.id = ? LIMIT 1;`;
    let queryParams = [req.params.id];
    const { email } = req.app.token;


    let result = await db.query(queryString, queryParams);

    if (result.length) {

        // GET Materi
        queryString = `SELECT s.*, IF(r.is_read IS NOT NULL, r.is_read, 0) AS is_read 
        FROM training_course_session s 
        LEFT JOIN (SELECT r.* FROM training_course_session_read r JOIN training_user u ON u.id = r.training_user_id WHERE u.email=? GROUP BY r.session_id) r ON r.session_id = s.id
        WHERE s.course_id=?
        ORDER BY s.sort ASC`;

        queryParams = [email, req.params.id];

        let session = await db.query(queryString, queryParams);

        if (session.length) {
            for (let i = 0; i < session.length; i++) {
                let media = await db.query(`SELECT * FROM training_course_media WHERE session_id =?`, [session[i].id]);
                session[i].media = media;
            }
        }

        let agenda = await getAgendaCourse(req.params.id);

        if (agenda) {
            agenda.forEach(async (str, index) => {

                let idx = session.findIndex((str1) => { return (str.type == 3 && str.id == str1.id); });
                if (idx > -1) {

                    str.title = session[idx].title;
                    str.time = session[idx].time;
                    str.is_read = session[idx].is_read;
                    str.media = session[idx].media;
                    str.content = session[idx].content;
                    str.sort = index;
                    str.course_id = req.params.id;

                } else {
                    if (str.type.toString() === '4') {
                        str.media = [];
                        str.sort = index;
                        str.course_id = req.params.id
                    } else {
                        str.media = [];
                        str.course_id = req.params.id;
                    }
                }
            });
            session = agenda;

            for (let i = 0; i < session.length; i++) {
                if (session[i].type === '4') {
                    let media = await db.query(`SELECT * FROM training_course_media WHERE submission_id =?`, [session[i].id]);
                    session[i].media = media;
                }
            }
        }

        result[0].session = session;

        if (result[0].require_course_id !== null && result[0].require_course_id > 0) {
            let requireCheck = await db.query(`SELECT MIN(r.pass_exam) AS require_check
                FROM (
                SELECT e.title, (SELECT COUNT(r.id) FROM training_exam_result r JOIN training_user tu ON tu.id = r.training_user_id WHERE r.exam_id=e.id AND r.pass=1 AND tu.email='${email}') AS pass_exam
                FROM training_exam e
                WHERE e.course_id = ${result[0].require_course_id} AND e.exam=1
                ) AS r`);
            if (requireCheck[0].require_check === 0) {
                let reqCourse = await db.query(`SELECT title FROM training_course WHERE id=${result[0].require_course_id}`);
                result[0].on_schedule = "0";
                result[0].message = `This course is require you to pass course '${reqCourse[0].title}'`
            }
            return res.json({ error: false, result: result[0] });
        } else {
            return res.json({ error: false, result: result[0] });
        }
    } else {
        return res.json({ error: false, result: result });
    }
};

exports.read2 = (req, res) => {
    const { email } = req.app.token
    db.query(
        `SELECT *, (SELECT SUM(time) FROM training_course_session WHERE course_id=c.id) AS total_session_time,
        (IF(NOW() BETWEEN c.start_time AND c.end_time != '1' && c.scheduled = 1, '0', '1')) AS on_schedule
        FROM training_course c
        WHERE c.id = ?
        LIMIT 1;`,
        [req.params.id],
        async (error, result) => {
            if (error) {
                return res.json({ error: true, result: error.message });
            };

            let session = await db.query(
                `SELECT s.*, IF(r.is_read IS NOT NULL, r.is_read, 0) AS is_read 
                FROM training_course_session s
                LEFT JOIN (SELECT r.* FROM training_course_session_read r JOIN training_user u ON u.id = r.training_user_id WHERE u.email=? GROUP BY r.session_id) r ON r.session_id = s.id
                WHERE s.course_id=?
                ORDER BY s.sort ASC`,
                [email, req.params.id]);
            for (let i = 0; i < session.length; i++) {
                let media = await db.query(`SELECT * FROM training_course_media WHERE session_id =?`, [session[i].id]);
                session[i].media = media;
            }
            result[0].session = session;
            if (result[0].require_course_id !== null && result[0].require_course_id > 0) {
                let requireCheck = await db.query(`SELECT MIN(r.pass_exam) AS require_check
                FROM (
                SELECT e.title, (SELECT COUNT(r.id) FROM training_exam_result r JOIN training_user tu ON tu.id = r.training_user_id WHERE r.exam_id=e.id AND r.pass=1 AND tu.email='${email}') AS pass_exam
                FROM training_exam e
                WHERE e.course_id = ${result[0].require_course_id} AND e.exam=1
                ) AS r`);
                if (requireCheck[0].require_check === 0) {
                    let reqCourse = await db.query(`SELECT title FROM training_course WHERE id=${result[0].require_course_id}`);
                    result[0].on_schedule = "0";
                    result[0].message = `This course is require you to pass course '${reqCourse[0].title}'`
                }
                return res.json({ error: false, result: result[0] });
            }
            else {
                return res.json({ error: false, result: result[0] });
            }
        }
    );
};

exports.browse = async (req, res) => {
    db.query(
        `SELECT c.*, GROUP_CONCAT(DISTINCT m.type) AS content, (SELECT COUNT(id) FROM training_course_session WHERE course_id=c.id) AS session_count,
        (SELECT SUM(time) FROM training_course_session WHERE course_id=c.id) AS total_session_time,
        (IF(NOW() BETWEEN c.start_time AND c.end_time != '1' && c.scheduled = 1, '0', '1')) AS on_schedule
        FROM training_course c
        LEFT JOIN training_course_session s ON s.course_id=c.id
        LEFT JOIN training_course_media m ON m.session_id=s.id
        WHERE c.company_id=?
            AND c.status='Active'
        GROUP BY c.id
        ORDER BY c.scheduled ASC, on_schedule DESC, c.created_at DESC`,
        [req.params.company_id],
        async (error, result) => {
            if (error) {
                return res.json({ error: true, result: error.message });
            };

            const { email } = req.app.token
            await Promise.all(result.map(async (item, index) => {
                if (item.require_course_id !== null && item.require_course_id > 0) {
                    let requireCheck = await db.query(`SELECT MIN(r.pass_exam) AS require_check
                    FROM (
                    SELECT e.title, (SELECT COUNT(r.id) FROM training_exam_result r JOIN training_user tu ON tu.id = r.training_user_id WHERE r.exam_id=e.id AND r.pass=1 AND tu.email='${email}') AS pass_exam
                    FROM training_exam e
                    WHERE e.course_id = ${item.require_course_id} AND e.exam=1
                    ) AS r`);
                    if (requireCheck[0].require_check === 0) {
                        let reqCourse = await db.query(`SELECT title FROM training_course WHERE id=${item.require_course_id}`);
                        result[index].on_schedule = "0";
                        result[index].message = `This course is require you to pass course '${reqCourse[0].title}'`
                    }
                }
            }))

            return res.json({ error: false, result: result });
        }
    );
};

exports.browseForQuestion = async (req, res) => {
    const listTagByCompany = await moduleDB.getListTagByIdCompany({ idCompany: req.params.company_id, lisensi_id: req.query.lisensi_id });
    db.query(
        `SELECT c.*, (SELECT COUNT(id) FROM training_questions WHERE training_course_id=c.id AND status='Active') AS total_questions,
        (SELECT GROUP_CONCAT(concat('{"value":',id,',"label":"',tag,'"}') SEPARATOR ';') FROM training_questions WHERE training_course_id=c.id AND status='Active' and tag is not null and CHAR_LENGTH(tag) > 1) AS tagList 
        FROM training_course c
        WHERE c.company_id=?
            AND c.status='Active'
        ORDER BY created_at DESC`,
        [req.params.company_id],
        async (error, result) => {
            if (error) {
                return res.json({ error: true, result: error.message });
            };

            if (result.length) {
                let tmp = []
                result.forEach((str) => {
                    if (str.tagList) {
                        let tagTmp = [];
                        let count = str.tagList.split(";");
                        count.forEach((itemTag) => {
                            try {

                                let json = JSON.parse(itemTag);
                                json.value = json.label;
                                let idx = tagTmp.findIndex((arg) => { return arg.label === json.label });
                                if (idx > -1) {
                                    tagTmp[idx].total_questions++;
                                } else {
                                    json.total_questions = 1;
                                    tagTmp.push(json);
                                }
                            } catch (e) { /*gagal build json*/ }
                        });


                        str.tagList = tagTmp;
                    }
                    tmp.push(str);
                });
                result = tmp;
            }

            let totalQuestions = await db.query(`
            SELECT SUM((SELECT COUNT(id) FROM training_questions WHERE training_course_id=c.id AND status='Active')) AS total_questions
            FROM training_course c
            WHERE c.company_id=${req.params.company_id}
                AND c.status='Active'
            ORDER BY created_at DESC`)
            return res.json({ error: false, result: result, total_questions: totalQuestions[0].total_questions, tag: listTagByCompany });
        }
    );
};
exports.browseAdmin = async (req, res) => {
    let sqlString = `SELECT c.*, 
    TIMESTAMPDIFF(MINUTE,c.start_time,c.end_time) as total_session,
    GROUP_CONCAT(DISTINCT m.type) AS content, 
    (SELECT COUNT(id) FROM training_course_session WHERE course_id=c.id) AS session_count, 
    (SELECT SUM(time) FROM training_course_session WHERE course_id=c.id) AS total_session_time,
    ac.agenda 
    FROM training_course c 
    LEFT JOIN training_course_session s ON s.course_id=c.id 
    LEFT JOIN training_course_media m ON m.session_id=s.id 
    left join agenda_course ac ON ac.course_id = c.id  
    WHERE c.company_id=?
        AND c.status='Active'
    GROUP BY c.id
    ORDER BY created_at DESC`;

    let result = await db.query(sqlString, [req.params.company_id]);
    let tmp = [];
    if (result.length) {
        result.forEach((item) => {
            if (item.agenda && item.agenda.length > 1) {
                let json = JSON.parse(item.agenda);
                item.session_count = json.length;
                if (item.total_session > 0) {
                    item.total_session_time = item.total_session;
                }
            }
            tmp.push(item);
        });
    }

    return res.json({ error: false, result: tmp });
    // db.query(sqlString,
    //     [req.params.company_id],
    //     async (error, result) => {
    //         if (error) {
    //             return res.json({ error: true, result: error.message });
    //         };
    //         return res.json({ error: false, result: result });
    //     }
    // );
};
exports.browseAdminArchived = async (req, res) => {
    db.query(
        `SELECT c.*, GROUP_CONCAT(DISTINCT m.type) AS content, (SELECT COUNT(id) FROM training_course_session WHERE course_id=c.id) AS session_count, (SELECT SUM(time) FROM training_course_session WHERE course_id=c.id) AS total_session_time
        FROM training_course c
        LEFT JOIN training_course_session s ON s.course_id=c.id
        LEFT JOIN training_course_media m ON m.session_id=s.id
        WHERE c.company_id=?
            AND c.status='Inactive'
        GROUP BY c.id
        ORDER BY created_at DESC`,
        [req.params.company_id],
        async (error, result) => {
            if (error) {
                return res.json({ error: true, result: error.message });
            };
            return res.json({ error: false, result: result });
        }
    );
};

/**
 * Update Course
 * 
 * @param {*} req.body.images = image course default "" (mandatory)
 * @param {*} req.body.title = title course (mandatory)
 * @param {*} req.body.overview = content course (mandatory)
 * @param {*} req.body.start_time = start dateTime course (mandatory)
 * @param {*} req.body.end_time = end_dateTime couse (mandatory)
 * @param {*} req.body.scheduled = default 0 (mandatory)
 * @param {*} req.body.require_course_id = default [] (mandatory)
 * @param {*} req.body.created_by = user action (mandatory)
 * @param {*} req.body.session = session course default [] (mandatory)
 * @param {*} res.error = boolean 
 * @param {*} res.result = object 
 * 
 */
exports.update = async (req, res) => {
    try {
        let queryString = `
        UPDATE training_course SET title = ?,overview = ?,require_course_id = ?,
        scheduled = ?,start_time = ?,end_time = ?WHERE id = ?;`;

        // untuk collection event yg dipakai, jika ada yg tidak dipakai maka akan di set ke course_id=0
        let groupWebinars = groupSession = groupExam = groupSubmission = [];

        let queryParams = [
            req.body.title, req.body.overview, req.body.require_course_id.length ? req.body.require_course_id : 0,
            req.body.scheduled, req.body.start_time, req.body.end_time, req.params.id
        ];
        let resultUpdate = await db.query(queryString, queryParams);

        if (req.body.session.length) {
            let sessions = req.body.session;
            let temp = [];
            let agendaExist = await db.query(`SELECT * FROM agenda_course WHERE course_id=?`, [req.params.id]);
            if (agendaExist.length){
                queryString = `UPDATE agenda_course SET agenda=? where course_id='${req.params.id}'`
            }
            else{
                queryString = `INSERT INTO agenda_course (course_id, agenda) VALUES ('${req.params.id}', ?)`;
            }

            let sqlAssignLiveclass = ``;
            let sqlAssignExam = ``;

            await req.body.session.map((item, index) => {

                // 1 liveclass, 2 exam, 3 session, 4 submission
                if (item.type === '1') {
                    groupWebinars.push(item.id);
                    sqlAssignLiveclass += `update webinars set training_course_id='${req.params.id}' where id='${item.id}';`;
                }
                else if (item.type === '2') {
                    groupExam.push(item.id);
                    sqlAssignExam += `update training_exam set course_id='${req.params.id}' where id='${item.id}';`;
                }
                else if (item.type === '3') {
                    groupSession.push(item.id);
                }
                else if (item.type === '4') {
                    groupSubmission.push(item.id);
                }

                temp.push({
                    type: item.type || null,
                    id: item.id || null,
                    is_mandatory: item.is_mandatory || null
                });
                if (item.type === '3') {
                    db.query(`UPDATE training_course_session SET title=?, content=?, time=?, sort=? WHERE id=?`, [item.title ? item.title : '', item.content ? item.content : '', item.time, index, item.id])
                }
            })
            if (sqlAssignExam.length > 1 || sqlAssignLiveclass.length > 1) {
                db.query(`${sqlAssignExam}${sqlAssignLiveclass}`);
            }
            db.query(queryString, [JSON.stringify(temp)]);

            // proses delete 
            try {

                let sqlWebinars = sqlExam = sqlSession = sqlSubmission = tmpSql = ``;
                let tmpParams = [];
                sqlWebinars += `update webinars set training_course_id=0 where id NOT IN(?) and training_course_id='${req.params.id}' and is_training_liveclass=1;`;
                sqlExam += `update training_exam set course_id=0 where id NOT IN(?) and course_id='${req.params.id}';`;
                sqlSession += `update training_course_session set course_id=0 where id NOT IN(?) and course_id='${req.params.id}';`;
                sqlSubmission += `update training_course_submission set course_id=0 where id NOT IN(?) and course_id='${req.params.id}';`;

                if (groupWebinars.length) { tmpSql += sqlWebinars; tmpParams.push(groupWebinars); }
                if (groupExam.length) { tmpSql += sqlExam; tmpParams.push(groupExam); }
                if (groupSession.length) { tmpSql += sqlSession; tmpParams.push(groupSession); }
                if (groupSubmission.length) { tmpSql += sqlSubmission; tmpParams.push(groupSubmission); }

                if (tmpSql.length > 1 && tmpParams.length) {
                    let sqlDelete = await db.query(`${tmpSql}`, tmpParams);
                }
            } catch (error) { console.log({ error: true, filename: __filename, message: error }) }

            return res.json({ error: false, result: resultUpdate })
        }
        else {
            queryString = `UPDATE agenda_course SET agenda=null where course_id='${req.params.id}'`;
            db.query(queryString);
            return res.json({ error: false, result: resultUpdate })
        }
    } catch (error) {
        console.error({ function: "update", data: error, filename: __filename, error: true });
        return res.json({ error: true, result: error })
    }
};

exports.addSession = (req, res) => {
    db.query(
        `INSERT INTO training_course_session
        (course_id, title, content, time, sort)
        VALUES (?);`,
        [[req.body.course_id, req.body.title, req.body.content, req.body.time, req.body.sort]],
        (error, result) => {
            if (error) {
                return res.json({ error: true, result: error.message });
            }
            else {
                return res.json({ error: false, result: result })
            }
        }
    );
};

exports.delete = (req, res) => {
    db.query(`
        UPDATE training_course
        SET status='Inactive'
        WHERE id = ?;

        UPDATE agenda_course
        SET status='Inactive'
        WHERE course_id = ?;
        `,
        [req.params.id, req.params.id],
        (error, result) => {
            if (error) {
                return res.json({ error: true, result: error.message });
            };

            return res.json({ error: false, result });
        }
    );
};
exports.activate = (req, res) => {
    db.query(
        `UPDATE training_course
        SET status='Active'
        WHERE id = ?;`,
        [req.params.id],
        (error, result) => {
            if (error) {
                return res.json({ error: true, result: error.message });
            };

            return res.json({ error: false, result });
        }
    );
};
exports.deleteSession = (req, res) => {
    db.query(
        `DELETE FROM training_course_session
        WHERE id = ?;`,
        [req.params.id],
        async (error, result) => {
            if (error) {
                return res.json({ error: true, result: error.message });
            };

            let currentAgenda = await getAgendaCourse(req.params.courseId)
            let index = currentAgenda.findIndex(x => x.type === String(req.params.type) && x.id === Number(req.params.id));
            if (index > -1) {
                currentAgenda.splice(index, 1);
            }

            queryString = `UPDATE agenda_course SET agenda=? where course_id='${req.params.courseId}'`
            db.query(queryString, [JSON.stringify(currentAgenda)],
                (error, result) => {
                    return res.json({ error: false, result });
                });
        }
    );
};

exports.readSession = (req, res) => {
    db.query(
        `DELETE FROM training_course_session_read WHERE session_id ='${req.body.session_id}' AND training_user_id='${req.body.training_user_id}'`,
        (error, result) => {
            if (error) {
                return res.json({ error: true, result: error.message });
            };

            db.query(
                `INSERT INTO training_course_session_read (session_id, training_user_id,is_read) VALUES ('${req.body.session_id}','${req.body.training_user_id}',1)`,
                (error, result) => {
                    if (error) {
                        return res.json({ error: true, result: error.message });
                    };

                    return res.json({ error: false, result });
                }
            );
        }
    );
};

exports.plan = async (req, res) => {
    let { email, user_id } = req.app.token
    let append_query = '';

    // checking user_training
    let queryChecking = `SELECT
    distinct a.user_id, a.voucher ,c.grup_id,c.company_id,c.grup_name,a.company_id,a.name
    FROM user a
    LEFT JOIN grup c ON c.grup_id = a.grup_id
    WHERE 
    a.user_id = ? and lower(c.grup_name) LIKE '%user training%'`;

    let resultChecking = await db.query(queryChecking, [user_id]);

    let dataUser = await db.query(`SELECT * FROM user WHERE email='${email}' AND status='active' LIMIT 1`);
    let dataUserTraining = await db.query(`SELECT * FROM training_user WHERE email='${email}' AND status='active' LIMIT 1`);
    dataUserTraining = dataUserTraining.length ? dataUserTraining : [{ id: 0 }];

    if (resultChecking.length > 0) {

        append_query = `
        FROM training_course_company tcc  
        left join training_course c on c.id = tcc.training_course_id 
        LEFT JOIN training_course_session s ON s.course_id=c.id 
        LEFT JOIN training_course_media m ON m.session_id=s.id
        LEFT JOIN training_course_submission s2 ON s2.course_id=c.id
        LEFT JOIN training_course_media m2 ON m2.submission_id=s2.id
        LEFT JOIN training_course c2 ON c2.id = c.require_course_id 
        WHERE tcc.training_company_id = '${dataUserTraining[0].training_company_id}' and tcc.active=1 AND c.status='Active'`;
    } else {
        append_query = `
        FROM training_course c
        LEFT JOIN training_course_session s ON s.course_id=c.id
        LEFT JOIN training_course_media m ON m.session_id=s.id
        LEFT JOIN training_course_submission s2 ON s2.course_id=c.id
        LEFT JOIN training_course_media m2 ON m2.submission_id=s2.id
        LEFT JOIN training_course c2 ON c2.id = c.require_course_id
        WHERE c.company_id='${req.params.company_id}' AND c.status='Active' `;
    }

    let query1 = `SELECT c.*, GROUP_CONCAT(DISTINCT m.type) AS content, (SELECT COUNT(id) FROM training_course_session WHERE course_id=c.id) AS session_count,
        ((SELECT IF(SUM(time) IS NULL, 0, SUM(time)) FROM training_course_session WHERE course_id=c.id)+(SELECT IF(SUM(time_limit) IS NULL, 0, SUM(time_limit)) FROM training_exam WHERE course_id=c.id AND status='Active')+(SELECT IF(SUM(TIMESTAMPDIFF(MINUTE, start_time, end_time)) IS NULL, 0, SUM(TIMESTAMPDIFF(MINUTE, start_time, end_time))) FROM webinars WHERE training_course_id=c.id AND publish='1')) AS total_session_time,
        (IF(NOW() BETWEEN c.start_time AND c.end_time != '1' && c.scheduled = 1, '0', '1')) AS on_schedule, c2.title AS require_course_title, 'Course' AS type 
        ${append_query} 
        GROUP BY c.id 
        ORDER BY c.scheduled ASC, c.start_time ASC, require_course_id ASC;`;

    db.query(query1,
        async (error, result) => {
            if (error) {
                return res.json({ error: true, result: error.message });
            };

            let arrCourseId = [];

            if (result.length) {
                if (dataUserTraining[0].id) { // for mobile

                    let infoUser = {
                        user_id: dataUserTraining[0].id,
                        training_company_id: dataUserTraining[0].training_company_id,
                        training_user_id: dataUserTraining[0].id
                    }
                    result = await checkUserAssignedCourseId(result, infoUser);
                }
                //console.log(infoUser, result.length)
            }

            await Promise.all(result.map(async (item, index) => {

                arrCourseId.push(item.id);

                if (item.require_course_id !== null && item.require_course_id > 0) {
                    let session = await db.query(
                        `SELECT s.*, IF(r.is_read IS NOT NULL, r.is_read, 0) AS is_read, 'Session' AS type
                        FROM training_course_session s
                        LEFT JOIN (SELECT r.* FROM training_course_session_read r JOIN training_user u ON u.id = r.training_user_id WHERE u.email=? GROUP BY r.session_id) r ON r.session_id = s.id
                        WHERE s.course_id=?
                        ORDER BY s.sort ASC`,
                        [email, item.require_course_id]);
                    //submission
                    let submission = await db.query(
                        `SELECT s.*, r.is_read, r.submission_file, 'Submission' AS type, IF(r.created_at, DATEDIFF(s.start_time,r.created_at), DATEDIFF(s.start_time,s.end_time) )  as 'duration' 
                            FROM training_course_submission s
                            LEFT JOIN (SELECT r.* FROM training_submission_answer r JOIN training_user u ON u.id = r.training_user_id WHERE u.email=? GROUP BY r.submission_id) r ON r.submission_id = s.id
                            WHERE s.course_id=? and r.status='active' 
                            ORDER BY s.sort ASC`,
                        [email, item.id]);
                    for (let i = 0; i < submission.length; i++) {
                        let media = await db.query(`SELECT * FROM training_course_media WHERE submission_id =?`, [submission[i].id]);
                        submission[i].media = media;
                    }
                    await Promise.all(submission.map(async (row, i) => {
                        var m = row.duration % 60;
                        var h = (row.duration - m) / 60;
                        var duration = row.duration > 0 ? (h < 10 ? "0" : "") + h.toString() + ":" + (m < 10 ? "0" : "") + m.toString() : '-';
                        row.duration = duration;
                    }))
                    result[index].submission = submission;
                    let liveclass = await db.query(
                        `SELECT w.*, IF((SELECT p.id FROM webinar_peserta p LEFT JOIN user u ON u.user_id = p.user_id WHERE u.email=? AND p.webinar_id=w.id) IS NOT NULL, 1, 0) AS is_invited, 'Live Class' AS type,
                        (IF((SELECT COUNT(p.id) AS hadir FROM webinar_peserta p WHERE p.webinar_id=w.id AND p.user_id=? AND p.status='2')>0 AND w.status=3,1,0)) AS is_read,
                        (IF(NOW() BETWEEN w.start_time AND w.end_time != '1', '0', '1')) AS on_schedule
                        FROM webinars w
                        WHERE w.training_course_id=? AND w.publish='1'
                        ORDER BY w.id DESC`,
                        [email, dataUser[0].user_id, item.require_course_id]);

                    let quiz = await db.query(
                        `SELECT e.*, (SELECT SUM(total) FROM training_exam_composition WHERE exam_id=e.id) AS number_of_question, l.name AS licenses_type,  IF(e.scheduled=1,'Yes','No') AS is_scheduled, IF(e.exam = 1, 'Exam', 'Quiz') AS type,
                        (IF((SELECT COUNT(r.id) AS hadir FROM training_exam_result r WHERE r.exam_id=e.id AND r.training_user_id=? AND r.assignee_id=a.id)>0,1,0)) AS is_read, a.id AS assignee_id,
                        (IF(NOW() BETWEEN e.start_time AND e.end_time != '1' && e.scheduled = 1, '0', '1')) AS on_schedule
                        FROM training_exam e
                        LEFT JOIN training_licenses_type l ON l.id = e.licenses_type_id
                        LEFT JOIN training_exam_assignee a ON a.exam_id = e.id
                        WHERE e.status='Active' AND e.exam=0 AND e.course_id=? AND a.training_user_id=?
                        ORDER BY e.created_at DESC;`,
                        [dataUserTraining[0].id, item.require_course_id, dataUserTraining[0].id]);
                    let exam = await db.query(
                        `SELECT e.*, (SELECT SUM(total) FROM training_exam_composition WHERE exam_id=e.id) AS number_of_question, l.name AS licenses_type,  IF(e.scheduled=1,'Yes','No') AS is_scheduled, IF(e.exam = 1, 'Exam', 'Quiz') AS type,
                        (
                            IF(
                                (
                                    SELECT COUNT(r.id) AS hadir FROM training_exam_result r WHERE r.exam_id=e.id AND r.training_user_id=? AND r.assignee_id=a.id and r.pass = 1 order by r.score DESC
                                )>0,1,0)
                        ) AS is_read, 
                        (
                            SELECT IF(r.score IS NULL,0,r.score) AS hadir FROM training_exam_result r WHERE r.exam_id=e.id AND r.training_user_id=? AND r.assignee_id=a.id order by r.created_at DESC LIMIT 1
                        ) AS current_score, 
                        a.id AS assignee_id,
                        (IF(NOW() BETWEEN e.start_time AND e.end_time != '1' && e.scheduled = 1, '0', '1')) AS on_schedule,
                        r.id AS result_id,
                        r.correct AS correct_answer,
                        r.total AS total_question
                        FROM training_exam e
                        LEFT JOIN training_licenses_type l ON l.id = e.licenses_type_id
                        LEFT JOIN training_exam_assignee a ON a.exam_id = e.id
                        LEFT JOIN (SELECT * FROM training_exam_result WHERE training_user_id=?) r ON r.exam_id=e.id
                        WHERE e.status='Active' AND e.exam=1 AND e.course_id=? AND a.training_user_id=?
                        ORDER BY e.created_at DESC;`,
                        [dataUserTraining[0].id, dataUserTraining[0].id, dataUserTraining[0].id, item.require_course_id, dataUserTraining[0].id]);


                    // session material based on agenda
                    let agenda = await getAgendaCourse(item.require_course_id);
                    if (agenda) {
                        agenda.forEach((str, index) => {

                            let idx = session.findIndex((str1) => { return (str.type == 3 && str.id == str1.id); });
                            if (idx > -1) {
                                str.time = session[idx].time;
                                str.is_read = session[idx].is_read;
                                str.media = session[idx].media;
                                str.content = session[idx].content;
                                str.sort = index;
                            } else {
                                str.media = [];
                                str.content = '';
                                str.sort = index;
                            }
                        });

                        session = agenda.filter(x => x.label === 'Theory');
                    }

                    // course progress
                    let totalSession = session.length;
                    let readSession = session.filter(x => x.is_read === 1).length;
                    let readSubmission = submission.filter(x => x.is_read === 1).length;
                    let totalLiveclass = liveclass.length;
                    let readLiveclass = liveclass.filter(x => x.is_read === 1).length;
                    let totalQuiz = quiz.length;
                    let readQuiz = quiz.filter(x => x.is_read === 1).length;
                    let totalExam = exam.length;
                    let readExam = exam.filter(x => x.is_read === 1).length;

                    let total = agenda.length;
                    let read = readSession + readLiveclass + readQuiz + readExam + readSubmission;
                    let progress = Math.round(read / total * 100);

                    if (progress < 100) {
                        let reqCourse = await db.query(`SELECT title FROM training_course WHERE id=${item.require_course_id}`);
                        result[index].on_schedule = "0";
                        result[index].message = `This course is require you to pass all content in course '${reqCourse[0].title}'`
                    }
                    // let requireCheck = await db.query(`SELECT MIN(r.pass_exam) AS require_check
                    // FROM (
                    // SELECT e.title, (SELECT COUNT(r.id) FROM training_exam_result r JOIN training_user tu ON tu.id = r.training_user_id WHERE r.exam_id=e.id AND r.pass=1 AND tu.email='${email}') AS pass_exam
                    // FROM training_exam e
                    // WHERE e.course_id = ${item.require_course_id} AND e.exam=1
                    // ) AS r`);
                    // if (requireCheck[0].require_check === 0) {
                    //     let reqCourse = await db.query(`SELECT title FROM training_course WHERE id=${item.require_course_id}`);
                    //     result[index].on_schedule = "0";
                    //     result[index].message = `This course is require you to pass course '${reqCourse[0].title}'`
                    // }
                }
                var m = item.total_session_time % 60;
                var h = (item.total_session_time - m) / 60;
                var duration = item.total_session_time > 0 ? (h < 10 ? "0" : "") + h.toString() + ":" + (m < 10 ? "0" : "") + m.toString() : '-';
                result[index].duration = duration;
                let session = await db.query(
                    `SELECT s.*, IF(r.is_read IS NOT NULL, r.is_read, 0) AS is_read, 'Session' AS type
                    FROM training_course_session s
                    LEFT JOIN (SELECT r.* FROM training_course_session_read r JOIN training_user u ON u.id = r.training_user_id WHERE u.email=? GROUP BY r.session_id) r ON r.session_id = s.id
                    WHERE s.course_id=?
                    ORDER BY s.sort ASC`,
                    [email, item.id]);
                await Promise.all(session.map(async (row, i) => {
                    var m = row.time % 60;
                    var h = (row.time - m) / 60;
                    var duration = row.time > 0 ? (h < 10 ? "0" : "") + h.toString() + ":" + (m < 10 ? "0" : "") + m.toString() : '-';
                    row.duration = duration;
                }))
                result[index].session = session;

                let submission = await db.query(
                    `SELECT s.*, IF(r.is_read IS NOT NULL, r.is_read, 0) AS is_read, 'Submission' AS type, r.created_at as 'end_submit', '-' as 'duration' 
                    FROM training_course_submission s
                    LEFT JOIN (SELECT r.* FROM training_submission_answer r JOIN training_user u ON u.id = r.training_user_id WHERE u.email=? GROUP BY r.submission_id) r ON r.submission_id = s.id
                    WHERE s.course_id=?
                    ORDER BY s.sort ASC`,
                    [email, item.id]);
                await Promise.all(submission.map(async (row, i) => {
                    let jamMl = new Date(row.start_time);
                    row.end_time = row.end_submit ? row.end_submit : row.end_time;
                    let jamSl = new Date(row.end_time);
                    let diff = Math.abs(jamSl - jamMl);
                    let diffHour = Math.floor((diff % 86400000) / 3600000);
                    let diffMin = Math.round(((diff % 86400000) % 3600000) / 60000);
                    let duration = row.start_time && row.end_time && diff > 0 ? diffHour.toString().padStart(2, "0") + ':' + diffMin.toString().padStart(2, "0") : '-';
                    row.duration = duration;
                }))
                result[index].submission = submission;

                let liveclass = await db.query(
                    `SELECT w.*, IF((SELECT p.id FROM webinar_peserta p LEFT JOIN user u ON u.user_id = p.user_id WHERE u.email=? AND p.webinar_id=w.id) IS NOT NULL, 1, 0) AS is_invited, 'Live Class' AS type,
                    (IF((SELECT COUNT(p.id) AS hadir FROM webinar_peserta p WHERE p.webinar_id=w.id AND p.user_id=? AND p.status='2')>0,1,0)) AS is_read
                    FROM webinars w
                    WHERE w.training_course_id=? AND w.publish='1'
                    ORDER BY w.id DESC`,
                    [email, dataUser[0].user_id, item.id]);
                await Promise.all(liveclass.map(async (row, i) => {
                    let jamMl = new Date(row.start_time);
                    let jamSl = new Date(row.end_time);
                    let diff = Math.abs(jamSl - jamMl);
                    let diffHour = Math.floor((diff % 86400000) / 3600000);
                    let diffMin = Math.round(((diff % 86400000) % 3600000) / 60000);
                    let duration = row.start_time && row.end_time && diff > 0 ? diffHour.toString().padStart(2, "0") + ':' + diffMin.toString().padStart(2, "0") : '-';
                    row.duration = duration;
                }))
                result[index].liveclass = liveclass;

                let sql = `SELECT e.*, (SELECT SUM(total) FROM training_exam_composition WHERE exam_id=e.id) AS number_of_question, l.name AS licenses_type,  IF(e.scheduled=1,'Yes','No') AS is_scheduled, IF(e.exam = 1, 'Exam', 'Quiz') AS type,
                (IF((SELECT COUNT(r.id) AS hadir FROM training_exam_result r WHERE r.exam_id=e.id AND r.training_user_id=? AND r.assignee_id=a.id)>0,1,0)) AS is_read, a.id AS assignee_id
                FROM training_exam e
                LEFT JOIN training_licenses_type l ON l.id = e.licenses_type_id
                LEFT JOIN training_exam_assignee a ON a.exam_id = e.id
                WHERE e.status='Active' AND e.exam=0 AND e.course_id=? AND a.training_user_id=?
                ORDER BY e.created_at DESC;`
                let sqlAdmin = `SELECT e.*, (SELECT SUM(total) FROM training_exam_composition WHERE exam_id=e.id) AS number_of_question, l.name AS licenses_type,  IF(e.scheduled=1,'Yes','No') AS is_scheduled, IF(e.exam = 1, 'Exam', 'Quiz') AS type,
                (IF((SELECT COUNT(r.id) AS hadir FROM training_exam_result r WHERE r.exam_id=e.id AND r.training_user_id=?)>0,1,0)) AS is_read
                FROM training_exam e
                LEFT JOIN training_licenses_type l ON l.id = e.licenses_type_id
                WHERE e.status='Active' AND e.exam=0 AND e.course_id=?
                ORDER BY e.created_at DESC;`
                let quiz = await db.query(dataUserTraining[0].id === 0 ? sqlAdmin : sql, [dataUserTraining[0].id, item.id, dataUserTraining[0].id]);

                await Promise.all(quiz.map(async (row, i) => {
                    var m = row.time_limit % 60;
                    var h = (row.time_limit - m) / 60;
                    var duration = row.time_limit > 0 ? (h < 10 ? "0" : "") + h.toString() + ":" + (m < 10 ? "0" : "") + m.toString() : '-';
                    row.duration = duration;
                    if (row.repeatable > 0) {
                        row.is_read = 0;
                        row.on_schedule = "1";
                    }
                }))
                result[index].quiz = quiz;

                let sql2 = `SELECT e.*, (SELECT SUM(total) FROM training_exam_composition WHERE exam_id=e.id) AS number_of_question, l.name AS licenses_type,  IF(e.scheduled=1,'Yes','No') AS is_scheduled, IF(e.exam = 1, 'Exam', 'Quiz') AS type,
                (IF((SELECT COUNT(r.id) AS hadir FROM training_exam_result r WHERE r.exam_id=e.id AND r.training_user_id=? AND r.assignee_id=a.id AND r.pass=1)>0,1,0)) AS is_read, a.id AS assignee_id,
                (
                    IF(
                        (
                            SELECT COUNT(r.id) AS hadir FROM training_exam_result r WHERE r.exam_id=e.id AND r.training_user_id='${dataUserTraining[0].id}' AND r.assignee_id=a.id order by r.score DESC
                        )>0,1,0)
                ) AS is_read_repeat 
                FROM training_exam e
                LEFT JOIN training_licenses_type l ON l.id = e.licenses_type_id
                LEFT JOIN training_exam_assignee a ON a.exam_id = e.id
                WHERE e.status='Active' AND e.exam=1 AND e.course_id=? AND a.training_user_id=?
                ORDER BY e.created_at DESC;`
                let sql2Admin = `SELECT e.*, (SELECT SUM(total) FROM training_exam_composition WHERE exam_id=e.id) AS number_of_question, l.name AS licenses_type,  IF(e.scheduled=1,'Yes','No') AS is_scheduled, IF(e.exam = 1, 'Exam', 'Quiz') AS type,
                (IF((SELECT COUNT(r.id) AS hadir FROM training_exam_result r WHERE r.exam_id=e.id AND r.training_user_id=? AND r.pass=1)>0,1,0)) AS is_read
                FROM training_exam e
                LEFT JOIN training_licenses_type l ON l.id = e.licenses_type_id
                WHERE e.status='Active' AND e.exam=1 AND e.course_id=?
                ORDER BY e.created_at DESC;`
                let exam = await db.query(dataUserTraining[0].id === 0 ? sql2Admin : sql2, [dataUserTraining[0].id, item.id, dataUserTraining[0].id]);
                await Promise.all(exam.map(async (row, i) => {
                    var m = row.time_limit % 60;
                    var h = (row.time_limit - m) / 60;
                    var duration = row.time_limit > 0 ? (h < 10 ? "0" : "") + h.toString() + ":" + (m < 10 ? "0" : "") + m.toString() : '-';
                    row.duration = duration;

                    if (row.repeatable > 0) {
                        if (row.is_read_repeat > 0) {
                            row.is_read = 1
                        } else {
                            row.is_read = 0
                        }
                    }
                }))
                result[index].exam = exam;

                // session material based on agenda
                let agenda = await getAgendaCourse(item.id);
                if (agenda) {
                    agenda.forEach((str, index) => {

                        let idx = session.findIndex((str1) => { return (str.type == 3 && str.id == str1.id); });
                        if (idx > -1) {
                            str.time = session[idx].time;
                            str.is_read = session[idx].is_read;
                            str.media = session[idx].media;
                            str.content = session[idx].content;
                            str.sort = index;
                        } else {
                            str.media = [];
                            str.content = '';
                            str.sort = index;
                        }
                    });

                    session = agenda.filter(x => x.label === 'Theory');
                }

                // course progress
                let totalSession = session.length;
                let readSession = session.filter(x => x.is_read === 1).length;
                let readSubmission = submission.filter(x => x.is_read === 1).length;
                let totalLiveclass = liveclass.length;
                let readLiveclass = liveclass.filter(x => x.is_read === 1).length;
                let totalQuiz = quiz.length;
                let readQuiz = quiz.filter(x => x.is_read === 1).length;
                let totalExam = exam.length;
                let readExam = exam.filter(x => x.is_read === 1).length;

                let total = agenda.length;
                let read = readSession + readLiveclass + readQuiz + readExam + readSubmission;
                let progress = Math.round(read / total * 100);
                result[index].progress = progress;

                // progress_status
                result[index].progress_status =
                    item.on_schedule === "0" ?
                        "Inactive"
                        : item.on_schedule === "1" && item.progress === 0 ?
                            "Active"
                            : item.on_schedule === "1" && item.progress < 100 ?
                                "Progress"
                                : "Done";
                // setup agenda
                result[index].agenda = [];
            }))

            let unassignedExam = await db.query(
                `SELECT e.*, (SELECT SUM(total) FROM training_exam_composition WHERE exam_id=e.id) AS number_of_question, l.name AS licenses_type,  IF(e.scheduled=1,'Yes','No') AS is_scheduled, IF(e.exam = 1, 'Exam', 'Quiz') AS type
                FROM training_exam e
                LEFT JOIN training_licenses_type l ON l.id = e.licenses_type_id
                WHERE e.company_id=? AND e.status='Active' AND (e.course_id = 0 OR e.course_id IS NULL)
                ORDER BY e.created_at DESC;`,
                [req.params.company_id]);

            // AGENDA
            //console.log(JSON.stringify(result, null, 2))

            let resultAgenda = await getAgendaCoursePlan(result, arrCourseId);
            result = resultAgenda.length ? resultAgenda : result;
            return res.json({ error: false, result: { data: result, unassigned: unassignedExam } });
        }
    );
};

function getAgendaCoursePlan(result, arrCourseId) {
    return new Promise(async (resolve) => {
        let resultAgenda = await db.query(`select course_id, agenda from agenda_course where course_id in(?) and agenda is not null`, [(arrCourseId.length ? arrCourseId : 'null')]);
        let before = null;
        try {
            resultAgenda.forEach((str, i) => {
                let temp = [];
                let idx = result.findIndex((arg) => { return arg.id == str.course_id });

                if (idx > -1) {
                    let totalExam = 0;
                    let totalSubmission = 0;
                    let totalLiveclass = 0;
                    let totalSession = 0;
                    let totalQuiz = 0;
                    let readExam = 0;
                    let readSubmission = 0;
                    let readLiveclass = 0;
                    let readSession = 0;
                    let readQuiz = 0;
                    let agenda = JSON.parse(str.agenda.toString());
                    agenda.forEach((items, j) => {

                        let idx2 = -1;
                        let name = '';
                        if (items.type === '1') {
                            name = 'liveclass';
                        } else if (items.type === '2') {
                            name = 'exam';
                        } else if (items.type === '3') {
                            name = 'session';
                        }
                        else if (items.type === '4') {
                            name = 'submission';
                        }

                        if (name != '') {
                            idx2 = result[idx][name].findIndex((checkLV) => { return checkLV.id == items.id });
                            if (idx2 > -1) {
                                let obj = result[idx][name];
                                obj[idx2].access = false;
                                if (obj[idx2].is_read > 0 && j == 0) {
                                    obj[idx2].access = true;
                                }
                                else if (j > 0 && temp[j - 1]) {
                                    before = temp[j - 1];
                                    if ((!obj[idx2].on_schedule || Number(obj[idx2].on_schedule) > 0) && before.is_read > 0) {
                                        obj[idx2].access = true;
                                    }
                                }
                                if (items.type === '1' && (!items.is_mandatory || items.is_mandatory === null) && obj[idx2].status === 3) {
                                    obj[idx2].is_read = 1;
                                }

                                if (obj[idx2].type === "Exam") {
                                    if (obj[idx2].score) {
                                        obj[idx2].availableResult = true;
                                    } else {
                                        if (obj[idx2].score > 0) {
                                            obj[idx2].availableResult = true;
                                        } else {
                                            obj[idx2].availableResult = false;

                                        }
                                    }
                                }

                                if (name === 'submission') {
                                    totalSubmission++;
                                    readSubmission += obj[idx2].is_read;
                                }
                                else if (name === 'session') {
                                    totalSession++;
                                    readSession += obj[idx2].is_read;
                                }
                                else if (name === 'exam') {
                                    totalExam++;
                                    readExam += obj[idx2].is_read;
                                }
                                else if (name === 'liveclass') {
                                    totalLiveclass++;
                                    readLiveclass += obj[idx2].is_read;
                                }

                                let total = totalSession + totalLiveclass + totalQuiz + totalExam + totalSubmission;
                                let read = readSession + readLiveclass + readQuiz + readExam + readSubmission;
                                let progress = Math.round(read / total * 100);
                                result[idx].progress = progress > 100 ? 100 : progress;
                                result[idx].progress_status =
                                    result[idx].on_schedule === "0" ?
                                        "Inactive"
                                        : result[idx].on_schedule === "1" && result[idx].progress === 0 ?
                                            "Active"
                                            : result[idx].on_schedule === "1" && result[idx].progress < 100 ?
                                                "Progress"
                                                : "Done";

                                temp.push(obj[idx2]);
                                // if (j == 0) {
                                //     console.log(j, temp[j].is_read, obj[idx2].on_schedule, obj[idx2].access)
                                // } else {
                                //     // console.log(j, temp[j--].is_read, obj[idx2].on_schedule, obj[idx2].access)
                                // }
                            }
                        }
                    });
                    result[idx].agenda = temp;
                    //resultAgenda[i].agenda = temp;
                }
            });
            return resolve(result);
        } catch (error) {
            console.error({ error: true, data: error, filename: __filename, function: "getAgendaCoursePlan" }, before)
            return resolve([]);
        }
    });
}

exports.planByTrainingUserId = async (req, res) => {
    try {
        let arrCourseId = [];
        let dataUserTraining = await moduleDB.getUserTraining({ idTrainingUser: req.params.training_user_id });
        dataUserTraining = dataUserTraining.length ? dataUserTraining : [{ id: 0 }];

        const email = dataUserTraining[0].email;
        const dataUser = await moduleDB.getDataUser({ email: email });

        if (dataUser.length) {

            let progressPercent = 0;
            let finalProgress = 0;
            let progressUnassigned = 0;
            let dataUnassignedExam = [];
            let dataTrainingCourseCompany = await moduleDB.getTrainingCourseByTrainingUserId({ trainingUserId: req.params.training_user_id },req.headers);

            for (let index = 0; index < dataTrainingCourseCompany.length; index++) {

                const dataTraining = dataTrainingCourseCompany[index];

                if (dataTraining && dataTraining.require_course_id !== null && dataTraining.require_course_id > 0) {

                    const requireCheck = await moduleDB.getTrainingExamCheck({
                        email: email,
                        requireCourseId: dataTraining.require_course_id,
                    });

                    if (requireCheck[0].require_check === 0) {

                        const requiredCourse = await moduleDB.getRequiredCourse({
                            requireCourseId: dataTraining.require_course_id,
                        });

                        dataTrainingCourseCompany[index].on_schedule = '0';
                        dataTrainingCourseCompany[
                            index
                        ].message = `This course is require you to pass course '${requiredCourse[0].title}'`;
                    }
                }
                const minute = dataTraining.total_session_time % 60;
                const hour = (dataTraining.total_session_time - minute) / 60;
                const duration =
                    dataTraining.total_session_time > 0
                        ? (hour < 10 ? '0' : '') + hour.toString() + ':' + (minute < 10 ? '0' : '') + minute.toString()
                        : '-';
                dataTrainingCourseCompany[index].duration = duration;

                arrCourseId.push(dataTrainingCourseCompany[index].id);

                let dataCourseSessions = await moduleDB.getTrainingCourseSession({
                    email: email,
                    courseId: dataTraining.id,
                });

                dataCourseSessions.forEach((dataCourseSession) => {
                    const minute = dataCourseSession.time % 60;
                    const hour = (dataCourseSession.time - minute) / 60;
                    const duration =
                        dataCourseSession.time > 0
                            ? (hour < 10 ? '0' : '') +
                            hour.toString() +
                            ':' +
                            (minute < 10 ? '0' : '') +
                            minute.toString()
                            : '-';
                    dataCourseSession.duration = duration;
                });

                dataTrainingCourseCompany[index].session = dataCourseSessions;


                let dataCourseSubmission = await moduleDB.getTrainingCourseSubmission({
                    email: email,
                    courseId: dataTraining.id,
                });

                dataTrainingCourseCompany[index].submission = dataCourseSubmission || [];


                const detailLiveClass = await moduleDB.getLiveClass({
                    email: email,
                    userId: dataUser[0].user_id,
                    idTrainingCourse: dataTraining.id,
                });

                detailLiveClass.forEach(async (dataLiveClass) => {
                    const scoreLiveClass = await moduleDB.getScoreLiveClass({
                        idWebinar: dataLiveClass.id,
                        idUser: dataUser[0].user_id,
                    });

                    const scorePreTest = await moduleDB.getScoreWebinar({
                        idWebinar: dataLiveClass.id,
                        typeTest: 0,
                        idUser: dataUser[0].user_id,
                    });

                    const scorePostTest = await moduleDB.getScoreWebinar({
                        idWebinar: dataLiveClass.id,
                        typeTest: 1,
                        idUser: dataUser[0].user_id,
                    });

                    const timeLiveClassStart = new Date(dataLiveClass.start_time);
                    const timeLiveClassEnd = new Date(dataLiveClass.end_time);
                    const diff = Math.abs(timeLiveClassEnd - timeLiveClassStart);
                    const diffHour = Math.floor((diff % 86400000) / 3600000);
                    const diffMin = Math.round(((diff % 86400000) % 3600000) / 60000);
                    const duration =
                        dataLiveClass.start_time && dataLiveClass.end_time && diff > 0
                            ? diffHour.toString().padStart(2, '0') + ':' + diffMin.toString().padStart(2, '0')
                            : '-';
                    dataLiveClass.duration = duration;
                    dataLiveClass.score = scoreLiveClass.length ? scoreLiveClass[0].score : '0';
                    dataLiveClass.scorePretest = scorePreTest;
                    dataLiveClass.scorePostTest = scorePostTest;
                });

                dataTrainingCourseCompany[index].liveclass = detailLiveClass;

                let dataAllQuiz = await moduleDB.getQuiz({
                    idTrainingUser: dataUserTraining[0].id,
                    idCourse: dataTraining.id,
                });

                dataAllQuiz.forEach((dataquiz) => {
                    const minute = dataquiz.time_limit % 60;
                    const hour = (dataquiz.time_limit - minute) / 60;
                    const duration =
                        dataquiz.time_limit > 0
                            ? (hour < 10 ? '0' : '') +
                            hour.toString() +
                            ':' +
                            (minute < 10 ? '0' : '') +
                            minute.toString()
                            : '-';
                    dataquiz.duration = duration;
                });

                dataTrainingCourseCompany[index].quiz = dataAllQuiz;

                let dataAllExam = await moduleDB.getExam({
                    idTrainingUser: dataUserTraining[0].id,
                    idCourse: dataTraining.id,
                });

                dataAllExam.forEach((dataexam) => {
                    const minute = dataexam.time_limit % 60;
                    const hour = (dataexam.time_limit - minute) / 60;
                    const duration =
                        dataexam.time_limit > 0
                            ? (hour < 10 ? '0' : '') +
                            hour.toString() +
                            ':' +
                            (minute < 10 ? '0' : '') +
                            minute.toString()
                            : '-';
                    dataexam.duration = duration;
                });

                dataTrainingCourseCompany[index].exam = dataAllExam;
                dataTrainingCourseCompany[index].agenda = [];
                // session material based on agenda
                let agenda = await getAgendaCourse(dataTraining.id);
                if (agenda) {
                    agenda.forEach((str, index) => {

                        let idx = dataCourseSessions.findIndex((str1) => { return (str.type == 3 && str.id == str1.id); });
                        if (idx > -1) {
                            str.time = dataCourseSessions[idx].time;
                            str.is_read = dataCourseSessions[idx].is_read;
                            str.media = dataCourseSessions[idx].media;
                            str.content = dataCourseSessions[idx].content;
                            str.sort = index;
                        } else {
                            str.media = [];
                            str.content = '';
                            str.sort = index;
                        }
                    });

                    dataCourseSessions = agenda.filter(x => x.label === 'Theory');
                }
                detailLiveClass.forEach((x, i) => {
                    let idx = agenda.findIndex((y) => y.id === x.id && y.type === '1' && (!y.is_mandatory || y.is_mandatory === null))
                    if (idx > -1) {
                        if (x.status === 3) {
                            detailLiveClass[i].is_read = 1;
                        }
                    }
                })

                // course progress
                const totalSession = dataCourseSessions.length;
                const readSession = dataCourseSessions.filter((coursesession) => coursesession.is_read === 1).length;
                const totalLiveclass = detailLiveClass.length;
                const readLiveclass = detailLiveClass.filter((dataliveclass) => dataliveclass.is_read === 1).length;
                const totalQuiz = dataAllQuiz.length;
                const readQuiz = dataAllQuiz.filter((dataquiz) => dataquiz.is_read === 1).length;
                const totalExam = dataAllExam.length;
                const readExam = dataAllExam.filter((dataexam) => dataexam.is_read === 1).length;

                const total = agenda.length;
                const read = readSession + readLiveclass + readQuiz + readExam;
                const progress = Math.round((read / total) * 100);
                dataTrainingCourseCompany[index].progress = progress;
                progressPercent = progressPercent + progress;

                // progress_status
                dataTrainingCourseCompany[index].progress_status =
                    dataTraining.on_schedule === '0'
                        ? 'Inactive'
                        : dataTraining.on_schedule === '1' && dataTraining.progress === 0
                            ? 'Active'
                            : dataTraining.on_schedule === '1' && dataTraining.progress < 100
                                ? 'Progress'
                                : 'Done';

                finalProgress = progressPercent;
            }

            dataUnassignedExam = await moduleDB.getUnassignedExam({
                idTrainingUser: req.params.training_user_id,
                companyId: dataUser[0].company_id,
            });

            if (dataUnassignedExam.length) {
                let valueProgressUnassigned = 0;
                dataUnassignedExam.forEach((unassignedExam) => {
                    valueProgressUnassigned += (unassignedExam.is_read ? 100 : 0);
                });

                progressUnassigned = valueProgressUnassigned / dataUnassignedExam.length;
            }

            const totalCourse = (dataTrainingCourseCompany.length + (dataUnassignedExam.length ? 1 : 0))
            finalProgress = (finalProgress + progressUnassigned) / totalCourse;

            // AGENDA
            let resultAgenda = await getAgendaCoursePlan(dataTrainingCourseCompany, arrCourseId);
            dataTrainingCourseCompany = resultAgenda.length ? resultAgenda : dataTrainingCourseCompany;

            let listDataResult = [];
            if (dataTrainingCourseCompany.length) {
                dataTrainingCourseCompany.forEach((str) => {
                    if (str.agenda.length) {
                        str.agenda.forEach((str2) => {
                            if (str2.type.toLowerCase() === "exam" && str2.availableResult) {
                                listDataResult.push({
                                    resultId: str2.resultId,
                                    startTime: str2.start_time,
                                    submissionTime: str2.submission_time,
                                    assigneeId: str2.assignee_id,
                                    courseId: str2.course_id,
                                    examTitle: str2.title,
                                    courseTitle: str.title,
                                    type: str2.type.toLowerCase(),
                                    score: str2.score || '-',
                                    minScore: str2.minimum_score,
                                    timeLimit: str2.time_limit,
                                    duration: str2.duration,
                                    availableResult: str2.availableResult
                                });
                            }

                        })
                    }
                });
            }

            if (dataUnassignedExam.length > 0) {
                dataUnassignedExam.forEach((str2, i) => {
                    if (str2.type === "Exam" && str2.score !== null) {

                        listDataResult.push({
                            resultId: str2.resultId,
                            startTime: str2.start_time,
                            submissionTime: str2.submission_time,
                            assigneeId: str2.assignee_id,
                            courseId: str2.course_id,
                            examTitle: str2.title,
                            courseTitle: "Unassigned to course",
                            type: str2.type.toLowerCase(),
                            score: str2.score || '-',
                            minScore: str2.minimum_score,
                            timeLimit: str2.time_limit,
                            duration: null,
                            availableResult: true
                        });
                        dataUnassignedExam[i].availableResult = true
                    }
                });
            }

            res.json({
                error: false,
                result: { data: dataTrainingCourseCompany, listDataResult: listDataResult, unassigned: dataUnassignedExam, progress: Math.round(finalProgress), progressUnassigned: progressUnassigned },
            });

        } else {
            res.json({ error: true, result: 'No user' });
        }
    } catch (error) {
        res.json({ error: true, result: error.message });
    }
};

exports.detailPlan = async (req, res) => {
    const { email } = req.app.token;
    let arrCourseId = [];
    let dataUser = await db.query(`SELECT * FROM user WHERE email='${email}' AND status='active' LIMIT 1`);
    let dataUserTraining = await db.query(`SELECT * FROM training_user WHERE email='${email}' AND status='active' LIMIT 1`);
    dataUserTraining = dataUserTraining.length ? dataUserTraining : [{ id: 0 }];
    db.query(
        `SELECT c.*, GROUP_CONCAT(DISTINCT m.type) AS content, (SELECT COUNT(id) FROM training_course_session WHERE course_id=c.id) AS session_count,
        ((SELECT IF(SUM(time) IS NULL, 0, SUM(time)) FROM training_course_session WHERE course_id=c.id)+(SELECT IF(SUM(time_limit) IS NULL, 0, SUM(time_limit)) FROM training_exam WHERE course_id=c.id AND status='Active')+(SELECT IF(SUM(TIMESTAMPDIFF(MINUTE, start_time, end_time)) IS NULL, 0, SUM(TIMESTAMPDIFF(MINUTE, start_time, end_time))) FROM webinars WHERE training_course_id=c.id AND publish='1')) AS total_session_time,
        (IF(NOW() BETWEEN c.start_time AND c.end_time != '1' && c.scheduled = 1, '0', '1')) AS on_schedule, c2.title AS require_course_title, 'Course' AS type
        FROM training_course c
        LEFT JOIN training_course_session s ON s.course_id=c.id
        LEFT JOIN training_course_media m ON m.session_id=s.id
        LEFT JOIN training_course c2 ON c2.id = c.require_course_id
        WHERE c.status='Active'
            AND c.id=?
        GROUP BY c.id
        ORDER BY c.scheduled ASC, c.start_time ASC, require_course_id ASC`,
        [req.params.id],
        async (error, result) => {
            if (error) {
                return res.json({ error: true, result: error.message });
            };

            await Promise.all(result.map(async (item, index) => {
                if (item.require_course_id !== null && item.require_course_id > 0) {

                    let session = await db.query(
                        `SELECT s.*, IF(r.is_read IS NOT NULL, r.is_read, 0) AS is_read, 'Session' AS type
                        FROM training_course_session s
                        LEFT JOIN (SELECT r.* FROM training_course_session_read r JOIN training_user u ON u.id = r.training_user_id WHERE u.email=? GROUP BY r.session_id) r ON r.session_id = s.id
                        WHERE s.course_id=?
                        ORDER BY s.sort ASC`,
                        [email, item.require_course_id]);

                    let submission = await db.query(
                        `SELECT s.*, /*IF(r.is_read IS NULL, 0, r.is_read) AS is_read*/ r.is_read, r.submission_file, 'Submission' AS type, IF(r.created_at, TIMEDIFF(r.created_at, s.start_time), TIMEDIFF(s.start_time, s.end_time)) AS 'duration'
                            FROM training_course_submission s
                            LEFT JOIN (SELECT r.* FROM training_submission_answer r JOIN training_user u ON u.id = r.training_user_id WHERE u.email=? GROUP BY r.submission_id) r ON r.submission_id = s.id
                            WHERE s.course_id=?
                            ORDER BY s.sort ASC`,
                        [email, item.require_course_id]);

                    let liveclass = await db.query(
                        `SELECT w.*, IF((SELECT p.id FROM webinar_peserta p LEFT JOIN user u ON u.user_id = p.user_id WHERE u.email=? AND p.webinar_id=w.id) IS NOT NULL, 1, 0) AS is_invited, 'Live Class' AS type,
                        (IF((SELECT COUNT(p.id) AS hadir FROM webinar_peserta p WHERE p.webinar_id=w.id AND p.user_id=? AND p.status='2')>0 AND w.status=3,1,0)) AS is_read,
                        (IF(NOW() BETWEEN w.start_time AND w.end_time != '1', '0', '1')) AS on_schedule
                        FROM webinars w
                        WHERE w.training_course_id=? AND w.publish='1'
                        ORDER BY w.id DESC`,
                        [email, dataUser[0].user_id, item.require_course_id]);

                    let quiz = await db.query(
                        `SELECT e.*, (SELECT SUM(total) FROM training_exam_composition WHERE exam_id=e.id) AS number_of_question, l.name AS licenses_type,  IF(e.scheduled=1,'Yes','No') AS is_scheduled, IF(e.exam = 1, 'Exam', 'Quiz') AS type,
                        (IF((SELECT COUNT(r.id) AS hadir FROM training_exam_result r WHERE r.exam_id=e.id AND r.training_user_id=? AND r.assignee_id=a.id)>0,1,0)) AS is_read, a.id AS assignee_id,
                        (IF(NOW() BETWEEN e.start_time AND e.end_time != '1' && e.scheduled = 1, '0', '1')) AS on_schedule
                        FROM training_exam e
                        LEFT JOIN training_licenses_type l ON l.id = e.licenses_type_id
                        LEFT JOIN training_exam_assignee a ON a.exam_id = e.id
                        WHERE e.status='Active' AND e.exam=0 AND e.course_id=? AND a.training_user_id=?
                        ORDER BY e.created_at DESC;`,
                        [dataUserTraining[0].id, item.require_course_id, dataUserTraining[0].id]);
                    let exam = await db.query(
                        `SELECT e.*, (SELECT SUM(total) FROM training_exam_composition WHERE exam_id=e.id) AS number_of_question, l.name AS licenses_type,  IF(e.scheduled=1,'Yes','No') AS is_scheduled, IF(e.exam = 1, 'Exam', 'Quiz') AS type,
                        (
                            IF(
                                (
                                    SELECT COUNT(r.id) AS hadir FROM training_exam_result r WHERE r.exam_id=e.id AND r.training_user_id=? AND r.assignee_id=a.id and r.pass = 1 order by r.score DESC
                                )>0,1,0)
                        ) AS is_read, 
                        (
                            SELECT IF(r.score IS NULL,0,r.score) AS hadir FROM training_exam_result r WHERE r.exam_id=e.id AND r.training_user_id=? AND r.assignee_id=a.id order by r.created_at DESC LIMIT 1
                        ) AS current_score, 
                        a.id AS assignee_id,
                        (IF(NOW() BETWEEN e.start_time AND e.end_time != '1' && e.scheduled = 1, '0', '1')) AS on_schedule,
                        r.id AS result_id,
                        r.correct AS correct_answer,
                        r.total AS total_question
                        FROM training_exam e
                        LEFT JOIN training_licenses_type l ON l.id = e.licenses_type_id
                        LEFT JOIN training_exam_assignee a ON a.exam_id = e.id
                        LEFT JOIN (SELECT * FROM training_exam_result WHERE training_user_id=?) r ON r.exam_id=e.id
                        WHERE e.status='Active' AND e.exam=1 AND e.course_id=? AND a.training_user_id=?
                        ORDER BY e.created_at DESC;`,
                        [dataUserTraining[0].id, dataUserTraining[0].id, dataUserTraining[0].id, item.require_course_id, dataUserTraining[0].id]);

                    for (let i = 0; i < submission.length; i++) {
                        const submissionAnswer = await moduleDB.getTrainingSubmissionAnswerByIdSubmission({idSubmission: submission[i].id, idTrainingUser: dataUserTraining[0].id});
                        submission[i].answerMedia = submissionAnswer || [];
                    }

                    // session material based on agenda
                    let agenda = await getAgendaCourse(item.require_course_id);
                    if (agenda) {
                        agenda.forEach( async (str, index) => {

                            if(str.type === "4"){ //Submission
                                const submissionAnswer = await moduleDB.getTrainingSubmissionAnswerByIdSubmission({idSubmission: str.id, idTrainingUser: dataUserTraining[0].id});
                                str.duration =
                                str.answerMedia = submissionAnswer || [];
                            }

                            let idx = session.findIndex((str1) => { return (str.type == 3 && str.id == str1.id); });
                            if (idx > -1) {
                                str.time = session[idx].time;
                                str.is_read = session[idx].is_read;
                                str.media = session[idx].media;
                                str.content = session[idx].content;
                                str.sort = index;
                            } else {
                                str.media = [];
                                str.content = '';
                                str.sort = index;
                            }
                        });

                        session = agenda.filter(x => x.label === 'Theory');
                    }

                    // course progress
                    let totalSession = session.length;
                    let readSession = session.filter(x => x.is_read === 1).length;
                    let readSubmission = submission.filter(x => x.is_read === 1).length;
                    let totalLiveclass = liveclass.length;
                    let readLiveclass = liveclass.filter(x => x.is_read === 1).length;
                    let totalQuiz = quiz.length;
                    let readQuiz = quiz.filter(x => x.is_read === 1).length;
                    let totalExam = exam.length;
                    let readExam = exam.filter(x => x.is_read === 1).length;

                    let total = agenda.length;
                    let read = readSession + readLiveclass + readQuiz + readExam + readSubmission;
                    let progress = Math.round(read / total * 100);

                    if (progress < 100) {
                        let reqCourse = await db.query(`SELECT title FROM training_course WHERE id=${item.require_course_id}`);
                        result[index].on_schedule = "0";
                        result[index].message = `This course is require you to pass all content in course '${reqCourse[0].title}'`
                    }
                    // let requireCheck = await db.query(`SELECT MIN(r.pass_exam) AS require_check
                    // FROM (
                    // SELECT e.title, (SELECT COUNT(r.id) FROM training_exam_result r JOIN training_user tu ON tu.id = r.training_user_id WHERE r.exam_id=e.id AND r.pass=1 AND tu.email='${email}') AS pass_exam
                    // FROM training_exam e
                    // WHERE e.course_id = ${item.require_course_id} AND e.exam=1
                    // ) AS r`);
                    // if (requireCheck[0].require_check === 0) {
                    //     let reqCourse = await db.query(`SELECT title FROM training_course WHERE id=${item.require_course_id}`);
                    //     result[index].on_schedule = "0";
                    //     result[index].message = `This course is require you to pass course '${reqCourse[0].title}'`
                    // }
                }
                var m = item.total_session_time % 60;
                var h = (item.total_session_time - m) / 60;
                var duration = item.total_session_time > 0 ? (h < 10 ? "0" : "") + h.toString() + ":" + (m < 10 ? "0" : "") + m.toString() : '-';
                result[index].duration = duration;
                arrCourseId.push(result[index].id);
                let session = await db.query(
                    `SELECT s.*, IF(r.is_read IS NOT NULL, r.is_read, 0) AS is_read, 'Session' AS type
                    FROM training_course_session s
                    LEFT JOIN (SELECT r.* FROM training_course_session_read r JOIN training_user u ON u.id = r.training_user_id WHERE u.email=? GROUP BY r.session_id) r ON r.session_id = s.id
                    WHERE s.course_id=?
                    ORDER BY s.sort ASC`,
                    [email, item.id]);
                for (let i = 0; i < session.length; i++) {
                    let media = await db.query(`SELECT * FROM training_course_media WHERE session_id =?`, [session[i].id]);
                    session[i].media = media;
                }
                await Promise.all(session.map(async (row, i) => {
                    var m = row.time % 60;
                    var h = (row.time - m) / 60;
                    var duration = row.time > 0 ? (h < 10 ? "0" : "") + h.toString() + ":" + (m < 10 ? "0" : "") + m.toString() : '-';
                    row.duration = duration;
                }))
                result[index].session = session;

                //submission
                let submission = await db.query(
                    `SELECT s.*, /*IF(r.is_read IS NULL, 0, r.is_read) AS is_read*/ r.is_read, r.submission_file, 'Submission' AS type, IF(r.created_at, TIMEDIFF(r.created_at, s.start_time), TIMEDIFF(s.start_time, s.end_time)) AS 'duration'
                    FROM training_course_submission s
                    LEFT JOIN (SELECT r.* FROM training_submission_answer r JOIN training_user u ON u.id = r.training_user_id WHERE u.email=? GROUP BY r.submission_id) r ON r.submission_id = s.id
                    WHERE s.course_id=?
                    ORDER BY s.sort ASC`,
                    [email, item.id]);
                for (let i = 0; i < submission.length; i++) {
                    let media = await db.query(`SELECT * FROM training_course_media WHERE submission_id =?`, [submission[i].id]);
                    const submissionAnswer = await moduleDB.getTrainingSubmissionAnswerByIdSubmission({idSubmission: submission[i].id, idTrainingUser: dataUserTraining[0].id});
                    submission[i].answerMedia = submissionAnswer || [];

                    submission[i].media = media;
                }
                // await Promise.all(submission.map(async (row, i) => {
                    // var m = row.duration % 60;
                    // var h = (row.duration - m) / 60;
                    // var duration = row.duration > 0 ? (h < 10 ? "0" : "") + h.toString() + ":" + (m < 10 ? "0" : "") + m.toString() : '-';
                    // row.duration = duration;
                // }))
                result[index].submission = submission;

                let liveclass = await db.query(
                    `SELECT w.*, IF((SELECT p.id FROM webinar_peserta p LEFT JOIN user u ON u.user_id = p.user_id WHERE u.email=? AND p.webinar_id=w.id) IS NOT NULL, 1, 0) AS is_invited, 'Live Class' AS type,
                    (IF((SELECT COUNT(p.id) AS hadir FROM webinar_peserta p WHERE p.webinar_id=w.id AND p.user_id=? AND p.status='2')>0 AND w.status=3,1,0)) AS is_read,
                    (IF(NOW() BETWEEN w.start_time AND w.end_time != '1', '0', '1')) AS on_schedule
                    FROM webinars w
                    WHERE w.training_course_id=? AND w.publish='1'
                    ORDER BY w.id DESC`,
                    [email, dataUser[0].user_id, item.id]);
                await Promise.all(liveclass.map(async (row, i) => {
                    liveclass[i].peserta_speaker = [];
                    liveclass[i].guest_speaker = [];

                    let moderator = await db.query(`SELECT user_id, name FROM user WHERE user_id IN (${liveclass[i].moderator})`);
                    liveclass[i].moderator = moderator.length != 0 ? moderator : { user_id: liveclass[i].moderator, name: "Anonymous" };

                    let sekretaris = await db.query(`SELECT user_id, name FROM user WHERE user_id IN (${liveclass[i].sekretaris})`);
                    liveclass[i].sekretaris = sekretaris.length != 0 ? sekretaris : { user_id: liveclass[i].sekretaris, name: "Anonymous" };

                    //let pembicara = await db.query(`SELECT user_id, name FROM user WHERE user_id IN (${liveclass[i].pembicara})`);
                    //liveclass[i].pembicara = pembicara.length != 0 ? pembicara : { user_id: liveclass[i].pembicara, name: "Anonymous" };

                    let split = liveclass[i].pembicara.split(",");

                    let tmp = { object: [], user_id: [], guest: [] }
                    for (let j = 0; j < split.length; j++) {
                        if (split[j] !== "") {
                            if (isNaN(Number(split[j]))) {
                                tmp.guest.push(split[j]);
                                tmp.object.push({ user_id: split[j], name: "Anonymous", type: "guest", email: null, phone: null });
                            } else {
                                tmp.user_id.push(split[j]);
                                tmp.object.push({ user_id: split[j], name: "Anonymous", type: "user", email: null, phone: null });
                            }
                        }
                    }

                    if (split.length > 0 && tmp.object.length > 0) {

                        let queryString = '';
                        let params = [];
                        if (tmp.user_id.length > 0) {
                            queryString = `SELECT user_id, name, email,phone FROM user WHERE user_id IN(?);`;
                            params.push(tmp.user_id);
                        }
                        if (tmp.guest.length > 0) {
                            queryString = `${queryString}SELECT wt.voucher as user_id, wt.name ,wt.email,wt.phone from webinar_tamu wt where wt.voucher in(?);`;
                            params.push(tmp.guest);
                        }

                        let pembicara = await db.query(queryString, params);

                        if (pembicara && pembicara.length > 1 && tmp.guest.length > 0 && Array.isArray(pembicara[0])) {
                            pembicara = pembicara[0].concat(pembicara[1]);
                        }

                        try {
                            tmp.object.forEach((str, i) => {
                                let idx = pembicara.findIndex((check) => { return check.user_id == str.user_id });
                                if (idx > -1) {
                                    tmp.object[i].name = pembicara[idx].name;
                                    tmp.object[i].email = pembicara[idx].email;
                                    tmp.object[i].phone = pembicara[idx].phone;
                                }
                            });
                        } catch (e) {
                            // handle error undefined variable
                        }

                        // build peserta_speaker && guest_speaker
                        tmp.object.forEach((str) => {
                            if (str.type === "guest") {
                                liveclass[i].guest_speaker.push(str);
                            } else {
                                liveclass[i].peserta_speaker.push(str);
                            }
                        });
                        liveclass[i].pembicara = tmp.object;
                    } else {
                        liveclass[i].pembicara = [];
                    }


                    let owner = await db.query(`SELECT user_id, name FROM user WHERE user_id IN (${liveclass[i].owner})`);
                    liveclass[i].owner = owner.length != 0 ? owner : { user_id: liveclass[0].owner, name: "Anonymous" };

                    let peserta = await db.query(`SELECT w.*, u.name FROM webinar_peserta w JOIN user u ON u.user_id = w.user_id WHERE webinar_id = '${liveclass[i].id}'`)
                    liveclass[i].peserta = peserta;

                    let tamu = await db.query(`SELECT * FROM webinar_tamu WHERE webinar_id='${liveclass[i].id}'`);
                    liveclass[i].tamu = tamu;
                }))
                await Promise.all(liveclass.map(async (item, index) => {
                    let jamMl = new Date(item.start_time);
                    let jamSl = new Date(item.end_time);
                    let diff = Math.abs(jamSl - jamMl);
                    let diffHour = Math.floor((diff % 86400000) / 3600000);
                    let diffMin = Math.round(((diff % 86400000) % 3600000) / 60000);
                    let duration = item.start_time && item.end_time && diff > 0 ? diffHour.toString().padStart(2, "0") + ':' + diffMin.toString().padStart(2, "0") : '-';
                    item.duration = duration;
                    if (item.course_id !== 0 && item.course_id !== null) {
                        let schedule_course = await db.query(`SELECT c.require_course_id FROM training_course c WHERE c.id = ${item.training_course_id} LIMIT 1;`);
                        if (schedule_course[0].require_course_id !== 0 && schedule_course[0].require_course_id !== null) {
                            let requireCheck = await db.query(`SELECT MIN(r.pass_exam) AS require_check
                            FROM (
                            SELECT e.title, (SELECT COUNT(r.id) FROM training_exam_result r JOIN training_user tu ON tu.id = r.training_user_id WHERE r.exam_id=e.id AND r.pass=1 AND tu.email='${email}') AS pass_exam
                            FROM training_exam e
                            WHERE e.course_id = ${schedule_course[0].require_course_id} AND e.exam=1
                            ) AS r`);
                            if (requireCheck[0].require_check === 0) {
                                let reqCourse = await db.query(`SELECT title FROM training_course WHERE id=${schedule_course[0].require_course_id}`);
                                liveclass[index].on_schedule = "0"
                                liveclass[index].message = `This live class is require you to pass course '${reqCourse[0].title}'`
                            }
                        }
                    }
                }))
                result[index].liveclass = liveclass;

                let quiz = await db.query(
                    `SELECT e.*, (SELECT SUM(total) FROM training_exam_composition WHERE exam_id=e.id) AS number_of_question, l.name AS licenses_type,  IF(e.scheduled=1,'Yes','No') AS is_scheduled, IF(e.exam = 1, 'Exam', 'Quiz') AS type,
                    (IF((SELECT COUNT(r.id) AS hadir FROM training_exam_result r WHERE r.exam_id=e.id AND r.training_user_id=? AND r.assignee_id=a.id)>0,1,0)) AS is_read, a.id AS assignee_id,
                    (IF(NOW() BETWEEN e.start_time AND e.end_time != '1' && e.scheduled = 1, '0', '1')) AS on_schedule
                    FROM training_exam e
                    LEFT JOIN training_licenses_type l ON l.id = e.licenses_type_id
                    LEFT JOIN training_exam_assignee a ON a.exam_id = e.id
                    WHERE e.status='Active' AND e.exam=0 AND e.course_id=? AND a.training_user_id=?
                    ORDER BY e.created_at DESC;`,
                    [dataUserTraining[0].id, item.id, dataUserTraining[0].id]);
                await Promise.all(quiz.map(async (item, index) => {
                    var m = item.time_limit % 60;
                    var h = (item.time_limit - m) / 60;
                    var duration = item.time_limit > 0 ? (h < 10 ? "0" : "") + h.toString() + ":" + (m < 10 ? "0" : "") + m.toString() : '-';
                    item.duration = duration;
                    if (item.course_id !== 0 && item.course_id !== null) {
                        let schedule_course = await db.query(`SELECT c.require_course_id FROM training_course c WHERE c.id = ${item.course_id} LIMIT 1;`);
                        if (schedule_course[0].require_course_id !== 0 && schedule_course[0].require_course_id !== null) {
                            let requireCheck = await db.query(`SELECT MIN(r.pass_exam) AS require_check
                            FROM (
                            SELECT e.title, (SELECT COUNT(r.id) FROM training_exam_result r JOIN training_user tu ON tu.id = r.training_user_id WHERE r.exam_id=e.id AND r.pass=1 AND tu.email='${email}') AS pass_exam
                            FROM training_exam e
                            WHERE e.course_id = ${schedule_course[0].require_course_id} AND e.exam=1
                            ) AS r`);
                            if (requireCheck[0].require_check === 0) {
                                let reqCourse = await db.query(`SELECT title FROM training_course WHERE id=${schedule_course[0].require_course_id}`);
                                quiz[index].on_schedule = "0"
                                quiz[index].message = `This quiz is require you to pass course '${reqCourse[0].title}'`
                            }
                        }

                        if (quiz[index].repeatable > 0) {
                            quiz[index].is_read = 0;
                            quiz[index].on_schedule = "1";
                        }
                    }
                    if (item.repeatable === 1) {
                        if (item.scheduled === 0) {
                            quiz[index].on_schedule = "1";
                            quiz[index].is_read = 0;
                        }
                        else {
                            if (item.start_time <= new Date() && item.end_time >= new Date()) {
                                quiz[index].on_schedule = "1";
                                quiz[index].is_read = 0;
                            }
                        }
                    }
                }))
                result[index].quiz = quiz;

                let exam = await db.query(
                    `SELECT e.*, (SELECT SUM(total) FROM training_exam_composition WHERE exam_id=e.id) AS number_of_question, l.name AS licenses_type,  IF(e.scheduled=1,'Yes','No') AS is_scheduled, IF(e.exam = 1, 'Exam', 'Quiz') AS type,
                    (
                        IF(
                            (
                                SELECT COUNT(r.id) AS hadir FROM training_exam_result r WHERE r.exam_id=e.id AND r.training_user_id=? AND r.assignee_id=a.id and r.pass = 1 order by r.score DESC
                            )>0,1,0)
                    ) AS is_read, 
                    (
                        IF(
                            (
                                SELECT COUNT(r.id) AS hadir FROM training_exam_result r WHERE r.exam_id=e.id AND r.training_user_id='${dataUserTraining[0].id}' AND r.assignee_id=a.id order by r.score DESC
                            )>0,1,0)
                    ) AS is_read_repeat,
                    (
                        SELECT IF(r.score IS NULL,0,r.score) AS hadir FROM training_exam_result r WHERE r.exam_id=e.id AND r.training_user_id=? AND r.assignee_id=a.id order by r.created_at DESC LIMIT 1
                    ) AS current_score, 
                    a.id AS assignee_id,
                    (IF(NOW() BETWEEN e.start_time AND e.end_time != '1' && e.scheduled = 1, '0', '1')) AS on_schedule,
                    r.id AS result_id,
                    r.correct AS correct_answer,
                    r.total AS total_question
                    FROM training_exam e
                    LEFT JOIN training_licenses_type l ON l.id = e.licenses_type_id
                    LEFT JOIN training_exam_assignee a ON a.exam_id = e.id
                    LEFT JOIN (SELECT * FROM training_exam_result WHERE training_user_id=?) r ON r.exam_id=e.id
                    WHERE e.status='Active' AND e.exam=1 AND e.course_id=? AND a.training_user_id=?
                    ORDER BY e.created_at DESC;`,
                    [dataUserTraining[0].id, dataUserTraining[0].id, dataUserTraining[0].id, item.id, dataUserTraining[0].id]);
                await Promise.all(exam.map(async (item, index) => {
                    var m = item.time_limit % 60;
                    var h = (item.time_limit - m) / 60;
                    var duration = item.time_limit > 0 ? (h < 10 ? "0" : "") + h.toString() + ":" + (m < 10 ? "0" : "") + m.toString() : '-';
                    item.duration = duration;
                    if (item.course_id !== 0 && item.course_id !== null) {
                        let schedule_course = await db.query(`SELECT c.require_course_id FROM training_course c WHERE c.id = ${item.course_id} LIMIT 1;`);
                        if (schedule_course[0].require_course_id !== 0 && schedule_course[0].require_course_id !== null) {
                            let requireCheck = await db.query(`SELECT MIN(r.pass_exam) AS require_check
                            FROM (
                            SELECT e.title, (SELECT COUNT(r.id) FROM training_exam_result r JOIN training_user tu ON tu.id = r.training_user_id WHERE r.exam_id=e.id AND r.pass=1 AND tu.email='${email}') AS pass_exam
                            FROM training_exam e
                            WHERE e.course_id = ${schedule_course[0].require_course_id} AND e.exam=1
                            ) AS r`);
                            if (requireCheck[0].require_check === 0) {
                                let reqCourse = await db.query(`SELECT title FROM training_course WHERE id=${schedule_course[0].require_course_id}`);
                                exam[index].on_schedule = "0"
                                exam[index].message = `This exam is require you to pass course '${reqCourse[0].title}'`
                            }
                        }
                        if (exam[index].repeatable > 0) {
                            //exam[index].is_read = 0;
                            if (exam[index].scheduled === 0) {
                                // if (exam[index].is_read < 1) {
                                //     exam[index].current_score = 0
                                // }
                                if (exam[index].is_read_repeat > 0) {
                                    exam[index].is_read = 1
                                } else {
                                    exam[index].is_read = 0
                                }
                                exam[index].on_schedule = "1";
                            }
                            else {
                                if (exam[index].start_time <= new Date() && exam[index].end_time >= new Date()) {
                                    if (exam[index].is_read < 1) {
                                        exam[index].current_score = 0
                                    }
                                    if (exam[index].is_read_repeat > 0) {
                                        exam[index].is_read = 1
                                    } else {
                                        exam[index].is_read = 0
                                    }
                                    exam[index].on_schedule = "1";
                                } else {
                                    if (exam[index].is_read_repeat > 0) {
                                        exam[index].is_read = 1
                                    } else {
                                        exam[index].is_read = 0
                                    }
                                }
                            }
                        }
                        else {
                            exam[index].on_schedule = exam[index].result_id ? '0' : exam[index].on_schedule
                            exam[index].message = exam[index].result_id ? 'You have complete and not passed this exam' : ''
                        }
                    }
                    exam[index].current_score = exam[index].current_score ? exam[index].current_score : 0;
                }))
                result[index].exam = exam;

                // session material based on agenda
                let agenda = await getAgendaCourse(item.id);
                if (agenda) {
                    agenda.forEach( async (str, index) => {
                        if(str.type === "4"){ //Submission
                            const submissionAnswer = await moduleDB.getTrainingSubmissionAnswerByIdSubmission({idSubmission: str.id, idTrainingUser: dataUserTraining[0].id});
                            str.answerMedia = submissionAnswer || [];
                        }


                        let idx = session.findIndex((str1) => { return (str.type == 3 && str.id == str1.id); });
                        if (idx > -1) {
                            str.time = session[idx].time;
                            str.is_read = session[idx].is_read;
                            str.media = session[idx].media;
                            str.content = session[idx].content;
                            str.sort = index;
                        } else {
                            str.media = [];
                            str.content = '';
                            str.sort = index;
                        }
                    });

                    session = agenda.filter(x => x.label === 'Theory');
                }

                // course progress
                let totalSession = session.length;
                let readSession = session.filter(x => x.is_read === 1).length;
                let readSubmission = submission.filter(x => x.is_read === 1).length;
                let totalLiveclass = liveclass.length;
                let readLiveclass = liveclass.filter(x => x.is_read === 1).length;
                let totalQuiz = quiz.length;
                let readQuiz = quiz.filter(x => x.is_read === 1).length;
                let totalExam = exam.length;
                let readExam = exam.filter(x => x.is_read === 1).length;

                let total = agenda.length;
                let read = readSession + readLiveclass + readQuiz + readExam + readSubmission;
                let progress = Math.round(read / total * 100);
                result[index].progress = progress;

                // progress_status
                result[index].progress_status =
                    item.on_schedule === "0" ?
                        "Inactive"
                        : item.on_schedule === "1" && item.progress === 0 ?
                            "Active"
                            : item.on_schedule === "1" && item.progress < 100 ?
                                "Progress"
                                : "Done";
            }))

            // return console.log(result);
            // AGENDA
            let resultAgenda = await getAgendaCoursePlan(result, arrCourseId);
            result = resultAgenda.length ? resultAgenda : result;
            if(result[0].agenda){

                result[0].agenda.length && result[0].agenda.map((item, index)=>{
                    if (index>0){
                        let prev = result[0].agenda[index-1];
                        if (prev.type==='Exam' && prev.current_score < prev.minimum_score){
                            if(prev.repeatable == 1){
                                item.access=true;
                                item.on_schedule="1";
                                item.is_read = 1;
                                item.scheduled = 1;
                            }else{
                                item.access=false;
                                item.on_schedule="0";
                            }
                        }
                    }
                })
            }else{
                result[0].agenda = []
            }

            return res.json({ error: false, result: result[0] });
        }
    );
};


exports.browseUserByCompany = (req, res) => {
    db.query(
        `SELECT tu.*, tc.name AS company
        FROM training_user tu
        JOIN training_company tc ON tc.id = tu.training_company_id
        JOIN company c ON c.company_id = tc.company_id
        WHERE c.company_id=? AND tu.status='Active' AND level=? ORDER BY tu.created_at DESC;`,
        [req.params.id, req.params.level],
        async (error, results) => {
            if (error) {
                return res.json({ error: true, result: error.message });
            }
            else {
                let ress = results;
                await Promise.all(ress.map(async (x, i) => {
                    // START
                    let dataUserTraining = await db.query(`SELECT * FROM training_user WHERE id='${x.id}' AND status='active' LIMIT 1`);
                    dataUserTraining = dataUserTraining.length ? dataUserTraining : [{ id: 0 }];
                    const email = x.email;
                    let dataUser = await db.query(`SELECT * FROM user WHERE email='${email}' AND status='active' LIMIT 1`);
                    dataCourse = {};
                    if (dataUser.length) {
                        let result = await db.query(
                            `SELECT c.*, GROUP_CONCAT(DISTINCT m.type) AS content, (SELECT COUNT(id) FROM training_course_session WHERE course_id=c.id) AS session_count,
                            ((SELECT IF(SUM(time) IS NULL, 0, SUM(time)) FROM training_course_session WHERE course_id=c.id)+(SELECT IF(SUM(time_limit) IS NULL, 0, SUM(time_limit)) FROM training_exam WHERE course_id=c.id AND status='Active')+(SELECT IF(SUM(TIMESTAMPDIFF(MINUTE, start_time, end_time)) IS NULL, 0, SUM(TIMESTAMPDIFF(MINUTE, start_time, end_time))) FROM webinars WHERE training_course_id=c.id AND publish='1')) AS total_session_time,
                            (IF(NOW() BETWEEN c.start_time AND c.end_time != '1' && c.scheduled = 1, '0', '1')) AS on_schedule, c2.title AS require_course_title, 'Course' AS type
                            FROM training_course c
                            LEFT JOIN training_course_session s ON s.course_id=c.id
                            LEFT JOIN training_course_media m ON m.session_id=s.id
                            LEFT JOIN training_course c2 ON c2.id = c.require_course_id
                            WHERE c.company_id=?
                                AND c.status='Active'
                            GROUP BY c.id
                            ORDER BY c.scheduled ASC, c.start_time ASC, require_course_id ASC`,
                            [dataUser[0].company_id]);
                        let progressPercent = 0;
                        let jumlahProgress = 0;
                        await Promise.all(result.map(async (item, index) => {
                            if (item.require_course_id !== null && item.require_course_id > 0) {
                                let requireCheck = await db.query(`SELECT MIN(r.pass_exam) AS require_check
                                FROM (
                                SELECT e.title, (SELECT COUNT(r.id) FROM training_exam_result r JOIN training_user tu ON tu.id = r.training_user_id WHERE r.exam_id=e.id AND r.pass=1 AND tu.email='${email}') AS pass_exam
                                FROM training_exam e
                                WHERE e.course_id = ${item.require_course_id} AND e.exam=1
                                ) AS r`);
                                if (requireCheck[0].require_check === 0) {
                                    let reqCourse = await db.query(`SELECT title FROM training_course WHERE id=${item.require_course_id}`);
                                    result[index].on_schedule = "0";
                                    result[index].message = `This course is require you to pass course '${reqCourse[0].title}'`
                                }
                            }
                            let session = await db.query(
                                `SELECT s.*, IF(r.is_read IS NOT NULL, r.is_read, 0) AS is_read, 'Session' AS type
                                FROM training_course_session s
                                LEFT JOIN (SELECT r.* FROM training_course_session_read r JOIN training_user u ON u.id = r.training_user_id WHERE u.email=? GROUP BY r.session_id) r ON r.session_id = s.id
                                WHERE s.course_id=?
                                ORDER BY s.sort ASC`,
                                [email, item.id]);
                            result[index].session = session;

                            let liveclass = await db.query(
                                `SELECT w.*, IF((SELECT p.id FROM webinar_peserta p LEFT JOIN user u ON u.user_id = p.user_id WHERE u.email=? AND p.webinar_id=w.id) IS NOT NULL, 1, 0) AS is_invited, 'Live Class' AS type,
                                (IF((SELECT COUNT(p.id) AS hadir FROM webinar_peserta p WHERE p.webinar_id=w.id AND p.user_id=? AND p.status='2')>0,1,0)) AS is_read
                                FROM webinars w
                                WHERE w.training_course_id=? AND w.publish='1'
                                ORDER BY w.id DESC`,
                                [email, dataUser[0].user_id, item.id]);
                            result[index].liveclass = liveclass;

                            let sql = `SELECT e.*, (SELECT SUM(total) FROM training_exam_composition WHERE exam_id=e.id) AS number_of_question, l.name AS licenses_type,  IF(e.scheduled=1,'Yes','No') AS is_scheduled, IF(e.exam = 1, 'Exam', 'Quiz') AS type,
                            (IF((SELECT COUNT(r.id) AS hadir FROM training_exam_result r WHERE r.exam_id=e.id AND r.training_user_id=? AND r.assignee_id=a.id)>0,1,0)) AS is_read, a.id AS assignee_id
                            FROM training_exam e
                            LEFT JOIN training_licenses_type l ON l.id = e.licenses_type_id
                            LEFT JOIN training_exam_assignee a ON a.exam_id = e.id
                            WHERE e.status='Active' AND e.exam=0 AND e.course_id=? AND a.training_user_id=?
                            ORDER BY e.created_at DESC;`
                            let sqlAdmin = `SELECT e.*, (SELECT SUM(total) FROM training_exam_composition WHERE exam_id=e.id) AS number_of_question, l.name AS licenses_type,  IF(e.scheduled=1,'Yes','No') AS is_scheduled, IF(e.exam = 1, 'Exam', 'Quiz') AS type,
                            (IF((SELECT COUNT(r.id) AS hadir FROM training_exam_result r WHERE r.exam_id=e.id AND r.training_user_id=?)>0,1,0)) AS is_read
                            FROM training_exam e
                            LEFT JOIN training_licenses_type l ON l.id = e.licenses_type_id
                            WHERE e.status='Active' AND e.exam=0 AND e.course_id=?
                            ORDER BY e.created_at DESC;`
                            let quiz = await db.query(dataUserTraining[0].id === 0 ? sqlAdmin : sql, [dataUserTraining[0].id, item.id, dataUserTraining[0].id]);
                            result[index].quiz = quiz;

                            let sql2 = `SELECT e.*, (SELECT SUM(total) FROM training_exam_composition WHERE exam_id=e.id) AS number_of_question, l.name AS licenses_type,  IF(e.scheduled=1,'Yes','No') AS is_scheduled, IF(e.exam = 1, 'Exam', 'Quiz') AS type,
                            (IF((SELECT COUNT(r.id) AS hadir FROM training_exam_result r WHERE r.exam_id=e.id AND r.training_user_id=? AND r.assignee_id=a.id)>0,1,0)) AS is_read, a.id AS assignee_id
                            FROM training_exam e
                            LEFT JOIN training_licenses_type l ON l.id = e.licenses_type_id
                            LEFT JOIN training_exam_assignee a ON a.exam_id = e.id
                            WHERE e.status='Active' AND e.exam=1 AND e.course_id=? AND a.training_user_id=?
                            ORDER BY e.created_at DESC;`
                            let sql2Admin = `SELECT e.*, (SELECT SUM(total) FROM training_exam_composition WHERE exam_id=e.id) AS number_of_question, l.name AS licenses_type,  IF(e.scheduled=1,'Yes','No') AS is_scheduled, IF(e.exam = 1, 'Exam', 'Quiz') AS type,
                            (IF((SELECT COUNT(r.id) AS hadir FROM training_exam_result r WHERE r.exam_id=e.id AND r.training_user_id=?)>0,1,0)) AS is_read
                            FROM training_exam e
                            LEFT JOIN training_licenses_type l ON l.id = e.licenses_type_id
                            WHERE e.status='Active' AND e.exam=1 AND e.course_id=?
                            ORDER BY e.created_at DESC;`
                            let exam = await db.query(dataUserTraining[0].id === 0 ? sql2Admin : sql2, [dataUserTraining[0].id, item.id, dataUserTraining[0].id]);
                            result[index].exam = exam;

                            // session material based on agenda
                            let agenda = await getAgendaCourse(item.id);
                            if (agenda) {
                                agenda.forEach((str, index) => {

                                    let idx = session.findIndex((str1) => { return (str.type == 3 && str.id == str1.id); });
                                    if (idx > -1) {
                                        str.time = session[idx].time;
                                        str.is_read = session[idx].is_read;
                                        str.media = session[idx].media;
                                        str.content = session[idx].content;
                                        str.sort = index;
                                    } else {
                                        str.media = [];
                                        str.content = '';
                                        str.sort = index;
                                    }
                                });

                                session = agenda.filter(x => x.label === 'Theory');
                            }

                            // course progress
                            let totalSession = session.length;
                            let readSession = session.filter(x => x.is_read === 1).length;
                            let totalLiveclass = liveclass.length;
                            let readLiveclass = liveclass.filter(x => x.is_read === 1).length;
                            let totalQuiz = quiz.length;
                            let readQuiz = quiz.filter(x => x.is_read === 1).length;
                            let totalExam = exam.length;
                            let readExam = exam.filter(x => x.is_read === 1).length;

                            let total = agenda.length;
                            let read = readSession + readLiveclass + readQuiz + readExam;
                            let progress = Math.round(read / total * 100);
                            result[index].progress = progress;
                            progressPercent = progressPercent + progress;
                        }))

                        let unassignedExam = await db.query(
                            `SELECT e.*, (SELECT SUM(total) FROM training_exam_composition WHERE exam_id=e.id) AS number_of_question, l.name AS licenses_type,  IF(e.scheduled=1,'Yes','No') AS is_scheduled, IF(e.exam = 1, 'Exam', 'Quiz') AS type,
                            (IF((SELECT COUNT(r.id) AS hadir FROM training_exam_result r WHERE r.exam_id=e.id AND r.training_user_id=? AND r.assignee_id=a.id)>0,1,0)) AS is_read
                            FROM training_exam e
                            LEFT JOIN training_licenses_type l ON l.id = e.licenses_type_id
                            LEFT JOIN training_exam_assignee a ON a.exam_id = e.id
                            WHERE e.company_id=? AND e.status='Active' AND (e.course_id = 0 OR e.course_id IS NULL) AND a.training_user_id=?
                            ORDER BY e.created_at DESC;`,
                            [x.id, dataUser[0].company_id, x.id]);
                        await Promise.all(unassignedExam.map((x) => {
                            progressPercent = progressPercent + (x.is_read ? 100 : 0);
                        }))
                        jumlahProgress = result.length + unassignedExam.length;
                        finalProgress = progressPercent / jumlahProgress;
                        results[i].progress = Math.round(finalProgress);
                    }
                    else {
                        results[i].progress = 0;
                    }
                    // END
                }))
                return res.json({ error: false, result: results });
            }
        }
    );
};

// saat ini belum terpakai, hold dulu.
exports.storeAssignmentCompany_byUserId = async (req, res) => {
    const createdBy = req.app.token.user_id;
    let form = req.body;
    let multiStore = [];
    let multiStoreParam = ``;
    let msg = { "error": true, "result": "Data invalid" };

    if (form.training_course_id && form.training_company_id && form.training_user_id.length > 0) {

        let checkCourseCompany = await db.query(` select training_company_id from training_course_company where training_company_id = ? and training_course_id = ? limit 1`,
            [form.training_company_id, form.training_course_id]);

        if (checkCourseCompany.length > 0) {
            let deleteCourseCompany = await db.query(` DELETE FROM training_course_company where training_company_id = ? and training_course_id = ? `,
                [form.training_company_id, form.training_course_id]);
        }

        for (let i = 0; i < form.training_user_id.length; i++) {

            multiStore.push(form.training_course_id, form.training_company_id, form.training_user_id[i], createdBy);

            if (i == form.training_user_id.length - 1) {
                multiStoreParam += `(?,?,?,?);`;
            } else {
                multiStoreParam += `(?,?,?,?),`;
            }
        }

        let store = await db.query(`INSERT INTO training_course_company(training_course_id, training_company_id, training_user_id, created_by) value${multiStoreParam}`, multiStore);

        if (store.affectedRows == form.training_user_id.length) {
            msg = { error: false, result: 'Store success' };
        } else {
            msg = { error: true, result: 'Store invalid' }
        }
    }

    res.json(msg);
}

exports.getAssignmentCompany_byUserId = async (req, res) => {
    let courseId = req.params.courseId;

    if (Number(courseId) > 0) {

        let query = ` 
        SELECT
        tcc.id, 
        tcc.training_course_id,
        tcc.training_company_id,
        tc.name AS 'company',
        tcc.created_at AS 'assignment_date', 
        if(tcc.active > 0,'Active','Inactive') AS 'assignment_status',
        tu.name,
        tu.image AS 'image_user',
        tu.identity,
        tu.identity_image,
        tu.license_number,
        tu.email,
        tu.phone
        from training_course_company tcc
        left join training_company tc on tc.id = tcc.training_company_id
        left join training_user tu on tu.id = tcc.training_user_id
        where 
        tcc.training_course_id = ? AND tcc.active = 1; `;

        let result = await db.query(query, [courseId]);

        res.json({ error: false, result: result });
    } else {
        res.json({ error: true, result: 'Data invalid' });
    }
}

// BY COMPANY_ID
exports.getAssignmentCompany_byCompanyId = async (req, res) => {
    let courseId = req.params.courseId;
    let msg = { error: true, result: 'Data invalid' };

    if (Number(courseId) > 0) {

        let query = ` 
        SELECT
        tcc.id, 
        tcc.training_course_id,
        tcc.training_company_id,
        tc.name AS 'company',
        tc.image AS 'image_company',
        tcc.created_at AS 'assignment_date', 
        if(tcc.active > 0,'Active','Inactive') AS 'assignment_status'
        from training_course_company tcc
        left join training_company tc on tc.id = tcc.training_company_id
        where 
        tcc.training_course_id = ? AND tcc.active = 1;`;

        let result = await db.query(query, [courseId]);
        msg = { error: false, result: result };
    }

    if (msg.result.length) {
        let results = msg.result;
        let checkUser = await getUserAssignCourse(msg.result, 1);
        //console.log(checkUser, 88876)
        msg.result = checkUser;
    }
    res.json(msg);
    db.getConnection((errCon, resCon) => { resCon.release() })
}

exports.getUserAssignCourse = getUserAssignCourse;

function getUserAssignCourse(arg, type) {
    return new Promise(async (resolve) => {
        try {
            for (let i = 0; i < arg.length; i++) {
                let params = { companyId: arg[i].training_company_id, courseId: arg[i].training_course_id };
                let getAgenda = await db.query(`select agenda from agenda_course where course_id='${params.courseId}'`);
                let sqlString = `
                    -- get user
                    SELECT tu.id, tu.name, tu.email, tu.image, tu.gender, 0 as assigned FROM training_user tu WHERE tu.training_company_id = '${params.companyId}' AND tu.status='Active' ORDER BY tu.id DESC;
                    -- get data exam
                    SELECT 
                    distinct
                    tea.exam_id,
                    tea.training_user_id,
                    te2.title,
                    te2.exam,
                    tea.id
                    FROM training_exam_assignee tea
                    LEFT JOIN training_exam te2 ON te2.id = tea.exam_id AND te2.exam=1
                    WHERE
                    tea.exam_id IN(SELECT te.id FROM training_exam te WHERE te.course_id = '${params.courseId}') AND
                    tea.training_user_id IN(SELECT tu.id FROM training_user tu WHERE tu.training_company_id = '${params.companyId}' AND tu.status='Active') AND
                    te2.title IS NOT NULL AND 
                    te2.exam IS NOT NULL
                    ORDER BY tea.created_at DESC;

                    -- liveclass 
                    SELECT 
                    wp.user_id,
                    u.email,
                    wp.webinar_id,
                    w.judul AS title,
                    wp.id
                    FROM webinars w
                    LEFT JOIN webinar_peserta wp ON wp.webinar_id = w.id
                    LEFT JOIN user u ON u.user_id = wp.user_id
                    WHERE 
                    w.training_course_id = '${params.courseId}' AND
                    wp.user_id IN(
                        SELECT u.user_id FROM training_user tu LEFT JOIN user u ON u.email = tu.email WHERE tu.training_company_id = '${params.companyId}'
                    )`;

                let result = await db.query(sqlString);
                let user = result[0];
                let exam = result[1];
                let liveclass = result[2];
                let session = [];
                let submission = [];
                if(getAgenda.length){
                    try {
                        let agenda = JSON.parse(getAgenda[0].agenda);
                        agenda.forEach((item)=>{
                            if(item.type === '3'){
                                session.push(item.id);
                            }else if(item.type === '4'){
                                submission.push(item.id);
                            }
                        })
                    } catch (error) {}
                }

                let tmp = []
                user.forEach((str, i) => { str.countAssignee=0; str.session=[]; str.submission=[]; str.webinar_peserta = []; str.training_exam_assignee = []; tmp.push(str); });
                user = tmp;

                if(session.length > 0){
                    let getSessionAssingee = await db.query(`select distinct training_user_id,session_id from training_course_session_read where session_id in(?)`,[session]);
                    if(getSessionAssingee.length > 0){
                        getSessionAssingee.forEach((item)=>{
                            let idx = user.findIndex((str)=>{ return str.id == item.training_user_id });
                            if(idx > -1){
                                user[idx].session.push(item.session_id);
                                user[idx].assigned = 1;
                                user[idx].countAssignee++;
                            }
                        });
                    }
                }

                if(submission.length > 0){
                    let getSubmissionAssingee = await db.query(`select distinct training_user_id,submission_id from training_submission_answer where submission_id in(?)`,[submission]);
                    if(getSubmissionAssingee.length > 0){
                        getSubmissionAssingee.forEach((item)=>{
                            let idx = user.findIndex((str)=>{ return str.id == item.training_user_id });
                            if(idx > -1){
                                user[idx].submission.push(item.submission_id);
                                user[idx].assigned = 1;
                                user[idx].countAssignee++;
                            }
                        });
                    }
                }

                exam.forEach((str) => {
                    let idx = user.findIndex((str2) => { return str2.id == str.training_user_id });
                    if (idx > -1) {
                        user[idx].training_exam_assignee.push(str.id);
                        user[idx].assigned = 1;
                        user[idx].countAssignee++;
                    }
                });

                liveclass.forEach((str) => {
                    let idx = user.findIndex((str2) => { return str2.email === str.email });
                    if (idx > -1) {
                        user[idx].webinar_peserta.push(str.id);
                        user[idx].assigned = 1;
                        user[idx].countAssignee++;
                    }
                });

                //type 1 berati ngambil semua type assgined
                //type 0 berati ngambil yg assigned nya 1
                if (type === 0) {
                    user = user.filter(val => val.assigned == 1);
                };

                user = user.map(str => {
                    if (arg[i]) {
                        str.company = arg[i].company;
                        return {
                            ...str
                        }
                    }
                })

                arg[i].training_user = user;
            }

            resolve(arg);

        } catch (error) {
            resolve([]);
        }
    });
}

exports.storeAssignmentCompany_byCompany_id = async (req, res) => {
    const createdBy = req.app.token.user_id;
    let form = req.body;
    let multiStore = [];
    let multiStoreParam = ``;
    let multiSelectParam = ``;
    let msg = { "error": true, "result": "Data Invalid" };

    try {
        if (form.training_course_id && form.training_company_id) {

            for (let i = 0; i < form.training_company_id.length; i++) {

                multiStore.push(form.training_course_id, form.training_company_id[i], createdBy);

                if (i == form.training_company_id.length - 1) {
                    multiStoreParam += `(?,?,?);`;
                    multiSelectParam += `?`;
                } else {
                    multiStoreParam += `(?,?,?),`;
                    multiSelectParam += `?,`;
                }
            }

            let concatquery = form.training_company_id.concat(form.training_course_id);

            let checkCourseCompany = await db.query(` select training_company_id from training_course_company where training_company_id IN(${multiSelectParam}) and training_course_id = ? limit 1`, concatquery);

            if (checkCourseCompany.length > 0) {
                await db.query(` DELETE FROM training_course_company where training_company_id IN(${multiSelectParam}) and training_course_id = ? `, concatquery);
            }

            let store = await db.query(`INSERT INTO training_course_company(training_course_id, training_company_id, created_by) value${multiStoreParam}`, multiStore);

            if (store.affectedRows == form.training_company_id.length) {
                msg.error = false;
                msg.result = 'Store success';
            } else {
                msg.result = 'Store invalid';
            }
        }
    } catch (e) {
        msg.error = true;
        msg.result = e;
    }

    res.json(msg);
    db.getConnection((errCon, resCon) => { resCon.release() })
}

// BY COURSE ID
exports.getAssignmentCompany_byCourseId = async (req, res) => {
    let companyId = req.params.companyId;
    let msg = { error: true, result: 'Data invalid' };

    if (Number(companyId) > 0) {

        let query = ` 
        SELECT
        tcc.id, 
        tcc.training_course_id,
        tcc.training_company_id,
        tc.name AS 'company',
        tc.image AS 'image_company',
        tcc.created_at AS 'assignment_date',
        tc2.title AS 'course_name',
        tc2.overview,
        tc2.image AS 'image_course',
        tc2.start_time,
        tc2.end_time,
        if(tcc.active > 0,'Active','Inactive') AS 'assignment_status'

        from training_course_company tcc
        left join training_course tc2 on tc2.id = tcc.training_course_id 
        left join training_company tc on tc.id = tcc.training_company_id
        where 

        tcc.training_company_id = ? AND tcc.active = 1;`;

        let result = await db.query(query, [companyId]);
        msg = { error: false, result: result };
    }

    res.json(msg);
    db.getConnection((errCon, resCon) => { resCon.release() })
}

exports.storeAssignmentCompany_byCourseId = async (req, res) => {
    const createdBy = req.app.token.user_id;
    let form = req.body;
    let multiStore = [];
    let multiStoreParam = ``;
    let multiSelectParam = ``;
    let msg = { "error": true, "result": "Data Invalid" };

    try {
        if (form.training_course_id.length > 0 && form.training_company_id) {

            for (let i = 0; i < form.training_course_id.length; i++) {

                multiStore.push(form.training_course_id[i], form.training_company_id, createdBy);

                if (i == form.training_course_id.length - 1) {
                    multiStoreParam += `(?,?,?);`;
                    multiSelectParam += `?`;
                } else {
                    multiStoreParam += `(?,?,?),`;
                    multiSelectParam += `?,`;
                }
            }

            let concatquery = form.training_course_id.concat(form.training_company_id);

            let checkCourseCompany = await db.query(` select training_company_id from training_course_company where training_course_id IN(${multiSelectParam}) and training_company_id = ? limit 1`, concatquery);

            if (checkCourseCompany.length > 0) {
                await db.query(` DELETE FROM training_course_company where training_course_id IN(${multiSelectParam}) and training_company_id = ? `, concatquery);
            }

            let store = await db.query(`INSERT INTO training_course_company(training_course_id, training_company_id, created_by) value${multiStoreParam}`, multiStore);

            if (store.affectedRows == form.training_course_id.length) {
                msg.error = false;
                msg.result = 'Store success';
            } else {
                msg.result = 'Store invalid';
            }
        }
    } catch (e) {
        msg.error = true;
        msg.result = e;
    }

    res.json(msg);
    db.getConnection((errCon, resCon) => { resCon.release() })
}

exports.deleteAssignmentCompany_byId = async (req, res) => {
    let listDelete = req.body.listDelete;
    let msg = '';
    let queryParam = '';
    let state = false;

    if (listDelete.length > 0) {

        for (let i = 0; i < listDelete.length; i++) {
            if (i == listDelete.length - 1) {
                queryParam += '?'
            } else {
                queryParam += '?,'
            }
        }
        listDelete.forEach(element => {
            if (Number(element) > 0) {

            } else {
                state = true;
            }
        });

        if (state) {
            return res.json({ error: true, result: 'Data invalid' });
        }

        let checkQuery = `SELECT id FROM training_course_company where id IN(${queryParam});`;
        let check = await db.query(checkQuery, listDelete);

        if (check.length > 0) {

            let delQuery = `UPDATE training_course_company SET active=0 where id IN(${queryParam})`;
            let deletes = await db.query(delQuery, listDelete);

            if (deletes) {
                msg = { error: false, result: 'Success' };
            } else {
                msg = { error: true, result: 'Cannot find data user' };
            }
        } else {
            msg = { error: true, result: 'Cannot find data user' };
        }
    } else {
        msg = { error: true, result: 'Data Invalid' };
    }

    db.getConnection((errCon, resCon) => { resCon.release() })
    res.json(msg);
}

exports.storeAssignmentUser = async (req, res) => {
    try {

        // let form = {
        //     created_by: 65,
        //     course_id: "73",
        //     training_user_id: [50]
        // }
        let confAxios = { headers: { "Authorization": req.headers.authorization } };

        let form = req.body;
        if (form.action !== 'add') {
            await this.deleteAssignmentUser(req, res);
        } else {

            let getUser = await db.query(`SELECT u.user_id from training_user tu left join user u on u.email = tu.email where tu.id IN(?);`, [form.training_user_id]);

            if (getUser.length) {
                let tmp = [];
                getUser.forEach((str) => {
                    tmp.push(str.user_id);
                });
                form.user_id = tmp;
            }

            let getAgenda = await db.query(`select * from agenda_course a where a.course_id ='${form.course_id}';`);
            let session = [];
            let submissions = [];
            
            try {
                let agenda = JSON.parse(getAgenda[0].agenda);
                agenda.forEach((str)=>{
                    if(str.type === '3'){
                        session.push(str.id);
                    }else if(str.type === '4'){
                        submissions.push(str.id);
                    }
                });
            } catch (error) {}

            let querySQL = `
            SELECT distinct te.id from training_exam te where te.course_id = '${form.course_id}' and te.status='Active';
            SELECT distinct w.id from webinars w where w.training_course_id = '${form.course_id}' and w.status < 3;
            `
            let resultData = await db.query(querySQL);
            //console.log('run',resultData)
            // liveclass
            if (resultData[1].length > 0) {

                let liveclass = resultData[1];
                for (let i = 0; i < liveclass.length; i++) {

                    for (let j = 0; j < form.user_id.length; j++) {

                        try {

                            let getUser = await db.query(`SELECT u.user_id from training_user tu left join user u on u.email = tu.email where tu.id = '${form.training_user_id[j]}' limit 1;`);

                            let liveclassData = {
                                webinarId: liveclass[i].id.toString(),
                                userId: form.user_id[j].toString()
                            }
                            let respData = await axios.post(`${env.APP_URL}/v2/webinar/peserta`, liveclassData, confAxios);
                            //console.log(respData.data, "9090 Webinars");
                        } catch (e) {

                        }
                    }
                }
            }
            // exam
            if (resultData[0].length > 0) {

                let exam = resultData[0];
                for (let i = 0; i < exam.length; i++) {

                    for (let j = 0; j < form.training_user_id.length; j++) {

                        let examData = {
                            created_by: form.created_by,
                            exam_id: exam[i].id.toString(),
                            training_user_id: form.training_user_id[j]
                        }
                        let respData = await axios.post(`${env.APP_URL}/v2/training/assign`, examData, confAxios);
                        // console.log(respData.data, "9090 EXAM");
                    }
                }
            }

            // submission / session
            for (let j = 0; j < form.training_user_id.length; j++) {
                // clear record user
                db.query(`
                    DELETE FROM training_course_session_read where session_id in(?) and training_user_id=?; 
                    DELETE FROM training_submission_answer where submission_id in(?) and training_user_id=?;`
                ,[session, form.training_user_id[j], submissions, form.training_user_id[j]]);


                //console.log('run',submissions,session)
                if(submissions.length > 0){
                    
                    await db.query(`INSERT INTO training_submission_answer(submission_id, training_user_id) select a.id as submission_id, ${form.training_user_id[j]} as training_user_id from training_course_submission a where a.id in(?);`,[submissions]);
                }

                if(session.length > 0){
                    await db.query(`INSERT INTO training_course_session_read(session_id, training_user_id) select a.id as session_id, ${form.training_user_id[j]} as training_user_id from training_course_session a where a.id in(?);`,[session]);
                }

                // let submissionData = {
                //     created_by: form.created_by,
                //     training_user_id: form.training_user_id[j],
                //     course_id: form.course_id,
                //     action: 'add'
                // }
                // console.log(submissionData);
                // let respData = await axios.post(`${env.APP_URL}/v2/training/submission/assign`, submissionData, confAxios);
                // console.log(respData.data, "9090 EXAM");
            }
            return res.json({ error: false, result: 'Success' });
        }
    } catch (e) {
        console.error({ error: true, filename: __filename, function: "storeAssignmentUser", message: e });
        return res.json({ error: true, result: e });
    }
}

exports.deleteAssignmentUser = async (req, res) => {
    try {

        // let form = {
        //     created_by: 65,
        //     course_id: "73",
        //     training_user_id: [],
        //    training_exam_assignee:[],
        //    webinar_peserta:[], 
        //     action: "add"/"delete"
        // }
        let confAxios = { headers: { "Authorization": req.headers.authorization } };
        let form = req.body;

        let getAgenda = await db.query(`select * from agenda_course a where a.course_id ='${form.course_id}';`);
        let session = [];
        let submissions = [];
        
        try {
            let agenda = JSON.parse(getAgenda[0].agenda);
            agenda.forEach((str)=>{
                if(str.type === '3'){
                    session.push(str.id);
                }else if(str.type === '4'){
                    submissions.push(str.id);
                }
            });
        } catch (error) {}

        // liveclass
        let querySQL = `
        SELECT distinct w.id from webinars w where w.training_course_id = '${form.course_id}' and w.status < 3;
        `
        let resultData = await db.query(querySQL);
        console.log('run',form)
        // liveclass
        if (resultData.length > 0) {

            let liveclass = resultData;
            for (let i = 0; i < liveclass.length; i++) {

                for (let j = 0; j < form.training_user_id.length; j++) {

                    try {

                        let getUser = await db.query(`SELECT u.user_id from training_user tu left join user u on u.email = tu.email where tu.id = '${form.training_user_id[j]}' limit 1;`);
                        db.query("DELETE FROM webinar_peserta where user_id=? and webinar_id=?", [getUser[0].user_id.toString(),liveclass[i].id.toString()])
                    } catch (e) {

                    }
                }
            }
        }
        // exam
        if (form.training_exam_assignee.length > 0) {

            let tmp2 = ``;
            for (let j = 0; j < form.training_exam_assignee.length; j++) {
                tmp2 += `id[]=${form.training_exam_assignee[j]}&`;
            }
            // console.log(`${env.APP_URL}/v2/training/bulk-assign?${tmp2}created_by=${form.created_by}`)
            let respData = await axios.delete(`${env.APP_URL}/v2/training/bulk-assign?${tmp2}created_by=${form.created_by}`, confAxios);
            //console.log(respData.data, "9090 EXAM");
        }

        // submission

        for (let j = 0; j < form.training_user_id.length; j++) {

            let submissionData = {
                created_by: form.created_by,
                training_user_id: form.training_user_id[j],
                course_id: form.course_id,
                action: 'delete'
            }
            let respData = await axios.post(`${env.APP_URL}/v2/training/submission/assign`, submissionData, confAxios);
            // console.log(respData.data, "9090 EXAM");
        }

        for (let j = 0; j < form.training_user_id.length; j++) {
            db.query(`
                DELETE FROM training_course_session_read where session_id in(?) and training_user_id=?; 
                DELETE FROM training_submission_answer where submission_id in(?) and training_user_id=?;`
            ,[session, form.training_user_id[j], submissions, form.training_user_id[j]]);
        }

        return res.json({ error: false, result: 'Success' });

    } catch (e) {
        console.error({ error: true, filename: __filename, function: "storeAssignmentUser", message: e });
        return res.json({ error: true, result: e });
    }
}

exports.retrieveReportCourseByIdCourse = async (req, res) => {
    try {
        const dtoDetailReadLiveClassByCourse = {
            idCourse: req.params.idCourse,
            idCompany: req.params.idCompany
        };
        const { email } = req.app.token;
        //Validate Request
        const retrieveValidRequest = await validatorRequest.validateReportCourseByIdCourse(dtoDetailReadLiveClassByCourse);
        const retrieveData = await moduleDB.getAgendaByCourseId(retrieveValidRequest);
        let detailCourse = {
            title: '',
            scheduled: false,
            startTime: null,
            endTime: null,
            totalSession: 0,
            totalTheory: 0,
            totalLiveClass: 0,
            totalExam: 0,
            totalSubmission: 0,
            listSessions: [],
            listParticipants: []
        };

        const retrieveTitleCourse = await moduleDB.getRequiredCourse({ requireCourseId: retrieveValidRequest.idCourse });
        detailCourse.title = retrieveTitleCourse.length ? retrieveTitleCourse[0].title : null;
        detailCourse.scheduled = retrieveTitleCourse.length ? (retrieveTitleCourse[0].scheduled === 1 ? true : false) : null;
        detailCourse.startTime = retrieveTitleCourse.length ? retrieveTitleCourse[0].start_time : null;
        detailCourse.endTime = retrieveTitleCourse.length ? retrieveTitleCourse[0].end_time : null;
        if (retrieveData.length) {
            retrieveData[0].agenda = JSON.parse(retrieveData[0].agenda) || [];
            if (retrieveData[0].agenda.length) {
                retrieveData[0].agenda.forEach(dataAgenda => {
                    if (dataAgenda.type.toString() === '1') {
                        detailCourse.totalLiveClass += 1;
                    } else if (dataAgenda.type.toString() === '2') {
                        detailCourse.totalExam += 1;
                    } else if (dataAgenda.type.toString() === '3') {
                        detailCourse.totalTheory += 1;
                    }
                    else if (dataAgenda.type.toString() === '4') {
                        detailCourse.totalSubmission += 1;
                    }
                })

                // GET Materi
                let queryString = `SELECT s.*, IF(r.is_read IS NOT NULL, r.is_read, 0) AS is_read 
                    FROM training_course_session s 
                    LEFT JOIN (SELECT r.* FROM training_course_session_read r JOIN training_user u ON u.id = r.training_user_id WHERE u.email=? GROUP BY r.session_id) r ON r.session_id = s.id
                    WHERE s.course_id=?
                    ORDER BY s.sort ASC`;

                let queryParams = [email, retrieveValidRequest.idCourse];

                let session = await db.query(queryString, queryParams);

                if (session.length) {
                    let getSessionId = [];

                    for (let i = 0; i < session.length; i++) {
                        getSessionId.push(session[i].id);
                    }

                    let media = await db.query(`SELECT * FROM training_course_media WHERE session_id IN(?)`, [getSessionId]);
                    let medias = {};
                    for (let i = 0; i < session.length; i++) {
                        if (!session[i].media) {
                            session[i].media = [];
                        }

                        session[i].media = media.filter((str) => str.session_id == session[i].id)
                    }
                }

                // GET Submission
                queryString = `SELECT s.*, r.submission_file, r.submission_text,score, 
                    r.created_at as 'answer_created_at',r.updated_at as 'answer_updated_at', 
                    4 as type , tu.name, tu.email , r.training_user_id ,
                    r.id as 'idResultSubmission'
                    FROM training_course_submission s 
                    LEFT JOIN training_submission_answer r ON r.submission_id = s.id
                    LEFT JOIN training_user tu on tu.id = r.training_user_id  
                    WHERE s.course_id=? and r.status = 'active' 
                    ORDER BY s.sort ASC`;

                queryParams = [retrieveValidRequest.idCourse];

                let submission = await db.query(queryString, queryParams);
                if (submission.length) {
                    let tmp = [];
                    let participant = {};
                    let attachments = {};
                    let jumlahSubmitParticipant = {};

                    let getIdSubmission = [];
                    submission.forEach((arg) => {
                        getIdSubmission.push(arg.id);
                    });

                    let media = await db.query(`SELECT * FROM training_course_media WHERE submission_id IN(?)`, [getIdSubmission]);
                    if (media.length) {
                        for (let i = 0; i < media.length; i++) {
                            if (!attachments[`media${media[i].submission_id}`]) {
                                attachments[`media${media[i].submission_id}`] = [];
                            }
                            attachments[`media${media[i].submission_id}`].push(media[i]);
                        }
                    }

                    for (let i = 0; i < submission.length; i++) {

                        if (!participant[`participants${submission[i].id}`]) {
                            participant[`participants${submission[i].id}`] = [];
                            jumlahSubmitParticipant[`countSubmit${submission[i].id}`] = 0;
                        }

                        participant[`participants${submission[i].id}`].push({
                            id: submission[i].idResultSubmission,
                            training_user_id: submission[i].training_user_id,
                            name: submission[i].name,
                            email: submission[i].email,
                            score: submission[i].score,
                            file: submission[i].submission_file ? JSON.parse(submission[i].submission_file) : [],
                            text: submission[i].submission_text
                        });

                        if (submission[i].submission_file) {
                            jumlahSubmitParticipant[`countSubmit${submission[i].id}`]++;
                        }
                    }
                    for (let i = 0; i < submission.length; i++) {
                        if (submission[i].submission_file) {

                            //let json_file = JSON.parse(submission[i].submission_file);
                            submission[i].attachments = attachments[`media${submission[i].id}`];

                            let index = tmp.findIndex((arg) => { return arg.id === submission[i].id });
                            if (index == -1) {

                                tmp.push({
                                    id: submission[i].id,
                                    type: 4,
                                    title: submission[i].title,
                                    content: submission[i].content,
                                    start_time: submission[i].start_time,
                                    end_time: submission[i].end_time,
                                    listParticipants: participant[`participants${submission[i].id}`],
                                    countSubmit: jumlahSubmitParticipant[`countSubmit${submission[i].id}`]
                                })
                            }
                        }
                    }

                    submission = tmp;
                }

                console.log(new Date(), "END build data participant submisssion");

                let agenda = await getAgendaCourse(retrieveValidRequest.idCourse);
                if (agenda) {
                    tmp = [];
                    agenda.forEach((str, index) => {

                        let idx = session.findIndex((str1) => { return (str.type == 3 && str.id == str1.id); });
                        if (idx > -1) {
                            str.time = session[idx].time;
                            str.is_read = session[idx].is_read;
                            str.media = session[idx].media;
                            str.content = session[idx].content;
                            str.sort = index;
                        } else {
                            if (str.type.toString() === '4') {
                                let index = submission.findIndex((item) => { return item.id == str.id });
                                if (index > -1) {
                                    str = submission[index];
                                }
                            } else {
                                str.media = [];
                                str.content = '';
                                str.sort = index;
                            }
                        }
                        tmp.push(str)
                    });

                    session = tmp;
                }
                detailCourse.listSessions = session;
                detailCourse.totalSession = session.length;
            }
        }

        console.log(new Date(), "END build agenda");

        let getListCompanyByIdCourse = await moduleDB.getListCompanyByCourseId(retrieveValidRequest);
        console.log(new Date(), "END getListCompanyByIdCourse");
        // NEW
        if (getListCompanyByIdCourse.length) {
            let getListAllTrainingUser = await getUserAssignCourseProgress(getListCompanyByIdCourse, 0, retrieveValidRequest.idCourse);
            detailCourse.listParticipants = getListAllTrainingUser;
        }
        /* OLD, (HOLD DULU)
        if (getListCompanyByIdCourse.length) {
            let getListAllTrainingUser = await getUserAssignCourse(getListCompanyByIdCourse, 0);
            console.log(new Date(), "END getUserAssignCourse");
            let listAllTrainingUser = [];
            if (getListAllTrainingUser.length) {
                for (let i = 0; i < getListAllTrainingUser.length; i++) {
                    let listTrainingUser = getListAllTrainingUser[i].training_user;
                    listAllTrainingUser = listAllTrainingUser.concat(listTrainingUser);
                }

                for (let j = 0; j < listAllTrainingUser.length; j++) {
                    let dataUser = listAllTrainingUser[j]
                    const detailUser = {
                        idCourse: retrieveValidRequest.idCourse,
                        idTrainingUser: dataUser.id,
                        email: dataUser.email
                    };

                    const retrieveProgressionCourse = await getProgressionCourse(detailUser, detailCourse.listSessions);
                    // console.log(retrieveProgressionCourse, 'hue')
                    dataUser.courseProgress = retrieveProgressionCourse.progress;
                    dataUser.currentState = retrieveProgressionCourse.currentState;
                }
                console.log(new Date(), "END getUserAssignCourse");
            }
            detailCourse.listParticipants = listAllTrainingUser;

            // getListCompanyByIdCourse = getListAllTrainingUser;
        }
        */

        res.json({ error: false, result: detailCourse })

    } catch (error) {
        console.log({ error: true, filename: __filename, message: error })
        res.json({ error: true, result: error.message })
    }
}

exports.getUserAssignCourseProgress = getUserAssignCourseProgress;

async function getUserAssignCourseProgress(arg, type, course_id) {
    return new Promise(async (resolve) => {
        try {
            let arrTrainingCompany = [];
            let sql = ``;
            let listParticipants = [];
            let arr = [];
            
            for (let i = 0; i < arg.length; i++) {
                arrTrainingCompany.push(arg[i].training_company_id);
            }

            let getAgenda = await db.query(`select agenda from agenda_course where course_id='${course_id}'`);
            let sqlLivecass = `
                
                SELECT
                w.id, w.judul as 'title',wp.user_id , tu.id as training_user_id,tu.name, tu.email, tu.training_company_id,IF((wl.action = 'join' AND wp.status = 2), 1,0) as is_read 
                FROM webinars w
                LEFT JOIN webinar_peserta wp ON wp.webinar_id = w.id AND wp.status = 2
                LEFT JOIN webinar_logs wl ON wl.webinar_id = wp.id AND wl.peserta_id = wp.user_id
                left join user u on u.user_id = wp.user_id
                left join training_user tu on lower(tu.email) = lower(u.email) and tu.training_company_id in(${arrTrainingCompany.toString()}) 
                WHERE w.training_course_id='${course_id}' AND lower(w.status) = 'active' and w.id in(?) and tu.training_company_id IS NOT NULL;
            `;
            let sqlExam = `
                
                SELECT 
                tea.id as 'assignee_id',
                te.id, te.title, IF(ter.pass > 0, 1,0) as is_read,
                tu.id as training_user_id,tu.name, tu.email, tu.training_company_id
                FROM training_exam te
                left join training_exam_assignee tea on tea.exam_id = te.id
                left join training_exam_result ter on ter.assignee_id = tea.id
                left join training_user tu on tu.id = tea.training_user_id and tu.training_company_id in(${arrTrainingCompany.toString()})
                WHERE te.id IN(?) and tu.training_company_id IS NOT NULL
                ORDER BY tea.training_user_id DESC;
            `;
            let sqlSubmission = `

                SELECT
                tcs.id , tcs.title,tsa.training_user_id, tsa.score, IF(tsa.score IS NULL, 0,1) as is_read ,
                tu.id as training_user_id, tu.name, tu.email, tu.training_company_id  
                FROM training_course_submission tcs
                LEFT JOIN training_submission_answer tsa ON tsa.submission_id = tcs.id 
                left join training_user tu on tu.id = tsa.training_user_id and tu.training_company_id in(${arrTrainingCompany.toString()})
                WHERE tcs.course_id=${course_id} AND lower(tsa.status) = 'active' and tcs.id in(?) and tu.training_company_id IS NOT NULL;
            `;
            let sqlSession = `

                SELECT 
                t.session_id as 'id', tcs.title, tu.id AS 'training_user_id', tu.name, tu.email, tu.training_company_id, t.is_read 
                FROM training_course_session_read t 
                left join training_course_session tcs on tcs.id = t.session_id
                LEFT JOIN training_user tu ON tu.id = t.training_user_id and tu.training_company_id in(${arrTrainingCompany.toString()})
                WHERE t.session_id in(?) and tu.training_company_id IS NOT NULL;
            `;
            let state = { liveclass:[], exam:[], submission:[], session:[] }
            if(getAgenda.length){

                getAgenda[0].agenda = getAgenda[0].agenda ? JSON.parse(getAgenda[0].agenda) : null;
                if(getAgenda[0].agenda){
                    
                    getAgenda[0].agenda.forEach((str)=>{ // get used data
                        // type 1=liveclass ,2= exam, 3=theory/sessions ,4= submission
                        str.sql = ``;
                        if(str.type == 1){
                            state.liveclass.push(str.id);
                        }else if(str.type == 2){
                            state.exam.push(str.id)
                        }
                        else if(str.type == 3){
                            state.session.push(str.id)
                        }
                        else if(str.type == 4){
                            state.session.push(str.id)
                        }
                    });

                    if(state.session.length > 0) sql += sqlSession.replace('?',state.session.toString());
                    if(state.submission.length > 0) sql += sqlSubmission.replace('?',state.submission.toString());
                    if(state.liveclass.length > 0) sql += sqlLivecass.replace('?',state.liveclass.toString());
                    if(state.exam.length > 0) sql += sqlExam.replace('?',state.exam.toString());

                    let result = await db.query(sql);
                    let persenSplit = 100 / getAgenda[0].agenda.length;

                    if(result.length){

                        for(let i=0; i < result.length; i++){
                            for(let j=0; j < result[i].length; j++){
                                arr.push(result[i][j]);
                            }   
                        }
                        
                        arr.forEach((str)=>{
                            let existUser = listParticipants.findIndex(list=> str.training_user_id == list.training_user_id);
                            let exist = arg.findIndex(list=> list.training_company_id == str.training_company_id);
                            if(existUser == -1){
                                listParticipants.push({
                                    id: str.training_user_id,
                                    training_user_id: str.training_user_id,
                                    name: str.name,
                                    email: str.email,
                                    company: arg[exist].company,
                                    training_company_id: str.training_company_id,
                                    courseProgress: 0,
                                    currentState: '',
                                    stepLength:0
                                })
                            }
                        });

                        // console.log(listParticipants.length,'============== 2892')

                        getAgenda[0].agenda.forEach((str,index)=>{

                            let step = arr.filter(list=> list.id == str.id);
                            if(step.length){
                                listParticipants.forEach((users)=>{
                                    
                                    let perUser = step.filter(list=> list.training_user_id == users.training_user_id);
                                    
                                    if(index == 0){
                                        users.currentState = step[0].title;
                                    }else{
                                        if(users.next) users.currentState = step[0].title;
                                    }

                                    if(perUser.length){

                                        let is_read = 0;
                                        perUser.forEach(users=> is_read += users.is_read);
                                        if(is_read > 0){
                                            users.stepLength += 1;
                                            if(users.stepLength == getAgenda[0].agenda.length){
                                                users.courseProgress = 100;
                                            }else{
                                                users.courseProgress += Math.floor(persenSplit);
                                            }
                                            users.next = true;
                                        }
                                    }
                                });
                            }
                        })   
                    }
                }
            }

            if(listParticipants.length) listParticipants = listParticipants.sort((a,b)=>b.courseProgress - a.courseProgress);
            resolve(listParticipants);
        } catch (error) {
            console.log(error)
            resolve([]);
        }
    });
}

/**
 * @param detailUser
 * idCourse
 * idTrainingUser
 * email
 */
async function getProgressionCourse(detailUser, stepAgenda) {
    const dataUser = await moduleDB.getDataUser({ email: detailUser.email });
    let finalProgress = 0;
    let currentStepAgenda = null;
    if (dataUser.length) {
        let progressPercent = 0;
        let dataTrainingCourseCompany = await moduleDB.getTrainingCourseByTrainingUserIdAndIdCourse(detailUser);
        for (let index = 0; index < dataTrainingCourseCompany.length; index++) {
            const dataTraining = dataTrainingCourseCompany[index];
            if (dataTraining && dataTraining.require_course_id !== null && dataTraining.require_course_id > 0) {
                const requireCheck = await moduleDB.getTrainingExamCheck({
                    email: detailUser.email,
                    requireCourseId: dataTraining.require_course_id,
                });
                if (requireCheck[0].require_check === 0) {
                    const requiredCourse = await moduleDB.getRequiredCourse({
                        requireCourseId: dataTraining.require_course_id,
                    });
                    dataTrainingCourseCompany[index].on_schedule = '0';
                    dataTrainingCourseCompany[
                        index
                    ].message = `This course is require you to pass course '${requiredCourse[0].title}'`;
                }
            }
            const minute = dataTraining.total_session_time % 60;
            const hour = (dataTraining.total_session_time - minute) / 60;
            const duration =
                dataTraining.total_session_time > 0
                    ? (hour < 10 ? '0' : '') + hour.toString() + ':' + (minute < 10 ? '0' : '') + minute.toString()
                    : '-';
            dataTrainingCourseCompany[index].duration = duration;

            let dataCourseSessions = await moduleDB.getTrainingCourseSession({
                email: detailUser.email,
                courseId: dataTraining.id,
            });

            dataCourseSessions.forEach((dataCourseSession) => {
                const minute = dataCourseSession.time % 60;
                const hour = (dataCourseSession.time - minute) / 60;
                const duration =
                    dataCourseSession.time > 0
                        ? (hour < 10 ? '0' : '') +
                        hour.toString() +
                        ':' +
                        (minute < 10 ? '0' : '') +
                        minute.toString()
                        : '-';
                dataCourseSession.duration = duration;
            });

            dataTrainingCourseCompany[index].session = dataCourseSessions;

            const detailLiveClass = await moduleDB.getLiveClass({
                email: detailUser.email,
                userId: dataUser[0].user_id,
                idTrainingCourse: dataTraining.id,
            });

            detailLiveClass.forEach(async (dataLiveClass) => {
                const scoreLiveClass = await moduleDB.getScoreLiveClass({
                    idWebinar: dataLiveClass.id,
                    idUser: dataUser[0].user_id
                });

                const scorePreTest = await moduleDB.getScoreWebinar({
                    idWebinar: dataLiveClass.id,
                    typeTest: 0,
                    idUser: dataUser[0].user_id
                });

                const scorePostTest = await moduleDB.getScoreWebinar({
                    idWebinar: dataLiveClass.id,
                    typeTest: 1,
                    idUser: dataUser[0].user_id
                });

                const timeLiveClassStart = new Date(dataLiveClass.start_time);
                const timeLiveClassEnd = new Date(dataLiveClass.end_time);
                const diff = Math.abs(timeLiveClassEnd - timeLiveClassStart);
                const diffHour = Math.floor((diff % 86400000) / 3600000);
                const diffMin = Math.round(((diff % 86400000) % 3600000) / 60000);
                const duration =
                    dataLiveClass.start_time && dataLiveClass.end_time && diff > 0
                        ? diffHour.toString().padStart(2, '0') + ':' + diffMin.toString().padStart(2, '0')
                        : '-';
                dataLiveClass.duration = duration;
                dataLiveClass.score = scoreLiveClass.length ? scoreLiveClass[0].score : '0';
                dataLiveClass.scorePretest = scorePreTest;
                dataLiveClass.scorePostTest = scorePostTest;
            });

            dataTrainingCourseCompany[index].liveclass = detailLiveClass;

            let dataAllQuiz = await moduleDB.getQuiz({
                idTrainingUser: detailUser.idTrainingUser,
                idCourse: dataTraining.id,
            });

            dataAllQuiz.forEach((dataquiz) => {
                const minute = dataquiz.time_limit % 60;
                const hour = (dataquiz.time_limit - minute) / 60;
                const duration =
                    dataquiz.time_limit > 0
                        ? (hour < 10 ? '0' : '') +
                        hour.toString() +
                        ':' +
                        (minute < 10 ? '0' : '') +
                        minute.toString()
                        : '-';
                dataquiz.duration = duration;
            });

            dataTrainingCourseCompany[index].quiz = dataAllQuiz;

            let dataAllExam = await moduleDB.getExam({
                idTrainingUser: detailUser.idTrainingUser,
                idCourse: dataTraining.id,
            });

            dataAllExam.forEach((dataexam) => {
                const minute = dataexam.time_limit % 60;
                const hour = (dataexam.time_limit - minute) / 60;
                const duration =
                    dataexam.time_limit > 0
                        ? (hour < 10 ? '0' : '') +
                        hour.toString() +
                        ':' +
                        (minute < 10 ? '0' : '') +
                        minute.toString()
                        : '-';
                dataexam.duration = duration;
            });

            dataTrainingCourseCompany[index].exam = dataAllExam;
            let agenda = await getAgendaCourse(dataTraining.id);

            detailLiveClass.forEach((x, i) => {
                let idx = agenda.findIndex((y) => y.id === x.id && y.type === '1' && (!y.is_mandatory || y.is_mandatory === null))
                if (idx > -1) {
                    if (x.status === 3) {
                        detailLiveClass[i].is_read = 1;
                    }
                }
            })

            // course progress
            const totalSession = dataCourseSessions.length;
            const readSession = dataCourseSessions.filter((coursesession) => coursesession.is_read === 1).length;
            const totalLiveclass = detailLiveClass.length;
            const readLiveclass = detailLiveClass.filter((dataliveclass) => dataliveclass.is_read === 1).length;
            const totalQuiz = dataAllQuiz.length;
            const readQuiz = dataAllQuiz.filter((dataquiz) => dataquiz.is_read === 1).length;
            const totalExam = dataAllExam.length;
            const readExam = dataAllExam.filter((dataexam) => dataexam.is_read === 1).length;

            const total = agenda.length;
            const read = readSession + readLiveclass + readQuiz + readExam;
            const progress = Math.round((read / total) * 100);
            dataTrainingCourseCompany[index].progress = progress;
            progressPercent = progressPercent + progress;

            // progress_status
            dataTrainingCourseCompany[index].progress_status =
                dataTraining.on_schedule === '0'
                    ? 'Inactive'
                    : dataTraining.on_schedule === '1' && dataTraining.progress === 0
                        ? 'Active'
                        : dataTraining.on_schedule === '1' && dataTraining.progress < 100
                            ? 'Progress'
                            : 'Done';
            finalProgress = progressPercent;

            for (let i = 0; i < stepAgenda.length; i++) {
                const session = stepAgenda[i];

                if (session.type == 2) {
                    //Exam
                    const isFound = dataAllExam.find(exam => exam.id == session.id);
                    if (isFound) {
                        currentStepAgenda = isFound.title;
                    }
                } else if (session.type == 1) {
                    //Liveclass
                    const isFound = dataCourseSessions.find(course => course.id == session.id);
                    if (isFound) {
                        currentStepAgenda = isFound.title;
                    }
                } else if (session.type == 3) {
                    //Theroy/Session
                    const isFound = detailLiveClass.find(liveclass => liveclass.id == session.id);
                    if (isFound) {
                        currentStepAgenda = isFound.title;
                    }
                }
            }
        }
    }

    return { progress: finalProgress, currentState: currentStepAgenda };
}


exports.sendCertificateCourse = (req, res) => {
    uploadSignature(req, res, err => {
            try {
                let signData = JSON.parse(req.body.sign);
        
                // map
                JSON.parse(req.body.peserta).map(async (elem, idx) => {
                    let logo = await db.query(
                        `SELECT logo FROM company WHERE company_id='${req.body.company_id}'`
                    );
        
                    let option = {
                        "format":"A4",
                        "type": "pdf",           // allowed file types: png, jpeg, pdf
                        "quality": "75",
                        "orientation": req.body.certificate_orientation == 'landscape' ? "landscape" : "portrait",
                    };
                    
                    let htmlString = await reportHtml.htmltCourse(                    
                        elem.name,
                        req.body.cert_title,
                        moment(new Date()).format("dddd, MMMM Do YYYY"),
                        req.files.signature,
                        signData,
                        req.files.cert_logo ? req.files.cert_logo[0] : logo[0].logo,
                        req.body.cert_subtitle,
                        req.body.cert_description,
                        req.body.cert_topic,
                        req.files.certificate_background ? req.files.certificate_background[0] :null,
                        req.body.certificate_orientation || 'potrait',
                        req.body.bodyNewPage || null,
                        req.files.image_left ? req.files.image_left[0] : null,
                        req.files.image_right ? req.files.image_right[0] : null,
                        req.body.cert_number || null
                        );
                    
                    let url = require('url');
                    let uri = url.pathToFileURL(path.resolve('./public/certcourse.html')).href;
                    let browser = null;
                    fs.writeFileSync('./public/certcourse.html',htmlString);
                    
                    try {
                        //LINUX
                        browser = await puppeteer.launch({
                            executablePath: '/usr/bin/chromium-browser',
                            headless: true, 
                            args: ['--no-sandbox', '--disable-setuid-sandbox']
                          })
                    } catch (error) {
                        // WIN10
                        browser = await puppeteer.launch({
                            headless: true, 
                            args: ['--no-sandbox', '--disable-setuid-sandbox']
                        })
                    }
                    const page = await browser.newPage();
                    const filename = `./public/course/sertifikat/${elem.email}-${Date.now()}.pdf`;
                    await page.goto(uri, {waitUntil: 'networkidle0'});
                    const pdf = await page.pdf({ 
                        format: 'A4',
                        landscape:req.body.certificate_orientation == 'landscape' ? true:false,
                        printBackground: true,
                        path: filename,
                        margin: {
                            top: "0px",
                            right: "0px",
                            bottom: "0px",
                            left: "0px"
                        }
                    });
                    
                    await browser.close();
                    fs.writeFileSync('./public/certcourse.html','');
                    let obj_mail = {
                        type: "sendmail_certificate_course",
                        peserta: elem.name,
                        from: env.MAIL_USER,
                        to: elem.email,
                        subject:
                            "Certificate of Participation : Course " + req.body.cert_topic,
                        text:
                            "Certificate of Participation : Course " + req.body.cert_topic,
                        attachments: [
                            {
                                filename: `certificate-${req.body.cert_topic}.pdf`,
                                path: filename,
                                contentType: "application/pdf",
                            },
                        ],
                        html: `
                            <b>Hello ${elem.name},</b><br><br>
                            Thank you for your participation in Course “${req.body.cert_topic}”. <br>
                            Please find attached file for your certificate of participation in the event mentioned above.`,
                    };

                    sendMailQueue.add(obj_mail, {
                        ...options_queue,
                        delay: options_queue.delay * idx,
                    });
                    
                    // pdf.create(
                    //         htmlString
                    //         ,option
                    //     )
                    //     .toFile(
                    //         `./public/course/sertifikat/${elem.email}.pdf`,
                    //         function (err, res) {
                    //             if (err) return console.log(err, "ERROR HTML-PDF");
                    //             return console.log('rees',res, htmlString);
                    //         //     let obj_mail = {
                    //         //         type: "sendmail_certificate_course",
                    //         //         peserta: elem.name,
                    //         //         from: env.MAIL_USER,
                    //         //         cc: ['agus.triadji@gmail.com','adeputrasusila19@gmail.com'],
                    //         //         subject:
                    //         //             "Certificate of Participation : Course " + req.body.cert_topic,
                    //         //         text:
                    //         //             "Certificate of Participation : Course " + req.body.cert_topic,
                    //         //         attachments: [
                    //         //             {
                    //         //                 filename: `certificate-${req.body.cert_topic}.pdf`,
                    //         //                 path: res.filename,
                    //         //                 contentType: "application/pdf",
                    //         //             },
                    //         //         ],
                    //         //         html: `
                    //         //   <b>Hello ${elem.name},</b><br><br>
                    //         //   Thank you for your participation in Course “${req.body.cert_topic}”. <br>
                    //         //   Please find attached file for your certificate of participation in the event mentioned above.`,
                    //         //     };
        
                    //         //     sendMailQueue.add(obj_mail, {
                    //         //         ...options_queue,
                    //         //         delay: options_queue.delay * idx,
                    //         //     });
                    //         }
                    //     );
                });
                res.json({ error: false, result: 'Success' });
            } catch (error) {
                res.json({ error: false, result: error });
                
            }
        
    });
};

exports.planResultByAssignee = async (req, res) => {
    try {

        //console.log(`${env.APP_URL}/v2/training/exam/read/answer-by-result/${req.params.assignee_id}?by=assignee&user=${req.params.training_user_id}`)
        if (!req.params.training_user_id && !req.params.assignee_id) {
            return res.json({ error: true, result: "Data not found" })
        }
        const { training_user_id, assignee_id } = req.params;
        const axiosConf = { headers: { "Authorization": req.headers.authorization } };

        let response = await axios.get(`${env.APP_URL}/v2/training/exam/read/answer-by-result/${assignee_id}?by=assignee&user=${training_user_id}`, axiosConf);
        //console.log(JSON.stringify(response.data.result, null, 2), "===================")
        if (response.data.error || typeof response.data.result !== 'object') {
            return res.json({ error: true, result: "Data not found" });
        }

        let data = response.data.result;
        let tempSummary = { correct_answer: 0, total_question: data.length, list: [] };
        let correct_answer = 0;
        data.forEach((str) => {
            let objects = {
                id: str.id,
                question: str.question,
                pass: false,
                answer: {
                    keyAnswer: null,
                    yourAnswer: null
                },
                option: [],
                type: str.type
            };


            if (str.type === 2) {
                //str.answer
                let arr_answer = str.answer.split(",");
                let tmp_answer = [];
                let labels = ["A,B,C,D,E"]
                // let ori_user_answer = str.user_answer.split("|");
                let arr_user_answer = str.user_answer.toLocaleLowerCase().replace(/\s/g, "");
                arr_user_answer = arr_user_answer.split("|");

                objects.answer.keyAnswer = [];
                objects.answer.yourAnswer = []
                // let state_push = []


                str.option.forEach((str_op, index) => {
                    // sync option dengan answer
                    let idx = arr_answer.indexOf(str_op.option_label);
                    if (idx > -1) {

                        let op_text_lower = str_op.option_text.toLocaleLowerCase().replace(/\s/g, "");
                        tmp_answer.push(op_text_lower);

                        // checking user_answer
                        // let arrOpt = op_text_lower.split(',');
                        let idx_arr_user_answer = arr_user_answer.indexOf(op_text_lower);

                        // let arr_user_answer2 = arr_user_answer[index].split(",");
                        // let statePass = [];
                        // let jawab = '';
                        // for (let i = 0; i < arrOpt.length; i++) {
                        //     let a = arr_user_answer2.indexOf(arrOpt[i]);
                        //     if (a > -1) {
                        //         statePass.push(true);
                        //         str_op.pass = true;
                        //         jawab = ori_user_answer[a];
                        //     } else {
                        //         jawab = arr_user_answer[index] || ''
                        //     }
                        // }

                        if (idx_arr_user_answer > -1) {
                            userAnswer = {
                                "id": 0,
                                "question_id": str.id,
                                "option_label": str_op.option_label,
                                "option_text": arr_user_answer[idx_arr_user_answer],
                                pass: true
                            }
                        } else {
                            // let idx_arr_user_answer = arrOpt.indexOf(arr_user_answer);
                            userAnswer = {
                                "id": 0,
                                "question_id": str.id,
                                "option_label": str_op.option_label,
                                "option_text": arr_user_answer[index] || '',
                                pass: false
                            }
                        }


                        objects.answer.yourAnswer.push(userAnswer);
                        objects.answer.keyAnswer.push({ ...str_op, pass: true });
                        objects.option.push(str_op);
                    } else {
                        console.log("ANSWER NOT FOUND ============")
                    }
                });

                // set pass
                let idx = objects.answer.yourAnswer.findIndex((str2) => { return str2.pass == true });
                if (idx > -1) {

                    objects.pass = true;
                    correct_answer++;
                } else {
                    objects.pass = false;
                }


            } else {
                str.option.forEach((str2) => {
                    if (str.answer === str2.option_label) {
                        objects.answer.keyAnswer = str2;

                        if (str.answer === str.user_answer) {
                            objects.pass = true;
                            correct_answer++;
                        }
                    }

                    if (str.user_answer === str2.option_label) {
                        tmp = str2;
                        objects.answer.yourAnswer = str2;
                    }

                    objects.option.push(str2)
                });
            }
            tempSummary.correct_answer = correct_answer;
            tempSummary.list.push(objects);
        });

        let listExam = tempSummary.list;
        for (let i = 0; i < listExam.length; i++) {
            let dataOption = listExam[i].option;
            listExam[i].answer.yourAnswer = listExam[i].answer.yourAnswer ? listExam[i].answer.yourAnswer : { ...listExam[i].answer.keyAnswer, option_text: 'User Not Answer This Question..', option_label: '-' };
            const userAnswer = listExam[i].answer.yourAnswer;
            const correctAnswer = listExam[i].answer.keyAnswer;
            const passed = listExam[i].pass;
            const isUserCorrect = userAnswer === correctAnswer;
            for (let j = 0; j < dataOption.length; j++) {
                if (listExam[i].type == 1) {
                    if (dataOption[j].id === correctAnswer.id) {
                        dataOption[j].type = 'clear';
                    } else if (dataOption[j].id === userAnswer.id) {
                        dataOption[j].type = 'wrong';
                    } else {
                        dataOption[j].type = 'default';
                    }
                } else {
                    const isAvailableUserAnswer = userAnswer.find(val => val.question_id === dataOption[j].question_id).pass ? true : false;
                    const isAvailableCorrectAnswer = correctAnswer.find(val => val.question_id === dataOption[j].question_id).pass ? true : false;
                    if (isAvailableCorrectAnswer) {
                        dataOption[j].type = 'clear';
                    } else if (isAvailableUserAnswer) {
                        dataOption[j].type = 'wrong';
                    } else {
                        dataOption[j].type = 'default';
                    }
                }
            }
            if (listExam[i].type === 2) {
                if (passed) {
                    let tmpListExam = [];
                    listExam[i].displayedOption = listExam[i].answer.yourAnswer.map(val => {
                        if (val.pass) {
                            tmpListExam.push({ ...val, type: 'clear' });
                            return { ...val, type: 'clear' }
                        } else {
                            const findObjectPassed = listExam[i].answer.keyAnswer.find(answer => answer.option_label === val.option_label && answer.pass);
                            tmpListExam.push({ ...findObjectPassed, type: 'clear' });
                            tmpListExam.push({ ...val, type: 'wrong' });
                            return { ...val, type: 'wrong' }
                        }
                    });

                    listExam[i].displayedOptionDetail = tmpListExam;
                } else {
                    listExam[i].displayedOption = listExam[i].answer.keyAnswer.concat(listExam[i].answer.yourAnswer).map(val => {
                        if (val.pass) {
                            return { ...val, type: 'clear' }
                        } else {
                            return { ...val, type: 'wrong' }
                        }
                    });

                    listExam[i].displayedOptionDetail = listExam[i].displayedOption;
                }
            } else {
                listExam[i].displayedOption = passed ? [{ ...userAnswer, pass: true }] : [{ ...correctAnswer, pass: true }, { ...userAnswer, pass: false }];
                listExam[i].displayedOptionDetail = passed ? [{ ...userAnswer, pass: true }] : [{ ...correctAnswer, pass: true }, { ...userAnswer, pass: false }];
            }
            //listExam[i].displayedOption = passed ? [{ ...userAnswer, pass: true }] : [{ ...correctAnswer, pass: true }, { ...userAnswer, pass: false }];

        }
        tempSummary.list = listExam;
        
        if(tempSummary.list.length){
            let userData = await db.query('select id from training_user where email = ?',[req.app.token.email]);
            if(userData.length){
                req.params.training_user_id = userData[0].id;
            }
            let getDetail = await getOneHistoryByUser({ training_user_id: req.params.training_user_id , training_result_id: req.params.assignee_id });
            tempSummary.detailInfo = getDetail[0];
        }else{
            tempSummary = 'Data is empty';
        }

        res.json({ error: tempSummary.list.length ? false : true, result: tempSummary });
    } catch (error) {
        console.error({ filename: __filename, error: true, message: error, function: 'planResultByAssignee' });
        res.json({ error: true, result: 'Data not found' });
    }
};

async function getOneHistoryByUser(body){
    return new Promise(async (res)=>{
        db.query(
          /* sql */ `SELECT
              e.id,
              e.company_id,
              e.title,
              e.image,
              e.licenses_type_id,
              e.time_limit,
              e.minimum_score,
              e.generate_question,
              e.course_id,
              e.scheduled,
              e.generate_membership,
              e.see_correct_answer,
              e.multiple_assign,
              e.start_time,
              e.end_time,
              e.exam,
              e.repeatable,
              e.status,
              e.created_by,
              r.created_at AS created_at,
              ( SELECT SUM( total ) FROM training_exam_composition WHERE exam_id = e.id ) AS number_of_question,
              l.NAME AS licenses_type,
          IF
              ( e.scheduled = 1, 'Yes', 'No' ) AS is_scheduled,
              a.id AS assignee_id,
              r.score,
              r.pass,-- 	rec.id AS record_id,
              r.id AS result_id,
          IF
              ( rec.start_time IS NULL, a.start_time, rec.start_time ) AS work_start_time,
          IF
              ( rec.submission_time IS NULL, a.submission_time, rec.submission_time ) AS submission_time,
              TIMESTAMPDIFF(
                  MINUTE,
              IF
                  ( rec.start_time IS NULL, a.start_time, rec.start_time ),
              IF
              ( rec.submission_time IS NULL, a.submission_time, rec.submission_time )) AS work_time 
          FROM
              training_exam_result r
              JOIN training_exam e ON e.id = r.exam_id
              JOIN training_licenses_type l ON l.id = e.licenses_type_id
              LEFT JOIN training_exam_record rec ON rec.exam_result_id = r.id
              LEFT JOIN training_exam_assignee a ON ( a.exam_id = r.assignee_id AND a.STATUS = 'Finish' ) 
          WHERE
              r.training_user_id = ? 
              AND r.id = ? 
          ORDER BY
              r.created_at DESC;`,
          [body.training_user_id, body.training_result_id],
          (error, result) => {
            if (error) {
                return res(null);
            }
      
            if (result.length > 0) {
              result.forEach(str => {
                if (str.repeatable > 0) {
                  str.is_read = str.created_at ? 1 : 0;
                  str.on_schedule = "1";
                }
              });
              result = result.sort((a, b) => {
                return new Date(b.created_at) - new Date(a.created_at);
              });
            }
      
            return res(result);
          }
        );
    })
  };

exports.postSubmission = async (req, res) => {
    try {
        // validator
        let valid = await validatorRequest.validPostSubmission(req.body);
        let paramSql = [valid.title, valid.course_id, valid.content, valid.start_time_submission, valid.end_time_submission, valid.sort];
        let result = await db.query(`SELECT id from training_course_submission where id =?`, [valid.id]);

        let msg = { error: true, result: 'process not successfully.' }
        if (result.length) {
            paramSql.push(valid.id);
            let res = await db.query(`UPDATE training_course_submission SET title=?, course_id=?,content=?, start_time=?, end_time=?, sort=? where id=?`, paramSql);
            res.insertId = valid.id;
            msg.error = false;
            msg.result = res;
        } else {
            let res = await db.query(`INSERT INTO training_course_submission(title,course_id,content,start_time,end_time,sort) value(?,?,?,?,?,?);`, paramSql);
            msg.error = false;
            msg.result = res;
        }

        res.json(msg);
    } catch (error) {
        console.error({ filename: __filename, error: true, message: error, function: 'postSubmission' });
        res.json({ error: true, result: 'Data not found' });
    }
};


exports.deleteSubmission = async (req, res) => {
    try {
        // validator
        let valid = await validatorRequest.validDeleteSubmission(req.params);
        let result = await db.query(`SELECT id from training_course_submission where id =?`, [valid.id]);
        let msg = { error: true, message: 'process not successfully.' };

        if (result.length) {
            await db.query(`DELETE FROM training_course_submission where id=?`, [result[0].id]);
            msg.error = false;
            msg.message = 'delete Successfully.';
        } else {
            msg.error = true;
            msg.message = 'Data is not found';
        }

        res.json(msg);
    } catch (error) {
        console.error({ filename: __filename, error: true, message: error, function: 'deleteSubmission' });
        res.json({ error: true, result: 'Data not found3' });
    }
};

exports.getSubmission = async (req, res) => {
    try {
        // validator
        let valid = await validatorRequest.validGetSubmission(req.body);
        let result = await db.query(`SELECT id, title, content, start_date,end_date,media from training_course_submission where id =?`, [valid.id]);
        let msg = { error: true, message: 'Data is not found.' }
        if (result.length) {
            msg.error = false;
            msg.message = result;
        }

        res.json(msg);
    } catch (error) {
        console.error({ filename: __filename, error: true, message: error, function: 'getSubmission' });
        res.json({ error: true, result: 'Data not found3' });
    }
};

exports.readSubmission = async (req, res) => {
    try {
        // validator
        let valid = await validatorRequest.validGetSubmission(req.params);
        let email = req.app.token.email;

        let msg = { error: true, message: 'Data is not found.' }
        
        msg.error = false;

        let submission = await db.query(
            `SELECT s.*, IF(r.is_read IS NULL, 0, r.is_read) AS is_read, r.submission_file, 'Submission' AS type, IF(r.created_at, DATEDIFF(s.start_time,r.created_at), DATEDIFF(s.start_time,s.end_time) )  as 'duration' 
            FROM training_course_submission s
            LEFT JOIN (SELECT r.* FROM training_submission_answer r JOIN training_user u ON u.id = r.training_user_id WHERE u.email=? GROUP BY r.submission_id) r ON r.submission_id = s.id
            WHERE s.id = ? 
            ORDER BY s.sort ASC`,
            [email, valid.id]);

        for (let i = 0; i < submission.length; i++) {
            let media = await db.query(`SELECT * FROM training_course_media WHERE submission_id =?`, [submission[i].id]);
            submission[i].media = media;
        }
        await Promise.all(submission.map(async (row, i) => {
            
            let start = moment(row.start_time);
            let end = moment(row.end_time);
            let now = moment(new Date());
            row.duration = moment.duration(end.diff(now)).asMinutes();
            var m = row.duration % 60;
            var h = (row.duration - m) / 60;
            //var duration = row.duration > 0 ? (h < 10 ? "0" : "") + h.toString() + ":" + (m < 10 ? "0" : "") + parseInt(m) : '-';
            row.duration = 0;
        }))
        result = submission;
        msg.result = result;

        res.json(msg);
    } catch (error) {
        console.error({ filename: __filename, error: true, message: error, function: 'getSubmission' });
        res.json({ error: true, result: 'Data not found3' });
    }
};

exports.assigneeSubmission = async (req, res) => {
    try {
        let valid = await validatorRequest.validAssigneeSubmission(req.body);
        console.log(valid, "assigneeSubmission")
        let paramSql = [valid.course_id];
        let check = await db.query(`select id from training_course_submission where course_id=?`, paramSql);
        let msg = null;
        if (check.length) {
            valid.submission_id = check[0].id;
            paramSql = [valid.submission_id, valid.training_user_id];
            check = await db.query(`select id from training_submission_answer where submission_id=? and training_user_id=?`, paramSql);
            if (check.length) {


                paramSql = ['active', valid.submission_id, valid.training_user_id];
                if (valid.action === 'delete') {
                    paramSql = ['inactive', valid.submission_id, valid.training_user_id];
                }
                msg = await db.query(`UPDATE training_submission_answer SET status=? where submission_id=? and training_user_id=? `, paramSql);
            } else {
                if (valid.action === 'add') {
                    paramSql = [valid.submission_id, valid.training_user_id, valid.created_by];
                    msg = await db.query(`INSERT INTO training_submission_answer(submission_id, training_user_id,created_by) value(?)`, [paramSql]);
                }
            }
        }
        res.json({ error: false, result: msg });

    } catch (error) {
        console.error({ filename: __filename, error: true, message: error, function: 'assigneeSubmission' });
        res.json({ error: true, result: 'Data not found' });
    }
}

exports.uploadMediaSubmission = (req, res) => {
    uploadMedia(req, res, (err) => {
        if (!req.file) {
            res.json({ error: true, result: "File size cannot larger than 500MB" });
        } else {
            let maxSize = checkfileSize(req.file);
            if(req.file.size > maxSize.size){
                return res.json({ error:true, result:`${maxSize.type.toUpperCase()} file size cannot larger than ${maxSize.size/1000000}MB` })
            }
            if (env.AWS_BUCKET) {
                let path = req.file.path;
                let newDate = new Date();
                let keyName = `${env.AWS_BUCKET_ENV}/training/course/session`;

                var params = {
                    ACL: 'public-read',
                    Bucket: env.AWS_BUCKET,
                    Body: fs.createReadStream(path),
                    Key: `${keyName}/${Date.now()}-${req.file.originalname}`,
                    ContentDisposition: 'inline',
                    ContentType: req.file.mimetype
                };

                s3.upload(params, (err, data) => {
                    if (err) {
                        res.json({ error: true, msg: "Error Upload Media", result: err });
                    }
                    if (data) {
                        fs.unlinkSync(path);

                        let form = {
                            image: data.Location,
                            type: ''
                        }

                        switch (req.file.mimetype) {
                            case 'application/pdf': form.type = 'PDF'; break;
                            case 'application/vnd.ms-powerpoint': form.type = 'PowerPoint'; break;
                            case 'application/vnd.openxmlformats-officedocument.presentationml.presentation': form.type = 'PowerPoint'; break;
                            case 'application/vnd.ms-excel': form.type = 'Excel'; break;
                            case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet': form.type = 'Excel'; break;
                            case 'image/jpeg': form.type = 'Image'; break;
                            case 'image/png': form.type = 'Image'; break;
                            case 'video/mp4': form.type = 'Video'; break;
                            case 'audio/mp4': form.type = 'Audio'; break;
                            case 'video/x-msvideo': form.type = 'Video'; break;
                            case 'application/msword': form.type = 'Word'; break;
                            case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document': form.type = 'Word'; break;
                            default: form.type = 'Others'
                        }

                        db.query(`INSERT INTO training_course_media (submission_id, name, type, url) VALUES('${req.params.id}','${req.file.originalname}','${form.type}','${form.image}')`, async (error, result, fields) => {
                            if (error) {
                                res.json({ error: true, result: error })
                            } else {
                                let mediaUploaded = await db.query(`SELECT * FROM training_course_media WHERE id='${result.insertId}'`);
                                res.json({ error: false, result: mediaUploaded[0] })
                            }
                        })

                    }
                })
            }
            else {
                let form = {
                    image: `${env.APP_URL}/training/course/session/${req.file.filename}`
                }
                switch (req.file.mimetype) {
                    case 'application/pdf': form.type = 'PDF'; break;
                    case 'application/vnd.ms-powerpoint': form.type = 'PowerPoint'; break;
                    case 'application/vnd.openxmlformats-officedocument.presentationml.presentation': form.type = 'PowerPoint'; break;
                    case 'application/vnd.ms-excel': form.type = 'Excel'; break;
                    case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet': form.type = 'Excel'; break;
                    case 'image/jpeg': form.type = 'Image'; break;
                    case 'image/png': form.type = 'Image'; break;
                    case 'video/mp4': form.type = 'Video'; break;
                    case 'audio/mp4': form.type = 'Audio'; break;
                    case 'video/x-msvideo': form.type = 'Video'; break;
                    case 'application/msword': form.type = 'Word'; break;
                    case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document': form.type = 'Word'; break;
                    default: form.type = 'Others'
                }

                db.query(`INSERT INTO training_course_media (submission_id, name, type, url) VALUES('${req.params.id}','${req.file.originalname}','${form.type}','${form.image}')`, async (error, result, fields) => {
                    if (error) {
                        res.json({ error: true, result: error })
                    } else {
                        let mediaUploaded = await db.query(`SELECT * FROM training_course_media WHERE id='${result.insertId}'`);
                        res.json({ error: false, result: mediaUploaded[0] })
                    }
                })
            }

        }
    })
}

exports.deleteMediaSubmission = (req, res) => {
    db.query(
        `DELETE FROM training_course_media
        WHERE id = ?;`,
        [req.params.id],
        (error, result) => {
            if (error) {
                return res.json({ error: true, result: error.message });
            };

            return res.json({ error: false, result });
        }
    );
};

exports.userAnswerSubmission = async (req, res) => {
    try {

        const responseUploadFile = await uploadSubmissionAnswerFile(req, res);
        res.json({ error: false, result: responseUploadFile });

    } catch (error) {
        res.json({ error: true, result: error.message || 'process not accepted' })
    }
}

exports.userAnswerSubmission2 = async (req, res) => {
    try {

        const responseUploadFile = await uploadSubmissionAnswerFile(req, res);
        res.json({ error: false, result: responseUploadFile });

    } catch (error) {
        res.json({ error: true, result: error.message || 'process not accepted' })
    }
}

exports.setScoreSubmission = async (req, res) => {
    try {
        let form = req.body;
        if (form.listParticipants.length) {
            let sqlString = ``;
            let sqlParam = [];
            let state = true;
            for (let i = 0; i < form.listParticipants.length; i++) {
                if (form.listParticipants[i].score && form.listParticipants[i].idResultSubmission && form.idSubmission) {
                    sqlString += `UPDATE training_submission_answer SET score=?, is_read = 1 where id=? and submission_id=?;`;
                    sqlParam.push(form.listParticipants[i].score, form.listParticipants[i].idResultSubmission, form.idSubmission)
                } else {
                    state = false;
                }
            }
            let update = 'Payload is failed';
            if (state) {
                update = db.query(sqlString, sqlParam);
            }
            res.json({ error: false, result: 'Success' });
        } else {
            res.json({ error: true, result: 'process not accepted' })
        }
    } catch (error) {
        res.json({ error: true, result: error.message || 'process not accepted' })
    }
}

exports.getDetailUserSubmission = async (req, res) => {
    try {
        const dtoSubmission = {
            idSubmission: req.params.idSubmission,
            idTrainingUser: req.params.idTrainingUser
        };

        const retrieveValidRequest = await validatorRequest.validateGetUserAnswerSubmission(dtoSubmission);
        const retrieveDataAnswer = await moduleDB.getDetailSubmissionUser(retrieveValidRequest);
        let resp = retrieveDataAnswer[0];
        resp.submission_file = JSON.parse(retrieveDataAnswer[0].submission_file)
        res.json({ error: false, result: resp });

    } catch (error) {
        res.json({ error: true, result: error.message })
    }
}

async function uploadSubmissionAnswerFile(req, res) {
    return new Promise((resolve, reject) => {
        uploadSubmissionAnswer(req, res, async (err) => {
            console.log(req.files,req.file,"999")
            if (!req.files && !req.file) {
                reject('Error Media Upload!');
            } else {

                let detailFile = {
                    fileLocation: '',
                    fileName: '',
                    type: ''
                };

                const dtoSubmission = {
                    idSubmission: req.body.idSubmission,
                    idTrainingUser: req.body.idTrainingUser,
                    textSubmission: req.body.textSubmission,
                };

                let files = [req.file];
                if(req.files){
                    files = req.files
                }

                const retrieveValidRequest = await validatorRequest.validateUserAnswerSubmission(dtoSubmission);
                // const retrieveDetailSubmission = await moduleDB.getDetailSubmissionById(retrieveValidRequest);
                // if(retrieveDetailSubmission.length || retrieveDetailSubmission){
                //     const dateStart = retrieveDetailSubmission[0].start_time;
                //     const dateEnd = retrieveDetailSubmission[0].end_time;

                //     if(dateStart || dateEnd){
                //         // Have Date Start n Date End Check if They in Range
                //         const dateNow = moment(new Date());
                //         const dateSubmissionStart = moment(dateStart);
                //         const dateSubmissionEnd = moment(dateEnd);
                //         const isDateInRange = dateNow.isBetween(dateSubmissionStart, dateSubmissionEnd);

                //         if(!isDateInRange){
                //             reject('Submission Expired...')
                //         }
                //     }

                    const retrieveDataAnswer = await moduleDB.getDetailSubmissionUser(retrieveValidRequest);

                    let submissionFile = [];
                    submissionFile = await Promise.all(
                    files.map(async(item)=>{
                        req.file = item;

                        const responseUploadFile = await uploadSubmissionFile(item);
                        if (responseUploadFile.error) {
                            reject(responseUploadFile.message);
                        }

                        return {
                            // ...item,
                            fileLocation: responseUploadFile.fileLocation,
                            fileName: responseUploadFile.fileName,
                            type: responseUploadFile.type
                        }
                    }));

                    /**
                    * Check User Already Submit
                    */
                    if (retrieveDataAnswer.length) {

                        /**
                        * Check User Already Submit
                        */
                        if (retrieveDataAnswer.length && retrieveDataAnswer[0].answer_id) {

                            let dataSubmissionFile = JSON.parse(retrieveDataAnswer[0].submission_file || '[]');
                            if(submissionFile.length) {
                                dataSubmissionFile = dataSubmissionFile.concat(submissionFile);
                            }
                            const countUploaded = dataSubmissionFile.length;
                            const updateUserAnswer = await moduleDB.updateTrainingUserAnswer({ ...retrieveValidRequest, arrSubmissionFile: JSON.stringify(dataSubmissionFile), countUploaded: countUploaded });

                        } else {

                            const arrSubmissionFile = submissionFile.length === 0 ? JSON.stringify([]) : JSON.stringify(submissionFile);
                            const countUploaded = submissionFile.length === 0 ? 0 : 1;
                            const postUserAnswer = await moduleDB.postTrainingUserAnswer({ ...retrieveValidRequest, arrSubmissionFile: arrSubmissionFile, countUploaded: countUploaded });
                            
                        }
                    }

                    resolve('Success Answer Submission');
                    
            //     }else{
            //         //No Data in DB
            //         reject('Error No Data Available..');
            //     }
            }
        });

    });
}

async function uploadSubmissionFile(detailFile) {
    return new Promise((resolve, reject) => {
        if (env.AWS_BUCKET) {
            let path = detailFile.path;
            let newDate = new Date();
            let keyName = `${env.AWS_BUCKET_ENV}/training/course/submission`;

            let replacename = detailFile.originalname.replace(/[^A-Z0-9.]/ig, "_");
            var params = {
                ACL: "public-read",
                Bucket: env.AWS_BUCKET,
                Body: fs.createReadStream(path),
                Key: `${keyName}/${Date.now()}-${replacename}`,
                ContentDisposition: "inline",
                ContentType: detailFile.mimetype,
            };

            s3.upload(params, (err, data) => {
                if (err) {
                    reject({ error: true, message: 'Error Media Upload!!' });
                }
                if (data) {
                    //fs.unlink(path,(err)=>{ console.log(err) });

                    let form = {
                        image: data.Location,
                        type: "",
                    };

                    switch (detailFile.mimetype) {
                        case "application/pdf":
                            form.type = "PDF";
                            break;
                        case "application/vnd.ms-powerpoint":
                            form.type = "PowerPoint";
                            break;
                        case "application/vnd.openxmlformats-officedocument.presentationml.presentation":
                            form.type = "PowerPoint";
                            break;
                        case "application/vnd.ms-excel":
                            form.type = "Excel";
                            break;
                        case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
                            form.type = "Excel";
                            break;
                        case "image/jpeg":
                            form.type = "Image";
                            break;
                        case "image/png":
                            form.type = "Image";
                            break;
                        case "video/mp4":
                            form.type = "Video";
                            break;
                        case "audio/mp4":
                            form.type = "Audio";
                            break;
                        case "video/x-msvideo":
                            form.type = "Video";
                            break;
                        case "application/msword":
                            form.type = "Word";
                            break;
                        default:
                            form.type = "Others";
                    }
                    resolve({
                        error: false,
                        fileLocation: data.Location,
                        fileName: detailFile.originalname,
                        type: form.type,
                        body: detailFile.body,
                    });

                }
            });
        } else {
            let form = {
                image: `${env.APP_URL}/training/course/session/${detailFile.filename}`,
            };
            switch (req.file.mimetype) {
                case "application/pdf":
                    form.type = "PDF";
                    break;
                case "application/vnd.ms-powerpoint":
                    form.type = "PowerPoint";
                    break;
                case "application/vnd.openxmlformats-officedocument.presentationml.presentation":
                    form.type = "PowerPoint";
                    break;
                case "application/vnd.ms-excel":
                    form.type = "Excel";
                    break;
                case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
                    form.type = "Excel";
                    break;
                case "image/jpeg":
                    form.type = "Image";
                    break;
                case "image/png":
                    form.type = "Image";
                    break;
                case "video/mp4":
                    form.type = "Video";
                    break;
                case "audio/mp4":
                    form.type = "Audio";
                    break;
                case "video/x-msvideo":
                    form.type = "Video";
                    break;
                case "application/msword":
                    form.type = "Word";
                    break;
                case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document': form.type = 'Word'; break;
                default:
                    form.type = "Others";
            }

            resolve({
                error: false,
                fileLocation: `${env.APP_URL}/training/course/session/${req.file.filename}`,
                fileName: req.file.originalname,
                type: form.type,
                body: req.body,
            });
        }
    })

}