var env = require("../env.json");
const db = require("../configs/database");
const Xendit = require("xendit-node");
const x = new Xendit({
  secretKey:
    "xnd_development_XQrewVa5Dny64JCqaLkZwpZa9as09XcOL0Uz2yKhE7PjkqV9jhBOAqBSMrET",
});
const { Invoice } = x;

exports.create = (req, res) => {
  const invoiceSpecificOptions = {};
  const i = new Invoice(invoiceSpecificOptions);
  // pilih modul
  if (req.body.modul === "training_registration_user") {
    // insert data payment awal
    db.query(
      `INSERT INTO payment (internal_id, modul) VALUES (?)`,
      [[req.body.internal_id, req.body.modul]],
      (error, result) => {
        if (error) {
          return res.json({ error: true, result: error.message });
        } else {
          // update training_registration_list payment data
          db.query(
            `UPDATE training_registration_list SET payment_status='PENDING', payment_id=? WHERE id=?;`,
            [result.insertId, req.body.internal_id],
            async (error, result2) => {
              if (error) {
                res.json({ error: true, result: error.message });
              } else {
                // req invoice url ke xendit
                let form = {
                  externalID: String(result.insertId),
                  amount: req.body.amount,
                  payerEmail: req.body.email,
                  description: req.body.description,
                  customer: {
                    given_names: req.body.name,
                    email: req.body.email,
                    mobile_number: '-'
                  }
                };
                const resp = await i.createInvoice(form);
                if (resp) {
                  // save payment url to db
                  db.query(
                    `UPDATE payment SET payment_url = ? WHERE id = ?`,
                    [resp.invoice_url, result.insertId],
                    (error, result3) => {
                      if (error) {
                        res.json({ error: true, result: error.message });
                      } else {
                        res.json({ error: false, result: resp.invoice_url });
                      }
                    }
                  );
                } else {
                  res.json({ error: true, result: "xendit error" });
                }
              }
            }
          );
        }
      }
    );
  } else {
    res.json({ error: true, result: "modules not match" });
  }
};

exports.callback = async (req, res) => {
  req.body.external_id = Number(req.body.external_id);
  if (req.body.id && req.body.external_id > 0) {
    // update data payment callback
    db.query(
      `UPDATE payment SET payment_callback=? WHERE id=?`,
      [JSON.stringify(req.body), req.body.external_id],
      (error, result) => {
        if (error) {
          return res.json({ error: true, result: error.message });
        } else {
          // update training_registration_list payment status
          db.query(
            `UPDATE training_registration_list SET payment_status=? WHERE payment_id=?`,
            [req.body.status, req.body.external_id],
            (error, result) => {
              if (error) {
                res.json({ error: true, result: error.message });
              } else {
                res.json({ error: false, result: "success" });
              }
            }
          );
        }
      }
    );
  } else {
    res.json({ error: true, result: "request rejected" });
  }
};

exports.detail = async (req, res) => {
  db.query(
    `SELECT payment_callback FROM payment WHERE id=?`,
    req.params.id,
    (error, result) => {
        if (error){
            res.json({ error: true, result: error.message });
        }
        else{
            if (result.length){
                res.json({ error: false, result: JSON.parse(result[0].payment_callback) });
            }
            else{
                res.json({ error: true, result: "no data" });
            }
        }
    }
  );
};
