var env = require("../env.json");
var conf = require("../configs/config");
var db = require("../configs/database");
var fs = require("fs");

// insert into learning_chapter
let learning_chapter_create = (data) => {
    return new Promise((resolve, reject) => {
        db.query(
            `INSERT INTO learning_chapter
                (number, pelajaran, moderator, type, start, end)
            VALUES
                (?)`,
            [[data.number, data.pelajaran, data.moderator, data.type, new Date(), new Date()]],
            (error, result) => {
                if (error) {
                    return reject(error.message);
                } else {
                    return resolve(result.insertId);
                }
            }
        );
    });
};

// insert into learning_chapter_detail
let learning_chapter_detail_create = (id, data) => {
    return new Promise((resolve, reject) => {
        db.query(
            `INSERT INTO learning_chapter_detail
                (id, title, body, pdf, video)
            VALUES
                (?)`,
            [[id, data.title, data.body, data.pdf, data.video]],
            (error, result) => {
                if (error) {
                    return reject(error.message);
                } else {
                    return resolve("success create learning");
                }
            }
        );
    });
};

// insert into learning_questions and learning_options
let learning_test = (id, data) => {
    return new Promise(async (resolve, reject) => {
        // map detail
        data.detail.map(async (elem) => {
            let question = [id, elem.tanya, elem.jawab];
            let options = [];

            // insert into learning_questions
            let insert_questions = async () => {
                try {
                    return await db.query(
                        `INSERT INTO learning_questions (chapter_id, question, answer)
                            VALUES (?);`,
                        [question]
                    );
                } catch (error) {
                    return reject(await error.message);
                }
            };

            let last_insert_id = await insert_questions();

            await Object.keys(elem).map((e) => {
                if (e === "tanya" || e === "jawab") {
                } else {
                    options.push([last_insert_id.insertId, e, elem[e]]);
                }
            });

            // insert into learning_options
            let insert_options = async () => {
                try {
                    await db.query(
                        `INSERT INTO learning_options (question_id, options, answer)
                            VALUES ?;`,
                        [options]
                    );

                    return resolve("success create learning");
                } catch (error) {
                    return reject(await error.message);
                }
            };

            await insert_options();
        });
    });
};

/**
 * chapter
 * @param   {int}   req.body.number 
 * @param   {int}   req.body.pelajaran
 * @param   {int}   req.body.moderator
 * @param   {int}   req.body.type
 * @param   {date}  req.body.start
 * @param   {date}  req.body.end
 * @param   {string}    req.body.title
 * @param   {string}    req.body.body
 * @param   {string}    req.body.pdf
 * @param   {string}    req.body.video
 * 
 * pre test
 * post test
 * @param   {int}   req.body.number 
 * @param   {int}   req.body.pelajaran
 * @param   {int}   req.body.moderator
 * @param   {int}   req.body.type
 * @param   {date}  req.body.start
 * @param   {date}  req.body.end
 * @param   {array.<{tanya: string, jawab: string, a: string}>} req.body.detail
 */
exports.create = async (req, res) => {
    // chapter
    let chapter_id = new Promise((resolve, reject) => {
        learning_chapter_create(req.body)
            .then((result) => {
                return resolve(result);
            })
            .catch((error) => {
                return reject(error);
            });
    });

    switch (req.body.type) {
        case 0:
            chapter_id
                .then((result) => {
                    // chapter detail
                    learning_chapter_detail_create(result, req.body)
                        .then((results) => {
                            return res.json({ error: false, result: results });
                        })
                        .catch((error) => {
                            return res.json({ error: true, result: error });
                        });
                })
                .catch((error) => {
                    return res.json({ error: true, result: error });
                });
            break;

        default:
            // 1 = pre test
            // 2 = post test
            chapter_id
                .then((result) => {
                    // pre test and post test
                    learning_test(result, req.body)
                        .then((results) => {
                            return res.json({ error: false, result: results });
                        })
                        .catch((error) => {
                            return res.json({ error: true, result: error });
                        });
                })
                .catch((error) => {
                    return res.json({ error: true, result: error });
                });
            break;
    }
};

// select from learning_chapter
let learning_chapter_read = (data) => {
    return new Promise((resolve, reject) => {
        db.query(`SELECT * FROM learning_chapter WHERE id = ?;`, [data.id], (error, result) => {
            if (error) {
                return reject(error.message);
            } else {
                return resolve(result);
            }
        });
    });
};

// select from learning_chapter_detail
let learning_chapter_detail_read = (params) => {
    return new Promise((resolve, reject) => {
        db.query(
            `SELECT *
            FROM learning_chapter_detail 
            WHERE id = ?`,
            [params.id],
            (error, result) => {
                if (error) {
                    return reject(error.message);
                } else {
                    return resolve(result);
                }
            }
        );
    });
};

// select from learning questions
let learning_questions_read = (params) => {
    return new Promise((resolve, reject) => {
        db.query(
            `SELECT * 
            FROM learning_questions 
            WHERE chapter_id = ?`,
            [params],
            (error, result) => {
                if (error) {
                    return reject(error.message);
                } else {
                    return resolve(result);
                }
            }
        );
    });
};

// select from learning_options
let learning_options_read = (params) => {
    return new Promise((resolve, reject) => {
        db.query(
            `SELECT *
            FROM learning_options 
            WHERE question_id = ?`,
            [params],
            (error, result) => {
                if (error) {
                    return reject(error.message);
                } else {
                    return resolve(result);
                }
            }
        );
    });
};

/**
 * @param {int} req.params.id
 */
exports.read = (req, res) => {
    learning_chapter_read(req.params)
        .then((result) => {
            switch (result[0].type) {
                case 0:
                    // chapter
                    learning_chapter_detail_read(req.params)
                        .then((results) => {
                            let obj = Object.assign({}, result[0], results[0]);
                            return res.json({ error: false, result: obj });
                        })
                        .catch((error) => {
                            return res.json({ error: true, result: error });
                        });
                    break;

                default:
                    // 1 = pre test
                    // 2 = post test
                    learning_questions_read(result[0].id)
                        .then((e) => {
                            e.map((elem) => {
                                learning_options_read(e[0].id)
                                    .then((results) => {
                                        elem.jawab = results;
                                        return res.json({ error: false, result: elem });
                                    })
                                    .catch((error) => {
                                        return res.json({ error: true, result: error });
                                    });
                            });
                        })
                        .catch((error) => {
                            return res.json({ error: true, result: error });
                        });
                    break;
            }
        })
        .catch((error) => {
            return res.json({ error: true, result: error });
        });
};

/**
 * @param {int} req.params.pelajaran 
 */
exports.list_chapter = (req, res) => {
    db.query(`SELECT * FROM learning_chapter WHERE pelajaran = ? ORDER BY number;`, [req.params.pelajaran], (error, result) => {
        if (error) {
            return res.json({ error: true, result: error.message });
        } else {
            return res.json({ error: false, result: result });
        }
    });
};

/**
 * @param {int} req.params.id
 */
exports.delete = (req, res) => {
    db.query(
        `SET @id = ?;
        DELETE FROM learning_chapter WHERE id = @id;
        DELETE FROM learning_chapter_detail WHERE id = @id;
        DELETE FROM learning_options WHERE question_id IN (
            SELECT id FROM learning_questions WHERE chapter_id = @id);
        DELETE FROM learning_questions WHERE chapter_id = @id;`,
        [req.params.id],
        (error, result) => {
            if (error) {
                return res.json({ error: true, result: error.message });
            } else {
                return res.json({ error: false, result: result });
            }
        }
    );
};

let learning_chapter_update = (data) => {
    return new Promise ((resolve, reject) => {
        db.query(
            `UPDATE learning_chapter
            SET number = ?, 
                pelajaran = ?, 
                moderator = ?, 
                type = ?, 
                start = ?, 
                end = ?
            WHERE id = ?;`,
            [data.number, data.pelajaran, data.moderator, data.type, new Date(), new Date(), data.id],
            (error, result) => {
                if(error) {
                    return reject(error.message);
                } else {
                    return resolve(result);
                };
            }
        );
    });
};

let learning_chapter_detail_update = (data) => {
    return new Promise ((resolve, reject) => {
        db.query(
            `UPDATE learning_chapter_detail
            SET title = ?, 
                body = ?, 
                pdf = ?, 
                video = ?
            WHERE id = ?;`,
            [data.title, data.body, data.pdf, data.video, data.id],
            (error, result) => {
                if(error) {
                    return reject(error.message);
                } else {
                    return resolve(result);
                };
            }
        );
    });
};

let learning_test_update = (data) => {
    return new Promise ((resolve, reject) => {
        // map detail
        data.detail.map(async (elem) => {
            let question = [elem.tanya, elem.jawab, elem.id, data.id, elem.id];
            let options = [];

            // update learning_questions
            let update_questions = async () => {
                try {
                    return await db.query(
                        `UPDATE learning_questions
                        SET question = ?,
                            answer = ?
                        WHERE id = ?
                            AND chapter_id = ?;
                        
                        DELETE FROM learning_options 
                        WHERE question_id = ?;`,
                        question
                    );
                } catch (error) {
                    return reject(await error.message);
                };
            };

            await update_questions();

            await Object.keys(elem).map((e) => {
                if(e === "tanya" || e === "jawab" || e === "id") {
                } else {
                    options.push([elem.id, e, elem[e]]);
                };
            });

            // insert into learning_options
            let insert_options = async () => {
                try {
                    await db.query(
                        `INSERT INTO learning_options (question_id, options, answer)
                            VALUES ?;`,
                        [options]
                    );

                    return resolve("success update learning");
                } catch (error) {
                    return reject(await error.message);
                };
            };

            await insert_options();
        });
    });
};

/**
 * chapter
 * @param   {int}   req.body.id
 * @param   {int}   req.body.number 
 * @param   {int}   req.body.pelajaran
 * @param   {int}   req.body.moderator
 * @param   {int}   req.body.type
 * @param   {date}  req.body.start
 * @param   {date}  req.body.end
 * @param   {string}    req.body.title
 * @param   {string}    req.body.body
 * @param   {string}    req.body.pdf
 * @param   {string}    req.body.video
 * 
 * pre test
 * post test
 * @param   {int}   req.body.id
 * @param   {int}   req.body.number 
 * @param   {int}   req.body.pelajaran
 * @param   {int}   req.body.moderator
 * @param   {int}   req.body.type
 * @param   {date}  req.body.start
 * @param   {date}  req.body.end
 * @param   {array.<{id: int, tanya: string, jawab: string, a: string}>} req.body.detail
 */
exports.update = (req, res) => {
    let chapter_id = new Promise ((resolve, reject) => {
        learning_chapter_update(req.body)
            .then(result => {
                return resolve(result);
            })
            .catch(error => {
                return reject(error);
            });
    });

    if(req.body.type === 0) {
        // chapter
        chapter_id
            .then(result => {
                learning_chapter_detail_update(req.body)
                    .then(results => {
                        return res.json({ error: false, result: results });
                    })
                    .catch(error => {
                        return res.json({ error: true, result: error });
                    });
            })
            .catch(error => {
                return res.json({ error: true, result: error });
            });
    } else {
        // pre test
        // post test
        chapter_id
            .then(result => {
                learning_test_update(req.body)
                    .then(results => {
                        return res.json({ error: false, result: results });
                    })
                    .catch(error => {
                        return res.json({ error: true, result: error });
                    });
            })
            .catch(error => {
                return res.json({ error: true, result: error });
            });
    };
};

/**
 * @param   {int}   req.body.id
 * @param   {int}   req.body.user
 * @param   {array.<{tanya: int, jawab: int}>}    req.body.detail
 */
exports.answer = (req, res) => {
    let answer = [];
    req.body.detail.map(e => {
        answer.push([req.body.id, e.tanya, e.jawab, req.body.user]);
    });

    db.query(
        `INSERT INTO learning_answer
            (chapter_id, question_id, answer_id, user_id)
        VALUES
            ?;`,
        [answer],
        (error, result) => {
            if(error) {
                return res.json({ error: true, result: error.message });
            } else {
                return res.json({ error: false, result: result });
            };
        }
    );
};