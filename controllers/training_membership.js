const db = require('../configs/database');
var env = require('../env.json');
var moment = require('moment');
var multer = require('multer');
const fs = require('fs');
const AWS = require('aws-sdk');
const nodeHtmlToImage = require('node-html-to-image');
const moduleDB = require('../repository');
const s3 = new AWS.S3({
  accessKeyId: env.AWS_ACCESS,
  secretAccessKey: env.AWS_SECRET
});
const storage = multer.diskStorage({
	destination: (req, file, cb) => {
    cb(null, './public/training/membership/photo');
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname);
  }
})
let uploadImage = multer({ storage: storage }).single('photo');

exports.browseByCompany = (req, res) => {
    db.query(`SELECT m.*, u.name, c.name AS company_name, r.title AS passed
    FROM training_membership m
    JOIN training_user u ON u.id = m.training_user_id
    JOIN training_company c ON c.id = u.training_company_id
    LEFT JOIN (SELECT r.training_user_id, group_concat(e.title order by r.created_at desc) AS title FROM training_exam_result r JOIN training_exam e ON e.id = r.exam_id WHERE r.pass='1' GROUP BY r.training_user_id) r ON r.training_user_id = m.training_user_id
    WHERE c.company_id='${req.params.id}' AND m.status='${req.params.status}'
    ORDER BY m.expired DESC`, (error, result) => {
        if (error){
            res.json({error: true, result: error.message})
        }
        else{
            res.json({error: false, result: result})
        }
    })
}

exports.browseByTrainingCompany = (req, res) => {
    db.query(`SELECT m.*, u.name, c.name AS company_name, r.title AS passed
    FROM training_membership m
    JOIN training_user u ON u.id = m.training_user_id
    JOIN training_company c ON c.id = u.training_company_id
    LEFT JOIN (SELECT r.training_user_id, group_concat(e.title order by r.created_at desc) AS title FROM training_exam_result r JOIN training_exam e ON e.id = r.exam_id WHERE r.pass='1' GROUP BY r.training_user_id) r ON r.training_user_id = m.training_user_id
    WHERE u.training_company_id='${req.params.id}' AND m.status='${req.params.status}'
    ORDER BY m.expired DESC`, (error, result) => {
        if (error){
            res.json({error: true, result: error.message})
        }
        else{
            res.json({error: false, result: result})
        }
    })
}

exports.read = (req, res) => {
    db.query(`SELECT m.*, u.* FROM training_membership m JOIN training_user u ON u.id = m.training_user_id WHERE m.id='${req.params.id}' ORDER BY m.expired DESC LIMIT 1`, (error, result) => {
        if (error){
            res.json({error: true, result: error.message})
        }
        else{
            res.json({error: false, result: result[0]})
        }
    })
}

exports.readByUser = (req, res) => {
    db.query(`SELECT m.*, u.*, tc.name AS training_company_name FROM training_membership m JOIN training_user u ON u.id = m.training_user_id JOIN training_company tc ON u.training_company_id = tc.id WHERE m.training_user_id='${req.params.id}' AND m.status='Active' ORDER BY m.expired DESC LIMIT 1`, (error, result) => {
        if (error){
            res.json({error: true, result: error.message})
        }
        else{
            res.json({error: false, result: result.length ? result[0] : {}})
        }
    })
}

exports.create = async(req, res) => {
    let format = await db.query(`SELECT fo.format
    FROM training_exam_assignee a
    LEFT JOIN training_user u ON u.id = a.training_user_id
    LEFT JOIN training_company tc ON tc.id = u.training_company_id
    LEFT JOIN training_company_licenses_format fo ON fo.company_id = tc.company_id
    WHERE a.id = '${req.body.assignee_id}'`);
    let result = await db.query(`SELECT r.*, u.gender, e.company_id FROM training_exam_result r JOIN training_user u ON u.id = r.training_user_id JOIN training_exam e ON e.id = r.exam_id WHERE r.assignee_id='${req.body.assignee_id}'`);
    if (result[0].pass === 1){
        let now = new Date();
        let licenseFormat = format[0].format ? format[0].format : '[YYYY][MM][DD].A0[GENDER]-[NUMBER]';

        let totalPassByGender = await db.query(`SELECT (COUNT(NULLIF(u.license_number,''))+1) AS number FROM training_exam_result r JOIN training_user u ON u.id = r.training_user_id JOIN training_exam e ON e.id = r.exam_id WHERE r.pass=1 AND DATE(r.created_at)=CURDATE() AND u.gender='${result[0].gender}' AND e.company_id='${result[0].company_id}'`);
        let increment = totalPassByGender[0].number;
        //format
        let YYYY = moment(new Date(result[0].created_at)).format('YYYY');
        let MM = moment(new Date(result[0].created_at)).format('MM');
        let DD = moment(new Date(result[0].created_at)).format('DD');
        let GENDER = result[0].gender === 'Male' ? '1' : '2';
        let NUMBER = increment.toString().padStart(9, "0")

        let parseFormat = licenseFormat.replace(/\[/g, "${").replace(/\]/g,"}");
        let license_number = eval('`'+parseFormat+'`')

        let form = [
            training_user_id = req.body.training_user_id,
            expired = moment(new Date(now.setFullYear(now.getFullYear() + 2))).format('YYYY-MM-DD'),
            photo = '',
            license_card = '',
            license_number = license_number,
            status = 'Active',
            created_at = new Date()
        ];

        let check = await db.query(`SELECT * FROM training_membership WHERE training_user_id='${form[0]}' ORDER BY created_at DESC LIMIT 1`);
        if (check.length){
            db.query(`UPDATE training_membership SET
            expired=?,
            status=?
            WHERE id=?`,[form[1], form[5], check[0].id], (error, result) => {
                if (error){
                    res.json({error: true, result: error})
                }
                else{
                    db.query(`UPDATE training_user SET license_number='${check[0].license_number}' WHERE id='${form[0]}'`, (error, result2) => {
                        if (error){
                            res.json({error: true, result: error.message})
                        }
                        else{
                            res.json({error: false, result: {membership_id: check[0].id}})
                        }
                    });
                }
            })
        }
        else{
            db.query(`INSERT INTO training_membership (training_user_id, expired, photo, license_card, license_number, status, created_at) VALUES(?)`,[form], (error, result) => {
                if (error){
                    res.json({error: true, result: error.message})
                }
                else{
                    db.query(`UPDATE training_user SET license_number='${license_number}' WHERE id='${form[0]}'`, (error, result2) => {
                        if (error){
                            res.json({error: true, result: error.message})
                        }
                        else{
                            res.json({error: false, result: {membership_id: result.insertId}})
                        }
                    });
                }
            })
        }
    }
    else{
        res.json({error: false, result: 'validationError', message: 'You are not pass this exam'})
    }
}

exports.checkUserMembership = async (req, res) => {
    try {
        const dtoUserTraining = {
            idTrainingUser: req.params.idTrainingUser,
            status: 'Active'
        };
        const retrieveUserMembership = await moduleDB.retrieveUserMembership(dtoUserTraining); // Data User Membership
        const retrieveTrainingExamResult = await moduleDB.retrieveTrainingExamResult(dtoUserTraining); // Data Exam Result Id
        const idMembership = await moduleDB.getIdMembershipByTrainingUser({idTrainingUser: req.params.idTrainingUser});
        const responseExamResultId = retrieveUserMembership.length ? [] : retrieveTrainingExamResult.map((dataExam => (dataExam.id)));
        const responseIdMembership = retrieveTrainingExamResult.length ? (idMembership.length ? idMembership[0].id : 'nopending') : 'nopending';
        // const isConditionalMembership = (retrieveUserMembership.length === 0 && retrieveTrainingExamResult.length)
        res.json({ error: false, result: {exam_result_id: responseExamResultId, membership_id: responseIdMembership}});

    } catch (error) {
        res.json({ error: true, result: error.message })
    }
};

exports.createByResultId = async(req, res) => {

    // req.body.assignee_id itu exam result id
    let format = await db.query(`SELECT fo.format
    FROM training_exam_result r
    LEFT JOIN training_user u ON u.id = r.training_user_id
    LEFT JOIN training_company tc ON tc.id = u.training_company_id
    LEFT JOIN training_company_licenses_format fo ON fo.company_id = tc.company_id
    WHERE r.id = '${req.body.assignee_id}'`);
    let result = await db.query(`SELECT r.*, u.gender, e.company_id FROM training_exam_result r JOIN training_user u ON u.id = r.training_user_id JOIN training_exam e ON e.id = r.exam_id WHERE r.id='${req.body.assignee_id}'`);
    if (result[0].pass === 1){
        let now = new Date();
        let licenseFormat = format[0].format ? format[0].format : '[YYYY][MM][DD].A0[GENDER]-[NUMBER]';

        let totalPassByGender = await db.query(`SELECT (COUNT(NULLIF(u.license_number,''))+1) AS number FROM training_exam_result r JOIN training_user u ON u.id = r.training_user_id JOIN training_exam e ON e.id = r.exam_id WHERE r.pass=1 AND DATE(r.created_at)=CURDATE() AND u.gender='${result[0].gender}' AND e.company_id='${result[0].company_id}'`);
        let increment = totalPassByGender[0].number;
        //format
        let YYYY = moment(new Date(result[0].created_at)).format('YYYY');
        let MM = moment(new Date(result[0].created_at)).format('MM');
        let DD = moment(new Date(result[0].created_at)).format('DD');
        let GENDER = result[0].gender === 'Male' ? '1' : '2';
        let NUMBER = increment.toString().padStart(9, "0")

        let parseFormat = licenseFormat.replace(/\[/g, "${").replace(/\]/g,"}");
        let license_number = eval('`'+parseFormat+'`')

        let form = [
            training_user_id = req.body.training_user_id,
            expired = moment(new Date(now.setFullYear(now.getFullYear() + 2))).format('YYYY-MM-DD'),
            photo = '',
            license_card = '',
            license_number = license_number,
            status = 'Active',
            created_at = new Date()
        ];

        let check = await db.query(`SELECT * FROM training_membership WHERE training_user_id='${form[0]}' ORDER BY created_at DESC LIMIT 1`);
        if (check.length){
            //New Formula For Expired Time 
            // From Today + 2 Years extended
            // To: Expired Time + 2 Years Extended
            const expTime = new Date(check[0].expired);
            const expiredNewTimeRule = moment(new Date(expTime.setFullYear(expTime.getFullYear() + 2))).format('YYYY-MM-DD')

            let queryExtend = `UPDATE training_membership SET expired=?, status=? WHERE id=?`;
            let dataExtend = [expiredNewTimeRule, form[5], check[0].id];
            let queryReplace = `UPDATE training_membership SET license_number=?, expired=?, status=? WHERE id=?`;
            let dataReplace = [license_number, expiredNewTimeRule, form[5], check[0].id];
            let query = license_number.length === check[0].license_number.length ? queryExtend : queryReplace;
            let dataQuery = license_number.length === check[0].license_number.length ? dataExtend : dataReplace;
            let licenseWillSave = license_number.length === check[0].license_number.length ? check[0].license_number : license_number;

            db.query(query, dataQuery, (error, result) => {
                if (error){
                    res.json({error: true, result: error})
                }
                else{
                    db.query(`UPDATE training_user SET license_number='${licenseWillSave}' WHERE id='${form[0]}'`, (error, result2) => {
                        if (error){
                            res.json({error: true, result: error.message})
                        }
                        else{
                            res.json({error: false, result: {membership_id: check[0].id}})
                        }
                    });
                }
            })
        }
        else{
            db.query(`INSERT INTO training_membership (training_user_id, expired, photo, license_card, license_number, status, created_at) VALUES(?)`,[form], (error, result) => {
                if (error){
                    res.json({error: true, result: error.message})
                }
                else{
                    db.query(`UPDATE training_user SET license_number='${license_number}' WHERE id='${form[0]}'`, (error, result2) => {
                        if (error){
                            res.json({error: true, result: error.message})
                        }
                        else{
                            res.json({error: false, result: {membership_id: result.insertId}})
                        }
                    });
                }
            })
        }
    }
    else{
        res.json({error: false, result: 'validationError', message: 'You are not pass this exam'})
    }
}

const characters ='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

function generateString(length) {
    let result = ' ';
    const charactersLength = characters.length;
    for ( let i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }

    return result;
}
exports.uploadImage = (req, res) => {
    let html = `
    <html>
    <head>
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;700&display=swap" rel="stylesheet">
      <style>
        body, .card {
          width: 581px;
          height: 370px;
          position:relative;
          font-family: 'Open Sans', sans-serif;
        }
        .logo {
          position:absolute;
          top:32px;
          left:50px;
          max-width: 180px;
          max-height: 120px;
        }
        .photo {
          position:absolute;
          width: 150px;
          bottom:50px;
          right: 50px;
        }
        .company {
          position:absolute;
          width:240px;
          top:34px;
          right:10px;
          text-align:center;
          font-size:20px;
          color: #194482;
          font-weight: bold;
        }
        .data {
          position:absolute;
          width:300px;
          bottom:50px;
          left:50px;
        }
        .name {
          position:relative;
          width:100%;
          text-align:left;
          font-size:18px;
          color: #194482;
          font-weight: bold;
        }
        .training_company {
          position:relative;
          width:100%;
          text-align:left;
          font-size:18px;
          color: #194482;
          font-weight: bold;
        }
        .license_number {
          position:relative;
          width:100%;
          text-align:left;
          font-size:16px;
          color: #194482;
        }
        .expired {
          position:relative;
          width:100%;
          text-align:left;
          font-size:16px;
          color: #194482;
        }
      </style>
    </head>
    <body>
        <div class="card">
            <img src="{{image_card}}" width="581px"/>
            <img src="{{logo}}" class="logo"/>
            <img src="{{photo}}" class="photo"/>
            <p class="company">{{company}}</p>
            <div class="data">
                <p class="name">{{name}}</p>
                <p class="training_company">{{training_company}}</p>
                <p class="license_number">License : {{license}}</p>
                <p class="expired">Expired : {{expired}}</p>
            </div>
        </div>
    </body>
    </html>`;
    uploadImage(req, res, async (err) => {
      if(!req.file) {
        res.json({ error: true, result: err });
      } else {
          const retrieveIdTrainingUser = await moduleDB.retrieveIdTrainingUserByIdMembership({idMembership: req.params.id});
              if(env.AWS_BUCKET) {
                let path = req.file.path;
                let newDate = new Date();
                let keyName = `${env.AWS_BUCKET_ENV}/training/membership/photo`;
    
                var params = {
                ACL: 'public-read',
                Bucket: env.AWS_BUCKET,
                Body: fs.createReadStream(path),
                Key: `${keyName}/${Date.now()}-${req.file.originalname}`,
                ContentDisposition: 'inline',
                ContentType: req.file.mimetype
                };
    
                s3.upload(params, async (err, data) => {
                if (err) {
                    res.json({ error: true, msg: "Error Upload Image", result: err });
                }
                if (data) {
                    fs.unlinkSync(path);
    
                    let form = {
                        image: data.Location
                    }
                    
                    let dataLicense = await db.query(`
                    SELECT t.*
                    FROM training_licenses_type t
                    JOIN training_exam e ON e.licenses_type_id = t.id
                    JOIN training_exam_result r ON r.exam_id = e.id
                    JOIN training_user tu ON tu.id = r.training_user_id
                    JOIN training_membership m ON m.training_user_id = tu.id
                    WHERE m.id=${req.params.id}
                    ORDER BY r.created_at DESC
                    LIMIT 1
                    `);
                    let dataMember = await db.query(`SELECT u.name, c.company_name, tc.name AS training_company_name, c.logo, u.license_number, m.expired
                    FROM training_membership m
                    JOIN training_user u ON u.id = m.training_user_id
                    JOIN training_company tc ON tc.id = u.training_company_id
                    JOIN company c ON c.company_id = tc.company_id
                    WHERE m.id='${req.params.id}'`);
                    let rndm =generateString(5);
                    nodeHtmlToImage({
                        output: `./public/training/membership/license_card/${moment(new Date()).format('YYYY-MM-DD')+'-'+req.params.id+'-'+rndm}.png`,
                        html: html,
                        content: {
                            image_card: dataLicense.length ? dataLicense[0].image_card.length ? dataLicense[0].image_card : `${env.APP_URL}/training/membership/card.svg` : `${env.APP_URL}/training/membership/card.svg`,
                            logo: dataMember[0].logo,
                            photo: form.image,
                            company: dataMember[0].company_name,
                            name: dataMember[0].name,
                            training_company: dataMember[0].training_company_name,
                            license: dataMember[0].license_number,
                            expired: moment(new Date(dataMember[0].expired)).format('DD/MM/YYYY')
                        },
                        puppeteerArgs: {
                            args: ['--no-sandbox', '--headless'], ignoreHTTPSErrors: true
                        }
                      })
                        .then(() =>{
                            db.query(`UPDATE training_membership SET photo = '${form.image}', license_card = '${env.APP_URL}/training/membership/license_card/${moment(new Date()).format('YYYY-MM-DD')+'-'+req.params.id+'-'+rndm}.png' WHERE id = '${req.params.id}'`, async (error, result, fields) => {
                            if(error) {
                                res.json({error: true, result: error})
                            } else {
                                await moduleDB.updatePhotoTakenByIdTrainingUser({idTrainingUser: retrieveIdTrainingUser[0].training_user_id})
                                res.json({error: false, result: result})
                            }
                            })
                        })
    
                }
                })
              }
              else {
                  let form = {
                      image: `${env.APP_URL}/training/membership/photo/${req.file.filename}`
                  }
  
                  let dataMember = await db.query(`SELECT u.name, c.company_name, tc.name AS training_company_name, c.logo, u.license_number, m.expired
                  FROM training_membership m
                  JOIN training_user u ON u.id = m.training_user_id
                  JOIN training_company tc ON tc.id = u.training_company_id
                  JOIN company c ON c.company_id = tc.company_id
                  WHERE m.id='${req.params.id}'`);
                  let rndm =generateString(5);
                  nodeHtmlToImage({
                      output: `./public/training/membership/license_card/${moment(new Date()).format('YYYY-MM-DD')+'-'+req.params.id+'-'+rndm}.png`,
                      html: html,
                      content: {
                          image_card: dataLicense[0].image_card.length ? dataLicense[0].image_card : `${env.APP_URL}/training/membership/card.svg`,
                          logo: dataMember[0].logo,
                          photo: form.image,
                          company: dataMember[0].company_name,
                          name: dataMember[0].name,
                          training_company: dataMember[0].training_company_name,
                          license: dataMember[0].license_number,
                          expired: moment(new Date(dataMember[0].expired)).format('DD/MM/YYYY')
                      },
                      puppeteerArgs: {
                        args: ['--no-sandbox', '--headless'], ignoreHTTPSErrors: true
                      }
                    })
                      .then(() =>{
                          db.query(`UPDATE training_membership SET photo = '${form.image}', license_card = '${env.APP_URL}/training/membership/license_card/${moment(new Date()).format('YYYY-MM-DD')+'-'+req.params.id+'-'+rndm}.png' WHERE id = '${req.params.id}'`, async (error, result, fields) => {
                          if(error) {
                              res.json({error: true, result: error})
                          } else {
                            await moduleDB.updatePhotoTakenByIdTrainingUser({idTrainingUser: retrieveIdTrainingUser[0].training_user_id})
                            res.json({error: false, result: result})
                          }
                          })
                      })
              }
  
      }
    })
}
exports.delete = (req, res) => {
    db.query(
        `UPDATE training_membership
        SET status='Inactive'
        WHERE id = ?;`,
        [req.params.id],
        (error, result) => {
            if(error) {
                return res.json({error: true, result: error.message});
            };

            db.query(
                `UPDATE training_user
                INNER JOIN training_membership ON training_user.id = training_membership.training_user_id
                SET training_user.license_number = ''
                WHERE training_membership.id = ?;`,
                [req.params.id],
                (error, result) => {
                    if(error) {
                        return res.json({error: true, result: error.message});
                    };
        
                    return res.json({error: false, result});
                }
            );
        }
    );
};
exports.activate = (req, res) => {
    db.query(
        `UPDATE training_membership
        SET status='Active'
        WHERE id = ?;`,
        [req.params.id],
        (error, result) => {
            if(error) {
                return res.json({error: true, result: error.message});
            };

            db.query(
                `UPDATE training_user
                INNER JOIN training_membership ON training_user.id = training_membership.training_user_id
                SET training_user.license_number = IF(training_membership.license_number != '', training_membership.license_number, training_user.license_number)
                WHERE training_membership.id = ?;`,
                [req.params.id],
                (error, result) => {
                    if(error) {
                        return res.json({error: true, result: error.message});
                    };
        
                    return res.json({error: false, result});
                }
            );
        }
    );
};