var env = require('../env.json');
var conf = require('../configs/config');
var db = require('../configs/database');

exports.getKurikulumByCompany = (req, res) => {
  let sql = `SELECT * FROM learning_kurikulum WHERE company_id = ?`;
  db.query(sql, [req.params.company_id], async (err, rows) => {
    if(err) res.json({ error: true, result: err })

    if(rows.length) {
      for(var i=0; i<rows.length; i++) {
        let mpl = `SELECT lp.kode_pelajaran, lp.nama_pelajaran, lp.* FROM learning_kurikulum_mapel lkm
          JOIN learning_pelajaran lp ON lkm.mapel_id = lp.pelajaran_id
          WHERE lkm.kurikulum_id = ?`
        let mapel = await db.query(mpl, [rows[i].id])
        rows[i].mapel = mapel;
      }

      res.json({ error: false, result: rows })
    }
    else {
      res.json({ error: false, result: rows })
    }
  })
}

exports.createKurikulum = (req, res) => {
  let sql = `INSERT INTO learning_kurikulum (company_id, name) VALUES (?)`;
  let data = [[req.body.company_id, req.body.name]];

  db.query(sql, data, (err, rows) => {
    if(err) res.json({ error: true, result: err })

    res.json({ error: false, result: rows })
  })
}

exports.updateKurikulum = (req, res) => {
  let sql = `UPDATE learning_kurikulum SET name = ? WHERE id = ?`
  let dara = [req.body.name, req.params.id]
  db.query(sql, dara, (err, rows) => {
    if(err) res.json({ error: true, result: err })
    res.json({ error: false, result: rows })
  })
}

exports.deleteKurikulum = (req, res) => {
  let sql = `DELETE FROM learning_kurikulum WHERE id = ?`
  let dara = [req.params.id]
  db.query(sql, dara, (err, rows) => {
    if(err) res.json({ error: true, result: err })
    res.json({ error: false, result: rows })
  })
}

exports.addMapelById = async (req, res) => {
  let id = req.params.id;
  let mapel = req.body.mapel

  await db.query(`DELETE FROM learning_kurikulum_mapel WHERE kurikulum_id='${id}'`)
  let data = [];
  for(var i=0; i<mapel.length; i++) {
    let replace = `
      INSERT INTO learning_kurikulum_mapel (kurikulum_id, mapel_id)
      SELECT ?, ?
      FROM DUAL
      WHERE NOT EXISTS(
        SELECT 1
        FROM learning_kurikulum_mapel
        WHERE kurikulum_id = ? AND mapel_id = ?
      )
      LIMIT 1`
    await db.query(replace, [id, mapel[i], id, mapel[i]]);

    data.push([id, mapel[i]]);
  }

  res.json({error: false, result: 'OK'})

  // let sql = `INSERT INTO learning_kurikulum_mapel (kurikulum_id, mapel_id) VALUES ?`;
  // db.query(sql, [data], (err, rows) => {
    // if(err) res.json({ error: true, result: err })
    // res.json({ error: false, result: rows })
  // })
}

exports.getKurikulumByKurikulum = (req, res) => {
  let sql = `SELECT lk.*, lkm.mapel_id, lp.nama_pelajaran, lp.kategori, lp.kode_pelajaran
    FROM learning_kurikulum lk
    	JOIN learning_kurikulum_mapel lkm ON lk.id = lkm.kurikulum_id
      JOIN learning_pelajaran lp ON lp.pelajaran_id = lkm.mapel_id
    WHERE lk.id = ?`
  let data = [req.params.id];
  db.query(sql, data, (err, rows) => {
    if(err) res.json({error: true, result: err})

    res.json({ error: false, result: rows })
  })
}

exports.deleteMapelKurikulum = (req, res) => {
  let id = req.params.id;
  let mapel = req.params.mapel;
  let sql = `DELETE FROM learning_kurikulum_mapel WHERE kurikulum_id = ? AND mapel_id = ?`
  db.query(sql, [id, mapel], (err, rows) => {
    if(err) res.json({ error: true, result: err })
    res.json({ error: false, result: rows })
  })
}
