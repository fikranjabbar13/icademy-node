const multer = require("multer");
const moment = require("moment");
const pdf = require("html-pdf");
const path = require("path");
const env = require("../env.json");
const db = require("../configs/database");
const send = require("./helper/sendEmail");
const AWS = require("aws-sdk");
const fs = require("fs");
const s3 = new AWS.S3({
  accessKeyId: env.AWS_ACCESS,
  secretAccessKey: env.AWS_SECRET,
});
const sendMailQueue = require("../configs/bullqueue");
const options_queue = {
  delay: 4000,
  attempts: 2,
  backoff: 20000,
};

const checkAccess = require("../middleware").checkAccess;
const validatorRequest = require('../validator');
const FilePathREG = path.join(__basedir, `../public/webinar/registration-guest`);
const moduleDB = require('../repository');

const storageBG = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "./public/webinar/background");
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname);
  },
});

const storageSlip = multer.diskStorage({
  destination: (req, file, cb) => {

    if (!fs.existsSync(FilePathREG)) {
      fs.mkdirSync(FilePathREG);
    }
    cb(null, "./public/webinar/registration-guest");
  },
  filename: function (req, file, cb) {
    cb(null, `registration-${Date.now()}-${file.originalname}`);
  },
});
const storage = multer.diskStorage({
  filename: function (req, file, cb) {
    cb(null, file.originalname);
  },
});
let uploadImage = multer({ storage: storageBG }).single("image");
let uploadSlip = multer({ storage: storageSlip }).single("slip");
let upload = multer({
  storage: storage,
  limits: { fileSize: 500000000 },
}).single("gambar");

var storageEx = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "./public/webinar/excel");
  },
  filename: (req, file, cb) => {
    cb(null, "excel-" + Date.now() + "-" + file.originalname);
  },
});
var uploadExcel = multer({ storage: storageEx }).single("file");

exports.import = (req, res) => {
  uploadExcel(req, res, err => {
    try {
      if (!req.file) {
        res.json({ error: true, result: err });
      } else {
        // create class Excel
        var Excel = require("exceljs");
        var wb = new Excel.Workbook();
        var path = require("path");
        var filePath = path.resolve(
          __dirname,
          "../public/webinar/excel/" + req.file.filename
        );

        var form = {
          webinar_id: req.body.webinar_id,
        };

        wb.xlsx.readFile(filePath).then(async function () {
          var sh = wb.getWorksheet("Sheet1");
          //console.log("countRow: ", sh.rowCount);

          let inserted = [];
          for (let i = 4; i <= sh.rowCount; i++) {
            if (sh.getRow(i).getCell(1).value !== null) {
              let emailParsed =
                typeof sh.getRow(i).getCell(2).value === "object" &&
                  typeof sh.getRow(i).getCell(2).value !== null
                  ? sh.getRow(i).getCell(2).value.text
                  : sh.getRow(i).getCell(2).value;

              let sqlString = `INSERT INTO webinar_tamu (webinar_id, name, email, phone, status, voucher)
                                      VALUES (?)`;
              let formInsert = [form.webinar_id, sh.getRow(i).getCell(1).value, emailParsed, sh.getRow(i).getCell(3).value, 0, Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15)]

              const response = await new Promise((resolve, reject) => {
                db.query(sqlString, [formInsert], async (error, result) => {
                  if (error) reject(error);
                  resolve(result);
                });
              })

              if (response.insertId) {
                inserted.push(response.insertId);
              } else {
                await db.query(`DELETE FROM webinar_tamu WHERE id IN (${inserted})`);
                res.json({
                  error: true,
                  result: `Error : failed to import row ${i}, all row has been canceled`,
                  row: i,
                });
                break;
              }

            } else {
              res.json({ error: false, result: "Success import data" });
              break;
            }
          }
        });
      }
    } catch (error) {
      res.json({ error: true, result: error.message });
    }
  });
};

exports.uploadImage = (req, res) => {
  uploadImage(req, res, err => {
    if (!req.file) {
      res.json({ error: true, result: err });
    } else {
      if (env.AWS_BUCKET) {
        let path = req.file.path;
        let newDate = new Date();
        let keyName = `${env.AWS_BUCKET_ENV}/webinar/background`;

        var params = {
          ACL: "public-read",
          Bucket: env.AWS_BUCKET,
          Body: fs.createReadStream(path),
          Key: `${keyName}/${Date.now()}-${req.file.originalname}`,
          ContentDisposition: "inline",
          ContentType: req.file.mimetype,
        };

        s3.upload(params, (err, data) => {
          if (err) {
            res.json({ error: true, msg: "Error Upload Image", result: err });
          }
          if (data) {
            fs.unlinkSync(path);
            let form = {
              image: data.Location,
            };
            db.query(
              `UPDATE webinars SET certificate_background = '${form.image}' WHERE id = '${req.params.id}'`,
              (error, result, fields) => {
                if (error) {
                  res.json({ error: true, result: error });
                } else {
                  res.json({ error: false, result: result });
                }
              }
            );
          }
        });
      } else {
        let form = {
          image: `${env.APP_URL}/training/user/${req.file.filename}`,
        };
        db.query(
          `UPDATE webinars SET certificate_background = '${form.image}' WHERE id = '${req.params.id}'`,
          (error, result, fields) => {
            if (error) {
              res.json({ error: true, result: error });
            } else {
              res.json({ error: false, result: result });
            }
          }
        );
      }
    }
  });
};
exports.getRole = (req, res, next) => {
  db.query(
    `SELECT *
			FROM webinar_roles
			WHERE company_id = '${req.params.company_id}'
			AND project_id = '${req.params.project_id}'
			ORDER BY id DESC`,
    (error, result, fields) => {
      if (error) res.json({ error: true, result: error });

      res.json({ error: false, result: result });
    }
  );
};

exports.createRole = (req, res, next) => {
  let form = {
    name: req.body.name,
    company_id: req.body.companyId,
    user_id: req.body.userId,
    project_id: req.body.projectId,
  };

  let sql = `
		INSERT INTO webinar_roles (name, project_id, user_id, company_id) VALUES ('${form.name}','${form.project_id}','${form.user_id}','${form.company_id}')
	`;
  db.query(sql, (error, result, fields) => {
    if (error) res.json({ error: true, result: error });

    db.query(
      `SELECT * FROM webinar_roles WHERE id = '${result.insertId}'`,
      (error, result, fields) => {
        res.json({ error: false, result: result });
      }
    );
  });
};

exports.updateRole = (req, res, next) => {
  let sql = `UPDATE webinar_roles SET name = '${req.body.name}' WHERE id = '${req.params.id}'`;
  db.query(sql, (error, result, fields) => {
    if (error) res.json({ error: true, result: error });

    db.query(
      `SELECT * FROM webinar_roles WHERE id = '${req.params.id}'`,
      (error, result, fields) => {
        res.json({ error: false, result: result });
      }
    );
  });
};

exports.deleteRole = (req, res, next) => {
  let sql = `DELETE FROM webinar_roles WHERE id = '${req.params.id}'`;
  db.query(sql, (error, result, fields) => {
    if (error) res.json({ error: true, result: error });

    res.json({ error: false, result: result });
  });
};

exports.getWebinarsByProject = (req, res, next) => {
  db.query(
    `SELECT 
      id,company_id,judul,sekretaris,moderator,owner,pembicara,
      gambar,tanggal,jam_mulai,jam_selesai,start_time,end_time,
      project_id,dokumen_id,status,training_course_id,publish,created_at 

			FROM webinars
			WHERE project_id = '${req.params.project_id}' AND publish='1'
			ORDER BY id DESC`,
    async (error, result, fields) => {
      if (error) res.json({ error: true, result: error });

      if (result.length == 0) {
        res.json({ error: true, result: "Tidak ada webinar yang tersedia" });
      } else {

        let isCreateWebinar = false;
        if (req.app.token.level === 'superadmin') {
          isCreateWebinar = true;
        } else {
          let roles = req.app.token.level === 'client' ? 'user' : req.app.token.level;
          req.app.accessInfo.forEach((arg) => {
            if (arg.role === roles && arg.sub === 'webinar' && arg.code === 'CD_WEBINAR' && arg.status) {
              isCreateWebinar = true;
            }
          })
        }

        for (var i = 0; i < result.length; i++) {
          result[i].isEdit = false;
          result[i].isDelete = false;
          result[i].isCrudRoles = false;
          result[i].isDetail = false;
          result[i].isRole = null;
          result[i].external_speaker = [];

          let moderator = await db.query(
            `SELECT user_id, name FROM user WHERE user_id IN (${result[i].moderator})`
          );
          result[i].moderator =
            moderator.length != 0
              ? moderator
              : { user_id: result[i].moderator, name: "Anonymous" };

          let sekretaris = await db.query(
            `SELECT user_id, name FROM user WHERE user_id IN (${result[i].sekretaris})`
          );
          result[i].sekretaris =
            sekretaris.length != 0
              ? sekretaris
              : { user_id: result[i].sekretaris, name: "Anonymous" };

          let split = result[i].pembicara.split(",");

          let tmp = { object: [], user_id: [], guest: [], external: [] }
          for (let j = 0; j < split.length; j++) {
            if (split[j] !== "") {
              if(split[j].search('external') > -1){
                tmp.external.push(split[j]);
                tmp.object.push({ user_id: split[j], name: "Anonymous", type: "external", email: null, phone: null,status:0 });
              }else if (isNaN(Number(split[j]))) {
                tmp.guest.push(split[j]);
                tmp.object.push({ user_id: split[j], name: "Anonymous", type: "guest", email: null, phone: null,status:0 });
              } else {
                tmp.user_id.push(split[j]);
                tmp.object.push({ user_id: split[j], name: "Anonymous", type: "peserta", email: null, phone: null,status:0 });
              }
            }
          }

          if (split.length > 0 && tmp.object.length > 0) {

            let queryString = '';
            let params = [];
            if (tmp.user_id.length > 0) {
              queryString = `SELECT user_id, name, email,phone FROM user WHERE user_id IN(?);`;
              params.push(tmp.user_id);
            }
            if (tmp.guest.length > 0) {
              queryString = `${queryString}SELECT wt.voucher as user_id, wt.name ,wt.email,wt.phone from webinar_tamu wt where wt.voucher in(?);`;
              params.push(tmp.guest);
            }
            if (tmp.external.length > 0) {
              queryString = `${queryString}SELECT wt.voucher as user_id, wt.name ,wt.email,wt.phone, wt.status from webinar_role_external wt where wt.voucher in(?);`;
              params.push(tmp.external);
            }

            let tmpPembicara = [];
            let pembicara = await db.query(queryString, params);

            if(pembicara && pembicara.length > 1){
              if(pembicara[0]){
                tmpPembicara = tmpPembicara.concat(pembicara[0]);
              }
              if(pembicara[1]){
                tmpPembicara = tmpPembicara.concat(pembicara[1]);
              }
              if(pembicara[2]){
                tmpPembicara = tmpPembicara.concat(pembicara[2]);
              }
              pembicara = tmpPembicara;
            }

            try {
              tmp.object.forEach((str, i) => {
                let idx = pembicara.findIndex((check) => { return check.user_id == str.user_id });
                if (idx > -1) {
                  tmp.object[i].name = pembicara[idx].name;
                  tmp.object[i].email = pembicara[idx].email;
                  tmp.object[i].phone = pembicara[idx].phone;
                  if(pembicara[idx].status){
                  tmp.object[i].status = pembicara[idx].status;
                  }
                }
              });
            } catch (e) {
              // handle error undefined variable
            }

            // build peserta_speaker && guest_speaker
            result[i].peserta_speaker = [];
            result[i].guest_speaker = [];
            tmp.object.forEach((str) => {
              if(str.type === "external"){
                result[i].external_speaker.push(str);
              }else if (str.type === "guest") {
                result[i].guest_speaker.push(str);
              } else {
                result[i].peserta_speaker.push(str);
              }
            });
            result[i].pembicara = tmp.object;
          } else {
            result[i].pembicara = [];
          }
          // let pembicara = await db.query(
          //   `SELECT user_id, name FROM user WHERE user_id IN (${result[i].pembicara})`
          // );
          // result[i].pembicara =
          //   pembicara.length != 0
          //     ? pembicara
          //     : { user_id: result[i].pembicara, name: "Anonymous" };

          let owner = await db.query(
            `SELECT user_id, name FROM user WHERE user_id IN (${result[i].owner})`
          );
          result[i].owner =
            owner.length != 0
              ? owner
              : { user_id: result[0].owner, name: "Anonymous" };

          let peserta = await db.query(
            `SELECT w.*, u.name FROM webinar_peserta w JOIN user u ON u.user_id = w.user_id WHERE webinar_id = '${result[i].id}'`
          );
          result[i].peserta = peserta;

          let tamu = await db.query(
            `SELECT * FROM webinar_tamu WHERE webinar_id='${result[i].id}'`
          );
          result[i].tamu = tamu;

          // project admin
          let project_admin = await db.query(
            `SELECT u.user_id, u.name 
            FROM files_folder_user ffu
            left join user u on u.user_id = ffu.user_id
            WHERE 
            lower(ffu.role) = 'project admin' and folder_id = '${result[i].project_id}';`
          );
          result[i].project_admin = project_admin;

          // ACCESS
          let roles = ['moderator', 'pembicara', 'sekretaris', 'peserta'];
          let state = {};
          roles.forEach((arg1) => {
            if (result[i][arg1].length > 0) {
              let idx = result[i][arg1].findIndex((arg) => { return req.app.token.user_id.toString() === arg.user_id.toString(); });
              if (idx > -1) {
                result[i].isRole = arg1;
                state[arg1] = true;

                let setName = null;
                if(arg1 === 'pembicara'){
                  setName = `is_speaker`;
                }
                else if(arg1 === 'peserta'){
                  setName = `is_participant`;
                }else{
                  setName = `is_${arg1}`;
                }

                result[i][setName] = true;
              }
            }
          });

          // pilih salah satu
          try {
            if (state.moderator) {
              result[i].isRole = 'moderator';
              result[i].isJoin = true;
            }
            if (state.sekretaris) {
              result[i].isRole =  result[i].isRole == 'moderator' ? result[i].isRole : 'sekretaris';
              result[i].isJoin = true;
              result[i].isDetail = true;
            } else if (req.app.token.user_id === result[i].created_by || state.owner) {
              result[i].isRole = 'owner';
              result[i].isJoin = true;
            } else if (state.pembicara) {
              result[i].isRole = 'speaker';
              result[i].isJoin = true;
            } else if (state.peserta) {
              result[i].isRole = 'participant';
              result[i].isJoin = true;
            }
          } catch (e) { }

          if (req.app.token.level === 'superadmin' || (req.app.token.level === 'admin' && !result[0].isRole)) {
            result[i].isEdit = true;
            result[i].isDelete = true;
            result[i].isDetail = true;
            result[i].isJoin = true;
            result[i].isRole = req.app.token.level;
            let roles = result[i].isRole.toLowerCase().replace(' ','_');
            result[i][`is_${roles}`] = true;
            if (req.app.token.level === 'admin') {
              result[i].isDetail = false;
              result[i].isJoin = false;
            }

          } else {

            let idx = -1;
            if (result[i].project_admin.length > 0) {
              idx = result[i].project_admin.findIndex((str) => { return str.user_id == req.app.token.user_id });
              if (idx > -1) {
                result[i].isEdit = true;
                result[i].isDelete = true;
                if(!result[i].isRole)
                    result[i].isRole = req.app.token.level;
                    let roles = result[i].isRole.toLowerCase().replace(' ','_');
                    result[i][`is_${roles}`] = true;
              }
            }

            if (idx == -1) {
              req.app.accessInfo.forEach((arg) => {

                if (arg.role === result[i].isRole && arg.sub === 'webinar' && arg.code === 'CD_WEBINAR' && arg.status) {
                  result[i].isEdit = true;
                  result[i].isDelete = true;
                }
                if (arg.role === result[i].isRole && arg.sub === 'webinar' && arg.code === 'DETAIL_WEBINAR' && arg.status) {
                  result[i].isDetail = true;
                }
                if (arg.role === result[i].isRole && arg.sub === 'webinar' && arg.code === 'CRUD_ROLES' && arg.status) {
                  result[i].isCrudRoles = true;
                }
              })
            }
          }
        }

        let tmp = [];
        if(result.length){
          result.forEach((str)=>{
            if(str.isRole){
              tmp.push(str);
            }
          });
          result = tmp;
        }
        
        res.json({ error: false, result: result, access: { isCreateWebinar: isCreateWebinar } });
      }
    }
  );
};

exports.getWebinarsByCompany = (req, res, next) => {
  db.query(
    `SELECT *
			FROM webinars
			WHERE company_id = '${req.params.company_id}' AND publish='1' AND (training_course_id IS NULL OR training_course_id='0') and is_training_liveclass = 0 
			ORDER BY id DESC`,
    async (error, result, fields) => {
      if (error) res.json({ error: true, result: error });

      if (result.length == 0) {
        res.json({ error: true, result: "Tidak ada webinar yang tersedia" });
      } else {
        for (var i = 0; i < result.length; i++) {

          result[i].isEdit = false;
          result[i].isDelete = false;
          result[i].isCrudRoles = false;
          result[i].isDetail = false;
          result[i].isRole = null;
          result[i].external_speaker = [];

          let moderator = await db.query(
            `SELECT user_id, name FROM user WHERE user_id IN (${result[i].moderator})`
          );
          result[i].moderator =
            moderator.length != 0
              ? moderator
              : [{ user_id: result[i].moderator, name: "Anonymous" }];

          let sekretaris = await db.query(
            `SELECT user_id, name FROM user WHERE user_id IN (${result[i].sekretaris})`
          );
          result[i].sekretaris =
            sekretaris.length != 0
              ? sekretaris
              : { user_id: result[i].sekretaris, name: "Anonymous" };

          let split = result[i].pembicara.split(",");

          let tmp = { object: [], user_id: [], guest: [], external: [] }
          for (let j = 0; j < split.length; j++) {
            if (split[j] !== "") {
              if(split[j].search('external') > -1){
                tmp.external.push(split[j]);
                tmp.object.push({ user_id: split[j], name: "Anonymous", type: "external", email: null, phone: null,status:0 });
              }else if (isNaN(Number(split[j]))) {
                tmp.guest.push(split[j]);
                tmp.object.push({ user_id: split[j], name: "Anonymous", type: "guest", email: null, phone: null,status:0 });
              } else {
                tmp.user_id.push(split[j]);
                tmp.object.push({ user_id: split[j], name: "Anonymous", type: "peserta", email: null, phone: null,status:0 });
              }
            }
          }

          if (split.length > 0 && tmp.object.length > 0) {

            let queryString = '';
            let params = [];
            if (tmp.user_id.length > 0) {
              queryString = `SELECT user_id, name, email,phone FROM user WHERE user_id IN(?);`;
              params.push(tmp.user_id);
            }
            if (tmp.guest.length > 0) {
              queryString = `${queryString}SELECT wt.voucher as user_id, wt.name ,wt.email,wt.phone from webinar_tamu wt where wt.voucher in(?);`;
              params.push(tmp.guest);
            }
            if (tmp.external.length > 0) {
              queryString = `${queryString}SELECT wt.voucher as user_id, wt.name ,wt.email,wt.phone, wt.status from webinar_role_external wt where wt.voucher in(?);`;
              params.push(tmp.external);
            }

            let tmpPembicara = [];
            let pembicara = await db.query(queryString, params);

            if(pembicara && pembicara.length > 1){
              if(pembicara[0]){
                tmpPembicara = tmpPembicara.concat(pembicara[0]);
              }
              if(pembicara[1]){
                tmpPembicara = tmpPembicara.concat(pembicara[1]);
              }
              if(pembicara[2]){
                tmpPembicara = tmpPembicara.concat(pembicara[2]);
              }
              pembicara = tmpPembicara;
            }

            try {
              tmp.object.forEach((str, i) => {
                let idx = pembicara.findIndex((check) => { return check.user_id == str.user_id });
                if (idx > -1) {
                  tmp.object[i].name = pembicara[idx].name;
                  tmp.object[i].email = pembicara[idx].email;
                  tmp.object[i].phone = pembicara[idx].phone;
                  if(pembicara[idx].status){
                  tmp.object[i].status = pembicara[idx].status;
                  }
                }
              });
            } catch (e) {
              // handle error undefined variable
            }

            // build peserta_speaker && guest_speaker
            result[i].peserta_speaker = [];
            result[i].guest_speaker = [];
            tmp.object.forEach((str) => {
              if(str.type === "external"){
                result[i].external_speaker.push(str);
              }else if (str.type === "guest") {
                result[i].guest_speaker.push(str);
              } else {
                result[i].peserta_speaker.push(str);
              }
            });
            result[i].pembicara = tmp.object;
          } else {
            result[i].pembicara = [];
          }
          // let pembicara = await db.query(
          //   `SELECT user_id, name FROM user WHERE user_id IN (${result[i].pembicara})`
          // );
          // result[i].pembicara =
          //   pembicara.length != 0
          //     ? pembicara
          //     : { user_id: result[i].pembicara, name: "Anonymous" };

          let owner = await db.query(
            `SELECT user_id, name FROM user WHERE user_id IN (${result[i].owner})`
          );
          result[i].owner =
            owner.length != 0
              ? owner
              : { user_id: result[0].owner, name: "Anonymous" };

          let peserta = await db.query(
            `SELECT w.*, u.name FROM webinar_peserta w JOIN user u ON u.user_id = w.user_id WHERE webinar_id = '${result[i].id}'`
          );
          result[i].peserta = peserta;

          let tamu = await db.query(
            `SELECT * FROM webinar_tamu WHERE webinar_id='${result[i].id}'`
          );
          result[i].tamu = tamu;

          // project admin
          let project_admin = await db.query(
            `SELECT u.user_id, u.name 
            FROM files_folder_user ffu
            left join user u on u.user_id = ffu.user_id
            WHERE 
            lower(ffu.role) = 'project admin' and folder_id = '${result[i].project_id}';`
          );
          result[i].project_admin = project_admin;

          // ACCESS
          let roles = ['moderator', 'pembicara', 'sekretaris', 'peserta'];
          let state = {};
          roles.forEach((arg1) => {
            if (result[i][arg1].length > 0) {
              let idx = result[i][arg1].findIndex((arg) => { return req.app.token.user_id.toString() === arg.user_id.toString(); });
              if (idx > -1) {
                result[i].isRole = arg1;
                state[arg1] = true;

                let setName = null;
                if(arg1 === 'pembicara'){
                  setName = `is_speaker`;
                }
                else if(arg1 === 'peserta'){
                  setName = `is_participant`;
                }else{
                  setName = `is_${arg1}`;
                }

                result[i][setName] = true;
              }
            }
          });

          // pilih salah satu
          try {
            if (state.moderator) {
              result[i].isRole = 'moderator';
              result[i].isJoin = true;
            }if (state.sekretaris) {
              result[i].isRole =  result[i].isRole == 'moderator' ? result[i].isRole : 'sekretaris';
              result[i].isJoin = true;
              result[i].isDetail = true;
            } else if (req.app.token.user_id === result[i].created_by || state.owner) {
              result[i].isRole = 'owner';
              result[i].isJoin = true;
            } else if (state.pembicara) {
              result[i].isRole = 'speaker';
              result[i].isJoin = true;
            } else if (state.peserta) {
              result[i].isRole = 'participant';
              result[i].isJoin = true;
            }
          } catch (e) { }

          if (req.app.token.level === 'superadmin' || (req.app.token.level === 'admin' && !result[0].isRole)) {
            result[i].isEdit = true;
            result[i].isDelete = true;
            result[i].isDetail = true;
            result[i].isJoin = true;
            result[i].isRole = req.app.token.level;
            let roles = result[i].isRole.toLowerCase().replace(' ','_');
            result[i][`is_${roles}`] = true;
            if (req.app.token.level === 'admin') {
              result[i].isDetail = false;
              result[i].isJoin = false;
            }

          } else {

            let idx = -1;
            if (result[i].project_admin.length > 0) {
              idx = result[i].project_admin.findIndex((str) => { return str.user_id == req.app.token.user_id });
              if (idx > -1) {
                result[i].isEdit = true;
                result[i].isDelete = true;
                if(!result[i].isRole){
                  result[i].isRole = req.app.token.level;
                  let roles = result[i].isRole.toLowerCase().replace(' ','_');
                  result[i][`is_${roles}`] = true;
                }
              }
            }

            if (idx == -1) {
              req.app.accessInfo.forEach((arg) => {

                if (arg.role === result[i].isRole && arg.sub === 'webinar' && arg.code === 'CD_WEBINAR' && arg.status) {
                  result[i].isEdit = true;
                  result[i].isDelete = true;
                }
                if (arg.role === result[i].isRole && arg.sub === 'webinar' && arg.code === 'DETAIL_WEBINAR' && arg.status) {
                  result[i].isDetail = true;
                }
                if (arg.role === result[i].isRole && arg.sub === 'webinar' && arg.code === 'CRUD_ROLES' && arg.status) {
                  result[i].isCrudRoles = true;
                }
              })
            }
          }
        }
        res.json({ error: false, result: result });
      }
    }
  );
};

exports.getWebinarsByTraining = (req, res, next) => {
  db.query(
    `SELECT *
			FROM webinars
			WHERE company_id = '${req.params.company_id}' AND publish='1' AND training_course_id IS NOT NULL and is_training_liveclass = 1 
			ORDER BY id DESC`,
    async (error, result, fields) => {
      if (error) res.json({ error: true, result: error });

      if (result.length == 0) {
        res.json({ error: true, result: "Tidak ada webinar yang tersedia" });
      } else {
        for (var i = 0; i < result.length; i++) {

          result[i].peserta_speaker = [];
          result[i].guest_speaker = [];
          result[i].external_speaker = [];
          result[i].isEdit = false;
          result[i].isDelete = false;
          result[i].isCrudRoles = false;
          result[i].isDetail = false;
          result[i].isCreateWebinarTest = false;
          result[i].isSendWebinarTest = false;
          result[i].isAcceptWebinarTest = false;
          result[i].isAccessDoorprize = false;
          result[i].isUploadFileProject = false;
          result[i].isReadFileProject = false;
          result[i].isAccessQuestionForm = false;
          result[i].isRole = null;

          let moderator = await db.query(
            `SELECT user_id, name FROM user WHERE user_id IN (${result[i].moderator})`
          );
          result[i].moderator =
            moderator.length != 0
              ? moderator
              : { user_id: result[i].moderator, name: "Anonymous" };

          let sekretaris = await db.query(
            `SELECT user_id, name FROM user WHERE user_id IN (${result[i].sekretaris})`
          );
          result[i].sekretaris =
            sekretaris.length != 0
              ? sekretaris
              : { user_id: result[i].sekretaris, name: "Anonymous" };

          // let pembicara = await db.query(
          //   `SELECT user_id, name FROM user WHERE user_id IN ('${result[i].pembicara}')`
          // );
          // result[i].pembicara =
          //   pembicara.length != 0
          //     ? pembicara
          //     : { user_id: result[i].pembicara, name: "Anonymous" };
          result[i].guest_speaker = [];
          result[i].peserta_speaker = [];
          let split = result[i].pembicara.split(",");

          let tmp = { object: [], user_id: [], guest: [], external: [] }
          for (let j = 0; j < split.length; j++) {
            if (split[j] !== "") {
              if(split[j].search('external') > -1){
                tmp.external.push(split[j]);
                tmp.object.push({ user_id: split[j], name: "Anonymous", type: "external", email: null, phone: null,status:0 });
              }else if (isNaN(Number(split[j]))) {
                tmp.guest.push(split[j]);
                tmp.object.push({ user_id: split[j], name: "Anonymous", type: "guest", email: null, phone: null,status:0 });
              } else {
                tmp.user_id.push(split[j]);
                tmp.object.push({ user_id: split[j], name: "Anonymous", type: "peserta", email: null, phone: null,status:0 });
              }
            }
          }

          if (split.length > 0 && tmp.object.length > 0) {

            let queryString = '';
            let params = [];
            if (tmp.user_id.length > 0) {
              queryString = `SELECT user_id, name, email,phone FROM user WHERE user_id IN(?);`;
              params.push(tmp.user_id);
            }
            if (tmp.guest.length > 0) {
              queryString = `${queryString}SELECT wt.voucher as user_id, wt.name ,wt.email,wt.phone from webinar_tamu wt where wt.voucher in(?);`;
              params.push(tmp.guest);
            }
            if (tmp.external.length > 0) {
              queryString = `${queryString}SELECT wt.voucher as user_id, wt.name ,wt.email,wt.phone, wt.status from webinar_role_external wt where wt.voucher in(?);`;
              params.push(tmp.external);
            }

            let tmpPembicara = [];
            let pembicara = await db.query(queryString, params);

            if(pembicara && pembicara.length > 1){
              if(pembicara[0]){
                tmpPembicara = tmpPembicara.concat(pembicara[0]);
              }
              if(pembicara[1]){
                tmpPembicara = tmpPembicara.concat(pembicara[1]);
              }
              if(pembicara[2]){
                tmpPembicara = tmpPembicara.concat(pembicara[2]);
              }
              pembicara = tmpPembicara;
            }

            try {
              tmp.object.forEach((str, i) => {
                let idx = pembicara.findIndex((check) => { return check.user_id == str.user_id });
                if (idx > -1) {
                  tmp.object[i].name = pembicara[idx].name;
                  tmp.object[i].email = pembicara[idx].email;
                  tmp.object[i].phone = pembicara[idx].phone;
                  if(pembicara[idx].status){
                  tmp.object[i].status = pembicara[idx].status;
                  }
                }
              });
            } catch (e) {
              // handle error undefined variable
            }

            // build peserta_speaker && guest_speaker
            tmp.object.forEach((str) => {
              if(str.type === "external"){
                result[i].external_speaker.push(str);
              }else if (str.type === "guest") {
                result[i].guest_speaker.push(str);
              } else {
                result[i].peserta_speaker.push(str);
              }
            });
            result[i].pembicara = tmp.object;
          } else {
            result[i].pembicara = [];
          }

          let owner = await db.query(
            `SELECT user_id, name FROM user WHERE user_id IN (${result[i].owner})`
          );
          result[i].owner =
            owner.length != 0
              ? owner
              : { user_id: result[i].owner, name: "Anonymous" };

          let peserta = await db.query(
            `SELECT w.*, u.name FROM webinar_peserta w JOIN user u ON u.user_id = w.user_id WHERE webinar_id = '${result[i].id}'`
          );
          result[i].peserta = peserta;

          let tamu = await db.query(
            `SELECT * FROM webinar_tamu WHERE webinar_id='${result[i].id}'`
          );
          result[i].tamu = tamu;


          let project_admin = await db.query(
            `SELECT u.user_id, u.name 
            FROM files_folder_user ffu
            left join user u on u.user_id = ffu.user_id
            WHERE 
            lower(ffu.role) = 'project admin' and folder_id = '${result[i].project_id}';`
          );
          result[i].project_admin = project_admin;


          // ACCESS
          // let roles = ['pembicara', 'peserta', 'tamu'];
          // let state = { pembicara: false, tamu: false };
          // roles.forEach((arg1) => {
          //   if (result[i][arg1].length > 0) {
          //     let UserLogin = req.query.voucher;
          //     let idx = result[i][arg1].findIndex((arg) => { return UserLogin === arg.voucher && arg.voucher; });

          //     if (idx > -1) {
          //       result[i].isRole = arg1;
          //       state[arg1] = true;
          //     }
          //   }
          // });
          // ACCESS
          let roles = ['moderator', 'pembicara', 'sekretaris', 'peserta'];
          let state = {};
          roles.forEach((arg1) => {
            if (result[i][arg1].length > 0) {
              let idx = result[i][arg1].findIndex((arg) => { return req.app.token.user_id.toString() === arg.user_id.toString(); });
              if (idx > -1) {
                result[i].isRole = arg1;
                state[arg1] = true;

                let setName = null;
                if(arg1 === 'pembicara'){
                  setName = `is_speaker`;
                }
                else if(arg1 === 'peserta'){
                  setName = `is_participant`;
                }else{
                  setName = `is_${arg1}`;
                }

                result[i][setName] = true;
              }
            }
          });

          // pilih salah satu
          // try {
          //   if (state.moderator) {
          //     result[i].isRole = 'moderator';
          //     result[i].isJoin = true;
          //     result[i].isDetail = true;
          //   } else if (state.sekretaris) {
          //     result[i].isRole = 'sekretaris';
          //     result[i].isJoin = true;
          //   }
          //   else if (state.pembicara) {
          //     result[i].isRole = 'speaker';
          //     result[i].isJoin = true;
          //   } else if (state.peserta) {
          //     result[i].isRole = 'participant';
          //     result[i].isJoin = true;
          //   } else if (state.tamu) {
          //     result[i].isRole = 'participant';
          //     result[i].isJoin = true;
          //   }
          // } catch (e) { }

          // pilih salah satu
          try {
            if (state.moderator) {
              result[i].isRole = 'moderator';
              result[i].isJoin = true;
            }if (state.sekretaris) {
              result[i].isRole =  result[i].isRole == 'moderator' ? result[i].isRole : 'sekretaris';
              result[i].isJoin = true;
              result[i].isDetail = true;
            } else if (req.app.token.user_id === result[i].created_by || state.owner) {
              result[i].isRole = 'owner';
              result[i].isJoin = true;
            } else if (state.pembicara) {
              result[i].isRole = 'speaker';
              result[i].isJoin = true;
            } else if (state.peserta) {
              result[i].isRole = 'participant';
              result[i].isJoin = true;
            }
          } catch (e) { }

          let LevelUserLogin = null;
          let grup_name = null;
          let decodedToken = req.app.token;
          if (req.app.token) {
            LevelUserLogin = decodedToken.level;
            grup_name = decodedToken.grup_name;
          }
		  console.log('ALVIN', LevelUserLogin)
          if ((LevelUserLogin === 'superadmin' || grup_name === 'Admin Training') || (LevelUserLogin === 'admin' && !result[0].isRole)) {
            result[i].isEdit = true;
            result[i].isDelete = true;
            result[i].isDetail = true;
            result[i].isJoin = true;
            result[i].isCreateWebinarTest = true;
            result[i].isSendWebinarTest = true;
            result[i].isUploadFileProject = true;
            result[i].isReadFileProject = true;
            result[i].isAccessQuestionForm = true;
            result[i].isRole = LevelUserLogin;

            if (LevelUserLogin === 'admin') {
              result[i].isDetail = true;
              result[i].isJoin = true;
              result[i].isCreateWebinarTest = false;
              result[i].isSendWebinarTest = false;
            }

            if (grup_name === 'Admin Training') {
              result[i].isRole = 'admin training';
              result[i].isDetail = true;
              result[i].isJoin = true;
              result[i].isCreateWebinarTest = true;
              result[i].isSendWebinarTest = true;
            }
            let roles = result[i].isRole.toLowerCase().replace(' ','_');
                  result[i][`is_${roles}`] = true;

          } else {

            let idx = -1;
            // if (result[i].project_admin.length > 0) {
            //   idx = result[i].project_admin.findIndex((str) => { return str.user_id == userId });
              if (idx > -1) {
                result[i].isEdit = true;
                result[i].isDelete = true;
                result[i].isDetail = true;
                result[i].isJoin = true;
                result[i].isCreateWebinarTest = true;
                result[i].isSendWebinarTest = true;
                result[i].isUploadFileProject = true;
                result[i].isReadFileProject = true;
                result[i].isAccessQuestionForm = true;
                result[i].isRole = decodedToken.level;
                let roles = result[i].isRole.toLowerCase().replace(' ','_');
                  result[i][`is_${roles}`] = true;
              }
            // }

            if (idx == -1) {
              req.app.accessInfo && req.app.accessInfo.forEach((arg) => {

                if (arg.role === result[i].isRole && arg.sub === 'webinar' && arg.code === 'CD_WEBINAR' && arg.status) {
                  result[i].isEdit = true;
                  result[i].isDelete = true;
                }
                if (arg.role === result[i].isRole && arg.sub === 'webinar' && arg.code === 'DETAIL_WEBINAR' && arg.status) {
                  result[i].isDetail = true;
                }
                if (arg.role === result[i].isRole && arg.sub === 'webinar' && arg.code === 'CRUD_ROLES' && arg.status) {
                  result[i].isCrudRoles = true;
                }
                if (arg.role === result[i].isRole && arg.sub === 'webinar' && arg.code === 'CREATE_TEST' && arg.status) {
                  result[i].isCreateWebinarTest = true;
                }
                if (arg.role === result[i].isRole && arg.sub === 'webinar' && arg.code === 'SEND_TEST' && arg.status) {
                  result[i].isSendWebinarTest = true;
                }
                if (arg.role === result[i].isRole && arg.sub === 'webinar' && arg.code === 'ACCEPT_TEST' && arg.status) {
                  result[i].isAcceptWebinarTest = true;
                }
                if (arg.role === result[i].isRole && arg.sub === 'webinar' && arg.code === 'ACCESS_DOORPRIZE' && arg.status) {
                  result[i].isAccessDoorprize = true;
                }
                if (arg.role === result[i].isRole && arg.sub === 'webinar' && arg.code === 'U_FILES_FOLDER' && arg.status) {
                  result[i].isUploadFileProject = true;
                }
                if (arg.role === result[i].isRole && arg.sub === 'webinar' && arg.code === 'R_FILES_FOLDER' && arg.status) {
                  result[i].isReadFileProject = true;
                }
                if (arg.role === result[i].isRole && arg.sub === 'webinar' && arg.code === 'CR_FILLED_FORM' && arg.status) {
                  result[i].isAccessQuestionForm = true;
                }
              })
            }
          }
        }
        res.json({ error: false, result: result });
      }
    }
  );
};

exports.getWebinarsByCompanyPlain = (req, res, next) => {
  db.query(
    `SELECT *
			FROM webinars
			WHERE company_id = '${req.params.company_id}' AND publish='1'
			ORDER BY id DESC`,
    async (error, result, fields) => {
      if (error) res.json({ error: true, result: error });

      if (result.length == 0) {
        res.json({ error: true, result: "Tidak ada webinar yang tersedia" });
      } else {
        for (var i = 0; i < result.length; i++) {
          let peserta = await db.query(
            `SELECT w.*, u.name FROM webinar_peserta w JOIN user u ON u.user_id = w.user_id WHERE webinar_id = '${result[i].id}'`
          );
          result[i].peserta = peserta;

          let tamu = await db.query(
            `SELECT * FROM webinar_tamu WHERE webinar_id='${result[i].id}'`
          );
          result[i].tamu = tamu;
        }
        res.json({ error: false, result: result });
      }
    }
  );
};

exports.importParticipants = async (req, res, next) => {
  let peserta = await db.query(
    `SELECT w.*, u.name FROM webinar_peserta w JOIN user u ON u.user_id = w.user_id WHERE webinar_id = '${req.body.importId}'`
  );
  let tamu = await db.query(
    `SELECT * FROM webinar_tamu WHERE webinar_id='${req.body.importId}'`
  );
  let current_webinar_peserta = await db.query(
    `SELECT w.*, u.name FROM webinar_peserta w JOIN user u ON u.user_id = w.user_id WHERE webinar_id = '${req.body.webinarId}'`
  );
  let current_webinar_tamu = await db.query(
    `SELECT * FROM webinar_tamu WHERE webinar_id='${req.body.webinarId}'`
  );

  for (let i = 0; i < peserta.length; i++) {
    let idx = current_webinar_peserta.findIndex(str => {
      return str.user_id.toString() === peserta[i].user_id.toString();
    });
    if (idx < 0) {
      await db.query(
        `INSERT INTO webinar_peserta (webinar_id, user_id, status) VALUES('${req.body.webinarId}','${peserta[i].user_id}','0') `
      );
    }
  }
  for (let i = 0; i < tamu.length; i++) {
    let idx = current_webinar_tamu.findIndex(str => {
      if (str.name === tamu[i].name && str.email === tamu[i].email) {
        return true;
      } else {
        return false;
      }
    });
    if (idx < 0) {
      await db.query(
        `INSERT INTO webinar_tamu (webinar_id, name, email, phone, status, voucher) VALUES('${req.body.webinarId
        }','${tamu[i].name}','${tamu[i].email}','${tamu[i].phone}','0','${Math.random().toString(36).substring(2, 15) +
        Math.random().toString(36).substring(2, 15)
        }')`
      );
    }
  }
  res.json({ error: false, result: "success" });
};

exports.importParticipantsByTrainingCompany = async (req, res, next) => {
  let peserta = await db.query(
    //`SELECT u.user_id AS user_id FROM training_user tu JOIN user u ON u.email = tu.email WHERE tu.training_company_id='${req.body.importId}' AND tu.status='active'`
    `SELECT u.user_id  FROM training_user tu JOIN user u ON u.email = tu.email WHERE tu.training_company_id='${req.body.importId}' AND tu.status='active' and u.status = 'active'`
    );
  let current_webinar_peserta = await db.query(
    `SELECT w.*, u.name FROM webinar_peserta w JOIN user u ON u.user_id = w.user_id WHERE webinar_id = '${req.body.webinarId}'`
  );
  for (let i = 0; i < peserta.length; i++) {
    let idx = current_webinar_peserta.findIndex(str => {
      return str.user_id.toString() === peserta[i].user_id.toString();
    });

    if (idx < 0) {
      await db.query(
        `INSERT INTO webinar_peserta (webinar_id, user_id, status) VALUES('${req.body.webinarId}','${peserta[i].user_id}','0')`
      );
    }
  }
  res.json({ error: false, result: "success" });
};

exports.createWebinar = async (req, res, next) => {
  let checks = await checkAccess({
    company_id: req.body.companyId.toString(),
    sub: 'general',
    role: req.app.token.level === 'superadmin' ? 'admin' : req.app.token.level,
    code: ['CD_WEBINAR', 'CRUD_ROLES']
  }, req);


  checks.map((str) => {
    if (!str.status && str.code === 'CD_WEBINAR') {
      return res.json({ error: true, result: `You dont have permission for this action, please contact your administrator.`, access: checks });
    }
  });

  if (req.body.moderatorId.length == 0 || req.body.sekretarisId.length == 0 || req.body.pembicaraId.length == 0 || req.body.ownerId == 0) {
    req.body.moderatorId.push(req.app.token.user_id);
    req.body.sekretarisId.push(req.app.token.user_id);
    req.body.ownerId.push(req.app.token.user_id);
  }

  let form = {
    judul: req.body.judul,
    moderatorId: req.body.moderatorId.toString(),
    sekretarisId: req.body.sekretarisId.toString(),
    pembicaraId: req.body.pembicaraId.toString(),
    ownerId: req.body.ownerId.toString(),
    projectId: req.body.projectId == "" ? 0 : req.body.projectId,
    dokumenId: req.body.dokumenId,
    companyId: req.body.companyId,
    course_id: req.body.course_id,
    certificate_orientation: req.body.certificate_orientation,
    is_training_liveclass: req.body.is_training_liveclass,

    engine: req.body.engine,
    mode: req.body.mode,
  };

  let sql = `INSERT INTO webinars (company_id, judul, sekretaris, moderator, pembicara, owner, engine, mode, project_id, dokumen_id, status, training_course_id, certificate_orientation, certificate_background, publish, created_by, is_training_liveclass)
		VALUES ('${form.companyId}','${form.judul}', '${form.sekretarisId}', '${form.moderatorId
    }', '${form.pembicaraId}', '${form.ownerId}', '${form.engine}', '${form.mode
    }', '${form.projectId}', '${form.dokumenId}', '0','${req.body.course_id ? req.body.course_id : 0
    }', '${form.certificate_orientation}', '','1', '${req.app.token.user_id}',${form.is_training_liveclass})`;
  db.query(sql, (error, result, fields) => {
    if (error) res.json({ error: true, result: error, access: checks });
    // insert on webinar_zoom
    if (form.engine === "zoom") {
      db.query(`INSERT INTO webinar_zoom (class_id, zoom_id, pwd) VALUES (?)`, [
        [result.insertId, "4912503275", "hoster"],
      ]);
    }

    db.query(
      `SELECT * FROM webinars WHERE id = '${result.insertId}'`,
      (error, result, fields) => {
        res.json({ error: false, result: result[0], access: checks });
      }
    );
  });
};

exports.getWebinarsById_public = async (req, res, next) => {
  const decodedToken = null;
  const userId = decodedToken;
  db.query(
    `SELECT * FROM webinars WHERE id = '${req.params.id}'`,
    async (error, result, fields) => {
      if (error) res.json({ error: true, result: error });
      if (result.length > 0) {

        result[0].peserta_speaker = [];
        result[0].guest_speaker = [];
        result[0].external_speaker = [];
        result[0].isEdit = false;
        result[0].isDelete = false;
        result[0].isCrudRoles = false;
        result[0].isDetail = false;
        result[0].isCreateWebinarTest = false;
        result[0].isSendWebinarTest = false;
        result[0].isAcceptWebinarTest = false;
        result[0].isAccessDoorprize = false;
        result[0].isUploadFileProject = false;
        result[0].isReadFileProject = false;
        result[0].isAccessQuestionForm = false;
        result[0].isRole = null;

        let moderator = await db.query(
          `SELECT user_id, name FROM user WHERE user_id IN (${result[0].moderator})`
        );
        result[0].moderator =
          moderator.length != 0
            ? moderator
            : { user_id: result[i].moderator, name: "Anonymous" };

        let sekretaris = await db.query(
          `SELECT user_id, name FROM user WHERE user_id IN (${result[0].sekretaris})`
        );
        result[0].sekretaris =
          sekretaris.length != 0
            ? sekretaris
            : { user_id: result[i].sekretaris, name: "Anonymous" };

        let split = result[0].pembicara.split(",");

        let tmp = { object: [], user_id: [], guest: [], external: [] }
        for (let j = 0; j < split.length; j++) {
          if (split[j] !== "") {
            if(split[j].search('external') > -1){
              tmp.external.push(split[j]);
              tmp.object.push({ user_id: split[j], name: "Anonymous", type: "external", email: null, phone: null,status:0 });
            }else if (isNaN(Number(split[j]))) {
              tmp.guest.push(split[j]);
              tmp.object.push({ user_id: split[j], name: "Anonymous", type: "guest", email: null, phone: null,status:0 });
            } else {
              tmp.user_id.push(split[j]);
              tmp.object.push({ user_id: split[j], name: "Anonymous", type: "peserta", email: null, phone: null,status:0 });
            }
          }
        }

        if (split.length > 0 && tmp.object.length > 0) {

          let queryString = '';
          let params = [];
          if (tmp.user_id.length > 0) {
            queryString = `SELECT user_id, name, email,phone FROM user WHERE user_id IN(?);`;
            params.push(tmp.user_id);
          }
          if (tmp.guest.length > 0) {
            queryString = `${queryString}SELECT wt.voucher as user_id, wt.name ,wt.email,wt.phone from webinar_tamu wt where wt.voucher in(?);`;
            params.push(tmp.guest);
          }
          if (tmp.external.length > 0) {
            queryString = `${queryString}SELECT wt.voucher as user_id, wt.name ,wt.email,wt.phone, wt.status from webinar_role_external wt where wt.voucher in(?);`;
            params.push(tmp.external);
          }

          let tmpPembicara = [];
          let pembicara = await db.query(queryString, params);

          if(pembicara && pembicara.length > 1){
            if(pembicara[0]){
              tmpPembicara = tmpPembicara.concat(pembicara[0]);
            }
            if(pembicara[1]){
              tmpPembicara = tmpPembicara.concat(pembicara[1]);
            }
            if(pembicara[2]){
              tmpPembicara = tmpPembicara.concat(pembicara[2]);
            }
            pembicara = tmpPembicara;
          }

          try {
            tmp.object.forEach((str, i) => {
              let idx = pembicara.findIndex((check) => { return check.user_id == str.user_id });
              if (idx > -1) {
                tmp.object[i].name = pembicara[idx].name;
                tmp.object[i].email = pembicara[idx].email;
                tmp.object[i].phone = pembicara[idx].phone;
              }
            });
          } catch (e) {
            // handle error undefined variable
          }

          // build peserta_speaker && guest_speaker
          tmp.object.forEach((str) => {
            if(str.type === "external"){
              result[0].external_speaker.push(str);
            }else if (str.type === "guest") {
              result[0].guest_speaker.push(str);
            } else {
              result[0].peserta_speaker.push(str);
            }
          });
          result[0].pembicara = tmp.object;
        } else {
          result[0].pembicara = [];
        }
        // result[0].pembicara =
        //   pembicara.length != 0
        //     ? pembicara
        //     : { user_id: result[i].pembicara, name: "Anonymous" };

        let owner = await db.query(
          `SELECT user_id, name FROM user WHERE user_id IN (${result[0].owner})`
        );
        result[0].owner =
          owner.length != 0
            ? owner
            : { user_id: result[0].owner, name: "Anonymous" };

        let peserta = await db.query(
          `SELECT w.*, u.name, u.email, u.phone FROM webinar_peserta w JOIN user u ON u.user_id = w.user_id WHERE webinar_id = '${result[0].id}'`
        );
        result[0].peserta = peserta;

        let tamu = await db.query(
          `SELECT * FROM webinar_tamu WHERE webinar_id='${result[0].id}'`
        );

        let tamu_form_reg = await db.query(
          `SELECT wt.*, u.name as 'approved_name' FROM webinar_transactions wt 
          left join user u on u.user_id = wt.approved_by  
          WHERE wt.webinar_id='${result[0].id}' and wt.status in('0','1')`
        );
        result[0].tamu = tamu;
        if (tamu_form_reg.length > 0) {
          let tmp = [];
          tamu_form_reg.forEach((str) => {
            let idx = result[0].tamu.findIndex((str2) => {
              if (str2.voucher === str.voucher && str.status == 1) { return true; }
              else { return false; }
            });
            if (idx > -1) {
              result[0].tamu[idx].slips = str.slips;
              result[0].tamu[idx].approved_by = str.approved_name;
            } else {
              tmp.push(str);
            }
          });
          result[0].tamu = tmp.concat(result[0].tamu);
        }

        let isFeedback = await db.query(
          `SELECT id FROM kuesioner_questions WHERE webinar_id='${result[0].id}'`
        );
        result[0].isFeedback = isFeedback.length ? true : false;

        const isKuesionerAlreadyInputed = await db.query(
          `SELECT IF(EXISTS(SELECT * FROM kuesioner_answer WHERE user_id = ${userId} AND webinar_id = ${req.params.id}), 1, 0) AS isExist`
        );
        result[0].kuesioner_answered = isKuesionerAlreadyInputed[0].isExist;

        // project admin
        let project_admin = await db.query(
          `SELECT u.user_id, u.name 
            FROM files_folder_user ffu
            left join user u on u.user_id = ffu.user_id
            WHERE 
            lower(ffu.role) = 'project admin' and folder_id = '${result[0].project_id}';`
        );
        result[0].project_admin = project_admin;


        // ACCESS
        let roles = ['pembicara', 'peserta', 'tamu'];
        let state = { pembicara: false, tamu: false };
        roles.forEach((arg1) => {
          if (result[0][arg1].length > 0) {

            let UserLogin = req.query.voucher;
            let idx = result[0][arg1].findIndex((arg) => { return UserLogin === arg.voucher; });

            if (idx > -1) {
              result[0].isRole = arg1;
              state[arg1] = true;
            }
          }
        });

        // pilih salah satu
        try {
          if (state.pembicara) {
            result[0].isRole = 'speaker';
            result[0].isJoin = true;
          } else if (state.peserta) {
            result[0].isRole = 'participant';
            result[0].isJoin = true;
          } else if (state.tamu) {
            result[0].isRole = 'participant';
            result[0].isJoin = true;
          }
        } catch (e) { }

        if(req.query.voucher && tmp.external.length){
          let idx = tmp.external.findIndex((check)=>{ return check == req.query.voucher });
          if(idx > -1) result[0].isRole = 'speaker';
        }

        req.app.accessInfo.forEach((arg) => {
          if (arg.role === result[0].isRole && arg.sub === 'webinar' && arg.code === 'CREATE_TEST' && arg.status) {
            result[0].isCreateWebinarTest = true;
          }
          if (arg.role === result[0].isRole && arg.sub === 'webinar' && arg.code === 'SEND_TEST' && arg.status) {
            result[0].isSendWebinarTest = true;
          }
          if (arg.role === result[0].isRole && arg.sub === 'webinar' && arg.code === 'ACCEPT_TEST' && arg.status) {
            result[0].isAcceptWebinarTest = true;
          }
          if (arg.role === result[0].isRole && arg.sub === 'webinar' && arg.code === 'ACCESS_DOORPRIZE' && arg.status) {
            result[0].isAccessDoorprize = true;
          }
          if (arg.role === result[0].isRole && arg.sub === 'webinar' && arg.code === 'U_FILES_FOLDER' && arg.status) {
            result[0].isUploadFileProject = true;
          }
          if (arg.role === result[0].isRole && arg.sub === 'webinar' && arg.code === 'R_FILES_FOLDER' && arg.status) {
            result[0].isReadFileProject = true;
          }
          if (arg.role === result[0].isRole && arg.sub === 'webinar' && arg.code === 'CR_FILLED_FORM' && arg.status) {
            result[0].isAccessQuestionForm = true;
          }
        })

        res.json({ error: false, result: result.length > 0 ? result[0] : [] });
      } else {
        res.json({ error: true, result: "ID tidak ditemukan" });
      }
    }
  );
};

exports.getWebinarsById = async (req, res, next) => {
  const decodedToken = req.app.token;
  const userId = decodedToken ? decodedToken.user_id : null;
  db.query(
    `SELECT * FROM webinars WHERE id = '${req.params.id}'`,
    async (error, result, fields) => {
      if (error) res.json({ error: true, result: error });
      if (result.length > 0) {

        result[0].peserta_speaker = [];
        result[0].guest_speaker = [];
        result[0].external_speaker = [];
        result[0].isEdit = false;
        result[0].isDelete = false;
        result[0].isCrudRoles = false;
        result[0].isDetail = false;
        result[0].isCreateWebinarTest = false;
        result[0].isSendWebinarTest = false;
        result[0].isAcceptWebinarTest = false;
        result[0].isAccessDoorprize = false;
        result[0].isUploadFileProject = false;
        result[0].isReadFileProject = false;
        result[0].isAccessQuestionForm = false;
        result[0].isRole = null;

        let moderator = await db.query(
          `SELECT user_id, name FROM user WHERE user_id IN (${result[0].moderator})`
        );
        result[0].moderator =
          moderator.length != 0
            ? moderator
            : { user_id: result[i].moderator, name: "Anonymous" };

        let sekretaris = await db.query(
          `SELECT user_id, name FROM user WHERE user_id IN (${result[0].sekretaris})`
        );
        result[0].sekretaris =
          sekretaris.length != 0
            ? sekretaris
            : { user_id: result[i].sekretaris, name: "Anonymous" };

        let split = result[0].pembicara.split(",");

        let tmp = { object: [], user_id: [], guest: [], external: [] }
        for (let j = 0; j < split.length; j++) {
          if (split[j] !== "") {
            if(split[j].search('external') > -1){
              tmp.external.push(split[j]);
              tmp.object.push({ user_id: split[j], name: "Anonymous", type: "external", email: null, phone: null,status:0 });
            }else if (isNaN(Number(split[j]))) {
              tmp.guest.push(split[j]);
              tmp.object.push({ user_id: split[j], name: "Anonymous", type: "guest", email: null, phone: null,status:0 });
            } else {
              tmp.user_id.push(split[j]);
              tmp.object.push({ user_id: split[j], name: "Anonymous", type: "peserta", email: null, phone: null,status:0 });
            }
          }
        }

        if (split.length > 0 && tmp.object.length > 0) {

          let queryString = '';
          let params = [];
          if (tmp.user_id.length > 0) {
            queryString = `SELECT user_id, name, email,phone FROM user WHERE user_id IN(?);`;
            params.push(tmp.user_id);
          }
          if (tmp.guest.length > 0) {
            queryString = `${queryString}SELECT wt.voucher as user_id, wt.name ,wt.email,wt.phone from webinar_tamu wt where wt.voucher in(?);`;
            params.push(tmp.guest);
          }
          if (tmp.external.length > 0) {
            queryString = `${queryString}SELECT wt.voucher as user_id, wt.name ,wt.email,wt.phone, wt.status from webinar_role_external wt where wt.voucher in(?);`;
            params.push(tmp.external);
          }
          
          let tmpPembicara = [];
          let pembicara = await db.query(queryString, params);

          if(pembicara && pembicara.length > 1){
            if(pembicara[0]){
              tmpPembicara = tmpPembicara.concat(pembicara[0]);
            }
            if(pembicara[1]){
              tmpPembicara = tmpPembicara.concat(pembicara[1]);
            }
            if(pembicara[2]){
              tmpPembicara = tmpPembicara.concat(pembicara[2]);
            }
            pembicara = tmpPembicara;
          }

          try {
            tmp.object.forEach((str, i) => {
              let idx = pembicara.findIndex((check) => { return check.user_id == str.user_id });
              if (idx > -1) {
                tmp.object[i].name = pembicara[idx].name;
                tmp.object[i].email = pembicara[idx].email;
                tmp.object[i].phone = pembicara[idx].phone;
                if(pembicara[idx].status){
                tmp.object[i].status = pembicara[idx].status;
                }
              }
            });
          } catch (e) {
            // handle error undefined variable
          }

          // build peserta_speaker && guest_speaker
          tmp.object.forEach((str) => {
            if(str.type === "external"){
              result[0].external_speaker.push(str);
            }else if (str.type === "guest") {
              result[0].guest_speaker.push(str);
            } else {
              result[0].peserta_speaker.push(str);
            }
          });
          result[0].pembicara = tmp.object;
        } else {
          result[0].pembicara = [];
        }
        // result[0].pembicara =
        //   pembicara.length != 0
        //     ? pembicara
        //     : { user_id: result[i].pembicara, name: "Anonymous" };

        let owner = await db.query(
          `SELECT user_id, name FROM user WHERE user_id IN (${result[0].owner})`
        );
        result[0].owner =
          owner.length != 0
            ? owner
            : { user_id: result[0].owner, name: "Anonymous" };

        let peserta = await db.query(
          `SELECT w.*, u.name, u.email, u.phone FROM webinar_peserta w JOIN user u ON u.user_id = w.user_id WHERE webinar_id = '${result[0].id}'`
        );
        result[0].peserta = peserta;

        let tamu = await db.query(
          `SELECT * FROM webinar_tamu WHERE webinar_id='${result[0].id}'`
        );

        let tamu_form_reg = await db.query(
          `SELECT wt.*, u.name as 'approved_name' FROM webinar_transactions wt 
          left join user u on u.user_id = wt.approved_by  
          WHERE wt.webinar_id='${result[0].id}' and wt.status in('0','1')`
        );
        result[0].tamu = tamu;
        if (tamu_form_reg.length > 0) {
          let tmp = [];
          tamu_form_reg.forEach((str) => {
            let idx = result[0].tamu.findIndex((str2) => {
              if (str2.voucher === str.voucher && str.status == 1) { return true; }
              else { return false; }
            });
            if (idx > -1) {
              result[0].tamu[idx].slips = str.slips;
              result[0].tamu[idx].approved_by = str.approved_name;
            } else {
              tmp.push(str);
            }
          });
          result[0].tamu = tmp.concat(result[0].tamu);
        }

        let isFeedback = await db.query(
          `SELECT id FROM kuesioner_questions WHERE webinar_id='${result[0].id}'`
        );
        result[0].isFeedback = isFeedback.length ? true : false;

        const isKuesionerAlreadyInputed = await db.query(
          `SELECT IF(EXISTS(SELECT * FROM kuesioner_answer WHERE user_id = ${userId} AND webinar_id = ${req.params.id}), 1, 0) AS isExist`
        );
        result[0].kuesioner_answered = isKuesionerAlreadyInputed[0].isExist;

        // project admin
        let project_admin = await db.query(
          `SELECT u.user_id, u.name 
          FROM files_folder_user ffu
          left join user u on u.user_id = ffu.user_id
          WHERE 
          lower(ffu.role) = 'project admin' and folder_id = '${result[0].project_id}';`
        );
        result[0].project_admin = project_admin;


        // ACCESS
        let roles = ['moderator', 'pembicara', 'sekretaris', 'peserta'];
        let state = { moderator: false, pembicara: false, sekretaris: false, peserta: false };
        roles.forEach((arg1) => {
          if (result[0][arg1].length > 0) {

            let idx = result[0][arg1].findIndex((arg) => { return userId.toString() === arg.user_id.toString(); });
            if (idx > -1) {
              result[0].isRole = arg1;
              state[arg1] = true;
            }
          }
        });

        //console.log(result[0].isRole, "2313123");

        // pilih salah satu
        try {
          if (state.moderator) {
            result[0].isRole = 'moderator';
            result[0].isJoin = true;
          }
          if (state.sekretaris) {
            result[0].isRole =  result[0].isRole == 'moderator' ? result[0].isRole : 'sekretaris';
            result[0].isJoin = true;
            result[0].isDetail = true;
          }else if (req.app.token.user_id === result[0].created_by || state.owner) {
            result[0].isRole = 'owner';
            result[0].isJoin = true;
          } else if (state.pembicara) {
            result[0].isRole = 'speaker';
            result[0].isJoin = true;
          } else if (state.peserta) {
            result[0].isRole = 'participant';
            result[0].isJoin = true;
          }
        } catch (e) { }

        let LevelUserLogin = null;
        if (decodedToken) {
          LevelUserLogin = decodedToken.level;
        }
        

        if (LevelUserLogin === 'superadmin' || (LevelUserLogin === 'admin' && !result[0].isRole)) {
          result[0].isEdit = true;
          result[0].isDelete = true;
          result[0].isDetail = true;
          result[0].isJoin = true;
          result[0].isCreateWebinarTest = true;
          result[0].isSendWebinarTest = true;
          result[0].isUploadFileProject = true;
          result[0].isReadFileProject = true;
          result[0].isAccessQuestionForm = true;
          result[0].isRole = LevelUserLogin;

          if (LevelUserLogin === 'admin') {
            result[0].isDetail = false;
            result[0].isJoin = false;
            result[0].isCreateWebinarTest = false;
            result[0].isSendWebinarTest = false;
          }

        } else {

          let idx = -1;
          if (result[0].project_admin.length > 0) {
            idx = result[0].project_admin.findIndex((str) => { return str.user_id == userId });
            if (idx > -1) {
              result[0].isEdit = true;
              result[0].isDelete = true;
              result[0].isDetail = true;
              result[0].isJoin = true;
              result[0].isCreateWebinarTest = true;
              result[0].isSendWebinarTest = true;
              result[0].isUploadFileProject = true;
              result[0].isReadFileProject = true;
              result[0].isAccessQuestionForm = true;
              result[0].isRole = decodedToken.level;
            }
          }

          if (idx == -1) {
            req.app.accessInfo.forEach((arg) => {

              if (arg.role === result[0].isRole && arg.sub === 'webinar' && arg.code === 'CD_WEBINAR' && arg.status) {
                result[0].isEdit = true;
                result[0].isDelete = true;
              }
              if (arg.role === result[0].isRole && arg.sub === 'webinar' && arg.code === 'DETAIL_WEBINAR' && arg.status) {
                result[0].isDetail = true;
              }
              if (arg.role === result[0].isRole && arg.sub === 'webinar' && arg.code === 'CRUD_ROLES' && arg.status) {
                result[0].isCrudRoles = true;
              }
              if (arg.role === result[0].isRole && arg.sub === 'webinar' && arg.code === 'CREATE_TEST' && arg.status) {
                result[0].isCreateWebinarTest = true;
              }
              if (arg.role === result[0].isRole && arg.sub === 'webinar' && arg.code === 'SEND_TEST' && arg.status) {
                result[0].isSendWebinarTest = true;
              }
              if (arg.role === result[0].isRole && arg.sub === 'webinar' && arg.code === 'ACCEPT_TEST' && arg.status) {
                result[0].isAcceptWebinarTest = true;
              }
              if (arg.role === result[0].isRole && arg.sub === 'webinar' && arg.code === 'ACCESS_DOORPRIZE' && arg.status) {
                result[0].isAccessDoorprize = true;
              }
              if (arg.role === result[0].isRole && arg.sub === 'webinar' && arg.code === 'U_FILES_FOLDER' && arg.status) {
                result[0].isUploadFileProject = true;
              }
              if (arg.role === result[0].isRole && arg.sub === 'webinar' && arg.code === 'R_FILES_FOLDER' && arg.status) {
                result[0].isReadFileProject = true;
              }
              if (arg.role === result[0].isRole && arg.sub === 'webinar' && arg.code === 'CR_FILLED_FORM' && arg.status) {
                result[0].isAccessQuestionForm = true;
              }
            })
          }
        }

        res.json({ error: false, result: result.length > 0 ? result[0] : [] });
      } else {
        res.json({ error: true, result: "ID tidak ditemukan" });
      }
    }
  );
};
exports.getWebinarHistory = (req, res, next) => {
  db.query(
    `SELECT * FROM webinars WHERE id = '${req.params.id}'`,
    async (error, result, fields) => {
      if (error) res.json({ error: true, result: error });
      if (result.length > 0) {
        result[0].guest_speaker = [];
        result[0].peserta_speaker = [];
        result[0].external_speaker = [];

        let moderator = await db.query(
          `SELECT user_id, name FROM user WHERE user_id IN (${result[0].moderator})`
        );
        result[0].moderator =
          moderator.length != 0
            ? moderator
            : { user_id: result[i].moderator, name: "Anonymous" };

        let sekretaris = await db.query(
          `SELECT user_id, name FROM user WHERE user_id IN (${result[0].sekretaris})`
        );
        result[0].sekretaris =
          sekretaris.length != 0
            ? sekretaris
            : { user_id: result[i].sekretaris, name: "Anonymous" };

        // let pembicara = await db.query(
        //   `SELECT user_id, name FROM user WHERE user_id IN (${result[0].pembicara})`
        // );
        // result[0].pembicara =
        //   pembicara.length != 0
        //     ? pembicara
        //     : { user_id: result[i].pembicara, name: "Anonymous" };

        let split = result[0].pembicara.split(",");

        let tmp = { object: [], user_id: [], guest: [], external: [] }
        for (let j = 0; j < split.length; j++) {
          if (split[j] !== "") {
            if(split[j].search('external') > -1){
              tmp.external.push(split[j]);
              tmp.object.push({ user_id: split[j], name: "Anonymous", type: "external", email: null, phone: null,status:0 });
            }else if (isNaN(Number(split[j]))) {
              tmp.guest.push(split[j]);
              tmp.object.push({ user_id: split[j], name: "Anonymous", type: "guest", email: null, phone: null,status:0 });
            } else {
              tmp.user_id.push(split[j]);
              tmp.object.push({ user_id: split[j], name: "Anonymous", type: "peserta", email: null, phone: null,status:0 });
            }
          }
        }

        if (split.length > 0 && tmp.object.length > 0) {

          let queryString = '';
          let params = [];
          if (tmp.user_id.length > 0) {
            queryString = `SELECT user_id, name, email,phone FROM user WHERE user_id IN(?);`;
            params.push(tmp.user_id);
          }
          if (tmp.guest.length > 0) {
            queryString = `${queryString}SELECT wt.voucher as user_id, wt.name ,wt.email,wt.phone from webinar_tamu wt where wt.voucher in(?);`;
            params.push(tmp.guest);
          }
          if (tmp.external.length > 0) {
            queryString = `${queryString}SELECT wt.voucher as user_id, wt.name ,wt.email,wt.phone, wt.status from webinar_role_external wt where wt.voucher in(?);`;
            params.push(tmp.external);
          }

          let tmpPembicara = [];
          let pembicara = await db.query(queryString, params);

          if(pembicara && pembicara.length > 1){
            if(pembicara[0]){
              tmpPembicara = tmpPembicara.concat(pembicara[0]);
            }
            if(pembicara[1]){
              tmpPembicara = tmpPembicara.concat(pembicara[1]);
            }
            if(pembicara[2]){
              tmpPembicara = tmpPembicara.concat(pembicara[2]);
            }
            pembicara = tmpPembicara;
          }

          try {
            tmp.object.forEach((str, i) => {
              let idx = pembicara.findIndex((check) => { return check.user_id == str.user_id });
              if (idx > -1) {
                tmp.object[i].name = pembicara[idx].name;
                tmp.object[i].email = pembicara[idx].email;
                tmp.object[i].phone = pembicara[idx].phone;
                if(pembicara[idx].status){
                tmp.object[i].status = pembicara[idx].status;
                }
              }
            });
          } catch (e) {
            // handle error undefined variable
          }

          // build peserta_speaker && guest_speaker
          result[0].peserta_speaker = [];
          result[0].guest_speaker = [];
          tmp.object.forEach((str) => {
            if(str.type === "external"){
              result[0].external_speaker.push(str);
            }else if (str.type === "guest") {
              result[0].guest_speaker.push(str);
            } else {
              result[0].peserta_speaker.push(str);
            }
          });
          result[0].pembicara = tmp.object;
        } else {
          result[0].pembicara = [];
        }

        let owner = await db.query(
          `SELECT user_id, name FROM user WHERE user_id IN (${result[0].owner})`
        );
        result[0].owner =
          owner.length != 0
            ? owner
            : { user_id: result[0].owner, name: "Anonymous" };

        let peserta =
          await db.query(`SELECT w.*, u.name, u.email, u.phone, min(l.time) AS jam_mulai, max(l2.time) AS jam_selesai, MAX(s.status) AS status_sertifikat
			FROM webinar_peserta w
			JOIN user u ON u.user_id = w.user_id
			LEFT JOIN (SELECT * FROM webinar_logs WHERE type='peserta' AND action='join' AND webinar_id='${result[0].id}') l ON l.peserta_id = w.user_id
			LEFT JOIN (SELECT * FROM webinar_logs WHERE type='peserta' AND action='left' AND webinar_id='${result[0].id}') l2 ON l2.peserta_id = w.user_id
			LEFT JOIN (SELECT * FROM webinar_sertifikat WHERE webinar_id='${result[0].id}') s ON s.user_id = w.user_id
			WHERE w.webinar_id = '${result[0].id}'
			GROUP BY w.id`);
        for (let i = 0; i <= peserta.length - 1; i++) {
          let cameraOn = await db.query(
            `SELECT * FROM conference_logs WHERE user_id='${peserta[i].user_id}' AND conference_id='${result[0].id}' AND event='user-cam-broadcast-start' ORDER BY time ASC`
          );
          let cameraOff = await db.query(
            `SELECT * FROM conference_logs WHERE user_id='${peserta[i].user_id}' AND conference_id='${result[0].id}' AND event='user-cam-broadcast-end' ORDER BY time ASC`
          );
          let audioOn = await db.query(
            `SELECT * FROM conference_logs_audio WHERE user_id='${peserta[i].user_id}' AND conference_id='${result[0].id}' AND event='user-audio-voice-enabled' ORDER BY time ASC`
          );
          let audioOff = await db.query(
            `SELECT * FROM conference_logs_audio WHERE user_id='${peserta[i].user_id}' AND conference_id='${result[0].id}' AND event='user-audio-voice-disabled' ORDER BY time ASC`
          );

          if (cameraOn.length) {
            let duration = 0;
            cameraOn.map((item, i) => {
              let camOn = new Date(cameraOn[i].time);
              let camOff = new Date(
                cameraOff[i] ? cameraOff[i].time : cameraOn[i].time
              );
              duration += Math.abs(camOff - camOn);
            });
            let diffHour = Math.floor((duration % 86400000) / 3600000);
            let diffMin = Math.round(((duration % 86400000) % 3600000) / 60000);
            let diffSec = Math.round(
              (((duration % 86400000) % 3600000) % 60000) / 1000
            );
            peserta[i].camera = diffHour + ":" + diffMin + ":" + diffSec;
            if(peserta[i].status == 1){ peserta[i].status == 2 }
          } else {
            peserta[i].camera = "Off";
          }

          if (audioOn.length) {
            let duration = 0;
            audioOn.map((item, i) => {
              let camOn = new Date(audioOn[i].time);
              let camOff = new Date(
                audioOff[i] ? audioOff[i].time : audioOn[i].time
              );
              duration += Math.abs(camOff - camOn);
            });
            let diffHour = Math.floor((duration % 86400000) / 3600000);
            let diffMin = Math.round(((duration % 86400000) % 3600000) / 60000);
            let diffSec = Math.round(
              (((duration % 86400000) % 3600000) % 60000) / 1000
            );
            peserta[i].audio = diffHour + ":" + diffMin + ":" + diffSec;
            if(peserta[i].status == 1){ peserta[i].status == 2 }
          } else {
            peserta[i].audio = "Off";
          }
        }
        result[0].peserta = peserta;

        let tamu =
          await db.query(`SELECT w.*, min(l.time) AS jam_mulai, max(l2.time) AS jam_selesai, MAX(s.status) AS status_sertifikat
			FROM webinar_tamu w
			LEFT JOIN (SELECT * FROM webinar_logs WHERE type='tamu' AND action='join' AND webinar_id='${result[0].id}') l ON l.peserta_id = w.voucher
			LEFT JOIN (SELECT * FROM webinar_logs WHERE type='tamu' AND action='left' AND webinar_id='${result[0].id}') l2 ON l2.peserta_id = w.voucher
			LEFT JOIN (SELECT * FROM webinar_sertifikat WHERE webinar_id='${result[0].id}') s ON s.user_id = w.voucher
			WHERE w.webinar_id='${result[0].id}'
			GROUP BY w.id`);
        for (let i = 0; i <= tamu.length - 1; i++) {
          let cameraOn = await db.query(
            `SELECT * FROM conference_logs WHERE user_id='${tamu[i].voucher}' AND conference_id='${result[0].id}' AND event='user-cam-broadcast-start' ORDER BY time ASC`
          );
          let cameraOff = await db.query(
            `SELECT * FROM conference_logs WHERE user_id='${tamu[i].voucher}' AND conference_id='${result[0].id}' AND event='user-cam-broadcast-end' ORDER BY time ASC`
          );
          let audioOn = await db.query(
            `SELECT * FROM conference_logs_audio WHERE user_id='${tamu[i].voucher}' AND conference_id='${result[0].id}' AND event='user-audio-voice-enabled' ORDER BY time ASC`
          );
          let audioOff = await db.query(
            `SELECT * FROM conference_logs_audio WHERE user_id='${tamu[i].voucher}' AND conference_id='${result[0].id}' AND event='user-audio-voice-disabled' ORDER BY time ASC`
          );

          if (cameraOn.length) {
            let duration = 0;
            cameraOn.map((item, i) => {
              let camOn = new Date(cameraOn[i].time);
              let camOff = new Date(
                cameraOff[i] ? cameraOff[i].time : cameraOn[i].time
              );
              duration += Math.abs(camOff - camOn);
            });
            let diffHour = Math.floor((duration % 86400000) / 3600000);
            let diffMin = Math.round(((duration % 86400000) % 3600000) / 60000);
            let diffSec = Math.round(
              (((duration % 86400000) % 3600000) % 60000) / 1000
            );
            tamu[i].camera = diffHour + ":" + diffMin + ":" + diffSec;
            if(tamu[i].status == 1){ tamu[i].status == 2 }
          } else {
            tamu[i].camera = "Off";
          }

          if (audioOn.length) {
            let duration = 0;
            audioOn.map((item, i) => {
              let camOn = new Date(audioOn[i].time);
              let camOff = new Date(
                audioOff[i] ? audioOff[i].time : audioOn[i].time
              );
              duration += Math.abs(camOff - camOn);
            });
            let diffHour = Math.floor((duration % 86400000) / 3600000);
            let diffMin = Math.round(((duration % 86400000) % 3600000) / 60000);
            let diffSec = Math.round(
              (((duration % 86400000) % 3600000) % 60000) / 1000
            );
            tamu[i].audio = diffHour + ":" + diffMin + ":" + diffSec;

            if(tamu[i].status == 1){ tamu[i].status == 2 }
          } else {
            tamu[i].audio = "Off";
          }
        }
        result[0].tamu = tamu;

        res.json({ error: false, result: result.length > 0 ? result[0] : [] });
      } else {
        res.json({ error: true, result: "ID tidak ditemukan" });
      }
    }
  );
};

exports.getTamuByVoucher = (req, res, next) => {
  let table = 'webinar_tamu';
  if(req.params.voucher.search('external') > -1){
    table = 'webinar_role_external';
  }
  db.query(
    `SELECT voucher AS user_id, name, 'tamu' AS type FROM ${table} WHERE voucher = '${req.params.voucher}'`,
    async (error, result, fields) => {
      if (error) res.json({ error: true, result: error });
      if (result.length > 0) {
        res.json({ error: false, result: result.length > 0 ? result[0] : [] });
      } else {
        res.json({ error: true, result: result });
      }
    }
  );
};

exports.updateCoverWebinar = (req, res, next) => {
  upload(req, res, err => {
    if (!req.file) {
      res.json({ error: true, result: err });
    } else {
      let path = req.file.path;
      let newDate = new Date();
      let keyName = `${env.AWS_BUCKET_ENV}/webinar`;

      var params = {
        ACL: "public-read",
        Bucket: env.AWS_BUCKET,
        Body: fs.createReadStream(path),
        Key: `${keyName}/${req.file.originalname}`,
      };

      s3.upload(params, (err, data) => {
        if (err) {
          res.json({ error: true, msg: "Error Upload Image", result: err });
        }
        if (data) {
          fs.unlinkSync(path);

          db.query(
            `UPDATE webinars SET gambar = '${data.Location}' WHERE id = '${req.params.id}'`,
            (error, result, fields) => {
              if (error) {
                res.json({ error: true, result: error });
              } else {
                res.json({
                  error: false,
                  result: data.Location,
                });
              }
            }
          );
        }
      });
    }
  });
};

exports.updateStatusWebinar = (req, res) => {
  let form = req.body;
  db.query(
    `SELECT status FROM webinars WHERE id='${form.id}'`,
    (error, result) => {
      if (error) {
        res.json({ error: true, result: error.message });
      } else {
        if (result[0].status === 3) {
          res.json({ error: false, result: "success update webinar detail" });
        } else {
          db.query(
            `UPDATE webinars SET
									status='${form.status}'
								WHERE id='${form.id}'`,
            (error, result) => {
              if (error) {
                res.json({ error: true, result: error.message });
              } else {
                if (result.affectedRows === 0) {
                  res.json({ error: true, result: "data can't be found" });
                } else {
                  res.json({
                    error: false,
                    result: "success update webinar detail",
                  });
                }
              }
            }
          );
        }
      }
    }
  );
};
exports.deleteWebinar = (req, res) => {
  let id = req.params.id;

  db.query(
    `UPDATE webinars SET
					publish='0'
				WHERE id='${id}'`,
    (error, result) => {
      if (error) {
        res.json({ error: true, result: error.message });
      } else {
        if (result.affectedRows === 0) {
          res.json({ error: true, result: "data can't be found" });
        } else {
          db.query(
            `DELETE FROM calendar WHERE type='4' AND activity_id='${id}'`,
            (error, result) => {
              if (error) {
                res.json({ error: true, result: error.message });
              } else {
                res.json({ error: false, result: "success delete webinar" });
              }
            }
          );
        }
      }
    }
  );
};
exports.log = (req, res) => {
  let form = req.params;
  var d = moment().format("YYYY-MM-DD H:mm:ss");
  db.query(
    `INSERT INTO webinar_logs (webinar_id, peserta_id, type, action, time) VALUES
				('${form.webinar_id}','${form.peserta_id}','${form.type}','${form.action}','${d}')`,
    (error, result) => {
      if (error) {
        res.json({ error: true, result: error.message });
      } else {
        if (form.action == "join") {
          db.query(
            `UPDATE webinar_${form.type} SET
												status='2'
											WHERE webinar_id='${form.webinar_id}' AND ${form.type == "peserta" ? "user_id" : "voucher"
            }='${form.peserta_id}'`,
            (error, result) => {
              if (error) {
                res.json({ error: true, result: error.message });
              } else {
                if (result.affectedRows === 0) {
                  res.json({ error: true, result: "data can't be found" });
                } else {
                  res.json({
                    error: false,
                    result: "success update logs webinar",
                  });
                }
              }
            }
          );
        } else {
          res.json({ error: false, result: "success update logs webinar" });
        }
      }
    }
  );
};
/**
 *
 * @param {string} req.body.id
 * @param {string} req.body.status
 * @param {string} req.body.judul
 * @param {string} req.body.isi
 * @param {string} req.body.tanggal
 * @param {string} req.body.jam_mulai
 * @param {string} req.body.jam_selesai
 * @param {boolean} res.body.error
 * @param {string} res.body.result
 */
exports.updateDetailWebinar = (req, res) => {
  let form = req.body;

  if (form.status == 0) {
    db.query(`UPDATE webinars SET
					status='1'
				WHERE id='${form.id}'`);
  }

  db.query(
    `UPDATE webinars t1
		SET t1.judul = '${form.judul}',
			t1.isi = '${form.isi}',
			t1.start_time = '${form.start_time}',
			t1.end_time = '${form.end_time}'
		WHERE t1.id = '${form.id}';`,
    (error, result) => {
      if (error) {
        res.json({ error: true, result: error.message });
      } else {
        if (result.affectedRows === 0) {
          res.json({ error: true, result: "data can't be found" });
        } else {
          res.json({ error: false, result: "success update webinar detail" });
        }
      }
    }
  );
};

exports.addPeserta = async (req, res, next) => {
  let form = {
    webinarId: req.body.webinarId,
    userId: req.body.userId,
  };

  let Sqlduplicate = `select count(id) as in_used from webinar_peserta where webinar_id = '${form.webinarId}' and user_id = '${form.userId}';`;
  let resultDup = await db.query(Sqlduplicate);
  if (resultDup[0].in_used == 0) {
    let sql = `INSERT INTO webinar_peserta (webinar_id, user_id, status) VALUES('${form.webinarId}','${form.userId}','0')`;

    db.query(sql, (error, result, fields) => {
      if (error) res.json({ error: true, result: error });
      else res.json({ error: false, result: result });
    });
  } else {
    res.json({ error: false, result: "Participant already in used" })
  }
};

exports.deletePeserta = (req, res, next) => {
  let sql = `DELETE FROM webinar_peserta WHERE id = '${req.params.id}'`;
  db.query(sql, (error, result, fields) => {
    if (error) res.json({ error: true, result: error });

    res.json({ error: false, result: result });
  });
};

exports.addTamu = async (req, res, next) => {
  let form = {
    webinarId: req.body.webinarId,
    name: req.body.name,
    email: req.body.email,
    phone: req.body.phone,
  };
  
  const webinarHasSameParticipant = await db.query(`SELECT * FROM webinar_tamu WHERE webinar_id = ? AND email = ?`, [form.webinarId, form.email]);
  const isParticipantsHasAdded = webinarHasSameParticipant.length ? true : false;

  if(isParticipantsHasAdded) {
    res.json({error: true, result: 'Participant/Guest already added..'});
  }else{
    let sql = `INSERT INTO webinar_tamu (webinar_id, name, email, phone, status, voucher) VALUES(?)`;
    let formInsert = [form.webinarId, form.name, form.email, form.phone, 0, Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15)]
  
    db.query(sql, [formInsert], (error, result, fields) => {
      if (error) res.json({ error: true, result: error });
      else res.json({ error: false, result: result });
    });
  }
};
exports.deleteTamu = (req, res, next) => {
  let sql = `DELETE FROM webinar_tamu WHERE id = '${req.params.id}'`;
  db.query(sql, (error, result, fields) => {
    if (error) res.json({ error: true, result: error });

    res.json({ error: false, result: result });
  });
};

exports.editWebinar = (req, res, next) => {
  let form = {
    webinarId: req.body.webinarId,
    judul: req.body.judul,
    moderatorId: req.body.moderatorId,
    sekretarisId: req.body.sekretarisId,
    pembicaraId: req.body.pembicaraId,
    ownerId: req.body.ownerId,
    dokumenId: req.body.dokumenId,
    projectId: req.body.projectId,
    course_id: req.body.course_id,
    certificate_orientation: req.body.certificate_orientation,

    engine: req.body.engine,
    mode: req.body.mode,
  };

  db.query(
    `UPDATE webinars SET
				judul='${form.judul}',
				sekretaris='${form.sekretarisId}',
				moderator='${form.moderatorId}',
			--	pembicara='${form.pembicaraId}',
				owner='${form.ownerId}',
				dokumen_id='${form.dokumenId}',
				project_id='${form.projectId}',
				certificate_orientation='${form.certificate_orientation}',
        engine = '${form.engine}',
        mode = '${form.mode}',
				training_course_id='${req.body.course_id ? req.body.course_id : 0}'
			WHERE id='${form.webinarId}'`,
    (error, result, fields) => {
      if (error) {
        res.json({ error: true, result: error });
      } else {
        // deleve liveclass zoom if not zoom
        if (form.engine === "zoom") {
          db.query(
            `SELECT * FROM webinar_zoom WHERE class_id = ?`,
            [form.webinarId],
            (err, ress, fiel) => {
              if (!ress.length) {
                db.query(
                  `INSERT INTO webinar_zoom (class_id, zoom_id, pwd) VALUES (?)`,
                  [[form.webinarId, "4912503275", "hoster"]]
                );
              }
            }
          );
        } else {
          db.query(`DELETE FROM webinar_zoom WHERE class_id = ?`, [
            form.webinarId,
          ]);
        }

        res.json({ error: false, result: result });
      }
    }
  );
};

exports.sendKuesioner = (req, res, next) => {
  db.query(
    `UPDATE webinars SET
				kuesioner_sent='1'
			WHERE id='${req.params.id}'`,
    (error, result, fields) => {
      if (error) {
        res.json({ error: true, result: error });
      } else {
        res.json({ error: false, result: result });
      }
    }
  );
};
exports.sendPosttest = (req, res, next) => {
  db.query(
    `UPDATE webinars SET
				posttest_sent='1'
			WHERE id='${req.params.id}'`,
    (error, result, fields) => {
      if (error) {
        res.json({ error: true, result: error });
      } else {
        res.json({ error: false, result: result });
      }
    }
  );
};

exports.sendEssay = async (req, res) => {
  try {
    let form = {
      sent: req.body.hasOwnProperty("sent") ? req.body.sent : "1",
      essay: req.body.essay,
      time: parseInt(req.body.time),
    }
    let input = await validatorRequest.sendEssay(form);
    db.query(
      `
      UPDATE webinars SET
        essay_sent = ?, essay = ?, essay_time = ?
      WHERE id='${req.params.id}'`,
      [
        req.body.hasOwnProperty("sent") ? req.body.sent : "1",
        req.body.essay,
        req.body.time,
      ],
      (error, result, fields) => {
        if (error) {
          res.json({ error: true, result: error });
        } else {
          res.json({ error: false, result: result });
        }
      }
    );
  } catch (error) {
    res.json({ error: true, result: error });
  }
};

exports.sendPretest = (req, res, next) => {
  db.query(
    `UPDATE webinars SET
				pretest_sent='1'
			WHERE id='${req.params.id}'`,
    (error, result, fields) => {
      if (error) {
        res.json({ error: true, result: error });
      } else {
        res.json({ error: false, result: result });
      }
    }
  );
};

exports.sendEmail = async (req, res) => {
  const payload = { ...req.body };
  await send.sendEmailWebinar(payload);

  return await res.json({ error: false, result: "success send email" });
};

exports.createQNA = (req, res, next) => {
  let form = {
    webinar_id: req.body.webinar_id,
    jenis_peserta: req.body.jenis_peserta,
    peserta_id: req.body.peserta_id,
    description: req.body.description,
  };

  let sql = `INSERT INTO webinar_qna (webinar_id, jenis_peserta, peserta_id, description)
		VALUES ('${form.webinar_id}','${form.jenis_peserta}','${form.peserta_id}', '${form.description}')`;

  db.query(sql, (error, result, fields) => {
    if (error) {
      res.json({ error: true, result: error });
    } else {
      let sqlResult = `SELECT q.*, u.name, u.email FROM webinar_qna q JOIN user u ON u.user_id = q.peserta_id WHERE q.id = '${result.insertId}'`;
      let sqlResultTamu = `SELECT q.*, t.name, t.email FROM webinar_qna q JOIN webinar_tamu t ON t.voucher = q.peserta_id WHERE q.id = '${result.insertId}'`;
      // db.query(form.jenis_peserta == 'peserta' ? sqlResult : sqlResultTamu, (error, result, fields) => {
      // 	res.json({ error: false, result: result[0] });
      // })
      let q = sqlResult; // peserta
      if (form.jenis_peserta.toLowerCase() === "tamu") {
        q = sqlResultTamu;
      }
      db.query(q, (error, results, fields) => {
        res.json({ error: false, result: results[0] });
      });
    }
  });
};

exports.getWebinarQNA = (req, res, next) => {
  let sql = `SELECT q.*, u.name, u.email, u.phone
	FROM webinar_qna q
	LEFT JOIN user u ON u.user_id = q.peserta_id
	WHERE q.webinar_id = '${req.params.webinarId}' AND q.jenis_peserta='peserta'`;

  let sqlTamu = `SELECT q.*, t.name, t.email, t.phone
	FROM webinar_qna q
	LEFT JOIN webinar_tamu t ON t.voucher = q.peserta_id
	WHERE q.webinar_id = '${req.params.webinarId}' AND q.jenis_peserta='tamu'`;

  db.query(sql, (error, result, fields) => {
    if (error) res.json({ error: true, result: error });
    let peserta = result;
    db.query(sqlTamu, (error, result, fields) => {
      if (error) res.json({ error: true, result: error });
      let gabung = peserta.concat(result);
      const gabungSort = gabung.sort((a, b) => b.timestamp - a.timestamp);
      res.json({ error: false, result: gabungSort });
    });
  });
};

exports.getWebinarQNAByUser = async (req, res, next) => {

  let sql = `SELECT q.*, u.name, u.email, u.phone
  FROM webinar_qna q
  JOIN user u ON u.user_id = q.peserta_id
  WHERE q.webinar_id = '${req.params.webinarId}' AND q.peserta_id='${req.params.pesertaId}'`;

  if (isNaN(Number(req.params.pesertaId))) {

    sql = `SELECT q.*, t.name, t.email, t.phone
    FROM webinar_qna q
    JOIN webinar_tamu t ON t.voucher = q.peserta_id
    WHERE q.webinar_id = '${req.params.webinarId}' AND q.peserta_id='${req.params.pesertaId}'`;
  }

  let result = await db.query(sql);
  if (result.length > 0) {
    const gabungSort = result.sort((a, b) => b.timestamp - a.timestamp);
    res.json({ error: false, result: gabungSort });
  } else {
    res.json({ error: false, result: [] });
  }

  // db.query(sql, (error, result, fields) => {
  //   if (error) res.json({ error: true, result: error });
  //   let peserta = result;
  //   db.query(sqlTamu, (error, result, fields) => {
  //     if (error) res.json({ error: true, result: error });
  //     let gabung = peserta.concat(result);
  //     const gabungSort = gabung.sort((a, b) => b.timestamp - a.timestamp);
  //     res.json({ error: false, result: gabungSort });
  //   });
  // });
};

let storages = multer.diskStorage({
  destination: (req, file, cb) => {
    if (file.fieldname === "signature") {
      cb(null, "./public/signature");
    } else if (file.fieldname === "cert_logo") {
      cb(null, "./public/cert_logo");
    }
  },
  filename: (req, file, cb) => {
    let filetype = "";
    if (file.mimetype === "image/gif") {
      filetype = "gif";
    }
    if (file.mimetype === "image/png") {
      filetype = "png";
    }
    if (file.mimetype === "image/jpeg") {
      filetype = "jpg";
    }
    cb(null, "img-" + Date.now() + "." + filetype);
  },
});
let uploadSignature = multer({ storage: storages }).fields([
  { name: "signature", maxCount: 10 },
  { name: "cert_logo", maxCount: 1 },
]);

let html = (
  nama,
  cert_title,
  tanggal,
  signature,
  sign,
  logo,
  cert_subtitle,
  cert_description,
  cert_topic,
  cert_background,
  cert_orientation,
  bodyNewPage
) => {
  let BG = `file://${path.resolve(
    __dirname,
    "../public/sertifikat/images/BG.png"
  )}`;
  let TTD2 = [];
  signature.map(x => {
    TTD2.push(`file://${path.resolve(__dirname, `../${x.path}`)}`);
  });
  let signData = "";
  sign.map((item, index) => {
    signData =
      signData +
      `<span style="display: table-cell;">
		 <img style="height: 192px" src="${TTD2[index]}" /><br>
		 <span style="font-size:25px">${item.cert_sign_name}</span><br>
		 <span style="font-size:25px">${item.cert_sign_title}</span>
		</span>`;
  });
  let Icademy = `file://${path.resolve(
    __dirname,
    "../public/sertifikat/images/Icademy.png"
  )}`;
  let Star = `file://${path.resolve(
    __dirname,
    "../public/sertifikat/images/Star.png"
  )}`;
  // let Group1 = `file://${path.resolve(__dirname, '../public/sertifikat/images/Group1.png')}`;
  let Group1 =
    logo.substring(0, 4) === "http"
      ? logo
      : `file://${path.resolve(__dirname, `../${logo}`)}`;

  // BG = path.normalize(BG);
  return `<div style="width:${cert_orientation === "landscape" ? "1675px" : "1160px"
    }; height:${cert_orientation === "landscape" ? "1160px" : "1675px"
    }; padding:20px; text-align:center; border: ${cert_background ? "none" : "10px solid #787878"
    }; font-size:25px; position: relative">
		<img src="${cert_background}" style="width: 100%; height: 100%; position: absolute; top: 0px; left: 0px; z-index: 0" />
    <div style="width:${cert_orientation === "landscape" ? "1625px" : "1110px"
    }; height:${cert_orientation === "landscape" ? "1110px" : "1475px"
    }; padding:20px; ${cert_orientation === "portrait" && "padding-top:150px;"
    } text-align:center; border: ${cert_background ? "none" : "5px solid #787878"
    }; position: relative"><br><br>
           <img style="height: 100px" src="${Group1}" /><br><br>
		   <span style="font-size:50px; font-weight:bold">${cert_title}</span>
		   <br><br>
		   <span style="font-size:35px">${cert_subtitle}</span>
		   <br><br>
		   <span style="font-size:83px"><b>${nama}</b></span><br/><br/><br>
		   <span style="font-size:35px">${cert_description}</span> <br/><br/>
		   <span style="font-size:45px"><b>${cert_topic}</b></span> <br/><br/><br>
		   <span style="font-size:25px">${tanggal}</span><br><br>
           <div style="display:table;width:100%">
		   ${signData}
           </div>
	</div>
	</div>
  ${bodyNewPage != "" ? 
  `<div style="width:${cert_orientation === "landscape" ? "1675px" : "1160px"
}; height:${cert_orientation === "landscape" ? "1160px" : "1675px"
}; margin-top:40px;padding:20px; text-align:center; border: ${cert_background ? "none" : "10px solid #787878"
}; font-size:25px; position: relative">
<img src="${cert_background}" style="width: 100%; height: 100%; position: absolute; top: 0px; left: 0px; z-index: 0" />
<div style="width:${cert_orientation === "landscape" ? "1625px" : "1110px"
}; height:${cert_orientation === "landscape" ? "1110px" : "1475px"
}; padding:20px; ${cert_orientation === "portrait" && "padding-top:150px;"
} border: ${cert_background ? "none" : "5px solid #787878"
}; position: relative"><br><br>
      ${bodyNewPage}
</div>
</div>` : ""}
  `;
};
let options = {
  width: "1755px",
  height: "1240px",
};
let optionsPortrait = {
  width: "1240px",
  height: "1755px",
};

exports.postSertifikat = (req, res) => {
  uploadSignature(req, res, err => {
    let arr = [];
    let signData = JSON.parse(req.body.sign);

    // map
    JSON.parse(req.body.peserta).map(async (elem, idx) => {
      arr.push([
        elem.webinar_id,
        elem.user_id,
        elem.peserta,
        req.files.signature[0].path,
        signData[0].cert_sign_name,
        req.body.cert_title,
        req.body.cert_subtitle,
        req.body.cert_description,
        req.body.cert_topic,
        req.files.cert_logo ? req.files.cert_logo[0].path : "",
        req.body.bodyNewPage
      ]);

      let logo = await db.query(
        `SELECT logo FROM company WHERE company_id='${req.body.company_id}'`
      );
      let tanggal = await db.query(
        `SELECT start_time AS tanggal FROM webinars WHERE id='${req.body.webinar_id}'`
      );
      pdf
        .create(
          html(
            elem.nama,
            req.body.cert_title,
            moment(new Date(tanggal[0].tanggal)).format("dddd, MMMM Do YYYY"),
            req.files.signature,
            signData,
            req.files.cert_logo ? req.files.cert_logo[0].path : logo[0].logo,
            req.body.cert_subtitle,
            req.body.cert_description,
            req.body.cert_topic,
            req.body.certificate_background,
            req.body.certificate_orientation,
            req.body.bodyNewPage
          ),
          req.body.certificate_orientation === "landscape"
            ? options
            : optionsPortrait
        )
        .toFile(
          `./public/webinar/sertifikat/${elem.webinar_id}__${elem.user_id}__${req.body.cert_topic}${elem.email}.pdf`,
          function (err, res) {
            if (err) return console.log(err, "ERROR HTML-PDF");

            // let transporter = nodemailer.createTransport({
            // 	host: env.MAIL_HOST,
            // 	port: env.MAIL_PORT,
            // 	secure: env.MAIL_SECURE,
            // 	auth: {
            // 		user: env.MAIL_USER,
            // 		pass: env.MAIL_PASS
            // 	}
            // });

            let obj_mail = {
              type: "sendmail_certificate_webinar",
              webinar_id: elem.webinar_id,
              user_id: elem.user_id,
              peserta: elem.peserta,
              from: env.MAIL_USER,
              to: elem.email,
              subject:
                "Certificate of Participation : Webinar " + req.body.cert_topic,
              text:
                "Certificate of Participation : Webinar " + req.body.cert_topic,
              attachments: [
                {
                  filename: `certificate-${req.body.cert_topic}.pdf`,
                  path: res.filename,
                  contentType: "application/pdf",
                },
              ],
              html: `
					<b>Hello ${elem.nama},</b><br><br>
					Thank you for your participation in Webinar “${req.body.cert_topic}”. <br>
					Please find attached file for your certificate of participation in the event mentioned above.`,
            };

            sendMailQueue.add(obj_mail, {
              ...options_queue,
              delay: options_queue.delay * idx,
            });
            // transporter.sendMail(obj_mail).then(response => {
            // 	let success = null;
            // 	if (!response.messageId) {
            // 		// failed send email
            // 		success = 0;
            // 	} else {
            // 		// success send email
            // 		success = 1;
            // 	};

            // 	db.query(
            // 		`UPDATE webinar_sertifikat
            // 		 SET status = ${success}
            // 		 WHERE webinar_id = ${elem.webinar_id} AND user_id = '${elem.user_id}' AND peserta = '${elem.peserta}'`,
            // 		(error, response) => {
            // 			console.log(error, response);
            // 		}
            // 	)
            // });
          }
        );
    });

    db.query(
      `INSERT INTO webinar_sertifikat
				(webinar_id, user_id, peserta, ttd, nama, cert_title, cert_subtitle, cert_description, cert_topic, cert_logo, body_new_page)
			VALUES ?;`,
      [arr],
      (error, result) => {
        if (error) {
          return res.json({ error: true, result: error.message });
        } else {
          if (result.affectedRows > 0) {
            return res.json({ error: false, result: "success" });
          } else {
            return res.json({ error: true, result: "error" });
          }
        }
      }
    );
  });
};

exports.browseByCourse = (req, res) => {
  db.query(
    `SELECT *
			FROM webinars
			WHERE training_course_id = '${req.params.course_id}' AND publish='1'
			ORDER BY id DESC`,
    async (error, result, fields) => {
      if (error) res.json({ error: true, result: error });

      if (result.length == 0) {
        res.json({ error: true, result: "Tidak ada webinar yang tersedia" });
      } else {
        for (var i = 0; i < result.length; i++) {
          let moderator = await db.query(
            `SELECT user_id, name FROM user WHERE user_id IN (${result[i].moderator})`
          );
          result[i].moderator =
            moderator.length != 0
              ? moderator
              : { user_id: result[i].moderator, name: "Anonymous" };

          let sekretaris = await db.query(
            `SELECT user_id, name FROM user WHERE user_id IN (${result[i].sekretaris})`
          );
          result[i].sekretaris =
            sekretaris.length != 0
              ? sekretaris
              : { user_id: result[i].sekretaris, name: "Anonymous" };

          let dataPembicara = result[i].pembicara.split(',');
          let pembicaraUser = await db.query(
            `SELECT user_id, name FROM user WHERE user_id IN (${dataPembicara})`
          );
          let pembicaraGuest = await db.query(
            `SELECT voucher AS user_id, name FROM webinar_tamu WHERE voucher IN (${dataPembicara})`
          );
          let pembicara = pembicaraUser.concat(pembicaraGuest);
          result[i].pembicara =
            pembicara.length != 0
              ? pembicara
              : { user_id: result[i].pembicara, name: "Anonymous" };

          let owner = await db.query(
            `SELECT user_id, name FROM user WHERE user_id IN (${result[i].owner})`
          );
          result[i].owner =
            owner.length != 0
              ? owner
              : { user_id: result[0].owner, name: "Anonymous" };

          let peserta = await db.query(
            `SELECT w.*, u.name FROM webinar_peserta w JOIN user u ON u.user_id = w.user_id WHERE webinar_id = '${result[i].id}'`
          );
          result[i].peserta = peserta;

          let tamu = await db.query(
            `SELECT * FROM webinar_tamu WHERE webinar_id='${result[i].id}'`
          );
          result[i].tamu = tamu;
        }
        const { email } = req.app.token;
        await Promise.all(
          result.map(async (item, index) => {
            if (item.course_id !== 0 && item.course_id !== null) {
              let schedule_course = await db.query(
                `SELECT c.require_course_id FROM training_course c WHERE c.id = ${item.training_course_id} LIMIT 1;`
              );
              if (
                schedule_course[0].require_course_id !== 0 &&
                schedule_course[0].require_course_id !== null
              ) {
                let requireCheck =
                  await db.query(`SELECT MIN(r.pass_exam) AS require_check
						FROM (
						SELECT e.title, (SELECT COUNT(r.id) FROM training_exam_result r JOIN training_user tu ON tu.id = r.training_user_id WHERE r.exam_id=e.id AND r.pass=1 AND tu.email='${email}') AS pass_exam
						FROM training_exam e
						WHERE e.course_id = ${schedule_course[0].require_course_id} AND e.exam=1
						) AS r`);
                if (requireCheck[0].require_check === 0) {
                  let reqCourse = await db.query(
                    `SELECT title FROM training_course WHERE id=${schedule_course[0].require_course_id}`
                  );
                  result[index].on_schedule = "0";
                  result[
                    index
                  ].message = `This live class is require you to pass course '${reqCourse[0].title}'`;
                }
              }
            }
          })
        );
        res.json({ error: false, result: result });
      }
    }
  );
};

function AWSSTRORE(req, pathaws) {

  return new Promise(async (resolve) => {
    if (!env.AWS_BUCKET) {
      return resolve(null);
    }
    let pathfile = req.file.path;
    let keyName = `${env.AWS_BUCKET_ENV}/${pathaws}`;

    let params = {
      ACL: "public-read",
      Bucket: env.AWS_BUCKET,
      Body: fs.createReadStream(pathfile),
      Key: `${keyName}/${req.file.filename}`,
      ContentDisposition: "inline",
      ContentType: req.file.mimetype,
    };

    s3.upload(params, async (err, data) => {
      if (err) {
        resolve(null);
      }
      if (data) {
        fs.unlinkSync(pathfile);
        resolve(data);
      } else {
        resolve(null);
      }
    });
  });
}

exports.guestRegistration = async (req, res) => {
  try {

    uploadSlip(req, res, async (err) => {

      if (req.file) {
        req.body.slip = req.file;
        req.body.isPaid = 1
      }
      
      if (!req.body.isPaid) {
        req.body.isPaid = 0;
      }
      let validInput = await validatorRequest.guestRegistration(req.body);
      if (!validInput.isError) {
        const validRequest = validInput.result;
        const webinarHasSameParticipant = await db.query(`SELECT * FROM webinar_tamu WHERE webinar_id = ? AND email = ?`, [req.body.webinarId, req.body.email]);
        const isParticipantsHasAdded = webinarHasSameParticipant.length ? true : false;

        if(isParticipantsHasAdded) {
          res.json({error: true, result: 'Participant/Guest already added..'});
        }else{
          let voucher = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
          let query = '';
          let dtoArrayDB = [];

          if (validRequest.isPaid == 0) {
            query = `INSERT INTO webinar_tamu(webinar_id, name, email, phone, status, voucher) 
                      VALUE(?,?,?,?,?,? )`;
            dtoArrayDB = [validRequest.webinarId, validRequest.name, validRequest.email, validRequest.phone, "0", voucher];
          } else {

            if (validRequest.slip) {
              try {
                let pathAWS = 'webinar/registration-guest';
                let aws_store = await AWSSTRORE(req, pathAWS);
                validRequest.slip.filename = aws_store.Location;
                validRequest.slip.awsHandler = true;
              } catch (e) {
              }
            }
            let pathURL = `${env.APPS_URL}/webinar/registration-guest/${validRequest.slip.filename}`;
            if (validRequest.slip.awsHandler) {
              pathURL = validRequest.slip.filename;
            }

            query = `
                    INSERT INTO webinar_transactions(webinar_id,is_paid,name,email,phone,address,voucher,slips) 
                    value(?,?,?,?,?,?,?,?);
                  `;
            const slipURL = validRequest.slip ? `'${pathURL}'` : null;
            dtoArrayDB = [validRequest.webinarId, validRequest.isPaid, validRequest.name, validRequest.email, validRequest.phone, validRequest.address, voucher, slipURL];
                
          }

          let result = await db.query(query, dtoArrayDB);
          if (result.insertId) {
            res.json({ error: false, result: "Registration is success, Please confirm with the webinar committee" });
          } else {
            res.json({ error: true, result: "Registration is failed, please confirm with the webinar committee" });
          }  
        }
        
      } else {
        res.json({ error: true, result: validInput.result });

      }
    });
  } catch (e) {
    console.error(e, 'webinar.js::guestRegistration');
    res.json({ error: true, result: "Registration is failed, please confirm with the webinar committee" })
  }
}



exports.guestAction = async (req, res) => {
  try {

    let validInput = await validatorRequest.guestAction({
      webinar_id: req.body.webinar_id,
      id: req.body.id,
      approved_by: req.app.token.user_id
    });

    let queryString = null;
    let params = null;

    if (validInput) {

      if (req.query.action === 'approve') {
        queryString = `INSERT INTO webinar_tamu(webinar_id,name,email,phone,voucher,status) 
                      SELECT webinar_id,name,email,phone,voucher,0 as status from webinar_transactions 
                      WHERE webinar_id=? and id=? and status='0' limit 1;`;
        params = [validInput.webinar_id, validInput.id];

        let resultTamu = await db.query(queryString, params);

        if (resultTamu.insertId) {

          queryString = `UPDATE webinar_transactions SET status='1', approved_by=? 
                        WHERE webinar_id=? and id=?;`;
          params = [validInput.approved_by, validInput.webinar_id, validInput.id];
          let UpdateTamu = await db.query(queryString, params);
          if (UpdateTamu) {
            res.json({ error: false, result: "Success approve guest" });
          } else {
            res.json({ error: true, result: "Failed approve guest" });
          }
        } else {
          res.json({ error: true, result: "Guest not found" });
        }
      } else {
        queryString = `UPDATE webinar_transactions SET status='2', approved_by=?   
                        WHERE webinar_id=? and id=?;`;
        params = [validInput.approved_by, validInput.webinar_id, validInput.id];
        let UpdateTamu = await db.query(queryString, params);
        if (UpdateTamu) {
          res.json({ error: false, result: "Success" });
        } else {
          res.json({ error: true, result: "Failed" });
        }
      }
    } else {
      res.json({ error: true, result: "Error validation" });
    }
  } catch (e) {
    console.error(e, "GuestAction");
    res.json({ error: true, result: "Approve is failed" })
  }
}
exports.setSpeakerExternalWebinar = async (req, res) => {
  // transaction
  const crypto = require("crypto");
  const helper = require("./helper/generate")
  const voucher = `external-${helper.randomString(17)}`;
  let msg = { error: false, result: "Success add speaker" };
  let errors = false;
  let queryString = null;
  let params = null;

  try {

    let validInput = await validatorRequest.setSpeakerExternal(req.body);

    if (validInput) {
      // check double
      queryString = `select * from webinar_role_external where webinar_id = ? and lower(email) = ?;`;
      params = [validInput.webinarId,validInput.email.toLowerCase()];
      let getduplicate =  await db.query(queryString, params);

      if(getduplicate.length){
          msg = { error: true, result: "User Already in used" };
          errors = true;
      }else{
        // clear data
        queryString = `delete from webinar_role_external where webinar_id = ? and lower(email) = ?;`;
        params = [validInput.webinarId,validInput.email.toLowerCase()];
        let getRemove =  await db.query(queryString, params);
  
        // set data
        queryString = `INSERT webinar_role_external(webinar_id,name,email,phone,roles,voucher,created_by) value(?); `;
        params = [[validInput.webinarId, validInput.name,validInput.email.toLowerCase(),validInput.phone,validInput.role.toLowerCase(),voucher, req.app.token.user_id]];
        let getId = {};
        
        console.log(getId,getRemove,99090);
        getId = await db.query(queryString, params);
        if(!getId.insertId){
          msg = { error: true, result: "Failed add speaker" };
          return res.json(msg)
          errors = true;
        }
        
        validInput.userId = voucher;
        // get current speaker
        queryString = `select w.pembicara from webinars w where w.id = ?;`;
        params = [validInput.webinarId];
  
        let result = await db.query(queryString, params);
        let state = false;
        
        try {
          if (result[0].pembicara === '') {
            result[0].pembicara = validInput.userId.toString();
          } else {
            let split = result[0].pembicara.split(",");
  
            split.forEach((str) => {
              if (str.toString() === validInput.userId.toString()) {
                state = true;
              }
            });
  
            split.push(validInput.userId);
            result[0].pembicara = split.toString();
          }
        } catch (e) {
          console.log(e,"3188")
          result[0].pembicara = validInput.userId.toString();
        }
  
        if (!state){
          // set data
          queryString = `UPDATE webinars SET pembicara=?   
                          WHERE id=?;`;
          params = [result[0].pembicara, validInput.webinarId];
    
          let UpdateTamu = await db.query(queryString, params);
          if (!UpdateTamu) {
            msg = { error: true, result: "Failed add speaker" };
            errors = true;
          }
        }

      }


    } else {
      msg = { error: true, result: "Failed Error Validation" };
      errors = true;
    }
  } catch (e) {
    console.error(e, "ExternalRoleWebinar");
    msg = { error: true, result: "Cannot add speakers" };
    errors = true;
  }

  // if(errors){
  //   req.body = {
  //     webinarId: req.body.webinarId,
  //     bulk: [voucher]
  //   }
  //   let deletes = await this.deleteSpeakerExternalWebinar(req, res, true);
  //   console.log(deletes,"DELETE");
  // }

  return res.json(msg)
}

exports.deleteSpeakerExternalWebinar = async (req, res, bypass) => {
  let msg = { error: false, result: "Success delete speaker" };
  try {

    let validInput = await validatorRequest.delSpeakerExternal(req.body);

    let queryString = null;
    let params = null;

    if (validInput) {
      queryString = `select w.pembicara from webinars w where w.id = '${validInput.webinarId}';`;
      params = [validInput.webinarId];

      let result = await db.query(queryString, params);

      try {
        if (result[0].pembicara.length > 0) {
          let split = result[0].pembicara.split(",");
          let tmp = [];
          split.forEach((str) => {
            if (validInput.bulk.indexOf(str) == -1){
              tmp.push(str);
            }
          });
          result[0].pembicara = tmp.toString();
        }else{
          result[0].pembicara = '';
        }
      } catch (e) {
        console.log(e,'3255')
      }

      console.log(result[0].pembicara,validInput.bulk,9998)

      queryString = `UPDATE webinars SET pembicara=? WHERE id=?;`;
      params = [result[0].pembicara.length > 0 ? result[0].pembicara : '', validInput.webinarId];
      let UpdateTamu = await db.query(queryString, params);

      if(UpdateTamu){
        queryString = `${queryString} DELETE FROM webinar_role_external WHERE webinar_id=? and voucher in(?);`;
        params.push(validInput.webinarId, validInput.bulk);
  
        let removeTamu = await db.query(queryString, params);
      }
      if (!UpdateTamu) {
        msg = { error: true, result: "Failed delete speaker" };
      }

    } else {
      msg = { error: true, result: "Error validation2" };
    }
  } catch (e) {
    console.error(e, "GuestAction");
    msg = { error: true, result: "Cannot delete speakers" };
  }

  // if(bypass){
  //   return msg;
  // }else{
  // }
  return res.json(msg);
}

exports.sendMailSpeakerExternalWebinar = async (req, res, bypass) => {
  let msg = { error: false, result: "Success delete speaker" };
  try {

    let validInput = await validatorRequest.sendMailSpeakerExternal(req.body);

    let queryString = null;
    let params = [];

    if (validInput) {

      let collectEmail = []
      let guest = [];
      let external = [];
      let peserta = [];
      
      validInput.dataMail.forEach((str)=>{ 
        collectEmail.push(str.email) 
        if(str.type == 'guest'){
          guest.push(str.user_id);
        }else if(str.type == 'external'){
          external.push(str.user_id);
        }else if(str.type == 'peserta'){
          peserta.push(str.user_id);
        }
      });
      validInput.collectEmail = collectEmail;
      
      let dataWebinar = await db.query(
        `SELECT judul,start_time,end_time,calendar_event FROM webinars WHERE id='${validInput.webinarId}'`
      );

      validInput = { ...validInput, ...dataWebinar[0] };
      let sends = await send.sendEmailWebinarExternalRole(validInput);
      console.log(sends)
      if(sends){
        queryString = ` 
          ${guest.length == 0 ? '' : 'update webinar_tamu set status=4 where voucher in(?);'}
          ${external.length == 0 ? '' : 'update webinar_role_external set status=4 where voucher in(?);'}
        `;
        
        if(guest.length) params.push(guest);
        if(external.length) params.push(external);
  
        let updateStatus = await db.query(queryString,params);
      }
      msg = { error: false, result: "Send mail is success" };

    } else {
      msg = { error: true, result: "Error validation" };
    }
  } catch (e) {
    console.error(e, "GuestAction");
    msg = { error: true, result: "Send mail is failed" };
  }

  return res.json(msg);
}

exports.setSpeakerWebinar = async (req, res) => {
  try {

    let validInput = await validatorRequest.setSpeaker(req.body);

    let queryString = null;
    let params = null;

    if (validInput) {
      queryString = `select w.pembicara from webinars w where w.id = '${validInput.webinarId}';`;
      params = [validInput.webinarId];

      let result = await db.query(queryString, params);
      let state = false;

      try {
        if (result[0].pembicara === '') {
          result[0].pembicara = validInput.userId.toString();
        } else {
          let split = result[0].pembicara.split(",");

          split.forEach((str) => {
            if (str.toString() === validInput.userId.toString()) {
              state = true;
            }
          });

          split.push(validInput.userId);
          result[0].pembicara = split.toString();
        }
      } catch (e) {
        result[0].pembicara = validInput.userId.toString();
      }

      if (state) {
        return res.json({ error: false, result: "Success add speaker" });
      }

      queryString = `UPDATE webinars SET pembicara=?   
                      WHERE id=?;`;
      params = [result[0].pembicara, validInput.webinarId];
      if (validInput.option === 'guest') {
        queryString = `${queryString} UPDATE webinar_tamu SET roles='speaker'   
                          WHERE webinar_id=? and voucher=?;`;
        params.push(validInput.webinarId, validInput.userId);
      }

      let UpdateTamu = await db.query(queryString, params);
      if (UpdateTamu) {
        res.json({ error: false, result: "Success add speaker" });
      } else {
        res.json({ error: true, result: "Failed add speaker" });
      }
    } else {
      res.json({ error: true, result: "Error validation" });
    }
  } catch (e) {
    console.error(e, "GuestAction");
    res.json({ error: true, result: "Cannot add speakers" })
  }
}

exports.deleteSpeakerWebinar = async (req, res) => {
  try {

    let validInput = await validatorRequest.setSpeaker(req.body);

    let queryString = null;
    let params = null;

    if (validInput) {
      queryString = `select w.pembicara from webinars w where w.id = '${validInput.webinarId}';`;
      params = [validInput.webinarId];

      let result = await db.query(queryString, params);

      try {
        if (result[0].pembicara.length > 0) {
          let split = result[0].pembicara.split(",");
          let tmp = [];
          split.forEach((str) => {
            if (str != validInput.userId) {
              tmp.push(str);
            }
          });
          result[0].pembicara = tmp.toString();
        }
      } catch (e) {
        result[0].pembicara = validInput.userId.toString();
      }

      queryString = `UPDATE webinars SET pembicara=?   
                      WHERE id=?;`;
      params = [result[0].pembicara.length > 0 ? result[0].pembicara : '', validInput.webinarId];

      if (validInput.option === 'guest') {
        queryString = `${queryString} UPDATE webinar_tamu SET roles=null   
                          WHERE webinar_id=? and voucher=?;`;
        params.push(validInput.webinarId, validInput.userId);
      }

      let UpdateTamu = await db.query(queryString, params);
      if (UpdateTamu) {
        res.json({ error: false, result: "Success delete speaker" });
      } else {
        res.json({ error: true, result: "Failed delete speaker" });
      }

    } else {
      res.json({ error: true, result: "Error validation2" });
    }
  } catch (e) {
    console.error(e, "GuestAction");
    res.json({ error: true, result: "Cannot delete speakers" })
  }
}

exports.updateIsPaid = async (req, res) => {
  try {
    let state = [0, 1]
    if (state.indexOf(Number(req.query.action)) > -1 && !isNaN(Number(req.params.id))) {
      let queryString = `UPDATE webinars set is_paid=? where id=?`;
      let params = [req.query.action, req.params.id];

      //return console.log(queryString, params);
      let result = await db.query(queryString, params);
      if (result) {
        res.json({ error: false, result: "Update is success" });
      } else {
        res.json({ error: true, result: "Update is failed" });
      }
    } else {
      res.json({ error: true, result: "Error Validation" });
    }

  } catch (e) {
    res.json({ error: true, result: "Cannot update type webinar" })
  }
}

exports.readLiveClassByCourse = async (req, res) => {
  try {
    const dtoDetailReadLiveClassByCourse = {
      idCourse: req.params.idCourse,
      idCompany: req.params.idCompany,
    };

    //Validate Request
    const retrieveValidRequest = await validatorRequest.retrieveRequestExamByCourse(dtoDetailReadLiveClassByCourse);
    const retrieveData = await moduleDB.retrieveRepoLiveClassByCourse(retrieveValidRequest);
    res.json({ error: false, result: retrieveData })

  } catch (error) {
    res.json({ error: true, result: error.message })
  }
}

exports.sendOfferLetter = async (req, res) => {
  try {
    const dtoDetailTextOfferLetter = {
      textOfferLetter: req.body.textOfferLetter,
      peserta: req.body.peserta
    };

    //Validate Request
    const retrieveValidRequest = await validatorRequest.retrieveOfferLetterRequest(dtoDetailTextOfferLetter);

    let confirmRequest = retrieveValidRequest.peserta.map(dataPeserta => {
      const regExpEmail = /[a-z0-9]+@[a-z]+\.[a-z]{2,3}/;
      const isStringEmail = regExpEmail.test(dataPeserta.email);
      let temp = dtoDetailTextOfferLetter.textOfferLetter.replace("{{name}}", dataPeserta.name);
      return {
        ...dataPeserta,
        textEditor: temp
      }
    });

    if (confirmRequest.length) {
      for (let i = 0; i < confirmRequest.length; i++) {
        const elem = confirmRequest[i];
        pdf
          .create(
            elem.textEditor
          )
          .toFile(
            `./public/webinar/sertifikat/${elem.email}.pdf`,
            function (err, res) {
              if (err) return console.log(err, "ERROR HTML-PDF");
              let obj_mail = {
                type: "sendmail_offer_letter",
                from: env.MAIL_USER,
                to: elem.email,
                subject:
                  "Offer Letter ICADEMY",
                text:
                  "Offer Letter ICADEMY",
                attachments: [
                  {
                    filename: `Offer-Letter-ICADEMY-${elem.email}.pdf`,
                    path: res.filename,
                    contentType: "application/pdf",
                  },
                ],
                html: elem.textEditor
              };

              sendMailQueue.add(obj_mail, {
                ...options_queue,
                delay: options_queue.delay * i,
              });
            }
          );
      }
    }

    res.json({ error: false, result: 'Success Sending Offer Letter to Email' })
  } catch (error) {
    res.json({ error: true, result: error.message })
  }
}