const db = require('../configs/database');

exports.updateCourse = (req, res) => {
    db.query(
        `UPDATE training_course
        SET
        require_course_id = ?,
        scheduled = ?,
        start_time = ?,
        end_time = ?
        WHERE id = ?;`,
        [String(req.body.require_course_id).length ? req.body.require_course_id : 0, req.body.scheduled, req.body.start_time, req.body.end_time, req.params.id],
        async (error, result) => {
            if(error) {
                return res.json({error: true, result: error});
            }
            else{
                return res.json({error: false, result: result})
            }
        }
    );
};

exports.updateExam = (req, res) => {
    db.query(
        `UPDATE training_exam
        SET
        course_id = ?,
        scheduled = ?,
        start_time = ?,
        end_time = ?
        WHERE id = ?;`,
        [String(req.body.course_id).length ? req.body.course_id : 0, req.body.scheduled, req.body.start_time, req.body.end_time, req.params.id],
        async (error, result) => {
            if(error) {
                return res.json({error: true, result: error});
            }
            else{
                return res.json({error: false, result: result})
            }
        }
    );
};

exports.updateLiveclass = (req, res) => {
    db.query(
        `UPDATE webinars
        SET
        training_course_id = ?,
        start_time = ?,
        end_time = ?
        WHERE id = ?;`,
        [String(req.body.training_course_id).length ? req.body.training_course_id : 0, req.body.start_time, req.body.end_time, req.params.id],
        async (error, result) => {
            if(error) {
                return res.json({error: true, result: error});
            }
            else{
                return res.json({error: false, result: result})
            }
        }
    );
};