const path = require('path');
const multer = require('multer');
const moment = require("moment");
const AWS = require('aws-sdk');
const fs = require('fs');

const env = require('../env.json');
const model = require("../repository").crmTask;
const validator = require("../validator").crmTask;
const samplesTaskImport = 'https://icademys.s3.ap-southeast-1.amazonaws.com/staging/crm_task/tasks-sample.xlsx';
const storage = multer.diskStorage({
  filename: function (req, file, cb) {
    cb(null, file.originalname);
  }
})
const s3 = new AWS.S3({
  accessKeyId: env.AWS_ACCESS,
  secretAccessKey: env.AWS_SECRET
});

let uploadImage = multer({ storage: storage }).single('files');

let storageExcel = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, './public/company');
  },
  filename: (req, file, cb) => {
    cb(null, 'excel-' + Date.now() + '-' + file.originalname);
  }
});
let uploadExcel = multer({storage: storageExcel}).single('excel');

const storeTaskChecklistItem = async(req)=>{
	let msg = { error: false, result : [] }
	try {
		msg = await validator.storeTaskChecklistItem(req.body);
		if(!msg.error){
			msg = await model.storeTaskChecklistItem(msg.result,req.params);
		}
	} catch (error) {
		console.log(error);
		msg.error = true;
		msg.result = error;
	}
	return msg;
}

exports.getTask = async(req,res, next)=>{
	let msg = { error: false, result : [] }
	try {
		let user = req.app.token;
		let query = req.query;
		if(query.filter){
			query.filter = JSON.parse(query.filter);
			if(typeof query.filter.deadline === 'object'){
				query.filter.deadline = query.filter.deadline.value
			}
			// if(!query.filter.specific_user_id) query.filter.specific_user_id = user.user_id
			if(!query.filter.project_member_id) query.filter.project_member_id = query.filter.specific_user_id

		}else if(query.type === "kanban"){
			query.filter = {}
			if(!query.filter.project_id) query.filter.project_id = query.project_id
			if(!query.filter.priority_id) query.filter.priority_id = query.priority_id
			if(!query.filter.milestone_id) query.filter.milestone_id = query.milestone_id
			if(!query.filter.specific_user_id) query.filter.specific_user_id = query.specific_user_id
			if(!query.filter.deadline) query.filter.deadline = query.deadline
			if(!query.filter.quick_filter) query.filter.quick_filter = query.quick_filter
		}else {
			query.filter = {}
		}
		params = {
			...req.params,
			user ,
			query
		}
		//msg.result = params;
		msg = await model.getTask(null,params);
	} catch (error) {
		console.log(error);
		msg.error = true;
	}
	res.json(msg);
}

exports.getTaskList = async(req,res, next)=>{
	let msg = { error: false, result : [] }
	try {
		let user = req.app.token;
		let query = req.query;
		if(query.filter){
			query.filter = JSON.parse(query.filter);
			if(typeof query.filter.deadline === 'object'){
				query.filter.deadline = query.filter.deadline.value
			}
			// if(!query.filter.specific_user_id) query.filter.specific_user_id = user.user_id
			if(!query.filter.project_member_id) query.filter.project_member_id = query.filter.specific_user_id

		}else if(query.type === "kanban"){
			query.filter = {}
			if(!query.filter.project_id) query.filter.project_id = query.project_id
			if(!query.filter.priority_id) query.filter.priority_id = query.priority_id
			if(!query.filter.milestone_id) query.filter.milestone_id = query.milestone_id
			if(!query.filter.specific_user_id) query.filter.specific_user_id = query.specific_user_id
			if(!query.filter.deadline) query.filter.deadline = query.deadline
			if(!query.filter.quick_filter) query.filter.quick_filter = query.quick_filter
		}else {
			query.filter = {}
		}
		params = {
			...req.params,
			user ,
			query
		}
		//msg.result = params;
		msg = await model.getTaskList(null,params);
	} catch (error) {
		console.log(error);
		msg.error = true;
	}
	res.json(msg);
}

exports.getTaskFilterData = async(req,res, next)=>{
	let msg = { error: false, result : [] }
	try {
		msg = await model.getTaskFilterData(req.app.token, req.params);
	} catch (error) {
		console.log(error);
		msg.error = true;
	}
	res.json(msg);
}

exports.getGanttData = async(req,res,ext)=>{
	let msg = { error: false, result : [] }
	try {
		$project_id = 0, $group_by = "milestones", $milestone_id = 0, $user_id = 0, $status = ""
		let query = req.params;
		query.group_by = query.group_by || 'milestones';
		query.project_id = query.project_id === '0' ? null : query.project_id;
		query.milestone_id = query.milestone_id === '0' ? null : query.milestone_id;
		query.user_id = req.query.specific_user_id === '0' ? null : req.query.specific_user_id;
		query.status = query.status === '0' ? null : query.status.split(',');
		msg = await model.getGanttData(false,query);
	} catch (error) {
		console.log(error);
		msg.error = true;
	}
	res.json(msg);
}

exports.getAttributeProject = async(req,res, next)=>{
	let msg = { error: false, result : [] }
	try {
		req.params = { ...req.params, ...req.app.token};
		msg = await model.getAttributeProject(null,req.params);
	} catch (error) {
		console.log(error);
		msg.error = true;
	}
	res.json(msg);
}

exports.getTaskComments = async(req,res, next)=>{
	let msg = { error: false, result : [] }
	try {
		msg = await model.getTaskComments(null,{ ...req.params, user: req.app.token });
	} catch (error) {
		console.log(error);
		msg.error = true;
	}
	res.json(msg);
}

exports.getTaskActivity = async(req,res, next)=>{
	let msg = { error: false, result : [] }
	try {
		let params = '';
		if(req.params.idTask){
			params = {
				log_type_id: req.params.idTask,
				log_type: 'task',
				log_for_id: req.params.idProject || 1
			};
		}
		// params.log_type, params.log_type_id, params.log_for_id
		msg = await model.getTaskActivity(null,params);
	} catch (error) {
		console.log(error);
		msg.error = true;
	}
	res.json(msg);
}

exports.storeTask = async(req,res, next)=>{
	let msg = { error: false, result : [] }
	try {
		msg = await validator.store(req.body);
		if(!msg.error){
			let err = false;
			if(msg.result.start_date) msg.result.start_date = moment(msg.result.start_date).format("YYYY-MM-DD");
			if(msg.result.deadline) msg.result.deadline = moment(msg.result.deadline).format("YYYY-MM-DD");
			msg.result.created_by = req.app.token.user_id;

			if(req.query.subtask){
				if(!msg.result.parent_task_id){
					err = true;
					msg.error = true;
					msg.result = 'Validation is failed';
				}
			}

			if(!err) msg = await model.storeTask(msg.result,req.query);
		}
	} catch (error) {
		console.log(error);
		msg.error = true;
		msg.result = error;
	}
	res.json(msg);
}

exports.editTask = async(req,res, next)=>{
	let msg = { error: false, result : [] }
	try {
		if(req.params.singleInput === 'checklist_item'){
			msg = await storeTaskChecklistItem(req, res);
		}else{
			msg = await validator.edit({ ...req.body, id: req.params.id }, req.params.singleInput);
			if(!msg.error){

				if(msg.result.start_date) msg.result.start_date = moment(msg.result.start_date).format("YYYY-MM-DD");
				if(msg.result.deadline) msg.result.deadline = moment(msg.result.deadline).format("YYYY-MM-DD");
				msg.result.created_by = req.app.token.user_id;
				msg = await model.editTask(msg.result);
			}
		}
	} catch (error) {
		console.log(error);
		msg.error = true;
		msg.result = error;
	}
	res.json(msg);
}

exports.editTaskBatch = async(req,res, next)=>{
	let msg = { error: false, result : [] }
	try {

		msg = await validator.editBatch(req.body);
		if(!msg.error){

			if(msg.result.start_date) msg.result.start_date = moment(msg.result.start_date).format("YYYY-MM-DD");
			if(msg.result.deadline) msg.result.deadline = moment(msg.result.deadline).format("YYYY-MM-DD");
			msg.result.created_by = req.app.token.user_id;
			let err = null;
			let data = null;
			const form = msg.result;
			for(let i=0 ; i < form.selectedTask.length; i++){
				data = await model.editTask({ ...form, id:form.selectedTask[i] });
				if(data.error) err = data.error;
			}
			if(err){
				msg.error = err;
			}
		}
	} catch (error) {
		console.log(error);
		msg.error = true;
		msg.result = error;
	}
	res.json(msg);
}

exports.editTaskKanban = async(req,res, next)=>{
	let msg = { error: false, result : [] }
	try {

		msg = await validator.editKanban(req.body);
		if(!msg.error){
			let state = [];
			await Promise.all(msg.result.map(async (item, index) => {
				item.created_by = req.app.token.user_id;
				msg = await model.editTask(item);
				if(msg.error){
					state.push(msg);
					return;
				}
			}));

			if(state.length){
				msg = state[0];
			}else{
				msg.result = 'success';
			}

		}
	} catch (error) {
		console.log(error);
		msg.error = true;
		msg.result = error;
	}
	res.json(msg);
}

exports.storeTaskReminder = async(req,res, next)=>{
	let msg = { error: false, result : [] }
	try {
		msg = await validator.storeTaskReminder({ ...req.body, created_by: req.app.token.user_id });
		if(!msg.error){
			msg = await model.storeTaskReminder(msg.result);
		}
	} catch (error) {
		console.log(error);
		msg.error = true;
		msg.result = error;
	}
	res.json(msg);
}

exports.deleteTaskReminder = async(req,res, next)=>{
	let msg = { error: false, result : [] }
	try {
		msg = await validator.deleteTaskReminder({ created_by: req.app.token.user_id, ...req.params, deleted:1, type:'reminder' });
		if(!msg.error){
			msg = await model.deleteTaskReminder(msg.result);
		}
	} catch (error) {
		console.log(error);
		msg.error = true;
		msg.result = error;
	}
	res.json(msg);
}

const s3Upload = async(req,res,path_name)=>{
	return new Promise(async (hasil)=>{
		uploadImage(req, res,async (err) => {
			if(req.file){
				let path = req.file.path;
				let keyName = `${env.AWS_BUCKET_ENV}/${path_name}`;
				let params = {
					ACL: 'public-read',
					Bucket: env.AWS_BUCKET,
					Body: fs.createReadStream(path),
					Key: `${keyName}/${req.file.originalname}`,
					Metadata: {
						'Content-Type': req.file.mimetype,
						'Content-Disposition': 'inline'
					}
				};
				s3.upload(params, (err, data) => {
					if (data) {
						fs.unlinkSync(path);
					}

					req.body.files = data.Location
					return hasil(req);
				});
			}else{
				return hasil(req);
			}
		});
	});
}

exports.storeTaskComment = async(req,res, next)=>{
	let msg = { error: false, result : [] }
	try {

		req = await s3Upload(req,res,'crm_task');
		msg = await validator.storeTaskComment({ ...req.body, created_by: req.app.token.user_id });
		if(!msg.error){
			msg = await model.storeTaskComment(msg.result);
		}
	} catch (error) {
		console.log(error);
		msg.error = true;
		msg.result = error;
	}
	res.json(msg);
}

// for crud pin_comment + like_comment
exports.storeTaskCommentAttribute = async(req,res, next)=>{
	let msg = { error: false, result : [] }
	try {
		msg = await validator.storeTaskCommentAttribute({ ...req.body, created_by: req.app.token.user_id, ...req.params });
		if(!msg.error){
			let status = 'store';
			if(msg.result.id) status = 'delete';

			msg = await model.storeTaskCommentAttribute(msg.result,{ status });
		}
	} catch (error) {
		console.log(error);
		msg.error = true;
		msg.result = error;
	}
	res.json(msg);
}

exports.deleteTaskComment = async(req,res, next)=>{
	let msg = { error: false, result : [] }
	try {
		msg = await validator.deleteTaskComment({ ...req.body, created_by: req.app.token.user_id });
		if(!msg.error){
			msg = await model.deleteTaskComment(msg.result);
		}
	} catch (error) {
		console.log(error);
		msg.error = true;
		msg.result = error;
	}
	res.json(msg);
}

exports.storeTaskLogged = async(req,res, next)=>{
	let msg = { error: false, result : [] }
	try {
		msg = await validator.storeTaskLogged({ ...req.body, user_id: req.app.token.user_id });		
		if(!msg.error){
			msg = await model.storeTaskLogged(msg.result);
		}
	} catch (error) {
		console.log(error);
		msg.error = true;
		msg.result = error;
	}
	res.json(msg);
}

function AsyncUploadFile(req,res,next){
	return new Promise(async (hasil)=>{
		uploadExcel(req, res, async (err) => {
			if(!req.file) {
				return hasil({ error: true, result: err })
			} else {
				return hasil({ error: false, result: { file: req.file, body:req.body } });
			}
		});
	})	
}
exports.importTaskChecking = async (req, res, next)=>{
	let msg = { error: false, result : [] }
	let AsyncUploadFileData = await AsyncUploadFile(req,res,next);
	if(AsyncUploadFileData.error){
		msg = AsyncUploadFileData;
	}else{
		try {
			const companyID = AsyncUploadFileData.result.body.companyID || 45;
			const filePath = path.resolve(__dirname, '../public/company/'+AsyncUploadFileData.result.file.filename);
			msg = await model.importTask({ companyID,filePath, userId:req.app.token.user_id });
		} catch (error) {
			msg.result = error;
		}
		res.json(msg);
	}
}

exports.importTaskStore = async(req,res, next)=>{
	let msg = { error: false, result : [] }
	try {
		msg = await validator.storeImport(req.body);
		if(!msg.error){
			let err = false;
			let state = {err:[],sucess:null};
			for(let i=0; i< msg.result.data.length; i++){
				let data = msg.result.data[i];
				if(data.start_date) data.start_date = moment(data.start_date).format("YYYY-MM-DD");
				if(data.deadline) data.deadline = moment(data.deadline).format("YYYY-MM-DD");
				data.created_by = req.app.token.user_id;
				if(!err){
					result = await model.storeTask(data,req.query);
					if(msg.error){
						state.err.push(result);
					}else{
						state.sucess = result;
					}
				}
			}
		}
	} catch (error) {
		console.log(error);
		msg.error = true;
		msg.result = error;
	}
	res.json(msg);
}
