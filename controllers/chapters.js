var env = require('../env.json');
var db = require('../configs/database');
var conf = require('../configs/config');


var multer = require('multer');
var storage = multer.diskStorage({
    destination: (req,  file,  cb) => {
        cb(null, './public/chapter/');
    },
    filename: (req, file, cb) => {
        cb(null,file.originalname);
    }
});


const filterFile = ( (req, file, cb) => {
    if(file.mimetype === 'audio/mp4' ||
    file.mimetype === 'application/mp4' ||
    file.mimetype === 'video/mp4' ||
    file.mimetype === 'image/jpg' ||
    file.mimetype === 'image/png' ||
    file.mimetype === 'image/gif' ||
    file.mimetype === 'image/jpeg' ||
    file.mimetype === 'application/pdf'){
        cb(null, true);
    }else{
        cb(null, false);
    }
})



const uploadVideo = multer({storage: storage, fileFilter: filterFile}).single('chapter_video');
const uploadImage = multer({storage: storage, fileFilter: filterFile}).single('thumbnail');
const uploadAttachment = multer({storage: storage, fileFilter: filterFile}).array('attachment_id', 12);

exports.addChapter = function(req, res, next){

    uploadVideo(req, res, (err) => {
        if(!req.file){
             res.json({error: true, result: err});
        }else{

            let keyName = `${env.APP_URL}/chapter`;

            let data = {
                course_id: req.body.course_id,
                company_id: req.body.company_id,
                chapter_number: req.body.chapter_number,
                chapter_title: req.body.chapter_title,
                chapter_body: req.body.chapter_body,
                chapter_video: `${keyName}/${req.file.originalname}`,
                attachment_id: req.body.attachment_id
            };

            let query = `INSERT INTO course_chapter 
            (chapter_id, course_id, company_id, chapter_number,chapter_title, chapter_body, chapter_video, attachment_id) VALUES
            (null,'${data.course_id}','${data.company_id}','${data.chapter_number}','${data.chapter_title}','${data.chapter_body}','${data.chapter_video}','${data.attachment_id}')`;

            console.log(query);

            db.query(query, (err, result, fields) => {
                if(err){
                     res.json({error:true, result:err})
                }else{
                    db.query(`SELECT * FROM course_chapter WHERE chapter_id = '${result.insertId}'`, (err, result, fields) =>{
                         res.json({error:false, result: result[0] });
                    })
                }
            });
        }
    });
};


exports.addCourseChapter = function(req, res, next){

    let data = {
        course_id: req.body.course_id,
        chapter_id: req.body.chapter_id,
        user_id: req.body.user_id
    }

    let id = data.course_id+data.chapter_id+data.user_id;

    let query = `INSERT IGNORE INTO user_course_chapter
    ( user_course_chapter_id, course_id, chapter_id, user_id) VALUES ('${id}','${data.course_id}','${data.chapter_id}','${data.user_id}') `;

    db.query(query, (err, result, rows) => {
        if(err){
             res.json({error: true, result: result});
        }else{
             res.json({error: false, result: result[0]});
        }
    });
};


exports.getAllChapters = function(req, res, next) {

    let query = 'SELECT * FROM course_chapter';

    db.query(query, (err, result) => {
        if(err){
             res.json({error: true, result:err});
        }else{
             res.json({error: false, result:result});
        }
    })
}

exports.getChapterById = function(req, res, next){
    
    let query = `SELECT * FROM course_chapter WHERE chapter_id = ${req.params.chapter_id}`;
    db.query(query, (err, result, fields) => {
        if(err){
             res.json({error: true, result: err});
        }else{
             res.json({error: false, result: (result.length !== 0) ? result: [] });
        }
    })
}

exports.getChapterByCourse = function(req, res, next){

    let query = `SELECT * FROM course_chapter WHERE course_id = ${req.params.course_id} ORDER BY chapter_number ASC`;

    db.query(query, (err, result, fields) => {
        if(err){
             res.json({error: true, result: err});
        }else{
             res.json({error: false, result: (result.length !== 0) ? result: [] });
        }
    })
}


exports.updateChapter = function(req, res, next){

    let data = {
        
        course_id: req.body.course_id,
        company_id: req.body.company_id,
        chapter_number: req.body.chapter_number,
        chapter_title: req.body.chapter_title,
        chapter_body: req.body.chapter_body
    }

    let query = `UPDATE course_chapter SET 
    course_id = '${data.course_id}',
    company_id = '${data.company_id}',
    chapter_number = '${data.chapter_number}',
    chapter_title = '${data.chapter_title}',
    chapter_body = '${data.chapter_body}'
    WHERE chapter_id = '${req.params.chapter_id}'`;

    db.query(query, (err, result) => {
        if(err){
             res.json({error: true, result: err});
        }else{
             
            db.query(`SELECT * FROM course_chapter WHERE chapter_id = '${req.params.chapter_id}'`, (err, result) => {
                if(err){
                     res.json({error: true, result: err});
                }else{
                     res.json({error: false, result: [0]});
                }
            });
        }
    });
};


exports.updateVideo = function(req, res, next){

    uploadVideo(req,  res, (err) => {
        if(!req.file){
             res.json({error: true, result: err});
        }else{

            let data = {
                chapter_video: `${env.APP_URL}/chapter/${req.file.filename}`
            }

            let query = `UPDATE course_chapter SET chapter_video = '${data.chapter_video}' WHERE chapter_id = '${req.params.chapter_id}'`;

            db.query(query, (err, result) => {
                if(err){
                     res.json({error: true, result: err});
                }else{
                     res.json({error: false, result: data.chapter_video});
                }
            });
        }
    });
};


exports.updateThumbnail = function(req, res, next){
    uploadImage(req, res, (err) => {
        if(!req.file){
             res.json({error: true, result: err});
        }else{
            
                let data = {
                    chapter_video: `${env.APP_URL}/chapter/${req.file.filename}`
                }

                let query = `UPDATE course_chapter SET thumbnail = '${data.chapter_video}' WHERE chapter_id = '${req.params.chapter_id}'`;

                db.query(query, (err, result) => {
                    if(err){
                         res.json({error: true, result: err});
                    }else{
                         res.json({error: false, result: data.chapter_video});
                    }
                });
        }
    });
};


exports.updateAttachment = function(req, res, next){
    uploadAttachment(req, res, (err) => {
        if(!req.files){
             res.json({error: true, 'msg': 'gagal'});
        }else{

            let attachment = '';
            for( let i=0; i<req.files.length; i++){
                attachment += env.APP_URL + '/chapter/' + req.files[i].filename + ',';
            }

            attachment = attachment.substring(0, attachment.length - 1);

            let data = {
                chapter_video: attachment
            };

            console.log('req : ' , attachment);

            let query = `UPDATE course_chapter SET attachment_id = '${data.chapter_video}' WHERE chapter_id = '${req.params.chapter_id}'`;
            db.query(query, (err, result) => {
                if(err){
                     res.json({error: true, result: err});
                }else{
                     res.json({error: false, result: data.chapter_video});
                }
            });
        }
    });
}


exports.deleteChapters = function(req, res, next) {

    let query = `DELETE FROM course_chapter WHERE chapter_id = '${req.params.chapter_id}'`;

    db.query(query, (err, result) => {
        if(err){
             res.json({error: true, result: err});
        }else{
             res.json({error: false, result: result});
        }
    })
}

