var env = require('../env.json');
var conf = require('../configs/config');
var db = require('../configs/database');
var moment = require('moment-timezone');
const path = require("path");
var moment2 = require('moment');
const asyncs = require("async");
const bbb = require('bigbluebutton-js');
const { checkAccess } = require('../middleware');
var multer = require('multer');

const storage = multer.diskStorage({
  filename: function (req, file, cb) {
    cb(null, file.originalname);
  },
});
let upload = multer({ storage: storage, limits: { fileSize: 100000000 } }).single('file');

const AWS = require('aws-sdk');
const fs = require('fs');
const s3 = new AWS.S3({
  accessKeyId: env.AWS_ACCESS,
  secretAccessKey: env.AWS_SECRET,
});

exports.createFolder = (req, res, next) => {
  let form = {
    name: req.body.name,
    company: req.body.company,
    mother: req.body.mother,
    project_admin: req.body.project_admin,
    is_limit: req.body.is_limit ? req.body.is_limit : 0,
    user: req.body.user,
    aSekretaris: req.body.aSekretaris ? req.body.aSekretaris : 0,
    aModerator: req.body.aModerator ? req.body.aModerator : 0,
    aPembicara: req.body.aPembicara ? req.body.aPembicara : 0,
    aOwner: req.body.aOwner ? req.body.aOwner : 0,
    aPeserta: req.body.aPeserta ? req.body.aPeserta : 0,
    created_by: req.body.created_by,
  };
  //check folder name
  db.query(
    `SELECT name FROM files_folder WHERE name = '${form.name}' AND company_id = '${form.company}' AND mother = '${form.mother}' AND publish=1`,
    (error, result, fields) => {
      if (error) {
        res.json({ error: true, result: error });
      } else {
        if (result.length >= 1) {
          res.json({ error: true, result: 'Folder name already exist' });
        } else {
          // create folder
          db.query(
            `INSERT INTO files_folder
                    (id, company_id, mother, name, is_limit, publish, sekretaris, moderator, pembicara, owner, peserta, created_at, created_by)
                    VALUES
                    (null, '${form.company}', '${form.mother}', '${form.name}','${form.is_limit}','1','${form.aSekretaris
            }','${form.aModerator}','${form.aPembicara}','${form.aOwner}','${form.aPeserta}','${moment(
              new Date()
            ).format('YYYY-MM-DD HH:mm:ss')}','${form.created_by}')`,
            (error, result, fields) => {
              if (error) {
                res.json({ error: true, result: error });
              } else {
                if (form.project_admin) {
                  // insert project admin
                  for (let i = 0; i < form.project_admin.length; i++) {
                    db.query(`INSERT INTO files_folder_user
                              (id, folder_id, user_id, role)
                              VALUES
                              (null, '${result.insertId}', '${form.project_admin[i]}', 'Project Admin')`);
                  }
                }
                if (form.user) {
                  // insert user
                  for (let i = 0; i < form.user.length; i++) {
                    db.query(`INSERT INTO files_folder_user
                              (id, folder_id, user_id, role)
                              VALUES
                              (null, '${result.insertId}', '${form.user[i]}', 'User')`);
                  }
                }
                res.json({ error: false, result: result.insertId });
              }
            }
          );
        }
      }
    }
  );
};

exports.getFolderByCompany = (req, res, next) => {
  let company = req.params.company;
  let mother = req.params.mother;
  let form = req.body;
  if (!form.length) {
    form = {
      aSekretaris: 1,
      aModerator: 1,
      aPembicara: 1,
      aOwner: 1,
      aPeserta: 1,
    };
  }
  db.query(
    `SELECT f.*, u.name AS creator FROM files_folder f
          LEFT JOIN user u ON u.user_id = f.created_by
          WHERE f.company_id = '${company}' AND f.mother = '${mother}' AND f.publish='1' AND
          (
            (f.sekretaris='${form.aSekretaris}' AND f.sekretaris=1)
            OR (f.moderator='${form.aModerator}' AND f.moderator=1)
            OR (f.pembicara='${form.aPembicara}' AND f.pembicara=1)
            OR (f.owner='${form.aOwner}' AND f.owner=1)
            OR (f.peserta='${form.aPeserta}' AND f.peserta=1)
          )`,
    (error, result, fields) => {
      if (error) {
        res.json({ error: true, result: error });
      } else {
        res.json({
          error: false,
          result: result,
        });
      }
    }
  );
};
exports.getFolderByUser = (req, res, next) => {
  db.query(`SELECT company_id FROM files_folder WHERE id='${req.params.mother}'`, (error, result, fields) => {
    if (error) {
      res.json({ error: true, result: error });
    } else {
      let company = result.length > 0 ? result[0].company_id : req.params.company;
      let mother = req.params.mother;
      let user = req.params.user;
      let form = req.body;
      if (!form.length) {
        form = {
          aSekretaris: 1,
          aModerator: 1,
          aPembicara: 1,
          aOwner: 1,
          aPeserta: 1,
        };
      }
      if (req.params.guest) {
        db.query(
          `SELECT f.*, u.name AS creator FROM files_folder f
        LEFT JOIN user u ON u.user_id = created_by
        LEFT JOIN files_folder_user fu ON fu.folder_id=f.id
        WHERE f.company_id = '${company}' AND f.mother = '${mother}' AND f.publish='1' AND
        ((f.sekretaris='${form.aSekretaris}' AND f.sekretaris=1)
        OR (f.moderator='${form.aModerator}' AND f.moderator=1)
        OR (f.pembicara='${form.aPembicara}' AND f.pembicara=1)
        OR (f.owner='${form.aOwner}' AND f.owner=1)
        OR (f.peserta='${form.aPeserta}' AND f.peserta=1))`,
          (error, result, fields) => {
            if (error) {
              res.json({ error: true, result: error });
            } else {
              res.json({
                error: false,
                result: result,
              });
            }
          }
        );
      } else {
        db.query(`SELECT level FROM user WHERE user_id='${user}'`, (error, result, fields) => {
          if (error) {
            res.json({ error: true, result: error });
          } else {
            if (result[0].level === 'client') {
              db.query(
                `SELECT f.*, u.name AS creator FROM files_folder f
                      LEFT JOIN user u ON u.user_id = created_by
                      LEFT JOIN files_folder_user fu ON fu.folder_id=f.id
                      WHERE f.company_id = '${company}' AND f.mother = '${mother}' AND f.publish='1' AND ((f.is_limit='1' AND fu.user_id='${req.params.user}' AND fu.role='User') OR (f.is_limit='0')) AND
                      ((f.sekretaris='${form.aSekretaris}' AND f.sekretaris=1)
                      OR (f.moderator='${form.aModerator}' AND f.moderator=1)
                      OR (f.pembicara='${form.aPembicara}' AND f.pembicara=1)
                      OR (f.owner='${form.aOwner}' AND f.owner=1)
                      OR (f.peserta='${form.aPeserta}' AND f.peserta=1))`,
                (error, result, fields) => {
                  if (error) {
                    res.json({ error: true, result: error });
                  } else {
                    res.json({
                      error: false,
                      result: result,
                    });
                  }
                }
              );
            } else {
              db.query(
                `SELECT *, u.name AS creator FROM files_folder
                      LEFT JOIN user u ON u.user_id = created_by
                      WHERE company_id = '${company}' AND mother = '${mother}' AND publish='1' AND
                      ((sekretaris='${form.aSekretaris}' AND sekretaris=1)
                      OR (moderator='${form.aModerator}' AND moderator=1)
                      OR (pembicara='${form.aPembicara}' AND pembicara=1)
                      OR (owner='${form.aOwner}' AND owner=1)
                      OR (peserta='${form.aPeserta}' AND peserta=1))`,
                (error, result, fields) => {
                  if (error) {
                    res.json({ error: true, result: error });
                  } else {
                    res.json({
                      error: false,
                      result: result,
                    });
                  }
                }
              );
            }
          }
        });
      }
    }
  });
};
exports.getFolderById = (req, res, next) => {
  let mother = req.params.mother;

  db.query(`SELECT * FROM files_folder WHERE mother = '${mother}' AND publish='1'`, (error, result, fields) => {
    if (error) {
      res.json({ error: true, result: error });
    } else {
      res.json({
        error: false,
        result: result,
      });
    }
  });
};

exports.getPrevFolder = (req, res, next) => {
  let company = req.params.company;
  let id = req.params.id;
  db.query(
    `SELECT mother FROM files_folder WHERE company_id = '${company}' AND id = '${id}'`,
    (error, result, fields) => {
      if (error) {
        res.json({ error: true, result: error });
      } else {
        let prev = 0;
        if (result.length == 0) {
          prev = 0;
        } else {
          prev = result[0].mother;
        }
        res.json({
          error: false,
          result: prev,
        });
      }
    }
  );
};
exports.getPrevFolderByProject = (req, res, next) => {
  let id = req.params.id;
  db.query(`SELECT mother FROM files_folder WHERE id = '${id}'`, (error, result, fields) => {
    if (error) {
      res.json({ error: true, result: error });
    } else {
      let prev = 0;
      if (result.length == 0) {
        prev = 0;
      } else {
        prev = result[0].mother;
      }
      res.json({
        error: false,
        result: prev,
      });
    }
  });
};

exports.getFilesByFolder = (req, res, next) => {
  let folderId = req.params.folder_id;

  db.query(
    `SELECT f.*, u.name AS creator FROM files_file f LEFT JOIN user u ON u.user_id = f.created_by INNER JOIN files_folder fol ON f.folder_id=fol.id WHERE folder_id = '${folderId}' ORDER BY name ASC`,
    (error, result, fields) => {
      if (error) {
        res.json({ error: true, result: error });
      } else {
        res.json({
          error: false,
          result: result,
        });
      }
    }
  );
};

var storageFile = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, './public/files');
  },
  filename: (req, file, cb) => {
    var filetype = '';
    if (file.mimetype === 'audio/mp4' || file.mimetype === 'application/mp4' || file.mimetype === 'video/mp4') {
      filetype = 'mp4';
    }
    if (file.mimetype === 'application/pdf') {
      filetype = 'pdf';
    }
    if (file.mimetype === 'image/gif') {
      filetype = 'gif';
    }
    if (file.mimetype === 'image/png') {
      filetype = 'png';
    }
    if (file.mimetype === 'image/jpeg') {
      filetype = 'jpg';
    }
    cb(null, 'file' + '-' + Date.now() + '.' + file.originalname);
  },
});
var uploadFile = multer({ storage: storageFile }).single('file');

// insert into files_logs
let files_logs = (param) => {
  try {
    let param_db = [
      param.user_id,
      param.id,
      param.filename,
      param.folder,
      param.url,
      moment.tz(new Date(), 'Asia/Jakarta').format('YYYY-MM-DD HH:mm:ss'),
    ];
    return db.query(
      `INSERT INTO files_logs 
        (user_id, files_id, filename, folder_id, url, date) 
      VALUES ((SELECT user_id FROM user WHERE email = ? limit 1), ?, ?, ?, ?, ?);`,
      param_db
    );
  } catch (err) {
    return err;
  }
};

exports.uploadFiles = (req, res, next) => {
  uploadFile(req, res, (err) => {
    if (!req.file) {
      res.json({ error: true, result: err });
    } else {
      var filenameType = req.file.filename.split('.');
      var formData = {
        created_at: conf.dateNow(),
        folder: req.body.folder,
        type: filenameType[filenameType.length - 1],
        name: req.file.originalname,
        created_by: req.body.created_by ? req.body.created_by : 0,
        location: `${env.APP_URL}/files/${req.file.filename}`,
      };

      db.query(
        `INSERT INTO files_file
				(id, folder_id, name, type, location, created_at, created_by)
				VALUES
				(null, '${formData.folder}', '${formData.name}', '${formData.type}', '${formData.location}', '${formData.created_at}','${formData.created_by}')`,
        (error, result, fields) => {
          if (error) {
            res.json({ error: true, result: error });
          } else {
            db.query(
              `SELECT * FROM files_file WHERE id = '${result.insertId}'`,
              async (error, result, fields) => {
                // success upload file and save recent docs
                let logs = {
                  user_id: req.app.token.email,
                  id: result[0].id,
                  filename: result[0].name,
                  folder: result[0].folder_id,
                  url: result[0].location,
                };

                await files_logs(logs);
                res.json({
                  error: false,
                  result: result[0],
                });
              }
            );
          }
        }
      );
    }
  });
};

exports.uploadFilesS3 = (req, res, next) => {
  upload(req, res, (err) => {
    if (!req.file) {
      res.json({ error: true, result: err });
    } else {
      let path = req.file.path;
      let newDate = new Date();
      let keyName = `${env.AWS_BUCKET_ENV}/${req.body.folder}`;

      var params = {
        ACL: 'public-read',
        Bucket: env.AWS_BUCKET,
        Body: fs.createReadStream(path),
        Key: `${keyName}/${req.file.originalname}`,
        ContentDisposition: 'inline',
        ContentType: req.file.mimetype,
      };

      s3.upload(params, (err, data) => {
        if (err) {
          res.json({ error: true, msg: 'Error Upload Image', result: err });
        }
        if (data) {
          fs.unlinkSync(path);

          var filenameType = req.file.filename.split('.');
          var formData = {
            created_at: conf.dateNow(),
            folder: req.body.folder,
            type: filenameType[filenameType.length - 1],
            name: req.file.originalname,
            created_by: req.body.created_by ? req.body.created_by : 0,
            location: data.Location,
          };

          db.query(
            `INSERT INTO files_file
            (id, folder_id, name, type, location, created_at, created_by)
            VALUES
            (null, '${formData.folder}', '${formData.name}', '${formData.type}', '${formData.location}', '${formData.created_at}','${formData.created_by}')`,
            (error, result, fields) => {
              if (error) {
                res.json({ error: true, result: error });
              } else {
                db.query(
                  `SELECT * FROM files_file WHERE id = '${result.insertId}'`,
                  async (error, result, fields) => {
                    // success upload file and save recent docs
                    let logs = {
                      user_id: req.app.token.email,
                      id: result[0].id,
                      filename: result[0].name,
                      folder: result[0].folder_id,
                      url: result[0].location,
                    };

                    await files_logs(logs);
                    res.json({
                      error: false,
                      result: result[0],
                    });
                  }
                );
              }
            }
          );
        }
      });
    }
  });
};

exports.getFolderMOM = (req, res, next) => {
  // let q = `
  //   SELECT mom.*, u.name AS creator
  //   FROM liveclass_mom mom
  //   LEFT JOIN user u ON u.user_id = mom.created_by
  //   LEFT JOIN liveclass_booking lb ON lb.id = mom.liveclass_id
  //   INNER JOIN liveclass l ON  l.class_id = lb.meeting_id
  //   WHERE l.folder_id='${req.params.folder_id}' ORDER BY time ASC;
  // `;
  let q_old = `
    SELECT mom.*, u.name AS creator
    FROM liveclass_mom mom  
    LEFT JOIN user u ON u.user_id = mom.created_by
    INNER JOIN liveclass l ON l.class_id = mom.liveclass_id
    WHERE l.folder_id='${req.params.folder_id}' ORDER BY time ASC
  `;
  let append = '';
  if (req.headers.currentposition) {
    append = `l.company_id = '${req.headers.currentposition}' AND`;
  }

  let q = `
    SELECT mom.*, u.name AS creator,l.company_id, l.folder_id, l.class_id
    FROM liveclass_mom mom
    left JOIN liveclass l ON ${append} l.folder_id='${req.params.folder_id}' and (l.class_id = mom.liveclass_id OR l.class_id IN(SELECT lb.meeting_id FROM liveclass_booking lb WHERE lb.id = mom.liveclass_id))
    LEFT JOIN user u ON u.user_id = mom.created_by
    WHERE 
    l.company_id IS NOT NULL AND 
    l.folder_id IS NOT NULL AND 
    l.class_id IS NOT NULL AND 
    (mom.title IS NOT NULL AND CHAR_LENGTH(mom.title) > 0)
    ORDER BY mom.time ASC;
  `;
  db.query(q, (error, result, fields) => {
    if (error) {
      res.json({ error: true, result: error });
    } else {
      res.json({ error: false, result: result });
    }
  });
};

exports.deleteFolder = async (req, res, next) => {
  let check = await db.query(`SELECT 
                              count(b.id) as id_count
                              from files_folder a
                              LEFT JOIN files_file b ON b.folder_id = a.id
                              WHERE a.id='${req.params.folder_id}' AND a.publish=1;`);
  if (check[0].id_count > 0 && req.query.checkFiles && req.query.checkFiles === 'true') {
    res.json({ error: true, result: 'The directory you are going to delete has several files' });
  } else {
    db.query(
      `UPDATE files_folder SET publish='0' WHERE id='${req.params.folder_id}'; UPDATE liveclass SET folder_id='0' WHERE folder_id='${req.params.folder_id}'`,
      (error, result, fields) => {
        if (error) {
          res.json({ error: true, result: error });
        } else {
          res.json({ error: false, result: 'success' });
        }
      }
    );
  }
};

exports.deleteFile = (req, res, next) => {
  db.query(`delete from files_file WHERE id='${req.params.file_id}'`, (error, result, fields) => {
    if (error) {
      res.json({ error: true, result: error });
    } else {
      res.json({ error: false, result: result });
    }
  });
};

exports.editFile = (req, res, next) => {
  db.query(
    `UPDATE files_file SET name='${req.body.name}' WHERE id='${req.params.file_id}'`,
    (error, result, fields) => {
      if (error) {
        res.json({ error: true, result: error });
      } else {
        res.json({ error: false, result: result });
      }
    }
  );
};

exports.getRecord = (req, res, next) => {
  db.query(`SELECT * FROM record WHERE project_id = '${req.params.project_id}'`, (error, result, fields) => {
    if (error) {
      res.json({ error: true, result: error });
    } else {
      res.json({ error: false, result: result });
    }
  });
};

exports.renameRecord = (req, res, next) => {
  db.query(
    `SELECT COUNT(id) AS count_record FROM record WHERE record_id='${req.params.record_id}'`,
    (error, result, fields) => {
      if (error) {
        res.json({ error: true, result: error });
      } else {
        if (result[0].count_record > 0) {
          db.query(
            `UPDATE record SET name='${req.body.name}' WHERE record_id='${req.params.record_id}'`,
            (error, result, fields) => {
              if (error) {
                res.json({ error: true, result: error });
              } else {
                res.json({ error: false, result: result });
              }
            }
          );
        } else {
          db.query(
            `INSERT INTO record (record_id, project_id, name) VALUES('${req.params.record_id}','${req.body.project_id}','${req.body.name}')`,
            (error, result, fields) => {
              if (error) {
                res.json({ error: true, result: error });
              } else {
                res.json({ error: false, result: result });
              }
            }
          );
        }
      }
    }
  );
};

exports.editFolder = (req, res, next) => {
  let aSekretaris = req.body.aSekretaris ? req.body.aSekretaris : 0;
  let aModerator = req.body.aModerator ? req.body.aModerator : 0;
  let aPembicara = req.body.aPembicara ? req.body.aPembicara : 0;
  let aOwner = req.body.aOwner ? req.body.aOwner : 0;
  let aPeserta = req.body.aPeserta ? req.body.aPeserta : 0;

  db.query(
    `UPDATE files_folder SET name='${req.body.name}',
            sekretaris='${aSekretaris}',
            moderator='${aModerator}',
            pembicara='${aPembicara}',
            owner='${aOwner}',
            peserta='${aPeserta}',
            is_limit='${req.body.is_limit}'
            WHERE id='${req.params.folder_id}'`,
    (error, result, fields) => {
      if (error) {
        res.json({ error: true, result: error });
      } else {
        // edit project admin
        db.query(
          `DELETE FROM files_folder_user WHERE folder_id='${req.params.folder_id}' AND role='Project Admin'`,
          (error, result, fields) => {
            if (req.body.project_admin) {
              for (let i = 0; i < req.body.project_admin.length; i++) {
                db.query(`INSERT INTO files_folder_user
                  (id, folder_id, user_id, role, access)
                  VALUES
                  (null, '${req.params.folder_id}', '${req.body.project_admin[i]}', 'Project Admin', 'editor')`);
              }
            }
          }
        );
        // edit user
        db.query(
          `DELETE FROM files_folder_user WHERE folder_id='${req.params.folder_id}' AND role='User'`,
          (error, result, fields) => {
            if (req.body.user) {
              for (let i = 0; i < req.body.user.length; i++) {
                let userId = typeof (req.body.user[i]) === 'object' ? req.body.user[i].user_id : req.body.user[i];
                let access = typeof (req.body.user[i]) === 'object' ? req.body.user[i].access : 'editor';
                db.query(`INSERT INTO files_folder_user
                  (folder_id, user_id, role, access)
                  VALUES
                  ('${req.params.folder_id}', '${userId}', 'User','${access}' )`);
              }
            }
          }
        );
        res.json({ error: false, result: result });
      }
    }
  );
};

exports.getFolderRecordedMeeting = (req, res, next) => {
  db.query(`SELECT record FROM liveclass WHERE folder_id='${req.params.folder_id}'`, (error, result, fields) => {
    if (error) {
      res.json({ error: true, result: error });
    } else {
      res.json({ error: false, result: result });
    }
  });
};

exports.getProjectById = (req, res, next) => {
  let query = `SELECT p.*, GROUP_CONCAT(DISTINCT pu.user_id) AS project_admin, (SELECT GROUP_CONCAT(DISTINCT user_id) AS user FROM files_folder_user WHERE role='User' AND folder_id='${req.params.folder_id}') AS user
    FROM files_folder p
    LEFT JOIN files_folder_user pu
    ON p.id=pu.folder_id
    WHERE p.id='${req.params.folder_id}' AND pu.role='Project Admin'`;
  db.query(query, (error, result, fields) => {
    if (error) {
      res.json({ error: true, result: error });
    } else {
      res.json({ error: false, result: result[0] });
    }
  });
};

exports.checkProjectAccess = (req, res, next) => {
  let query = `SELECT * FROM files_folder_user WHERE folder_id='${req.params.folder_id}' AND user_id='${req.params.user_id}' AND role='Project Admin'`;
  db.query(query, (error, result, fields) => {
    if (error) {
      res.json({ error: true, result: error });
    } else {
      res.json({ error: false, result: result.length > 0 ? 'Project Admin' : 'Member' });
    }
  });
};

exports.logs = async (req, res) => {
  // save recent docs
  let logs = {
    user_id: req.app.token.email,
    id: req.body.id,
    filename: req.body.filename,
    folder: req.body.folder,
    url: req.body.url,
  };

  let save = await files_logs(logs);
  if (save.affectedRows !== 0) {
    return res.json({ error: false, result: 'success' });
  } else {
    return res.json({ error: true, result: 'error' });
  }
};

exports.filesLog = (req, res) => {
  db.query(
    `SELECT a.filename, b.name, a.url, a.date, c.type
    FROM files_logs a
    INNER JOIN files_file c
    ON c.id=a.files_id
    INNER JOIN files_folder b
    ON a.folder_id = b.id
    WHERE a.user_id = (SELECT user_id FROM user WHERE email = ?) AND b.company_id=?
    ORDER BY a.date DESC
    LIMIT 10;`,
    [req.app.token.email, req.params.company_id],
    (error, result) => {
      if (error) {
        return res.json({ error: true, result: error.message });
      } else {
        return res.json({ error: false, result: result });
      }
    }
  );
};

exports.filesGetAllProject = async (req, res, next) => {
  try {

    let checks = await checkAccess({
      company_id: req.params.company_id,
      sub: 'general',
      role: req.app.token.level === 'superadmin' ? 'admin' : req.app.token.level,
      code: ['CD_FILE_FOLDER', 'R_FILES_FOLDER', 'U_FILES']
    }, req);

    const user_id = req.app.token.user_id;
    let queryUser = `SELECT
        p.id,
        p.NAME AS name,
        p.NAME AS parentName,
        p.update_at AS updated_at,
        p.created_at,
        usr.name AS created_by,
        (SELECT COUNT(*) FROM files_file ff WHERE ff.folder_id = p.id) AS totalFiles,
        (
        SELECT
          COUNT( l.class_id ) 
        FROM
          liveclass l
          LEFT JOIN ( SELECT * FROM liveclass_participant lp WHERE lp.user_id = '${user_id}' ) AS lp ON lp.class_id = l.class_id 
        WHERE
          l.folder_id = p.id 
        AND (( l.is_private = '1' AND lp.user_id = '${user_id}' ) OR l.is_private = '0' )) AS meeting,
        ( SELECT COUNT( w.id ) FROM webinars w WHERE w.project_id = p.id AND w.publish = 1 ) AS webinar,
      IF
        ( p.company_id != '${req.params.company_id}', c.company_name, NULL ) AS share_from,
        p.update_at AS recent_project,
      'folder' AS 'type',
      'icon-folder' AS icon,
      0 AS active,
      0 AS parent,
      pu2.access AS access
      FROM
        files_folder p
        LEFT JOIN (SELECT * FROM files_folder_user WHERE user_id='${user_id}') pu2 ON  p.id = pu2.folder_id
        LEFT JOIN company c ON c.company_id = p.company_id
        LEFT JOIN project_share s ON s.project_id = p.id 
        LEFT JOIN user usr ON usr.user_id = p.created_by
      WHERE
        p.publish = '1'
        AND p.mother = '0'
        AND (

          IF(p.is_limit = 0, 
              p.company_id = '${req.params.company_id}' OR p.created_by = '${user_id}',
              (pu2.role = 'Project Admin' OR pu2.role = 'User')
          ) 
          
        )
        
        /* COMMENT QUERY
         ( p.company_id = '${req.params.company_id}' OR s.user_id = '${user_id}' ) 
         AND p.publish = '1' 
         AND p.mother = '0' 
         AND (
          (
            p.is_limit = 1 
            AND ( pu.user_id = '${user_id}' AND pu.role = 'Project Admin' ) 
            OR ( pu.user_id = '${user_id}' AND pu.role = 'User' )) 
          OR p.is_limit = 0 
          OR s.user_id = '${user_id}' 
         )
        */
      GROUP BY
        p.id 
      ORDER BY
        p.update_at DESC`;

    let queryAdmin = `SELECT
      p.id,
      p.NAME AS name,
      count( DISTINCT l.class_id ) AS meeting,
      count( DISTINCT w.id ) AS webinar,
      p.NAME AS parentName,
      p.update_at AS updated_at,
      p.created_at,
      usr.name AS created_by,
      (SELECT COUNT(*) FROM files_file ff WHERE ff.folder_id = p.id) AS totalFiles,
      IF( p.company_id != '${req.params.company_id}', c.company_name, NULL ) AS share_from,
      p.update_at AS recent_project,
      'folder' AS 'type',
      'icon-folder' AS icon,
      0 AS active,
      0 AS parent,
      'editor' AS access
    FROM
      files_folder p
      LEFT JOIN liveclass l ON p.id = l.folder_id
      LEFT JOIN ( SELECT * FROM webinars w WHERE w.publish = 1 ) AS w ON w.project_id = p.id
      LEFT JOIN project_share s ON s.project_id = p.id
      LEFT JOIN company c ON c.company_id = p.company_id
      LEFT JOIN user usr ON usr.user_id = p.created_by
    WHERE
      ( p.company_id = '${req.params.company_id}' OR s.user_id = '${user_id}' ) 
      AND p.mother = '0' 
      AND p.publish = '1' 
    GROUP BY
      p.id 
    ORDER BY
      p.update_at DESC;`;
    let queryFolder = req.params.level === 'client' ? queryUser : queryAdmin;
    // console.log(queryFolder)
    const responseFolder = await new Promise((resolve, reject) => {
      resolve(db.query(queryFolder));
    });

    let temp_folder = [{
      "id": -2,
      "name": "Temporary Folder",
      "meeting": 0,
      "webinar": 0,
      "parentName": "Temporary Folder",
      "updated_at": null,
      "created_at": null,
      "created_by": null,
      "totalFiles": 0,
      "share_from": null,
      "recent_project": null,
      "type": "folder",
      "icon": "icon-folder",
      "active": 1,
      "parent": 0,
      "access": "editor"
    },
    {
      "id": -1,
      "name": "My Documents",
      "meeting": 0,
      "webinar": 0,
      "parentName": "My Documents",
      "updated_at": null,
      "created_at": null,
      "created_by": null,
      "totalFiles": 0,
      "share_from": null,
      "recent_project": null,
      "type": "folder",
      "icon": "icon-folder",
      "active": 1,
      "parent": 0,
      "access": "editor"
    }
    ];

    temp_folder = temp_folder.concat(responseFolder);
    const obj = {
      id: 0,
      idChild: null,
      parent: true,
      name: 'Project & Files',
      folder: temp_folder,
    };
    res.json({ error: false, result: obj, access: checks });
  } catch (error) {
    res.json({ error: true, result: 'Error Connection!' });
  }
};

exports.filesGetChildProject = async (req, res, next) => {

  const user_id = req.app.token.user_id;
  const folderId = req.params.mother;
  let tmp_folder = {};
  let tmp = [];

  try {

    if (req.params.mother == -1 || req.params.mother == -2) {
      let name_path = "My Documents";
      if (req.params.mother == -2) {
        name_path = "Temporary Folder";
      }

      tmp_folder = {
        "id": req.params.mother.toString(),
        "idChild": 0,
        "parent": false,
        "name": name_path,
        "folder": []
      };
    }

    // FILE S3
    let getResultFiles = await new Promise((resolve, reject) => {

      let query_files = `
          SELECT
            f.id,
            f.NAME AS name,
            f.location AS link,
            f.created_at AS created_at,
            u.NAME AS creator,
            'files' AS type,
            type AS ext,
            'icon-files' AS icon,
            u.name AS created_by,
            0 AS totalFiles,
            fol.name AS parentName,
            (IF(fol.is_limit = 0,'editor',pu.access)) AS access
          FROM
            files_file f
            LEFT JOIN user u ON u.user_id = f.created_by
            LEFT JOIN files_folder_user pu ON pu.folder_id = f.folder_id
            INNER JOIN files_folder fol ON f.folder_id = fol.id 
          WHERE
            ( f.folder_id = '${folderId}' and f.created_by = '${user_id}' or f.folder_id = '${folderId}') 
            and f.active = 1
          ORDER BY
          NAME ASC;`;

      if (req.params.mother == -1 || req.params.mother == -2) {
        let accessLocal = 'editor';
        let name_path = "My Documents";
        if (req.params.mother == -2) {
          name_path = "Temporary Folder";
          accessLocal = 'viewer';
        }

        query_files = `
            SELECT
            f.id,
            f.NAME AS name,
            f.location AS link,
            f.created_at AS created_at,
            u.NAME AS creator,
            'files' AS type,
            type AS ext,
            'icon-files' AS icon,
            u.name AS created_by,
            0 AS totalFiles,
            '${name_path}' AS parentName,
            '${accessLocal}' AS access
          FROM
            files_file f
            LEFT JOIN user u ON u.user_id = f.created_by
          WHERE
            f.folder_id = '${req.params.mother}' and f.created_by = '${user_id}'
            and f.active = 1
          ORDER BY
          NAME ASC;`;
      }
      resolve(db.query(query_files));
    })

    if (getResultFiles.length > 0) {
      getResultFiles.forEach((str) => {
        tmp.push({
          id: str.id,
          parent: false,
          name: str.name,
          content: null,
          type: str.type,
          ext: str.ext,
          icon: str.icon,
          link: str.link,
          creator: null,
          active: false,
          new: false,
          access: str.access,
          parentName: str.parentName,
          updated_at: str.created_at, //ini mksdnya karena fieldnya tidak ada dan value nya tidak berubah"
          created_at: str.created_at,
          created_by: str.created_by,
          totalFiles: str.totalFiles,
        });
      });
    }

    if (req.params.mother == -2 || req.params.mother > -1) {

      let getFolderMOM = await getFolderMOM_func(req);
      let filesGetRecordingBBB = await filesGetRecordingBBB_func(req);
      // console.info(filesGetRecordingBBB)
      if (getFolderMOM.length > 0) {
        getFolderMOM.forEach((str) => {
          tmp.push({
            id: str.id,
            class_id: str.class_id || null,
            from_type: str.from_type || null,
            subclass: str.subclass || null,
            parent: false,
            name: str.name,
            content: str.content,
            type: 'files',
            ext: 'tiny',
            icon: 'icon-file',
            link: null,
            creator: null,
            active: false,
            new: false,
            docType: 'mom',
            access: str.access || 'editor',
            parentName: str.parentName,
            updated_at: str.updated_at,
            created_at: str.created_at,
            created_by: str.created_by,
            totalFiles: str.totalFiles,
          });
        });
      }
      if (filesGetRecordingBBB.length > 0) {
        filesGetRecordingBBB.forEach((str) => {
          tmp.push(str);
        });
      }
    }

    if (tmp.length > 0) {
      let removeRedudant = [];
      tmp.forEach((str) => {
        let idx = removeRedudant.findIndex((str1) => { return str1.id === str.id });
        if (idx < 0) {
          removeRedudant.push(str);
        }
      });

      tmp = removeRedudant;
    }
    // console.log(tmp, 9090)
    asyncs.waterfall(
      [
        (cb) => {
          let queryUser = `
                  SELECT
                  p.id, 
                  p.is_limit, 
                  p.created_by as 'creator', 
                  p.NAME AS name,
                  p.mother AS parent,
                  -- (SELECT ff.name FROM files_folder ff WHERE ff.id = p.mother) AS parentName,
                  p.update_at AS updated_at,
                  p.created_at,
                  usr.name AS created_by,
                  (SELECT COUNT(*) FROM files_file ff WHERE ff.folder_id = p.id) AS totalFiles 
                FROM
                  files_folder p 
                LEFT JOIN user usr ON usr.user_id = p.created_by
                WHERE
                  p.id = '${req.params.mother}';

                SELECT
                  p.id,
                  p.NAME AS name,
                  -- (SELECT ff.name FROM files_folder ff WHERE ff.id = p.mother) AS parentName,
                  p.update_at AS updated_at,
                  p.created_at,
                  usr.name AS created_by,
                  pu.access AS access,
                  -- (SELECT COUNT(*) FROM files_file ff WHERE ff.folder_id = p.id) AS totalFiles,
                  (
                  SELECT
                    COUNT( l.class_id ) 
                  FROM
                    liveclass l
                    LEFT JOIN ( SELECT * FROM liveclass_participant lp WHERE lp.user_id = '${user_id}' ) AS lp ON lp.class_id = l.class_id 
                  WHERE
                    l.folder_id = p.id 
                  AND (( l.is_private = '1' AND lp.user_id = '${user_id}' ) OR l.is_private = '0' )) AS meeting,
                  ( SELECT COUNT( w.id ) FROM webinars w WHERE w.project_id = p.id AND w.publish = 1 ) AS webinar,
                IF
                  ( p.company_id != '${req.params.company_id}', c.company_name, NULL ) AS share_from,
                  p.update_at AS recent_project 
                FROM
                  files_folder p
                  LEFT JOIN files_folder_user pu ON pu.folder_id = p.id
                  -- LEFT JOIN files_folder_user pu2 ON  p.id = pu2.folder_id
                  LEFT JOIN company c ON c.company_id = p.company_id
                  LEFT JOIN project_share s ON s.project_id = p.id 
                  LEFT JOIN user usr ON usr.user_id = p.created_by
                WHERE
                  /*pu2.user_id = '${user_id}' or
                  p.publish = '1'
                  AND p.mother = '0'
                  AND (
          
                    IF(p.is_limit = 0, 
                        p.company_id = '${req.params.company_id}' and p.created_by = '${user_id}',
                        ( pu2.user_id = '${user_id}' AND (pu2.role = 'Project Admin' OR pu2.role = 'User') )
                    ) 
                    
                  )*/
                  ( p.company_id = '${req.params.company_id}' OR s.user_id = '${user_id}' ) 
                  AND p.publish = '1' 
                  AND p.mother = '${req.params.mother}' 
                  AND ((
                      p.is_limit = 1 
                      AND ( pu.user_id = '${user_id}' AND pu.role = 'Project Admin' ) 
                      OR ( pu.user_id = '${user_id}' AND pu.role = 'User' )) 
                    OR p.is_limit = 0 
                    OR s.user_id = '${user_id}' 
                  ) 
                GROUP BY
                  p.id 
                ORDER BY
                  p.update_at DESC`;

          let queryAdmin = `
                  SELECT
                  p.id,
                  p.is_limit,
                  p.created_by as 'creator',
                  p.NAME AS name,
                  p.mother AS parent,
                  -- (SELECT ff.name FROM files_folder ff WHERE ff.id = p.mother) AS parentName,
                  p.update_at AS updated_at,
                  p.created_at,
                  usr.name AS created_by,
                  (SELECT COUNT(*) FROM files_file ff WHERE ff.folder_id = p.id) AS totalFiles
                FROM
                  files_folder p 
                LEFT JOIN user usr ON usr.user_id = p.created_by
                WHERE
                  p.id = '${req.params.mother}';
                SELECT
                  p.id,
                  p.NAME AS name,
                  count( DISTINCT l.class_id ) AS meeting,
                  count( DISTINCT w.id ) AS webinar,
                  -- (SELECT ff.name FROM files_folder ff WHERE ff.id = p.mother) AS parentName,
                  p.update_at AS updated_at,
                  p.created_at,
                  'editor' AS access,
                  usr.name AS created_by,
                  -- (SELECT COUNT(*) FROM files_file ff WHERE ff.folder_id = p.id) AS totalFiles,
                IF
                  ( p.company_id != '${req.params.company_id}', c.company_name, NULL ) AS share_from,
                  p.update_at AS recent_project 
                FROM
                  files_folder p
                  LEFT JOIN liveclass l ON p.id = l.folder_id
                  LEFT JOIN ( SELECT * FROM webinars w WHERE w.publish = 1 ) AS w ON w.project_id = p.id
                  LEFT JOIN project_share s ON s.project_id = p.id
                  LEFT JOIN company c ON c.company_id = p.company_id 
                    LEFT JOIN user usr ON usr.user_id = p.created_by
                WHERE
                  ( p.company_id = '${req.params.company_id}' OR s.user_id = '${user_id}' ) 
                  AND p.mother = '${req.params.mother}' 
                  AND p.publish = '1' 
                GROUP BY
                  p.id 
                ORDER BY
                  p.update_at DESC`;

          let query_mydoc = `
              SELECT 
              ff.id, 
              ff.name, 
              ff.name AS parentName, 
              usr.name AS created_by, 
              ff.created_at, 
              ff.update_at AS 'updated_at', 
              'icon-folder' as icon, 
              'folder' AS type, 
              ${req.params.mother} AS parent, 
              null as active, 
              NULL AS new, 
              'editor' AS access,
              ( 
                select 
                (COUNT(ff2.id)+COUNT(ff3.id)) AS 'counting' 
                FROM files_folder ff2 
                LEFT JOIN files_file ff3 ON ff3.folder_id = ff2.mother 
                WHERE 
                ff2.mother = ff.id   
              ) AS totalFiles 
              FROM 
              files_folder ff 
              LEFT JOIN user usr ON usr.user_id = ff.created_by 
              WHERE 
              ff.mother = '${req.params.mother}' AND 
              usr.user_id = '${user_id}' 
              ORDER BY
                  ff.update_at DESC;
          `;

          let query = req.params.level === 'client' ? queryUser : queryAdmin;
          // console.log(query, 9090)
          if (req.params.mother == -1 || req.params.mother == -2) {
            query = query_mydoc;
          }

          db.query(query, (error, result, fields) => {
            // console.log(error, result, 'ini database')
            if (error) {
              cb({ error: true, result: error });

            } else {

              // MY DOCUMENT OR TEMPORARY
              if (req.params.mother == -1 || req.params.mother == -2) {

                let obj = tmp_folder;

                if (tmp.length > 0) {
                  tmp.forEach((str) => {
                    if (!str.parentName) {
                      str.parentName = obj.name;
                    }
                    if (!str.created_by) {
                      str.created_by = str.created_by;
                    }
                    if (str.totalFiles > 0) {
                      str.totalFiles = str.totalFiles;
                    }

                  });
                }

                // ONLY My Documents
                if (req.params.mother == -1) {
                  if (result.length > 0) {
                    result.forEach((str) => {
                      obj.folder.push({
                        id: str.id,
                        name: str.name,
                        parent: false,
                        active: false,
                        new: false,
                        parentName: tmp_folder.name, //str.parentName,
                        updated_at: str.updated_at,
                        created_at: str.created_at,
                        created_by: str.created_by,
                        totalFiles: str.totalFiles, //str.totalFiles,
                        type: 'folder',
                        icon: 'icon-folder',
                        access: str.access,
                      });
                    });
                  }
                }

                if (tmp.length > 0) {
                  let sortedData = tmp.sort((a, b) => {
                    if (a.name.toUpperCase() < b.name.toUpperCase()) { return -1; }
                    if (a.name.toUpperCase() > b.name.toUpperCase()) { return 1; }
                    return 0;
                  });

                  tmp = sortedData;
                }

                obj.folder = obj.folder.concat(tmp);

                return cb(false, obj);

              } else {

                if (result[0].length > 0) {

                  let obj = {
                    id: req.params.mother,
                    idChild: (result[0][0].is_limit == 1 && result[0][0].creator != user_id) ? 0 : result[0][0].parent,
                    parent: false,
                    name: result[0][0].name,
                    folder: [],
                  };
                  if (req.params.mother == -1 || req.params.mother == -2) {
                    obj = tmp_folder;
                  }

                  if (tmp.length > 0) {
                    tmp.forEach((str) => {
                      if (!str.parentName) {
                        str.parentName = result[0][0].name;
                      }
                      if (!str.created_by) {
                        str.created_by = result[0][0].created_by;
                      }
                      if (result[0][0].totalFiles > 0) {
                        str.totalFiles = result[0][0].totalFiles;
                      }

                    });
                  }

                  if (result[1].length > 0) {
                    result[1].forEach((str) => {
                      obj.folder.push({
                        id: str.id,
                        name: str.name,
                        parent: false,
                        active: false,
                        new: false,
                        parentName: result[0][0].name, //str.parentName,
                        updated_at: str.updated_at,
                        created_at: str.created_at,
                        created_by: str.created_by,
                        totalFiles: result[0][0].totalFiles, //str.totalFiles,
                        type: 'folder',
                        icon: 'icon-folder',
                        access: str.access
                      });
                    });
                  }
                  // console.log(tmp.length);
                  if (tmp.length > 0) {
                    let sortedData = tmp.sort((a, b) => {
                      if (a.name.toUpperCase() < b.name.toUpperCase()) { return -1; }
                      if (a.name.toUpperCase() > b.name.toUpperCase()) { return 1; }
                      return 0;
                    });

                    tmp = sortedData;
                  }
                  obj.folder = obj.folder.concat(tmp);

                  return cb(false, obj);
                } else {
                  return cb({ error: 1, result: [] }, false);
                }
              }
            }
          });
        },
      ],
      (err, val) => {
        if (err) {
          res.json(err);
        } else {
          res.json({ error: false, result: val });
        }
      }
    );

  } catch (error) {
    console.info(error, 'ERROR');
    return res.json({ error: 2, result: error });
  }

};

exports.filesCreateFolder = async (req, res, next) => {
  try {
    const user_id = req.app.token.user_id;

    if (typeof req.body.company_id !== 'number' && typeof req.body.parent !== 'number') {
      return res.json({ error: true, result: 'Incorrectly validation', message: 'Incorrectly Validation!' });
    } else {
      if (req.body.parent == -2) {
        return res.json({ error: true, result: 'Cannot Create Folder in Temporary Folder.', message: 'Cannot Create Folder in Temporary Folder.' });
      }

      let queryInsert = `insert into files_folder(company_id, mother, name, created_by) values(${req.body.company_id},${req.body.parent},'${req.body.name}',${user_id})`;
      if (req.body.parent == 0) {
        queryInsert = `insert into files_folder(company_id, mother, name, created_by) values(${req.body.company_id},${req.body.parent},'${req.body.name}',${user_id})`;
      }
      let insert = await db.query(queryInsert);
      res.json({ error: false, result: { id: insert.insertId, parent: req.body.parent } });
    }
  } catch (e) {
    return res.json({ error: true, result: 'Incorrectly validation', message: 'Incorrectly Validation!' });
  }
};

exports.filesRenameFolder = async (req, res, next) => {
  try {
    const user_id = req.app.token.user_id;

    if (typeof req.body.company_id !== 'number' && typeof req.body.parent !== 'number') {
      return res.json({ error: true, result: 'Incorrectly validation' });
    } else {
      let insert = await db.query(
        `UPDATE files_folder set name = '${req.body.name}' where company_id='${req.body.company_id}' and mother ='${req.body.parent}' and id ='${req.body.id}'`
      );
      res.json({ error: false, message: 'Success Rename Folder' });
    }
  } catch (e) {
    return res.json({ error: true, result: 'Incorrectly validation' });
  }
};

exports.deleteProjectFile = (req, res, next) => {
  db.query(`update files_file set active=0 WHERE id='${req.body.file_id}'`, (error, result, fields) => {
    if (error) {
      res.json({ error: true, result: error });
    } else {
      res.json({ error: false, result: result });
    }
  });
};

exports.filesGetAllFolder = async (req, res, next) => {
  try {
    const user_id = req.app.token.user_id;
    let queryUser = `SELECT
        p.id,
        p.NAME AS name,
        p.update_at AS updated_at,
        p.created_at,
        usr.name AS created_by
      FROM
        files_folder p
        LEFT JOIN files_folder_user pu ON pu.folder_id = p.id
        LEFT JOIN company c ON c.company_id = p.company_id
        LEFT JOIN project_share s ON s.project_id = p.id 
        LEFT JOIN user usr ON usr.user_id = p.created_by
      WHERE
        ( p.company_id = '${req.params.company_id}' OR s.user_id = '${user_id}' ) 
        AND p.publish = '1' 
        AND ((
            p.is_limit = 1 
            AND ( pu.user_id = '${user_id}' AND pu.role = 'Project Admin' ) 
            OR ( pu.user_id = '${user_id}' AND pu.role = 'User' )) 
          OR p.is_limit = 0 
          OR s.user_id = '${user_id}' 
        ) 
      GROUP BY
        p.id 
      ORDER BY
        p.update_at DESC`;

    let queryAdmin = `SELECT
      p.id,
      p.NAME AS name,
      p.update_at AS updated_at,
      p.created_at,
      usr.name AS created_by
    FROM
      files_folder p
      LEFT JOIN liveclass l ON p.id = l.folder_id
      LEFT JOIN ( SELECT * FROM webinars w WHERE w.publish = 1 ) AS w ON w.project_id = p.id
      LEFT JOIN project_share s ON s.project_id = p.id
      LEFT JOIN company c ON c.company_id = p.company_id
      LEFT JOIN user usr ON usr.user_id = p.created_by
    WHERE
      ( p.company_id = '${req.params.company_id}' OR s.user_id = '${user_id}' ) 
      AND p.publish = '1' 
    GROUP BY
      p.id 
    ORDER BY
      p.update_at DESC;`;

    let queryFolder = req.params.level === 'client' ? queryUser : queryAdmin;

    const responseFolder = await new Promise((resolve, reject) => {
      resolve(db.query(queryFolder));
    });
    const obj = responseFolder;
    res.json({ error: false, result: obj });
  } catch (error) {

    res.json({ error: true, result: 'Error Connection!' });
  }
};

exports.filesMoveTo = async (req, res, next) => {
  try {
    const user_id = req.app.token.user_id;

    if (req.body.from_type === 'meeting') {
      let query = await db.query(`UPDATE liveclass SET folder_id = '${req.body.move_to_folder}' where class_id = '${req.body.class_id}' and folder_id = 0`);
      res.json({ error: false, result: 'Success Move File!' });
    }
    else if (req.body.from_type === 'webinar') {
      let query = await db.query(`UPDATE webinars SET project_id = '${req.body.move_to_folder}' where id = '${req.body.subclass}' and project_id = 0`);
      res.json({ error: false, result: 'Success Move File!' });
    } else {

      let query = await db.query(`SELECT id from files_file where id = '${req.body.id_file}'`);
      if (query.length > 0) {

        let update = await db.query(`UPDATE files_file set folder_id='${req.body.move_to_folder}' where id = '${query[0].id}'`);
        res.json({ error: false, result: 'Success Move File!' });
      } else {
        res.json({ error: true, result: 'File not already exist' });
      }
    }

  } catch (e) {
    console.info(e, "move file");
    res.json({ error: true, result: 'File not already exist' });
  }
};

exports.folderMoveTo = async (req, res, next) => {
  try {
    const user_id = req.app.token.user_id;

    const retrieveFolder = await new Promise((resolve, reject) => {
      resolve(db.query(`SELECT id from files_folder where id = '${req.body.idFolder}'`))
    });

    const isFolderAvailable = retrieveFolder.length > 0;
    if (isFolderAvailable) {
      await new Promise((resolve, reject) => {
        resolve(db.query(`UPDATE files_folder set mother='${req.body.moveToFolder}' where id = '${req.body.idFolder}'`))
      });
    }

    res.json({ error: false, result: 'Success Move Folder and File!' });

  } catch (e) {
    console.info(e, "move file");
    res.json({ error: true, result: 'Folder is not exist!' });
  }
};

exports.duplicateFile = async (req, res, next) => {
  try {
    const requestBody = {
      idFile: req.body.id,
      idParent: req.body.idParent,
      company_id: req.body.company_id
    };
    const user_id = req.app.token.user_id;

    const retrieveDataFile = await new Promise((resolve, reject) => {
      resolve(db.query(`SELECT ff.name, ff.type, ff.location, ff.active FROM files_file ff WHERE id = ${requestBody.idFile}`))
    });

    const insertFile = await new Promise((resolve, reject) => {
      resolve(db.query(`insert into files_file (folder_id, name, type, location, active, created_by, created_at) VALUES('${requestBody.idParent}','Duplicate_${retrieveDataFile[0].name}','${retrieveDataFile[0].type}','${retrieveDataFile[0].location}', '${retrieveDataFile[0].active}', '${user_id}', '${moment(new Date()).format('YYYY-MM-DD HH:mm:ss')}')`))
    });

    res.json({ error: false, result: 'Success Duplicate File!' });

  } catch (error) {
    res.json({ error: true, result: error });
  }
};

exports.duplicateFolder = async (req, res, next) => {
  try {
    const requestBody = {
      idFile: req.body.id,
      name: req.body.name,
      idParent: req.body.idParent,
      company_id: req.body.company_id
    }

    const user_id = req.app.token.user_id;

    const dataFiles = await new Promise((resolve, reject) => {
      resolve(db.query(`SELECT ff.name, ff.type, ff.location, ff.active FROM files_file ff WHERE folder_id = ${requestBody.idFile}`))
    });
    const dataFolderChild = await new Promise((resolve, reject) => {
      resolve(db.query(`SELECT ff.name FROM files_folder ff WHERE mother = ${requestBody.idFile} AND company_id = ${requestBody.company_id}`))
    })

    if (typeof requestBody.company_id !== 'number' && typeof requestBody.parent !== 'number') {
      return res.json({ error: true, result: 'Incorrectly validation' });
    } else {

      await new Promise((resolve, reject) => {
        resolve(db.query(
          `insert into files_folder(company_id, mother, name, created_by) values(${requestBody.company_id},${requestBody.idParent},'${requestBody.name}',${user_id})`
        ));
      });


      const getLastIDInsert = await new Promise((resolve, reject) => {
        resolve(db.query(
          `SELECT MAX(id) AS last_id FROM files_folder;`
        ));
      });

      if (dataFolderChild.length > 0) {
        // Jika Duplicate Terdapat Folder di Child nya

        let retrieveBulkFolder = [];
        for (let i = 0; i < dataFolderChild.length; i++) {
          retrieveBulkFolder.push([requestBody.company_id, getLastIDInsert[0].last_id, dataFolderChild[i].name, user_id])
        }

        await new Promise((resolve, reject) => {
          db.query('insert into files_folder (company_id, mother, name, created_by) VALUES ?', [retrieveBulkFolder],
            (err, res) => {
              if (err) reject(err);
              resolve(res)
            })
        })
      }

      if (dataFiles.length > 0) {
        //Files
        let retrieveBulkFiles = [];
        for (let i = 0; i < dataFiles.length; i++) {
          retrieveBulkFiles.push([getLastIDInsert[0].last_id, dataFiles[i].name, dataFiles[i].type, dataFiles[i].location, dataFiles[i].active, user_id, moment(new Date()).format('YYYY-MM-DD HH:mm:ss')])
        }

        await new Promise((resolve, reject) => {
          db.query('insert into files_file (folder_id, name, type, location, active, created_by, created_at) VALUES ?', [retrieveBulkFiles],
            (err, res) => {
              if (err) reject(err);
              resolve(res)
            })
        })
      }
    }
    res.json({ error: false, result: 'Success Duplicate Folder!' });

  } catch (error) {
    res.json({ error: true, result: 'Error Connection!' });
  }
};

function filesGetRecordingBBB_func(req) {
  return new Promise(async (resolve) => {
    const user_id = req.app.token.user_id;

    let mother = req.params.mother == -2 ? 0 : req.params.mother;
    let level = '';
    let dataRecord = [];
    let tmp_dataRecord = [];

    let appendQueryMeet = '';
    let appendQueryWebinar = '';
    if (mother == 0) {
      /** 
       * Sort by liveclass_booking alias b
      */
      appendQueryMeet = ` and b.date_start >= curdate() - INTERVAL 7 DAY`;
      appendQueryWebinar = ` DATE(w.start_time) >= curdate() - INTERVAL 7 DAY and `;
    }

    let getUser = `SELECT u.level FROM user u
    WHERE u.user_id = '${user_id}' AND u.status='active'`;

    db.query(getUser, async (error, result, fields) => {
      if (error) {
        return resolve(null);
      }
      else {
        if (result.length > 0) {
          level = result[0].level;
          let queryUser = `SELECT
            l.*,
            u.name,
            -- GROUP_CONCAT(b.id) AS booking,
            /* CHECKING ROLES */
            IF(pu.role IS NULL,
              (SELECT GROUP_CONCAT(b.id) FROM liveclass_booking b WHERE b.meeting_id = l.class_id AND b.date_start <= NOW() AND ( b.moderator = '${user_id}' OR b.user_id = '${user_id}' ) ${appendQueryMeet})
              ,
              (SELECT GROUP_CONCAT(b.id) FROM liveclass_booking b WHERE b.meeting_id = l.class_id AND b.date_start <= now())
            ) as booking,
            GROUP_CONCAT(p.user_id) AS participant,
            COUNT(lp.user_id) AS total_participant,
            LEFT(l.schedule_start, 10) AS tanggal,
            MID(l.schedule_start, 12, 8) AS waktu_start,
            MID(l.schedule_end,12, 8) AS waktu_end,
            pu.access AS access
            FROM
            liveclass l
            LEFT JOIN user u ON (l.moderator IS NOT NULL AND l.moderator = u.user_id)
            LEFT JOIN liveclass_participant p ON l.class_id = p.class_id
            LEFT JOIN liveclass_participant lp ON lp.class_id = l.class_id
            -- LEFT JOIN liveclass_booking b ON b.meeting_id = l.class_id AND b.date_start <= now()
            LEFT JOIN files_folder_user pu ON pu.folder_id = l.folder_id and pu.user_id = '${user_id}'  
            WHERE l.folder_id='${mother}' AND ((l.is_private='1' AND p.user_id='${user_id}') OR l.is_private='0')
            GROUP BY l.class_id
            ORDER BY l.schedule_start DESC`;

          let queryAdmin = `SELECT
            l.*,
            u.name,
            GROUP_CONCAT(p.user_id) AS participant,
            -- GROUP_CONCAT(b.id) AS booking,
            (
							SELECT GROUP_CONCAT(b.id) FROM liveclass_booking b where b.meeting_id = l.class_id AND b.date_start <= now() ${appendQueryMeet}
						) as booking,
            COUNT(p.user_id) AS total_participant,
            LEFT(l.schedule_start, 10) AS tanggal,
            MID(l.schedule_start, 12, 8) AS waktu_start,
            MID(l.schedule_end,12, 8) AS waktu_end,
            'editor' AS access
            FROM
            liveclass l
            LEFT JOIN user u ON (l.moderator IS NOT NULL AND l.moderator = u.user_id)
            LEFT JOIN liveclass_participant p ON l.class_id = p.class_id
            -- LEFT JOIN liveclass_booking b ON b.meeting_id = l.class_id AND b.date_start <= now()
            WHERE l.folder_id='${mother}'
            GROUP BY l.class_id
            ORDER BY l.schedule_start DESC`;


          let queryWebinar = `SELECT w.id, w.company_id, w.judul, w.gambar,w.start_time,w.end_time,w.created_at, pu.access AS access 
          FROM webinars w
          LEFT JOIN files_folder_user pu ON pu.folder_id = w.project_id
          WHERE ${appendQueryWebinar} w.project_id = '${mother}' AND w.publish='1'
          ORDER BY w.id DESC`;

          let querys = queryAdmin
          if (mother == 0) {
            querys = queryUser;

            queryWebinar = `SELECT w.id, w.company_id, w.judul, w.gambar,w.start_time,w.end_time,w.created_at, pu.access AS access 
              FROM webinars w
              LEFT JOIN files_folder_user pu ON pu.folder_id = w.project_id
              WHERE ${appendQueryWebinar} w.project_id = '${mother}' AND w.publish='1' and 
              (
                (CONCAT( ",", w.sekretaris, "," ) LIKE '%,${user_id},%' ) 
                OR ( CONCAT( ",", w.moderator, "," ) LIKE '%,${user_id},%' ) 
                OR ( CONCAT( ",", w.pembicara, "," ) LIKE '%,${user_id},%' ) 
                OR ( CONCAT( ",", w.OWNER, "," ) LIKE '%,${user_id},%' )
                )   
              ORDER BY w.id DESC`;
          } else {
            if (level === 'client') {
              querys = queryUser
            }
          }

          let dataMeeting = await db.query(querys);
          let dataWebinar = await db.query(queryWebinar);
          let checking_liveclass = { meeting: [], webinar: [] };
          // console.log(dataMeeting, 'simpen ga nih?')

          let api = bbb.api(env.BBB_URL, env.BBB_KEY);
          let http = bbb.http

          if (dataMeeting.length > 0) {
            dataMeeting.map((item) => {
              //tmp_dataRecord.push(item.class_id.toString())
              if (item.booking) {
                let split = item.booking.split(",");
                checking_liveclass.meeting.push({ id: item.class_id, subclass: item.booking, type: 'meeting' });
                tmp_dataRecord = tmp_dataRecord.concat(split);
              }
            })
          }
          if (dataWebinar.length > 0) {
            dataWebinar.map((item) => {
              if (item.id != null) {
                checking_liveclass.webinar.push({ subclass: item.id.toString(), type: 'webinar' });
                tmp_dataRecord.push(item.id.toString());
              }
            })
          }


          if (tmp_dataRecord.length > 0) {
            tmp_dataRecord = [...new Set(tmp_dataRecord)];
            await Promise.all(env.BBB_SERVER_LIST.map(async (x) => {
              let api = bbb.api(x.server, x.key);
              let http = bbb.http
              let getRecordingsUrl = api.recording.getRecordings({ meetingID: tmp_dataRecord.toString(), state: 'published,processing,processed' })
              // console.log(getRecordingsUrl, 'recordings url')
              await http(getRecordingsUrl).then((result) => {
                if (result.returncode === 'SUCCESS' && result.messageKey !== 'noRecordings') {
                  dataRecord.push(result.recordings)
                }
              })
            }))
          }
          let dataRecordings = [];
          let sortedData = [];
          let recordName = await db.query(`SELECT * FROM record WHERE project_id = '${mother}'`)

          let cek_redudant = [];
          if (dataRecord.length > 0) {
            for (let i = 0; i < dataRecord.length; i++) {
              if (dataRecord[i].recording.length > 0) {
                for (let j = 0; j < dataRecord[i].recording.length; j++) {

                  // console.log(dataRecord[i].recording[j])
                  let idx = cek_redudant.findIndex((str) => { return str.meetingID === dataRecord[i].recording[j].meetingID });
                  if (idx < 0) {
                    cek_redudant.push(dataRecord[i].recording[j]);
                  }
                }
              } else {
                let idx = cek_redudant.findIndex((str) => { return str.meetingID === dataRecord[i].meetingID });
                if (idx < 0) {
                  cek_redudant.push(dataRecord[i]);
                }
              }
            }
          }

          dataRecord = cek_redudant;
          if (recordName.length > 0 || dataRecord.length > 0) {

            dataRecord.map((item) => {
              let class_id = null;
              let from_type = null;
              let subclass = null;

              if (checking_liveclass.meeting.length > 0) {

                for (let i = 0; i < checking_liveclass.meeting.length; i++) {
                  // console.info(checking_liveclass.meeting[i]);
                  if (checking_liveclass.meeting[i].subclass.search(item.meetingID) > -1) {
                    class_id = checking_liveclass.meeting[i].id;
                    from_type = 'meeting';
                    subclass = item.meetingID;
                  }
                }
              }

              if (checking_liveclass.webinar.length > 0) {

                for (let i = 0; i < checking_liveclass.webinar.length; i++) {
                  if (checking_liveclass.webinar[i].subclass.search(item.meetingID) > -1) {
                    //class_id = checking_liveclass.meeting[i].id;
                    from_type = 'webinar';
                    subclass = item.meetingID;
                  }
                }
              }

              if (item.recording) item = item.recording;
              try {

                let filePath = path.join(__basedir, '../my-presentation/');
                let files = `media-${item.recordID}.mp4`;
                let dir = `${filePath}${files}`;

                let is_download = false;
                if (fs.existsSync(dir)) {
                  is_download = `${env.APP_URL}/v1/files-download-recording/${files}`;
                }

                let checkArr = recordName.filter(rec => rec.record_id === item.recordID);
                if(checkArr.length){
                  item.name = checkArr[0].name
                }
                dataRecordings.push({
                  id: item.recordID,
                  is_download: is_download,
                  class_id: class_id,
                  from_type: from_type,
                  subclass: subclass,
                  name: `${item.name ? item.name : 'None'} - ${moment(item.startTime).format('MM-DD-YYYY')}`,
                  parent: false,
                  content: null,
                  type: 'files',
                  ext: 'recording',
                  icon: 'icon-files',
                  creator: null,
                  active: false,
                  new: false,
                  size: 0,
                  state: item.state,
                  link: Array.isArray(item.playback.format) ? item.playback.format[0].url : item.playback.format.url,
                  totalFiles: 0,
                  parentName: null,
                  created_by: null,
                  access: item.access || 'editor',
                  created_at: new Date(item.startTime),
                  updated_at: new Date(item.startTime),
                })
              } catch (e) {
                console.log(item.playback, e, 8888)
              }
            })
            sortedData = dataRecordings.sort((a, b) => b.date - a.date)
          }
          return resolve(sortedData)
          //res.json({ error: false, result: sortedData })
        }
        else {
          return resolve(null)

          res.json({ error: true, result: 'User not found' })
        }
      }
    })

  })
}

async function getFolderMOM_func(req) {
  let mother = req.params.mother == -2 ? 0 : req.params.mother;

  const user_id = req.app.token.user_id;
  let append = '';
  let appendByUser = '';

  if (req.headers.currentposition) {
    append = `l.company_id = '${req.headers.currentposition}' AND`;
  }
  if (mother == 0) {
    appendByUser = ` mom.created_by='${user_id}' and `;
  }

  let appendQueryMeet = '';
  let appendQueryMOM = ' l.class_id = mom.liveclass_id OR ';
  if (mother == 0) {
    /** 
     * Sort by liveclass_booking alias lb
    */
    appendQueryMeet = ` lb.date_start >= curdate() - INTERVAL 7 DAY and `;
    appendQueryMOM = ` (l.class_id = mom.liveclass_id and DATE(mom.time) >= curdate() - INTERVAL 7 DAY) OR `;
  }

  let q = `
    SELECT
	mom.id,
  mom.title AS name,
	mom.content,
  u.NAME AS creator,
	l.company_id,
	l.folder_id,
	l.class_id,
	ff.name AS parentName,
	0 as totalFiles,
	mom.time AS created_at,
	mom.time AS updated_at,
	u.name AS created_by,
  pu.access AS access,
  mom.liveclass_id as subclass,
  'meeting' as from_type,
  l.class_id as 'class_id'
FROM
	liveclass_mom mom
	LEFT JOIN liveclass l ON ${append} l.folder_id = '${mother}' 
	AND ( ${appendQueryMOM} l.class_id IN ( SELECT lb.meeting_id FROM liveclass_booking lb WHERE ${appendQueryMeet} lb.id = mom.liveclass_id ))
	LEFT JOIN files_folder ff ON l.folder_id = ff.id
	LEFT JOIN user u ON u.user_id = mom.created_by 
  LEFT JOIN files_folder_user pu ON pu.folder_id = l.folder_id
WHERE
  ${appendByUser}
	l.company_id IS NOT NULL 
	AND l.folder_id IS NOT NULL 
	AND l.class_id IS NOT NULL 
	AND ( mom.title IS NOT NULL AND CHAR_LENGTH( mom.title ) > 0 ) 
ORDER BY
	mom.time ASC;
  `;

  let getResult = db.query(q);
  // console.info(getResult, q)
  return getResult;
}

function bytesToSize(bytes) {
  var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
  if (bytes == 0) return '0 Byte';
  var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
  return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
}

exports.getAccessFolder = (req, res, next) => {
  let id = req.params.id;
  db.query(
    `SELECT * FROM files_folder WHERE id=?`, [id],
    async (error, result, fields) => {
      if (error) {
        res.json({ error: true, result: error });
      } else {
        let data = result[0];
        data.users = await db.query(`SELECT * FROM files_folder_user WHERE folder_id=? AND role='user'`, [id]);
        res.json({
          error: false,
          result: data
        });
      }
    }
  );
};

exports.buildRecording = async (req, res, next) => {
  const { combineFile } = require("../middleware");
  const path = require("path")

  if (req.params.recordId) {
    let result = await combineFile(req.params.recordId);
    if (result.error) {
      return res.json(result);
    } else {
      // let filePath = path.join(__basedir, '../my-presentation/');
      // let dir = `${filePath}${result.result}`;
      let url = `${env.APP_URL}/v1/files-download-recording/${result.result}`;
      res.json({ error: false, result: url });
    }
  } else {
    return res.json({ error: true, result: 'file not found' });
  }
}

exports.downloadRecording = async (req, res, next) => {
  const path = require("path")

  if (req.params.recordId) {
    let filePath = path.join(__basedir, '../my-presentation/output/');
    let dir = `${filePath}${req.params.recordId}`;

    if (!fs.existsSync(dir)) {
      return res.json({ error: true, result: 'file not found' });
    } else {
      res.download(dir);
    }
  } else {
    return res.json({ error: true, result: 'file not found' });
  }
}