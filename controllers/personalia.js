var env = require('../env.json');
var conf = require('../configs/config');
var db = require('../configs/database');
var md5 = require('md5');
var moment = require('moment-timezone');

var multer = require('multer');
const { randomString } = require('./helper/generate');
const { sendEmailPassword } = require('./helper/sendEmail');
var storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, './public/company');
  },
  filename: (req, file, cb) => {
    cb(null, 'excel-' + Date.now() + '-' + file.originalname);
  }
});
var UploadExcel = multer({ storage: storage }).single('excel');

function NOW() {

  var date = new Date();
  var aaaa = date.getFullYear();
  var gg = date.getDate();
  var mm = (date.getMonth() + 1);

  if (gg < 10)
    gg = "0" + gg;

  if (mm < 10)
    mm = "0" + mm;

  var cur_day = aaaa + "-" + mm + "-" + gg;

  var hours = date.getHours()
  var minutes = date.getMinutes()
  var seconds = date.getSeconds();

  if (hours < 10)
    hours = "0" + hours;

  if (minutes < 10)
    minutes = "0" + minutes;

  if (seconds < 10)
    seconds = "0" + seconds;

  return cur_day + " " + hours + ":" + minutes + ":" + seconds;
}

function formatDate(date) {
  var d = new Date(date),
    month = '' + (d.getMonth() + 1),
    day = '' + d.getDate(),
    year = d.getFullYear();

  if (month.length < 2)
    month = '0' + month;
  if (day.length < 2)
    day = '0' + day;

  return [year, month, day].join('-');
}

exports.importMurid = (req, res) => {
  UploadExcel(req, res, async (err) => {
    if (!req.file) {
      res.json({ error: true, result: err });
    } else {

      let form = {
        companyId: req.body.companyId
      }

      let getGrupId = await db.query(`SELECT grup_id FROM grup WHERE company_id = ? AND grup_name LIKE '%Murid%'`, [form.companyId]);

      // create class Excel
      var Excel = require('exceljs');
      var fs = require('fs');
      var wb = new Excel.Workbook();
      var path = require('path');
      var filePath = path.resolve(__dirname, '../public/company/' + req.file.filename);

      wb.xlsx.readFile(filePath).then(async function () {
        var sh = wb.getWorksheet("Sheet1");

        var tempArray = [];
        for (i = 2; i <= sh.rowCount; i++) {

          console.log('==========================================================');

          // 1   |  2  | 3    | 4     | 5            |      6       |     7
          // No | NIK | nama | email | tempat lahir | tanggal lahir | jenis kelamin

          // sementara voucher di isi email
          let sqlCreateUser = `INSERT INTO user (company_id, branch_id, grup_id,
                  identity,
                  name,
                  email,
                  voucher,
                  password,
                  level, status, registered, unlimited, validity) VALUES (?)`;
          let generatePassword = randomString(8);
          let valueCreateUser = [[form.companyId, 0, getGrupId[0].grup_id,
          sh.getRow(i).getCell(2).value,
          sh.getRow(i).getCell(3).value,
          sh.getRow(i).getCell(4).value,
          sh.getRow(i).getCell(4).value,
          md5(generatePassword),
            'client', 'active', NOW(), 1, formatDate(Date.now())]];
          let userId = await db.query(sqlCreateUser, valueCreateUser);
          sendEmailPassword({
            name: sh.getRow(i).getCell(3).value,
            password: generatePassword,
            email: sh.getRow(i).getCell(4).value,
            phone: '-'
          })

          let sqlMurid = `INSERT INTO learning_murid (user_id, company_id,
                  nama, email,
                  no_induk, tempat_lahir,
                  tanggal_lahir, jenis_kelamin) VALUES (?)`;
          let dataMurid = [[userId.insertId, form.companyId,
          sh.getRow(i).getCell(3).value, sh.getRow(i).getCell(4).value,
          sh.getRow(i).getCell(2).value, sh.getRow(i).getCell(5).value,
          moment(sh.getRow(i).getCell(6).value).format('YYYY-MM-DD'), sh.getRow(i).getCell(7).value]];
          let muridId = await db.query(sqlMurid, dataMurid);

          tempArray.push(muridId.insertId)
          console.log('==========================================================');

        }

        form.murid = tempArray;

        fs.unlinkSync(filePath);

        res.json({ error: false, result: form });

      })
    }
  });
}

exports.createMurid = async (req, res) => {
  let generatePassword = randomString(8)
  let form = {
    companyId: req.body.companyId,
    nama: req.body.nama,
    noInduk: req.body.noInduk,
    tempatLahir: req.body.tempatLahir,
    tanggalLahir: req.body.tanggalLahir,
    jenisKelamin: req.body.jenisKelamin,
    email: req.body.email,
    password: generatePassword,
    userId: req.body.userId
  };

  if (req.query.exists) {
    let sql = `INSERT INTO learning_murid (user_id, company_id, nama, email, no_induk, tempat_lahir, tanggal_lahir, jenis_kelamin) VALUES (?, ?, ?, ?, ?, ?, ?, ?)`;
    db.query(sql, [form.userId, form.companyId, form.nama, form.email, form.noInduk, form.tempatLahir, form.tanggalLahir, form.jenisKelamin], (error, rows) => {
      if (error) res.json({ error: true, result: error })

      let sql = `SELECT * FROM learning_murid WHERE id = ?`;
      db.query(sql, [rows.insertId], (error, rows) => {
        res.json({ error: false, result: rows[0] })
      })
    })

  }
  else {

    let getGrupId = await db.query(`SELECT grup_id FROM grup WHERE company_id = ? AND grup_name LIKE '%Murid%'`, [form.companyId]);

    // sementara voucher di isi email
    let sqlCreateUser = `INSERT INTO user (company_id, branch_id, grup_id, identity, name, email, voucher, password, level, status, registered, unlimited, validity) VALUES (?)`;
    let valueCreateUser = [[form.companyId, 0, getGrupId[0].grup_id, form.noInduk, form.nama, form.email, form.email, md5(form.password), 'client', 'active', NOW(), 1, formatDate(Date.now())]];

    db.query(sqlCreateUser, valueCreateUser, (error, rows) => {
      if (error) res.json({ error: true, result: error })

      let sql = `INSERT INTO learning_murid (user_id, company_id, nama, email, no_induk, tempat_lahir, tanggal_lahir, jenis_kelamin) VALUES (?, ?, ?, ?, ?, ?, ?, ?)`;
      db.query(sql, [rows.insertId, form.companyId, form.nama, form.email, form.noInduk, form.tempatLahir, form.tanggalLahir, form.jenisKelamin], (error, row) => {
        if (error) res.json({ error: true, result: error })

        // send email
        let payload = {
            name: form.nama,
            password: generatePassword,
            email: form.email,
            phone: '-'
        };
        sendEmailPassword(payload);
        
        let sql = `SELECT * FROM learning_murid WHERE id = ?`;
        db.query(sql, [row.insertId], (error, result) => {
          res.json({ error: false, result: result[0] })
        })
      })

    })

  }
}

exports.getMuridById = (req, res) => {
  let sql = `SELECT * FROM learning_murid WHERE id = ?`;
  db.query(sql, [req.params.id], (error, rows) => {
    res.json({ error: false, result: rows.length ? rows[0] : [] })
  })
}

exports.getMuridByNoInduk = (req, res) => {
  let sql = `SELECT * FROM learning_murid WHERE no_induk = ?`;
  db.query(sql, [req.params.id], (error, rows) => {
    res.json({ error: false, result: rows.length ? rows[0] : [] })
  })
}

exports.getMuridByUserId = (req, res) => {
  let sql = `SELECT * FROM learning_murid WHERE user_id = ?`;
  db.query(sql, [req.params.id], (error, rows) => {
    res.json({ error: false, result: rows.length ? rows[0] : [] })
  })
}

exports.getMuridByCompanyId = (req, res) => {
  let sql = `SELECT * FROM learning_murid WHERE company_id = ?`;
  db.query(sql, [req.params.companyId], (error, rows) => {
    res.json({ error: false, result: rows })
  })
}

exports.getUserByCompanyId = (req, res) => {
  let sql = `SELECT u.name as nama, g.grup_name, lp.* FROM kelola.user u 
  JOIN kelola.grup g ON g.grup_id = u.grup_id 
  LEFT JOIN kelola.learning_personil lp ON lp.user_id = u.user_id
  WHERE u.company_id = ?`

  db.query(sql, [req.params.companyId], (error, rows) => {
    res.json({ error: false, result: rows })
  })

}


exports.getKelasByJadwalId = (req, res) => {
  let sql = `SELECT lj.jadwal_id, lj.kelas_id, lkm.*, s.semester_name AS semester
    FROM learning_jadwal lj JOIN learning_kelas lkm ON lkm.kelas_id = lj.kelas_id JOIN semester s ON s.semester_id = lkm.semester_id
    WHERE lj.jadwal_id = ?`;
  db.query(sql, [req.params.jadwalId], async (err, rows) => {
    if (err) res.json({ error: true, result: err })

    res.json({ error: false, result: rows.length ? rows[0] : [] })
  })
}

exports.getMuridByJadwalId = (req, res) => {
  let sql = `SELECT lj.jadwal_id, lj.kelas_id, lm.*
    FROM learning_jadwal lj JOIN learning_kelas_murid lkm ON lkm.kelas_id = lj.kelas_id JOIN learning_murid lm ON lm.id = lkm.murid_id
    WHERE lj.jadwal_id = ?`;
  db.query(sql, [req.params.jadwalId], async (err, rows) => {
    if (err) res.json({ error: true, result: err })

    res.json({ error: false, result: rows })
  })
}

exports.getMuridAbsenByJadwalId = (req, res) => {
  let sql = `SELECT lj.jadwal_id, lj.kelas_id, lm.*, ljk.absen_jam
    FROM learning_jadwal lj
    	JOIN learning_kelas_murid lkm ON lkm.kelas_id = lj.kelas_id
        JOIN learning_murid lm ON lm.id = lkm.murid_id
        LEFT JOIN learning_jadwal_kehadiran ljk
          ON ljk.user_id = lm.user_id
            AND ljk.event = ?
            AND ljk.sesi_id = ?
    WHERE lj.jadwal_id = ?`;
  db.query(sql, [req.params.event, req.params.sesiId, req.params.jadwalId], async (err, rows) => {
    if (err) res.json({ error: true, result: err })

    res.json({ error: false, result: rows })
  })
}

exports.postMuridAbsenByJadwalId = async (req, res) => {
  // cek sudah absen
  let data = [req.body.jadwalId, req.body.event, req.body.sesiId, req.body.userId];

  let sqlAbsen = `SELECT * FROM learning_jadwal_kehadiran WHERE jadwal_id = ? AND event = ? AND sesi_id = ? AND user_id = ?`;
  let cekRow = await db.query(sqlAbsen, data);
  if (cekRow.length === 1) {
    res.json({ error: false, result: cekRow[0] })
  } else {
    let sql = `INSERT INTO learning_jadwal_kehadiran (jadwal_id, event, sesi_id, user_id) VALUES (?)`;
    db.query(sql, [data], (err, rows) => {
      if (err) res.json({ error: true, result: err })
      res.json({ error: false, result: rows })
    })
  }
}

exports.getMuridByKelasId = (req, res) => {
  let sql = `SELECT
      lkm.*, lm.nama, lm.user_id, lm.nama, lm.no_induk, lm.tempat_lahir, lm.tanggal_lahir, lm.jenis_kelamin
    FROM learning_kelas_murid lkm JOIN learning_murid lm ON lm.id = lkm.murid_id
    WHERE lkm.kelas_id = ?`;
  db.query(sql, [req.params.kelasId], (err, rows) => {
    if (err) res.json({ error: true, result: err })

    res.json({ error: false, result: rows })
  })
}

exports.insertMuridToKelas = (req, res) => {
  let data = [];
  for (var i = 0; i < req.body.muridId.length; i++) {
    data.push([req.body.kelasId, req.body.muridId[i]]);
  }
  let sql = `INSERT INTO learning_kelas_murid (kelas_id, murid_id) VALUES  ?`;
  db.query(sql, [data], (err, rows) => {
    if (err) res.json({ error: true, result: err })

    res.json({ error: false, result: rows })
  })
}

exports.deleteMuridToKelas = (req, res) => {
  let sql = `DELETE FROM learning_kelas_murid WHERE kelas_id = '${req.body.kelasId}' AND  murid_id = '${req.body.muridId}'`;
  db.query(sql, (err, rows) => {
    if (err) res.json({ error: true, result: err })

    res.json({ error: false, result: rows })
  })
}

exports.updateMuridById = (req, res) => {
  let form = {
    nama: req.body.nama,
    noInduk: req.body.noInduk,
    tempatLahir: req.body.tempatLahir,
    tanggalLahir: req.body.tanggalLahir,
    jenisKelamin: req.body.jenisKelamin,
    alamat: req.body.alamat,
    telepon: req.body.telepon,
    email: req.body.email
  };

  let sql = `UPDATE learning_murid SET nama = ?, no_induk = ?, tempat_lahir = ?, tanggal_lahir = ?,
    jenis_kelamin = ?, alamat = ?, telepon = ?, email = ? WHERE id = ?`;
  let val = [form.nama, form.noInduk, form.tempatLahir, form.tanggalLahir, form.jenisKelamin, form.alamat, form.telepon, form.email, req.params.id];
  db.query(sql, val, (error, rows) => {
    if (error) res.json({ error: true, result: error })

    let sql = `SELECT * FROM learning_murid WHERE id = ?`;
    db.query(sql, [req.params.id], async (error, rows) => {

      let sql = `UPDATE user SET name = ?, identity = ?, address = ?, phone = ?, email = ? WHERE user_id = ?`;
      let val = [form.nama, form.noInduk, form.alamat, form.telepon, form.email, rows[0].user_id];
      db.query(sql, val);

      res.json({ error: false, result: rows.length ? rows[0] : [] })
    })
  })
}

exports.deleteMuridById = (req, res) => {
  let id = req.params.id;
  db.query(`SELECT user_id FROM learning_murid WHERE id = ?`, [id], (error, rows) => {

    db.query(`DELETE FROM user WHERE user_id = ?`, [rows[0].user_id])

    db.query(`DELETE FROM learning_murid WHERE id = ?`, [id], (error, rows) => {
      if (error) res.json({ error: true, result: error })

      res.json({ error: false, result: rows })
    })

  })
}

exports.deleteMuridByCompanyId = (req, res) => {
  let sql = `DELETE FROM learning_murid WHERE company_id = ?`;
  db.query(sql, [req.params.id], (error, rows) => {
    if (error) res.json({ error: true, result: error })

    res.json({ error: false, result: rows })
  })
}

// ========================================================================== //

exports.createParents = async (req, res) => {
  let generatePassword = randomString(8)
  let form = {
    muridId: req.body.muridId,
    companyId: req.body.companyId,
    nama: req.body.nama,
    noInduk: req.body.noInduk,
    tempatLahir: req.body.tempatLahir,
    tanggalLahir: req.body.tanggalLahir,
    jenisKelamin: req.body.jenisKelamin,
    email: req.body.email,
    password: generatePassword,
    alamat: req.body.alamat,
    telepon: req.body.telepon,
    userId: req.body.userId,
  };

  if (req.query.exists) {
    let sql = `INSERT INTO learning_murid_parents (alamat, telepon, user_id, murid_id, company_id, nama, email, no_induk, tempat_lahir, tanggal_lahir, jenis_kelamin) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`;
    db.query(sql, [form.alamat, form.telepon, form.userId, form.muridId, form.companyId, form.nama, form.email, form.noInduk, form.tempatLahir, form.tanggalLahir, form.jenisKelamin], (error, row) => {
      if (error) res.json({ error: true, result: error })

      let sql = `SELECT * FROM learning_murid_parents WHERE id = ?`;
      db.query(sql, [row.insertId], (error, result) => {
        res.json({ error: false, result: result[0] })
      })
    })
  }
  else {
    let getGrupId = await db.query(`SELECT grup_id FROM grup WHERE company_id = ? AND grup_name LIKE '%Parents%'`, [form.companyId]);

    // sementara voucher di isi email
    let sqlCreateUser = `INSERT INTO user (company_id, branch_id, grup_id, identity, name, email, voucher, password, level, status, registered, unlimited, validity) VALUES (?)`;
    let valueCreateUser = [[form.companyId, 0, getGrupId[0].grup_id, form.noInduk, form.nama, form.email, form.email, md5(form.password), 'client', 'active', NOW(), 1, formatDate(Date.now())]];

    db.query(sqlCreateUser, valueCreateUser, (error, rows) => {
      if (error) res.json({ error: true, result: error })

      let payload = {
        name: form.nama,
        email: form.email,
        password: form.password,
        phone: '-'
      }
      sendEmailPassword(payload)

      let sql = `INSERT INTO learning_murid_parents (alamat, telepon, user_id, murid_id, company_id, nama, email, no_induk, tempat_lahir, tanggal_lahir, jenis_kelamin) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`;
      db.query(sql, [form.alamat, form.telepon, rows.insertId, form.muridId, form.companyId, form.nama, form.email, form.noInduk, form.tempatLahir, form.tanggalLahir, form.jenisKelamin], (error, row) => {
        if (error) res.json({ error: true, result: error })

        let sql = `SELECT * FROM learning_murid_parents WHERE id = ?`;
        db.query(sql, [row.insertId], (error, result) => {
          res.json({ error: false, result: result[0] })
        })
      })

    })
  }
}

exports.getParentsById = (req, res) => {
  let sql = `SELECT * FROM learning_murid_parents WHERE id = ?`;
  db.query(sql, [req.params.id], (error, rows) => {
    res.json({ error: false, result: rows.length ? rows[0] : [] })
  })
}

exports.getParentsByNoInduk = (req, res) => {
  let sql = `SELECT * FROM learning_murid_parents WHERE no_induk = ?`;
  db.query(sql, [req.params.id], (error, rows) => {
    res.json({ error: false, result: rows.length ? rows[0] : [] })
  })
}

exports.getParentsByCompanyId = (req, res) => {
  let sql = `SELECT * FROM learning_murid_parents WHERE company_id = ?`;
  db.query(sql, [req.params.companyId], (error, rows) => {
    res.json({ error: false, result: rows })
  })
}

exports.getParentsByMuridId = (req, res) => {
  let sql = `SELECT * FROM learning_murid_parents WHERE murid_id = ?`;
  db.query(sql, [req.params.muridId], (error, rows) => {
    res.json({ error: false, result: rows })
  })
}

exports.updateParentsById = (req, res) => {
  let form = {
    nama: req.body.nama,
    noInduk: req.body.noInduk,
    tempatLahir: req.body.tempatLahir,
    tanggalLahir: req.body.tanggalLahir,
    jenisKelamin: req.body.jenisKelamin,
    alamat: req.body.alamat,
    telepon: req.body.telepon,
    email: req.body.email
  };

  let sql = `UPDATE learning_murid_parents SET nama = ?, no_induk = ?, tempat_lahir = ?, tanggal_lahir = ?,
    jenis_kelamin = ?, alamat = ?, telepon = ?, email = ? WHERE id = ?`;
  let val = [form.nama, form.noInduk, form.tempatLahir, form.tanggalLahir, form.jenisKelamin, form.alamat, form.telepon, form.email, req.params.id];
  db.query(sql, val, (error, rows) => {
    if (error) res.json({ error: true, result: error })

    let sql = `SELECT * FROM learning_murid_parents WHERE id = ?`;
    db.query(sql, [req.params.id], (error, rows) => {
      let sql = `UPDATE user SET name = ?, identity = ?, address = ?, phone = ?, email = ? WHERE user_id = ?`;
      let val = [form.nama, form.noInduk, form.alamat, form.telepon, form.email, rows[0].user_id];
      db.query(sql, val);

      res.json({ error: false, result: rows.length ? rows[0] : [] })
    })
  })
}

exports.deleteParentsById = (req, res) => {
  let id = req.params.id;
  db.query(`SELECT user_id FROM learning_murid_parents WHERE id = ?`, [id], (error, rows) => {

    db.query(`DELETE FROM user WHERE user_id = ?`, [rows[0].user_id])

    db.query(`DELETE FROM learning_murid_parents WHERE id = ?`, [id], (error, rows) => {
      if (error) res.json({ error: true, result: error })

      res.json({ error: false, result: rows })
    })

  })
}

exports.deleteParentsByCompanyId = (req, res) => {
  let sql = `DELETE FROM learning_murid_parents WHERE company_id = ?`;
  db.query(sql, [req.params.id], (error, rows) => {
    if (error) res.json({ error: true, result: error })

    res.json({ error: false, result: rows })
  })
}

exports.getMyMuridByParentId = (req, res) => {
  let parentId = req.params.userId;
  let sql = `SELECT lm.nama AS nama_murid, lm.user_id AS user_id_murid, lm.no_induk AS nik_murid, lk.kurikulum, lkur.name AS kurikulum_name, lkm.kelas_id, lk.kelas_nama, s.semester_name, lk.semester_id, lk.tahun_ajaran, lmp.*
    FROM learning_murid_parents lmp
    	LEFT JOIN learning_murid lm ON lm.id = lmp.murid_id
      LEFT JOIN learning_kelas_murid lkm ON lkm.murid_id = lmp.murid_id
      JOIN learning_kelas lk ON lk.kelas_id = lkm.kelas_id AND lk.tahun_ajaran = ?
      LEFT JOIN learning_kurikulum lkur ON lkur.id = lk.kurikulum
      LEFT JOIN semester s ON s.semester_id = lk.semester_id
    WHERE lmp.user_id = ?`;

  let tahunAjaran = '';
  if (req.query.tahunAjaran) {
    tahunAjaran = req.query.tahunAjaran;
  }
  else {
    let d = new Date();
    // bulan diawali dengan 0 = januari, 11 = desember
    let month = d.getMonth();
    tahunAjaran = month < 6 ? (d.getFullYear() - 1) + '/' + d.getFullYear() : d.getFullYear() + '/' + (d.getFullYear() + 1);
  }

  db.query(sql, [tahunAjaran, parentId], (err, rows) => {
    if (err) res.json({ error: true, result: err })

    res.json({ error: false, result: rows.length ? rows[0] : [], semester: rows })
  })
}

exports.cekUserParents = (req, res) => {
  let { company_id } = req.params
  let sql = `SELECT u.user_id, u.name, g.grup_name, lmp.id, lmp.murid_id
    FROM learning_murid_parents lmp
      RIGHT JOIN user u ON lmp.user_id = u.user_id
    	JOIN grup g ON g.grup_id = u.grup_id AND g.grup_name = 'Parents'
    WHERE u.company_id = ?`
  let data = [company_id];
  db.query(sql, data, (err, rows) => {
    if (err) res.json({ error: true, result: err })

    res.json({ error: false, result: rows })
  })
}

// ========================================================================== //

exports.importGuru = (req, res) => {
  UploadExcel(req, res, async (err) => {
    if (!req.file) {
      res.json({ error: true, result: err });
    } else {

      let form = {
        companyId: req.body.companyId
      }

      let getGrupId = await db.query(`SELECT grup_id FROM grup WHERE company_id = ? AND grup_name LIKE '%Guru%'`, [form.companyId]);

      // create class Excel
      var Excel = require('exceljs');
      var fs = require('fs');
      var wb = new Excel.Workbook();
      var path = require('path');
      var filePath = path.resolve(__dirname, '../public/company/' + req.file.filename);

      wb.xlsx.readFile(filePath).then(async function () {
        var sh = wb.getWorksheet("Sheet1");

        var tempArray = [];
        for (i = 2; i <= sh.rowCount; i++) {

          console.log('==========================================================');

          // 1   |  2  | 3    | 4     | 5            |      6       |     7
          // No | NIK | nama | email | tempat lahir | tanggal lahir | jenis kelamin

          // sementara voucher di isi email
          let sqlCreateUser = `INSERT INTO user (company_id, branch_id, grup_id,
                  identity,
                  name,
                  email,
                  voucher,
                  password,
                  level, status, registered, unlimited, validity) VALUES (?)`;
          let generatePassword = randomString(8)
          let valueCreateUser = [[form.companyId, 0, getGrupId[0].grup_id,
          sh.getRow(i).getCell(2).value,
          sh.getRow(i).getCell(3).value,
          sh.getRow(i).getCell(4).value,
          sh.getRow(i).getCell(4).value,
          md5(generatePassword),
            'client', 'active', NOW(), 1, formatDate(Date.now())]];
          let userId = await db.query(sqlCreateUser, valueCreateUser);
          
          let payload = {
            name: sh.getRow(i).getCell(2).value,
            email: sh.getRow(i).getCell(4).value,
            password: generatePassword,
            phone: '-'
          }
          sendEmailPassword(payload)

          let sqlMurid = `INSERT INTO learning_guru (user_id, company_id,
                  nama, email,
                  no_induk, tempat_lahir,
                  tanggal_lahir, jenis_kelamin) VALUES (?)`;
          let dataMurid = [[userId.insertId, form.companyId,
          sh.getRow(i).getCell(3).value, sh.getRow(i).getCell(4).value,
          sh.getRow(i).getCell(2).value, sh.getRow(i).getCell(5).value,
          moment(sh.getRow(i).getCell(6).value).format('YYYY-MM-DD'), sh.getRow(i).getCell(7).value]];
          let muridId = await db.query(sqlMurid, dataMurid);

          tempArray.push(muridId.insertId)
          console.log('==========================================================');

        }

        form.guru = tempArray;

        fs.unlinkSync(filePath);

        res.json({ error: false, result: form });

      })
    }
  });
}

exports.createGuru = async (req, res) => {
  let form = {
    companyId: req.body.companyId,
    nama: req.body.nama,
    noInduk: req.body.noInduk,
    tempatLahir: req.body.tempatLahir,
    tanggalLahir: req.body.tanggalLahir,
    jenisKelamin: req.body.jenisKelamin,
    email: req.body.email,
    password: req.body.password,
    userId: req.body.userId
  };

  if (req.query.exists) {
    let sql = `INSERT INTO learning_guru (user_id, company_id, nama, email, no_induk, tempat_lahir, tanggal_lahir, jenis_kelamin) VALUES (?, ?, ?, ?, ?, ?, ?, ?)`;
    db.query(sql, [form.userId, form.companyId, form.nama, form.email, form.noInduk, form.tempatLahir, form.tanggalLahir, form.jenisKelamin], (error, rows) => {
      if (error) res.json({ error: true, result: error })

      let sql = `SELECT * FROM learning_guru WHERE id = ?`;
      db.query(sql, [rows.insertId], (error, rows) => {
        res.json({ error: false, result: rows[0] })
      })
    })
  }
  else {
    let getGrupId = await db.query(`SELECT grup_id FROM grup WHERE company_id = ? AND grup_name LIKE '%Guru%'`, [form.companyId]);

    // sementara voucher di isi email
    let sqlCreateUser = `INSERT INTO user (company_id, branch_id, grup_id, identity, name, email, voucher, password, level, status, registered, unlimited, validity) VALUES (?)`;
    let valueCreateUser = [[form.companyId, 0, getGrupId[0].grup_id, form.noInduk, form.nama, form.email, form.email, md5(form.password), 'client', 'active', NOW(), 1, formatDate(Date.now())]];

    db.query(sqlCreateUser, valueCreateUser, (error, rows) => {
      if (error) res.json({ error: true, result: error })

      let sql = `INSERT INTO learning_guru (user_id, company_id, nama, email, no_induk, tempat_lahir, tanggal_lahir, jenis_kelamin) VALUES (?, ?, ?, ?, ?, ?, ?, ?)`;
      db.query(sql, [rows.insertId, form.companyId, form.nama, form.email, form.noInduk, form.tempatLahir, form.tanggalLahir, form.jenisKelamin], (error, rows) => {
        if (error) res.json({ error: true, result: error })

        let sql = `SELECT * FROM learning_guru WHERE id = ?`;
        db.query(sql, [rows.insertId], (error, rows) => {
          res.json({ error: false, result: rows[0] })
        })
      })

    })
  }
}


exports.getGuruById = (req, res) => {
  let sql = `SELECT * FROM learning_guru WHERE id = ?`;
  db.query(sql, [req.params.id], (error, rows) => {
    res.json({ error: false, result: rows.length ? rows[0] : [] })
  })
}

exports.getGuruByNoInduk = (req, res) => {
  let sql = `SELECT * FROM learning_guru WHERE no_induk = ?`;
  db.query(sql, [req.params.id], (error, rows) => {
    res.json({ error: false, result: rows.length ? rows[0] : [] })
  })
}

exports.getGuruByCompanyId = (req, res) => {
  let sql = `SELECT * FROM learning_guru WHERE company_id = ?`;
  db.query(sql, [req.params.companyId], (error, rows) => {
    res.json({ error: false, result: rows })
  })
}

exports.updateGuruById = (req, res) => {
  let form = {
    nama: req.body.nama,
    noInduk: req.body.noInduk,
    tempatLahir: req.body.tempatLahir,
    tanggalLahir: req.body.tanggalLahir,
    jenisKelamin: req.body.jenisKelamin,
    alamat: req.body.alamat,
    telepon: req.body.telepon,
    email: req.body.email
  };

  let sql = `UPDATE learning_guru SET nama = ?, no_induk = ?, tempat_lahir = ?, tanggal_lahir = ?,
    jenis_kelamin = ?, alamat = ?, telepon = ?, email = ? WHERE id = ?`;
  let val = [form.nama, form.noInduk, form.tempatLahir, form.tanggalLahir, form.jenisKelamin, form.alamat, form.telepon, form.email, req.params.id];
  db.query(sql, val, (error, rows) => {
    if (error) res.json({ error: true, result: error })

    let sql = `SELECT * FROM learning_guru WHERE id = ?`;
    db.query(sql, [req.params.id], (error, rows) => {

      let sql = `UPDATE user SET name = ?, identity = ?, address = ?, phone = ?, email = ? WHERE user_id = ?`;
      let val = [form.nama, form.noInduk, form.alamat, form.telepon, form.email, rows[0].user_id];
      db.query(sql, val);

      res.json({ error: false, result: rows.length ? rows[0] : [] })
    })
  })
}

exports.deleteGuruById = (req, res) => {
  let id = req.params.id;
  db.query(`SELECT user_id FROM learning_guru WHERE id = ?`, [id], (error, rows) => {

    db.query(`DELETE FROM user WHERE id = ?`, [rows[0].user_id])

    db.query(`DELETE FROM learning_guru WHERE id = ?`, [req.params.id], (error, rows) => {
      if (error) res.json({ error: true, result: error })

      res.json({ error: false, result: rows })
    })

  })
}

exports.deleteGuruByCompanyId = (req, res) => {
  let sql = `DELETE FROM learning_guru WHERE company_id = ?`;
  db.query(sql, [req.params.id], (error, rows) => {
    if (error) res.json({ error: true, result: error })

    res.json({ error: false, result: rows })
  })
}

exports.getSubmitMuridByKelasId = (req, res) => {
  let t1 = req.query.retake ? 'retake_exam_result' : 'exam_result_learning'
  let sql = `SELECT * FROM ${t1} WHERE exam_id = ? AND user_id = ?`;
  db.query(sql, [req.params.examId, req.params.userId], (err, rows) => {
    if (err) res.json({ error: true, result: err })

    res.json({ error: false, result: rows })
  })
}

exports.cekHadirMuridById = (req, res) => {
  let sql = `SELECT * FROM learning_jadwal_kehadiran WHERE user_id = ? AND sesi_id = ?`;
  let data = [req.params.user_id, req.params.sesi_id];
  db.query(sql, data, (err, rows) => {
    if (err) res.json({ error: true, result: err })

    res.json({ error: false, result: rows.length ? rows : [] })
  })
}
