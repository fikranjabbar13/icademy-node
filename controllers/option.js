var env = require('../env.json');
var conf = require('../configs/config');
var db = require('../configs/database');

exports.createOption = (req, res, next) => {
	let form = {
		question_id: req.body.question_id,
		exam_option: req.body.exam_option,
		description: req.body.description
	}

	db.query(`INSERT INTO exam_option 
		(option_id, question_id, exam_option, description) VALUES 
		(null, '${form.question_id}', '${form.exam_option}', '${form.description}')`, 
		(error, result, fields) => {
		if(error) {
			res.json({error: true, result: error})
		} else {
			db.query(`SELECT * FROM exam_option WHERE option_id = '${result.insertId}'`, (error, result, fields) => {
				res.json({error: false, result: result[0]})
			})
		}
	})
}

exports.getAllOption = (req, res, next) => {
	db.query(`SELECT * FROM exam_option`, (error, result, fields) => {
		if(error) {
			res.json({error: true, result: error})
		} else {
			res.json({error: false, result: result})
		}
	})
}

exports.getAllOptionByQuestion = (req, res, next) => {
	db.query(`SELECT * FROM exam_option WHERE question_id = '${req.params.question_id}' ORDER BY exam_option ASC`, (error, result, fields) => {
		if(error) {
			res.json({error: true, result: error})
		} else {
			res.json({error: false, result: result})
		}
	})	
}

exports.getOneOption = (req, res, next) => {
	db.query(`SELECT * FROM exam_option WHERE option_id = '${req.params.option_id}'`, (error, result, fields) => {
		if(error) {
			res.json({error: true, result: error})
		} else {
			res.json({error: false, result: result.length !== 0 ? result[0] : []})
		}
	})	
}

exports.updateOption = (req, res, next) => {
	let form = {
		exam_option: req.body.exam_option,
		description: req.body.description
	}

	db.query(`UPDATE exam_option SET 
		exam_option = '${form.exam_option}',
		description = '${form.description}'
		WHERE option_id = '${req.params.option_id}'`, (error, result, fields) => {
		if(error) {
			res.json({error: true, result: error})
		} else {
			db.query(`SELECT * FROM exam_option WHERE option_id = '${req.params.option_id}'`, (error, result, fields) => {
				res.json({error: false, result: result[0]})
			})
		}
	})
}

exports.deleteOption = (req, res, next) => {
	db.query(`DELETE FROM exam_option WHERE option_id = '${req.params.option_id}'`, (error, result, fields) => {
		if(error) {
			res.json({error: true, result: error})
		} else {
			res.json({error: false, result: result})
		}
	})	
}
