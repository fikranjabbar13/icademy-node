const chai = require("chai"),
    chaiHttp = require("chai-http"),
    should = chai.should(),
    appRoot = process.cwd(),
    server = require(appRoot + "/bin/www");

let data = {
    id: 1,
    user_id: 248,
    pengguna: 1,
    kuesioner: [
        {
            tanya: 77,
            jawab: 104,
        },
        {
            tanya: 78,
            jawab: 106,
        },
    ],
};

chai.use(chaiHttp);
describe("webinar", () => {
    it("webinar # isi kuesioner", (done) => {
        chai.request(server)
            .post("/v2/kuesioner/input")
            .send(data)
            .end((err, res) => {
                console.log(res.body);
                res.should.have.status(200);
                res.body.should.have.property("error").and.to.be.a("boolean");
                res.body.should.have.property("result").and.to.be.a("string");
                done();
            });
    });
});
