const chai = require("chai"),
    chaiHttp = require("chai-http"),
    should = chai.should(),
    appRoot = process.cwd(),
    server = require(appRoot + "/bin/www");

let data = {
    id: 1,
    kuesioner: [
        {
            tanya: "ini tanya 1",
            a: "a 1",
            b: "b 1",
            c: "c 1",
        },
        {
            tanya: "ini tanya 2",
            a: "a 2",
            b: "b 2",
            c: "c 2"
        },
    ],
};

chai.use(chaiHttp);
describe("webinar", () => {
    it("webinar # update kuesioner", (done) => {
        chai.request(server)
            .put("/v2/kuesioner")
            .send(data)
            .end((err, res) => {
                console.log(res.body);
                res.should.have.status(200);
                res.body.should.have.property("error").and.to.be.a("boolean");
                res.body.should.have.property("result").and.to.be.a("string");
                done();
            });
    });
});
