const chai = require("chai"),
    chaiHttp = require("chai-http"),
    should = chai.should(),
    appRoot = process.cwd(),
    server = require(appRoot + "/bin/www");

chai.use(chaiHttp);
describe("webinar", () => {
    it("webinar # delete kuesioner", (done) => {
        chai.request(server)
            .delete("/v2/kuesioner/1")
            .end((err, res) => {
                console.log(res.body);
                res.should.have.status(200);
                res.body.should.have.property("error").and.to.be.a("boolean");
                res.body.should.have.property("result").and.to.be.a("string");
                done();
            });
    });
});
