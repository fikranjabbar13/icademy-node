const chai = require('chai'),
  chaiHttp = require('chai-http'),
  should = chai.should(),
  appRoot = process.cwd(),
  server = require(appRoot + '/bin/www');

chai.use(chaiHttp);
describe('transcripts', () => {
  describe('#transcripts Get', () => {
    it('transcripts#transcripts Get', (done) => {
      chai
        .request(server)
        .get('/v1/transcripts/secondstefanus')
        .end((err, res) => {
          console.log(res.body);
          res.body.headers.should.have.status(200);
          done();
        });
    });
  });
});
