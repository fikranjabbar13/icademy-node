const chai = require("chai"),
    chaiHttp = require("chai-http"),
    should = chai.should(),
    appRoot = process.cwd(),
    server = require(appRoot + "/bin/www");

let data = {
    id: 3,
    pengguna: [65, 66],
    tamu: [3]
}

chai.use(chaiHttp);
describe("webinar", () => {
    it("webinar # update webinar send email", (done) => {
        chai.request(server)
            .post("/v2/webinar/send_email")
            .send(data)
            .end((err, res) => {
                console.log(res.body);
                res.should.have.status(200);
                res.body.should.have.property("error").and.to.be.a("boolean");
                res.body.should.have.property("result").and.to.be.a("string");
                done();
            });
    });
});
