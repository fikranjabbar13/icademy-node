const chai = require("chai"),
    chaiHttp = require("chai-http"),
    should = chai.should(),
    appRoot = process.cwd(),
    server = require(appRoot + "/bin/www");

let data = {
    judul: 'ini judul webinar',
    isi: 'ini isi webinar',
    tanggal: '2020-01-01',
    jam_mulai: 'ini jam mulai',
    jam_selesai: 'ini jam selesai',
    pembicara: 'ini pembicara webinar',
    id: '1'
}

chai.use(chaiHttp);
describe("webinar", () => {
    it("webinar # update webinar detail", (done) => {
        chai.request(server)
            .put("/v2/webinar/detail")
            .send(data)
            .end((err, res) => {
                console.log(res.body);
                res.should.have.status(200);
                res.body.should.have.property("error").and.to.be.a("boolean");
                res.body.should.have.property("result").and.to.be.a("string");
                done();
            });
    });
});
