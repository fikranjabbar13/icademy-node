var express = require('express');
var router = express.Router();
var jwt = require('jsonwebtoken');
const middleware = require("../middleware");

var auth = require('../configs/auth');
var env = require('../env.json');

var controllers = require('../controllers/index');

/* user login */
router.post('/auth', controllers.auth.auth);
router.post('/auth/voucher', controllers.auth.authVoucher);
router.get('/auth/logout/:user_id', controllers.auth.logout);

/* get me */
router.get('/auth/me/:email', auth.allow, controllers.auth.authMe);

router.get('/tes-kirim-email', controllers.user.testEmailPassword);

/* table company */
router.post('/company', auth.allow, controllers.company.createCompany);
router.get('/company', auth.allow, controllers.company.getCompanyList);
router.get('/company/:company_id', auth.allow, controllers.company.getCompanyOne);
router.put('/company/:company_id', auth.allow, controllers.company.updateCompany);
router.put('/company/logo/:company_id', auth.allow, controllers.company.updateCompanyLogo);
router.get('/company/delete-logo/:company_id', auth.allow, controllers.company.deleteCompanyLogo);
router.delete('/company/:company_id', auth.allow, controllers.company.deleteCompany);

/* table student */
router.post('/student', auth.allow, controllers.student.create);
router.get('/student', auth.allow, controllers.student.getAll);
router.get('/student/:student_id', auth.allow, controllers.student.getOne);
router.get('/student/company/:company_id', auth.allow, controllers.student.getByCompany);
router.put('/student/:student_id', auth.allow, controllers.student.update);
router.put('/student/change/:student_id', auth.allow, controllers.student.updateToStudent);
router.put('/student/attachment/:student_id', auth.allow, controllers.student.updateAttachment);
router.delete('/student/:student_id', auth.allow, controllers.student.delete);
/* dashboard */

router.get('/homepage/list-activity/:companyId/:userId', auth.allow, controllers.company.getListActivity);
//  sidebar-project
router.get('/sidebar/project/:level/:user_id/:company_id', auth.allow, controllers.company.Sidebar_Project);
router.get('/sidebar/project-list/:level/:user_id/:company_id', auth.allow, controllers.company.Sidebar_ProjectList);
router.get('/sidebar/project-detail/:level/:user_id/:project_id', auth.allow, controllers.company.Sidebar_ProjectDetail);
// homepage ongoing-upcoming event
router.get('/homepage/event/ongoing', auth.allow, controllers.company.homepage_list_event_ongoing);
router.get('/homepage/event/upcoming', auth.allow, controllers.company.homepage_list_event_upcoming);

router.get('/event/:level/:user_id/:company_id', auth.allow, controllers.company.getEventList);
router.get('/project/:level/:user_id/:company_id', auth.allow, controllers.company.getProjectList);
router.get('/project-get/:user_id/:company_id', auth.allow, controllers.company.getProjectListV2);
router.get('/project-read/:folder_id', auth.allow, controllers.files.getProjectById);
router.get('/project-access/:folder_id/:user_id', controllers.files.checkProjectAccess);

router.delete('/project/:folder_id', auth.allow, controllers.files.deleteFolder);
router.delete('/project-file/:file_id', auth.allow, controllers.files.deleteFile);
router.post('/project/delete-file', auth.allow, controllers.files.deleteProjectFile);
router.put('/project-file/:file_id', auth.allow, controllers.files.editFile);
router.put('/project/:folder_id', auth.allow, controllers.files.editFolder);

/* table user. */
router.post('/user', auth.allow, controllers.user.createUserPasswordEmail);
router.post('/user/password', auth.allow, controllers.user.cekPassword);
router.post('/user/import', auth.allow, controllers.user.importUsers);
router.post('/user/assign', auth.allow, controllers.user.assignUsers);
router.post('/user/check-otp', controllers.user.checkOTP);
router.get('/user/forgot-password/:email', controllers.user.forgotPassword);
router.get('/user/reset-password/:id/:otp/:password/:confirm/:time', controllers.user.resetPassword);
router.get('/user/konfirmasi-meeting/:id', controllers.user.konfirmasiMeeting);

router.get('/user/cek/:field', auth.allow, controllers.user.postCek);
router.get('/user', auth.allow, controllers.user.getUserList);
router.get('/user/:user_id', auth.allow, controllers.user.getUserOne);
router.get('/user/company/:company_id', auth.allow, controllers.user.getUserByCompany);
router.get('/user/all/company/:company_id', auth.allow, controllers.user.getAllUserByCompany);
router.get('/public/user/company/:company_id', controllers.user.getUserByCompany);
router.get('/user/group/:group_id', auth.allow, controllers.user.getUserByGroup);
router.get('/user/activity/:user_id', auth.allow, controllers.user.getUserActivity);
router.get('/user/assign/:user_id', auth.allow, controllers.user.getAssignUsers);

/* table user_course */
router.post('/user-course', auth.allow, controllers.usercourse.createUserCourse);
router.get('/user-course/:user_id', auth.allow, controllers.usercourse.getMyCourse);
router.get('/user-course/cek/:user_id/:course_id', auth.allow, controllers.usercourse.cekCourse);
router.put('/user-course/chapter/:user_id/:course_id', auth.allow, controllers.usercourse.updateStatChapter);
router.get('/user-course/result/:user_id/:course_id', auth.allow, controllers.usercourse.getResultExam);

router.put('/user/:user_id', auth.allow, controllers.user.updateUser);
router.put('/user/password/:user_id', auth.allow, controllers.user.updatePasswordUser);
router.put('/user/active/:user_id', auth.allow, controllers.user.updateActivePasive);
router.put('/user/voucher/:user_id', auth.allow, controllers.user.updateVoucherUser);
router.put('/user/email/:user_id', auth.allow, controllers.user.updateEmailUser);
router.put('/user/validity/:user_id', auth.allow, controllers.user.updateValidityUser);
router.put('/user/avatar/:user_id', auth.allow, controllers.user.updateAvatarUser);

router.delete('/user/assign/:user_id', auth.allow, controllers.user.deleteAssignUsers);
router.delete('/user/:user_id', auth.allow, controllers.user.deleteUser);

/* table user_setting */
router.post('/setting', auth.allow, controllers.setting.createSettingUser);
router.get('/setting/:setting_id', auth.allow, controllers.setting.getUserSettingById);
router.get('/setting/user/:user_id', auth.allow, controllers.setting.getUserSettingByUser);
router.put('/setting/:setting_id', auth.allow, controllers.setting.updateUserSettingById);
router.put('/setting/user/:user_id', auth.allow, controllers.setting.updateUserSettingByUser);
router.delete('/setting/:setting_id', auth.allow, controllers.setting.deleteUserSettingById);
router.delete('/setting/user/:user_id', auth.allow, controllers.setting.deleteUserSettingByUser);

/* table branch */
router.post('/branch', auth.allow, controllers.branch.createBranch);
router.get('/branch', auth.allow, controllers.branch.getBranchList);
router.get('/branch/:branch_id', auth.allow, controllers.branch.getBranchOne);
router.get('/branch/company/:company_id', auth.allow, controllers.branch.getBranchByCompany);
router.put('/branch/:branch_id', auth.allow, controllers.branch.updateBranch);
router.delete('/branch/:branch_id', auth.allow, controllers.branch.deleteBranch);

/* table semester */
router.post('/semester', auth.allow, controllers.semester.createSemester);
router.get('/semester', auth.allow, controllers.semester.getSemesterList);
router.get('/semester/:semester_id', auth.allow, controllers.semester.getSemesterOne);
router.get('/semester/company/:company_id', auth.allow, controllers.semester.getSemesterByCompany);
router.put('/semester/:semester_id', auth.allow, controllers.semester.updateSemester);
router.delete('/semester/:semester_id', auth.allow, controllers.semester.deleteSemester);

/* table grup */
router.post('/grup', auth.allow, controllers.grup.createGrup);
router.get('/grup', auth.allow, controllers.grup.getGrupList);
router.get('/grup/:grup_id', auth.allow, controllers.grup.getGrupOne);
router.get('/grup/company/:company_id', auth.allow, controllers.grup.getGrupByCompany);
router.put('/grup/:grup_id', auth.allow, controllers.grup.updateGrup);
router.delete('/grup/:grup_id', auth.allow, controllers.grup.deleteGrup);

/* table access */
router.post('/access', auth.allow, controllers.access.createAccess);
router.get('/access', auth.allow, controllers.access.getAccessList);
router.get('/access/user/:company_id', auth.allow, controllers.access.getAllUser)
router.get('/access/id/:access_id', auth.allow, controllers.access.getAccessOneById);
router.get('/access/company/:company_id', auth.allow, controllers.access.getAccessByCompany);

router.put('/access/id/:access_id', auth.allow, controllers.access.updateAccessById);
router.delete('/access/id/:access_id', auth.allow, controllers.access.deleteAccessById);

router.get('/access/user/:user_id', auth.allow, controllers.access.getAccessOneByUser);
router.put('/access/user/:user_id', auth.allow, controllers.access.updateAccessByUser);
router.delete('/access/user/:user_id', auth.allow, controllers.access.deleteAccessByUser);

/* table learning_category */
router.post('/category', auth.allow, controllers.category.createCategory);
router.get('/category', auth.allow, controllers.category.getAllCategory);
router.get('/category/:category_id', auth.allow, controllers.category.getOneCategory);
router.get('/category/company/:company_id', auth.allow, controllers.category.getCategoryByCompany);
router.put('/category/:category_id', auth.allow, controllers.category.updateCategory);
router.put('/category/image/:category_id', auth.allow, controllers.category.updateImageCategory);
router.delete('/category/:category_id', auth.allow, controllers.category.deleteCategory);

/* table attachment */
router.post('/attachment', auth.allow, controllers.attachment.createAttachment);
router.get('/attachment', auth.allow, controllers.attachment.getAllAttachment);
router.get('/attachment/:attachment_id', auth.allow, controllers.attachment.getOneAttachment);
router.put('/attachment/:attachment_id', auth.allow, controllers.attachment.updateAttachment);
router.delete('/attachment/:attachment_id', auth.allow, controllers.attachment.deleteAttachment);

/* table course */
router.post('/course', auth.allow, controllers.course.createCourse);
router.get('/course', auth.allow, controllers.course.getAllCourse);
router.get('/course/:course_id', auth.allow, controllers.course.getOneCourse);
router.get('/course/company/:company_id', auth.allow, controllers.course.getAllCourseByCompany);
router.get('/course/category/:category_id/:company_id', auth.allow, controllers.course.getAllCourseByCategory);
router.put('/course/:course_id', auth.allow, controllers.course.updateCourse);
router.put('/course/image/:course_id', auth.allow, controllers.course.updateImageCourse);
router.put('/course/thumbnail/:course_id', auth.allow, controllers.course.updateThumbnailCourse);
router.put('/course/publish/:course_id', auth.allow, controllers.course.updatePublishCourse);
router.put('/course/delete/:course_id', auth.allow, controllers.course.deleteCourse);

/* table course_chapter */
router.post('/chapter', auth.allow, controllers.chapter.createChapter);
router.post('/chapter/course', auth.allow, controllers.chapter.addCourseChapter);
router.get('/chapter', auth.allow, controllers.chapter.getAllChapter);
router.get('/chapter/:chapter_id', auth.allow, controllers.chapter.getOneChapter);
router.get('/chapter/course/:course_id', auth.allow, controllers.chapter.getChapterByCourse);

router.put('/chapter/:chapter_id', auth.allow, controllers.chapter.updateChapter);
router.put('/chapter/video/:chapter_id', auth.allow, controllers.chapter.updateVideoChapter);
router.put('/chapter/thumbnail/:chapter_id', auth.allow, controllers.chapter.updateTumbnailChapter);
router.put('/chapter/attachment/:chapter_id', auth.allow, controllers.chapter.updateAttachmentChapter);
router.delete('/chapter/:chapter_id', auth.allow, controllers.chapter.deleteChapter);

/** Course Chapter Forum */
router.post('/chapter/forum', auth.allow, controllers.chapter.createChapterForum);
router.put('/chapter/forum/:chapter_id', auth.allow, controllers.chapter.updateChapterForum);

/** Course Chapter Meeting */
router.post('/chapter/meeting', auth.allow, controllers.chapter.createChapterMeeting);
router.put('/chapter/meeting/:chapter_id', auth.allow, controllers.chapter.updateChapterMeeting);

/* course by company_id */
// router.get('/course/company/:company_id', auth.allow, controllers.course.getCourseByCompany);

/* table exam */
/* quiz */
router.post('/quiz', auth.allow, controllers.quiz.createQuiz);
router.get('/quiz', auth.allow, controllers.quiz.getAllQuiz);
router.get('/quiz/:quiz_id', auth.allow, controllers.quiz.getOneQuizById);
router.get('/quiz/course/:course_id/:company_id', auth.allow, controllers.quiz.getQuizByCourse);
router.get('/quiz/company/:company_id', auth.allow, controllers.quiz.getQuizByCompany);

router.put('/quiz/:quiz_id', auth.allow, controllers.quiz.updateQuizById);
router.put('/quiz/penempatan/:quiz_id', auth.allow, controllers.quiz.updateQuizAtById);
router.delete('/quiz/:quiz_id', auth.allow, controllers.quiz.deleteQuizById);

// cek if quiz or exam
router.get('/isquizorexam/:exam_id', auth.allow, controllers.exam.isQuizOrExam);

/* exam */
router.post('/exam', auth.allow, controllers.exam.createExam);
router.get('/exam', auth.allow, controllers.exam.getListExam);
router.get('/exam/:exam_id', auth.allow, controllers.exam.getOneExam);
router.get('/exam/course/:course_id/:company_id', auth.allow, controllers.exam.getExamByCourse);
router.get('/exam/coursepublish/:course_id/:company_id', auth.allow, controllers.exam.getExamPublishByCourse);

router.put('/exam/:exam_id', auth.allow, controllers.exam.updateExam);
router.put('/exam/publish/:exam_id', auth.allow, controllers.exam.updatePublishExam);
router.delete('/exam/:exam_id', auth.allow, controllers.exam.deleteExam);

/* table question */
router.post('/question', auth.allow, controllers.question.createQuestion);
router.post('/question/import', auth.allow, controllers.question.importQuestion);
router.get('/question', auth.allow, controllers.question.getAllQuestion);
router.get('/question/exam/:exam_id', auth.allow, controllers.question.getAllQuestionByExam);
router.get('/question/:question_id', auth.allow, controllers.question.getOneQuestion);
router.put('/question/:question_id', auth.allow, controllers.question.updateQuestion);
router.put('/question/penjelasan/:question_id', auth.allow, controllers.question.updatePenjelasanQuestion);
router.delete('/question/:question_id', auth.allow, controllers.question.deleteQuestion);
router.delete('/question/bulk/:question_id', auth.allow, controllers.question.deleteBulkQuestion);

/* table question options */
router.post('/option', auth.allow, controllers.option.createOption);
router.get('/option', auth.allow, controllers.option.getAllOption);
router.get('/option/question/:question_id', auth.allow, controllers.option.getAllOptionByQuestion);
router.get('/option/:option_id', auth.allow, controllers.option.getOneOption);
router.put('/option/:option_id', auth.allow, controllers.option.updateOption);
router.delete('/option/:option_id', auth.allow, controllers.option.deleteOption);

/* table exam answer */
router.post('/exam-answer', auth.allow, controllers.examanswer.answerQuestion);
router.get('/exam-answer/start/:exam_id/:user_id', auth.allow, controllers.examanswer.getStartExamQuestion);
router.get('/exam-answer/exam/:exam_id/:user_id', auth.allow, controllers.examanswer.getAllAnswerQuestion);
router.get('/exam-answer/answer/:user_id/:question_id', auth.allow, controllers.examanswer.getOneAnswerQuestion);
router.put('/exam-answer/update/:user_id/:question_id', auth.allow, controllers.examanswer.updateAnswerQuestion);
router.get('/exam-answer/submit/:exam_id/:user_id', auth.allow, controllers.examanswer.submitAnswerQuestion);

/* table forum */
router.post('/forum', auth.allow, controllers.forum.createForum);
router.post('/forum/add', auth.allow, controllers.forum.addUserForum);
router.get('/forum', auth.allow, controllers.forum.getAllForum);
router.get('/forum/list/:user_id', auth.allow, controllers.forum.getUserForum);
router.get('/forum/id/:forum_id/:userId', auth.allow, controllers.forum.getForumById);
router.get('/forum/user/:user_id', auth.allow, controllers.forum.getForumByUser);
router.get('/forum/company/:company_id/:userId', auth.allow, controllers.forum.getForumByCompany);

router.put('/forum/:forum_id', auth.allow, controllers.forum.updateForum);
router.put('/forum/cover/:forum_id', auth.allow, controllers.forum.updateForumCover);
router.put('/forum/kunci/:forum_id', auth.allow, controllers.forum.updateKunciForum);

router.delete('/forum/:forum_id', auth.allow, controllers.forum.deleteForum);
router.delete('/forum/remove/:forum_id/:user_id', auth.allow, controllers.forum.deleteUserForum);

/* table forum_post */
router.post('/forum-post', auth.allow, controllers.forumpost.createPost);
router.get('/forum-post/id/:post_id', auth.allow, controllers.forumpost.getPostById);
router.get('/forum-post/forum/:forum_id', auth.allow, controllers.forumpost.getPostByForum);
router.put('/forum-post/attachment/:post_id', auth.allow, controllers.forumpost.updateAttachmentPost);
router.put('/forum-post/:post_id', auth.allow, controllers.forumpost.updatePost);
router.delete('/forum-post/:post_id', auth.allow, controllers.forumpost.deletePost);

/* Activity */
router.get('/api-activity/history/:user_id', auth.allow, controllers.activity.getHistoryAll);
router.post('/api-activity/new-course', auth.allow, controllers.activity.newHistoryCourse);
router.post('/api-activity/new-forum', auth.allow, controllers.activity.newHistoryForum);
router.post('/api-activity/new-class', auth.allow, controllers.activity.newHistoryClass);
router.post('/api-activity/new-login', auth.allow, controllers.activity.newHistoryLogin);
router.get('/api-activity/chart/:company_id/:start/:end', auth.allow, controllers.activity.chart);

/* Notification */
router.get('/notification/all/:user_id', auth.allow, controllers.notification.getNotificationAll);
router.get('/notification/unread/:user_id', auth.allow, controllers.notification.getNotificationUnread);
router.put('/notification/read', auth.allow, controllers.notification.updateNotificationToRead);
router.put('/notification/read/all', auth.allow, controllers.notification.readAllToRead);
router.post('/notification/broadcast', auth.allow, controllers.notification.addNotification);
router.post('/notification/broadcast-bulk', auth.allow, controllers.notification.addNotificationBulk);
router.post('/notification/broadcast-bulk/private', controllers.notification.addNotificationBulk);
router.put('/notification/id/:id', auth.allow, controllers.notification.deleteAllNotif);
router.delete('/notification/id/:id', auth.allow, controllers.notification.deleteNotif);

/* calendar of agenda*/
router.get('/agenda/:user_id', auth.allow, controllers.calendar.getUserAgenda);
router.get('/sidebar/agenda/:companyId/:userId', auth.allow, controllers.calendar.getUserAgenda_sidebar);
router.post('/agenda/:user_id', auth.allow, controllers.calendar.createUserAgenda);

router.get('/mycourse/:user_id/:company_id', auth.allow, controllers.results.getMyCreateCourse);
router.get('/hasilkursus/:user_id/:course_id', auth.allow, controllers.results.getHaislKursus);

// UNTUK PUBLIC / NON STAFF
router.post('/liveclasspublic/share', controllers.liveclass.sendMailGmail);
router.get('/liveclasspublic/id/:class_id', controllers.liveclass.getLiveClassById);
router.get('/liveclasspublic/file/:class_id', controllers.liveclass.getFileSharingById);
router.get('/liveclass/activeparticipantpublic/:class_id', controllers.liveclass.getActiveParcicipantByRoomId);
// END UNTUK PUBLIC / NON STAFF

router.post('/liveclass', auth.allow, controllers.liveclass.postLiveClassByCompany);
router.post('/liveclass/share', auth.allow, controllers.liveclass.sendMailGmail);
router.post('/liveclass/file', auth.allow, controllers.liveclass.uploadFile);
router.post('/liveclass/mom', auth.allow, controllers.liveclass.addMOM);
router.post('/liveclass/id/:project_id', auth.allow, controllers.liveclass.updateRecentProject);


router.get('/liveclass/id/:class_id', auth.allow, controllers.liveclass.getLiveClassById);
router.get('/liveclass/project/:level/:user_id/:project_id', auth.allow, middleware.checkAccessBySub, controllers.liveclass.getLiveClassByProject);
router.get('/liveclass/webinar/:level/:user_id/:webinar_id', auth.allow, controllers.liveclass.getLiveClassByWebinar);
router.get('/liveclass/meeting-info/:class_id', auth.allow, controllers.liveclass.getInfoLiveclass);
router.post('/liveclass/meeting-booking-info', auth.allow, controllers.liveclass.getInfoLiveclassBooking);
router.get('/liveclass/company/:company_id', auth.allow, controllers.liveclass.getLiveClassByCompany);
router.get('/liveclass/company-user/:level/:user_id/:company_id', controllers.liveclass.getLiveClassByCompanyUser);
router.get('/liveclass/file/:class_id', auth.allow, controllers.liveclass.getFileSharingById);
router.get('/liveclass/invite/:company_id/:user_id', auth.allow, controllers.liveclass.getClassByCompany);
router.get('/liveclass/mom/:id', auth.allow, controllers.liveclass.getMOM);
router.get('/liveclass/activeparticipant/:class_id', auth.allow, controllers.liveclass.getActiveParcicipantByRoomId);

router.put('/liveclass/id/:class_id', auth.allow, controllers.liveclass.updateLiveClass);
router.put('/liveclass/cover/:class_id', auth.allow, controllers.liveclass.updateCoverLiveClass);
router.put('/liveclass/live/:class_id', auth.allow, controllers.liveclass.updateIsLiveClass);
router.put('/liveclass/lock-meeting/:class_id', auth.allow, controllers.liveclass.lockMeeting);
router.put('/liveclass/share-gantt/:class_id', controllers.liveclass.updateShareGantt);
router.put('/liveclass/share-file/:class_id', controllers.liveclass.updateFileShow);
router.put('/liveclass/active/:class_id', controllers.liveclass.updateActiveParticipants);
router.put('/liveclass/mom/:id', controllers.liveclass.editMOM);
router.put('/liveclass/title-mom/:id', controllers.liveclass.editTitleMOM);
router.put('/liveclass/confirmation/:class_id/:user_id', controllers.liveclass.confirmAttendance);
router.put('/liveclass/actualattendance/:class_id/:user_id', controllers.liveclass.actualAttendance);

router.delete('/liveclass/delete/:class_id', auth.allow, controllers.liveclass.deleteLiveClassById);
router.delete('/liveclass/file/delete/:class_id', auth.allow, controllers.liveclass.deleteFile);
router.post('/liveclass/file/remove', auth.allow, controllers.liveclass.deleteFileByAttachment);
router.delete('/liveclass/mom/delete/:id', auth.allow, controllers.liveclass.deleteMOM);
router.delete('/liveclass/participant/delete/:participant_id', auth.allow, controllers.liveclass.deleteParticipant);


/**
 * certificate
 */
router.post('/certificate', auth.allow, controllers.certificate.createCertificate);
router.get('/certificate/:type_activity/:activity_id', auth.allow, controllers.certificate.readCertificate);
router.get('/client-certificate/:type_activity/:user_id', auth.allow, controllers.certificate.clientCertificate);
router.get('/detail-certificate/:user_id/:certificate_id', auth.allow, controllers.certificate.detailCertificate);
router.put('/certificate/:id', auth.allow, controllers.certificate.updateCertificate);
router.delete('/certificate/:id', auth.allow, controllers.certificate.deleteCertificate);

/**
 * transcripts jigasi
 */
router.get('/transcripts/:room_name', controllers.transcripts.read);

/**
 * wording
 */
router.post('/wording', auth.allow, controllers.wording.createWording);
router.get('/wording/:by/:value', auth.allow, controllers.wording.getWording);
router.put('/wording/:by/:value', auth.allow, controllers.wording.updateWording);
router.delete('/wording/:by/:value', auth.allow, controllers.wording.deleteWording);

/**
 * informasi
 */
router.post('/informasi', auth.allow, controllers.informasi.create);
router.get('/informasi', auth.allow, controllers.informasi.get);
router.get('/informasi/company/:company', auth.allow, controllers.informasi.getByCompany);
router.get('/informasi/:id', auth.allow, controllers.informasi.getOne);
router.put('/informasi/:value', auth.allow, controllers.informasi.update);
router.put('/informasi/file/:value', auth.allow, controllers.informasi.updateFile);
router.delete('/informasi/:value', auth.allow, controllers.informasi.delete);

/**
 * Files & Folder
 */
router.get('/files-build-recording/:recordId', controllers.files.buildRecording);
router.get('/files-download-recording/:recordId', controllers.files.downloadRecording);
router.get('/files/:level/:company_id', auth.allow, controllers.files.filesGetAllProject);
router.get('/files/:level/:mother/:company_id', auth.allow, controllers.files.filesGetChildProject);
router.post('/files/create-folder', auth.allow, controllers.files.filesCreateFolder);
router.post('/files/rename-folder', auth.allow, controllers.files.filesRenameFolder);
router.get('/files-getfolder/:level/:company_id', auth.allow, controllers.files.filesGetAllFolder);
router.post('/files-movefolder/:level/:company_id', auth.allow, controllers.files.filesMoveTo);
router.post('/movefolder', auth.allow, controllers.files.folderMoveTo);
// router.post('/duplicate-folder', auth.allow, controllers.files.duplicateFolder); //Temporary Not Used
router.post('/duplicate-file', auth.allow, controllers.files.duplicateFile);
router.get('/files-folder-access/:id', auth.allow, controllers.files.getAccessFolder);

router.post('/folder', auth.allow, controllers.files.createFolder);
router.post('/folder/files', controllers.files.uploadFilesS3);
router.get('/folder/:company/:mother', controllers.files.getFolderByCompany);
router.get('/folder-by-user/:company/:mother/:user/:guest', controllers.files.getFolderByUser);
router.get('/files/:folder_id', controllers.files.getFilesByFolder);
router.get('/files-mom/:folder_id', controllers.files.getFolderMOM);
router.get('/files-recorded/:folder_id', controllers.files.getFolderRecordedMeeting);
router.get('/folder/back/:company/:id', controllers.files.getPrevFolder);
router.get('/folder-guest/:mother', controllers.files.getFolderById);
router.get('/folder-back-guest/:id', controllers.files.getPrevFolderByProject);
router.post('/files-logs', auth.allow, controllers.files.logs);
router.get('/files-logs/:company_id', auth.allow, controllers.files.filesLog);
// Record
router.put('/record/:record_id', controllers.files.renameRecord);
router.get('/record/:project_id', controllers.files.getRecord);

// Import Data Guru //  import Data Students
router.post('/upload/data-teacher', controllers.uploadFile.importGuru);
router.post('/upload/data-students', controllers.uploadFile.importStudents);

//  Pengumuman
router.post('/pengumuman/create', controllers.pengumuman.createPengumuman);
router.get('/pengumuman/:id', controllers.pengumuman.getById);
router.get('/pengumuman/company/:id', controllers.pengumuman.getByCompanyId);
router.get('/pengumuman/role/:id', controllers.pengumuman.getByUserId);
router.put('/pengumuman/update/:id', controllers.pengumuman.updatePengumuman);
router.put('/pengumuman/files/:id', controllers.pengumuman.filesPengumuman);
router.delete('/pengumuman/delete/:id', controllers.pengumuman.deletePengumuman);

// Chapter
router.post('/chapters', controllers.chapters.addChapter);
router.post('/chapters/course', controllers.chapters.addCourseChapter);
router.get('/chapters', controllers.chapters.getAllChapters);
router.get('/chapters/:chapter_id', controllers.chapters.getChapterById);
router.get('/chapters/course/:course_id', controllers.chapters.getChapterByCourse);
router.put('/chapters/:chapter_id', controllers.chapters.updateChapter);
router.put('/chapters/video/:chapter_id', controllers.chapters.updateVideo);
router.put('/chapters/thumbnail/:chapter_id', controllers.chapters.updateThumbnail);
router.put('/chapters/attachment/:chapter_id', controllers.chapters.updateAttachment);
router.delete('/chapters/:chapter_id', controllers.chapters.deleteChapters);

// Learning PTC
router.get('/ptc-room', controllers.ptc_room.getAllPtc);
router.get('/ptc-room/:ptc_id', controllers.ptc_room.getPtcById);
router.get('/ptc-room/peserta/:ptc_id', controllers.ptc_room.getAllPesertaByPtc);
router.get('/ptc-room/cek-hadir/:ptc_id/:user_id', controllers.ptc_room.cekKehadiran);
router.get('/ptc-room/company/:company_id', controllers.ptc_room.getPtcByCompanyId);
router.get('/ptc-room/user/:user_id', controllers.ptc_room.getPtcByUser);
router.get('/ptc-room/moderator/:moderator', controllers.ptc_room.getPtcByModerator);
router.get('/ptc-room/parents/:user_id', controllers.ptc_room.getPtcByParents);

router.post('/add/ptc-room', controllers.ptc_room.addLearningPtc);
router.post('/add/ptc-room/peserta', controllers.ptc_room.addLearningPtcPeserta);
router.put('/ptc-room/update/:ptc_id', controllers.ptc_room.updatePtc);
router.put('/ptc-room/konfirmasi/:ptc_id/:user_id', controllers.ptc_room.konfirmasiPtc);
router.put('/ptc-room/hadir/:ptc_id/:user_id', controllers.ptc_room.kehadiranPtc);
router.put('/ptc-room/peserta/update/:id', controllers.ptc_room.updatePesertaPtc);

router.delete('/ptc-room/delete/:ptc_id', controllers.ptc_room.deletePtc);
router.delete('/ptc-room/peserta/delete/:id', controllers.ptc_room.deletePesertaPtc);

// Cek Versi Apps


router.get('/api/cek-version', controllers.version.version);
router.post('/unduh-raport', controllers.reportRaport.reportRaport);


module.exports = router;
