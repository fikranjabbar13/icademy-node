var express = require('express');
var router = express.Router();

var auth = require('../configs/auth');
var env = require('../env.json');

var controllers = require('../controllers/index');

// TEST API
router.get('/version', controllers.version.version2);

// LEARNING 
router.get('/allUser/company/:companyId', auth.allow, controllers.personalia.getUserByCompanyId);

router.post('/bantuan/upload-file', controllers.bantuan.uploadFilesBantuanS3);
router.get('/bantuan/all-file', controllers.bantuan.getAllBantuan);
router.get('/bantuan/view-file/:filename', controllers.bantuan.viewFile);
router.get('/tutorial', controllers.bantuan.browseTutorial);

// Global Settings
router.get('/global-settings/check-access', auth.allow, controllers.grup.checkAccessAll);

// Zoom
router.get('/zoom/user/:userId', /**auth.allow,*/ controllers.zoom.getUserById);
router.post('/zoom/user', auth.allow, controllers.zoom.getUser);
router.delete('/zoom/user/:userId', auth.allow, controllers.zoom.deleteUser);

module.exports = router;
