var express = require('express');
var router = express.Router();
var jwt = require('jsonwebtoken');
const middleware = require("../middleware");

var auth = require('../configs/auth');
var env = require('../env.json');

var controllers = require('../controllers/index');

/** ONLY Upload File */
router.post('/media/upload', controllers.chapter.onlyUploadFile);

/* webinar */
router.get('/training/liveclass-by-course/:idCourse/:idCompany', auth.allow, controllers.webinar.readLiveClassByCourse);

router.post('/webinar/set/speaker', auth.allow, function (req, res, next) {
    if (req.query.action === 'remove') {
        return controllers.webinar.deleteSpeakerWebinar(req, res);
    } else {
        return controllers.webinar.setSpeakerWebinar(req, res);
    }
});

router.post('/webinar/roles/external',auth.allow, controllers.webinar.setSpeakerExternalWebinar);
router.post('/webinar/roles/external/delete',auth.allow, controllers.webinar.deleteSpeakerExternalWebinar);
router.post('/webinar/sendmail/role/speaker',auth.allow, controllers.webinar.sendMailSpeakerExternalWebinar);
router.get('/webinar/is-paid/:id', auth.allow, controllers.webinar.updateIsPaid);
router.post('/webinar/guest-action', auth.allow, controllers.webinar.guestAction);
router.post('/guest/event-webinar/registration', controllers.webinar.guestRegistration);
router.get('/webinar/role/:company_id/:project_id', auth.allow, controllers.webinar.getRole);
router.post('/webinar/role', auth.allow, controllers.webinar.createRole);
router.put('/webinar/role/:id', auth.allow, controllers.webinar.updateRole);
router.delete('/webinar/role/:id', auth.allow, controllers.webinar.deleteRole);

router.get('/webinar/list/:project_id', auth.allow, middleware.checkAccessBySub, controllers.webinar.getWebinarsByProject);
router.get('/webinar/list-by-course/:course_id', auth.allow, controllers.webinar.browseByCourse);
router.get('/webinar/list-by-training/:company_id', auth.allow, controllers.webinar.getWebinarsByTraining);
router.get('/webinar/list-by-company/:company_id', auth.allow, middleware.checkAccessBySub, controllers.webinar.getWebinarsByCompany);
router.get('/webinar/list-by-company-plain/:company_id', auth.allow, controllers.webinar.getWebinarsByCompanyPlain);
router.post('/webinar/create', auth.allow, controllers.webinar.createWebinar);
router.put('/webinar/image/:id', auth.allow, controllers.webinar.uploadImage);
router.post('/webinar/import-participant', auth.allow, controllers.webinar.importParticipants);
router.post('/webinar/import-participant-tc', auth.allow, controllers.webinar.importParticipantsByTrainingCompany);
router.post('/webinar/peserta', auth.allow, controllers.webinar.addPeserta);
router.post('/webinar/tamu', auth.allow, controllers.webinar.addTamu);
router.post('/webinar/tamu/import', auth.allow, controllers.webinar.import);
router.delete('/webinar/peserta/:id', auth.allow, controllers.webinar.deletePeserta);
router.delete('/webinar/tamu/:id', auth.allow, controllers.webinar.deleteTamu);
router.put('/webinar/cover/:id', auth.allow, controllers.webinar.updateCoverWebinar);
router.get('/webinar/one/:id', auth.allow, middleware.checkAccessBySub, controllers.webinar.getWebinarsById);
router.post('/webinar/send_email', auth.allow, controllers.webinar.sendEmail);
router.get('/webinar/tamu/one/:id', middleware.checkAccessBySub, controllers.webinar.getWebinarsById_public);
router.get('/webinar/tamu/:voucher', controllers.webinar.getTamuByVoucher);
router.put('/webinar/detail', controllers.webinar.updateDetailWebinar);
router.put('/webinar/status', controllers.webinar.updateStatusWebinar);
router.delete('/webinar/unpublish/:id', controllers.webinar.deleteWebinar);
router.put('/webinar/edit', controllers.webinar.editWebinar);
router.post('/webinar/log/:webinar_id/:peserta_id/:type/:action', controllers.webinar.log);
router.get('/webinar/history/:id', auth.allow, controllers.webinar.getWebinarHistory);
router.post('/webinar/sertifikat', controllers.webinar.postSertifikat);
router.post('/webinar/sendOfferLetter', controllers.webinar.sendOfferLetter);

// kuesioner
router.post('/kuesioner', auth.allow, controllers.kuesioner.create);
router.delete('/kuesioner/:id', auth.allow, controllers.kuesioner.delete);
router.delete('/kuesioner/pertanyaan/:id', auth.allow, controllers.kuesioner.deleteByIdPertanyaan);
router.put('/kuesioner', auth.allow, controllers.kuesioner.update);
router.get('/kuesioner/:id', auth.allow, controllers.kuesioner.read);
router.get('/kuesioner-peserta/:id', controllers.kuesioner.readPeserta);
router.post('/kuesioner/input', controllers.kuesioner.input);
router.get('/kuesioner/result/:id', controllers.kuesioner.result);
router.post('/kuesioner/import', auth.allow, controllers.kuesioner.import);
router.get('/kuesioner/sender/:id', controllers.kuesioner.getKuesionerSender);

router.put('/webinar/send-kuesioner/:id', auth.allow, controllers.webinar.sendKuesioner);
router.put('/webinar/send-posttest/:id', auth.allow, controllers.webinar.sendPosttest);
router.put('/webinar/send-pretest/:id', auth.allow, controllers.webinar.sendPretest);
router.put('/webinar/send-essay/:id', controllers.webinar.sendEssay);

// QNA
router.post('/webinar/qna', controllers.webinar.createQNA);
router.get('/webinar/qna/:webinarId', controllers.webinar.getWebinarQNA);
router.get('/webinar/qna-peserta/:webinarId/:pesertaId', controllers.webinar.getWebinarQNAByUser);

// bigbluebutton
router.post('/bbb/join', controllers.bbb.getUrl);
router.post('/bbb/is-running', controllers.bbb.isMeetingRunning);
router.get('/bbb/record/:user_id/:id', auth.allow, controllers.bbb.getRecordByProject);

// TASK
router.post('/task/create', auth.allow, controllers.task.createTask);
router.get('/task/project/:projectId', auth.allow, controllers.task.getTaskByProject);
router.get('/task/id/:id', auth.allow, controllers.task.getTaskById);
router.put('/task/update/:id', auth.allow, controllers.task.updateTaskById);
router.put('/task/lock/:id', auth.allow, controllers.task.lock);
router.delete('/task/delete/:id', auth.allow, controllers.task.delete);

// TASK -> Assigne
router.post('/task/assigne', auth.allow, controllers.task.assigneUser);
router.delete('/task/assigne/:id', auth.allow, controllers.task.deleteAssigne);
router.delete('/task/delete/:taskId/:userId', auth.allow, controllers.task.deleteBulkAssigne);
router.delete('/task/delete-all/:taskId', auth.allow, controllers.task.deleteAssigneByTask);

// TASK -> Subtasks
router.post('/task/subtask', auth.allow, controllers.task.createSubtask);
router.put('/task/subtask/:id', auth.allow, controllers.task.updateSubtaskById);
router.delete('/task/subtask/:id', auth.allow, controllers.task.deleteSubtaskById);

// TASK -> Attacments
router.post('/task/attachments', auth.allow, controllers.task.uploadAttachments);
router.delete('/task/attachments/:id', auth.allow, controllers.task.deleteAttachmentById);

// TO DO List
router.get('/todo/get/:userId', auth.allow, controllers.todo.getByUser);
router.post('/todo/create', auth.allow, controllers.todo.createTodo);
router.put('/todo/done/:id', auth.allow, controllers.todo.doneToDo);
router.delete('/todo/delete/:id', auth.allow, controllers.todo.deleteToDo);

// Booking Meeting
router.get('/meeting/booking/:meetingId', auth.allow, middleware.checkAccessBySub, controllers.liveclass.getBooking);
router.post('/meeting/booking', auth.allow, controllers.liveclass.addBooking);
router.post('/client/meeting/booking', middleware.verifyApi, controllers.liveclass.addClientBooking);
router.put('/meeting/booking/:id', auth.allow, controllers.liveclass.editBooking);
router.post('/meeting/invite-participants/:idBooking', auth.allow, controllers.liveclass.inviteParticipants)
router.delete('/meeting/booking/:id', auth.allow, controllers.liveclass.cancelBooking);

router.post('/meetpub/participant', controllers.liveclass.addParticipantMeetPub)
router.get('/meetpub/id/:booking_id', middleware.checkAccessBySub, controllers.liveclass.getMeetPubById_public);
router.get('/meet/id/:booking_id', auth.allow, middleware.checkAccessBySub, controllers.liveclass.getMeetPubById);
router.get('/meetpub/file/:booking_id', controllers.liveclass.getFilePubById);

// Learning kelas
router.post('/kelas/create', auth.allow, controllers.kelas.create);
router.get('/kelas/one/:id', auth.allow, controllers.kelas.getById);
router.get('/kelas/company/:companyId', auth.allow, controllers.kelas.getByCompanyId);

router.get('/kelas/murid/:userId', auth.allow, controllers.kelas.getKelasByUserMuridId);

router.get('/semester-kelulusan/:companyId', auth.allow, controllers.semester.getSemesterByCompanyId)
router.post('/semester-kelulusan', auth.allow, controllers.semester.postSemesterByCompanyId)

router.get('/kelas/semester/:companyId/:semesterId', auth.allow, controllers.kelas.getBySemesterId);
router.put('/kelas/update/:id', auth.allow, controllers.kelas.updateById);
router.delete('/kelas/delete/:id', auth.allow, controllers.kelas.deleteById);

// Silabus Pelajaran nilai
router.put('/nilai-pelajaran/:pelajaranId', auth.allow, controllers.pelajaran.updateProsentase);
router.get('/nilai-pelajaran/:pelajaranId', auth.allow, controllers.pelajaran.getProsentase);

// Silabus Pelajaran Silabus
router.post('/silabus/create', auth.allow, controllers.pelajaran.createSilabus);
// router.post('/silabus/import', auth.allow, controllers.pelajaran.importSilabus);
router.get('/silabus/id/:id', auth.allow, controllers.pelajaran.getSilabusById);
router.get('/silabus/jadwal/:id', auth.allow, controllers.pelajaran.getSilabusByJadwal);
router.get('/silabus/pelajaran/:id', auth.allow, controllers.pelajaran.getSilabusByPelajaran);
router.put('/silabus/update/:id', auth.allow, controllers.pelajaran.updateSilabus);
router.put('/silabus/files/:id', auth.allow, controllers.pelajaran.fileSilabus);
router.delete('/silabus/delete/:id', auth.allow, controllers.pelajaran.deleteSilabus);

// Learning Pelajaran
router.post('/pelajaran/create', auth.allow, controllers.pelajaran.create);
router.get('/pelajaran/one/:id', auth.allow, controllers.pelajaran.getById);
router.get('/pelajaran/jadwal/:jadwalId', auth.allow, controllers.pelajaran.getDetailPelajaranByJadwalId);
router.get('/pelajaran/preview/:jadwalId', auth.allow, controllers.pelajaran.getPelajaranByJadwalId);
router.get('/pelajaran/company/:companyId', auth.allow, controllers.pelajaran.getByCompanyId);
router.get('/pelajaran/company/guru/:companyId', auth.allow, controllers.pelajaran.getByCompanyGuruId);
router.put('/pelajaran/update/:id', auth.allow, controllers.pelajaran.updateById);
router.put('/pelajaran/overview/:id', auth.allow, controllers.pelajaran.updateOverviewById);
router.delete('/pelajaran/delete/:id', auth.allow, controllers.pelajaran.deletePelajaranById);

// chapter
router.post('/pelajaran/chapter/create', auth.allow, controllers.pelajaran.createChapter);
router.get('/pelajaran/chapter/all/:id', auth.allow, controllers.pelajaran.getAllChapters);
router.get('/pelajaran/chapter/one/:id', auth.allow, controllers.pelajaran.getOneChapters);
router.put('/pelajaran/chapter/files/:id', auth.allow, controllers.pelajaran.uploadAttachments);
router.put('/pelajaran/chapter/update/:id', auth.allow, controllers.pelajaran.updateChapter);
router.delete('/pelajaran/chapter/delete/:id', auth.allow, controllers.pelajaran.deleteChapter);

router.post('/pelajaran/pertanyaan/buat', auth.allow, controllers.pelajaran.createPertanyaan);
router.post('/pelajaran/pertanyaan/import', auth.allow, controllers.pelajaran.importPertanyaan);
router.get('/pelajaran/pertanyaan/semua/:id', auth.allow, controllers.pelajaran.getAllPertanyaan);
router.get('/pelajaran/pertanyaan/murid/:id', auth.allow, controllers.pelajaran.getMuridAllPertanyaan);
router.get('/pelajaran/pertanyaan/murid-user/:id/:userId', auth.allow, controllers.pelajaran.getMuridAllPertanyaan);
router.delete('/pelajaran/pertanyaan/hapus/:id', auth.allow, controllers.pelajaran.deletePertanyaan);

// kuis
router.post('/pelajaran/:tipe/create', auth.allow, controllers.pelajaran.createKuis);
router.get('/pelajaran/:tipe/all/:id', auth.allow, controllers.pelajaran.getAllKuis);
router.get('/pelajaran/:tipe/one/:id', auth.allow, controllers.pelajaran.getOneKuis);
router.put('/pelajaran/:tipe/update/:id', auth.allow, controllers.pelajaran.updateKuis);
router.delete('/pelajaran/:tipe/delete/:id', auth.allow, controllers.pelajaran.deleteKuis);

// range nilai
router.get('/range-nilai/company/:company_id', auth.allow, controllers.kpi.get)
router.post('/range-nilai/create', auth.allow, controllers.kpi.create)
router.put('/range-nilai/update/:id', auth.allow, controllers.kpi.patch)
router.delete('/range-nilai/delete/:id', auth.allow, controllers.kpi.delete)

// Learning personalia
// router.post('/personalia/import', auth.allow, controllers.personalia.importPersonalia);

// Learning Murid
router.post('/murid/create', auth.allow, controllers.personalia.createMurid);
router.post('/murid/import', auth.allow, controllers.personalia.importMurid);
router.get('/murid/one/:id', auth.allow, controllers.personalia.getMuridById);
router.get('/murid/no-induk/:id', auth.allow, controllers.personalia.getMuridByNoInduk);
router.get('/murid/user-id/:id', auth.allow, controllers.personalia.getMuridByUserId);
router.get('/murid/company/:companyId', auth.allow, controllers.personalia.getMuridByCompanyId);
router.get('/murid/jadwal/:jadwalId', auth.allow, controllers.personalia.getMuridByJadwalId);
router.get('/murid/jadwal-absen/:jadwalId/:event/:sesiId', auth.allow, controllers.personalia.getMuridAbsenByJadwalId);
router.post('/murid/jadwal-absen', auth.allow, controllers.personalia.postMuridAbsenByJadwalId);
router.get('/murid/kelas-v1/:jadwalId', auth.allow, controllers.personalia.getKelasByJadwalId);
router.put('/murid/update/:id', auth.allow, controllers.personalia.updateMuridById);
router.get('/murid/cek-hadir/:user_id/:sesi_id', auth.allow, controllers.personalia.cekHadirMuridById);
router.delete('/murid/one/:id', auth.allow, controllers.personalia.deleteMuridById);
router.delete('/murid/company/:id', auth.allow, controllers.personalia.deleteMuridByCompanyId);

// Murid di Kelas
router.post('/murid/assign/kelas', auth.allow, controllers.personalia.insertMuridToKelas);
router.post('/murid/assign/delete', auth.allow, controllers.personalia.deleteMuridToKelas);
router.get('/murid/kelas/:kelasId', auth.allow, controllers.personalia.getMuridByKelasId);

// Murid kuis & ujian
router.post('/murid/kuis-ujian/jawab', auth.allow, controllers.pelajaran.jawabMuridKuisUjian)
router.post('/murid/kuis-ujian/submit', auth.allow, controllers.pelajaran.submitMuridKuisUjian)
router.get('/murid/kuis-ujian/result/:userId/:examId', auth.allow, controllers.personalia.getSubmitMuridByKelasId);

// Learning Parents
router.post('/parents/create', auth.allow, controllers.personalia.createParents);
router.get('/parents/one/:id', auth.allow, controllers.personalia.getParentsById);
router.get('/parents/no-induk/:id', auth.allow, controllers.personalia.getParentsByNoInduk);
router.get('/parents/cek-user/:company_id', auth.allow, controllers.personalia.cekUserParents)
router.get('/parents/company/:companyId', auth.allow, controllers.personalia.getParentsByCompanyId);
router.get('/parents/murid/:muridId', auth.allow, controllers.personalia.getParentsByMuridId);
router.get('/parents/my-murid/:userId', auth.allow, controllers.personalia.getMyMuridByParentId);
router.put('/parents/update/:id', auth.allow, controllers.personalia.updateParentsById);
router.delete('/parents/one/:id', auth.allow, controllers.personalia.deleteParentsById);
router.delete('/parents/company/:id', auth.allow, controllers.personalia.deleteParentsByCompanyId);

// Learning Guru
router.post('/guru/create', auth.allow, controllers.personalia.createGuru);
router.post('/guru/import', auth.allow, controllers.personalia.importGuru);
router.get('/guru/one/:id', auth.allow, controllers.personalia.getGuruById);
router.get('/guru/no-induk/:id', auth.allow, controllers.personalia.getGuruByNoInduk);
router.get('/guru/company/:companyId', auth.allow, controllers.personalia.getGuruByCompanyId);
router.put('/guru/update/:id', auth.allow, controllers.personalia.updateGuruById);
router.delete('/guru/one/:id', auth.allow, controllers.personalia.deleteGuruById);
router.delete('/guru/company/:id', auth.allow, controllers.personalia.deleteGuruByCompanyId);

// Yang Mengumpulkan Tugas
router.get('/guru/mengumpulkan-tugas/:jadwalId/:examId', auth.allow, controllers.pelajaran.getMengumpulkanTugas);
router.get('/guru/mengumpulkan-kuis/:jadwalId/:examId', auth.allow, controllers.pelajaran.getMengumpulkanKuis);
router.get('/guru/mengumpulkan-ujian/:jadwalId/:examId', auth.allow, controllers.pelajaran.getMengumpulkanKuis);
router.get('/guru/detail-tugas/:id', auth.allow, controllers.pelajaran.getDetailTugas);
router.get('/guru/semua-tugas/:userId', auth.allow, controllers.pelajaran.getSemuaTugas);
router.put('/guru/detail-tugas/:id', auth.allow, controllers.pelajaran.beriNilaiTugas);
router.get('/guru/nilai-murid/:semesterId/:kelasId/:userId', auth.allow, controllers.pelajaran.getNilaiMurid);
router.get('/guru/nilai-kelas/:kelasId/:pelajaranId', auth.allow, controllers.pelajaran.getNilaiKelas);
router.get('/guru/nilai-rata/:kelasId/:pelajaranId', auth.allow, controllers.pelajaran.getNilaiRata);
router.get('/guru/tugas/:examId', auth.allow, controllers.pelajaran.getMengumpulkanTugasByExamId);
router.get('/guru/score-murid/:examId', auth.allow, controllers.pelajaran.getMengumpulkanScoreByExamId);

router.post('/principal/upload', auth.allow, controllers.kpi.uploadKPI);
router.get('/principal/kpi-guru/:companyId', controllers.kpi.getByCompanyId);

// ruangan mengajar
router.post('/ruangan-mengajar', controllers.ruangan_mengajar.create);
router.get('/ruangan-mengajar/:id', controllers.ruangan_mengajar.read);
router.get('/ruangan-mengajar/company/:id', controllers.ruangan_mengajar.readByCompany);
router.put('/ruangan-mengajar/:id', controllers.ruangan_mengajar.update);
router.delete('/ruangan-mengajar/:id', controllers.ruangan_mengajar.delete);
router.delete('/ruangan-mengajar/company/:id', controllers.ruangan_mengajar.deleteByCompany);

// jadwal mengajar
router.post('/jadwal-mengajar', auth.allow, controllers.jadwal.create);
router.get('/jadwal-mengajar/:id', auth.allow, controllers.jadwal.read);
router.get('/jadwal-mengajar/company/:id', auth.allow, controllers.jadwal.readByCompany);
router.get('/jadwal-mengajar/guru/:userId', auth.allow, controllers.jadwal.readGuruByUserId);
router.get('/jadwal-mengajar/murid/:userId', controllers.jadwal.readMuridByUserId);
router.get('/jadwal-mengajar/id/:id', auth.allow, controllers.jadwal.readByJadwalId);
router.put('/jadwal-mengajar/:id', auth.allow, controllers.jadwal.update);
router.delete('/jadwal-mengajar/:id', auth.allow, controllers.jadwal.delete);
router.delete('/jadwal-mengajar/company/:id', auth.allow, controllers.jadwal.deleteByCompany);

// events
router.get('/events/guru/:userId', /*auth.allow,*/ controllers.jadwal.eventGuruByUserId);
router.get('/events/murid/:userId', /*auth.allow,*/ controllers.jadwal.eventMuridByUserId);

// jadwal, tugas, kuis, ujian murid
router.post('/tugas-murid/submit', auth.allow, controllers.exam.postTugasMurid);
router.post('/tugas-murid/submit-langsung', auth.allow, controllers.exam.postTugasMuridLangsung);
router.get('/jadwal-murid/:userId', auth.allow, controllers.exam.getJadwalMuridByUserId);
router.get('/tugas-murid/:userId', auth.allow, controllers.exam.getTugasMuridByUserId);
router.get('/kuis-murid/:userId', auth.allow, controllers.exam.getKuisMuridByUserId);
router.get('/ujian-murid/:userId', auth.allow, controllers.exam.getUjianMuridByUserId);

// learning chapter
router.post('/learning-chapter', controllers.learning_chapter.create);
router.get('/learning-chapter/:id', controllers.learning_chapter.read);
router.get('/learning-chapter/list/:pelajaran', controllers.learning_chapter.list_chapter);
router.delete('/learning-chapter/:id', controllers.learning_chapter.delete);
router.put('/learning-chapter', controllers.learning_chapter.update);
router.post('/learning-chapter/answer', controllers.learning_chapter.answer);

// learning format KPI
router.post('/learning-kpi', controllers.company.createKpi);
router.get('/learning-kpi/company/:companyId', controllers.company.getKpiByCompanyId);
router.get('/learning-kpi/id/:id', controllers.company.getKpiById);
router.put('/learning-kpi/:id', controllers.company.uploadKpiById);
router.delete('/learning-kpi/:id', controllers.company.deleteKpi);

// Company Limit
router.get('/company-limit/:id', controllers.company.checkLimit);

// webinar_test
router.post('/webinar-test/set-score-essay', auth.allow, controllers.webinar_test.setScoreEssay);
router.post('/webinar-test', auth.allow, controllers.webinar_test.create);
router.delete('/webinar-test/pertanyaan/:id', auth.allow, controllers.webinar_test.deleteByIdPertanyaan);
router.delete('/webinar-test/:id/:jenis', auth.allow, controllers.webinar_test.delete);
router.put('/webinar-test', auth.allow, controllers.webinar_test.update);
router.get('/webinar-test/result/:id', /* auth.allow, */ controllers.webinar_test.result);
router.get('/webinar-test/result/:id/:jenis/:user', /* auth.allow, */ controllers.webinar_test.resultByUser);
router.get('/webinar-test/:id/:jenis', auth.allow, controllers.webinar_test.read);
router.get('/webinar-test-peserta/:id/:jenis/:user_id', controllers.webinar_test.readPeserta);
router.post('/webinar-test/input', controllers.webinar_test.input);
//tambahan essay
router.post('/webinar-test/essay', controllers.webinar_test.webinarEssay);
router.post('/webinar-test/import', auth.allow, controllers.webinar_test.import);

//polling
router.post('/webinar-test-polling/import', auth.allow, controllers.webinar_test.import_polling);
router.post('/webinar-test-polling', auth.allow, controllers.webinar_test.create_polling);
router.put('/webinar-test-polling', auth.allow, controllers.webinar_test.update_polling);
router.delete('/webinar-test-polling/:id', auth.allow, controllers.webinar_test.deleteByIdPolling);
router.get('/webinar-test-polling/:id', auth.allow, controllers.webinar_test.view_polling_live);
router.get('/webinar-test-polling/view/:id', auth.allow, controllers.webinar_test.view_polling);

router.post('/webinar-test-polling-submit', controllers.webinar_test.submit_polling);
router.put('/webinar-test-polling/:id', controllers.webinar_test.update_polling_id);
router.post('/webinar-test-polling-single', controllers.webinar_test.create_polling_id);
router.put('/webinar-test-polling-status/:id', controllers.webinar_test.update_polling_status);
router.get('/webinar-polling/guestspeaker/:id', controllers.webinar_test.view_polling_live);

// Reminder
router.get('/reminder/meeting/:waktu', controllers.reminder.meeting);
router.get('/reminder/webinar/:waktu', controllers.reminder.webinar);
router.get('/reminder/ptc/:waktu', controllers.reminder.ptc);
router.get('/reminder/tugas/:waktu', controllers.reminder.tugas);
router.get('/reminder/kuis/:waktu', controllers.reminder.quiz);
router.get('/reminder/ujian/:waktu', controllers.reminder.ujian);
router.get('/reminder/pelajaran-today/:waktu', controllers.reminder.pelajaranToday);

// Project Tags
router.get('/project/tags/:id', auth.allow, controllers.project.getProjectTags);
router.post('/project/tags', auth.allow, controllers.project.createTags);
router.put('/project/set-tags', auth.allow, controllers.project.setTags);

// Project Share
router.post('/project/share', auth.allow, controllers.project.addProjectShare);
router.get('/project/share/:id', auth.allow, controllers.project.getProjectShare);
router.delete('/project/share/:id', auth.allow, controllers.project.deleteProjectShare);
router.get('/project/user/:id', controllers.project.getUserProject);
router.get('/project/gantt/user/:id', controllers.project.getUserProjectGantt);
router.get('/project/gantt-user/user/:userId', controllers.project.getUserGanttByCompany);
router.get('/project/gantt/company/:id', controllers.project.getProjectCompany);
router.get('/project/gantt-user/company/:id', controllers.project.getProjectCompanyByUserReport);

// Gant Tasks
router.get('/gantt/:projectId/:visibility', controllers.gantt.getByProject);
router.get('/gantt-user/:projectId/:userId/:visibility', controllers.gantt.getByUser);
router.put('/gantt/:userId/:projectId/task/:id', controllers.gantt.update);
router.post('/gantt/:userId/:projectId/task', controllers.gantt.insert);
router.delete('/gantt/:userId/:projectId/task/:id', controllers.gantt.delete);
router.post('/gantt/:userId/:projectId/link', controllers.gantt.insertLink);
router.put('/gantt/:userId/:projectId/link/:id', controllers.gantt.updateLink);
router.delete('/gantt/:userId/:projectId/link/:id', controllers.gantt.deleteLink);
router.get('/gantt-history/:id', controllers.gantt.getHistory);

router.post('/conference/create', auth.allow, controllers.conference.create);
// Webhook BBB
router.post('/conference-log', controllers.conference.log)

// Webhook BBB - Logs dari postMessage
router.post('/conference-logs', controllers.conference.logs)

// Configuration
router.get('/configuration/:variable', controllers.configuration.getConfig);

// Mobile and Personal Dashboard
router.get(`/dashboard/upcomming-schedule/:userId/:companyId/:date`, controllers.personalDashboard.getUpcommingSchedule);
router.get(`/dashboard/schedule/:userId/:companyId/:date`, controllers.personalDashboard.getTodaySchedule);
router.get(`/dashboard/task-overdue/:userId/:companyId`, controllers.personalDashboard.getTaskOverdue);
router.get('/dashboard/project/:level/:user_id/:company_id', controllers.personalDashboard.getProjectListDash);
router.get('/search/:text', controllers.search.search);

router.get('/grup/:grup_id', auth.allow, controllers.grup.getGrup);

// Global Settings
router.get('/global-settings/:company_id/:role', auth.allow, controllers.grup.getAccess);
router.get('/global-settings/check-access', auth.allow, controllers.grup.checkAccess);
router.get('/global-settings/check-access-all', auth.allow, controllers.grup.checkAccessAll);
router.put('/global-settings/:id_access', auth.allow, controllers.grup.patchStatus);
router.post('/private/global-settings', auth.allow, controllers.grup.insertGlobalSettingAllCompany);
router.post('/private/create-new-global-settings', auth.allow, controllers.grup.createNewGlobalSettings);

router.get('/notification-alert/:company_id/:role', controllers.notification.getAccess);
router.get('/notif', controllers.notification.getNotif);
router.get('/notification-alert/check-access', controllers.notification.checkAccess);
router.put('/notification-alert/:id_access/:type', controllers.notification.patchStatus);

/** ZOOM */
router.post('/zoom/signature', controllers.zoom.signature);
router.get('/zoom/callback', controllers.zoom.callback);

router.post('/zoom/refresh', controllers.zoom.refreshToken);

router.get('/zoom/meetings', controllers.zoom.getMeetings);
router.get('/zoom/meetings/:id', controllers.zoom.getMeeting);
router.post('/zoom/meeting', controllers.zoom.createMeeting);

router.get('/liveclass/zoom/:id', controllers.zoom.liveclassZoom);
router.get('/webinar/zoom/:id', controllers.zoom.webinarZoom);

router.get('/kurikulum/company/:company_id', auth.allow, controllers.kurikulum.getKurikulumByCompany);
router.get('/kurikulum/id/:id', auth.allow, controllers.kurikulum.getKurikulumByKurikulum);
router.post('/kurikulum/:id/mapel', auth.allow, controllers.kurikulum.addMapelById);
router.post('/kurikulum', auth.allow, controllers.kurikulum.createKurikulum);
router.put('/kurikulum/:id', auth.allow, controllers.kurikulum.updateKurikulum);
router.delete('/kurikulum/:id/mapel/:mapel', auth.allow, controllers.kurikulum.deleteMapelKurikulum);
router.delete('/kurikulum/:id', auth.allow, controllers.kurikulum.deleteKurikulum);

router.get('/exam/:examId', auth.allow, controllers.exam.getOneExamById);
router.put('/exam/:event/:id', auth.allow, controllers.exam.updateStarted);

/* TRAINING -> COMPANY */
router.post('/training/company', auth.allow, controllers.training_company.create);
router.post('/training/company/import', auth.allow, controllers.training_company.import);
router.get('/training/company/read/:id', auth.allow, controllers.training_company.read);
router.get('/training/company/read-public/:id', controllers.training_company.read);
router.get('/training/company/:id', auth.allow, controllers.training_company.browseByCompany);
router.get('/training/company/:idCompany/:idCourse', auth.allow, controllers.training_company.getListCompanyByIdCourse);
router.get('/training/company-archived/:id', auth.allow, controllers.training_company.browseByCompanyArchived);
router.put('/training/company/:id', auth.allow, controllers.training_company.update);
router.put('/training/company/enable-registration/:id', auth.allow, controllers.training_company.updateEnableRegist);
router.put('/training/company/enable-paid-registration/:id', auth.allow, controllers.training_company.updateEnablePaidRegist);
router.put('/training/company/image/:id', auth.allow, controllers.training_company.uploadImage);
router.delete('/training/company/:id', auth.allow, controllers.training_company.delete);
router.put('/training/company-activate/:id', auth.allow, controllers.training_company.activate);

/* TRAINING -> LICENSES ALLOCATION */
router.get('/training/licenses-allocation/:training_company_id', auth.allow, controllers.training_licenses_allocation.allocation);
router.get('/training/licenses-allocation-transaction-history/:training_company_id', auth.allow, controllers.training_licenses_allocation.transactionHistory);
router.post('/training/licenses-allocation-transaction', auth.allow, controllers.training_licenses_allocation.allocationTransaction);

/* TRAINING -> QUOTA */
router.get('/training/quota/company', auth.allow, controllers.training_licenses_allocation.getCompanyList);
router.get('/training/quota/company/:id', auth.allow, controllers.training_licenses_allocation.allocationCompany);
router.get('/training/quota/company/history/:id', auth.allow, controllers.training_licenses_allocation.transactionHistoryCompany);
router.post('/training/quota/company/transaction', auth.allow, controllers.training_licenses_allocation.allocationTransactionCompany);

/* TRAINING -> USER */
router.post('/training/user', auth.allow, controllers.training_user.create);
router.post('/training/user-registration', controllers.training_user.createInactive);
router.post('/training/user/import', auth.allow, controllers.training_user.import);
router.get('/training/user/read/:id', auth.allow, controllers.training_user.read);
router.get('/training/user/read/user/:id', auth.allow, controllers.training_user.readByUserId);
router.get('/training/user/:level/:id', auth.allow, controllers.training_user.browseByCompany);
router.get('/training/user-archived/:level/:id', auth.allow, controllers.training_user.browseByCompanyArchived);
router.get('/training/user/training-company/:level/:id', auth.allow, controllers.training_user.browseByTrainingCompany);
router.get('/training/user-archived/training-company/:level/:id', auth.allow, controllers.training_user.browseByTrainingCompanyArchived);
router.put('/training/user/:id', auth.allow, controllers.training_user.update);
router.put('/training/user/image/:id', auth.allow, controllers.training_user.uploadImage);
router.put('/training/user/image-identity/:id', auth.allow, controllers.training_user.uploadImageIdentity);
router.delete('/training/user/:id', auth.allow, controllers.training_user.delete);
router.post('/training/update-status-user-bulk', auth.allow, controllers.training_user.updateStatusTrainingUserBulk);
router.put('/training/user-activate/:id', auth.allow, controllers.training_user.activate);
router.get('/training/user-by-company/:id', controllers.training_user.getUserForWebinar);
router.put('/training/user-password', auth.allow, controllers.training_user.changePassword);
router.get('/training/user-history/:id', auth.allow, controllers.training_user.history);

router.post('/chapter/exam', auth.allow, controllers.exam.createChapterExam)
router.get('/chapter/:id', controllers.chapter.getChapterWithTugasAndKuis)
router.delete('/chapter/exam/:id', auth.allow, controllers.exam.deleteChapterExam)

/* TRAINING -> SETTINGS */
router.get('/training/settings/licenses-type/:company_id', auth.allow, controllers.training_licenses_type.browse);
router.post('/training/settings/licenses-type', auth.allow, controllers.training_licenses_type.create);
router.put('/training/settings/licenses-type/image/:id', auth.allow, controllers.training_licenses_type.uploadImage);
router.get('/training/settings/licenses-type/delete-image/:id', auth.allow, controllers.training_licenses_type.deleteImage);
router.put('/training/settings/licenses-type', auth.allow, controllers.training_licenses_type.edit);
router.delete('/training/settings/licenses-type/:id', auth.allow, controllers.training_licenses_type.delete);
router.get('/training/settings/license-format/:id', auth.allow, controllers.training_licenses_type.readLicenseFormat);
router.put('/training/settings/license-format/:id', auth.allow, controllers.training_licenses_type.updateLicenseFormat);

/* TRAINING -> COURSE */
// BY COMPANY ID
router.get('/training/course/assignment/:courseId', auth.allow, controllers.training_course.getAssignmentCompany_byCompanyId);
router.post('/training/course/assignment', auth.allow, controllers.training_course.storeAssignmentCompany_byCompany_id);
router.post('/send-certificate-course', auth.allow, controllers.training_course.sendCertificateCourse);

// BY COURSE ID
router.get('/training/course/assignment/by-course/:companyId', auth.allow, controllers.training_course.getAssignmentCompany_byCourseId);
router.post('/training/course/assignment/by-course', auth.allow, controllers.training_course.storeAssignmentCompany_byCourseId);
router.post('/training/course/deleteAssignment', auth.allow, controllers.training_course.deleteAssignmentCompany_byId);
router.get('/training/report-course/:idCourse/:idCompany', auth.allow, controllers.training_course.retrieveReportCourseByIdCourse);
// ASSIGN TO USER
router.post('/training/course/assignment-to-user', auth.allow, controllers.training_course.storeAssignmentUser);

router.get('/training/course-list/:company_id', auth.allow, controllers.training_course.browse);
router.get('/training/course-list-admin/:company_id', auth.allow, controllers.training_course.browseAdmin);
router.get('/training/course-list-question/:company_id', auth.allow, controllers.training_course.browseForQuestion);
router.get('/training/course-list-admin-archived/:company_id', auth.allow, controllers.training_course.browseAdminArchived);
router.post('/training/course', auth.allow, controllers.training_course.create);
router.post('/training/course/session', auth.allow, controllers.training_course.addSession);
router.delete('/training/course/session/:courseId/:type/:id', auth.allow, controllers.training_course.deleteSession);
router.delete('/training/course/:id', auth.allow, controllers.training_course.delete);
router.put('/training/course-activate/:id', auth.allow, controllers.training_course.activate);
router.put('/training/course/:id', auth.allow, controllers.training_course.update);
router.put('/training/course/image/:id', auth.allow, controllers.training_course.uploadImage);
router.post('/training/course/session/media/:id', auth.allow, controllers.training_course.uploadMedia);
router.delete('/training/course/session/media/:id', auth.allow, controllers.training_course.deleteMedia);
router.post('/training/course/session/read', auth.allow, controllers.training_course.readSession);
router.get('/training/course/read/:id', auth.allow, controllers.training_course.read);

/* TRAINING -> EXAM */
router.post('/training/exam', auth.allow, controllers.training_exam.create);
router.get('/training/exam/read/:id', auth.allow, controllers.training_exam.read);
router.get('/training/exam/read-user/:assignee_id', auth.allow, controllers.training_exam.readByUser);
router.get('/training/exam/result/:assignee_id', auth.allow, controllers.training_exam.result);
router.get('/training/exam/result-by-result/:result_id', auth.allow, controllers.training_exam.resultByExamResult);
router.get('/training/exam/read/answer/:assignee_id', auth.allow, controllers.training_exam.readAnswer);
router.get('/training/exam/read/answer-by-result/:result_id', auth.allow, controllers.training_exam.readAnswerByResult);
router.post('/training/exam/submit', auth.allow, controllers.training_exam.submit);
router.post('/training/exam/temporary-submit', auth.allow, controllers.training_exam.updateAnswerExam);
router.get('/training/exam/:id/:exam', auth.allow, controllers.training_exam.browseByCompany);
router.get('/training/exam-by-user/:training_user_id', auth.allow, controllers.training_exam.browseByTrainingUserId);
router.get('/training/exam-course-company/:id/:exam', auth.allow, controllers.training_exam.browseByCompanyCourse);
router.get('/training/exam-archived/:id/:exam', auth.allow, controllers.training_exam.browseByCompanyArchived);
router.get('/training/exam-by-user/:training_user_id/:company_id/:exam', auth.allow, controllers.training_exam.browseByUser);
router.get('/training/exam-by-course/:training_user_id/:company_id/:exam/:course_id', auth.allow, controllers.training_exam.browseByCourse);
router.get('/training/exam-history-by-user/:training_user_id/:company_id/:exam', auth.allow, controllers.training_exam.browseHistoryByUser);
router.delete('/training/exam/:id', auth.allow, controllers.training_exam.delete);
router.put('/training/exam-activate/:id', auth.allow, controllers.training_exam.activate);
router.put('/training/exam/image/:id', auth.allow, controllers.training_exam.uploadImage);
router.put('/training/exam/:id', auth.allow, controllers.training_exam.update);
router.post('/training/exam/question', auth.allow, controllers.training_exam.addQuestion);
router.delete('/training/exam/question/:id', auth.allow, controllers.training_exam.deleteQuestion);
router.post('/training/exam/import', auth.allow, controllers.training_exam.import);
router.post('/training/assign', auth.allow, controllers.training_exam.assign);
router.post('/training/bulk-assign', auth.allow, controllers.training_exam.assignBulk);
router.get('/training/assignee/:training_user_id', auth.allow, controllers.training_exam.readAssignee);
router.get('/training/assignee/exam/:exam_id', auth.allow, controllers.training_exam.readAssigneeByExam);
router.delete('/training/assign/:id/:created_by', auth.allow, controllers.training_exam.deleteAssignee);
router.delete('/training/bulk-assign', auth.allow, controllers.training_exam.deleteAssigneeBulk);
router.get('/training/exam-by-course/:idCourse/:idCompany', auth.allow, controllers.training_exam.readExamByCourse);

/* TRAINING -> QUESTIONS */
router.get('/training/questions/:id', auth.allow, controllers.training_questions.browseByCompany);
router.get('/training/questions-archived/:id', auth.allow, controllers.training_questions.browseByCompanyArchived);
router.get('/training/questions/read/:id', auth.allow, controllers.training_questions.read);
router.post('/training/questions', auth.allow, controllers.training_questions.create);
router.post('/training/questions/import', auth.allow, controllers.training_questions.import);
router.delete('/training/questions/:id', auth.allow, controllers.training_questions.delete);
router.post('/training/update-status-questions', auth.allow, controllers.training_questions.updateStatusQuestionsBulk);
router.put('/training/questions-activate/:id', auth.allow, controllers.training_questions.activate);
router.put('/training/questions/:id', auth.allow, controllers.training_questions.update);

/* TRAINING -> MEMBERSHIP */
router.get('/training/check-user-membership/:idTrainingUser', auth.allow, controllers.training_membership.checkUserMembership);
router.get('/training/membership-company/:id/:status', auth.allow, controllers.training_membership.browseByCompany);
router.get('/training/membership-training/:id/:status', auth.allow, controllers.training_membership.browseByTrainingCompany);
router.get('/training/membership/read/:id', auth.allow, controllers.training_membership.read);
router.get('/training/membership/read-user/:id', auth.allow, controllers.training_membership.readByUser);
router.post('/training/membership', auth.allow, controllers.training_membership.createByResultId);
router.put('/training/membership/photo/:id', auth.allow, controllers.training_membership.uploadImage);
router.delete('/training/membership/:id', auth.allow, controllers.training_membership.delete);
router.put('/training/membership-activate/:id', auth.allow, controllers.training_membership.activate);

/* TRAINING -> REPORT */
router.post('/training/report', auth.allow, controllers.training_exam.report);
router.post('/training/detail-report-training', auth.allow, controllers.training_report.detailReportTraining)
router.get('/training/detail-report-user', auth.allow, controllers.training_report.detailReportTrainingUser)
router.post('/training/certificate', controllers.training_exam.certificate);
/* ANALITIC EXAM */
router.get('/training/report/overview-exam/:company_id', auth.allow, controllers.training_report.getExamOverviewByCompany);
router.post('/training/report/overview-exam-selected/:company_id', auth.allow, controllers.training_report.postSelectedExamOverview);

/* TRAINING -> PLAN */
router.get('/training/plan/:company_id', auth.allow, controllers.training_course.plan);
router.get('/training/plan-user/:training_user_id', auth.allow, controllers.training_course.planByTrainingUserId);
router.get('/training/plan-user-result/:training_user_id/:assignee_id', auth.allow, controllers.training_course.planResultByAssignee);
router.get('/training/plan/read/:id', auth.allow, controllers.training_course.detailPlan);
router.put('/training/plan/course/:id', controllers.training_plan.updateCourse);
router.put('/training/plan/exam/:id', controllers.training_plan.updateExam);
router.put('/training/plan/liveclass/:id', controllers.training_plan.updateLiveclass);
router.get('/training/plan/user/:level/:id', auth.allow, controllers.training_course.browseUserByCompany);

/* NEWS */
router.post('/news', auth.allow, controllers.news.create);
router.get('/news/read/:id', auth.allow, controllers.news.read);
router.get('/news/:id', auth.allow, controllers.news.browseByCompany);
router.put('/news/:id', auth.allow, controllers.news.update);
router.put('/news/image/:id', auth.allow, controllers.news.uploadImage);
router.delete('/news/:id', auth.allow, controllers.news.delete);

/* TRAINING SUBMISSION */
router.post('/training/submission/post', auth.allow, controllers.training_course.postSubmission);
router.post('/training/submission-user-answer', auth.allow, controllers.training_course.userAnswerSubmission);
router.get('/training/detail-submission-user/:idTrainingUser/:idSubmission', auth.allow, controllers.training_course.getDetailUserSubmission);
router.delete('/training/submission/delete/:id', auth.allow, controllers.training_course.deleteSubmission);
router.get('/training/submission/get/:id', auth.allow, controllers.training_course.getSubmission);
router.get('/training/submission/read/:id', auth.allow, controllers.training_course.readSubmission);
router.post('/training/submission/assign', auth.allow, controllers.training_course.assigneeSubmission);
router.post('/training/course/submission/media/:id', auth.allow, controllers.training_course.uploadMediaSubmission);
router.delete('/training/course/submission/media/:id', auth.allow, controllers.training_course.deleteMediaSubmission);
router.post('/training/course/submission/set-score', auth.allow, controllers.training_course.setScoreSubmission);

/** TRAINING USER REGISTRATION FORM */
router.get('/training/setup-registration-form/:id', controllers.training_company.getFormRegisterCompany);
router.post('/training/company/registration-form/:id', auth.allow, controllers.training_company.postFormRegisterCompany);
router.post('/training/user-registration-form', controllers.training_user.userRegistration);
router.get('/training/user-registration/:idCompany', auth.allow ,controllers.training_user.readUserRegistrationByTrainingCompany);
router.get('/training/user-registration/detail-form/:idTrainingUser', auth.allow ,controllers.training_user.readUserRegistrationByIdTraining);
router.post('/training/activate-user-registration', auth.allow , controllers.training_user.activateUserRegistration);
router.post('/training/remove-user-registration', auth.allow , controllers.training_user.removeUserRegistration);
router.post('/training/registration-user/media',  controllers.training_user.uploadMedia);
router.delete('/training/registration-user/media/:id', controllers.training_user.deleteMedia);

/* PAYMENT GATEWAY XENDIT */
router.post('/payment/xendit/create', controllers.xendit.create);
router.post('/payment/xendit/callback', controllers.xendit.callback);
router.get('/payment/xendit/detail/:id', controllers.xendit.detail);

/** CRM ------ */
/** GET FILTER DATA */
router.get('/crm/task/filter-data/:idProject', auth.allow,controllers.crmTask.getTaskFilterData);
/** TASK */
router.get('/crm/task/read', auth.allow,controllers.crmTask.getTask);
router.get('/crm/task/read-list', auth.allow,controllers.crmTask.getTaskList);
router.get('/crm/task/read-gantt/:project_id/:group_by/:milestone_id/:status', auth.allow,controllers.crmTask.getGanttData);
router.get('/crm/task/read/:id', auth.allow,controllers.crmTask.getTask);
router.get('/crm/task/read/attribute-project/:idProject', auth.allow,controllers.crmTask.getAttributeProject);
router.get('/crm/task/read/comments/:idTask/:idProject', auth.allow,controllers.crmTask.getTaskComments);
router.get('/crm/task/read/activity/:idTask/:idProject', auth.allow,controllers.crmTask.getTaskActivity);
router.post('/crm/task/import/checking', auth.allow,controllers.crmTask.importTaskChecking);
router.post('/crm/task/import/store', auth.allow,controllers.crmTask.importTaskStore);
router.post('/crm/task/store', auth.allow,controllers.crmTask.storeTask);
router.put('/crm/task/edit/:id/:singleInput', auth.allow,controllers.crmTask.editTask);
router.post('/crm/task/batchupdate', auth.allow,controllers.crmTask.editTaskBatch);
router.put('/crm/task/edit-kanban', auth.allow,controllers.crmTask.editTaskKanban);
router.post('/crm/task/store/reminder', auth.allow,controllers.crmTask.storeTaskReminder);
router.delete('/crm/task/delete/reminder/:id', auth.allow,controllers.crmTask.deleteTaskReminder);
router.post('/crm/task/store/comment', auth.allow,controllers.crmTask.storeTaskComment);
router.post('/crm/task/delete/comment', auth.allow,controllers.crmTask.deleteTaskComment);
router.post('/crm/task/store/comment/:attType', auth.allow,controllers.crmTask.storeTaskCommentAttribute);
router.post('/crm/task/store/logged', auth.allow,controllers.crmTask.storeTaskLogged);
/** PROJECT*/
router.get('/crm/project/read', auth.allow,controllers.crmProject.getProjects);
router.post('/crm/project/read-filter-data', auth.allow,controllers.crmProject.getProjectsFilterData);
router.post('/crm/project/read/milestone', auth.allow,controllers.crmProject.getProjectsMilestone);
router.post('/crm/project/label/:status', auth.allow,controllers.crmProject.storeProjectLabel);
router.post('/crm/project/store', auth.allow,controllers.crmProject.storeProjects);
router.post('/crm/project/edit', auth.allow,controllers.crmProject.editProjects);
router.post('/crm/project/remove', auth.allow,controllers.crmProject.deleteProjects);
router.post('/crm/project/clone', auth.allow,controllers.crmProject.cloneProjects);
router.post('/crm/project/milestones/:status', auth.allow,controllers.crmProject.storeProjectMilestones);
router.post('/crm/project/add-member', auth.allow,controllers.crmProject.storeProjectMembers);
router.post('/crm/project/remove-member', auth.allow,controllers.crmProject.removeProjectMembers);
router.get('/crm/project/get-project-member/:idProject', auth.allow,controllers.crmProject.getProjectMembers);
router.get('/crm/project/read/activity/:idProject', auth.allow,controllers.crmProject.getProjectActivity);
router.post('/crm/project/store/reminder', auth.allow,controllers.crmProject.storeProjectReminder);
router.get('/crm/project/get/reminder', auth.allow,controllers.crmProject.getProjectReminder);

router.get('/crm/project/notes/:idProject', auth.allow,controllers.crmProject.getProjectNotes);
router.post('/crm/project/notes', auth.allow,controllers.crmProject.postProjectNotes);
router.put('/crm/project/notes', auth.allow,controllers.crmProject.updateProjectNotes);
router.delete('/crm/project/notes/:id', auth.allow,controllers.crmProject.removeProjectNotes);
router.delete('/crm/project/notes/file/:id', auth.allow,controllers.crmProject.removeProjectNotesFile);

/** OPERATORS */
router.get('/crm/operators/list', auth.allow,controllers.crmOperators.getOperators);
router.post('/crm/operators/:status', auth.allow,controllers.crmOperators.storeOperators);


module.exports = router;
