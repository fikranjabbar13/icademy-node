var express = require('express');
var router = express.Router();

var env = require('../env.json');
var jwt = require('jsonwebtoken');
var fs = require('fs');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'API ICADEMY' });
});

router.get('/auth/:user/:pass', function(req, res, next) {
	var userParams = req.params.user;
	var passParams = req.params.pass;

	if(!userParams) return res.json({ status: 500, message: 'Params user must be filled.'});
	if(!passParams) return res.json({ status: 500, message: 'Params pass must be filled.'});

	var token = jwt.sign(
		{ user: userParams, pass: passParams },
		env.APP_KEY,
		{ algorithm: 'HS384' }
	);

	res.json({ error: false, result: token });
});

router.get('/token', function(req, res, next) {
  if(req.query.hasOwnProperty('name')) {
    var obj = {
      context: {
        user: {
          avatar: req.query.avatar,
          email: req.query.email,
          name: req.query.name,
          id: req.query.email
        }
      },
      sub: "meet.icademy.id",
      room: req.query.room,
      moderator: req.query.moderator
    };

    var cert = fs.readFileSync('routes/private.key');
    var token = jwt.sign(obj, cert, { algorithm: 'RS256'});
    res.json({ error: false, token: token });

  } else {
    res.json({ error: true, token: '' });
  }
});

module.exports = router;
